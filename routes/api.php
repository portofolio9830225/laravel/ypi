<?php

use App\Http\Controllers\API\AuthAPIController;
use Illuminate\Support\Facades\Route;
use Stancl\Tenancy\Middleware\InitializeTenancyByRequestData;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/login', [AuthAPIController::class, 'auth']);
Route::post('/register', [AuthAPIController::class, 'register']);

Route::middleware(['auth:sanctum'])->group(function () {
    Route::get('/user', function () {
        return auth()->user();
    })->middleware('CekRole:superadmin');

    Route::resource('users', App\Http\Controllers\API\UserAPIController::class);

    Route::get('clubs', [App\Http\Controllers\API\ClubAPIController::class, 'showAll']);
    Route::resource('club', App\Http\Controllers\API\ClubAPIController::class)->middleware('CekRole:superadmin');
});

Route::middleware(['auth:sanctum', InitializeTenancyByRequestData::class])->group(function () {

    Route::get('templates_data', [App\Http\Controllers\API\TemplateDataAPIController::class, 'templates']);
    Route::get('templates_data/{name}', [App\Http\Controllers\API\TemplateDataAPIController::class, 'populateTenantData']);

    Route::resource('ypi', App\Http\Controllers\API\YpiAPIController::class);

    Route::resource('subscription', App\Http\Controllers\API\SubscriptionController::class);

    Route::resource('club_subscription', App\Http\Controllers\API\ClubSubscriptionAPIController::class);


    Route::resource('gym_types', App\Http\Controllers\API\GymTypeAPIController::class);


    Route::resource('tabel_kategoris', App\Http\Controllers\API\TabelKategoriAPIController::class);


    Route::resource('level_kategoris', App\Http\Controllers\API\LevelKategoriAPIController::class);


    Route::resource('tabel_kelompoks', App\Http\Controllers\API\TabelKelompokAPIController::class);


    Route::resource('master_contacts', App\Http\Controllers\API\MasterContactAPIController::class);


    Route::resource('master_pelatihs', App\Http\Controllers\API\MasterPelatihAPIController::class);


    Route::get('master_pemains/by_coach/{coach_id}', [App\Http\Controllers\API\MasterPemainAPIController::class, 'byCoach']);
    Route::get('master_pemains/not_coach/{coach_id}', [App\Http\Controllers\API\MasterPemainAPIController::class, 'notCoach']);
    Route::post('master_pemains/pelatih', [App\Http\Controllers\API\MasterPemainAPIController::class, 'pelatih']);
    Route::post('master_pemains/remove_pelatih', [App\Http\Controllers\API\MasterPemainAPIController::class, 'removePelatih']);
    Route::resource('master_pemains', App\Http\Controllers\API\MasterPemainAPIController::class);


    Route::resource('competition_statuses', App\Http\Controllers\API\CompetitionStatusAPIController::class);


    Route::resource('master_ypis', App\Http\Controllers\API\MasterYpiAPIController::class);


    Route::resource('exercise_categories', App\Http\Controllers\API\ExerciseCategoryAPIController::class);


    Route::resource('sub_exercises', App\Http\Controllers\API\SubExerciseAPIController::class);


    Route::get('/national_height_avgs/nations', [App\Http\Controllers\API\NationalHeightAvgAPIController::class, 'groupByNation']);
    Route::post('national_height_avgs/batchStore', [App\Http\Controllers\API\NationalHeightAvgAPIController::class, 'batchStore']);
    Route::resource('national_height_avgs', App\Http\Controllers\API\NationalHeightAvgAPIController::class);


    Route::resource('group_activities', App\Http\Controllers\API\GroupActivityAPIController::class);


    Route::post('annual_plan_settings/batchStore', [App\Http\Controllers\API\AnnualPlanSettingAPIController::class, 'batchStore']);
    Route::resource('annual_plan_settings', App\Http\Controllers\API\AnnualPlanSettingAPIController::class);

    Route::post('setting_attendances/batchStore', [App\Http\Controllers\API\SettingAttendanceAPIController::class, 'batchStore']);
    Route::resource('setting_attendances', App\Http\Controllers\API\SettingAttendanceAPIController::class);

    Route::get('header_weekly_plans/related_data', [App\Http\Controllers\API\HeaderWeeklyPlanAPIController::class, 'relatedData']);
    Route::post('header_weekly_plans/withDetails', [App\Http\Controllers\API\HeaderWeeklyPlanAPIController::class, 'withDetails']);
    Route::resource('header_weekly_plans', App\Http\Controllers\API\HeaderWeeklyPlanAPIController::class);


    Route::resource('header_gaps', App\Http\Controllers\API\HeaderGapAPIController::class);


    Route::resource('detailkpi_events', App\Http\Controllers\API\DetailkpiEventAPIController::class);


    Route::post('medical_records/data_report', [App\Http\Controllers\API\MedicalRecordAPIController::class, 'reportData']);
    Route::get('medical_records/table_report', [App\Http\Controllers\API\MedicalRecordAPIController::class, 'tableReport']);
    Route::resource('medical_records', App\Http\Controllers\API\MedicalRecordAPIController::class);


    Route::resource('result_categories', App\Http\Controllers\API\ResultCategoryAPIController::class);


    Route::post('competition_reports/details', [App\Http\Controllers\API\CompetitionReportAPIController::class, 'storeWithDetailData']);
    Route::resource('competition_reports', App\Http\Controllers\API\CompetitionReportAPIController::class);


    Route::post('training_attendances/delete_daily', [App\Http\Controllers\API\TrainingAttendanceAPIController::class, 'deleteDaily']);
    Route::post('training_attendances/store_daily', [App\Http\Controllers\API\TrainingAttendanceAPIController::class, 'storeDaily']);
    Route::post('training_attendances/show_daily', [App\Http\Controllers\API\TrainingAttendanceAPIController::class, 'showDaily']);
    Route::post('training_attendances/batch_save', [App\Http\Controllers\API\TrainingAttendanceAPIController::class, 'batchStore']);
    Route::post('training_attendances/sessions_range', [App\Http\Controllers\API\TrainingAttendanceAPIController::class, 'getSessionsRange']);
    Route::resource('training_attendances', App\Http\Controllers\API\TrainingAttendanceAPIController::class);


    Route::resource('strength_conditionings', App\Http\Controllers\API\StrengthConditioningAPIController::class);

    Route::post('strength_conditioning_datas/updateRm', [App\Http\Controllers\API\StrengthConditioningDataAPIController::class, 'updateRmCalculated']);
    Route::resource('strength_conditioning_datas', App\Http\Controllers\API\StrengthConditioningDataAPIController::class);

    Route::post('height_predicteds/maturationalStatus', [App\Http\Controllers\API\HeightPredictedAPIController::class, 'storeMaturationalStatus']);
    Route::resource('height_predicteds', App\Http\Controllers\API\HeightPredictedAPIController::class);


    Route::get('fitness_tools/table_detail_report/{date}/{gender}/{group_id}', [App\Http\Controllers\API\FitnessToolAPIController::class, 'tableDetailReport']);
    Route::get('fitness_tools/table_report', [App\Http\Controllers\API\FitnessToolAPIController::class, 'tableReport']);
    Route::resource('fitness_tools', App\Http\Controllers\API\FitnessToolAPIController::class);

    Route::resource('master_achievements', App\Http\Controllers\API\MasterAchievementAPIController::class);

    Route::get('fitness_tests/table_detail_report/{date}/{gender}/{group_id}', [App\Http\Controllers\API\FitnessTestAPIController::class, 'tableDetailReport']);
    Route::get('fitness_tests/table_data_report', [App\Http\Controllers\API\FitnessTestAPIController::class, 'tableDataReport']);
    Route::post('fitness_tests/table_data', [App\Http\Controllers\API\FitnessTestAPIController::class, 'tableData']);
    Route::post('fitness_tests/progress_report', [App\Http\Controllers\API\FitnessTestAPIController::class, 'progressReport']);
    Route::resource('fitness_tests', App\Http\Controllers\API\FitnessTestAPIController::class);

    Route::resource('default_targets', App\Http\Controllers\API\DefaultTargetAPIController::class);


    Route::resource('ypi_goals', App\Http\Controllers\API\YpiGoalAPIController::class);


    Route::resource('ypi_events', App\Http\Controllers\API\YpiEventAPIController::class);


    Route::post('setting_presences/batchStore', [App\Http\Controllers\API\SettingPresenceAPIController::class, 'batchStore']);
    Route::resource('setting_presences', App\Http\Controllers\API\SettingPresenceAPIController::class);


    Route::resource('season_planners', App\Http\Controllers\API\SeasonPlannerAPIController::class);


    Route::resource('season_planner_details', App\Http\Controllers\API\SeasonPlannerDetailAPIController::class);

    Route::post('season_planner_detail_values/valuesPerWeek', [App\Http\Controllers\API\SeasonPlannerDetailValueAPIController::class, 'valuesPerWeek']);
    Route::post('season_planner_detail_values/valuePerWeek', [App\Http\Controllers\API\SeasonPlannerDetailValueAPIController::class, 'valuePerWeek']);
    Route::resource('season_planner_detail_values', App\Http\Controllers\API\SeasonPlannerDetailValueAPIController::class);


    Route::resource('detail_weekly_plans', App\Http\Controllers\API\DetailWeeklyPlanAPIController::class);


    Route::resource('detail_gaps', App\Http\Controllers\API\DetailGapsAPIController::class);


    Route::resource('detail_kpi_physics', App\Http\Controllers\API\DetailKpiPhysicAPIController::class);


    Route::resource('kpi', App\Http\Controllers\API\DetailPbKpiAPIController::class);

    Route::post('detail_competition_reports/batchUpdate', [App\Http\Controllers\API\DetailCompetitionReportAPIController::class, 'batchUpdate']);
    Route::resource('detail_competition_reports', App\Http\Controllers\API\DetailCompetitionReportAPIController::class);


    Route::resource('strength_condition_players', App\Http\Controllers\API\StrengthConditionPlayerAPIController::class);


    Route::resource('strength_condition_values', App\Http\Controllers\API\StrengthConditionValueAPIController::class);


    Route::resource('height_datas', App\Http\Controllers\API\HeightDataAPIController::class);


    Route::resource('puberty_datas', App\Http\Controllers\API\PubertyDataAPIController::class);

    Route::post('annual_plans/condition_delete', [App\Http\Controllers\API\AnnualPlanAPIController::class, 'conditionDestroy']);
    Route::get('annual_plans/seasonPlanner', [App\Http\Controllers\API\AnnualPlanAPIController::class, 'seasonPlanner']);
    Route::resource('annual_plans', App\Http\Controllers\API\AnnualPlanAPIController::class);

    Route::post('/logout', [AuthAPIController::class, 'logout']);

    Route::resource('ytp_charts', App\Http\Controllers\API\YtpChartsAPIController::class);

    Route::resource('header_kpis', App\Http\Controllers\API\HeaderKpiAPIController::class);

    Route::resource('competition_groups', App\Http\Controllers\API\CompetitionGroupAPIController::class);


    Route::resource('kpi_indicators', App\Http\Controllers\API\KpiIndicatorAPIController::class);


    Route::resource('kpi_subindicators', App\Http\Controllers\API\KpiSubindicatorAPIController::class);


    Route::resource('kpi_values', App\Http\Controllers\API\KpiValueAPIController::class);

    Route::get('kpis/{message}', [App\Http\Controllers\API\KpiAPIController::class, 'testing']);
    Route::resource('kpis', App\Http\Controllers\API\KpiAPIController::class);


    Route::resource('season_plans', App\Http\Controllers\API\SeasonPlanAPIController::class);


    Route::resource('season_plan_details', App\Http\Controllers\API\SeasonPlanDetailAPIController::class);


    Route::resource('season_plan_values', App\Http\Controllers\API\SeasonPlanValueAPIController::class);


    Route::get('constants/help/{name}', [App\Http\Controllers\API\ConstantAPIController::class, 'showHelp']);
    Route::post('constants/help/{name}', [App\Http\Controllers\API\ConstantAPIController::class, 'storeHelp']);
    Route::get('constants/parameter/{module}/{id}', [App\Http\Controllers\API\ConstantAPIController::class, 'findParameter']);
    Route::delete('constants/parameter/{module}/{id}', [App\Http\Controllers\API\ConstantAPIController::class, 'destroyParameter']);
    Route::get('constants/parameter/{module}', [App\Http\Controllers\API\ConstantAPIController::class, 'indexParameter']);
    Route::post('constants/parameter/{module}', [App\Http\Controllers\API\ConstantAPIController::class, 'storeParameter']);
    Route::get('constants/default_season_plans', [App\Http\Controllers\API\ConstantAPIController::class, 'defaultSeasonPlans']);
    Route::resource('constants', App\Http\Controllers\API\ConstantAPIController::class);


    Route::resource('kpi_parameters', App\Http\Controllers\API\KpiParameterAPIController::class);


    Route::get('kpi_results/fitness_test/report_summary', [App\Http\Controllers\API\KpiResultAPIController::class, 'fitnessTestReportSummary']);
    Route::get('kpi_results/fitness_test/report_detail/{date}/{group_id}', [App\Http\Controllers\API\KpiResultAPIController::class, 'fitnessTestReportDetail']);
    Route::get('kpi_results/fitness_test/{date}/{group_id}', [App\Http\Controllers\API\KpiResultAPIController::class, 'fitnessTestIndex']);
    Route::post('kpi_results/compare', [App\Http\Controllers\API\KpiResultAPIController::class, 'compare']);
    Route::post('kpi_results/batch', [App\Http\Controllers\API\KpiResultAPIController::class, 'batchStore']);
    Route::get('kpi_results/batch/{date}/{group_id}', [App\Http\Controllers\API\KpiResultAPIController::class, 'batchShow']);
    Route::get('kpi_results/dates', [App\Http\Controllers\API\KpiResultAPIController::class, 'dates']);
    Route::delete('kpi_results/batch/{date}/{player_id}', [App\Http\Controllers\API\KpiResultAPIController::class, 'batchDestroy']);
    Route::resource('kpi_results', App\Http\Controllers\API\KpiResultAPIController::class);

    Route::resource('gap_analysist', App\Http\Controllers\API\GapAnalysistAPIController::class);
});

Route::post('report_player/global', [App\Http\Controllers\API\ReportPlayerAPIController::class, 'global']);
Route::post('report_player/specialist', [App\Http\Controllers\API\ReportPlayerAPIController::class, 'specialist']);
Route::post('report_player/age', [App\Http\Controllers\API\ReportPlayerAPIController::class, 'age']);

Route::post('report_competition/global', [App\Http\Controllers\API\ReportCompetitionAPIController::class, 'global']);
Route::post('report_competition/group-by-category', [App\Http\Controllers\API\ReportCompetitionAPIController::class, 'group_by_category']);

Route::post('report_medical_record', [App\Http\Controllers\API\ReportMedicalRecordAPIontroller::class, 'global']);
