const fs = require("fs");
const configFile = "./config/oneapi.json";
const constantFile = './src/utils/constant.ts';
const dev = 'ypi2.qbase.id';
const prod = 'ypi.bypro.id';
const host = process.argv[2] === 'prod' ? prod : dev;

// 👇️ if using ES6 imports uncomment next line
// import {readFile, writeFile, writeFileSync, promises as fsPromises} from 'fs';
const { readFile, writeFile, promises: fsPromises } = require('fs');

async function replaceInFile(filename, pattern, replacement) {
    try {
        const contents = await fsPromises.readFile(filename, 'utf-8');
        const replaced = contents.replace(pattern, replacement);

        await fsPromises.writeFile(filename, replaced);
    } catch (err) {
        console.log(err);
    }
}


function jsonReader(filePath, cb) {
    fs.readFile(filePath, (err, fileData) => {
        if (err) {
            return cb && cb(err);
        }
        try {
            const object = JSON.parse(fileData);
            return cb && cb(null, object);
        } catch (err) {
            return cb && cb(err);
        }
    });
}

// jsonReader(configFile, (err, config) => {
// if (err) {
// console.log(err);
// return;
// }
// config.servers[0].url = "http://ypi2.qbase.id/";
// config.servers[1].url = "https://ypi2.qbase.id/";

// fs.writeFile(configFile, JSON.stringify(config, null, 2), err => {
// if (err) console.log("Error writing file:", err);
// });
// });
async function replaceUrls() {
    console.log('Change URL from local to ', host);
    await replaceInFile(constantFile, 'http://127.0.0.1:8000', 'https://' + host);
    await replaceInFile(configFile, 'http://localhost:8000', 'http://' + host);
    await replaceInFile(configFile, 'https://localhost:8000', 'https://' + host);
};

replaceUrls();