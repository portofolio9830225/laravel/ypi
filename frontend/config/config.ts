// https://umijs.org/config/
import { defineConfig } from 'umi';
import { join } from 'path';
import defaultSettings from './defaultSettings';
import proxy from './proxy';

const { REACT_APP_ENV } = process.env;

export default defineConfig({
  hash: true,
  antd: {},
  dva: {
    hmr: true,
  },
  layout: {
    // https://umijs.org/zh-CN/plugins/plugin-layout
    locale: true,
    siderWidth: 208,
    collapsed: true,
    ...defaultSettings,
  },
  // https://umijs.org/zh-CN/plugins/plugin-locale
  locale: {
    // default zh-CN
    default: 'en-US',
    antd: true,
    // default true, when it is true, will use `navigator.language` overwrite default
    baseNavigator: true,
  },
  dynamicImport: {
    loading: '@ant-design/pro-layout/es/PageLoading',
  },
  targets: {
    ie: 11,
  },
  // umi routes: https://umijs.org/docs/routing
  routes: [
    {
      path: '/file',
      routes: [
        {
          path: '/file/competition-info',
          routes: [
            {
              path: '/file/competition-info/competition-info',
              component: './file/competition-info/competition-info',
            },
            {
              path: '/file/competition-info/competition-level',
              component: './file/competition-info/competition-level',
            },
          ],
        },
        {
          path: '/file/group-classification',
          component: './file/group-classification',
        },
        {
          path: '/file/phonebook',
          component: './file/phonebook',
        },
        {
          path: '/file/coach-data',
          component: './file/coach-data',
        },
        {
          path: '/file/player-data',
          component: './file/player-data',
        },
        {
          path: '/file/group-player-by-coach',
          component: './file/group-player-by-coach',
        },
        {
          path: '/file/competition-detail',
          routes: [
            {
              path: '/file/competition-detail/competition-status',
              component: './file/competition-detail/competition-status',
            },
            {
              path: '/file/competition-detail/competition-detail',
              component: './file/competition-detail/competition-detail',
            },
          ],
        },
      ],
    },
    {
      path: '/setting',
      routes: [
        {
          path: '/setting/sc-joint-action',
          component: './setting/sc-joint-action',
        },
        {
          path: '/setting/sc-muscle-group',
          component: './setting/sc-muscle-group',
        },
        {
          path: '/setting/sub-sc-setting',
          component: './setting/sub-sc-setting',
        },
        {
          path: '/setting/national-height',
          component: './setting/national-height',
        },
        {
          path: '/setting/group-activity',
          component: './setting/group-activity',
        },
        {
          path: '/setting/coach-agenda-setting',
          component: './setting/coach-agenda-setting',
        },
        {
          path: '/setting/attendance-setting',
          component: './setting/attendance-setting',
        },
      ],
    },
    {
      path: '/individual',
      routes: [
        {
          path: '/individual/competition-report',
          routes: [
            {
              path: '/individual/competition-report/result-category',
              component: './individual/competition-report/result-category',
            },
            {
              path: '/individual/competition-report/competition-report',
              component: './individual/competition-report/competition-report',
            },
          ],
        },
        {
          path: '/individual/medical-record',
          component: './individual/medical-record',
        },
        {
          path: '/individual/attendance',
          component: './individual/attendance',
        },
        {
          path: '/individual/kpi',
          component: './individual/kpi-2',
        },
        {
          path: '/individual/kpi-setup',
          component: './individual/kpi-setup',
          access: 'canEditClub',
        },
        {
          path: '/individual/kpi-monitoring',
          component: './individual/kpi-monitoring',
        },
      ],
    },
    {
      path: '/data-management',
      routes: [
        {
          path: '/data-management/strength-conditioning',
          component: './data-management/strength-conditioning',
        },
        {
          path: '/data-management/strength-conditioning-data',
          component: './data-management/strength-conditioning-data',
        },
        {
          path: '/data-management/maturational-status',
          component: './data-management/maturational-status',
        },
        {
          path: '/data-management/fitness-monitor-tool',
          component: './data-management/fitness-monitor-tool',
        },
        {
          path: '/data-management/fitness-test',
          component: './data-management/fitness-test',
        },
      ],
    },
    {
      path: '/master-plan',
      routes: [
        {
          path: '/master-plan/coach-agenda',
          component: './master-plan/coach-agenda',
        },
        {
          path: '/master-plan/ypi',
          component: './master-plan/ypi',
        },
        {
          path: '/master-plan/ytp',
          component: './master-plan/ytp',
        },
        {
          path: '/master-plan/wtp',
          component: './master-plan/wtp',
        },
      ],
    },
    {
      path: '/report',
      routes: [
        {
          path: '/report/data-player',
          component: './report/data-player',
        },
        {
          path: '/report/tournament-detail',
          component: './report/tournament-detail',
        },
        {
          path: '/report/kpi-comparison',
          component: './report/kpi-comparison',
        },
        {
          path: '/report/medical-record',
          component: './report/medical-record',
        },
        {
          path: '/report/attendance',
          component: './report/attendance',
        },
        {
          path: '/report/vo2max-ca',
          component: './report/vo2max-ca',
        },
        {
          path: '/report/benchpress-squat',
          component: './report/benchpress-squat',
        },
        {
          path: '/report/fitness-test',
          component: './report/fitness-test',
        },
        {
          path: '/report/progress-fitness',
          component: './report/progress-fitness',
        },
        {
          path: '/report/fitness-tool',
          component: './report/fitness-tool',
        },
      ],
    },
    {
      path: '/system',
      routes: [
        {
          path: '/system/edit-profile',
          component: './system/edit-profile',
        },
        {
          path: '/system/club-profile',
          component: './system/club-profile',
          access: 'canEditClub',
        },
        {
          path: '/system/users-management',
          component: './system/users-management',
          access: 'canAdmin',
        },
        {
          path: '/system/clubs-management',
          component: './system/clubs-management',
          access: 'canAdmin',
        },
      ],
    },
    {
      path: '/menu',
      component: './menu',
      layout: false,
    },
    {
      path: '/user',
      layout: false,
      routes: [
        {
          path: '/user/login',
          layout: false,
          component: './user/Login',
        },
        {
          path: '/user',
          redirect: '/user/login',
        },
        {
          path: '/user/register-result',
          component: './user/register-result',
        },
        {
          path: '/user/register',
          component: './user/register',
        },
        {
          component: '404',
        },
      ],
    },
    {
      path: '/',
      redirect: '/menu',
    },
    {
      component: '404',
    },
  ],
  // Theme for antd: https://ant.design/docs/react/customize-theme-cn
  theme: {
    'primary-color': defaultSettings.primaryColor,
  },
  // esbuild is father build tools
  // https://umijs.org/plugins/plugin-esbuild
  esbuild: {},
  title: false,
  ignoreMomentLocale: true,
  proxy: proxy[REACT_APP_ENV || 'dev'],
  manifest: {
    basePath: '/',
  },
  // Fast Refresh 热更新
  fastRefresh: {},
  openAPI: [],
  nodeModulesTransform: {
    type: 'none',
  },
  // mfsu: {},
  webpack5: {},
  exportStatic: {},
});
