import { Settings as LayoutSettings } from '@ant-design/pro-layout';

const Settings: LayoutSettings & {
  pwa?: boolean;
  logo?: string;
  subTitle?: string;
} = {
  navTheme: 'light',
  // 拂晓蓝
  primaryColor: '#1890ff',
  layout: 'top',
  contentWidth: 'Fluid',
  fixedHeader: false,
  fixSiderbar: true,
  colorWeak: false,
  title: 'By Pro - YPI Online',
  pwa: false,
  logo: '/logo.png',
  subTitle: 'Semarang - Indonesia',
  iconfontUrl: '',
};

export default Settings;
