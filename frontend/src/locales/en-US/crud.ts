export default {
  'crud.action': 'Action',
  'crud.edit': 'Edit',
  'crud.toggle_edit': 'Toggle Edit',
  'crud.delete': 'Delete',
  'crud.save': 'Save',
  'crud.update': 'Update',
  'crud.cancel': 'Cancel',
  'crud.yes': 'Yes',
  'crud.no': 'No',
  'crud.next': 'Next',
  'crud.prev': 'Previous',
  'crud.addNewData': 'Add New Data',
  'crud.add_item': 'Add Item',
  'crud.editData': 'Edit Data',
  'crud.process': 'Process',
  'crud.reports': 'Reports',
  'crud.reset': 'Reset',
  'crud.print': 'Print',
  'crud.help': 'Help',
  'crud.copy_to_next_player': 'Copy to the next Player',
  'crud.generate_report': 'Generate Report',
};
