import { request } from '@/utils/request';

// request.use(async (ctx, next) => {
//   try {
//     console.log(ctx);
//     await next();

//     if (ctx.res.message === 'Unauthenticated' || ctx.res.message === 'Unauthenticated.') {
//       history.push('/user/login');
//     }
//   } catch (error) {
//     console.log('eee', error);
//   }
// });

export async function get(url: string, options?: { [key: string]: any }) {
  return request.get('/api/' + url, { ...(options || {}) });
}

export async function create(url: string, data: any, options?: { [key: string]: any }) {
  return request.post('/api/' + url, { data, ...(options || {}) });
}

export async function update(
  url: string,
  id: number | string,
  data: any,
  options?: { [key: string]: any },
) {
  return request.patch(`/api/${url}/${id}`, { data, ...(options || {}) });
}

export async function destroy(url: string, id?: number | string, options?: { [key: string]: any }) {
  if (id) return request.delete(`/api/${url}/${id}`, { ...(options || {}) });
  return request.delete(`/api/${url}`, { ...(options || {}) });
}

export async function login(data: any, options?: { [key: string]: any }) {
  return request.post('/api/login', { data, ...(options || {}) });
}

export async function logout(options?: { [key: string]: any }) {
  return request.post('/api/logout', { ...(options || {}) });
}
