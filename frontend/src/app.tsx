import type { Settings as LayoutSettings } from '@ant-design/pro-layout';
import { PageLoading, BasicLayoutProps } from '@ant-design/pro-layout';
import type { RunTimeLayoutConfig } from 'umi';
import { history } from 'umi';
import RightContent from '@/components/RightContent';
import headerLayout from '@/components/HeaderLayout';
import type { Club, User } from '@/types/ypi';

const loginPath = '/user/login';

/** 获取用户信息比较慢的时候会展示一个 loading */
export const initialStateConfig = {
  loading: <PageLoading />,
};

/**
 * @see  https://umijs.org/zh-CN/plugins/plugin-initial-state
 * */
export async function getInitialState(): Promise<{
  settings?: Partial<LayoutSettings>;
  currentUser?: User;
  currentClub?: Club;
  fetchUserInfo?: () => User | undefined;
  fetchClubInfo?: () => Club | undefined;
}> {
  const fetchUserInfo = () => {
    let user = localStorage.getItem('user');
    if (user) {
      return JSON.parse(user) as User;
    }

    user = sessionStorage.getItem('user');
    if (user) {
      return JSON.parse(user) as User;
    }

    return undefined;
  };

  const fetchClubInfo = () => {
    let club = localStorage.getItem('club');
    if (club) {
      return JSON.parse(club) as Club;
    }

    return undefined;
  };

  if (history.location.pathname !== loginPath) {
    const currentUser = fetchUserInfo();
    const currentClub = fetchClubInfo();

    return {
      fetchUserInfo,
      currentUser,
      settings: {},
      currentClub,
    };
  }

  return {
    fetchUserInfo,
    settings: {},
  };
}

// ProLayout 支持的api https://procomponents.ant.design/components/layout
export const layout: RunTimeLayoutConfig = ({ initialState }) => {
  return {
    navTheme: 'dark',
    rightContentRender: () => <RightContent />,
    disableContentMargin: false,
    onPageChange: () => {
      const { location } = history;
      if (!initialState?.currentUser && location.pathname !== loginPath) {
        history.push(loginPath);
      }
    },
    links: [],
    menuHeaderRender: undefined,
    headerRender: headerLayout,
    ...initialState?.settings,
  };
};
