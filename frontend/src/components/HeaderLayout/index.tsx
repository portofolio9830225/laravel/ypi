import { Row, Col, Button, Space, Avatar } from 'antd';
import { useHistory, useIntl, useModel } from 'umi';
import { useEffect, useState } from 'react';
import {
  MENU_SYSTEM_COLOR,
  MENU_FILE_COLOR,
  MENU_MASTER_PLAN_COLOR,
  MENU_INDIVIDUAL_COLOR,
  MENU_DATA_MANAGEMENT_COLOR,
  MENU_REPORT_COLOR,
} from '@/utils/constant';
import { ArrowLeftOutlined } from '@ant-design/icons';
import RightContent from '@/components/RightContent';
import { Club } from '@/types/ypi';

const Header = () => {
  const intl = useIntl();
  const history = useHistory();
  const [bgColor, setBgColor] = useState<string>(MENU_SYSTEM_COLOR);
  const [pageTitle, setPageTitle] = useState<string>('');
  const [club, setClub] = useState<Club>();
  const { initialState } = useModel('@@initialState');

  const updateHeader = async (pathName: string) => {
    let processedPathName = pathName;
    if (pathName.slice(pathName.length - 1) === '/')
      processedPathName = pathName.substring(0, pathName.length - 1);

    if (processedPathName) {
      const formattedTitle = intl.formatMessage({
        id: 'menu' + processedPathName.replaceAll('/', '.'),
      });
      setPageTitle(formattedTitle);
    }

    switch (processedPathName) {
      case '/file/competition-info/competition-info':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/competition-info/competition-level':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/group-classification':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/phonebook':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/coach-data':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/player-data':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/group-player-by-coach':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/competition-detail/competition-status':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/file/competition-detail/competition-detail':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/setting/national-height':
        setBgColor(MENU_FILE_COLOR);
        break;
      case '/setting/national-height':
        setBgColor(MENU_FILE_COLOR);
        break;
      // ===========================================================
      case '/setting/group-activity':
        setBgColor(MENU_MASTER_PLAN_COLOR);
        break;
      case '/setting/coach-agenda-setting':
        setBgColor(MENU_MASTER_PLAN_COLOR);
        break;
      case '/master-plan/coach-agenda':
        setBgColor(MENU_MASTER_PLAN_COLOR);
        break;
      case '/master-plan/ypi':
        setBgColor(MENU_MASTER_PLAN_COLOR);
        break;
      case '/master-plan/ytp':
        setBgColor(MENU_MASTER_PLAN_COLOR);
        break;
      case '/master-plan/wtp':
        setBgColor(MENU_MASTER_PLAN_COLOR);
        break;
      // ===========================================================
      case '/individual/competition-report/result-category':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      case '/individual/competition-report/competition-report':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      case '/individual/medical-record':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      case '/individual/attendance':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      case '/setting/attendance-setting':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      case '/individual/kpi':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      case '/individual/kpi-setup':
        setBgColor(MENU_INDIVIDUAL_COLOR);
        break;
      // case '/individual/kpi-monitoring':
      //   setBgColor(MENU_INDIVIDUAL_COLOR);
      //   break;
      // ===========================================================
      case '/data-management/strength-conditioning':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/data-management/strength-conditioning-data':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/data-management/strength-conditioning-data':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/data-management/maturational-status':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/data-management/maturational-status':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/data-management/fitness-monitor-tool':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/data-management/fitness-test':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/setting/sc-joint-action':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/setting/sc-muscle-group':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/setting/sc-muscle-group':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/setting/sub-sc-setting':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      case '/setting/sub-sc-setting':
        setBgColor(MENU_DATA_MANAGEMENT_COLOR);
        break;
      // ===========================================================
      case '/report/data-player':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/coach-agenda':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/tournament-detail':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/kpi-comparison':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/medical-record':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/attendance':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/vo2max-ca':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/benchpress-squat':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/fitness-test':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/progress-fitness':
        setBgColor(MENU_REPORT_COLOR);
        break;
      case '/report/fitness-tool':
        setBgColor(MENU_REPORT_COLOR);
        break;
      // ===========================================================
      case '/system/edit-profile':
        setBgColor(MENU_SYSTEM_COLOR);
        break;
      case '/system/users-management':
        setBgColor(MENU_SYSTEM_COLOR);
        break;
      case '/system/clubs-management':
        setBgColor(MENU_SYSTEM_COLOR);
        break;

      default:
        setBgColor(MENU_SYSTEM_COLOR);
        break;
    }
  };

  useEffect(() => {
    updateHeader(location.pathname);
  }, [history]);

  useEffect(() => {
    if (initialState && initialState.currentClub) {
      setClub(initialState.currentClub);
    }
  }, [initialState]);

  useEffect(() => {
    const unlisten = history.listen((location, action) => {
      updateHeader(location.pathname);
    });

    return () => {
      unlisten();
    };
  }, []);

  return (
    <Row style={{ backgroundColor: bgColor }}>
      <Col span={8}>
        <Space>
          <Button
            type="text"
            onClick={() => {
              history.goBack();
            }}
          >
            <ArrowLeftOutlined style={{ color: '#ffffff', fontSize: '1rem' }} />
          </Button>
          <span style={{ color: '#ffffff', fontSize: '1rem' }}>{pageTitle}</span>
        </Space>
      </Col>
      <Col span={8} style={{ textAlign: 'center' }}>
        <Space>
          <Avatar src={club?.image} shape="square" />
          <Space
            direction="vertical"
            size={0}
            style={{ lineHeight: 'normal', display: 'flex' }}
            align="start"
          >
            <span style={{ color: '#ffffff', fontWeight: 'bold' }}>{club?.nama}</span>
            <span style={{ color: '#ffffff' }}>{club?.alamat}</span>
          </Space>
        </Space>
      </Col>
      <Col span={8}>
        <RightContent />
      </Col>
    </Row>
  );
};

const headerRender = () => {
  return <Header />;
};

export default headerRender;
