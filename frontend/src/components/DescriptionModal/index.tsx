import { Modal } from 'antd';
import { FormattedMessage } from 'umi';
import { useState, useEffect, FC } from 'react';
import '@wangeditor/editor/dist/css/style.css';
import { Editor, Toolbar } from '@wangeditor/editor-for-react';
import { IDomEditor, IEditorConfig, IToolbarConfig, i18nChangeLanguage } from '@wangeditor/editor';
i18nChangeLanguage('en');

interface Props {
  value: string | undefined;
  onOk: (value: string) => void;
  onCancel: () => void;
  visible: boolean;
}

const DescriptionModal: FC<Props> = ({ value, onCancel, onOk, visible }) => {
  const [editor, setEditor] = useState<IDomEditor | null>(null);
  const [html, setHtml] = useState('');

  // Timely destroy editor, important!
  useEffect(() => {
    return () => {
      if (editor == null) return;
      editor.destroy();
      setEditor(null);
    };
  }, [editor]);

  useEffect(() => {
    if (value !== undefined) {
      setHtml(value);
    } else if (value === '') {
      editor?.clear();
    } else {
      editor?.clear();
    }
    console.log(value);
  }, [value]);

  useEffect(() => {
    console.log(html);
  }, [html]);

  const handleOk = () => {
    onOk(html);
  };

  return (
    <Modal
      title={<FormattedMessage id="commonField.help" />}
      onOk={handleOk}
      visible={visible}
      onCancel={onCancel}
      centered={true}
      width="80%"
    >
      <div style={{ border: '1px solid #ccc', zIndex: 100 }}>
        <Toolbar
          editor={editor}
          // defaultConfig={toolbarConfig}
          mode="default"
          style={{ borderBottom: '1px solid #ccc' }}
        />
        <Editor
          // defaultConfig={editorConfig}
          value={html}
          onCreated={setEditor}
          onChange={(editor) => setHtml(editor.getHtml())}
          mode="default"
          style={{ height: '70%', overflowY: 'hidden' }}
        />
      </div>
      {/* <div style={{ marginTop: '15px' }}>{html}</div> */}
    </Modal>
  );
};

export default DescriptionModal;
