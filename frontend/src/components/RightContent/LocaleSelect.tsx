import React from 'react';
import { Button } from 'antd';
import { setLocale, getLocale } from 'umi';


const LocaleSelect: React.FC = () => {
    const changeLang = () => {
      const locale = getLocale();
  
      if (!locale || locale === 'id-ID') {
        setLocale('en-US', true);
      } else {
        setLocale('id-ID', true);
      }
    }
  
    const buttonLabel = getLocale() == 'id-ID' ? "🇺🇸 En" : "🇮🇩 Id" ;
  
    return (
      <Button
        ghost
        size="small"
        style={{
          borderRadius: '14px',
          margin: '0 8px',
          color: 'rgb(156, 147, 132)',
        }}
        onClick={() => { changeLang();}}
      >
        {buttonLabel}
      </Button>
    );
  }

export default LocaleSelect;