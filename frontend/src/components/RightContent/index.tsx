import { Space } from 'antd';
import React from 'react';
import { useModel } from 'umi';
import Avatar from './AvatarDropdown';
import LocaleSelect from './LocaleSelect';
import styles from './index.less';
import NoticeIconView from '../NoticeIcon';

export type SiderTheme = 'light' | 'dark';

const GlobalHeaderRight: React.FC = () => {
  const { initialState } = useModel('@@initialState');

  if (!initialState || !initialState.settings) {
    return null;
  }

  const { navTheme, layout } = initialState.settings;
  let className = styles.right;

  // if ((navTheme === 'dark' && layout === 'top') || layout === 'mix') {
  //   className = `${styles.right}  ${styles.dark}`;
  // }
  if ((navTheme === 'light' && layout === 'top') || layout === 'mix') {
    className = `${styles.right}  ${styles.light}`;
  }

  return (
    <Space className={className}>
      {/* <NoticeIconView /> */}
      <Avatar menu />
      <LocaleSelect />
    </Space>
  );
};

export default GlobalHeaderRight;
