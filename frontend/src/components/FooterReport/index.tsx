import { Divider, Space } from 'antd';

import { useModel } from 'umi';

export const FooterReport = () => {
  const { initialState } = useModel('@@initialState');
  const club = initialState?.currentClub;

  return (
    <>
      {club && (
        <>
          <Divider />
          <Space>
            <img style={{ width: '2rem' }} src={club.image} />
            <Space
              direction="vertical"
              size={0}
              style={{ lineHeight: 'normal', display: 'flex', fontSize: '11px' }}
              align="start"
            >
              <span style={{ fontWeight: 'bold', fontSize: '11px' }}>{club?.nama}</span>
              <span>{club?.alamat}</span>
            </Space>
          </Space>
        </>
      )}
    </>
  );
};

export default FooterReport;
