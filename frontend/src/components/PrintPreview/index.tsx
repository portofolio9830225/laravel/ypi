import { FC, PropsWithChildren } from 'react';
import { Space } from 'antd';

interface PageProps {
  
}
 
export const Page: FC<PageProps> = () => {
  return (  );
}
 
export const PrintPreview = ({ children, ...props }: PropsWithChildren<{}>) => {
  return <Space direction="vertical">{children}</Space>;
};
