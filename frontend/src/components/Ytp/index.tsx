import { Table, TableColumnsType, TableColumnType } from 'antd';
import { FC } from 'react';
// import { Context, getLocaleKey } from '../index';
import { useIntl } from 'umi';
import { YtpRecordType, CompetitionDetail } from '@/types/ypi';
import styles from './ytp.less';

const CustomRow: FC = ({ ...props }) => {
  return <tr {...props} />;
};
interface CustomCellProps {
  record: YtpRecordType;
  children: React.ReactNode;
  eventData?: Partial<CompetitionDetail>[];
  week: number;
  onCellClick: (startWeek: number, endWeek?: number, rowId?: number, content?: string) => void;
  col_span: number;
}

const CustomCell: React.FC<CustomCellProps> = ({
  record,
  children,
  week,
  eventData,
  onCellClick,
  col_span,
  ...props
}) => {
  if (record && record.isEventData) {
    const event = eventData?.find((d) => d.nama_event == record['week_' + week]);

    return (
      <td {...props} style={{ background: event?.color }}>
        {children}
      </td>
    );
  }

  if (record && record.isEditable) {
    const startWeek = col_span ? week - col_span + 1 : week;
    return (
      <td
        {...props}
        onClick={() =>
          onCellClick(startWeek, week, record.seasonPlanDetailId, record['week_' + week])
        }
      >
        {children}
      </td>
    );
  }

  return <td {...props}>{children}</td>;
};

interface Props {
  mergeCells?: boolean;
  onCellClick: (startWeek: number, endWeek?: number, rowId?: number, content?: string) => void;
  tableData: Partial<YtpRecordType>[];
  eventData: Partial<CompetitionDetail>[] | undefined;
  ytpChart?: (data: readonly Partial<YtpRecordType>[]) => React.ReactNode;
}

const Component: FC<Props> = ({
  mergeCells = true,
  onCellClick,
  tableData: data,
  eventData,
  ytpChart,
}) => {
  const intl = useIntl();

  let weekColumns = [];
  let value: string[] = [];
  let index: number = 0;
  for (let i = 1; i <= 53; i++) {
    weekColumns.push({
      colSpan: 0,
      rowSpan: 0,
      dataIndex: 'week_' + i,
      onCell: (val: YtpRecordType, indexRow: number) => {
        const currentValue = val['week_' + i];
        const nextValue = val['week_' + (i + 1)];
        let props = {};
        let style = {};

        if (mergeCells) {
          if (currentValue !== '') {
            if (index !== indexRow) {
              index = indexRow;
              value = [];
            }

            value.push(currentValue);
            if (currentValue == nextValue) {
              props = { colSpan: 0, col_span: 0 };
            } else {
              props = { colSpan: value.length, col_span: value.length };
              value = [];
            }
          } else {
            props = { ...props, col_span: 0 };
          }
        }

        if (val.isEventData) props = { ...props, eventData };
        if (val.isEditable) {
          style = { cursor: 'pointer' };
          props = {
            ...props,
            onCellClick,
          };
        }
        if (val.isEditable || val.isEventData) props = { ...props, record: val };

        style = { ...style, textAlign: 'center' };
        return {
          ...props,
          style,
          week: i,
        };
      },
    });
  }

  const columns: TableColumnsType<Partial<YtpRecordType>> = [
    {
      title: intl.formatMessage({ id: 'pages.ytp.season_planner' }),
      dataIndex: 'seasonPlanner',
      fixed: 'left',
      colSpan: 0,
      onCell: (val, index) => {
        let props = {};
        if (index != undefined) {
          let prevValue = 'undefined';
          if (data[index - 1]) {
            prevValue = data[index - 1].seasonPlanner as string;
          }
          const currentValue = data[index].seasonPlanner;

          let rowSpan = 1;
          if (currentValue != prevValue) {
            for (let i = index; i < data.length; i++) {
              let nextValue = undefined;
              if (data[i + 1]) nextValue = data[i + 1].seasonPlanner;
              if (data[i].seasonPlanner != nextValue) {
                break;
              } else {
                rowSpan++;
              }
            }
          }
          if (currentValue == prevValue) {
            rowSpan = 0;
          }

          props = { rowSpan };
        }
        if (index === 0) {
          props = { rowSpan: 6 };
        } else if ((index as number) < 6) {
          props = { rowSpan: 0 };
        }
        if (val.isEditable) props = { ...props, style: { background: val.color } };
        return props;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.ytp.season_planner_detail' }),
      dataIndex: 'seasonPlannerDetail',
      colSpan: 0,
      fixed: 'left',
      ellipsis: true,
      onCell: (record: Partial<YtpRecordType>) => {
        return { style: { background: record.color } };
      },
    },
    ...(weekColumns as TableColumnType<Partial<YtpRecordType>>[]),
  ];

  const components = {
    body: {
      row: CustomRow,
      cell: CustomCell,
    },
  };

  return (
    <Table
      onRow={(record: Partial<YtpRecordType>, rowIndex) => {
        return { style: { background: record.color } };
      }}
      bordered
      size="small"
      columns={columns}
      components={components}
      scroll={{ x: true }}
      dataSource={data}
      pagination={false}
      summary={ytpChart}
      rowClassName={styles.cell}
    />
  );
};

export default Component;
