import { useState, useCallback } from 'react';
import { User } from '@/types/ypi';

export default () => {
  const [user, setUser] = useState<User>();
  const setCurrentUser = useCallback(
    () =>
      setUser({
        id: 1,
        name: 'admin',
        email: 'admin@gmail.com',
        email_verified_at: 'test',
        level: 'superadmin',
      }),
    [],
  );
  return { user, setCurrentUser };
};
