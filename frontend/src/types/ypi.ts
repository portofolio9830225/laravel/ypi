import { Moment } from 'moment';

export interface Player {
  key: string;
  id: number;
  nomor_ic: string;
  nama_lengkap: string;
  alamat: string;
  negara: string;
  kodepos: number;
  tempat_lahir: string;
  tanggal_lahir: string;
  home_phone: string;
  hand_phone: string;
  business_phone: string;
  email: string;
  golongan_darah: string;
  keterangan: string;
  nama_sekolah: string;
  kelas: string;
  wali_kelas: string;
  no_ruang: string;
  contact_id: number;
  aktif: boolean;
  sex: string;
  kelompok_id: number;
  tunggal_putra: boolean;
  tunggal_putri: boolean;
  ganda_putra: boolean;
  ganda_putri: boolean;
  ganda_campuran: boolean;
  //
  age: number;
  group: GroupClassification;
}

export interface ResultCategory {
  key: string;
  id: number;
  result: string;
}

export interface CompetitionReport {
  key: string;
  id: number;
  pemain_id: number;
  master_achievement_id: number;
  venue: string;
  specialist: string;
  event_level: string;
  result_id: number;
  summary_notes: string;
  suggestion_notes: string;
  kelompok_id: number;
  partner_id: number;
}

export interface DetailCompetitionReport {
  key: string;
  id: number;
  competition_report_id: number;
  round: string;
  opponent: string;
  nationality: string;
  set1a: number;
  set1b: number;
  set2a: number;
  set2b: number;
  set3a: number;
  set3b: number;
  comment: string;
  result: string;
}

export interface CompetitionGroup {
  id: number;
  competition_id: number;
  groups_id?: string;
}

export interface CompetitionLevel {
  id: number;
  level: number;
}

export interface CompetitionInfo {
  id: number;
  nama_kategori: string;
  level_kategori_id: number;
}

export interface CompetitionStatus {
  id: number;
  status: string;
}

export interface CompetitionDetail {
  key: string;
  id: number;
  nama_event: string;
  kategori_id: number;
  tanggal_mulai: string;
  tanggal_akhir: string;
  competition_status_id: number;
  color: string;
  // deps
  groups_id: number[];
  competition_group: CompetitionGroup;
  group_classifications: string;
  date: [Moment, Moment];
}

export interface GroupClassification {
  key: string;
  id: number;
  age_min: number;
  age_max: number;
  kelompok: string;
}

export interface MedicalRecord {
  id: number;
  pemain_id: number;
  master_ypi_id: number;
  week: number;
  tanggal: string;
  hari: string;
  injured: boolean;
  primary_injured: string;
  detail_injured: string;
  treatment_injured: boolean;
  injured_days: number;
  treatment_injured_days: number;
  illied: boolean;
  primary_illied: string;
  symptomps: string;
  treatment_illied: boolean;
  illied_days: number;
  treatment_illied_days: number;
  no?: number;
  tanggal_moment?: Moment;
}

export interface YpiDefaultTarget {
  key?: string;
  session?: string;
  id: number;
  master_ypi_id: number;
  mon1: number;
  mon2: number;
  mon3: number;
  mon4: number;
  tue1: number;
  tue2: number;
  tue3: number;
  tue4: number;
  wed1: number;
  wed2: number;
  wed3: number;
  wed4: number;
  thu1: number;
  thu2: number;
  thu3: number;
  thu4: number;
  fri1: number;
  fri2: number;
  fri3: number;
  fri4: number;
  sat1: number;
  sat2: number;
  sat3: number;
  sat4: number;
  sun1: number;
  sun2: number;
  sun3: number;
  sun4: number;
}

export interface YpiSeasonPlannerDetailValue {
  key: string;
  id: number;
  master_ypi_id: number;
  season_planner_id: number;
  season_planner_detail_id: number;
  week_index: number;
  value: number | string;
  text_color: string;
  description: string;
}

export interface YpiSeasonPlannerDetail {
  key: string;
  id: number;
  master_ypi_id: number;
  season_planner_id: number;
  name: string;
  // description: string;
  type: 'text' | 'number';
  inChart: boolean;
  chartType: 'line' | 'bar' | 'scatter';
  chartColor: string;
  ytp_charts_id: number;
  desc_en: string;
  desc_id: string;
}

export interface YpiSeasonPlanner {
  key: string;
  id: number;
  master_ypi_id: number;
  background_color: string;
  name: string;
  description: string;
  season_planner_details?: Partial<YpiSeasonPlannerDetail>[];
  // frontend needs
  active?: boolean;
}

export interface YpiEvent {
  id: number;
  master_ypi_id: number;
  master_achievement_id: number;
  active: boolean;
  // competition_details?: CompetitionDetail;
}

export interface YpiGoal {
  key: string;
  id: number;
  master_ypi_id: number;
  goal: string;
}

export interface YpiEventCompetitionDetail extends CompetitionDetail {
  checked?: boolean;
}

export interface Ypi {
  id: number | undefined;
  keterangan: string;
  kode_pelatih: number;
  voltarget: number;
  tanggal_mulai: string;
  tanggal_selesai: string;
  default_intensity: number;
}

export interface YtpChart {
  key: string;
  id: number;
  name: string;
}

export interface YtpRecordType {
  key: string;
  color: string;
  seasonPlanner: string;
  seasonPlannerDetail: string;
  isEventData: boolean;
  isDateData: boolean;
  seasonPlanId: number;
  seasonPlanDetailId: number;
  isPeriodization: boolean;
  isEditable: boolean;
  inputType: 'text' | 'number';
}

export interface Attendance {
  key: string;
  id: number;
  kelompok_id: number;
  master_pelatih_id: number;
  pemain_id: number;
  tanggal: string;
  session: number;
  presence_id: number;
  note: string;
  tanggal_moment?: Moment;
}

export interface AttendanceStatus {
  id: number;
  kelompok_id: number;
  pelatih_id: number;
  keterangan: string;
  inisial: string;
  presence: boolean;
  deleted: boolean;
}

export interface AttendanceSetting {
  key: string;
  id: number;
  bulan: number;
  tahun: number;
  kelompok: number;
  kode_pelatih: number;
  tanggal: string;
  session: number;
  am_pm: number;
  am_pm_string?: string;
  deleted?: boolean;
}

export interface Coach {
  key: string;
  id: number;
  nomor_ic: string;
  nama_lengkap: string;
  alamat: string;
  negara: string;
  kodepos: number;
  tempat_lahir: string;
  tanggal_lahir: Date;
  home_phone: string;
  hand_phone: string;
  business_phone: string;
  email: string;
  golongan_darah: string;
  keterangan: string;
}

export interface StrengthConditioning {
  key: string;
  id: number;
  date_test: string;
  gender: string;
  age_group_id: number;
  date_test_moment?: Moment;
}

export interface StrengthConditionPlayer {
  key: string;
  id: number;
  strength_conditioning_id: number;
  pemain_id: number;
  body_weight: number;
  player_name?: string;
}

export interface StrengthConditionValue {
  key: string;
  id: number;
  strength_condition_id: number;
  strength_condition_player_id: number;
  name: number;
  reps: number;
  weight: number;
  predict: number;
  min: number;
  max: number;
  strength: number;
  last: number;
  diff: number;
}

export interface SubScSetting {
  key: string;
  id: number;
  excercise_desc: string;
  gym_type_id: number;
  tut_per_rep: number;
  exercise_category_id: number;
}

export interface StrengthConditioningData {
  key: string;
  id: number;
  pemain_id: number;
  gym_type: number;
  master_ypi_id: number;
  week: number;
  hari: string;
  tanggal: string;
  comments: string;
  rep_1: number;
  weight_1: number;
  rep_2: number;
  weight_2: number;
  rep_3: number;
  weight_3: number;
  rep_4: number;
  weight_4: number;
  rep_5: number;
  weight_5: number;
  rep_6: number;
  weight_6: number;
  tut_rep: number;
  tut_exercise: number;
  tonnage: number;
  average: number;
  physical_load: number;
  predicted_1rm: number; //rest interval
  average_1rm_predict: number; //rest period
  actual_1rm: number;
  average_1rm: number;
  weekly_tut: number;
  weekly_tonnage: number;
  weekly_average: number;
  weekly_load: number;
  ypi_start?: string;
  ypi_end?: string;
}

export interface RmCalculation {
  week: number;
  weight: number;
  reps: number;
  mean: number;
  range_min: number;
  range_max: number;
  c1: number;
  c2: number;
  c3: number;
  c4: number;
  c5: number;
  rm_calculated: number;
}

export interface ScJointAction {
  key: string;
  id: number;
  gym_description: string;
}

export interface FitnessTest {
  id: number;
  tanggal_test: string;
  gender: string;
  kelompok_id: number;
  pemain_id: number;
  court_agility: number;
  vo2max: number;
  bench_press: number;
  squat: number;
}

export interface FitnessMonitorTool {
  key: string;
  id: number;
  pemain_id: number;
  kelompok_id: number;
  gender: string;
  tanggal: string;
  beep_test: number;
  court_agility: number;
  bench_press: number;
  squat: number;
  reach: number;
  cj_actual: number;
  cj_elevation: number;
  sq_actual: number;
  sq_elevation: number;
  rl_actual: number;
  rl_elevation: number;
  ll_actual: number;
  ll_elevation: number;
  ratio: number;
  difference: number;
  tanggal_moment?: Moment;
  player_name?: string;
  no?: number;
  beep_test_best?: number;
  court_agility_best?: number;
  cj_best?: number;
  sq_best?: number;
  rl_best?: number;
  ll_best?: number;
}

export interface HeightPredicted {
  key: string;
  id: number;
  pemain_id: number;
  age: number;
  date_predicted: string;
  height: number;
  weight: number;
  fathers_height: number;
  mothers_height: number;
  predicted_height: number;
  maturity_status: number;
  dob?: string;
  gender?: string;
  height_data?: HeightData;
  puberty_data?: PubertyData;
  player_data?: Player;
}

export interface HeightData {
  key: string;
  id: number;
  height_predicted_id: number;
  pemain_id: number;
  dob: string;
  age: number;
  bulan_awal: number;
  tahun_awal: number;
  bulan_akhir: number;
  tahun_akhir: number;
  bulan_1: number;
  bulan_2: number;
  bulan_3: number;
  bulan_4: number;
  bulan_5: number;
  bulan_6: number;
  bulan_7: number;
  bulan_8: number;
  bulan_9: number;
  bulan_10: number;
  bulan_11: number;
  bulan_12: number;
  growth_rate: number;
}

export interface PubertyData {
  key: string;
  id: number;
  height_predicted_id: number;
  pemain_id: number;
  dob: string;
  age: number;
  height: number;
  national_average: number;
  growth_rate: number;
  biological_category: number;
  biological_category_text: string;
  nation_id: number;
  bulan_awal: number;
  tahun_awal: number;
}

export interface NationalHeightAvg {
  key: string;
  id: number;
  nation: string;
  age: number;
  height: number;
  gender: string;
  nation_id?: number;
}

export interface User {
  id: number;
  name: string;
  email: string;
  email_verified_at: string;
  role: 'superadmin' | 'club_admin' | 'coach';
  foto: string;
  password?: string;
  active_club_id: number;
  as_coach_id: number | null;
}

export interface Club {
  id: string;
  nama: string;
  alamat: string;
  image: string;
  subscription_type: string;
}

export interface CoachAgenda {
  id: number;
  master_ypi_id: number;
  kelompok: number;
  annual_plan_setting_id: number;
  week: number;
  plan_description: string;
  color: string;
}

export interface GroupActivity {
  key: string;
  id: number;
  group: string;
}

export interface CoachAgendaSetting {
  id: number;
  master_ypi_id: number;
  kelompok: number;
  description: string;
  color: string;
  group_activity?: GroupActivity;
}

export interface DateSeasonPlanner {
  week: number;
  year: string;
  month: string;
  month_number: number;
  month_number_str: string;
  monday: string;
  saturday: string;
  sunday: string;
}

export interface DetailWeeklyPlan {
  id: number;
  master_ypi_id: number;
  minggu: number;
  tanggal: string;
  session: number;
  jam: string;
  details: string;
  set: number;
  rep: number;
  pace: number;
  work_time: number;
  rest_interval: number;
  rest_period: number;
  emphesis_goal: number;
  duration: number;
  intensity: number;
  tr_load: number;
  //
  no?: number;
  fieldIndex?: number;
  tanggal_moment?: Moment;
  jam_moment?: Moment;
  key?: string;
}

export interface HeaderWeeklyPlan {
  id: number;
  master_ypi_id: number;
  minggu: number; //week_index
  tanggal_awal: string;
  tanggal_akhir: string;
  actual_tr_vol: number;
  actual_tr_vol_prosen: number;
  actual_tr_intensity: number;
  actual_tr_load: number;
  target_tr_vol: number;
  target_tr_vol_prosen: number;
  target_tr_intensity: number;
  target_tr_load: number;
  weekly_training_goal: string;
  notes: string;
  // relation data
  detailsWtp?: Partial<DetailWeeklyPlan>[];
  deletedDetailsWtp?: Partial<DetailWeeklyPlan>[];
  periodization?: string;
  technical_development?: string;
  tactic?: string;
  physical?: string;
  mental?: string;
  session_per_week?: number;
  specificity?: number;
  volume?: number;
  intensity?: number;
}

export interface KpiEvent {
  id: number;
  pemain_id: number;
  tanggal_kpi: string;
  keterangan_1: string;
  keterangan_2: string;
  keterangan_3: string;
  keterangan_4: string;
  event_name: string;
  target_1: string;
  target_2: string;
  target_3: string;
  target_4: string;
  actual_1: string;
  actual_2: string;
  actual_3: string;
  actual_4: string;
  //
  no?: number;
  fieldIndex?: number;
  key?: string;
}

export interface KpiPhysic {
  id: number;
  pemain_id: number;
  tanggal_kpi: string;
  keterangan: string;
  tanggal_1: string;
  pb_1: number;
  target_1: number;
  actual_1: number;
  tanggal_2: string;
  pb_2: number;
  target_2: number;
  actual_2: number;
  tanggal_3: string;
  pb_3: number;
  target_3: number;
  actual_3: number;
  tanggal_4: string;
  pb_4: number;
  target_4: number;
  actual_4: number;
  //
  no?: number;
  fieldIndex?: number;
  key?: string;
}

export interface HeaderKpi {
  id: number;
  pemain_id: number;
  tanggal_kpi: string;
}

export interface KpiIndicator {
  id: number;
  name: string;
}

export interface KpiSubIndicator {
  id: number;
  kpi_indicator_id: number;
  name: string;
  indicator_type: 'more_is_better' | 'less_is_better';
  color: string;
}

export interface KpiValue {
  id: number;
  kpi_indicator_id: number;
  kpi_subindicator_id: number;
  date: string;
  pb: number;
  target: number;
  actual: number;
}

export interface Kpi {
  id: number;
  date: string;
  player_id: number;
  umur: number;
  height: number;
  weight: number;
  beep_test: number;
  vo2max: number;
  court_agility_1: number;
  court_agility_2: number;
  court_agility_3: number;
  squad: number;
  bench_press: number;
  vertical_jump: number;
  skipping: number;
  run: number;
  note: number;
}

export interface KpiParameter {
  id: number;
  name: string;
  input_type: 'number' | 'text';
  column_order: number;
  calculate_statistic: boolean;
}

export interface CustomParameter {
  id: number;
  name: string;
  type: string;
  parameters_id: number[];
}

export interface SeasonPlan {
  id: number;
  name: string;
  description: string;
}

export interface SeasonPlanDetail {
  id: number;
  season_plan_id: number;
  name: string;
  input_type: string;
  desc_en: string;
  desc_id: string;
}

export interface SeasonPlanValue {
  id: number;
  ypi_id: number;
  season_plan_id: number;
  season_plan_detail_id: number;
  bg_color: string;
  active: boolean;
  week_1: string;
  week_2: string;
  week_3: string;
  week_4: string;
  week_5: string;
  week_6: string;
  week_7: string;
  week_8: string;
  week_9: string;
  week_10: string;
  week_11: string;
  week_12: string;
  week_13: string;
  week_14: string;
  week_15: string;
  week_16: string;
  week_17: string;
  week_18: string;
  week_19: string;
  week_20: string;
  week_21: string;
  week_22: string;
  week_23: string;
  week_24: string;
  week_25: string;
  week_26: string;
  week_27: string;
  week_28: string;
  week_29: string;
  week_30: string;
  week_31: string;
  week_32: string;
  week_33: string;
  week_34: string;
  week_35: string;
  week_36: string;
  week_37: string;
  week_38: string;
  week_39: string;
  week_40: string;
  week_41: string;
  week_42: string;
  week_43: string;
  week_44: string;
  week_45: string;
  week_46: string;
  week_47: string;
  week_48: string;
  week_49: string;
  week_50: string;
  week_51: string;
  week_52: string;
  week_53: string;
  week_54: string;
  week_55: string;
}

export interface Constant {
  id: number;
  key: string;
  value: string;
}
