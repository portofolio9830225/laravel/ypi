import { Form, FormInstance, message } from 'antd';
import { createContext, useEffect, useState } from 'react';
import FormConfiguration from './components/Form';
import { Player, GroupClassification, FitnessTest } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import { useIntl } from 'umi';
import { Moment } from 'moment';
import PrintModal from './components/PrintModal';

export interface Record extends Player {
  isAgeGroupIndicator?: boolean;
  isAgeGroupFooter?: boolean;
}

export enum ReportType {
  Global = 'Global',
  GroupByCategory = 'Category',
}

export interface FormType {
  group_id: number;
  gender: 'Male' | 'Female';
  player_id: number;
  year_range: [Moment, Moment];
  start_year: string;
  end_year: string;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.tournament-detail.' + fieldName;
};

export interface PrintData extends FitnessTest {}

export interface ContextType {
  form: FormInstance<FormType> | undefined;
  groups: GroupClassification[];
  players: Player[];
  printData: PrintData[];
  setPrintData: React.Dispatch<React.SetStateAction<PrintData[]>>;
}

export const Context = createContext<ContextType>({
  form: undefined,
  groups: [],
  players: [],
  printData: [],
  setPrintData: () => {},
});

export default () => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const [groups, setGroups] = useState<GroupClassification[]>([]);
  const [players, setPlayers] = useState<Player[]>([]);
  const [printData, setPrintData] = useState<PrintData[]>([]);

  const contextValue: ContextType = {
    form,
    groups,
    players,
    printData,
    setPrintData,
  };

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const groupRes = await get('tabel_kelompoks');
      setGroups(groupRes.data);
      const playerRes = await get('master_pemains');
      setPlayers(playerRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
      <PrintModal />
    </Context.Provider>
  );
};
