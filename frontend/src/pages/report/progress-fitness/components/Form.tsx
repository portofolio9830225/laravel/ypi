import { Form, Select, Button, Space, DatePicker, Modal, message } from 'antd';
import { Context, FormType, PrintData } from '../index';
import { useContext, useState } from 'react';
import { create } from '@/services/ypi/api';
import { FormattedMessage, useIntl, useHistory } from 'umi';
import { PrinterOutlined } from '@ant-design/icons';
import moment from 'moment';
import { Player } from '@/types/ypi';
const { Option } = Select;

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const [filteredPlayer, setFilteredPlayer] = useState<Player[]>([]);
  const { form, groups, players, setPrintData } = useContext(Context);

  const handleCancel = () => {
    history.goBack();
  };

  const handlePrint = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formData = await form?.validateFields();
      if (formData) {
        formData.start_year = formData.year_range[0].format('YYYY-MM-DD');
        formData.end_year = formData.year_range[1].format('YYYY-MM-DD');
        const dataRes = await create('fitness_tests/progress_report', formData);
        const data: PrintData[] = dataRes.data;
        if (data.length === 0) message.info(intl.formatMessage({ id: 'message.data_not_found' }));
        setPrintData(
          (dataRes.data as PrintData[]).map((d) => ({
            ...d,
            tanggal_test: d.tanggal_test.substring(0, 10),
          })),
        );
      }
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const handleFormChange = (changedValues: any, values: FormType) => {
    if (changedValues.group_id) {
      form?.setFieldsValue({ player_id: undefined });
      setFilteredPlayer(players.filter((d) => d.kelompok_id === changedValues.group_id));
    }
  };

  return (
    <Modal
      title={<FormattedMessage id="menu.report.progress-fitness" />}
      centered={true}
      visible={true}
      closable={false}
      footer={
        <Space>
          <Button htmlType="button" onClick={handleCancel}>
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
          <Button type="primary" htmlType="button" onClick={handlePrint} icon={<PrinterOutlined />}>
            {intl.formatMessage({ id: 'commonField.preview' })}
          </Button>
        </Space>
      }
    >
      <Form
        onValuesChange={handleFormChange}
        size="small"
        scrollToFirstError
        form={form}
        layout="vertical"
      >
        <Form.Item
          rules={[{ required: true, message: 'Please enter Group' }]}
          label={<FormattedMessage id={'commonField.group'} />}
          name="group_id"
        >
          <Select>
            {groups.map((d, i) => (
              <Option value={d.id} key={i}>
                {d.kelompok}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label={<FormattedMessage id={'commonField.gender'} />}
          name="gender"
          rules={[{ required: true, message: 'Please enter Gender' }]}
        >
          <Select>
            <Option value="Male">
              <FormattedMessage id="commonField.male" />
            </Option>
            <Option value="Female">
              <FormattedMessage id="commonField.female" />
            </Option>
          </Select>
        </Form.Item>

        <Form.Item
          label={<FormattedMessage id={'commonField.player'} />}
          name="player_id"
          rules={[{ required: true, message: 'Please enter Player' }]}
        >
          <Select>
            {filteredPlayer.map((d, i) => (
              <Option value={d.id} key={i}>
                {d.nomor_ic} - {d.nama_lengkap}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label={<FormattedMessage id={'commonField.year_range'} />}
          name="year_range"
          initialValue={[moment().subtract(1, 'y'), moment()]}
        >
          <DatePicker.RangePicker picker="year" />
        </Form.Item>
      </Form>
    </Modal>
  );
};
