import { Drawer, Space, Button, message } from 'antd';
import styles from './PrintModal.less';
import { Context } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { jsPDF } from 'jspdf';
import { Line, LineConfig } from '@ant-design/plots';
import { FooterReport } from '@/components/FooterReport';

const Header = () => {
  return (
    <div className={styles.header}>
      <div className={styles.title}>
        <FormattedMessage id="menu.report.progress-fitness" />
      </div>
    </div>
  );
};

const Chart = ({ title, yField }: { title: string; yField: string }) => {
  const { printData } = useContext(Context);

  const config: LineConfig = {
    data: printData || [],
    xField: 'tanggal_test',
    yField,
    height: 200,
    appendPadding: 16,
    point: {
      size: 4,
      shape: 'square',
      style: {
        lineWidth: 2,
      },
    },
    theme: {
      styleSheet: {
        backgroundColor: '#ffffff',
      },
    },
  };

  return (
    <>
      <div className={styles.chart_title}>{title}</div>
      <Line {...config} />
    </>
  );
};

const Footer = () => {
  return <div></div>;
};

export default () => {
  const [visible, setVisible] = useState<boolean>(false);
  const intl = useIntl();
  const { printData, setPrintData } = useContext(Context);

  useEffect(() => {
    if (printData.length > 0) {
      setVisible(true);
    }
  }, [printData]);

  const handleClose = () => {
    setVisible(false);
    setPrintData([]);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('progress-fitness-report.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1000,
      autoPaging: 'text',
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );

  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={visible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <Chart title="Court Agility" yField="court_agility" />
          <Chart title="VO2 Max" yField="vo2max" />
          <div style={{ height: '10rem' }} />
          <Chart title="Bench Press" yField="bench_press" />
          <Chart title="Squat" yField="squat" />
          <Footer />
          <FooterReport />
        </div>
      </Drawer>
    </>
  );
};
