import { Card } from 'antd';
import { useContext, useEffect, useState } from 'react';
import { Context } from '../index';
import { median } from '@/utils/calculation';
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  ReferenceArea,
  Tooltip,
  ResponsiveContainer,
  Legend,
  Label,
  LabelList,
} from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
import { useIntl } from 'umi';

const colors = scaleOrdinal(schemeCategory10).range();
export default () => {
  const intl = useIntl();
  const { tableData } = useContext(Context);

  const [minX, setMinX] = useState<number>(0);
  const [minY, setMinY] = useState<number>(0);

  const [maxX, setMaxX] = useState<number>(0);
  const [maxY, setMaxY] = useState<number>(0);

  const [medianX, setMedianX] = useState<number>(0);
  const [medianY, setMedianY] = useState<number>(0);

  useEffect(() => {
    let benchpresses: number[] = [];
    let squats: number[] = [];
    tableData.forEach((d) => {
      if (d.benchpress != 0) benchpresses.push(d.benchpress);
      if (d.squat != 0) squats.push(d.squat);
    });

    setMedianX(median(benchpresses) || 0);
    setMedianY(median(squats) || 0);

    setMinX(Math.min(...benchpresses));
    setMinY(Math.min(...squats));

    setMaxX(Math.max(...benchpresses));
    setMaxY(Math.max(...squats));
  }, [tableData]);

  return (
    <Card style={{ height: '24rem' }}>
      <ResponsiveContainer width="100%" height="100%" minHeight="22rem">
        <ScatterChart
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <ZAxis type="category" dataKey="player_name" name="Name" />
          <XAxis
            type="number"
            dataKey="benchpress"
            name="Best Court Agility"
            domain={['dataMin', 'dataMax']}
            tickCount={10}
            label={{ value: 'Bench Press', position: 'insideBottomRight', offset: -20 }}
          />
          <YAxis
            orientation="right"
            type="number"
            tickCount={10}
            dataKey="squat"
            name="VO2Max"
            domain={['dataMin', 'dataMax']}
            label={{ value: 'Squat', angle: 90, position: 'insideRight' }}
          />
          <Tooltip label="-" cursor={{ strokeDasharray: '3 3' }} />

          <Legend />
          <ReferenceArea
            x1={minX}
            x2={medianX}
            y1={minY}
            y2={medianY}
            stroke="red"
            strokeOpacity={0.3}
            fill="red"
          >
            <Label value="Upper and Lower Body Weak" />
          </ReferenceArea>
          <ReferenceArea
            x1={minX}
            x2={medianX}
            y1={medianY}
            y2={maxY}
            stroke="red"
            strokeOpacity={0.3}
            fill="yellow"
          >
            <Label value="Upper Body Weak" />
          </ReferenceArea>
          <ReferenceArea
            x1={medianX}
            x2={maxX}
            y1={medianY}
            y2={maxY}
            stroke="red"
            strokeOpacity={0.3}
            fill="green"
          >
            <Label value="Best" />
          </ReferenceArea>
          <ReferenceArea
            x1={medianX}
            x2={maxX}
            y1={minY}
            y2={medianY}
            stroke="red"
            strokeOpacity={0.3}
            fill="yellow"
          >
            <Label value="Lower Body Weak" />
          </ReferenceArea>
          {tableData.map((d, i) => (
            <Scatter key={i} name={d.player_name} data={[d]} fill={colors[i % colors.length]}>
              <LabelList dataKey="player_name" position="top" fill="#606060" />
            </Scatter>
          ))}
        </ScatterChart>
      </ResponsiveContainer>
    </Card>
  );
};
