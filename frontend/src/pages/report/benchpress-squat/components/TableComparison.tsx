import { Table, TableColumnsType as ColumnsType } from 'antd';
import { useContext, useEffect } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { ChartData, Context, TableRecord } from '../index';
import { median } from '@/utils/calculation';
import styles from './table.less';

const groupBy = function (xs: any[], key: string) {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export default () => {
  const intl = useIntl();
  const { chartData, setTableData, tableData, dependencyData } = useContext(Context);

  useEffect(() => {
    const groupedData = groupBy(chartData, 'player_name');
    setTableData(() => {
      const tableData: TableRecord[] = [];
      for (const d in groupedData) {
        const row: TableRecord = { player_name: '', benchpress: 0, squat: 0, key: '0' };
        const groupedDataByName: ChartData[] = groupedData[d];
        row.player_name = groupedDataByName[0].player_name;
        groupedDataByName.forEach((d, i) => {
          if (d.parameter === dependencyData?.benchpress_parameter.name) {
            row.benchpress = d.value;
          }
          if (d.parameter === dependencyData?.squat_parameter.name) {
            row.squat = d.value;
          }
        });
        tableData.push(row);
      }
      return tableData.map((d, i) => ({ ...d, key: i.toString() }));
    });
  }, [chartData]);

  const colums: ColumnsType<TableRecord> = [
    {
      title: intl.formatMessage({ id: 'commonField.player_name' }),
      dataIndex: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'commonField.bench_press' }),
      dataIndex: 'benchpress',
    },
    {
      title: intl.formatMessage({ id: 'commonField.squat' }),
      dataIndex: 'squat',
    },
  ];

  const summary = (data: readonly TableRecord[]) => {
    let benchpresses: number[] = [];
    let squats: number[] = [];

    data.forEach((value) => {
      value.benchpress && benchpresses.push(value.benchpress);
      value.squat && squats.push(value.squat);
    });

    return (
      <>
        <Table.Summary.Row className={styles.densed_row} style={{ fontWeight: '500' }}>
          <Table.Summary.Cell index={0}>
            <FormattedMessage id="commonField.median" />
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{median(benchpresses)?.toFixed(2)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{median(squats)?.toFixed(2)}</Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row className={styles.densed_row} style={{ fontWeight: '500' }}>
          <Table.Summary.Cell index={0}>
            <FormattedMessage id="commonField.min" />
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{Math.min(...benchpresses).toFixed(2)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{Math.min(...squats).toFixed(2)}</Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row className={styles.densed_row} style={{ fontWeight: '500' }}>
          <Table.Summary.Cell index={0}>
            <FormattedMessage id="commonField.max" />
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{Math.max(...benchpresses).toFixed(2)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{Math.max(...squats).toFixed(2)}</Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  };

  return (
    <Table
      bordered
      summary={tableData.length > 0 ? summary : undefined}
      size="small"
      dataSource={tableData}
      columns={colums}
      className={styles.custom_table}
      rowClassName={styles.densed_row}
    />
  );
};
