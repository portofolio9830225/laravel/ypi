import FormConfiguration from './components/FormConfiguration';
import { createContext, useEffect, useState } from 'react';
import { Player, KpiParameter } from '@/types/ypi';
import { message, Form, FormInstance } from 'antd';
import { useIntl } from 'umi';
import { get } from '@/services/ypi/api';
import CreateParameterModal from './components/ParameterModal';
import KpiChart from './components/KpiChart';

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.kpi-comparison.' + fieldName;
};

interface DevType {
  edited: boolean;
  key: string;
}

export interface CustomParameter extends DevType {
  id: number;
  name: string;
  type: string;
  parameters_id: number[];
}

export enum ChartType {
  Line = 1,
  Column,
  Radar,
}

interface FormType {
  players_id: number[];
  parameters_id: string;
  chart_type: ChartType;
}

interface ChartData {
  parameter: string;
  value: number;
  player_name: string;
}

interface ContextType {
  players: Player[];
  kpiParameters: KpiParameter[];
  form: FormInstance<FormType> | undefined;
  modalVisible: boolean;
  setModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  chartData: ChartData[];
  setChartData: React.Dispatch<React.SetStateAction<ChartData[]>>;
  customParameters: Partial<CustomParameter>[];
  setCustomParameters: React.Dispatch<React.SetStateAction<Partial<CustomParameter>[]>>;
}

export const Context = createContext<ContextType>({
  players: [],
  kpiParameters: [],
  chartData: [],
  form: undefined,
  modalVisible: false,
  setModalVisible: () => {},
  setChartData: () => {},
  customParameters: [],
  setCustomParameters: () => {},
});

export default () => {
  const intl = useIntl();
  const [players, setPlayers] = useState<Player[]>([]);
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [kpiParameters, setKpiParameters] = useState<KpiParameter[]>([]);
  const [form] = Form.useForm<FormType>();
  const [chartData, setChartData] = useState<ChartData[]>([]);
  const [customParameters, setCustomParameters] = useState<Partial<CustomParameter>[]>([]);

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const playerRes = await get('master_pemains');
      setPlayers(playerRes.data);
      const kpiParameterRes = await get('kpi_parameters?input_type=number');
      setKpiParameters(kpiParameterRes.data);
      const parameterRes = await get('constants/parameter/KPI_COMPARISON');
      setCustomParameters(
        (parameterRes.data as CustomParameter[]).map((d, i) => ({ ...d, key: i.toString() })),
      );
    } catch (error) {
      message.error(intl.formatMessage({ id: 'errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  const contextValue = {
    players,
    kpiParameters,
    form,
    modalVisible,
    setModalVisible,
    chartData,
    setChartData,
    customParameters,
    setCustomParameters,
  };

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
      <CreateParameterModal />
      <KpiChart />
    </Context.Provider>
  );
};
