import { Radar, Line, Column } from '@ant-design/plots';
import { useContext } from 'react';
import { Context, ChartType } from '../index';
import { Card, Form } from 'antd';

export default () => {
  const { chartData, form } = useContext(Context);
  const chartType = Form.useWatch('chart_type', form);

  const lineConfig = {
    data: chartData,
    xField: 'parameter',
    yField: 'value',
    seriesField: 'player_name',
    point: {
      size: 4,
    },
  };

  const columnConfig = {
    data: chartData,
    xField: 'parameter',
    yField: 'value',
    seriesField: 'player_name',
    isGroup: true,
  };

  const radarConfig = {
    data: chartData,
    xField: 'parameter',
    yField: 'value',
    seriesField: 'player_name',
    area: {},
    point: {
      size: 2,
    },
  };

  if (chartType === ChartType.Column)
    return (
      <Card>
        <Column {...columnConfig} />
      </Card>
    );

  if (chartType === ChartType.Radar)
    return (
      <Card>
        <Radar {...radarConfig} />
      </Card>
    );

  return (
    <Card>
      <Line {...lineConfig} />
    </Card>
  );
};
