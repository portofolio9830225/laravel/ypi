import {
  Modal,
  Form,
  Space,
  Button,
  Table,
  FormInstance,
  Tooltip,
  Popconfirm,
  Input,
  message,
  Select,
} from 'antd';
import { useContext, createContext, FC, useState, useEffect } from 'react';
import { FormattedMessage, useIntl, Access } from 'umi';
import { Context, getLocaleKey, CustomParameter } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { create, destroy } from '@/services/ypi/api';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
  Textarea,
  MultiSelect,
  Select,
}

const EditableContext = createContext<FormInstance<CustomParameter> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof CustomParameter | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: CustomParameter;
  handleToggleEdit: (form: FormInstance<CustomParameter> | null, record: CustomParameter) => void;
  handleSave: (
    form: FormInstance<CustomParameter> | null,
    record: CustomParameter,
  ) => Promise<void>;
  handleDelete: (record: CustomParameter) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);
  const { kpiParameters } = useContext(Context);

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Form.Item name="id" hidden />
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.MultiSelect && !record.edited) {
    let value = '';
    if (record.parameters_id.length > 0) {
      record.parameters_id.forEach((d) => {
        const kpiParameter = kpiParameters.find((dd) => dd.id === d);
        if (kpiParameter) value += kpiParameter.name + ', ';
      });
    }
    return <td {...props}>{value}</td>;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) {
      inputNode = <Input style={{ width: '100%' }} />;
      rules = [{ required: true, message: 'Please Input Name' }];
    }
    if (inputType === InputType.Select) {
      inputNode = (
        <Select>
          <Select.Option value="max">Max</Select.Option>
          <Select.Option value="min">Min</Select.Option>
          <Select.Option value="avg">Average</Select.Option>
        </Select>
      );
      rules = [{ required: true, message: 'Please Input Type' }];
    }
    if (inputType === InputType.MultiSelect) {
      inputNode = (
        <Select
          mode="multiple"
          allowClear
          style={{ width: '100%' }}
          optionFilterProp="children"
          filterOption={(input, option) =>
            (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
          }
        >
          {kpiParameters.map((d, i) => (
            <Select.Option key={i} value={d.id}>
              {d.name}
            </Select.Option>
          ))}
        </Select>
      );
      rules = [{ required: true, message: 'Please Input Parameters' }];
    }

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

export default () => {
  const intl = useIntl();
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const { modalVisible, setModalVisible, customParameters, setCustomParameters } =
    useContext(Context);

  useEffect(() => {
    if (modalVisible) {
    }
  }, [modalVisible]);

  const handleClose = () => {
    setModalVisible(false);
  };

  const Footer = () => (
    <Space>
      <Button onClick={handleClose} type="primary">
        <FormattedMessage id={'crud.save'} />
      </Button>
    </Space>
  );

  const handleToggleEdit = (
    form: FormInstance<CustomParameter> | null,
    record: CustomParameter,
  ) => {
    const editedDataIndex = customParameters.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);
    const currentData = [...customParameters];
    currentData[editedDataIndex] = editedData;
    setCustomParameters(currentData);
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.name' }),
      dataIndex: 'name',
      inputType: InputType.Text,
      width: '10rem',
    },
    {
      title: intl.formatMessage({ id: 'commonField.type' }),
      dataIndex: 'type',
      inputType: InputType.Select,
    },
    {
      title: intl.formatMessage({ id: 'commonField.parameter' }) + 's',
      dataIndex: 'parameters_id',
      inputType: InputType.MultiSelect,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      width: '20%',
    },
  ];

  const handleSave = async (
    form: FormInstance<CustomParameter> | null,
    record: CustomParameter,
  ) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = customParameters.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();
      if (values && values.parameters_id.length < 2) {
        message.info('Input more than 1 parameters');
        throw new Error('Input more than 1 parameters');
      }
      console.log(values);
      // return;
      let savedData = {};

      const createRes = await create('constants/parameter/KPI_COMPARISON', values);
      savedData = createRes.data;

      let currentData = [...customParameters];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      hideLoading();
      setIsEditMode(false);
      setCustomParameters(currentData);
    } catch (error) {
      setIsEditMode(false);
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: CustomParameter) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = customParameters.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy('constants/parameter/KPI_COMPARISON', record.id);
      }
      let currentData = [...customParameters];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setCustomParameters(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: CustomParameter) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    const newData = [...customParameters];
    newData.push({
      id: undefined,
      edited: true,
      key: newData.length.toString(),
    });
    setIsEditMode(!isEditMode);
    setCustomParameters([...newData]);
  };

  const footerTable = () => {
    if (isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Modal
      title={<FormattedMessage id={getLocaleKey('create_parameter')} />}
      footer={<Footer />}
      width="60rem"
      visible={modalVisible}
      onCancel={handleClose}
    >
      <Table
        size="small"
        className="file-child-content"
        components={components}
        bordered
        dataSource={customParameters}
        columns={processedColumns}
        scroll={{ x: 'max-content' }}
        footer={footerTable}
        pagination={isEditMode ? false : { size: 'small' }}
      />
    </Modal>
  );
};
