import { Space, Button, Form, Select, Row, Col, Divider, message } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { useContext } from 'react';
import { Context, getLocaleKey, ChartType } from '../index';
import { EditOutlined } from '@ant-design/icons';
import { create } from '@/services/ypi/api';

export default () => {
  const intl = useIntl();
  const { players, setModalVisible, form, setChartData, kpiParameters, customParameters } =
    useContext(Context);

  const handleCreateParameter = () => {
    setModalVisible(true);
  };

  const handleProcess = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formValues = await form?.validateFields();
      // console.log(formValues);
      const chartDataRes = await create('kpi_results/compare', formValues);
      setChartData(chartDataRes.data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Chart Data' }));
    }
    hideLoading();
  };

  return (
    <Form form={form} layout="vertical">
      <Row gutter={8}>
        <Col span={6}>
          <Form.Item
            name="players_id"
            label={<FormattedMessage id={getLocaleKey('players')} />}
            rules={[{ required: true, message: 'Please input players' }]}
          >
            <Select
              mode="multiple"
              allowClear
              style={{ width: '100%' }}
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
              }
            >
              {players.map((d, i) => (
                <Select.Option value={d.id} key={i}>
                  {d.nama_lengkap}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            rules={[{ required: true, message: 'Please input parameters' }]}
            name="parameters_id"
            label={<FormattedMessage id={getLocaleKey('parameters')} />}
          >
            <Select
              mode="multiple"
              allowClear
              style={{ width: '100%' }}
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
              }
            >
              {kpiParameters.map((d, i) => (
                <Select.Option key={i + 1} value={d.id}>
                  {d.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="custom_parameters_id"
            label={<FormattedMessage id={getLocaleKey('custom_parameters')} />}
          >
            <Select
              mode="multiple"
              allowClear
              style={{ width: '100%' }}
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
              }
              dropdownRender={(menu) => (
                <>
                  {menu}
                  <Divider />
                  <Space style={{ padding: '0 8px 4px' }}>
                    <Button type="primary" onClick={handleCreateParameter} icon={<EditOutlined />}>
                      <FormattedMessage id={getLocaleKey('create_parameter')} />
                    </Button>
                  </Space>
                </>
              )}
            >
              {customParameters.map((d, i) => (
                <Select.Option key={i} value={d.id}>
                  {d.name}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="chart_type"
            label={<FormattedMessage id="commonField.chart_type" />}
            initialValue={ChartType.Line}
          >
            <Select>
              <Select.Option value={ChartType.Line}>Line</Select.Option>
              <Select.Option value={ChartType.Column}>Bar</Select.Option>
              <Select.Option value={ChartType.Radar}>Radar</Select.Option>
            </Select>
          </Form.Item>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Item>
            <Button type="primary" onClick={handleProcess}>
              <FormattedMessage id="crud.generate_report" />
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
