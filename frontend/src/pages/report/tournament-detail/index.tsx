import { Form, FormInstance, message } from 'antd';
import { createContext, useEffect, useState } from 'react';
import FormConfiguration from './components/Form';
import { Player, CompetitionInfo } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import { useIntl } from 'umi';
import { Moment } from 'moment';

export interface Record extends Player {
  isAgeGroupIndicator?: boolean;
  isAgeGroupFooter?: boolean;
}

export interface ContextType {
  form: FormInstance<FormType> | undefined;
  competitionDetails: CompetitionInfo[];
}

export const Context = createContext<ContextType>({
  form: undefined,
  competitionDetails: [],
});

export enum ReportType {
  Global = 'Global',
  GroupByCategory = 'Category',
}

interface FormType {
  report_type: ReportType;
  id_competition_detail?: number;
  year_range: [Moment, Moment];
  start_year: number;
  end_year: number;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.tournament-detail.' + fieldName;
};

export default () => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const [competitionDetails, setCompetitionDetails] = useState<CompetitionInfo[]>([]);

  const contextValue: ContextType = {
    form,
    competitionDetails,
  };

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const competitionStatusesRes = await get('tabel_kategoris');
      setCompetitionDetails(competitionStatusesRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
    </Context.Provider>
  );
};
