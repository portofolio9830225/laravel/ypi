import { Form, Select, Button, Space, DatePicker, Modal } from 'antd';
import { Context, ReportType, getLocaleKey } from '../index';
import { useContext } from 'react';
import { create } from '@/services/ypi/api';
import { FormattedMessage, useIntl, useHistory, useModel } from 'umi';
import { PrinterOutlined } from '@ant-design/icons';
const { Option } = Select;

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const { form, competitionDetails } = useContext(Context);
  const { initialState } = useModel('@@initialState');

  const handleCancel = () => {
    history.goBack();
  };

  const handlePrint = async () => {
    const formData = await form?.validateFields();
    if (formData) {
      formData.start_year = formData.year_range[0].year();
      formData.end_year = formData.year_range[1].year();
      if (formData.id_competition_detail === 0) {
        delete formData.id_competition_detail;
      }
      let pdf = undefined;
      const payload = {
        ...formData,
        club_image: initialState?.currentClub?.image,
      };
      if (formData.report_type === ReportType.Global) {
        pdf = await create('report_competition/global', payload, { responseType: 'blob' });
      } else if (formData.report_type === ReportType.GroupByCategory) {
        pdf = await create('report_competition/group-by-category', payload, {
          responseType: 'blob',
        });
      }
      if (pdf) {
        const url = window.URL.createObjectURL(new Blob([pdf], { type: 'application/pdf' }));
        window.open(url);
      }
    }
  };

  return (
    <Modal
      title="Tournament Detail Report"
      centered={true}
      visible={true}
      closable={false}
      footer={
        <Space>
          <Button htmlType="button" onClick={handleCancel}>
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
          <Button type="primary" htmlType="button" onClick={handlePrint} icon={<PrinterOutlined />}>
            {intl.formatMessage({ id: 'crud.print' })}
          </Button>
        </Space>
      }
    >
      <Form size="small" scrollToFirstError form={form} layout="vertical">
        <Form.Item
          label={<FormattedMessage id={'commonField.report_type'} />}
          name="report_type"
          initialValue={ReportType.Global}
        >
          <Select>
            <Option value={ReportType.Global}>
              <FormattedMessage id={getLocaleKey('global')} />
            </Option>
            <Option value={ReportType.GroupByCategory}>
              <FormattedMessage id={getLocaleKey('category')} />
            </Option>
          </Select>
        </Form.Item>

        <Form.Item
          label="Competition Detail"
          name="id_competition_detail"
          rules={[{ required: true, message: 'Please enter Competition Detail' }]}
        >
          <Select>
            <Option value={0}>
              <FormattedMessage id={getLocaleKey('all')} />
            </Option>
            {competitionDetails.map((d, i) => (
              <Option value={d.id} key={i}>
                {d.nama_kategori}
              </Option>
            ))}
          </Select>
        </Form.Item>

        <Form.Item
          label={<FormattedMessage id={'commonField.year_range'} />}
          name="year_range"
          rules={[{ required: true, message: 'Please enter Competition Detail' }]}
        >
          <DatePicker.RangePicker picker="year" />
        </Form.Item>
      </Form>
    </Modal>
  );
};
