import { Form, Select, Button, Space, Row, Col, Modal, Checkbox } from 'antd';
import { Context, ReportType, getLocaleKey, PlayerStatus, SpecialistType } from '../index';
import { useContext } from 'react';
import { create } from '@/services/ypi/api';
import { FormattedMessage, useIntl, useHistory, useModel } from 'umi';
import { PrinterOutlined } from '@ant-design/icons';
const { Option } = Select;
import type { CheckboxValueType } from 'antd/es/checkbox/Group';

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const { form } = useContext(Context);
  const { initialState } = useModel('@@initialState');

  const handleCancel = () => {
    history.goBack();
  };

  const handlePrint = async () => {
    // return;
    const formData = form?.getFieldsValue();
    const payload = {
      ...formData,
      club_image: initialState?.currentClub?.image,
    };
    let pdf = undefined;
    if (formData?.report_type === ReportType.Global) {
      pdf = await create('report_player/global', payload, { responseType: 'blob' });
    }
    if (formData?.report_type === ReportType.GroupByAge) {
      pdf = await create('report_player/age', payload, { responseType: 'blob' });
    }
    if (formData?.report_type === ReportType.GroupBySpecialist) {
      pdf = await create('report_player/specialist', payload, { responseType: 'blob' });
    }
    if (pdf) {
      const url = window.URL.createObjectURL(new Blob([pdf], { type: 'application/pdf' }));
      window.open(url);
    }
  };

  const onChange = (checkedValues: CheckboxValueType[]) => {
    console.log('checked = ', checkedValues);
  };

  const plainOptions = [
    {
      label: <FormattedMessage id={'pages.player-data.tunggal_putra'} />,
      value: SpecialistType.ManSingle,
    },
    {
      label: <FormattedMessage id={'pages.player-data.ganda_putra'} />,
      value: SpecialistType.ManDouble,
    },
    {
      label: <FormattedMessage id={'pages.player-data.tunggal_putri'} />,
      value: SpecialistType.WomanSingle,
    },
    {
      label: <FormattedMessage id={'pages.player-data.ganda_putri'} />,
      value: SpecialistType.WomanDouble,
    },
    {
      label: <FormattedMessage id={'pages.player-data.ganda_campuran'} />,
      value: SpecialistType.MixDouble,
    },
  ];

  return (
    <Modal
      title="Data Player Report"
      centered={true}
      visible={true}
      closable={false}
      footer={
        <Space>
          <Button htmlType="button" onClick={handleCancel}>
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
          <Button type="primary" htmlType="button" onClick={handlePrint} icon={<PrinterOutlined />}>
            {intl.formatMessage({ id: 'crud.print' })}
          </Button>
        </Space>
      }
    >
      <Form size="small" scrollToFirstError form={form} layout="vertical">
        <Row gutter={8}>
          <Col span={8}>
            <Form.Item
              label={<FormattedMessage id={getLocaleKey('report_type')} />}
              name="report_type"
              initialValue={ReportType.Global}
            >
              <Select placement="topLeft">
                <Option value={ReportType.Global}>
                  <FormattedMessage id={getLocaleKey('global')} />
                </Option>
                <Option value={ReportType.GroupBySpecialist}>
                  <FormattedMessage id={getLocaleKey('group_by_specialist')} />
                </Option>
                <Option value={ReportType.GroupByAge}>
                  <FormattedMessage id={getLocaleKey('group_by_age')} />
                </Option>
                {/* <Option value={ReportType.GroupByAgeAndSpecialist}>
                  <FormattedMessage id={getLocaleKey('group_by_age_specialist')} />
                </Option> */}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label={<FormattedMessage id={getLocaleKey('specialist')} />}
              name="specialist"
              // initialValue={[1, 2, 3, 4, 5]}
            >
              <Checkbox.Group options={plainOptions} onChange={onChange} />
              {/* <Select mode="multiple" placement="topLeft">
                <Option value={SpecialistType.ManSingle}>
                  <FormattedMessage id={'pages.player-data.tunggal_putra'} />
                </Option>
                <Option value={SpecialistType.ManDouble}>
                  <FormattedMessage id={'pages.player-data.ganda_putra'} />
                </Option>
                <Option value={SpecialistType.WomanSingle}>
                  <FormattedMessage id={'pages.player-data.tunggal_putri'} />
                </Option>
                <Option value={SpecialistType.WomanDouble}>
                  <FormattedMessage id={'pages.player-data.ganda_putri'} />
                </Option>
                <Option value={SpecialistType.MixDouble}>
                  <FormattedMessage id={'pages.player-data.ganda_campuran'} />
                </Option>
              </Select> */}
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label={<FormattedMessage id={getLocaleKey('player_status')} />}
              name="player_status"
              initialValue={PlayerStatus.All}
            >
              <Select placement="topLeft">
                <Option value={PlayerStatus.All}>
                  <FormattedMessage id={getLocaleKey('all_player')} />
                </Option>
                <Option value={PlayerStatus.Active}>
                  <FormattedMessage id={getLocaleKey('active')} />
                </Option>
                <Option value={PlayerStatus.Inactive}>
                  <FormattedMessage id={getLocaleKey('inactive')} />
                </Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
