import { Form, FormInstance } from 'antd';
import { createContext } from 'react';
import FormConfiguration from './components/Form';
import { Player } from '@/types/ypi';

export interface Record extends Player {
  isAgeGroupIndicator?: boolean;
  isAgeGroupFooter?: boolean;
}

export interface ContextType {
  form: FormInstance<FormType> | undefined;
}

export const Context = createContext<ContextType>({
  form: undefined,
});

export const tableName = 'master_pemains';

export enum ReportType {
  Global = 1,
  GroupBySpecialist,
  GroupByAge,
  GroupByAgeAndSpecialist,
}

export enum SpecialistType {
  ManSingle = 1,
  ManDouble,
  WomanSingle,
  WomanDouble,
  MixDouble,
}

export enum PlayerStatus {
  All = 1,
  Active,
  Inactive,
}

interface FormType {
  report_type: ReportType;
  specialist: SpecialistType[];
  player_status: PlayerStatus;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.data-player.' + fieldName;
};

export default () => {
  const [form] = Form.useForm<FormType>();

  const contextValue: ContextType = {
    form,
  };

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
    </Context.Provider>
  );
};
