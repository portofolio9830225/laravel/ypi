import { Form, Select, Button, Space, Modal, DatePicker, Row, Col } from 'antd';
import { Context, getLocaleKey } from '../index';
import { useContext } from 'react';
import { create } from '@/services/ypi/api';
import { FormattedMessage, useIntl, useHistory } from 'umi';
import { PrinterOutlined } from '@ant-design/icons';
const { Option } = Select;

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const { form, groups, coaches } = useContext(Context);

  const handleCancel = () => {
    history.goBack();
  };

  const handlePrint = async () => {
    // return;
    const formData = await form?.validateFields();
    let pdf = undefined;
    // if (formData?.report_type === ReportType.Global) {
    //   pdf = await create('report_player/global', formData, { responseType: 'blob' });
    // }
    if (pdf) {
      const url = window.URL.createObjectURL(new Blob([pdf]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', `player_data.pdf`);

      // Append to html link element page
      document.body.appendChild(link);

      // Start download
      link.click();

      // Clean up and remove the link
      link?.parentNode?.removeChild(link);
    }
  };

  return (
    <Modal
      title={<FormattedMessage id="menu.report.attendance" />}
      centered={true}
      visible={true}
      closable={false}
      footer={
        <Space>
          <Button htmlType="button" onClick={handleCancel}>
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
          <Button type="primary" htmlType="button" onClick={handlePrint} icon={<PrinterOutlined />}>
            {intl.formatMessage({ id: 'crud.print' })}
          </Button>
        </Space>
      }
    >
      <Form size="small" scrollToFirstError form={form} layout="vertical">
        <Row gutter={8}>
          <Col span={12}>
            <Form.Item
              label={<FormattedMessage id={'commonField.group'} />}
              name="group_id"
              rules={[{ required: true, message: 'Please enter Group' }]}
            >
              <Select>
                {groups.map((d, i) => (
                  <Option value={d.id} key={i}>
                    {d.kelompok}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={<FormattedMessage id={'commonField.coach'} />}
              name="coach_id"
              rules={[{ required: true, message: 'Please enter Coach' }]}
            >
              <Select>
                {coaches.map((d, i) => (
                  <Option value={d.id} key={i}>
                    {d.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={8}>
          <Col span={12}>
            <Form.Item
              label={<FormattedMessage id={'commonField.month'} />}
              name="month"
              rules={[{ required: true, message: 'Please enter Month' }]}
            >
              <DatePicker picker="month" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              label={<FormattedMessage id={'commonField.year'} />}
              name="year"
              rules={[{ required: true, message: 'Please enter Year' }]}
            >
              <DatePicker picker="year" />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Modal>
  );
};
