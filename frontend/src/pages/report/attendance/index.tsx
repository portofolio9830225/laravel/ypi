import { Form, FormInstance, message } from 'antd';
import { createContext, useEffect, useState } from 'react';
import FormConfiguration from './components/Form';
import { GroupClassification, Coach } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import { useIntl } from 'umi';

interface FormType {
  group_id: number;
  coach_id: number;
  month: number;
  year: number;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.medical-record.' + fieldName;
};

export interface ContextType {
  form: FormInstance<FormType> | undefined;
  groups: GroupClassification[];
  coaches: Coach[];
}

export const Context = createContext<ContextType>({
  form: undefined,
  groups: [],
  coaches: [],
});

export default () => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const [groups, setGroups] = useState<GroupClassification[]>([]);
  const [coaches, setCoaches] = useState<Coach[]>([]);

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const groupsRes = await get('tabel_kelompoks');
      setGroups(groupsRes.data);
      const coachesRes = await get('master_pelatihs');
      setCoaches(coachesRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  const contextValue: ContextType = {
    form,
    groups,
    coaches,
  };

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
    </Context.Provider>
  );
};
