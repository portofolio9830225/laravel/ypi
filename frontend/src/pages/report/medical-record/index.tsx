import { Form, FormInstance, message } from 'antd';
import { createContext, useEffect, useState } from 'react';
import FormConfiguration from './components/Form';
import { get } from '@/services/ypi/api';
import { useIntl } from 'umi';
import { MedicalRecord } from '@/types/ypi';
import PrintModal from './components/PrintModal';

export interface Record {
  key: string;
  player_name: string;
  ypi_desc: string;
  player_id: number;
  ypi_id: number;
}

interface PrintData {
  player_name: string;
  table_data: MedicalRecord[];
}

export enum ReportType {
  Global = 1,
  GroupByIlness,
  GroupByInjured,
}

interface FormType {
  report_type: ReportType;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.medical-record.' + fieldName;
};

export interface ContextType {
  form: FormInstance<FormType> | undefined;
  tableData: Record[];
  printData: PrintData | undefined;
  setPrintData: React.Dispatch<React.SetStateAction<PrintData | undefined>>;
}

export const Context = createContext<ContextType>({
  form: undefined,
  tableData: [],
  printData: undefined,
  setPrintData: () => {},
});

export default () => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const [tableData, setTableData] = useState<Record[]>([]);
  const [printData, setPrintData] = useState<PrintData>();

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const tableDataRes = await get('medical_records/table_report');
      setTableData((tableDataRes.data as Record[]).map((d, i) => ({ ...d, key: i.toString() })));
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  const contextValue: ContextType = {
    form,
    tableData,
    printData,
    setPrintData,
  };

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
      <PrintModal />
    </Context.Provider>
  );
};
