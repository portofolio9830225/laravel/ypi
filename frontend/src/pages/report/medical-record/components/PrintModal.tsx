import {
  Drawer,
  Table,
  TableColumnsType,
  Space,
  Button,
  message,
  Descriptions,
  Typography,
  Avatar,
} from 'antd';
import styles from './PrintModal.less';
import { getLocaleKey, Context, ReportType } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { MedicalRecord } from '@/types/ypi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { jsPDF } from 'jspdf';
import { FooterReport } from '@/components/FooterReport';
const { Text } = Typography;

const CheckOutlined = () => {
  return <img style={{ width: '1rem' }} src="/icons/done.png" />;
};

var groupBy = function (xs: any[], key: string) {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

const Header = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <Space>
        <span className={styles.title}>Medical</span>
        <div style={{ width: '0.5rem' }} />
        <span className={styles.title}>Record</span>
      </Space>
      <br />
      <Descriptions labelStyle={{ fontWeight: 'bold' }} size="small" column={6}>
        <Descriptions.Item label={<FormattedMessage id="commonField.player_name" />}>
          {printData?.player_name}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

interface TablePrint extends MedicalRecord {
  key: string;
  no: number;
  headerGroup?: 'injured' | 'illied';
  footerGroup?: 'injured' | 'illied';
}

const TableDetail = () => {
  const intl = useIntl();
  const { printData, form } = useContext(Context);
  const [data, setData] = useState<TablePrint[]>([]);

  useEffect(() => {
    if (printData && printData.table_data) {
      const tableData = printData.table_data;
      let processedData: Partial<TablePrint>[] = [];
      let lastRow: Partial<TablePrint>[] = [];
      const reportType = form?.getFieldValue('report_type');

      if (reportType === ReportType.GroupByInjured) {
        const groupedData = groupBy(
          tableData.map((d, i) => ({ ...d, no: i + 1, tanggal: d.tanggal.substring(0, 10) })),
          'primary_injured',
        );
        for (const property in groupedData) {
          if (!property) {
            lastRow = [...groupedData[property]];
          } else {
            processedData.push({ headerGroup: 'injured', primary_injured: property });
            processedData.push(...groupedData[property]);
            let injuredDays = 0;
            let treatmentDays = 0;
            (groupedData[property] as TablePrint[]).forEach((d) => {
              injuredDays += d.injured_days || 0;
              treatmentDays += d.treatment_injured_days || 0;
            });
            processedData.push({
              footerGroup: 'injured',
              primary_injured: `total ${property}: ${groupedData[property].length}`,
              injured_days: `subtotal: ${injuredDays}`,
              treatment_injured_days: `subtotal: ${treatmentDays}`,
            });
          }
        }
      }

      if (reportType === ReportType.GroupByIlness) {
        const groupedData = groupBy(
          tableData.map((d, i) => ({ ...d, no: i + 1, tanggal: d.tanggal.substring(0, 10) })),
          'primary_illied',
        );
        for (const property in groupedData) {
          if (!property) {
            lastRow = [...groupedData[property]];
          } else {
            processedData.push({ headerGroup: 'illied', primary_illied: property });
            processedData.push(...groupedData[property]);
            let illiedDays = 0;
            let treatmentDays = 0;
            (groupedData[property] as TablePrint[]).forEach((d) => {
              illiedDays += d.illied_days || 0;
              treatmentDays += d.treatment_illied_days || 0;
            });
            processedData.push({
              footerGroup: 'illied',
              primary_illied: `total ${property}: ${groupedData[property].length}`,
              illied_days: `subtotal: ${illiedDays}`,
              treatment_illied_days: `subtotal: ${treatmentDays}`,
            });
          }
        }
      }

      if (reportType === ReportType.Global) {
        processedData = tableData.map((d, i) => ({
          ...d,
          no: i + 1,
          tanggal: d.tanggal.substring(0, 10),
        }));
      }

      setData(
        ([...processedData, ...lastRow] as TablePrint[]).map((d, i) => ({
          ...d,
          key: i.toString(),
        })),
      );
    }
  }, [printData]);

  const columns: TableColumnsType<TablePrint> = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
    },
    {
      title: intl.formatMessage({ id: 'commonField.date' }),
      dataIndex: 'tanggal',
    },
    {
      title: intl.formatMessage({ id: 'commonField.day' }),
      dataIndex: 'hari',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('injury_status') }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.medical-record.injured' }),
          dataIndex: 'injured',
          render: (_, record) => record.injured && <CheckOutlined />,
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.primary_injured' }),
          dataIndex: 'primary_injured',
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.detail_injured' }),
          dataIndex: 'detail_injured',
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.treatment_injured' }),
          dataIndex: 'treatment_injured',
          render: (_, record) => record.treatment_injured && <CheckOutlined />,
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.injured_days' }),
          dataIndex: 'injured_days',
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.treatment_injured_days' }),
          dataIndex: 'treatment_injured_days',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('illness_status') }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.medical-record.illied' }),
          dataIndex: 'illied',
          render: (_, record) => record.illied && <CheckOutlined />,
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.primary_illied' }),
          dataIndex: 'primary_illied',
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.symtomps' }),
          dataIndex: 'symptomps',
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.treatment_illied' }),
          dataIndex: 'treatment_illied',
          render: (_, record) => record.treatment_illied && <CheckOutlined />,
        },
        {
          title: intl.formatMessage({ id: 'pages.medical-record.illied_days' }),
          dataIndex: 'illied_days',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.treatment_illied_days' }),
      dataIndex: 'treatment_illied_days',
    },
  ];

  const summary = (data: readonly TablePrint[]) => {
    let totalInjuredDays = 0;
    let totalTreatmentDays = 0;
    let totalSickDays = 0;
    let totalTrainingAbsent = 0;

    data.forEach((value) => {
      if (!value.headerGroup && !value.footerGroup) {
        totalInjuredDays += value.injured_days || 0;
        totalTreatmentDays += value.treatment_injured_days || 0;
        totalSickDays += value.illied_days || 0;
        totalTrainingAbsent += value.treatment_illied_days || 0;
      }
    });

    return (
      <Table.Summary fixed>
        <Table.Summary.Row>
          <Table.Summary.Cell index={0}>
            <Text strong>Total</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={1} />

          <Table.Summary.Cell index={2} colSpan={5} />

          <Table.Summary.Cell index={7}>
            <Text strong>{totalInjuredDays}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={8}>
            <Text strong>{totalTreatmentDays}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={9} colSpan={4} />

          <Table.Summary.Cell index={13}>
            <Text strong>{totalSickDays}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={14}>
            <Text strong>{totalTrainingAbsent}</Text>
          </Table.Summary.Cell>
        </Table.Summary.Row>
      </Table.Summary>
    );
  };

  return (
    <Table
      onRow={(record, rowIndex) => {
        if (record.headerGroup) {
          return {
            style: { backgroundColor: '#c9e5cb' },
          };
        }
        if (record.footerGroup) {
          return {
            style: { backgroundColor: '#e5e3c9' },
          };
        }
        return {};
      }}
      summary={summary}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      pagination={false}
    />
  );
};

export default () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [visible, setVisible] = useState<boolean>(false);

  useEffect(() => {
    if (printData) {
      setVisible(true);
    }
  }, [printData]);

  const handleClose = () => {
    setVisible(false);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('medical-record.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1500,
      autoPaging: 'text',
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );
  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={visible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <br />
          <TableDetail />
          <FooterReport />
        </div>
      </Drawer>
    </>
  );
};
