import { Button, Table, TableColumnsType, Tooltip, message } from 'antd';
import { useIntl } from 'umi';
import { Record, Context } from '../index';
import { useContext } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { create } from '@/services/ypi/api';

export default () => {
  const intl = useIntl();
  const { tableData, setPrintData } = useContext(Context);

  const handlePrint = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const printDataRes = await create('medical_records/data_report', record);
      setPrintData(printDataRes.data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const columns: TableColumnsType<Record> = [
    {
      title: intl.formatMessage({ id: 'commonField.player' }),
      dataIndex: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'commonField.ypi' }),
      dataIndex: 'ypi_desc',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      render: (_, record) => (
        <>
          <Tooltip title="Print">
            <Button
              type="primary"
              onClick={() => handlePrint(record)}
              icon={<PrinterOutlined />}
              shape="circle"
            />
          </Tooltip>
        </>
      ),
    },
  ];

  return <Table bordered size="small" columns={columns} dataSource={tableData} />;
};
