import { Form, Select, Button, Space, Modal } from 'antd';
import { Context, ReportType, getLocaleKey } from '../index';
import { useContext } from 'react';
import { create } from '@/services/ypi/api';
import { FormattedMessage, useIntl, useHistory } from 'umi';
import { PrinterOutlined } from '@ant-design/icons';
const { Option } = Select;
import Table from './Table';

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const { form } = useContext(Context);

  const handleCancel = () => {
    history.goBack();
  };

  return (
    <Modal
      title={<FormattedMessage id="menu.report.medical-record" />}
      centered={true}
      visible={true}
      closable={false}
      footer={
        <Space>
          <Button htmlType="button" onClick={handleCancel}>
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
        </Space>
      }
    >
      <Form size="small" scrollToFirstError form={form} layout="vertical">
        <Form.Item
          label={<FormattedMessage id={'commonField.report_type'} />}
          name="report_type"
          initialValue={ReportType.Global}
        >
          <Select>
            <Option value={ReportType.Global}>
              <FormattedMessage id={getLocaleKey('global')} />
            </Option>
            <Option value={ReportType.GroupByIlness}>
              <FormattedMessage id={getLocaleKey('by_illness')} />
            </Option>
            <Option value={ReportType.GroupByInjured}>
              <FormattedMessage id={getLocaleKey('by_injured')} />
            </Option>
          </Select>
        </Form.Item>

        <Form.Item label={<FormattedMessage id={getLocaleKey('data')} />} name="data">
          <Table />
        </Form.Item>
      </Form>
    </Modal>
  );
};
