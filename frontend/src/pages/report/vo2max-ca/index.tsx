import FormConfiguration from './components/FormConfiguration';
import { createContext, useEffect, useState } from 'react';
import { Player, KpiParameter, GroupClassification, CustomParameter } from '@/types/ypi';
import { message, Form, FormInstance } from 'antd';
import { useIntl } from 'umi';
import { get } from '@/services/ypi/api';
import KpiChart from './components/KpiChart';
import TableComparison from './components/TableComparison';

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.vo2max-ca.' + fieldName;
};

export enum ChartType {
  Line = 1,
  Column,
  Radar,
}

export interface FormType {
  group_id: number;
}

export interface ChartData {
  parameter: string;
  value: number;
  player_name: string;
}

interface RelatedIds {
  vo2max_parameter_id: number;
  ca_min_custom_parameter_id: number;
}

interface DependencyData {
  vo2max_parameter: KpiParameter;
  ca_min_custom_parameter: CustomParameter;
}

export interface TableRecord {
  key: string;
  player_name: string;
  vo2max: number;
  max_ca: number;
}

interface ContextType {
  form: FormInstance<FormType> | undefined;
  modalVisible: boolean;
  setModalVisible: React.Dispatch<React.SetStateAction<boolean>>;
  chartData: ChartData[];
  setChartData: React.Dispatch<React.SetStateAction<ChartData[]>>;
  groups: GroupClassification[];
  players: Player[];
  dependencyData: DependencyData | undefined;
  tableData: TableRecord[];
  setTableData: React.Dispatch<React.SetStateAction<TableRecord[]>>;
}

export const Context = createContext<ContextType>({
  chartData: [],
  form: undefined,
  modalVisible: false,
  setModalVisible: () => {},
  setChartData: () => {},
  groups: [],
  players: [],
  dependencyData: undefined,
  tableData: [],
  setTableData: () => {},
});

export default () => {
  const intl = useIntl();
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [form] = Form.useForm<FormType>();
  const [chartData, setChartData] = useState<ChartData[]>([]);
  const [tableData, setTableData] = useState<TableRecord[]>([]);
  const [groups, setGroups] = useState<GroupClassification[]>([]);
  const [players, setPlayers] = useState<Player[]>([]);
  const [dependencyData, setDependencyData] = useState<DependencyData>();

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const groupsRes = await get('tabel_kelompoks');
      setGroups(groupsRes.data);
      const playersRes = await get('master_pemains');
      setPlayers(playersRes.data);
      const relatedRes = await get('constants?key=DATA_VO2MAX_CA_COMPARISON');
      const relatedIds: RelatedIds = JSON.parse(relatedRes.data[0].value);
      const vo2maxRes = await get('kpi_parameters/' + relatedIds.vo2max_parameter_id);
      const minCaRes = await get(
        'constants/parameter/KPI_COMPARISON/' + relatedIds.ca_min_custom_parameter_id,
      );
      setDependencyData({
        vo2max_parameter: vo2maxRes.data,
        ca_min_custom_parameter: minCaRes.data,
      });
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  const contextValue = {
    form,
    modalVisible,
    setModalVisible,
    chartData,
    setChartData,
    groups,
    players,
    dependencyData,
    tableData,
    setTableData,
  };

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
      <TableComparison />
      <KpiChart />
    </Context.Provider>
  );
};
