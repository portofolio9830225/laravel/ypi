import { Card } from 'antd';
import { useContext, useEffect, useState } from 'react';
import { Context } from '../index';
import { median } from '@/utils/calculation';
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  ReferenceArea,
  Tooltip,
  ResponsiveContainer,
  Legend,
  Label,
  LabelList,
} from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';
import { useIntl } from 'umi';

const colors = scaleOrdinal(schemeCategory10).range();
export default () => {
  const intl = useIntl();
  const { tableData } = useContext(Context);

  const [minX, setMinX] = useState<number>(0);
  const [minY, setMinY] = useState<number>(0);

  const [maxX, setMaxX] = useState<number>(0);
  const [maxY, setMaxY] = useState<number>(0);

  const [medianX, setMedianX] = useState<number>(0);
  const [medianY, setMedianY] = useState<number>(0);

  useEffect(() => {
    let courtAgilities: number[] = [];
    let vo2maxs: number[] = [];
    tableData.forEach((d) => {
      if (d.max_ca != 0) courtAgilities.push(d.max_ca);
      if (d.vo2max != 0) vo2maxs.push(d.vo2max);
    });

    setMedianX(median(courtAgilities) || 0);
    setMedianY(median(vo2maxs) || 0);

    setMinX(Math.min(...courtAgilities));
    setMinY(Math.min(...vo2maxs));

    setMaxX(Math.max(...courtAgilities));
    setMaxY(Math.max(...vo2maxs));
  }, [tableData]);

  return (
    <Card style={{ height: '24rem' }}>
      <ResponsiveContainer width="100%" height="100%" minHeight="22rem">
        <ScatterChart
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <ZAxis type="category" dataKey="player_name" name="Name" />
          <XAxis
            reversed={true}
            type="number"
            dataKey="max_ca"
            name="Best Court Agility"
            domain={['dataMin', 'dataMax']}
            tickCount={10}
            label={{ value: 'Court Agility', position: 'insideBottomRight', offset: -20 }}
          />
          <YAxis
            orientation="right"
            type="number"
            tickCount={10}
            dataKey="vo2max"
            name="VO2Max"
            domain={['dataMin', 'dataMax']}
            label={{ value: 'VO2Max', angle: 90, position: 'insideRight' }}
          />
          <Tooltip cursor={{ strokeDasharray: '3 3' }} />

          <Legend />
          <ReferenceArea
            x1={minX}
            x2={medianX}
            y1={minY}
            y2={medianY}
            stroke="red"
            strokeOpacity={0.3}
            fill="yellow"
          >
            <Label value={intl.formatMessage({ id: 'commonField.low_endurance' })} />
          </ReferenceArea>
          <ReferenceArea
            x1={minX}
            x2={medianX}
            y1={medianY}
            y2={maxY}
            stroke="red"
            strokeOpacity={0.3}
            fill="green"
          >
            <Label value={intl.formatMessage({ id: 'commonField.best' })} />
          </ReferenceArea>
          <ReferenceArea
            x1={medianX}
            x2={maxX}
            y1={medianY}
            y2={maxY}
            stroke="red"
            strokeOpacity={0.3}
            fill="yellow"
          >
            <Label value={intl.formatMessage({ id: 'commonField.low_agility' })} />
          </ReferenceArea>
          <ReferenceArea
            x1={medianX}
            x2={maxX}
            y1={minY}
            y2={medianY}
            stroke="red"
            strokeOpacity={0.3}
            fill="red"
          >
            <Label value={intl.formatMessage({ id: 'commonField.low_agility_endurance' })} />
          </ReferenceArea>
          {tableData.map((d, i) => (
            <Scatter key={i} name={d.player_name} data={[d]} fill={colors[i % colors.length]}>
              <LabelList dataKey="player_name" position="top" fill="#606060" />
            </Scatter>
          ))}
        </ScatterChart>
      </ResponsiveContainer>
    </Card>
  );
};
