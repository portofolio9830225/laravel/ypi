import { Table, TableColumnsType as ColumnsType } from 'antd';
import { useContext, useEffect } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { ChartData, Context, TableRecord } from '../index';
import { median } from '@/utils/calculation';
import styles from './table.less';

const groupBy = function (xs: any[], key: string) {
  return xs.reduce(function (rv, x) {
    (rv[x[key]] = rv[x[key]] || []).push(x);
    return rv;
  }, {});
};

export default () => {
  const intl = useIntl();
  const { chartData, setTableData, tableData, dependencyData } = useContext(Context);

  useEffect(() => {
    const groupedData = groupBy(chartData, 'player_name');
    setTableData(() => {
      const tableData: TableRecord[] = [];
      for (const d in groupedData) {
        const row: TableRecord = { player_name: '', max_ca: 0, vo2max: 0, key: d };
        const groupedDataByName: ChartData[] = groupedData[d];
        row.player_name = groupedDataByName[0].player_name;
        groupedDataByName.forEach((d, i) => {
          if (d.parameter === dependencyData?.vo2max_parameter.name) {
            row.vo2max = d.value;
          }
          if (d.parameter === dependencyData?.ca_min_custom_parameter.name) {
            row.max_ca = d.value;
          }
        });
        tableData.push(row);
      }
      return tableData.map((d, i) => ({ ...d, key: i.toString() }));
    });
  }, [chartData]);

  const colums: ColumnsType<TableRecord> = [
    {
      title: intl.formatMessage({ id: 'commonField.player_name' }),
      dataIndex: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'commonField.court_agility' }),
      dataIndex: 'max_ca',
    },
    {
      title: intl.formatMessage({ id: 'commonField.vo2max' }),
      dataIndex: 'vo2max',
    },
  ];

  const summary = (data: readonly TableRecord[]) => {
    let vo2maxs: number[] = [];
    let maxCas: number[] = [];

    data.forEach((value) => {
      value.vo2max && vo2maxs.push(value.vo2max);
      value.max_ca && maxCas.push(value.max_ca);
    });

    return (
      <>
        <Table.Summary.Row className={styles.densed_row} style={{ fontWeight: '500' }}>
          <Table.Summary.Cell index={0}>
            <FormattedMessage id="commonField.median" />
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{median(maxCas)?.toFixed(2)}</Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{median(vo2maxs)?.toFixed(2)}</Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row className={styles.densed_row} style={{ fontWeight: '500' }}>
          <Table.Summary.Cell index={0}>
            <FormattedMessage id="commonField.min" />
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{Math.min(...maxCas).toFixed(2)}</Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{Math.min(...vo2maxs).toFixed(2)}</Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row className={styles.densed_row} style={{ fontWeight: '500' }}>
          <Table.Summary.Cell index={0}>
            <FormattedMessage id="commonField.max" />
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{Math.max(...maxCas).toFixed(2)}</Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{Math.max(...vo2maxs).toFixed(2)}</Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  };

  return (
    <Table
      className={styles.custom_table}
      bordered
      summary={tableData.length > 0 ? summary : undefined}
      size="small"
      dataSource={tableData}
      columns={colums}
      rowClassName={styles.densed_row}
    />
  );
};
