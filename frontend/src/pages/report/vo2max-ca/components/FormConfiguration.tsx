import { Button, Form, Select, Row, Col, message } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { useContext, useState } from 'react';
import { Context, FormType } from '../index';
import { create } from '@/services/ypi/api';
import { Player } from '@/types/ypi';

export default () => {
  const intl = useIntl();
  const { form, setChartData, groups, players, dependencyData } = useContext(Context);
  const [playersByGroup, setPlayersByGroup] = useState<Player[]>([]);

  const handleProcess = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formValues = await form?.validateFields();
      const playersId = playersByGroup.map((d) => d.id);
      const payload = {
        players_id: playersId,
        parameters_id: [dependencyData?.vo2max_parameter.id],
        custom_parameters_id: [dependencyData?.ca_min_custom_parameter.id],
      };
      // console.log(formValues);
      const chartDataRes = await create('kpi_results/compare', payload);
      setChartData(chartDataRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Chart Data' }));
    }
    hideLoading();
  };

  const handleValuesChange = (changedValues: any, values: FormType) => {
    if (values.group_id) {
      setPlayersByGroup(() => {
        const filteredPlayer = players.filter((d) => d.kelompok_id === values.group_id);
        return filteredPlayer;
      });
    }
  };

  return (
    <Form onValuesChange={handleValuesChange} form={form} layout="horizontal">
      <Row gutter={8}>
        <Col span={6}>
          <Form.Item
            name="group_id"
            label={<FormattedMessage id="commonField.group" />}
            rules={[{ required: true, message: 'Please input players' }]}
          >
            <Select>
              {groups.map((d, i) => (
                <Select.Option value={d.id} key={i}>
                  {d.kelompok}
                </Select.Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col>
          <Form.Item>
            <Button type="primary" onClick={handleProcess}>
              <FormattedMessage id="crud.generate_report" />
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
