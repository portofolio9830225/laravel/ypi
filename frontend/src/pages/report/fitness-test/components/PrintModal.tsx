import { Drawer, Table, TableColumnsType, Space, Button, message, Descriptions } from 'antd';
import styles from './PrintModal.less';
import { getLocaleKey, Context, FitnessTestExtra, ReportType } from '../index';
import { Access, FormattedMessage, useIntl } from 'umi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { jsPDF } from 'jspdf';
const { Item } = Descriptions;
import { median } from '@/utils/calculation';
import { FooterReport } from '@/components/FooterReport';
import {
  ScatterChart,
  Scatter,
  XAxis,
  YAxis,
  ZAxis,
  CartesianGrid,
  ReferenceArea,
  Tooltip,
  ResponsiveContainer,
  Legend,
  Label,
  LabelList,
} from 'recharts';
import { scaleOrdinal } from 'd3-scale';
import { schemeCategory10 } from 'd3-scale-chromatic';

const colors = scaleOrdinal(schemeCategory10).range();

const Header = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <span className={styles.title}>Tournament Report</span>

      <Descriptions labelStyle={{ fontWeight: 'bold' }}>
        <Item label={<FormattedMessage id="commonField.date_test" />}>{printData?.date}</Item>
        <Item label={<FormattedMessage id="commonField.gender" />}>{printData?.gender}</Item>
        <Item label={<FormattedMessage id="commonField.group" />}>{printData?.group_name}</Item>
      </Descriptions>
    </div>
  );
};

const Chart = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);

  const [minX, setMinX] = useState<number>(0);
  const [minY, setMinY] = useState<number>(0);

  const [maxX, setMaxX] = useState<number>(0);
  const [maxY, setMaxY] = useState<number>(0);

  const [medianX, setMedianX] = useState<number>(0);
  const [medianY, setMedianY] = useState<number>(0);

  useEffect(() => {
    let courtAgilities: number[] = [];
    let vo2maxs: number[] = [];
    printData?.table_data.forEach((d) => {
      if (d.court_agility != 0) courtAgilities.push(d.court_agility);
      if (d.vo2max != 0) vo2maxs.push(d.vo2max);
    });

    setMedianX(median(courtAgilities) || 0);
    setMedianY(median(vo2maxs) || 0);

    setMinX(Math.min(...courtAgilities));
    setMinY(Math.min(...vo2maxs));

    setMaxX(Math.max(...courtAgilities));
    setMaxY(Math.max(...vo2maxs));
  }, [printData]);

  return (
    <>
      <ResponsiveContainer width="100%" height="100%" minHeight="22rem">
        <ScatterChart
          margin={{
            top: 20,
            right: 20,
            bottom: 20,
            left: 20,
          }}
        >
          <CartesianGrid />
          <ZAxis type="category" dataKey="player_name" name="Name" />
          <XAxis
            reversed={true}
            type="number"
            dataKey="court_agility"
            name="Best Court Agility"
            domain={['dataMin', 'dataMax']}
            tickCount={10}
            label={{ value: 'Court Agility', position: 'insideBottomRight', offset: -20 }}
          />
          <YAxis
            orientation="right"
            type="number"
            tickCount={10}
            dataKey="vo2max"
            name="VO2Max"
            domain={['dataMin', 'dataMax']}
            label={{ value: 'VO2Max', angle: 90, position: 'insideRight' }}
          />
          <Tooltip cursor={{ strokeDasharray: '3 3' }} />

          <Legend />
          <ReferenceArea
            x1={minX}
            x2={medianX}
            y1={minY}
            y2={medianY}
            stroke="red"
            strokeOpacity={0.3}
            fill="yellow"
          >
            <Label value={intl.formatMessage({ id: 'commonField.low_endurance' })} />
          </ReferenceArea>
          <ReferenceArea
            x1={minX}
            x2={medianX}
            y1={medianY}
            y2={maxY}
            stroke="red"
            strokeOpacity={0.3}
            fill="green"
          >
            <Label value={intl.formatMessage({ id: 'commonField.best' })} />
          </ReferenceArea>
          <ReferenceArea
            x1={medianX}
            x2={maxX}
            y1={medianY}
            y2={maxY}
            stroke="red"
            strokeOpacity={0.3}
            fill="yellow"
          >
            <Label value={intl.formatMessage({ id: 'commonField.low_agility' })} />
          </ReferenceArea>
          <ReferenceArea
            x1={medianX}
            x2={maxX}
            y1={minY}
            y2={medianY}
            stroke="red"
            strokeOpacity={0.3}
            fill="red"
          >
            <Label value={intl.formatMessage({ id: 'commonField.low_agility_endurance' })} />
          </ReferenceArea>
          {printData?.table_data.map((d, i) => (
            <Scatter key={i} name={d.player_name} data={[d]} fill={colors[i % colors.length]}>
              <LabelList dataKey="player_name" position="top" fill="#606060" />
            </Scatter>
          ))}
        </ScatterChart>
      </ResponsiveContainer>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </>
  );
};

const Chart2 = () => {
  const { printData } = useContext(Context);
  const [xBaseline, setXBaseline] = useState<number>(0);
  const [yBaseline, setYBaseline] = useState<number>(0);

  const [minX, setMinX] = useState<number>(0);
  const [minY, setMinY] = useState<number>(0);

  const [maxX, setMaxX] = useState<number>(0);
  const [maxY, setMaxY] = useState<number>(0);

  const [medianX, setMedianX] = useState<number>(0);
  const [medianY, setMedianY] = useState<number>(0);

  useEffect(() => {
    if (printData) {
      let benchpresses: number[] = [];
      let squats: number[] = [];
      printData.table_data.forEach((d) => {
        if (d.bench_press != 0) benchpresses.push(d.bench_press);
        if (d.squat != 0) squats.push(d.squat);
      });

      setMedianX(median(benchpresses) || 0);
      setMedianY(median(squats) || 0);

      setMinX(Math.min(...benchpresses));
      setMinY(Math.min(...squats));

      setMaxX(Math.max(...benchpresses));
      setMaxY(Math.max(...squats));
    }
  }, [printData]);

  return (
    <ResponsiveContainer width="100%" height="100%" minHeight="22rem">
      <ScatterChart
        margin={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
      >
        <CartesianGrid />
        <ZAxis type="category" dataKey="player_name" name="Name" />
        <XAxis
          type="number"
          dataKey="bench_press"
          name="Best Court Agility"
          domain={['dataMin', 'dataMax']}
          tickCount={10}
          label={{ value: 'Bench Press', position: 'insideBottomRight', offset: -20 }}
        />
        <YAxis
          orientation="right"
          type="number"
          tickCount={10}
          dataKey="squat"
          name="VO2Max"
          domain={['dataMin', 'dataMax']}
          label={{ value: 'Squat', angle: 90, position: 'insideRight' }}
        />
        <Tooltip label="-" cursor={{ strokeDasharray: '3 3' }} />

        <Legend />
        <ReferenceArea
          x1={minX}
          x2={medianX}
          y1={minY}
          y2={medianY}
          stroke="red"
          strokeOpacity={0.3}
          fill="red"
        >
          <Label value="Upper and Lower Body Weak" />
        </ReferenceArea>
        <ReferenceArea
          x1={minX}
          x2={medianX}
          y1={medianY}
          y2={maxY}
          stroke="red"
          strokeOpacity={0.3}
          fill="yellow"
        >
          <Label value="Upper Body Weak" />
        </ReferenceArea>
        <ReferenceArea
          x1={medianX}
          x2={maxX}
          y1={medianY}
          y2={maxY}
          stroke="red"
          strokeOpacity={0.3}
          fill="green"
        >
          <Label value="Best" />
        </ReferenceArea>
        <ReferenceArea
          x1={medianX}
          x2={maxX}
          y1={minY}
          y2={medianY}
          stroke="red"
          strokeOpacity={0.3}
          fill="yellow"
        >
          <Label value="Lower Body Weak" />
        </ReferenceArea>
        {printData?.table_data.map((d, i) => (
          <Scatter key={i} name={d.player_name} data={[d]} fill={colors[i % colors.length]}>
            <LabelList dataKey="player_name" position="top" fill="#606060" />
          </Scatter>
        ))}
      </ScatterChart>
    </ResponsiveContainer>
  );
};

const TableDetail = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [data, setData] = useState<FitnessTestExtra[]>([]);

  useEffect(() => {
    if (printData) {
      setData(printData.table_data.map((d, i) => ({ ...d, key: i.toString() })));
    }
  }, [printData]);

  const columns: TableColumnsType<FitnessTestExtra> = [
    {
      title: intl.formatMessage({ id: 'commonField.player_name' }),
      dataIndex: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'commonField.court_agility' }),
      dataIndex: 'court_agility',
    },
    {
      title: intl.formatMessage({ id: 'commonField.vo2max' }),
      dataIndex: 'vo2max',
    },
    {
      title: intl.formatMessage({ id: 'commonField.bench_press' }),
      dataIndex: 'bench_press',
    },
    {
      title: intl.formatMessage({ id: 'commonField.squat' }),
      dataIndex: 'squat',
    },
  ];

  const summary = (data: readonly FitnessTestExtra[]) => {
    return (
      <>
        <Table.Summary.Row style={{ fontWeight: 'bold' }}>
          <Table.Summary.Cell index={0} colSpan={6}>
            {data.length} <FormattedMessage id="commonField.player" />
          </Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  };

  return (
    <Table
      summary={summary}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      pagination={false}
    />
  );
};

const Footer = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <Descriptions labelStyle={{ fontWeight: '500' }}>
        <Item label={<FormattedMessage id={getLocaleKey('absent')} />}>
          {printData?.players_absent}
        </Item>
      </Descriptions>
    </div>
  );
};

export default () => {
  const [visible, setVisible] = useState<boolean>(false);
  const intl = useIntl();
  const { printData, setPrintData, form } = useContext(Context);
  const [reportType, setReportType] = useState<ReportType>(ReportType.ChartAndTable);

  useEffect(() => {
    if (printData) {
      setVisible(true);
      setReportType(form?.getFieldValue('report_type'));
    }
  }, [printData]);

  const handleClose = () => {
    setVisible(false);
    setPrintData(undefined);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('fitness-test-report.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1000,
      autoPaging: 'text',
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );

  const isChartVisible = (): boolean => {
    return reportType === ReportType.Chart || reportType === ReportType.ChartAndTable
      ? true
      : false;
  };

  const isTableVisible = (): boolean => {
    return reportType === ReportType.Table || reportType === ReportType.ChartAndTable
      ? true
      : false;
  };

  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={visible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <br />
          <Access accessible={isChartVisible()}>
            <Chart />
            <br />
            <Chart2 />
            <br />
          </Access>
          <Access accessible={isTableVisible()}>
            <TableDetail />
          </Access>
          <br />
          <Footer />
          <FooterReport />
        </div>
      </Drawer>
    </>
  );
};
