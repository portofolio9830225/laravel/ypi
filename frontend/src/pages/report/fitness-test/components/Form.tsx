import { Form, Select, Button, Space, Modal } from 'antd';
import { Context, ReportType, getLocaleKey } from '../index';
import { useContext } from 'react';
import { FormattedMessage, useIntl, useHistory } from 'umi';
import Table from './Table';
const { Option } = Select;

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const { form } = useContext(Context);

  const handleCancel = () => {
    history.goBack();
  };

  return (
    <Modal
      title={<FormattedMessage id="menu.report.fitness-test" />}
      centered={true}
      visible={true}
      closable={false}
      footer={
        <Space>
          <Button htmlType="button" onClick={handleCancel}>
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
        </Space>
      }
      width="80%"
    >
      <Form size="small" scrollToFirstError form={form} layout="vertical">
        <Form.Item
          label={<FormattedMessage id={'commonField.report_type'} />}
          name="report_type"
          initialValue={ReportType.Table}
        >
          <Select>
            <Option value={ReportType.Table}>
              <FormattedMessage id="commonField.table_report" />
            </Option>
            <Option value={ReportType.Chart}>
              <FormattedMessage id="commonField.chart_report" />
            </Option>
            <Option value={ReportType.ChartAndTable}>
              <FormattedMessage id="commonField.table_chart_report" />
            </Option>
          </Select>
        </Form.Item>

        <Form.Item label={<FormattedMessage id={getLocaleKey('data')} />}>
          <Table />
        </Form.Item>
      </Form>
    </Modal>
  );
};
