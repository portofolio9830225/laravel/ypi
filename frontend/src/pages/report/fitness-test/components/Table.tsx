import { Button, Table, TableColumnsType, Tooltip, message } from 'antd';
import { useIntl } from 'umi';
import { Record, Context } from '../index';
import { useContext } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { get } from '@/services/ypi/api';

export default () => {
  const intl = useIntl();
  const { tableData, setPrintData } = useContext(Context);

  const handlePrint = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const printDataRes = await get(
        `kpi_results/fitness_test/report_detail/${record.date}/${record.group_id}`,
      );
      setPrintData(printDataRes.data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const columns: TableColumnsType<Record> = [
    {
      title: intl.formatMessage({ id: 'commonField.date' }),
      dataIndex: 'date',
    },
    {
      title: intl.formatMessage({ id: 'commonField.group' }),
      dataIndex: ['group', 'kelompok'],
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      render: (_, record) => (
        <>
          <Tooltip title="Print">
            <Button
              type="primary"
              onClick={() => handlePrint(record)}
              icon={<PrinterOutlined />}
              shape="circle"
            />
          </Tooltip>
        </>
      ),
    },
  ];

  return <Table bordered size="small" columns={columns} dataSource={tableData} />;
};
