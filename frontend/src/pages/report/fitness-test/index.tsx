import { Form, FormInstance, message } from 'antd';
import { createContext, useState, useEffect } from 'react';
import FormConfiguration from './components/Form';
import { useIntl } from 'umi';
import { get } from '@/services/ypi/api';
import { FitnessTest } from '@/types/ypi';
import PrintModal from './components/PrintModal';

export interface Record {
  key: string;
  date: string;
  group_id: number;
  group: {
    id: number;
    kelompok: string;
  };
}

export enum ReportType {
  Table = 1,
  Chart,
  ChartAndTable,
}

interface FormType {
  report_type: ReportType;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.report.fitness-test.' + fieldName;
};

export interface FitnessTestExtra extends FitnessTest {
  player_name: string;
  key: string;
}

interface PrintData {
  table_data: FitnessTestExtra[];
  date: string;
  players_absent: string;
  gender: string;
  group_name: string;
}

export interface ContextType {
  form: FormInstance<FormType> | undefined;
  tableData: Record[];
  printData: PrintData | undefined;
  setPrintData: React.Dispatch<React.SetStateAction<PrintData | undefined>>;
}

export const Context = createContext<ContextType>({
  form: undefined,
  tableData: [],
  printData: undefined,
  setPrintData: () => {},
});

export default () => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const [tableData, setTableData] = useState<Record[]>([]);
  const [printData, setPrintData] = useState<PrintData>();

  const contextValue: ContextType = {
    form,
    tableData,
    printData,
    setPrintData,
  };

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const tableDataRes = await get('kpi_results/fitness_test/report_summary');
      setTableData((tableDataRes.data as Record[]).map((d, i) => ({ ...d, key: i.toString() })));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  return (
    <Context.Provider value={contextValue}>
      <FormConfiguration />
      <PrintModal />
    </Context.Provider>
  );
};
