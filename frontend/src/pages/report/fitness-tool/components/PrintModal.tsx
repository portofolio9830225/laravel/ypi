import {
  Drawer,
  Table,
  TableColumnsType,
  Space,
  Button,
  message,
  Descriptions,
  Typography,
} from 'antd';
import styles from './PrintModal.less';
import { Context, FitnessTestExtra, ReportType } from '../index';
import { Access, FormattedMessage, useIntl } from 'umi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { Scatter, ScatterConfig } from '@ant-design/plots';
import { jsPDF } from 'jspdf';
import { median } from '@/utils/calculation';
import { FooterReport } from '@/components/FooterReport';
const { Item } = Descriptions;
const { Text } = Typography;

const getAverageText = (values: number[]) => {
  if (values.length > 0) {
    const avg = values.reduce((a, b) => a + b, 0) / values.length;
    return <Text strong>{avg.toFixed(2)}</Text>;
  }
  return <></>;
};

const getStdDeviationText = (values: number[]) => {
  if (values.length > 0) {
    const n = values.length;
    const mean = values.reduce((a, b) => a + b) / n;
    const result = Math.sqrt(values.map((x) => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n);
    return <Text strong>{result.toFixed(2)}</Text>;
  }
  return <></>;
};

const getMaximumText = (values: number[]) => {
  if (values.length > 0) {
    return <Text strong>{Math.max(...values)}</Text>;
  }
  return <></>;
};

const getMinimumText = (values: number[]) => {
  if (values.length > 0) {
    return <Text strong>{Math.min(...values)}</Text>;
  }
  return <></>;
};

const Header = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <span className={styles.title}>Fitness Monitor Tool Report</span>
      <Descriptions labelStyle={{ fontWeight: 'bold' }}>
        <Item label={<FormattedMessage id="commonField.date_test" />}>{printData?.date}</Item>
        <Item label={<FormattedMessage id="commonField.gender" />}>{printData?.gender}</Item>
        <Item label={<FormattedMessage id="commonField.group" />}>{printData?.group_name}</Item>
      </Descriptions>
    </div>
  );
};

const Chart = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [xBaseline, setXBaseline] = useState<number>(0);
  const [yBaseline, setYBaseline] = useState<number>(0);

  useEffect(() => {
    if (printData) {
      let maxCas: number[] = [];
      let beepTests: number[] = [];
      printData.table_data.forEach((d) => {
        if (d.court_agility != 0) maxCas.push(d.court_agility);
        if (d.beep_test != 0) beepTests.push(d.beep_test);
      });
      setXBaseline(median(maxCas) || 0);
      setYBaseline(median(beepTests) || 0);
    }
  }, [printData]);

  const config: ScatterConfig = {
    data: printData?.table_data || [],
    xField: 'court_agility',
    yField: 'beep_test',
    colorField: 'player_name',
    shape: 'circle',
    size: 6,
    appendPadding: 16,
    theme: {
      styleSheet: {
        backgroundColor: '#ffffff',
      },
    },
    pointStyle: {
      fillOpacity: 0.8,
    },
    xAxis: {
      title: { text: 'Court Agility' },
      tickInterval: 1,
    },
    yAxis: {
      title: { text: 'VO2 Max' },
      position: 'right',
      tickInterval: 1,
    },
    label: {
      formatter: (item) => {
        return item.player_name;
      },
    },
    quadrant: {
      xBaseline,
      yBaseline,
      labels: [
        {
          content: intl.formatMessage({ id: 'commonField.low_agility' }),
          style: { fill: '#000', fillOpacity: 0.5 },
        },
        {
          content: intl.formatMessage({ id: 'commonField.best' }),
          style: { fill: '#000', fillOpacity: 0.5 },
        },
        {
          content: intl.formatMessage({ id: 'commonField.low_endurance' }),
          style: { fill: '#000', fillOpacity: 0.5 },
        },
        {
          content: intl.formatMessage({ id: 'commonField.low_agility_endurance' }),
          style: { fill: '#000', fillOpacity: 0.5 },
        },
      ],
      regionStyle: [
        {
          fill: '#ffe500',
          fillOpacity: 0.1,
        },
        {
          fill: '#5bd8a6',
          fillOpacity: 0.1,
        },
        {
          fill: '#ffe500',
          fillOpacity: 0.1,
        },
        {
          fill: '#f7664e',
          fillOpacity: 0.1,
        },
      ],
    },
  };

  return (
    <>
      <div className={styles.chart_title}>Court Agility vs Beep Test</div>
      <Scatter {...config} />
    </>
  );
};

const Chart2 = () => {
  const { printData } = useContext(Context);
  const [xBaseline, setXBaseline] = useState<number>(0);
  const [yBaseline, setYBaseline] = useState<number>(0);

  useEffect(() => {
    if (printData) {
      let benchpresses: number[] = [];
      let squats: number[] = [];
      printData.table_data.forEach((d) => {
        if (d.bench_press != 0) benchpresses.push(d.bench_press);
        if (d.squat != 0) squats.push(d.squat);
      });
      setXBaseline(median(benchpresses) || 0);
      setYBaseline(median(squats) || 0);
    }
  }, [printData]);

  const config: ScatterConfig = {
    data: printData?.table_data || [],
    xField: 'bench_press',
    yField: 'squat',
    colorField: 'player_name',
    shape: 'circle',
    size: 6,
    appendPadding: 16,
    pointStyle: {
      fillOpacity: 0.8,
    },
    theme: {
      styleSheet: {
        backgroundColor: '#ffffff',
      },
    },
    xAxis: {
      title: { text: 'Court Agility' },
      tickInterval: 1,
    },
    yAxis: {
      title: { text: 'VO2 Max' },
      position: 'right',
      tickInterval: 1,
    },
    label: {
      formatter: (item) => {
        return item.player_name;
      },
    },
    quadrant: {
      xBaseline,
      yBaseline,
      labels: [
        {
          content: 'Best',
          style: { fill: '#000', fillOpacity: 0.5 },
        },
        {
          content: 'Upper Body Weak',
          style: { fill: '#000', fillOpacity: 0.5 },
        },
        {
          content: 'Upper and Lower Body Weak',
          style: { fill: '#000', fillOpacity: 0.5 },
        },
        {
          content: 'Lower Body Weak',
          style: { fill: '#000', fillOpacity: 0.5 },
        },
      ],
      regionStyle: [
        {
          fill: '#5bd8a6',
          fillOpacity: 0.1,
        },
        {
          fill: '#ffe500',
          fillOpacity: 0.1,
        },
        {
          fill: '#f7664e',
          fillOpacity: 0.1,
        },
        {
          fill: '#ffe500',
          fillOpacity: 0.1,
        },
      ],
    },
  };

  return (
    <>
      <div className={styles.chart_title}>Benchpress vs Squat</div>
      <Scatter {...config} />
    </>
  );
};

const TableDetail = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [data, setData] = useState<FitnessTestExtra[]>([]);

  useEffect(() => {
    if (printData) {
      setData(printData.table_data.map((d, i) => ({ ...d, key: i.toString(), no: i + 1 })));
    }
  }, [printData]);

  const columns: TableColumnsType<FitnessTestExtra> = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
    },
    {
      title: intl.formatMessage({ id: 'commonField.beep_test' }),
      dataIndex: 'beep_test',
    },
    {
      title: intl.formatMessage({ id: 'commonField.court_agility' }),
      dataIndex: 'court_agility',
    },
    {
      title: intl.formatMessage({ id: 'commonField.bench_press' }),
      dataIndex: 'bench_press',
    },
    {
      title: intl.formatMessage({ id: 'commonField.squat' }),
      dataIndex: 'squat',
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.reach' }),
      dataIndex: 'reach',
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.vertical_jump' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.cj' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'cj_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'cj_elevation',
            },
          ],
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.sj' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'sq_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'sq_elevation',
            },
          ],
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.rl' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'rl_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'rl_elevation',
            },
          ],
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.ll' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'll_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'll_elevation',
            },
          ],
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.eu' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.ratio' }),
          dataIndex: 'ratio',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.leg_strength' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.r_vs_l_leg' }),
          dataIndex: 'difference',
        },
      ],
    },
  ];

  const summary = (data: readonly FitnessTestExtra[]) => {
    let t_beep_test: number[] = [];
    let t_court: number[] = [];
    let t_bench_press: number[] = [];
    let t_squat: number[] = [];
    let t_reach: number[] = [];
    let t_cj_actual: number[] = [];
    let t_cj_elevation: number[] = [];
    let t_sq_actual: number[] = [];
    let t_sq_elevation: number[] = [];
    let t_rl_actual: number[] = [];
    let t_rl_elevation: number[] = [];
    let t_ll_actual: number[] = [];
    let t_ll_elevation: number[] = [];
    let t_ratio: number[] = [];
    let t_diff: number[] = [];

    data.forEach((value) => {
      value.beep_test && t_beep_test.push(value.beep_test);
      value.court_agility && t_court.push(value.court_agility);
      value.bench_press && t_bench_press.push(value.bench_press);
      value.squat && t_squat.push(value.squat);
      value.reach && t_reach.push(value.reach);
      value.cj_actual && t_cj_actual.push(value.cj_actual);
      value.cj_elevation && t_cj_elevation.push(value.cj_elevation);
      value.sq_actual && t_sq_actual.push(value.sq_actual);
      value.sq_elevation && t_sq_elevation.push(value.sq_elevation);
      value.rl_actual && t_rl_actual.push(value.rl_actual);
      value.rl_elevation && t_rl_elevation.push(value.rl_elevation);
      value.ll_actual && t_ll_actual.push(value.ll_actual);
      value.ll_elevation && t_ll_elevation.push(value.ll_elevation);
      value.ratio && t_ratio.push(value.ratio);
      value.difference && t_diff.push(value.difference);
    });

    return (
      <Table.Summary fixed>
        <Table.Summary.Row>
          <Table.Summary.Cell index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.average" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{getAverageText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getAverageText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getAverageText(t_bench_press)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getAverageText(t_squat)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getAverageText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getAverageText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getAverageText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getAverageText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getAverageText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getAverageText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getAverageText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getAverageText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getAverageText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getAverageText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getAverageText(t_diff)}</Table.Summary.Cell>
        </Table.Summary.Row>

        <Table.Summary.Row>
          <Table.Summary.Cell index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.std_deviasi" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{getStdDeviationText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getStdDeviationText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getStdDeviationText(t_bench_press)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getStdDeviationText(t_squat)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getStdDeviationText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getStdDeviationText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getStdDeviationText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getStdDeviationText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getStdDeviationText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getStdDeviationText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getStdDeviationText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getStdDeviationText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getStdDeviationText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getStdDeviationText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getStdDeviationText(t_diff)}</Table.Summary.Cell>
        </Table.Summary.Row>

        <Table.Summary.Row>
          <Table.Summary.Cell index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.maximum" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{getMaximumText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getMaximumText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getMaximumText(t_bench_press)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getMaximumText(t_squat)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getMaximumText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getMaximumText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getMaximumText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getMaximumText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getMaximumText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getMaximumText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getMaximumText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getMaximumText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getMaximumText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getMaximumText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getMaximumText(t_diff)}</Table.Summary.Cell>
        </Table.Summary.Row>

        <Table.Summary.Row>
          <Table.Summary.Cell index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.maximum" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={1}>{getMinimumText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getMinimumText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getMinimumText(t_bench_press)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getMinimumText(t_squat)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getMinimumText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getMinimumText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getMinimumText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getMinimumText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getMinimumText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getMinimumText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getMinimumText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getMinimumText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getMinimumText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getMinimumText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getMinimumText(t_diff)}</Table.Summary.Cell>
        </Table.Summary.Row>
      </Table.Summary>
    );
  };

  return (
    <>
      <div style={{ height: '5rem' }} />
      <Table
        summary={summary}
        bordered
        columns={columns}
        dataSource={data}
        size="small"
        pagination={false}
      />
    </>
  );
};

export default () => {
  const [visible, setVisible] = useState<boolean>(false);
  const intl = useIntl();
  const { printData, setPrintData, form } = useContext(Context);
  const [reportType, setReportType] = useState<ReportType>(ReportType.ChartAndTable);

  useEffect(() => {
    if (printData) {
      setVisible(true);
      setReportType(form?.getFieldValue('report_type'));
    }
  }, [printData]);

  const handleClose = () => {
    setVisible(false);
    setPrintData(undefined);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('fitness-monitor-report.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1500,
      autoPaging: 'text',
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );

  const isChartVisible = (): boolean => {
    return reportType === ReportType.Chart || reportType === ReportType.ChartAndTable
      ? true
      : false;
  };

  const isTableVisible = (): boolean => {
    return reportType === ReportType.Table || reportType === ReportType.ChartAndTable
      ? true
      : false;
  };

  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={visible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <br />
          <Access accessible={isChartVisible()}>
            <Chart />
            <br />
            <Chart2 />
            <br />
          </Access>
          <Access accessible={isTableVisible()}>
            <TableDetail />
          </Access>
          <br />
          <FooterReport />
        </div>
      </Drawer>
    </>
  );
};
