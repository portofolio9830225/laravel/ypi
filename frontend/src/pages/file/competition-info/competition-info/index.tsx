import { Col, message, Row } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { useIntl, history, useAccess, Access } from 'umi';
export interface Item {
  key: string;
  id: number;
  nama_kategori: string;
  level_kategori_id: number;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'tabel_kategoris';

export default () => {
  const pageHeaderHeight = 250;

  const [data, setData] = useState<Item[]>([]);
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.file.competition-info.competition-info' }),
  );
  const access = useAccess();

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get(tableName);
      setData(
        dataRes.data.map((d: Item) => {
          return { ...d, key: d.id.toString() };
        }),
      );
    } catch (err: any) {
      message.error(err.response.statusText);
      if (err.response.status === 401) history.push('/user/login');
    }
    hideLoading();
  };

  useEffect(() => {
    getTableData();
  }, []);

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <div>
      <Context.Provider value={contextValue}>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px)`} />
          </Col>
          <Col span={12}>
            <Access accessible={access.canEditClub}>
              <Form />
            </Access>
          </Col>
        </Row>
      </Context.Provider>
    </div>
  );
};
