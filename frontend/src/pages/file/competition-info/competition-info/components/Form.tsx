import { Card, Form, Input, Button, message, Space, Select, Divider } from 'antd';
import { tableName, Context } from '../index';
import { create, update, get } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage, useHistory } from 'umi';
import { EditOutlined } from '@ant-design/icons';

const { Option } = Select;

export interface LevelCompetition {
  id: number;
  level: number;
}

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const history = useHistory();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [levelData, setLevelData] = useState<LevelCompetition[]>([{ id: -1, level: 0 }]);

  const labelCompDetail = intl.formatMessage({ id: 'pages.competition-info.comp-detail' });
  const labelCompLevel = intl.formatMessage({ id: 'pages.competition-info.comp-level' });

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  const onFinish = async () => {
    const values = await form.validateFields();
    if (editedData) {
      update(tableName, values.id, values)
        .then(() => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch(() => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    } else {
      create(tableName, values)
        .then(() => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch(() => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    }
    onCancel();
  };

  useEffect(() => {
    get('level_kategoris')
      .then((res) => {
        setLevelData(res.data);
      })
      .catch((err) => {
        console.log(err);
        message.error(err.response.statusText);
      });
  }, [intl]);

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
    }
  }, [form, editedData]);

  const handleOpenCompetitionLevel = () => {
    history.push('/file/competition-info/competition-level');
  };

  return (
    <Card title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}>
      <Form form={form} name="form" onFinish={onFinish} layout="vertical">
        <Form.Item name="id" hidden />
        <Form.Item
          name="nama_kategori"
          label={labelCompDetail}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: labelCompDetail },
              ),
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="level_kategori_id"
          label={labelCompLevel}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: labelCompLevel },
              ),
            },
          ]}
        >
          <Select
            dropdownRender={(menu) => (
              <>
                {menu}
                <Divider />
                <Space style={{ padding: '0 8px 4px' }}>
                  <Button
                    type="primary"
                    onClick={handleOpenCompetitionLevel}
                    icon={<EditOutlined />}
                  >
                    <FormattedMessage id="crud.editData" />
                  </Button>
                </Space>
              </>
            )}
          >
            {levelData.map((level) => (
              <Option key={level.id} value={level.id}>
                {level.level}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
