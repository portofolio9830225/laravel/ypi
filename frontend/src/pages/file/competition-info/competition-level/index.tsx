import React, { useState, createContext } from 'react';
import Table from './components/Table';
import { CompetitionLevel } from '@/types/ypi';

interface DevType {
  edited: boolean;
  key: string;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.competition-level.' + fieldName;
};

export interface Record extends CompetitionLevel, DevType {}

export interface ContextType {
  data: Partial<Record>[];
  setData: React.Dispatch<React.SetStateAction<Partial<Record>[]>>;
}

export const Context = createContext<ContextType>({
  data: [],
  setData: () => {},
});

export const tableName = 'level_kategoris';

export default () => {
  const [data, setData] = useState<Partial<Record>[]>([]);

  const contextValue: ContextType = { data, setData };

  return (
    <Context.Provider value={contextValue}>
      <Table />
      {/* <Form /> */}
    </Context.Provider>
  );
};
