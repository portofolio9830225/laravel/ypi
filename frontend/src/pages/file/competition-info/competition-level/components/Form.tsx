import { Form, InputNumber, Card, Button, message, Space } from 'antd';
import { tableName, Context, Item } from '../index';
import { create, update } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl } from 'umi';

const layout = {
  labelCol: { span: 2 },
};

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const moduleName = intl.formatMessage({ id: 'menu.file.competition-info.competition-level' });
  const { editedData, setEditedData, getTableData } = useContext(Context);
  const [openForm, setOpenForm] = useState<boolean>(false);

  const onFinish = (values: Item) => {
    if (editedData) {
      update(tableName, values.id, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    } else {
      create(tableName, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    }
    onCancel();
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
      setOpenForm(true);
    }
  }, [editedData]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
    setOpenForm(false);
  };

  const onOpenForm = () => {
    setOpenForm(true);
  };

  if (!openForm) {
    return (
      <Button style={{ marginTop: '1rem' }} type="primary" htmlType="button" onClick={onOpenForm}>
        {intl.formatMessage({ id: 'crud.addNewData' })}
      </Button>
    );
  }

  return (
    <Card
      style={{ marginTop: '1rem' }}
      title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
    >
      <Form {...layout} form={form} name="form" onFinish={onFinish} layout="horizontal">
        <Form.Item name="id" label={intl.formatMessage({ id: 'commonField.id' })}>
          <InputNumber disabled />
        </Form.Item>
        <Form.Item
          name="level"
          label={intl.formatMessage({ id: 'pages.competition-level.level' })}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: intl.formatMessage({ id: 'pages.competition-level.level' }) },
              ),
            },
          ]}
        >
          <InputNumber min={0} />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 2 }}>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
