import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage, useAccess } from 'umi';
import { getLocaleKey, Context, Record } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { create, destroy, get, update } from '@/services/ypi/api';
import styles from './table.less';

const TABLE_NAME = 'level_kategoris';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
}

const EditableContext = createContext<FormInstance<Record> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof Record | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: Record;
  handleToggleEdit: (form: FormInstance<Record> | null, record: Record) => void;
  handleSave: (form: FormInstance<Record> | null, record: Record) => Promise<void>;
  handleDelete: (record: Record) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Id) {
    return (
      <td {...props}>
        {
          <>
            <Form.Item hidden name={dataIndex} />
            {childNode}
          </>
        }
      </td>
    );
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;

    rules = getRequiredRule('level');

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const { data, setData } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const access = useAccess();

  const fetchData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get(TABLE_NAME);
      const d: Record[] = dataRes.data;
      setData(
        d.map((d, i) => {
          return { ...d, key: i.toString() };
        }),
      );
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.id' }),
      dataIndex: 'id',
      inputType: InputType.Id,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('level') }),
      dataIndex: 'level',
      inputType: InputType.Number,
    },
    ...(access.canEditClub
      ? [
          {
            title: intl.formatMessage({ id: 'crud.action' }),
            dataIndex: 'action',
            inputType: InputType.None,
            width: '20%',
          },
        ]
      : []),
  ];

  const handleToggleEdit = (form: FormInstance<Record> | null, record: Record) => {
    const editedDataIndex = data.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...data];
    currentData[editedDataIndex] = editedData;
    setData(currentData);
  };

  const handleSave = async (form: FormInstance<Record> | null, record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = data.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();

      let savedData = {};
      if (values && values.id) {
        const updateRes = await update(TABLE_NAME, values.id, values);
        savedData = updateRes.data;
      } else {
        const createRes = await create(TABLE_NAME, values);
        savedData = createRes.data;
      }

      let currentData = [...data];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      hideLoading();
      setIsEditMode(false);
      setData(currentData);
    } catch (error) {
      setIsEditMode(false);
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = data.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy(TABLE_NAME, record.id);
      }
      let currentData = [...data];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Record) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    const newData = [...data];
    newData.push({
      id: undefined,
      edited: true,
      key: newData.length.toString(),
    });
    setIsEditMode(!isEditMode);
    setData([...newData]);
  };

  const footer = () => {
    if (isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      className={styles.custom_table}
      components={components}
      bordered
      dataSource={data}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={access.canEditClub ? footer : undefined}
      rowClassName={styles.densed_row}
      pagination={isEditMode ? false : { size: 'small' }}
    />
  );
};

export default EditableTable;
