export interface Item {
  key: string;
  id: number;
  keterangan: string;
  kode_pelatih: number;
  voltarget: number;
  tanggal_mulai: string;
  tanggal_selesai: string;
  default_intensity: number;
}
