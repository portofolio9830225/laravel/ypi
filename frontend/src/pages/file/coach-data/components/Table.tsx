import { Table, Input, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import type { InputRef } from 'antd';
import type { FilterConfirmProps } from 'antd/lib/table/interface';
import type { ColumnsType, ColumnType } from 'antd/lib/table';
import Highlighter from 'react-highlight-words';
import { SearchOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext, useState, useRef, useEffect } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, Item } from '../index';
import { useAccess, useIntl } from 'umi';
import styles from './table.less';
import moment from 'moment';

type DataIndex = keyof Item;

export default ({ data }: { data: Item[] }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);
  const [searchText, setSearchText] = useState<string>('');
  const [searchedColumn, setSearchedColumn] = useState<string>('');
  const searchInput = useRef<InputRef>(null);
  const access = useAccess();

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then(() => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: DataIndex,
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex: DataIndex): ColumnType<Item> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Search"
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <Button
        type={filtered ? 'primary' : 'default'}
        size="small"
        shape="circle"
        icon={<SearchOutlined />}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns: ColumnsType<Item> = [
    {
      title: '# ID',
      dataIndex: 'id',
      key: 'id',
      width: 80,
      fixed: 'left',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.nomor_ic' }),
      dataIndex: 'nomor_ic',
      key: 'nomor_ic',
      width: 100,
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.nama_lengkap' }),
      dataIndex: 'nama_lengkap',
      key: 'nama_lengkap',
      width: 200,
      fixed: 'left',
      ...getColumnSearchProps('nama_lengkap'),
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.alamat' }),
      dataIndex: 'alamat',
      key: 'alamat',
      width: 250,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.negara' }),
      dataIndex: 'negara',
      key: 'negara',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.kodepos' }),
      dataIndex: 'kodepos',
      key: 'kodepos',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.tempat_lahir' }),
      dataIndex: 'tempat_lahir',
      key: 'tempat_lahir',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.tanggal_lahir' }),
      dataIndex: 'tanggal_lahir',
      key: 'tanggal_lahir',
      width: 150,
      render: (_, record) => <span>{moment(record.tanggal_lahir).format('YYYY-MM-DD')}</span>,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.home_phone' }),
      dataIndex: 'home_phone',
      key: 'home_phone',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.hand_phone' }),
      dataIndex: 'hand_phone',
      key: 'hand_phone',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.business_phone' }),
      dataIndex: 'business_phone',
      key: 'business_phone',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.email' }),
      dataIndex: 'email',
      key: 'email',
      width: 200,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.golongan_darah' }),
      dataIndex: 'golongan_darah',
      key: 'golongan_darah',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.keterangan' }),
      dataIndex: 'keterangan',
      key: 'keterangan',
      width: 200,
    },
    ...(access.canEditClub
      ? [
          {
            title: intl.formatMessage({ id: 'crud.action' }),
            key: 'action',
            fixed: 'right' as any,
            width: 100,
            render: (_: any, record: Item) => (
              <Space>
                <Tooltip title="Edit">
                  <Button
                    size="small"
                    onClick={() => onEdit(record)}
                    type="primary"
                    shape="circle"
                    icon={<EditOutlined />}
                  />
                </Tooltip>
                <Tooltip title="Delete">
                  <Popconfirm
                    title={intl.formatMessage(
                      { id: 'message.confirmDeleteMessage' },
                      { moduleName },
                    )}
                    onConfirm={() => onDelete(record)}
                    okText={intl.formatMessage({ id: 'crud.yes' })}
                    cancelText={intl.formatMessage({ id: 'crud.no' })}
                  >
                    <Button
                      size="small"
                      type="primary"
                      danger
                      shape="circle"
                      icon={<DeleteOutlined />}
                    />
                  </Popconfirm>
                </Tooltip>
              </Space>
            ),
          },
        ]
      : []),
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      scroll={{ x: 'max-content' }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
