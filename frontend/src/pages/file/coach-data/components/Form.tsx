import { Form, DatePicker, Select, Input, Drawer, Button, message, Space, Row, Col } from 'antd';
import { tableName, Context } from '../index';
import { create, update } from '@/services/ypi/api';
import { useContext, useEffect } from 'react';
import { useIntl } from 'umi';
import moment from 'moment';
const { TextArea } = Input;
const { Option } = Select;

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);

  const lblFullName = intl.formatMessage({ id: 'pages.coach-data.nama_lengkap' });

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  const onFinish = async () => {
    try {
      const values = await form.validateFields();

      if (editedData) {
        update(tableName, values.id, values)
          .then(() => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch(() => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      } else {
        create(tableName, values)
          .then(() => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch(() => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      }
      onCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue({ ...editedData, tanggal_lahir: moment(editedData.tanggal_lahir) });
    }
  }, [editedData]);

  return (
    <Drawer
      title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
      placement="bottom"
      visible={true}
      mask={false}
      closable={false}
      headerStyle={{ padding: '6px 12px' }}
      height={'40%'}
      extra={
        <Space>
          <Button type="primary" htmlType="button" onClick={onFinish}>
            {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
          </Button>
          <Button
            type="primary"
            htmlType="button"
            onClick={onCancel}
            style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
          >
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
        </Space>
      }
    >
      <Form
        scrollToFirstError
        form={form}
        name="form"
        onFinish={onFinish}
        layout="horizontal"
        labelCol={{ span: 6 }}
      >
        <Form.Item name="id" hidden />
        <Row gutter={[16, 16]}>
          <Col span={8}>
            <Form.Item
              name="nama_lengkap"
              label={lblFullName}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblFullName },
                  ),
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="alamat" label={intl.formatMessage({ id: 'pages.coach-data.alamat' })}>
              <TextArea rows={4} />
            </Form.Item>
            <Form.Item name="negara" label={intl.formatMessage({ id: 'pages.coach-data.negara' })}>
              <Input />
            </Form.Item>
            <Form.Item
              name="kodepos"
              label={intl.formatMessage({ id: 'pages.coach-data.kodepos' })}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="nomor_ic"
              label={intl.formatMessage({ id: 'pages.coach-data.nomor_ic' })}
            >
              <Input />
            </Form.Item>
            <Form.Item name="email" label={intl.formatMessage({ id: 'pages.coach-data.email' })}>
              <Input type="email" />
            </Form.Item>
            <Form.Item
              name="golongan_darah"
              label={intl.formatMessage({ id: 'pages.coach-data.golongan_darah' })}
            >
              <Select>
                <Option value="A">A</Option>
                <Option value="A+">A+</Option>
                <Option value="B">B</Option>
                <Option value="B+">B+</Option>
                <Option value="AB">AB</Option>
                <Option value="AB+">AB+</Option>
                <Option value="O">O</Option>
                <Option value="O+">O+</Option>
              </Select>
            </Form.Item>
            <Form.Item
              name="tempat_lahir"
              label={intl.formatMessage({ id: 'pages.coach-data.tempat_lahir' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="tanggal_lahir"
              label={intl.formatMessage({ id: 'pages.coach-data.tanggal_lahir' })}
            >
              <DatePicker />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="home_phone"
              label={intl.formatMessage({ id: 'pages.coach-data.home_phone' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="hand_phone"
              label={intl.formatMessage({ id: 'pages.coach-data.hand_phone' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="business_phone"
              label={intl.formatMessage({ id: 'pages.coach-data.business_phone' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="keterangan"
              label={intl.formatMessage({ id: 'pages.coach-data.keterangan' })}
            >
              <TextArea rows={4} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};
