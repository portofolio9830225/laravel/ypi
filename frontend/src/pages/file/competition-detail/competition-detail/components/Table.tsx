import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message, Input } from 'antd';
import { EditOutlined, DeleteOutlined, SearchOutlined } from '@ant-design/icons';
import { useContext, useEffect, useRef, useState } from 'react';
import { destroy, get } from '@/services/ypi/api';
import { tableName, Context } from '../index';
import { useIntl } from 'umi';
import Highlighter from 'react-highlight-words';
import type { FilterConfirmProps } from 'antd/lib/table/interface';
import type { InputRef } from 'antd';
import type { ColumnType } from 'antd/lib/table';
import styles from './table.less';
import { CompetitionDetail as Item, GroupClassification } from '@/types/ypi';

type DataIndex = keyof Item;
const {} = Table;

export default ({ data, tableHeight }: { data: Item[]; tableHeight: string }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);
  const [searchText, setSearchText] = useState<string>('');
  const [searchedColumn, setSearchedColumn] = useState<string>('');
  const [groupData, setGroupData] = useState<GroupClassification[]>([]);
  const searchInput = useRef<InputRef>(null);
  const [filtersAgeGroup, setFiltersAgeGroup] = useState<string>('');

  const fetchDependencyData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const groupRes = await get('tabel_kelompoks');
      setGroupData(groupRes.data);
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    fetchDependencyData();
  }, []);

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: DataIndex,
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex: DataIndex): ColumnType<Item> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Search"
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <Button
        type={filtered ? 'primary' : 'default'}
        size="small"
        shape="circle"
        icon={<SearchOutlined />}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns: TableColumnsType<Item> = [
    {
      title:
        intl.formatMessage({ id: 'pages.competition-detail.group-classification' }) +
        ` ${filtersAgeGroup}`,
      dataIndex: 'group_classifications',
      width: 150,
      filters: groupData.map((d) => {
        return { value: d.kelompok, text: d.kelompok };
      }),
      onFilter: (value: string | number | boolean, record: Item) =>
        record.group_classifications.includes(value as string),
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-detail.nama_event' }),
      dataIndex: 'nama_event',
      key: 'nama_event',
      // fixed: 'left',
      width: 200,
      ...getColumnSearchProps('nama_event'),
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-detail.kategori_id' }),
      dataIndex: ['competition_info', 'nama_kategori'],
      key: 'kategori_id',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-detail.tanggal_mulai' }),
      dataIndex: 'tanggal_mulai',
      key: 'tanggal_mulai',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-detail.tanggal_akhir' }),
      dataIndex: 'tanggal_akhir',
      key: 'tanggal_akhir',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-detail.competition_status_id' }),
      dataIndex: ['competition_status', 'status'],
      key: 'competition_status_id',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-detail.color' }),
      dataIndex: 'color',
      key: 'color',
      width: 100,
      render: (_, record) => (
        <div style={{ backgroundColor: record.color, width: '100%', height: '2rem' }} />
      ),
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      width: '10%',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="link"
              size="small"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="text" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      onChange={(paginatin, filters) => {
        if (filters.group_classifications && filters.group_classifications.length > 0) {
          setFiltersAgeGroup(`(${filters.group_classifications.join()})`);
        } else {
          setFiltersAgeGroup('');
        }
      }}
      size="small"
      scroll={{ y: tableHeight, x: 'max-content' }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
