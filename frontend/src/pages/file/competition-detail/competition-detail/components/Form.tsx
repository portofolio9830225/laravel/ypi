import {
  Form,
  Input,
  Card,
  Button,
  message,
  Space,
  Select,
  DatePicker,
  Row,
  Col,
  Divider,
} from 'antd';
import { tableName, Context } from '../index';
import { create, update, get } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, useHistory, FormattedMessage } from 'umi';
import { Item as CompetitionInfoType } from '../../../competition-info/competition-info';
import { Item as CompetitionStatusType } from '../../competition-status';
import moment, { Moment } from 'moment';
import { RangeValue } from 'rc-picker/lib/interface';
import { CompetitionDetail, GroupClassification } from '@/types/ypi';
import { PrinterOutlined, EditOutlined } from '@ant-design/icons';

const { Option } = Select;
const { RangePicker } = DatePicker;

export default () => {
  const [form] = Form.useForm<CompetitionDetail>();
  const intl = useIntl();
  const history = useHistory();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [competitionInfoData, setCompetitionInfoData] = useState<CompetitionInfoType[]>([]);
  const [competitionStatusData, setCompetitionStatusData] = useState<CompetitionStatusType[]>([]);
  const [groupData, setGroupData] = useState<GroupClassification[]>([]);

  const lblTournamentName = intl.formatMessage({
    id: 'pages.competition-detail.nama_event',
  });

  const onFinish = async () => {
    console.log(form.getFieldsValue());
    const values = await form.validateFields();
    // return;
    if (editedData) {
      update(tableName, values.id, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    } else {
      create(tableName, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    }
    onCancel();
  };

  const getDependencyData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const competitionInfoRes = await get('tabel_kategoris');
      setCompetitionInfoData(competitionInfoRes.data);
      const competitionStatusRes = await get('competition_statuses');
      setCompetitionStatusData(competitionStatusRes.data);
      const groupRes = await get('tabel_kelompoks');
      setGroupData(groupRes.data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    getDependencyData();
  }, []);

  useEffect(() => {
    if (editedData) {
      let dates = {};
      if (editedData.tanggal_mulai) {
        dates = {
          tanggal_mulai: moment(editedData.tanggal_mulai),
          tanggal_akhir: moment(editedData.tanggal_akhir),
          date: [moment(editedData.tanggal_mulai), moment(editedData.tanggal_akhir)],
        };
      }
      form.setFieldsValue({
        ...editedData,
        ...dates,
      });
    }
  }, [editedData, form]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  const handlePrint = () => {
    history.push('/report/tournament-detail');
  };

  const onDateRangeChange = (date: RangeValue<Moment>, dateString: [string, string]) => {
    form.setFieldsValue({ tanggal_mulai: dateString[0], tanggal_akhir: dateString[1] });
  };

  const handleOpenCompetitionDetail = () => {
    history.push('/file/competition-info/competition-info');
  };

  const handleOpenCompetitionStatus = () => {
    history.push('/file/competition-detail/competition-status');
  };

  return (
    <Card
      title={
        <span style={{ padding: 0, margin: 0 }}>
          {intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
        </span>
      }
    >
      <Form form={form} name="form" layout="vertical" initialValues={{ groups_id: [] }}>
        <Form.Item name="id" hidden />
        <Form.Item name="tanggal_mulai" hidden />
        <Form.Item name="tanggal_akhir" hidden />
        <Row gutter={6}>
          <Col span={12}>
            <Form.Item
              name="nama_event"
              label={lblTournamentName}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblTournamentName },
                  ),
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="kategori_id"
              label={intl.formatMessage({ id: 'pages.competition-detail.kategori_id' })}
            >
              <Select
                dropdownRender={(menu) => (
                  <>
                    {menu}
                    <Divider />
                    <Space style={{ padding: '0 8px 4px' }}>
                      <Button
                        type="primary"
                        onClick={handleOpenCompetitionDetail}
                        icon={<EditOutlined />}
                      >
                        <FormattedMessage id="crud.editData" />
                      </Button>
                    </Space>
                  </>
                )}
              >
                {competitionInfoData.map((compInfo) => (
                  <Option key={compInfo.id} value={compInfo.id}>
                    {compInfo.nama_kategori}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={6}>
          <Col span={12}>
            <Form.Item
              name="date"
              label={intl.formatMessage({ id: 'pages.competition-detail.tanggal_akhir' })}
            >
              <RangePicker onChange={onDateRangeChange} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="competition_status_id"
              label={intl.formatMessage({ id: 'pages.competition-detail.competition_status_id' })}
            >
              <Select
                dropdownRender={(menu) => (
                  <>
                    {menu}
                    <Divider />
                    <Space style={{ padding: '0 8px 4px' }}>
                      <Button
                        type="primary"
                        onClick={handleOpenCompetitionStatus}
                        icon={<EditOutlined />}
                      >
                        <FormattedMessage id="crud.editData" />
                      </Button>
                    </Space>
                  </>
                )}
              >
                {competitionStatusData.map((compStatus) => (
                  <Option key={compStatus.id} value={compStatus.id}>
                    {compStatus.status}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={6}>
          <Col span={12}>
            <Form.Item
              name="color"
              label={intl.formatMessage({ id: 'pages.competition-detail.color' })}
              initialValue="#38e1ff"
            >
              <Input type="color" />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="groups_id"
              label={intl.formatMessage({ id: 'pages.competition-detail.group-classification' })}
            >
              <Select mode="multiple">
                {groupData.map((d, i) => (
                  <Option key={i} value={d.id}>
                    {d.kelompok}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="button" onClick={onFinish}>
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
            <Button htmlType="button" onClick={handlePrint} icon={<PrinterOutlined />}>
              {intl.formatMessage({ id: 'crud.print' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
