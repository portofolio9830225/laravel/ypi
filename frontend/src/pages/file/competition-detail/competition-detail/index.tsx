import { Col, message, Row } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { useIntl } from 'umi';
import { CompetitionDetail } from '@/types/ypi';

export interface ContextType {
  editedData: Partial<CompetitionDetail> | null;
  setEditedData:
    | React.Dispatch<React.SetStateAction<Partial<CompetitionDetail> | null>>
    | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'master_achievements';

export default () => {
  const pageHeaderHeight = 250;
  const [data, setData] = useState<CompetitionDetail[]>([]);
  const [editedData, setEditedData] = useState<Partial<CompetitionDetail> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.file.competition-detail.competition-detail' }),
  );

  useEffect(() => {
    getTableData();
  }, []);

  useEffect(() => {
    console.log(editedData);
  }, [editedData]);

  const getTableData = () => {
    get(tableName)
      .then((res) => {
        const fetchedData: CompetitionDetail[] = res.data;
        setData(
          fetchedData.map((d: CompetitionDetail) => {
            let groups_id: number[] | undefined = [];
            const competitionGroup = d.competition_group;
            if (competitionGroup) {
              if (competitionGroup.groups_id) {
                groups_id = competitionGroup.groups_id.split(',').map((cg) => parseInt(cg));
              }
            }
            return {
              ...d,
              key: d.id.toString(),
              tanggal_mulai: (d.tanggal_mulai as string).startsWith('-')
                ? undefined
                : (d.tanggal_mulai as string).substring(0, 10),
              tanggal_akhir: (d.tanggal_akhir as string).startsWith('-')
                ? undefined
                : (d.tanggal_akhir as string).substring(0, 10),
              groups_id: groups_id || [],
            };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
        console.log(err);
      });
  };

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <div id="content-container">
      <Context.Provider value={contextValue}>
        <Row gutter={[16, 16]}>
          <Col span={12}>
            <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px)`} />
          </Col>
          <Col span={12}>
            <Form />
          </Col>
        </Row>
      </Context.Provider>
    </div>
  );
};
