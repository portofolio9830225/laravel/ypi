import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext, useEffect, useState } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, Item } from '../index';
import { useAccess, useIntl } from 'umi';
import styles from './table.less';

export default ({ data, tableHeight }: { data: Item[]; tableHeight: string }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);
  const access = useAccess();

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'pages.group-classification.classification' }),
      dataIndex: 'kelompok',
      key: 'kelompok',
    },
    {
      title: intl.formatMessage({ id: 'pages.group-classification.age_min' }),
      dataIndex: 'age_min',
    },
    {
      title: intl.formatMessage({ id: 'pages.group-classification.age_max' }),
      dataIndex: 'age_max',
    },
    ...(access.canAdmin
      ? [
          {
            title: intl.formatMessage({ id: 'crud.action' }),
            key: 'action',
            width: '20%',
            render: (_: any, record: Item) => (
              <Space>
                <Tooltip title="Edit">
                  <Button
                    onClick={() => onEdit(record)}
                    type="primary"
                    shape="circle"
                    size="small"
                    icon={<EditOutlined />}
                  />
                </Tooltip>
                <Tooltip title="Delete">
                  <Popconfirm
                    title={intl.formatMessage(
                      { id: 'message.confirmDeleteMessage' },
                      { moduleName },
                    )}
                    onConfirm={() => onDelete(record)}
                    okText={intl.formatMessage({ id: 'crud.yes' })}
                    cancelText={intl.formatMessage({ id: 'crud.no' })}
                  >
                    <Button
                      type="primary"
                      size="small"
                      danger
                      shape="circle"
                      icon={<DeleteOutlined />}
                    />
                  </Popconfirm>
                </Tooltip>
              </Space>
            ),
          },
        ]
      : []),
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      size="small"
      scroll={{ y: tableHeight }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
      columns={columns}
      dataSource={data}
    />
  );
};
