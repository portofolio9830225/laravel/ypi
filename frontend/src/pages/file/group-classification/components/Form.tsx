import { Form, Input, Card, Button, message, Space, Row, Col, InputNumber } from 'antd';
import { tableName, Context, Item } from '../index';
import { create, update } from '@/services/ypi/api';
import { useContext, useEffect } from 'react';
import { FormattedMessage, useIntl } from 'umi';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const minAge = Form.useWatch('age_min', form);
  const maxAge = Form.useWatch('age_max', form);

  const labelClassification = intl.formatMessage({
    id: 'pages.group-classification.classification',
  });

  const onFinish = (values: Item) => {
    if (editedData) {
      update(tableName, values.id, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    } else {
      create(tableName, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    }
    onCancel();
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
    }
  }, [editedData]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  return (
    <Card title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}>
      <Form form={form} name="form" onFinish={onFinish} layout="vertical">
        <Form.Item name="id" hidden />
        <Row gutter={6}>
          <Col span={8}>
            <Form.Item
              name="kelompok"
              label={labelClassification}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: labelClassification },
                  ),
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="age_min"
              label={<FormattedMessage id="pages.group-classification.age_min" />}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: 'Min Age' },
                  ),
                },
                {
                  type: 'number',
                  max: maxAge,
                  message: intl.formatMessage({ id: 'message.must_less' }, { variable: 'Max Age' }),
                },
                {
                  type: 'number',
                  min: 1,
                  message: intl.formatMessage({ id: 'message.must_greater' }, { variable: '0' }),
                },
              ]}
            >
              <InputNumber style={{ width: '100%' }} />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="age_max"
              label={<FormattedMessage id="pages.group-classification.age_max" />}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: 'Max Age' },
                  ),
                },
                {
                  type: 'number',
                  min: minAge,
                  message: intl.formatMessage(
                    { id: 'message.must_greater' },
                    { variable: 'Min Age' },
                  ),
                },
                {
                  type: 'number',
                  min: 1,
                  message: intl.formatMessage({ id: 'message.must_greater' }, { variable: '0' }),
                },
              ]}
            >
              <InputNumber style={{ width: '100%' }} />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
