import { message, Row, Col } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { Access, useAccess, useIntl } from 'umi';

export interface Item {
  key: string;
  id: number;
  kelompok: string;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'tabel_kelompoks';

export default () => {
  const pageHeaderHeight = 250;
  const [data, setData] = useState<Item[]>([]);
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const access = useAccess();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.file.group-classification' }),
  );

  useEffect(() => {
    getTableData();
  }, []);

  const getTableData = () => {
    get(tableName)
      .then((res) => {
        const fetchedData: Item[] = res.data;
        setData(
          fetchedData.map((d: Item) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <Context.Provider value={contextValue}>
      <Row gutter={[16, 16]}>
        <Col span={12}>
          <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px)`} />
        </Col>
        <Col span={12}>
          <Access accessible={access.canAdmin}>
            <Form />
          </Access>
        </Col>
      </Row>
    </Context.Provider>
  );
};
