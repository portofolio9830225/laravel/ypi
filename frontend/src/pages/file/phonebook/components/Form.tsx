import { Form, Input, Button, message, Space, InputNumber, Row, Col, Drawer } from 'antd';
import { tableName, Context } from '../index';
import { create, update } from '@/services/ypi/api';
import { useContext, useEffect } from 'react';
import { useIntl } from 'umi';
const { TextArea } = Input;

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);

  const lblFullName = intl.formatMessage({ id: 'pages.phonebook.fullName' });
  const lblAdress = intl.formatMessage({ id: 'pages.phonebook.address' });
  const lblCountry = intl.formatMessage({ id: 'pages.phonebook.country' });
  const lblPostalCode = intl.formatMessage({ id: 'pages.phonebook.postalCode' });
  const lblPhone = intl.formatMessage({ id: 'pages.phonebook.phone' });
  const lblEmail = intl.formatMessage({ id: 'pages.phonebook.email' });
  const lblRemarks = intl.formatMessage({ id: 'pages.phonebook.remarks' });
  const lblFh = intl.formatMessage({ id: 'pages.phonebook.fh' });
  const lblFw = intl.formatMessage({ id: 'pages.phonebook.fw' });
  const lblMh = intl.formatMessage({ id: 'pages.phonebook.mh' });
  const lblMw = intl.formatMessage({ id: 'pages.phonebook.mw' });

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  const onFinish = async () => {
    try {
      const values = await form.validateFields();
      if (editedData) {
        update(tableName, values.id, values)
          .then(() => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch(() => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      } else {
        create(tableName, values)
          .then(() => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch(() => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      }
      onCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
    }
  }, [editedData, form]);

  return (
    <Drawer
      title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
      placement="bottom"
      visible={true}
      mask={false}
      closable={false}
      headerStyle={{ padding: '6px 12px' }}
      height={'40%'}
      extra={
        <Space>
          <Button type="primary" htmlType="button" onClick={onFinish}>
            {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
          </Button>
          <Button
            type="primary"
            htmlType="button"
            onClick={onCancel}
            style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
          >
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
        </Space>
      }
    >
      <Form labelCol={{ span: 6 }} form={form} name="form" onFinish={onFinish}>
        <Form.Item name="id" hidden />
        <Row gutter={8}>
          <Col span={8}>
            <Form.Item
              name="nama_lengkap"
              label={lblFullName}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblFullName },
                  ),
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="telepon" label={lblPhone}>
              <Input />
            </Form.Item>
            <Form.Item name="email" label={lblEmail}>
              <Input type="email" />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="alamat" label={lblAdress}>
              <TextArea rows={3} />
            </Form.Item>
            <Form.Item name="kodepos" label={lblPostalCode}>
              <Input />
            </Form.Item>
            <Form.Item name="negara" label={lblCountry}>
              <Input />
            </Form.Item>
          </Col>

          <Col span={8}>
            <Form.Item name="keterangan" label={lblRemarks}>
              <TextArea rows={4} />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={12}>
          <Col>
            <Form.Item
              labelCol={{ span: 16 }}
              name="father_height"
              label={lblFh}
              rules={[{ type: 'number', min: 0, max: 999 }]}
            >
              <InputNumber />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              labelCol={{ span: 16 }}
              name="father_weight"
              label={lblFw}
              rules={[{ type: 'number', min: 0, max: 999 }]}
            >
              <InputNumber />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              labelCol={{ span: 16 }}
              name="mother_height"
              label={lblMh}
              rules={[{ type: 'number', min: 0, max: 999 }]}
            >
              <InputNumber />
            </Form.Item>
          </Col>
          <Col>
            <Form.Item
              labelCol={{ span: 16 }}
              name="mother_weight"
              label={lblMw}
              rules={[{ type: 'number', min: 0, max: 999 }]}
            >
              <InputNumber />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};
