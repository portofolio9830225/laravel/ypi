import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { Access, useAccess, useIntl } from 'umi';

export interface Item {
  key: string;
  id: number;
  nama_lengkap: string;
  alamat: string;
  negara: string;
  kodepos: string;
  telepon: string;
  email: string;
  keterangan: string;
  father_height: number;
  father_weight: number;
  mother_height: number;
  mother_weight: number;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'master_contacts';

export default () => {
  // const pageHeaderHeight = 250;

  const [data, setData] = useState<Item[]>([]);
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const access = useAccess();
  const [moduleName] = useState<string>(intl.formatMessage({ id: 'menu.file.phonebook' }));

  useEffect(() => {
    getTableData();
  }, []);

  const getTableData = () => {
    get(tableName)
      .then((res) => {
        const fetchedData: Item[] = res.data;
        setData(
          fetchedData.map((d: Item) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <Context.Provider value={contextValue}>
      {/* <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px - 384px)`} /> */}
      <Table data={data} />
      <Access accessible={access.canEditClub}>
        <div style={{ height: '50vh' }} />
        <Form />
      </Access>
    </Context.Provider>
  );
};
