import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import TablePlayer from './components/TablePlayer';
import TableCoach from './components/TableCoach';
import { useIntl } from 'umi';
import { Coach, Player } from '@/types/ypi';
import { message, Row, Col } from 'antd';

export interface Item extends Player {
  age: number;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);

  coachData: Coach[];
  moduleName: string;
  selectedCoachId: number | undefined;
  selectedPlayerId: number | undefined;
  setSelectedCoachId: React.Dispatch<React.SetStateAction<number | undefined>>;
  setSelectedPlayerId: React.Dispatch<React.SetStateAction<number | undefined>>;
  dataPemain: Item[];
  setDataPemain: React.Dispatch<React.SetStateAction<Item[]>>;
  dataPlayer: Player[];
  setDataPlayer: React.Dispatch<React.SetStateAction<Player[]>>;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},

  coachData: [],
  moduleName: '',
  selectedCoachId: undefined,
  selectedPlayerId: undefined,
  setSelectedCoachId: () => {},
  setSelectedPlayerId: () => {},
  dataPemain: [],
  setDataPemain: () => {},
  dataPlayer: [],
  setDataPlayer: () => {},
});

export default () => {
  const [coachData, setCoachData] = useState<Coach[]>([]);
  const [selectedCoachId, setSelectedCoachId] = useState<number>();
  const [selectedPlayerId, setSelectedPlayerId] = useState<number>();
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const [dataPemain, setDataPemain] = useState<Item[]>([]);
  const [dataPlayer, setDataPlayer] = useState<Player[]>([]);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.file.group-player-by-coach' }),
  );

  const getCoachData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    await get('master_pelatihs')
      .then((res) => {
        const fetchedData: Coach[] = res.data;
        setCoachData(
          fetchedData.map((d: Coach) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
    hideLoading();
  };

  useEffect(() => {
    getCoachData();
  }, []);

  const contextValue: ContextType = {
    editedData,
    setEditedData,

    coachData,
    moduleName,
    selectedCoachId,
    selectedPlayerId,
    setSelectedCoachId,
    setSelectedPlayerId,
    dataPemain,
    setDataPemain,
    dataPlayer,
    setDataPlayer,
  };

  return (
    <Context.Provider value={contextValue}>
      {/* <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px - 384px)`} /> */}
      {/* <div style={{ display: 'flex', justifyContent: 'space-between' }}> */}
      <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
        <Col span={12}>
          <TableCoach />
        </Col>
        <Col span={12}>{selectedCoachId && <TablePlayer data={dataPemain} />}</Col>
      </Row>

      {/* </div> */}

      <div style={{ height: '50vh' }} />
      {/* <Form /> */}
    </Context.Provider>
  );
};
