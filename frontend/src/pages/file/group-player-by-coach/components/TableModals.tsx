import { Input, Table, Button, Space, message } from 'antd';
import type { InputRef } from 'antd';
import type { FilterConfirmProps } from 'antd/lib/table/interface';
import type { ColumnsType, ColumnType } from 'antd/lib/table';
import Highlighter from 'react-highlight-words';
import { SearchOutlined } from '@ant-design/icons';
import { useContext, useEffect, useRef, useState } from 'react';
import { Context, Item } from '../index';
import { useIntl } from 'umi';
import styles from './table.less';
import { Player } from '@/types/ypi';
import { create, get } from '@/services/ypi/api';

type DataIndex = keyof Item;

export default ({ dataPlayer }: { dataPlayer: Player[] }) => {
  const intl = useIntl();
  const { editedData, setSelectedPlayerId, selectedCoachId, setDataPlayer, moduleName } =
    useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);
  const [searchText, setSearchText] = useState<string>('');
  const [searchedColumn, setSearchedColumn] = useState<string>('');

  const searchInput = useRef<InputRef>(null);

  const assignPemain = async (record: Player) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    setSelectedPlayerId(record.id);
    await create('master_pemains/pelatih', { pelatih_id: selectedCoachId, pemain_id: record.id });

    hideLoading();
    get('master_pemains/not_coach/' + selectedCoachId)
      .then((res) => {
        const fetchedData: Player[] = res.data;
        setDataPlayer(
          fetchedData.map((d: Player) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: DataIndex,
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex: DataIndex): ColumnType<Item> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Search"
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <Button
        type={filtered ? 'primary' : 'default'}
        size="small"
        shape="circle"
        icon={<SearchOutlined />}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns: ColumnsType<Player> = [
    {
      title: '# ID',
      dataIndex: 'id',
      key: 'id',
      width: 80,
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.id - b.id,
    },

    {
      title: intl.formatMessage({ id: 'pages.player-data.nama_lengkap' }),
      dataIndex: 'nama_lengkap',
      key: 'nama_lengkap',
      width: 200,
      ...getColumnSearchProps('nama_lengkap'),
    },
    // {
    //   title: intl.formatMessage({ id: 'commonField.age' }),
    //   dataIndex: 'age',
    //   key: 'age',
    //   width: 100,
    // },
    {
      title: intl.formatMessage({ id: 'pages.player-data.kelompok_id' }),
      dataIndex: ['group', 'kelompok'],
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'commonField.coach_name' }),
      dataIndex: ['coach', 'nama_lengkap'],
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      width: 100,
      render: (_, record) => (
        <Button type="primary" size="small" onClick={() => assignPemain(record)}>
          Assign
        </Button>
      ),
    },
  ];

  return (
    <>
      <div>
        <Table
          className={styles.custom_table}
          pagination={{ showSizeChanger: true }}
          bordered
          columns={columns}
          dataSource={dataPlayer}
          size="small"
          scroll={{ x: 'max-content' }}
          rowClassName={(record: Item) =>
            `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
          }
        />
      </div>
    </>
  );
};
