import { Table, Button, message } from 'antd';

import type { ColumnsType } from 'antd/lib/table';

import { useContext, useState, useEffect } from 'react';
import { get } from '@/services/ypi/api';
import { Context } from '../index';
import { useIntl } from 'umi';
import styles from './table.less';
import { Coach, Player } from '@/types/ypi';

export default () => {
  const intl = useIntl();
  const { editedData, coachData, moduleName, setSelectedCoachId, setDataPemain } =
    useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);

  const getDataPemain = async (record: Coach) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    setSelectedCoachId(record.id);
    await get('master_pemains/by_coach/' + record.id)
      .then((res) => {
        const fetchedData: Player[] = res.data;
        setDataPemain(
          fetchedData.map((d: Player) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
    hideLoading();
  };

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const columns: ColumnsType<Coach> = [
    {
      title: '# ID',
      dataIndex: 'id',
      key: 'id',
      width: 80,
      fixed: 'left',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.nomor_ic' }),
      dataIndex: 'nomor_ic',
      key: 'nomor_ic',
      width: 100,
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-data.nama_lengkap' }),
      dataIndex: 'nama_lengkap',
      key: 'nama_lengkap',
      width: 160,
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      width: 100,
      render: (_, record) => (
        <Button type="primary" size="small" onClick={() => getDataPemain(record)}>
          Detail
        </Button>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={coachData}
      size="small"
      scroll={{ x: 'max-content' }}
      rowClassName={(record: Coach) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
