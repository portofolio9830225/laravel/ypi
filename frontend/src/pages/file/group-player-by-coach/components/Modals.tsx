import { Button, Modal, message } from 'antd';
import React, { useState, useContext } from 'react';
import TableModals from './TableModals';
import { get } from '@/services/ypi/api';
import { Player } from '@/types/ypi';
import { Context } from '../index';
import { Access, useAccess, useIntl } from 'umi';

const App: React.FC = () => {
  const [open, setOpen] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);
  const intl = useIntl();
  const { selectedCoachId, dataPlayer, setDataPlayer, setDataPemain, moduleName } =
    useContext(Context);
  const access = useAccess();

  const showModal = () => {
    setOpen(true);
    get('master_pemains/not_coach/' + selectedCoachId)
      .then((res) => {
        const fetchedData: Player[] = res.data;
        setDataPlayer(
          fetchedData.map((d: Player) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  const handleOk = async () => {
    setConfirmLoading(true);
    setTimeout(() => {
      setOpen(false);
      setConfirmLoading(false);
    }, 2000);
    await get('master_pemains/by_coach/' + selectedCoachId).then((res) => {
      const fetchedData: Player[] = res.data;
      setDataPemain(
        fetchedData.map((d: Player) => {
          return { ...d, key: d.id.toString() };
        }),
      );
    });
  };

  const handleCancel = () => {
    console.log('Clicked cancel button');
    setOpen(false);
  };

  return (
    <>
      <Access accessible={access.canEditClub}>
        <Button type="primary" onClick={showModal} style={{ width: '30%' }}>
          + Add Player
        </Button>
      </Access>
      <Modal
        centered={true}
        width={'100%'}
        title="Add Player"
        visible={open}
        onOk={handleOk}
        confirmLoading={confirmLoading}
        onCancel={handleCancel}
      >
        <TableModals dataPlayer={dataPlayer} />
      </Modal>
    </>
  );
};

export default App;
