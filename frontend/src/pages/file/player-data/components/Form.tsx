import {
  Form,
  Drawer,
  DatePicker,
  Checkbox,
  Select,
  Input,
  Button,
  message,
  Space,
  InputNumber,
  Row,
  Col,
} from 'antd';
import { tableName, Context } from '../index';
import { create, get, update } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, useHistory } from 'umi';
import moment from 'moment';
const { TextArea } = Input;
const { Option } = Select;
import type { Item as ContactType } from '../../phonebook';
import type { Item as GroupType } from '../../group-classification';
import { PhoneOutlined, PrinterOutlined } from '@ant-design/icons';

const layout = {
  labelCol: { span: 6 },
  //wrapperCol: { span: 12 },
};

const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const history = useHistory();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [contactData, setContactData] = useState<ContactType[]>([]);
  const [groupData, setGroupData] = useState<GroupType[]>([]);

  const lblFullName = intl.formatMessage({ id: 'pages.player-data.nama_lengkap' });

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  const onFinish = async () => {
    try {
      const values = await form.validateFields();
      if (editedData) {
        update(tableName, values.id, values)
          .then(() => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch(() => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      } else {
        create(tableName, values)
          .then(() => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch(() => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      }
      onCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  useEffect(() => {
    get('master_contacts')
      .then((res) => {
        setContactData(res.data);
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errGet' }, { moduleName: 'Contacts' }));
      });
    get('tabel_kelompoks')
      .then((res) => {
        setGroupData(res.data);
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errGet' }, { moduleName: 'Group' }));
      });
  }, []);

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue({ ...editedData, tanggal_lahir: moment(editedData.tanggal_lahir) });
    }
  }, [editedData]);

  const handlePrint = () => {
    history.push('/report/data-player');
  };

  return (
    <Drawer
      title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
      placement="bottom"
      visible={true}
      mask={false}
      closable={false}
      headerStyle={{ padding: '6px 12px' }}
      height={'40%'}
      extra={
        <Space>
          <Button type="primary" htmlType="button" onClick={onFinish}>
            {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
          </Button>
          <Button
            type="primary"
            htmlType="button"
            onClick={onCancel}
            style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
          >
            {intl.formatMessage({ id: 'crud.cancel' })}
          </Button>
          <Button htmlType="button" onClick={handlePrint} icon={<PrinterOutlined />}>
            {intl.formatMessage({ id: 'crud.print' })}
          </Button>
        </Space>
      }
    >
      <Form
        {...layout}
        labelAlign="left"
        size="small"
        scrollToFirstError
        form={form}
        name="form"
        onFinish={onFinish}
        layout="horizontal"
        labelWrap
      >
        <Form.Item name="id" hidden />
        <Row gutter={[8, 8]}>
          <Col span={6}>
            <Form.Item
              name="nomor_ic"
              label={intl.formatMessage({ id: 'pages.player-data.nomor_ic' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="nama_lengkap"
              label={lblFullName}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblFullName },
                  ),
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item name="alamat" label={intl.formatMessage({ id: 'pages.player-data.alamat' })}>
              <TextArea rows={3} />
            </Form.Item>
            <Form.Item label={intl.formatMessage({ id: 'pages.player-data.negara' })} name="negara">
              <Input />
            </Form.Item>
            <Form.Item
              name="kodepos"
              label={intl.formatMessage({ id: 'pages.player-data.kodepos' })}
            >
              <InputNumber controls={false} />
            </Form.Item>
          </Col>
          <Col span={7}>
            <Form.Item
              labelCol={{ span: 5 }}
              name="tempat_lahir"
              label={intl.formatMessage({ id: 'pages.player-data.tempat_lahir' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              labelCol={{ span: 5 }}
              name="tanggal_lahir"
              label={intl.formatMessage({ id: 'pages.player-data.tanggal_lahir' })}
            >
              <DatePicker />
            </Form.Item>
            <Form.Item
              labelCol={{ span: 5 }}
              style={{ marginBottom: '0px' }}
              label={intl.formatMessage({ id: 'pages.player-data.sex' })}
            >
              <Row gutter={8}>
                <Col span={8}>
                  <Form.Item noStyle name="sex">
                    <Select>
                      <Option value="Male">Male</Option>
                      <Option value="Female">Female</Option>
                    </Select>
                  </Form.Item>
                </Col>
                <Col flex={'auto'}>
                  <Form.Item
                    name="golongan_darah"
                    label={intl.formatMessage({ id: 'pages.player-data.golongan_darah' })}
                  >
                    <Select>
                      <Option value="A">A</Option>
                      <Option value="A+">A+</Option>
                      <Option value="B">B</Option>
                      <Option value="B+">B+</Option>
                      <Option value="AB">AB</Option>
                      <Option value="AB+">AB+</Option>
                      <Option value="O">O</Option>
                      <Option value="O+">O+</Option>
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item
              labelCol={{ span: 5 }}
              style={{ marginBottom: '0px' }}
              label={intl.formatMessage({ id: 'pages.player-data.kelompok_id' })}
            >
              <Row gutter={16}>
                <Col span={10}>
                  <Form.Item name="kelompok_id">
                    <Select>
                      {groupData.map((contact) => (
                        <Option key={contact.id} value={contact.id}>
                          {contact.kelompok}
                        </Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
                <Col>
                  <Form.Item
                    name="aktif"
                    label={intl.formatMessage({ id: 'pages.player-data.aktif' })}
                    valuePropName="checked"
                  >
                    <Checkbox />
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item style={{ marginBottom: 0 }}>
              <Row>
                <Col span={8}>
                  <Form.Item name="tunggal_putra" valuePropName="checked">
                    <Checkbox>
                      {intl.formatMessage({ id: 'pages.player-data.tunggal_putra' })}
                    </Checkbox>
                  </Form.Item>
                </Col>
                <Col span={16}>
                  <Form.Item name="tunggal_putri" valuePropName="checked">
                    <Checkbox>
                      {intl.formatMessage({ id: 'pages.player-data.tunggal_putri' })}
                    </Checkbox>
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item name="ganda_putra" valuePropName="checked">
                    <Checkbox>
                      {intl.formatMessage({ id: 'pages.player-data.ganda_putra' })}
                    </Checkbox>
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item name="ganda_putri" valuePropName="checked">
                    <Checkbox>
                      {intl.formatMessage({ id: 'pages.player-data.ganda_putri' })}
                    </Checkbox>
                  </Form.Item>
                </Col>
                <Col span={8}>
                  <Form.Item name="ganda_campuran" valuePropName="checked">
                    <Checkbox>
                      {intl.formatMessage({ id: 'pages.player-data.ganda_campuran' })}
                    </Checkbox>
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item name="email" label={intl.formatMessage({ id: 'pages.player-data.email' })}>
              <Input type="email" />
            </Form.Item>
            <Form.Item
              name="home_phone"
              label={intl.formatMessage({ id: 'pages.player-data.home_phone' })}
            >
              <Input prefix={<PhoneOutlined />} />
            </Form.Item>
            <Form.Item
              name="hand_phone"
              label={intl.formatMessage({ id: 'pages.player-data.hand_phone' })}
            >
              <Input prefix={<PhoneOutlined />} />
            </Form.Item>
            <Form.Item
              name="business_phone"
              label={intl.formatMessage({ id: 'pages.player-data.business_phone' })}
            >
              <Input prefix={<PhoneOutlined />} />
            </Form.Item>
            <Form.Item
              name="contact_id"
              label={intl.formatMessage({ id: 'pages.player-data.contact_id' })}
            >
              <Select>
                {contactData.map((contact) => (
                  <Option key={contact.id} value={contact.id}>
                    {contact.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="nama_sekolah"
              label={intl.formatMessage({ id: 'pages.player-data.nama_sekolah' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              style={{ marginBottom: 0 }}
              label={intl.formatMessage({ id: 'pages.player-data.kelas' })}
            >
              <Row gutter={16}>
                <Col span={12}>
                  <Form.Item name="kelas">
                    <Input />
                  </Form.Item>
                </Col>
                <Col span={12}>
                  <Form.Item
                    name="no_ruang"
                    label={intl.formatMessage({ id: 'pages.player-data.no_ruang' })}
                  >
                    <Input />
                  </Form.Item>
                </Col>
              </Row>
            </Form.Item>
            <Form.Item
              name="wali_kelas"
              label={intl.formatMessage({ id: 'pages.player-data.wali_kelas' })}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="keterangan"
              label={intl.formatMessage({ id: 'pages.player-data.keterangan' })}
            >
              <TextArea rows={4} />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Drawer>
  );
};
