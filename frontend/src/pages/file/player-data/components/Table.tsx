import { Input, Table, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import type { InputRef } from 'antd';
import type { FilterConfirmProps } from 'antd/lib/table/interface';
import type { ColumnsType, ColumnType } from 'antd/lib/table';
import Highlighter from 'react-highlight-words';
import {
  SearchOutlined,
  EditOutlined,
  DeleteOutlined,
  CheckOutlined,
  CloseOutlined,
} from '@ant-design/icons';
import { useContext, useEffect, useRef, useState } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, Item } from '../index';
import { useAccess, useIntl } from 'umi';
import styles from './table.less';
import moment from 'moment';

type DataIndex = keyof Item;

export default ({ data }: { data: Item[] }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);
  const [searchText, setSearchText] = useState<string>('');
  const [searchedColumn, setSearchedColumn] = useState<string>('');
  const searchInput = useRef<InputRef>(null);
  const access = useAccess();

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then(() => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: DataIndex,
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex: DataIndex): ColumnType<Item> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Search"
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <Button
        type={filtered ? 'primary' : 'default'}
        size="small"
        shape="circle"
        icon={<SearchOutlined />}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns: ColumnsType<Item> = [
    {
      title: '# ID',
      dataIndex: 'id',
      key: 'id',
      width: 80,
      fixed: 'left',
      defaultSortOrder: 'descend',
      sorter: (a, b) => a.id - b.id,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.nomor_ic' }),
      dataIndex: 'nomor_ic',
      key: 'nomor_ic',
      width: 100,
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.nama_lengkap' }),
      dataIndex: 'nama_lengkap',
      key: 'nama_lengkap',
      width: 200,
      fixed: 'left',
      ...getColumnSearchProps('nama_lengkap'),
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.alamat' }),
      dataIndex: 'alamat',
      key: 'alamat',
      width: 250,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.negara' }),
      dataIndex: 'negara',
      key: 'negara',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.kodepos' }),
      dataIndex: 'kodepos',
      key: 'kodepos',
      width: 80,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.tempat_lahir' }),
      dataIndex: 'tempat_lahir',
      key: 'tempat_lahir',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.tanggal_lahir' }),
      dataIndex: 'tanggal_lahir',
      key: 'tanggal_lahir',
      width: 150,
      render: (_, record) => <span>{moment(record.tanggal_lahir).format('YYYY-MM-DD')}</span>,
    },
    {
      title: intl.formatMessage({ id: 'commonField.age' }),
      dataIndex: 'age',
      key: 'age',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.kelompok_id' }),
      dataIndex: ['group', 'kelompok'],
      key: 'kelompok_id',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.home_phone' }),
      dataIndex: 'home_phone',
      key: 'home_phone',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.hand_phone' }),
      dataIndex: 'hand_phone',
      key: 'hand_phone',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.business_phone' }),
      dataIndex: 'business_phone',
      key: 'business_phone',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.email' }),
      dataIndex: 'email',
      key: 'email',
      width: 200,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.golongan_darah' }),
      dataIndex: 'golongan_darah',
      key: 'golongan_darah',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.nama_sekolah' }),
      dataIndex: 'nama_sekolah',
      key: 'nama_sekolah',
      width: 250,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.kelas' }),
      dataIndex: 'kelas',
      key: 'kelas',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.wali_kelas' }),
      dataIndex: 'wali_kelas',
      key: 'wali_kelas',
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.no_ruang' }),
      dataIndex: 'no_ruang',
      key: 'no_ruang',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.contact_id' }),
      dataIndex: ['contact', 'nama_lengkap'],
      key: 'contact_id',
      width: 200,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.aktif' }),
      dataIndex: 'aktif',
      key: 'aktif',
      width: 80,
      render: (_, record) => {
        if (record.aktif) {
          return <CheckOutlined />;
        } else {
          return <CloseOutlined />;
        }
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.sex' }),
      dataIndex: 'sex',
      key: 'sex',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.tunggal_putra' }),
      dataIndex: 'tunggal_putra',
      key: 'tunggal_putra',
      width: 80,
      render: (_, record) => {
        if (record.tunggal_putra) {
          return <CheckOutlined />;
        }
        return <></>;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.tunggal_putri' }),
      dataIndex: 'tunggal_putri',
      key: 'tunggal_putri',
      width: 80,
      render: (_, record) => {
        if (record.tunggal_putri) {
          return <CheckOutlined />;
        }
        return <></>;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.ganda_putra' }),
      dataIndex: 'ganda_putra',
      key: 'ganda_putra',
      width: 80,
      render: (_, record) => {
        if (record.ganda_putra) {
          return <CheckOutlined />;
        }
        return <></>;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.ganda_putri' }),
      dataIndex: 'ganda_putri',
      key: 'ganda_putri',
      width: 80,
      render: (_, record) => {
        if (record.ganda_putri) {
          return <CheckOutlined />;
        }
        return <></>;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.ganda_campuran' }),
      dataIndex: 'ganda_campuran',
      key: 'ganda_putri',
      width: 80,
      render: (_, record) => {
        if (record.ganda_campuran) {
          return <CheckOutlined />;
        }
        return <></>;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.player-data.keterangan' }),
      dataIndex: 'keterangan',
      key: 'keterangan',
      width: 200,
    },
    ...(access.canEditClub
      ? [
          {
            title: intl.formatMessage({ id: 'crud.action' }),
            key: 'action',
            fixed: 'right' as any,
            width: 100,
            render: (_: any, record: Item) => (
              <Space>
                <Tooltip title="Edit">
                  <Button
                    size="small"
                    onClick={() => onEdit(record)}
                    type="primary"
                    shape="circle"
                    icon={<EditOutlined />}
                  />
                </Tooltip>
                <Tooltip title="Delete">
                  <Popconfirm
                    title={intl.formatMessage(
                      { id: 'message.confirmDeleteMessage' },
                      { moduleName },
                    )}
                    onConfirm={() => onDelete(record)}
                    okText={intl.formatMessage({ id: 'crud.yes' })}
                    cancelText={intl.formatMessage({ id: 'crud.no' })}
                  >
                    <Button
                      type="primary"
                      size="small"
                      danger
                      shape="circle"
                      icon={<DeleteOutlined />}
                    />
                  </Popconfirm>
                </Tooltip>
              </Space>
            ),
          },
        ]
      : []),
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      scroll={{ x: 'max-content' }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
