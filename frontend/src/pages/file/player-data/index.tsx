import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { Access, useAccess, useIntl } from 'umi';
import { Player } from '@/types/ypi';
import moment from 'moment';

export interface Item extends Player {
  age: number;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'master_pemains';

export default () => {
  const [data, setData] = useState<Item[]>([]);
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(intl.formatMessage({ id: 'menu.file.player-data' }));
  const access = useAccess();

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const playersRes = await get(tableName);
      const playersData: Item[] = playersRes.data;
      setData(
        playersData.map((d: Item) => {
          const bodString = d.tanggal_lahir?.substring(0, 10);
          return {
            ...d,
            key: d.id.toString(),
            tanggal_lahir: bodString,
            age: moment().diff(bodString, 'y'),
          };
        }),
      );
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    getTableData();
  }, []);

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <Context.Provider value={contextValue}>
      {/* <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px - 384px)`} /> */}
      <Table data={data} />
      <Access accessible={access.canEditClub}>
        <div style={{ height: '50vh' }} />
        <Form />
      </Access>
    </Context.Provider>
  );
};
