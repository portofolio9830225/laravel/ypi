import {
  Button,
  Modal,
  Form,
  Row,
  Col,
  Checkbox,
  DatePicker,
  Input,
  InputNumber,
  message,
} from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context, tableName } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { MedicalRecord } from '@/types/ypi';
import moment from 'moment';
import { create, update } from '@/services/ypi/api';

export default () => {
  const { keyData, moduleName, getTableData, editedData } = useContext(Context);
  const [form] = Form.useForm();
  const intl = useIntl();
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: (
          <FormattedMessage
            id="message.pleaseInputField"
            values={{ fieldName: intl.formatMessage({ id: 'pages.medical-record.' + fieldName }) }}
          />
        ),
      },
    ];
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    form.resetFields();
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue({ ...editedData, tanggal_moment: moment(editedData.tanggal) });
      openModal();
    }
  }, [editedData]);

  const handleSave = async () => {
    try {
      const values: MedicalRecord = await form.validateFields();
      values.pemain_id = keyData?.pemain_id as number;
      values.master_ypi_id = keyData?.master_ypi_id as number;
      if (values.id) {
        await update(tableName, values.id, values);
      } else {
        await create(tableName, values);
      }
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
      getTableData(keyData?.pemain_id as number, keyData?.master_ypi_id as number);
      closeModal();
    } catch (error) {
      console.log(error);

      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  // useEffect(() => {
  //   console.log(activeTabKey);
  // }, [activeTabKey]);

  const onValuesChange = (changedValues: any, values: MedicalRecord) => {
    if (changedValues.tanggal_moment) {
      form.setFieldsValue({
        tanggal: values.tanggal_moment?.format('YYYY-MM-DD'),
        hari: values.tanggal_moment?.format('ddd'),
      });
    }
  };

  const modalFooter = [
    <Button key="cancel" onClick={closeModal}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button key="next" type="primary" htmlType="button" onClick={handleSave}>
      <FormattedMessage id="crud.save" />
    </Button>,
  ];

  return (
    <div>
      {keyData && (
        <Button type="primary" htmlType="button" onClick={openModal}>
          <FormattedMessage id="crud.addNewData" />
        </Button>
      )}
      <Modal
        title={<FormattedMessage id="crud.addNewData" />}
        visible={modalVisible}
        footer={modalFooter}
        width="70%"
        onCancel={closeModal}
        centered
        bodyStyle={{ overflowY: 'scroll', maxHeight: '80%' }}
      >
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          name="form"
          form={form}
          layout="horizontal"
          onValuesChange={onValuesChange}
        >
          <Form.Item name="id" hidden />
          <Row gutter={12}>
            <Col span={12}>
              <Form.Item
                name="tanggal_moment"
                label={<FormattedMessage id="pages.medical-record.date" />}
                rules={getRequiredRule('date')}
              >
                <DatePicker />
              </Form.Item>

              <Form.Item name="tanggal" hidden />

              <Form.Item name="hari" hidden />

              <Form.Item
                name="injured"
                valuePropName="checked"
                label={<FormattedMessage id="pages.medical-record.injured" />}
              >
                <Checkbox />
              </Form.Item>

              <Form.Item
                name="primary_injured"
                label={<FormattedMessage id="pages.medical-record.primary_injured" />}
              >
                <Input />
              </Form.Item>
              <Form.Item
                name="detail_injured"
                label={<FormattedMessage id="pages.medical-record.detail_injured" />}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="treatment_injured"
                valuePropName="checked"
                label={<FormattedMessage id="pages.medical-record.treatment_injured" />}
              >
                <Checkbox />
              </Form.Item>

              <Form.Item
                name="injured_days"
                label={<FormattedMessage id="pages.medical-record.injured_days" />}
              >
                <InputNumber type="number" controls={false} />
              </Form.Item>

              <Form.Item
                name="treatment_injured_days"
                label={<FormattedMessage id="pages.medical-record.treatment_injured_days" />}
              >
                <InputNumber type="number" controls={false} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="illied"
                valuePropName="checked"
                label={<FormattedMessage id="pages.medical-record.illied" />}
              >
                <Checkbox />
              </Form.Item>

              <Form.Item
                name="primary_illied"
                label={<FormattedMessage id="pages.medical-record.primary_illied" />}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="symptomps"
                label={<FormattedMessage id="pages.medical-record.symtomps" />}
              >
                <Input />
              </Form.Item>

              <Form.Item
                name="treatment_illied"
                valuePropName="checked"
                label={<FormattedMessage id="pages.medical-record.treatment_illied" />}
              >
                <Checkbox />
              </Form.Item>

              <Form.Item
                name="illied_days"
                label={<FormattedMessage id="pages.medical-record.illied_days" />}
              >
                <InputNumber type="number" controls={false} />
              </Form.Item>

              <Form.Item
                name="treatment_illied_days"
                label={<FormattedMessage id="pages.medical-record.treatment_illied_days" />}
              >
                <InputNumber type="number" controls={false} />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};
