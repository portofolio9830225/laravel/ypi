import { Form, Input, Card, Row, Col, message, Select } from 'antd';
import { Context } from '../index';
import { useContext, useEffect, useState } from 'react';
import { useAccess, useIntl } from 'umi';
import { MedicalRecord, Player, Ypi } from '@/types/ypi';
const { Option } = Select;
import { get } from '@/services/ypi/api';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const access = useAccess();
  const { getTableData } = useContext(Context);
  const [playerData, setPlayerData] = useState<Player[]>([]);
  const [ypiData, setYpiData] = useState<Ypi[]>([]);

  const lblPlayerName = intl.formatMessage({ id: 'pages.medical-record.player_name' });
  const lblYpi = intl.formatMessage({ id: 'pages.medical-record.ypi' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    try {
      let playerRes;
      if (access.coachId) {
        playerRes = await get('master_pemains?coach_id=' + access.coachId);
      } else {
        playerRes = await get('master_pemains');
      }
      setPlayerData(playerRes.data);
      const ypiRes = await get('master_ypis');
      setYpiData(ypiRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const onValuesChange = (changedValues: any, values: MedicalRecord) => {
    // console.log('changedValues', changedValues);
    // console.log('values', values);

    if (values.master_ypi_id && values.pemain_id) {
      getTableData(values.pemain_id, values.master_ypi_id);
    }

    if (changedValues.master_ypi_id) {
      const selectedYpiData = ypiData.find((d) => d.id == changedValues.master_ypi_id);
      form.setFieldsValue({
        start_date: selectedYpiData?.tanggal_mulai.substring(0, 10),
        end_date: selectedYpiData?.tanggal_selesai.substring(0, 10),
      });
    }
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
        <Row gutter={12}>
          <Col span={6}>
            <Form.Item
              name="pemain_id"
              label={lblPlayerName}
              rules={getRequiredRule(lblPlayerName)}
            >
              <Select>
                {playerData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="master_ypi_id" label={lblYpi} rules={getRequiredRule(lblYpi)}>
              <Select>
                {ypiData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.keterangan}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="start_date"
              label={intl.formatMessage({ id: 'pages.medical-record.time_schedule' })}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="end_date"
              label={intl.formatMessage({ id: 'pages.medical-record.end' })}
            >
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
