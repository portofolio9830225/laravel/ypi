import {
  Table,
  TableColumnsType,
  Button,
  Space,
  Tooltip,
  Popconfirm,
  message,
  Typography,
} from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context } from '../index';
import { useIntl } from 'umi';
import { MedicalRecord as Item } from '@/types/ypi';
import { CheckOutlined } from '@ant-design/icons';
import styles from './table.less';

const { Text } = Typography;

export default ({ data }: { data: Item[] }) => {
  const intl = useIntl();
  const { setEditedData, getTableData, moduleName, keyData } = useContext(Context);

  const onEdit = (record: Item) => {
    setEditedData(record);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData(keyData?.pemain_id as number, keyData?.master_ypi_id as number);
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      key: 'no',
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.date' }),
      dataIndex: 'tanggal',
      key: 'tanggal',
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.hari' }),
      dataIndex: 'hari',
      key: 'hari',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.injured' }),
      dataIndex: 'injured',
      key: 'injured',
      render: (_, record) => record.injured && <CheckOutlined />,
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.primary_injured' }),
      dataIndex: 'primary_injured',
      key: 'primary_injured',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.detail_injured' }),
      dataIndex: 'detail_injured',
      key: 'detail_injured',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.treatment_injured' }),
      dataIndex: 'treatment_injured',
      key: 'treatment_injured',
      render: (_, record) => record.treatment_injured && <CheckOutlined />,
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.injured_days' }),
      dataIndex: 'injured_days',
      key: 'injured_days',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.treatment_injured_days' }),
      dataIndex: 'treatment_injured_days',
      key: 'treatment_injured_days',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.illied' }),
      dataIndex: 'illied',
      key: 'illied',
      render: (_, record) => record.illied && <CheckOutlined />,
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.primary_illied' }),
      dataIndex: 'primary_illied',
      key: 'primary_illied',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.symtomps' }),
      dataIndex: 'symtomps',
      key: 'symtomps',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.treatment_illied' }),
      dataIndex: 'treatment_illied',
      key: 'treatment_illied',
      render: (_, record) => record.treatment_illied && <CheckOutlined />,
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.illied_days' }),
      dataIndex: 'illied_days',
      key: 'illied_days',
    },
    {
      title: intl.formatMessage({ id: 'pages.medical-record.treatment_illied_days' }),
      dataIndex: 'treatment_illied_days',
      key: 'treatment_illied_days',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
              size="small"
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  const summary = (data: readonly Item[]) => {
    let totalInjuredDays = 0;
    let totalTreatmentDays = 0;
    let totalSickDays = 0;
    let totalTrainingAbsent = 0;

    data.forEach((value) => {
      totalInjuredDays += value.injured_days;
      totalTreatmentDays += value.treatment_injured_days;
      totalSickDays += value.illied_days;
      totalTrainingAbsent += value.treatment_illied_days;
    });

    return (
      <Table.Summary fixed>
        <Table.Summary.Row>
          <Table.Summary.Cell index={0}>
            <Text strong>Total</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={1} />

          <Table.Summary.Cell index={2} colSpan={5} />

          <Table.Summary.Cell index={7}>
            <Text strong>{totalInjuredDays}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={8}>
            <Text strong>{totalTreatmentDays}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={9} colSpan={4} />

          <Table.Summary.Cell index={13}>
            <Text strong>{totalSickDays}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={14}>
            <Text strong>{totalTrainingAbsent}</Text>
          </Table.Summary.Cell>

          <Table.Summary.Cell index={15} />
        </Table.Summary.Row>
      </Table.Summary>
    );
  };

  return (
    <Table
      size="small"
      bordered
      columns={columns}
      scroll={{ x: true }}
      dataSource={data}
      summary={summary}
      className={styles.custom_table}
    />
  );
};
