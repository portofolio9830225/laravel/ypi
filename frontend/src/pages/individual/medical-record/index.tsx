import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import KeyForm from './components/KeyForm';
import { useIntl } from 'umi';
import { MedicalRecord as Item } from '@/types/ypi';
import moment from 'moment';
moment.locale('en-us');

export interface ContextType {
  keyData: Partial<Item> | undefined;
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: (playerId: number, ypiId: number) => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  keyData: undefined,
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'medical_records';

export default () => {
  const [data, setData] = useState<Item[]>([]);
  const [keyData, setKeyData] = useState<Partial<Item> | undefined>();
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.individual.medical-record' }),
  );

  useEffect(() => {
    console.log(editedData);
  }, [editedData]);

  const getTableData = (playerId: number, ypiId: number) => {
    get(`${tableName}?master_ypi_id=${ypiId}&pemain_id=${playerId}`)
      .then((res) => {
        const fetchedData: Item[] = res.data;
        setData(
          fetchedData.map((d: Item, index) => {
            return {
              ...d,
              key: d.id.toString(),
              no: index + 1,
              tanggal: d.tanggal.substring(0, 10),
              hari: moment(d.tanggal).format('ddd'),
            };
          }),
        );
        setKeyData({ pemain_id: playerId, master_ypi_id: ypiId });
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    moduleName,
    keyData,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm />
      <Table data={data} />
      <Form />
    </Context.Provider>
  );
};
