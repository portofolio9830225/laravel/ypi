import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { useAccess, useIntl } from 'umi';
import PrintModal from './components/PrintModal';
import {
  CompetitionDetail,
  CompetitionReport,
  GroupClassification,
  Player,
  ResultCategory,
} from '@/types/ypi';

export interface CompetitionReportExtra extends CompetitionReport {
  player: Player;
  result_category: ResultCategory;
  partner_name?: string;
  age_group_name?: string;
  event_level_name: string;
  competition_info: CompetitionDetail;
  specialist_string: string;
}

export interface ContextType {
  editedData: Partial<CompetitionReportExtra> | undefined;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<CompetitionReportExtra> | undefined>>;
  getTableData: () => void;
  moduleName: string;
  printData: CompetitionReportExtra | undefined;
  setPrintData: React.Dispatch<React.SetStateAction<CompetitionReportExtra | undefined>>;
  playerData: Player[];
}

export const Context = createContext<ContextType>({
  editedData: undefined,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
  printData: undefined,
  setPrintData: () => {},
  playerData: [],
});

export const tableName = 'competition_reports';

export const getLocaleKey = (fieldName: string) => {
  return 'pages.competition-report.' + fieldName;
};

export default () => {
  const [data, setData] = useState<CompetitionReportExtra[]>([]);
  const [editedData, setEditedData] = useState<Partial<CompetitionReportExtra>>();
  const [printData, setPrintData] = useState<CompetitionReportExtra>();
  const [playerData, setPlayerData] = useState<Player[]>([]);
  const intl = useIntl();
  const access = useAccess();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.individual.competition-report.competition-report' }),
  );

  useEffect(() => {
    getTableData();
  }, []);

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const competitionReportRes = await get(tableName);
      let playerRes;
      if (access.coachId) {
        playerRes = await get('master_pemains?coach_id=' + access.coachId);
      } else {
        playerRes = await get('master_pemains');
      }
      const playerData: Player[] = playerRes.data;
      setPlayerData(playerData);
      const groupRes = await get('tabel_kelompoks');
      const groupData: GroupClassification[] = groupRes.data;

      setData(
        (competitionReportRes.data as CompetitionReportExtra[]).map((d, i) => {
          let partnerName = undefined;
          let groupName = undefined;
          let eventLevelName = 'Qualifiying';

          if (d.partner_id) {
            const partnerData = playerData.find((dd) => dd.id == d.partner_id);
            if (partnerData) partnerName = partnerData.nama_lengkap;
          }
          if (d.event_level == 'main_draw') {
            eventLevelName = 'Main Draw';
          }
          const currentGroupData = groupData.find((dd) => dd.id == d.kelompok_id);
          if (currentGroupData) groupName = currentGroupData.kelompok;

          return {
            ...d,
            key: i.toString(),
            specialist_string: intl.formatMessage({
              id: 'pages.competition-report.' + d.specialist,
            }),
            partner_name: partnerName,
            age_group_name: groupName,
            event_level_name: eventLevelName,
          };
        }),
      );
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependency Data' }),
      );
    }
    hideLoading();
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    moduleName,
    printData,
    setPrintData,
    playerData,
  };

  return (
    <Context.Provider value={contextValue}>
      <Table data={data} />
      <Form />
      <PrintModal />
    </Context.Provider>
  );
};
