import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import { EditOutlined, DeleteOutlined, PrinterOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, CompetitionReportExtra as Item } from '../index';
import { useIntl } from 'umi';
import styles from './table.less';

export default ({ data }: { data: Item[] }) => {
  const intl = useIntl();
  const { setEditedData, getTableData, moduleName, setPrintData } = useContext(Context);

  const onEdit = (record: Item) => {
    setEditedData(record);
  };

  const onPrint = (record: Item) => {
    setPrintData(record);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'pages.competition-report.player_name' }),
      dataIndex: ['player', 'nama_lengkap'],
      key: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-report.tournament' }),
      dataIndex: ['competition_info', 'nama_event'],
      key: 'tournament',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-report.specialist' }),
      dataIndex: 'specialist_string',
      key: 'specialist',
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-report.level' }),
      dataIndex: 'event_level',
      key: 'event_level',
      render: (value, record) => {
        if (value == 'main_draw') return <>Main Draw</>;
        return <>Qualifiying</>;
      },
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-report.result' }),
      dataIndex: ['result_category', 'result'],
      key: 'result',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      width: '20%',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
              size="small"
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="primary" size="small" danger shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
          <Tooltip title="Print">
            <Button
              onClick={() => onPrint(record)}
              shape="circle"
              icon={<PrinterOutlined />}
              size="small"
            />
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      size="small"
      pagination={{ size: 'small' }}
      columns={columns}
      dataSource={data}
    />
  );
};
