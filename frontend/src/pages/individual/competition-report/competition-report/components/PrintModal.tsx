import { Drawer, Row, Col, Table, TableColumnsType, Space, Button, message } from 'antd';
import styles from './PrintModal.less';
import { getLocaleKey, Context } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { DetailCompetitionReport } from '@/types/ypi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { jsPDF } from 'jspdf';
import { get } from '@/services/ypi/api';
import FooterReport from '@/components/FooterReport';

interface DetailReport extends DetailCompetitionReport {
  set1: string;
  set2: string;
  set3: string;
}

const Header = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <Row justify="center">
        <Col>
          <span className={styles.title}>Tournament Report</span>
        </Col>
      </Row>
      <table style={{ width: '100%' }}>
        <tbody>
          <tr>
            <th style={{ width: '9rem' }}>
              <FormattedMessage id={getLocaleKey('player_name')} />
            </th>
            <td style={{ width: '14rem' }}>: {printData?.player.nama_lengkap}</td>
            <th style={{ width: '9rem' }}>
              <FormattedMessage id={getLocaleKey('partner')} />
            </th>
            <td>: {printData?.partner_name}</td>
          </tr>
          <tr>
            <th>
              <FormattedMessage id={getLocaleKey('venue')} />
            </th>
            <td>: {printData?.venue}</td>
            <th>
              <FormattedMessage id={getLocaleKey('age_group')} />
            </th>
            <td>: {printData?.age_group_name}</td>
          </tr>
          <tr>
            <th>
              <FormattedMessage id={getLocaleKey('result')} />
            </th>
            <td>: {printData?.result_category.result}</td>
            <th>Tournament Detail</th>
            <td>: {printData?.competition_info.nama_event}</td>
          </tr>
          <tr>
            <th>
              <FormattedMessage id={getLocaleKey('tournament_draw')} />
            </th>
            <td>: {printData?.event_level_name}</td>
            <th>
              <FormattedMessage id={getLocaleKey('start_event')} />
            </th>
            <td>: {printData?.competition_info.tanggal_mulai.substring(0, 10)}</td>
          </tr>
          <tr>
            <th>
              <FormattedMessage id={getLocaleKey('event')} />
            </th>
            <td>: {printData?.specialist_string}</td>
            <th>
              <FormattedMessage id={getLocaleKey('end_event')} />
            </th>
            <td>: {printData?.competition_info.tanggal_akhir.substring(0, 10)}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

const TableDetail = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [data, setData] = useState<DetailReport[]>([]);

  const fetchDetailData = async (editedDataId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const detailDataRes = await get(
        'detail_competition_reports?competition_report_id=' + editedDataId,
      );
      setData(
        (detailDataRes.data as DetailCompetitionReport[]).map((d) => ({
          ...d,
          set1: `${d.set1a} / ${d.set1b}`,
          set2: `${d.set2a} / ${d.set2b}`,
          set3: `${d.set3a} / ${d.set3b}`,
        })),
      );
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependency Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    if (printData) {
      fetchDetailData(printData.id as number);
    }
  }, [printData]);

  const columns: TableColumnsType<DetailCompetitionReport> = [
    {
      title: intl.formatMessage({ id: getLocaleKey('round') }),
      dataIndex: 'round',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('opponent') }),
      dataIndex: 'opponent',
    },
    {
      title: intl.formatMessage({ id: 'commonField.from' }),
      dataIndex: 'nationality',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('score') }),
      children: [
        {
          title: intl.formatMessage({ id: getLocaleKey('set1') }),
          dataIndex: 'set1',
        },
        {
          title: intl.formatMessage({ id: getLocaleKey('set2') }),
          dataIndex: 'set2',
        },
        {
          title: intl.formatMessage({ id: getLocaleKey('set3') }),
          dataIndex: 'set3',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'commonField.remarks' }),
      dataIndex: 'comment',
    },
    {
      title: intl.formatMessage({ id: 'commonField.result' }),
      dataIndex: 'result',
    },
  ];

  return (
    <Table
      bordered
      // rowClassName={rowClassName}
      columns={columns}
      dataSource={data}
      size="small"
      pagination={false}
    />
  );
};

const Footer = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <span className={styles.label}>
        <FormattedMessage id="commonField.summary" />
      </span>
      <br />
      <span>{printData?.summary_notes}</span>
      <br />
      <span className={styles.label}>
        <FormattedMessage id={getLocaleKey('next_suggestion')} />
      </span>
      <br />
      <span>{printData?.suggestion_notes}</span>
    </div>
  );
};

export default () => {
  const [visible, setVisible] = useState<boolean>(false);
  const intl = useIntl();
  const { printData, setPrintData } = useContext(Context);

  useEffect(() => {
    if (printData) {
      setVisible(true);
      console.log(printData);
    }
  }, [printData]);

  const handleClose = () => {
    setVisible(false);
    setPrintData(undefined);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('tournament_report.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1200,
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );
  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={visible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <TableDetail />
          <Footer />
          <FooterReport />
        </div>
      </Drawer>
    </>
  );
};
