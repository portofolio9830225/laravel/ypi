import { Form, Input, Card, Button, message, Space, Row, Col, Select, Divider } from 'antd';
import { Context, CompetitionReportExtra as Item } from '../index';
import { create, get } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage, useHistory } from 'umi';
import { Player, ResultCategory, CompetitionDetail, GroupClassification } from '@/types/ypi';
const { Option } = Select;
import DetailCompetitionReport from './DetailCompetitionReport';
import { EditOutlined } from '@ant-design/icons';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const history = useHistory();
  const { editedData, setEditedData, getTableData, moduleName, playerData } = useContext(Context);
  const [openForm, setOpenForm] = useState<boolean>(false);
  const [specialistData, setSpecialistData] = useState<string[]>([]);
  const eventLevelData = ['qualifiying', 'main_draw'];
  const [partnerData, setPartnerData] = useState<Player[]>([]);
  const [resultCategoryData, setResultCategoryData] = useState<ResultCategory[]>([]);
  const [competitionDetailData, setCompetitionDetailData] = useState<CompetitionDetail[]>([]);
  const [groupClassificationData, setGroupClassificationData] = useState<GroupClassification[]>([]);

  const lblPlayer = intl.formatMessage({
    id: 'pages.competition-report.player',
  });
  const lblSpecialist = intl.formatMessage({
    id: 'pages.competition-report.specialist',
  });
  const lblEventLevel = intl.formatMessage({
    id: 'pages.competition-report.event_level',
  });
  const lblPartner = intl.formatMessage({
    id: 'pages.competition-report.partner',
  });
  const lblEvent = intl.formatMessage({
    id: 'pages.competition-report.event',
  });
  const lblStartEvent = intl.formatMessage({
    id: 'pages.competition-report.start_event',
  });
  const lblEndEvent = intl.formatMessage({
    id: 'pages.competition-report.end_event',
  });
  const lblAgeGroup = intl.formatMessage({
    id: 'pages.competition-report.age_group',
  });
  const lblVenue = intl.formatMessage({
    id: 'pages.competition-report.venue',
  });
  const lblResult = intl.formatMessage({
    id: 'pages.competition-report.result',
  });
  const lblSummary = intl.formatMessage({
    id: 'pages.competition-report.summary',
  });
  const lblNextSuggest = intl.formatMessage({
    id: 'pages.competition-report.next_suggestion',
  });

  const onFinish = async (values: Item) => {
    console.log('values', values);
    // return;
    try {
      await create('competition_reports/details', values);
      getTableData();
      onReset();
      onCancel();
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
      console.log('error', error);
    }
  };

  const getSpecialistData = (playerData: Player) => {
    let specialists: string[] = [];
    if (playerData.tunggal_putra) specialists.push('tunggal_putra');
    if (playerData.tunggal_putri) specialists.push('tunggal_putri');
    if (playerData.ganda_putra) specialists.push('ganda_putra');
    if (playerData.ganda_putri) specialists.push('ganda_putri');
    if (playerData.ganda_campuran) specialists.push('ganda_campuran');
    return specialists;
  };

  const fetchDependenciesData = async () => {
    try {
      const resultRes = await get('result_categories');
      setResultCategoryData(resultRes.data);
      const compDetailRes = await get('master_achievements');
      setCompetitionDetailData(compDetailRes.data);
      const groupRes = await get('tabel_kelompoks');
      setGroupClassificationData(groupRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependencies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  useEffect(() => {
    if (specialistData.length === 0) {
      form.setFieldsValue({ specialist: undefined });
    }
  }, [specialistData]);

  const fetchDetailData = async (editedDataId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const detailDataRes = await get(
        'detail_competition_reports?competition_report_id=' + editedDataId,
      );
      form.setFieldsValue({ ...editedData, detailData: detailDataRes.data });
      setOpenForm(true);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependency Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    if (editedData) {
      fetchDetailData(editedData.id as number);
    }
  }, [editedData]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(undefined);
    setOpenForm(false);
  };

  const onOpenForm = () => {
    setOpenForm(true);
  };

  const getRequiredRule = (label: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName: label }),
      },
    ];
  };

  const onValuesChange = (changedValues: any, values: Item) => {
    if (changedValues.pemain_id) {
      const selectedPlayerData = playerData.find((p) => p.id == changedValues.pemain_id);
      setSpecialistData(getSpecialistData(selectedPlayerData as Player));
    }

    if (changedValues.specialist) {
      let partners: Player[] = [];
      if (changedValues.specialist == 'ganda_putra') {
        partners = playerData.filter((d) => d.ganda_putra && d.id != values.pemain_id);
      } else if (changedValues.specialist == 'ganda_putri') {
        partners = playerData.filter((d) => d.ganda_putri && d.id != values.pemain_id);
      } else if (changedValues.specialist == 'ganda_campuran') {
        partners = playerData.filter((d) => d.ganda_campuran && d.id != values.pemain_id);
      }
      setPartnerData(partners);
    }

    if (changedValues.master_achievement_id) {
      const selectedEvent = competitionDetailData.find(
        (d) => d.id == changedValues.master_achievement_id,
      );
      form.setFieldsValue({
        event_start_date: (selectedEvent?.tanggal_mulai as string).substring(0, 10),
        event_end_date: (selectedEvent?.tanggal_akhir as string).substring(0, 10),
      });
    }
  };

  if (!openForm) {
    return (
      <Button style={{ marginTop: '1rem' }} type="primary" htmlType="button" onClick={onOpenForm}>
        {intl.formatMessage({ id: 'crud.addNewData' })}
      </Button>
    );
  }

  const handleOpenResultCategory = () => {
    history.push('/individual/competition-report/result-category');
  };

  const getSpecialistPlaceholder = (): string => {
    const playerId = form.getFieldValue('pemain_id');
    if (playerId && specialistData.length === 0) {
      return 'No Specialist Found in Player Data';
    }

    if (playerId && specialistData.length > 0) {
      return 'Select a Specialist';
    }

    return 'Select Player';
  };

  return (
    <Card
      style={{ marginTop: '1rem' }}
      title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
    >
      <Form
        form={form}
        name="form"
        onFinish={onFinish}
        layout="vertical"
        onValuesChange={onValuesChange}
        initialValues={{ detailData: [{ id: undefined }] }}
      >
        <Form.Item name="id" hidden />
        <Row gutter={12}>
          <Col span={6}>
            <Form.Item name="pemain_id" label={lblPlayer} rules={getRequiredRule(lblPlayer)}>
              <Select>
                {playerData.map((p) => (
                  <Option key={p.id} value={p.id}>
                    {p.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="specialist"
              label={lblSpecialist}
              rules={getRequiredRule(lblSpecialist)}
            >
              <Select
                disabled={specialistData.length > 0 ? false : true}
                placeholder={getSpecialistPlaceholder()}
              >
                {specialistData.map((d) => (
                  <Option key={d} value={d}>
                    {intl.formatMessage({
                      id: 'pages.competition-report.' + d,
                    })}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="event_level"
              label={lblEventLevel}
              rules={getRequiredRule(lblEventLevel)}
            >
              <Select>
                {eventLevelData.map((d) => (
                  <Option key={d} value={d}>
                    {intl.formatMessage({
                      id: 'pages.competition-report.' + d,
                    })}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="partner_id" label={lblPartner}>
              <Select disabled={partnerData.length > 0 ? false : true}>
                {partnerData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={12}>
          <Col span={6}>
            <Form.Item
              name="master_achievement_id"
              label={lblEvent}
              rules={getRequiredRule(lblEvent)}
            >
              <Select>
                {competitionDetailData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.nama_event}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="event_start_date" label={lblStartEvent}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="event_end_date" label={lblEndEvent}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="kelompok_id" label={lblAgeGroup} rules={getRequiredRule(lblAgeGroup)}>
              <Select>
                {groupClassificationData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.kelompok}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={12}>
          <Col span={12}>
            <Form.Item name="venue" label={lblVenue}>
              <Input />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="result_id" label={lblResult} rules={getRequiredRule(lblResult)}>
              <Select
                dropdownRender={(menu) => (
                  <>
                    {menu}
                    <Divider />
                    <Space style={{ padding: '0 8px 4px' }}>
                      <Button
                        type="primary"
                        onClick={handleOpenResultCategory}
                        icon={<EditOutlined />}
                      >
                        <FormattedMessage id="crud.editData" />
                      </Button>
                    </Space>
                  </>
                )}
              >
                {resultCategoryData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.result}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Divider />

        <DetailCompetitionReport resultCategories={resultCategoryData} />

        <Divider />

        <Row gutter={12}>
          <Col span={12}>
            <Form.Item name="summary_notes" label={lblSummary}>
              <Input.TextArea rows={4} />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="suggestion_notes" label={lblNextSuggest}>
              <Input.TextArea rows={4} />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
