import { Form, Input, Button, Tooltip, Row, Col, InputNumber, Select, Divider, Space } from 'antd';
import { useIntl, useHistory, FormattedMessage } from 'umi';
import { PlusOutlined, MinusOutlined, EditOutlined } from '@ant-design/icons';
import { ResultCategory } from '@/types/ypi';

export default ({ resultCategories }: { resultCategories: ResultCategory[] }) => {
  const intl = useIntl();
  const history = useHistory();

  const lblRound = intl.formatMessage({ id: 'pages.competition-report.round' });
  const lblOpponent = intl.formatMessage({ id: 'pages.competition-report.opponent' });
  const lblClub = intl.formatMessage({ id: 'pages.competition-report.club' });
  const lblSet1 = intl.formatMessage({ id: 'pages.competition-report.set1' });
  const lblSet2 = intl.formatMessage({ id: 'pages.competition-report.set2' });
  const lblSet3 = intl.formatMessage({ id: 'pages.competition-report.set3' });
  const lblCoachComment = intl.formatMessage({ id: 'pages.competition-report.coach_comment' });
  const lblResult = intl.formatMessage({ id: 'pages.competition-report.result' });

  const getRequiredRule = (label: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName: label }),
      },
    ];
  };

  const handleOpenResultCategory = () => {
    history.push('/individual/competition-report/result-category');
  };

  return (
    <Form.List name="detailData">
      {(fields, { add, remove }, { errors }) => (
        <>
          {fields.map(({ key, name, ...restField }, index) => (
            <Row key={key} gutter={6} align={index === 0 ? 'middle' : undefined}>
              <Form.Item name={[name, 'id']} hidden />
              <Col span={3}>
                <Form.Item
                  {...restField}
                  name={[name, 'round']}
                  label={index === 0 ? lblRound : ''}
                  rules={getRequiredRule('round')}
                >
                  <Select
                    style={{ width: '100%' }}
                    dropdownRender={(menu) => (
                      <>
                        {menu}
                        <Divider />
                        <Space style={{ padding: '0 8px 4px' }}>
                          <Button
                            type="primary"
                            onClick={handleOpenResultCategory}
                            icon={<EditOutlined />}
                          >
                            <FormattedMessage id="crud.editData" />
                          </Button>
                        </Space>
                      </>
                    )}
                  >
                    {resultCategories.map((d) => (
                      <Select.Option key={d.id} value={d.result}>
                        {d.result}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col>
                <Form.Item
                  {...restField}
                  name={[name, 'opponent']}
                  label={index === 0 ? lblOpponent : ''}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item
                  {...restField}
                  name={[name, 'nationality']}
                  label={index === 0 ? lblClub : ''}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col span={1}>
                <Form.Item {...restField} name={[name, 'set1a']} label={index === 0 ? lblSet1 : ''}>
                  <InputNumber type="number" style={{ width: '100%' }} controls={false} />
                </Form.Item>
              </Col>
              <Col span={1}>
                <Form.Item {...restField} name={[name, 'set1b']} label={index === 0 ? ' ' : ''}>
                  <InputNumber type="number" style={{ width: '100%' }} controls={false} />
                </Form.Item>
              </Col>
              <Col span={1}>
                <Form.Item {...restField} name={[name, 'set2a']} label={index === 0 ? lblSet2 : ''}>
                  <InputNumber type="number" style={{ width: '100%' }} controls={false} />
                </Form.Item>
              </Col>
              <Col span={1}>
                <Form.Item {...restField} name={[name, 'set2b']} label={index === 0 ? ' ' : ''}>
                  <InputNumber type="number" style={{ width: '100%' }} controls={false} />
                </Form.Item>
              </Col>
              <Col span={1}>
                <Form.Item {...restField} name={[name, 'set3a']} label={index === 0 ? lblSet3 : ''}>
                  <InputNumber type="number" style={{ width: '100%' }} controls={false} />
                </Form.Item>
              </Col>
              <Col span={1}>
                <Form.Item {...restField} name={[name, 'set3b']} label={index === 0 ? ' ' : ''}>
                  <InputNumber style={{ width: '100%' }} controls={false} />
                </Form.Item>
              </Col>
              <Col>
                <Form.Item
                  {...restField}
                  name={[name, 'comment']}
                  label={index === 0 ? lblCoachComment : ''}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col span={3}>
                <Form.Item
                  {...restField}
                  name={[name, 'result']}
                  label={index === 0 ? lblResult : ''}
                >
                  <Select
                    style={{ width: '100%' }}
                    dropdownRender={(menu) => (
                      <>
                        {menu}
                        <Divider />
                        <Space style={{ padding: '0 8px 4px' }}>
                          <Button
                            type="primary"
                            onClick={handleOpenResultCategory}
                            icon={<EditOutlined />}
                          >
                            <FormattedMessage id="crud.editData" />
                          </Button>
                        </Space>
                      </>
                    )}
                  >
                    {resultCategories.map((d) => (
                      <Select.Option key={d.id} value={d.result}>
                        {d.result}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>

              <Col>
                {fields.length > 1 ? (
                  <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
                    <Button
                      type="primary"
                      danger
                      onClick={() => remove(name)}
                      shape="circle"
                      icon={<MinusOutlined />}
                    />
                  </Tooltip>
                ) : null}
              </Col>
            </Row>
          ))}
          <Form.ErrorList errors={errors} />
          <Form.Item>
            <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
              <Button
                style={{ backgroundColor: '#52c41a', borderColor: '#52c41a' }}
                type="primary"
                onClick={() => add()}
                shape="circle"
                icon={<PlusOutlined />}
              />
            </Tooltip>
          </Form.Item>
        </>
      )}
    </Form.List>
  );
};
