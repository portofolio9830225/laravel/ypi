import { Card } from 'antd';
import IndicatorTable from './components/IndicatorTable';
import { CSSProperties, createContext, useState } from 'react';
import { useIntl } from 'umi';
import { KpiParameter } from '@/types/ypi';

interface DevType {
  edited: boolean;
  key: string;
}

export interface Indicator extends KpiParameter, DevType {}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.kpi-setup.' + fieldName;
};

const cardStyle: CSSProperties = {
  padding: 'unset',
};

export interface ContextType {
  selectedIndicatorId: number | undefined;
  setSelectedIndicatorId: React.Dispatch<React.SetStateAction<number | undefined>>;
  indicatorData: Partial<Indicator>[];
  setIndicatorData: React.Dispatch<React.SetStateAction<Partial<Indicator>[]>>;
}

export const Context = createContext<ContextType>({
  selectedIndicatorId: undefined,
  setSelectedIndicatorId: () => {},
  indicatorData: [],
  setIndicatorData: () => {},
});

export default () => {
  const intl = useIntl();
  const [selectedIndicatorId, setSelectedIndicatorId] = useState<number>();
  const [indicatorData, setIndicatorData] = useState<Partial<Indicator>[]>([]);

  const contextValue: ContextType = {
    selectedIndicatorId,
    setSelectedIndicatorId,
    indicatorData,
    setIndicatorData,
  };

  return (
    <>
      <Context.Provider value={contextValue}>
        <Card bodyStyle={cardStyle} title={intl.formatMessage({ id: getLocaleKey('indicator') })}>
          <IndicatorTable />
        </Card>
      </Context.Provider>
    </>
  );
};
