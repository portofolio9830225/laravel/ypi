import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
  Select,
  Checkbox,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { getLocaleKey, Context, Indicator } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { create, destroy, get, update } from '@/services/ypi/api';
import { CheckOutlined } from '@ant-design/icons';
import styles from './table.less';

interface CustomRowProps {
  index: number;
  record: Partial<Indicator>;
}

enum InputType {
  Date = 1,
  Time,
  Text,
  Number,
  None,
  Id,
  TypeInput,
  Checkbox,
}

// const EditableContext = create{
//   form: createContext<FormInstance<Indicator> | undefined>(null),
//   canCalculateStatistic: true,
// };

interface RowContext {
  form: FormInstance<Indicator> | undefined;
  canCalculateStatistic: boolean;
}

const EditableContext = createContext<RowContext>({
  form: undefined,
  canCalculateStatistic: false,
});

const CustomRow: React.FC<CustomRowProps> = ({ index, record, ...props }) => {
  const [form] = Form.useForm<Indicator>();
  const [canCalculateStatistic, setCanCalculateStatistic] = useState<boolean>(true);

  useEffect(() => {
    if (record) {
      form.setFieldsValue(record);
    }
  }, [record]);

  const handleValueChange = (changedValues: any, values: Indicator) => {
    if (values.input_type !== 'number') {
      setCanCalculateStatistic(false);
    } else {
      setCanCalculateStatistic(true);
    }
  };

  return (
    <Form onValuesChange={handleValueChange} form={form} component={false}>
      <EditableContext.Provider value={{ form, canCalculateStatistic }}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof Indicator | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: Indicator;
  handleToggleEdit: (form: FormInstance<Indicator> | undefined, record: Indicator) => void;
  handleSave: (form: FormInstance<Indicator> | undefined, record: Indicator) => Promise<void>;
  handleDelete: (record: Indicator) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const { form, canCalculateStatistic } = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Id) {
    return (
      <td {...props}>
        {
          <>
            <Form.Item hidden name={dataIndex} />
            {childNode}
          </>
        }
      </td>
    );
  }

  if (inputType === InputType.Checkbox && !record.edited) {
    if (record.calculate_statistic)
      return (
        <td {...props}>
          <CheckOutlined />
        </td>
      );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;
    let valuePropName = undefined;

    if (inputType === InputType.Text) {
      inputNode = <Input style={{ width: '100%' }} />;
      rules = getRequiredRule('Indicator');
    }
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;
    if (inputType === InputType.TypeInput) {
      inputNode = (
        <Select>
          <Select.Option value="number">Number</Select.Option>
          <Select.Option value="text">Text</Select.Option>
        </Select>
      );
      rules = getRequiredRule('Type Input');
    }
    if (inputType === InputType.Checkbox) {
      inputNode = <Checkbox disabled={!canCalculateStatistic} />;
      valuePropName = 'checked';
    }

    childNode = (
      <Form.Item valuePropName={valuePropName} rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const IndicatorTable: FC = () => {
  const intl = useIntl();
  const { indicatorData, setIndicatorData } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  const fetchData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get('kpi_parameters');
      const d: Indicator[] = dataRes.data;
      setIndicatorData(
        d.map((d, i) => {
          return { ...d, key: i.toString() };
        }),
      );
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Week Data' }));
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.id' }),
      dataIndex: 'id',
      inputType: InputType.Id,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('indicator') }),
      dataIndex: 'name',
      inputType: InputType.Text,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('column_order') }),
      dataIndex: 'column_order',
      inputType: InputType.Number,
    },
    {
      title: intl.formatMessage({ id: 'commonField.input_type' }),
      dataIndex: 'input_type',
      inputType: InputType.TypeInput,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('calculate_statistic') }),
      dataIndex: 'calculate_statistic',
      inputType: InputType.Checkbox,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      width: '20%',
    },
  ];

  const handleToggleEdit = (form: FormInstance<Indicator> | undefined, record: Indicator) => {
    const editedDataIndex = indicatorData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...indicatorData];
    currentData[editedDataIndex] = editedData;
    setIndicatorData(currentData);
  };

  const handleSave = async (form: FormInstance<Indicator> | undefined, record: Indicator) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = indicatorData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();

      let savedData = {};
      if (values && values.id) {
        const updateRes = await update('kpi_parameters', values.id, values);
        savedData = updateRes.data;
      } else {
        const createRes = await create('kpi_parameters', values);
        savedData = createRes.data;
      }

      let currentData = [...indicatorData];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      hideLoading();
      setIndicatorData(currentData);
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: Indicator) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = indicatorData.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy('kpi_parameters', record.id);
      }
      let currentData = [...indicatorData];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setIndicatorData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Indicator) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    const newData = [...indicatorData];
    newData.push({
      id: undefined,
      edited: true,
      key: newData.length.toString(),
      input_type: 'number',
      column_order: newData.length + 1,
    });
    setIsEditMode(!isEditMode);
    setIndicatorData([...newData]);
  };

  const footer = () => {
    if (isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      className={styles.custom_table}
      size="small"
      components={components}
      bordered
      dataSource={indicatorData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={{ size: 'small' }}
      // @ts-ignore: Unreachable code error
      onRow={(record, rowIndex) => ({ index: rowIndex, record })}
      rowClassName={styles.densed_row}
    />
  );
};

export default IndicatorTable;
