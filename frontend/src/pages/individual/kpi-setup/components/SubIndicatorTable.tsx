import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
  Select,
} from 'antd';
import { FC, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { getLocaleKey, Context, SubIndicator } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { create, destroy, update } from '@/services/ypi/api';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
  Select,
  Color,
}

const EditableContext = createContext<FormInstance<SubIndicator> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof SubIndicator | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: SubIndicator;
  handleToggleEdit: (form: FormInstance<SubIndicator> | null, record: SubIndicator) => void;
  handleSave: (form: FormInstance<SubIndicator> | null, record: SubIndicator) => Promise<void>;
  handleDelete: (record: SubIndicator) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Id) {
    return (
      <td {...props}>
        {
          <>
            <Form.Item hidden name={dataIndex} />
            <Form.Item hidden name="kpi_indicator_id" />
            {childNode}
          </>
        }
      </td>
    );
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) {
      inputNode = <Input style={{ width: '100%' }} />;
      rules = getRequiredRule('sub_indicator');
    }
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;
    if (inputType === InputType.Color) inputNode = <Input type="color" />;
    if (inputType === InputType.Select) {
      inputNode = (
        <Select>
          <Select.Option value="more_is_better">
            <FormattedMessage id={getLocaleKey('more_is_better')} />
          </Select.Option>
          <Select.Option value="less_is_better">
            <FormattedMessage id={getLocaleKey('less_is_better')} />
          </Select.Option>
        </Select>
      );
      rules = getRequiredRule('type');
    }

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (record && !record.edited) {
    if (inputType === InputType.Select) {
      return (
        <td {...props}>
          <FormattedMessage id={getLocaleKey(record.indicator_type)} />
        </td>
      );
    }
    if (inputType === InputType.Color) {
      return <td {...props} style={{ backgroundColor: record.color }}></td>;
    }
  }

  return <td {...props}>{childNode}</td>;
};

const IndicatorTable: FC = () => {
  const intl = useIntl();
  const { subIndicatorData, setSubIndicatorData, selectedIndicatorId } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.id' }),
      dataIndex: 'id',
      inputType: InputType.Id,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('sub_indicator') }),
      dataIndex: 'name',
      inputType: InputType.Text,
    },
    {
      title: intl.formatMessage({ id: 'commonField.type' }),
      dataIndex: 'indicator_type',
      inputType: InputType.Select,
    },
    {
      title: intl.formatMessage({ id: 'commonField.color' }),
      dataIndex: 'color',
      inputType: InputType.Color,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
    },
  ];

  const handleToggleEdit = (form: FormInstance<SubIndicator> | null, record: SubIndicator) => {
    const editedDataIndex = subIndicatorData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...subIndicatorData];
    currentData[editedDataIndex] = editedData;
    setSubIndicatorData(currentData);
  };

  const handleSave = async (form: FormInstance<SubIndicator> | null, record: SubIndicator) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = subIndicatorData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();

      let savedData = {};
      if (values && values.id) {
        const updateRes = await update('kpi_subindicators', values.id, values);
        savedData = updateRes.data;
      } else {
        const newData: Partial<SubIndicator> = {
          ...values,
          kpi_indicator_id: selectedIndicatorId as number,
        };
        const createRes = await create('kpi_subindicators', newData);
        savedData = createRes.data;
      }

      let currentData = [...subIndicatorData];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      hideLoading();
      setIsEditMode(false);
      setSubIndicatorData(currentData);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: SubIndicator) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = subIndicatorData.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy('kpi_subindicators', record.id);
      }
      let currentData = [...subIndicatorData];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setSubIndicatorData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: SubIndicator) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    const newData = [...subIndicatorData];
    newData.push({
      id: undefined,
      edited: true,
      key: newData.length.toString(),
    });
    setIsEditMode(!isEditMode);
    setSubIndicatorData([...newData]);
  };

  const footer = () => {
    if (selectedIndicatorId === undefined || isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      dataSource={subIndicatorData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={{ size: 'small' }}
    />
  );
};

export default IndicatorTable;
