import { Button, Card, DatePicker, Form, message, Select } from 'antd';
import { Context, getLocaleKey, Item } from '../index';
import { useContext } from 'react';
import moment, { Moment } from 'moment';
import { FormattedMessage, useIntl } from 'umi';
import { get } from '@/services/ypi/api';

interface FormType {
  date_moment: Moment;
  date_select: string;
  group_id: number;
}

const KeyForm = () => {
  const [form] = Form.useForm<FormType>();
  const intl = useIntl();
  const { savedDateData, groups, setTableData, setSavedDateData, setDateTest } =
    useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleDateChange = (date: Moment | null, dateString: string) => {
    form.setFieldsValue({ date_select: undefined });
    setDateTest(date);
  };

  const onSelectChange = (value: string) => {
    const dateMoment = moment(value);
    form.setFieldsValue({ date_moment: dateMoment });
    setDateTest(dateMoment);
  };

  const handleProcess = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formValues = await form.validateFields();
      const kpiResultRes = await get(
        `kpi_results/batch/${formValues.date_moment.format('YYYY-MM-DD')}/${formValues.group_id}`,
      );
      const kpiResults: Item[] = kpiResultRes.data;
      setTableData(
        kpiResults.map((d, i) => {
          return { ...d, key: i.toString(), no: i + 1 };
        }),
      );
      const dateDataRes = await get('kpi_results/dates');
      setSavedDateData(dateDataRes.data);
      hideLoading();
    } catch (error) {
      console.log(error);
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  return (
    <Card>
      <Form layout="inline" form={form}>
        <Form.Item
          rules={getRequiredRule('Date')}
          name="date_moment"
          label={<FormattedMessage id={getLocaleKey('date_test')} />}
        >
          <DatePicker onChange={handleDateChange} />
        </Form.Item>
        <Form.Item name="date_select" label={<FormattedMessage id={getLocaleKey('saved_data')} />}>
          <Select
            showSearch
            placeholder="Select a date"
            optionFilterProp="children"
            onChange={onSelectChange}
            filterOption={(input, option) =>
              (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
            }
          >
            {savedDateData.map((d, i) => (
              <Select.Option value={d} key={i}>
                {d}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          rules={getRequiredRule('Group')}
          name="group_id"
          label={<FormattedMessage id="commonField.group" />}
        >
          <Select
            showSearch
            placeholder="Select a group"
            optionFilterProp="children"
            filterOption={(input, option) =>
              (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
            }
          >
            {groups.map((d, i) => (
              <Select.Option value={d.id} key={i}>
                {d.kelompok}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Button onClick={handleProcess} type="primary">
            <FormattedMessage id="crud.process" />
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};

export default KeyForm;
