import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
} from 'antd';
import { FC, useState, createContext, useContext, useEffect } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { Context, Item, InputType } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { create, destroy } from '@/services/ypi/api';
import { median, stdDeviation, getVo2max } from '@/utils/calculation';

interface CustomRowProps {
  index: number;
}

const EditableContext = createContext<FormInstance<Item> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm<Item>();
  const { constantValue } = useContext(Context);

  const handleValueChange = (changedValues: any, values: Item) => {
    if (changedValues.hasOwnProperty(constantValue?.beep_test_name)) {
      const beepTest = changedValues[constantValue?.beep_test_name || ''];
      if (beepTest) {
        if (beepTest > 4 || beepTest < 15.14) {
          const vo2max = getVo2max(beepTest);
          form.setFieldsValue({ [constantValue?.vo2max_name || '']: vo2max });
        }
      }
    }
  };

  return (
    <Form form={form} component={false} onValuesChange={handleValueChange}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof Item | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: Item;
  handleToggleEdit: (form: FormInstance<Item> | null, record: Item) => void;
  handleSave: (form: FormInstance<Item> | null, record: Item) => Promise<void>;
  handleDelete: (record: Item) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName: fieldName }),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  // if (dataIndex === 'no') {
  //   childNode = (<>
  //     <Form.Item name='player_id'
  //   </>
  //   );
  //   return <td {...props}>{childNode}</td>;
  // }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Number) inputNode = <InputNumber type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const TableComponent: FC = () => {
  const intl = useIntl();
  const { tableData, setTableData, dateTest, dynamicColumns } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  useEffect(() => {
    if (dateTest) {
      setIsEditMode(false);
    }
  }, [dateTest]);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      inputType: InputType.None,
    },
    {
      title: intl.formatMessage({ id: 'commonField.name' }),
      dataIndex: 'player_name',
      inputType: InputType.None,
    },
    {
      title: intl.formatMessage({ id: 'commonField.group' }),
      dataIndex: 'group_name',
      inputType: InputType.None,
    },
    ...dynamicColumns,
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      InputType: InputType.None,
      fixed: 'right',
    },
  ];

  const handleToggleEdit = (form: FormInstance<Item> | null, record: Item) => {
    const editedDataIndex = tableData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...tableData];
    currentData[editedDataIndex] = editedData;
    setTableData(currentData);
  };

  const handleSave = async (form: FormInstance<Item> | null, record: Item) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = tableData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();
      if (values) {
        values.player_id = record.player_id;
        values.group_id = record.group_id;
        values.player_name = record.player_name;
        values.group_name = record.group_name;
        if (dateTest) values.date = dateTest.format('YYYY-MM-DD');
      }
      // console.log(values);
      // return;
      let savedData = {};
      const createRes = await create('kpi_results/batch', values);
      savedData = createRes.data;

      let currentData = [...tableData];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString(), no: i + 1 };
      });

      setTableData(currentData);
      setIsEditMode(false);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
    hideLoading();
  };

  const handleDelete = async (record: Item) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const deletedIndex = tableData.findIndex((d) => d.key === record.key);
      let currentData = [...tableData];
      const currentDeletedData = currentData[deletedIndex];
      const deleteRes = await destroy(
        `kpi_results/batch/${dateTest?.format('YYYY-MM-DD')}/${currentDeletedData.player_id}`,
      );

      currentData[deletedIndex] = deleteRes.data;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString(), no: i + 1 };
      });
      setTableData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const mapColumns = (col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Item) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    if (col.children) {
      newCol.children = col.children.map(mapColumns);
    }
    return newCol;
  };

  const processedColumns = columns.map(mapColumns);

  const summary = (data: readonly Partial<Item>[]) => {
    return (
      <>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <FormattedMessage id="commonField.mean" />
          </Table.Summary.Cell>
          {dynamicColumns.map((d, i) => {
            if (d.inputType === InputType.Number && d.canCalculateStatistic) {
              let sum = 0;
              data.forEach((val) => {
                if (val[d.dataIndex]) sum += parseInt(val[d.dataIndex]);
              });
              return (
                <Table.Summary.Cell key={i + 2} index={i + 2}>
                  {(sum / data.length).toFixed(2)}
                </Table.Summary.Cell>
              );
            }
            return <Table.Summary.Cell key={i + 2} index={i + 2}></Table.Summary.Cell>;
          })}
          <Table.Summary.Cell index={dynamicColumns.length + 2}></Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <FormattedMessage id="commonField.median" />
          </Table.Summary.Cell>
          {dynamicColumns.map((d, i) => {
            if (d.inputType === InputType.Number && d.canCalculateStatistic) {
              let arrayOfValue: number[] = [];
              data.forEach((val) => {
                if (val[d.dataIndex]) arrayOfValue.push(parseInt(val[d.dataIndex]));
              });
              return (
                <Table.Summary.Cell index={i + 2} key={i + 2}>
                  {median(arrayOfValue)?.toFixed(2)}
                </Table.Summary.Cell>
              );
            }
            return <Table.Summary.Cell key={i + 2} index={i + 2}></Table.Summary.Cell>;
          })}
          <Table.Summary.Cell index={dynamicColumns.length + 2}></Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <FormattedMessage id="commonField.stdev" />
          </Table.Summary.Cell>
          {dynamicColumns.map((d, i) => {
            if (d.inputType === InputType.Number && d.canCalculateStatistic) {
              let arrayOfValue: number[] = [];
              data.forEach((val) => {
                if (val[d.dataIndex]) arrayOfValue.push(parseInt(val[d.dataIndex]));
              });
              return (
                <Table.Summary.Cell index={i + 2} key={i + 2}>
                  {stdDeviation(arrayOfValue)?.toFixed(2)}
                </Table.Summary.Cell>
              );
            }
            return <Table.Summary.Cell key={i + 2} index={i + 2}></Table.Summary.Cell>;
          })}
          <Table.Summary.Cell index={dynamicColumns.length + 2}></Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <FormattedMessage id="commonField.max" />
          </Table.Summary.Cell>
          {dynamicColumns.map((d, i) => {
            if (d.inputType === InputType.Number && d.canCalculateStatistic) {
              let arrayOfValue: number[] = [];
              data.forEach((val) => {
                if (val[d.dataIndex]) arrayOfValue.push(parseInt(val[d.dataIndex]));
              });
              return (
                <Table.Summary.Cell index={i + 2} key={i + 2}>
                  {Math.max(...arrayOfValue).toFixed(2)}
                </Table.Summary.Cell>
              );
            }
            return <Table.Summary.Cell key={i + 2} index={i + 2}></Table.Summary.Cell>;
          })}
          <Table.Summary.Cell index={dynamicColumns.length + 2}></Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <FormattedMessage id="commonField.min" />
          </Table.Summary.Cell>
          {dynamicColumns.map((d, i) => {
            if (d.inputType === InputType.Number && d.canCalculateStatistic) {
              let arrayOfValue: number[] = [];
              data.forEach((val) => {
                if (val[d.dataIndex]) arrayOfValue.push(parseInt(val[d.dataIndex]));
              });
              return (
                <Table.Summary.Cell key={i + 2} index={i + 2}>
                  {Math.min(...arrayOfValue).toFixed(2)}
                </Table.Summary.Cell>
              );
            }
            return <Table.Summary.Cell key={i + 2} index={i + 2}></Table.Summary.Cell>;
          })}
          <Table.Summary.Cell index={dynamicColumns.length + 2}></Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      dataSource={tableData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      pagination={{ size: 'small' }}
      summary={dateTest ? summary : undefined}
    />
  );
};

export default TableComponent;
