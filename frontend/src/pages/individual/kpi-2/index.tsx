import { Player, KpiParameter, GroupClassification } from '@/types/ypi';
import { createContext, useState, useEffect } from 'react';
import { message } from 'antd';
import { useIntl } from 'umi';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import KeyForm from './components/KeyForm';
import { Moment } from 'moment';

interface DevType {
  no: number;
  edited: boolean;
  key: string;
}

export enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
  Age,
}

interface Column {
  title: string;
  dataIndex: string;
  inputType: InputType;
  canCalculateStatistic: boolean;
}

interface ConstantValue {
  beep_test_name: string;
  vo2max_name: string;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.kpi-2.' + fieldName;
};

export interface Item extends DevType {
  player_id: number;
  player_name: string;
  group_id: number;
  group_name: string;
  date: string;
}

interface ContextType {
  tableData: Partial<Item>[];
  setTableData: React.Dispatch<React.SetStateAction<Partial<Item>[]>>;
  dateTest: Moment | null;
  setDateTest: React.Dispatch<React.SetStateAction<Moment | null>>;
  playerData: Player[];
  dynamicColumns: Column[];
  savedDateData: string[];
  constantValue: ConstantValue | undefined;
  groups: GroupClassification[];
  setSavedDateData: React.Dispatch<React.SetStateAction<string[]>>;
}

export const Context = createContext<ContextType>({
  tableData: [],
  setTableData: () => {},
  dateTest: null,
  setDateTest: () => {},
  playerData: [],
  dynamicColumns: [],
  savedDateData: [],
  constantValue: undefined,
  groups: [],
  setSavedDateData: () => {},
});

export default () => {
  const intl = useIntl();
  const [tableData, setTableData] = useState<Partial<Item>[]>([]);
  const [dateTest, setDateTest] = useState<Moment | null>(null);
  const [playerData, setPlayerData] = useState<Player[]>([]);
  const [dynamicColumns, setDynamicColumns] = useState<Column[]>([]);
  const [savedDateData, setSavedDateData] = useState<string[]>([]);
  const [constantValue, setConstantValue] = useState<ConstantValue>();
  const [groups, setGroups] = useState<GroupClassification[]>([]);

  const fetchDependenciesData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get('master_pemains');
      const d: Player[] = dataRes.data;
      setPlayerData(d);

      const dateDataRes = await get('kpi_results/dates');
      setSavedDateData(dateDataRes.data);

      const groupsRes = await get('tabel_kelompoks');
      setGroups(groupsRes.data);

      const kpiParameterRes = await get('kpi_parameters');
      const kpiParameter: KpiParameter[] = kpiParameterRes.data;
      setDynamicColumns(
        kpiParameter
          .sort((a, b) => a.column_order - b.column_order)
          .map((d) => {
            const inputType = d.input_type === 'number' ? InputType.Number : InputType.Text;
            return {
              title: d.name,
              dataIndex: d.name,
              inputType,
              canCalculateStatistic: d.calculate_statistic,
            };
          }),
      );

      const beepTestIdRes = await get('constants?key=KPI_BEEP_TEST_ID');
      const beepTestRes = await get('kpi_parameters/' + beepTestIdRes.data[0].value);
      const vo2maxIdRes = await get('constants?key=KPI_VO2MAX_ID');
      const vo2maxRes = await get('kpi_parameters/' + vo2maxIdRes.data[0].value);
      setConstantValue({ beep_test_name: beepTestRes.data.name, vo2max_name: vo2maxRes.data.name });

      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const contextValue: ContextType = {
    tableData,
    setTableData,
    dateTest,
    setDateTest,
    playerData,
    dynamicColumns,
    savedDateData,
    setSavedDateData,
    constantValue,
    groups,
  };

  return (
    <>
      <Context.Provider value={contextValue}>
        <KeyForm />
        <Table />
      </Context.Provider>
    </>
  );
};
