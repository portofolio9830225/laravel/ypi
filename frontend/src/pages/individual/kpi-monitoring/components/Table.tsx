import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
  TableColumnType,
  DatePicker,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { getLocaleKey, Context, Record } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { create, destroy, get, update } from '@/services/ypi/api';
import { SubIndicator } from '../../kpi-setup';
import moment from 'moment';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
}

const EditableContext = createContext<FormInstance<Record> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof Record | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: Record;
  handleToggleEdit: (form: FormInstance<Record> | null, record: Record) => void;
  handleSave: (form: FormInstance<Record> | null, record: Record) => Promise<void>;
  handleDelete: (record: Record) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '4rem' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;
    if (inputType === InputType.Date) {
      inputNode = <DatePicker style={{ width: '100%' }} />;
      rules = getRequiredRule('date');
    }

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Date) {
    return <td {...props}>{record.date}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const MonitoringTable: FC = () => {
  const intl = useIntl();
  const { selectedIndicatorId, selectedPlayerId } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const [tableData, setTableData] = useState<Partial<Record>[]>([]);
  const [subIndicatorData, setSubIndicatorData] = useState<SubIndicator[]>([]);

  const fetchSubIndicator = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const subIndicatorRes = await get(
        'kpi_subindicators?kpi_indicator_id=' + selectedIndicatorId,
      );
      setSubIndicatorData(subIndicatorRes.data);

      const recordRes = await get(
        `kpi_values?kpi_indicator_id=${selectedIndicatorId}&player_id=${selectedPlayerId}&process_data=${true}`,
      );
      const records: Record[] = recordRes.data;

      setTableData(
        records.map((d, i) => {
          return { ...d, key: i.toString(), no: i + 1, date_moment: moment(d.date) };
        }),
      );

      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Week Data' }));
    }
  };

  useEffect(() => {
    if (selectedIndicatorId && selectedPlayerId) {
      setIsEditMode(false);
      fetchSubIndicator();
    }
  }, [selectedIndicatorId, selectedPlayerId]);

  let dynamicColumns: TableColumnType<Partial<Record>>[] = [];
  dynamicColumns = subIndicatorData.map((d) => {
    return {
      title: d.name,
      children: [
        { title: 'PB', dataIndex: 'pb_' + d.id, inputType: InputType.Number },
        { title: 'Target', dataIndex: 'target_' + d.id, inputType: InputType.Number },
        { title: 'Actual', dataIndex: 'actual_' + d.id, inputType: InputType.Number },
      ],
    };
  });

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'group_id',
      inputType: InputType.None,
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'commonField.date' }),
      dataIndex: 'date_moment',
      inputType: InputType.Date,
      fixed: 'left',
    },
    ...dynamicColumns,
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      fixed: 'right',
    },
  ];

  const handleToggleEdit = (form: FormInstance<Record> | null, record: Record) => {
    const editedDataIndex = tableData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);
    const currentData = [...tableData];
    currentData[editedDataIndex] = editedData;
    setTableData(currentData);
  };

  const handleSave = async (form: FormInstance<Record> | null, record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = tableData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();
      if (values) {
        values.date = values?.date_moment.format('YYYY-MM-DD') as string;
        values.kpi_indicator_id = selectedIndicatorId as number;
        values.player_id = selectedPlayerId as number;
        values.group_id = record.group_id;
      }

      let savedData: Partial<Record> = {};
      const createRes = await create('kpi_values', values);
      savedData = createRes.data;
      savedData.date_moment = moment(savedData.date);

      let currentData = [...tableData];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString(), no: i + 1 };
      });
      hideLoading();
      setIsEditMode(false);
      setTableData(currentData);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = tableData.findIndex((d) => d.key === record.key);
      if (record.group_id) {
        await destroy('kpi_values', record.group_id);
      }
      let currentData = [...tableData];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setTableData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      setIsEditMode(false);
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const mapColumns = (col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Record) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    if (col.children) {
      newCol.children = col.children.map(mapColumns);
    }
    return newCol;
  };

  const processedColumns = columns.map(mapColumns);

  const handleAddRow = () => {
    const newData = [...tableData];
    newData.push({
      group_id: newData.length + 1,
      edited: true,
      key: newData.length.toString(),
    });
    setIsEditMode(!isEditMode);
    setTableData([...newData]);
  };

  const footer = () => {
    if (!selectedIndicatorId || !selectedPlayerId || isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      dataSource={tableData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={{ size: 'small' }}
    />
  );
};

export default MonitoringTable;
