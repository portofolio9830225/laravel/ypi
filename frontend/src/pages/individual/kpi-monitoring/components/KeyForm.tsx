import { Card, Select, Space } from 'antd';
import { FC, useContext, CSSProperties } from 'react';
import { Context } from '../index';

interface KeyFormProps {}

const selectStyles: CSSProperties = {
  width: '12rem',
};

const KeyForm: FC<KeyFormProps> = () => {
  const { playerData, indicatorData, setSelectedIndicatorId, setSelectedPlayerId } =
    useContext(Context);
  const handlePlayerChange = (value: number) => {
    setSelectedPlayerId(value);
  };
  const handleIndicatorChange = (value: number) => {
    setSelectedIndicatorId(value);
  };
  return (
    <Card>
      <Space>
        <Select onChange={handlePlayerChange} placeholder="Select Player..." style={selectStyles}>
          {playerData.map((d) => (
            <Select.Option key={d.id} value={d.id}>
              {d.nama_lengkap}
            </Select.Option>
          ))}
        </Select>
        <Select
          onChange={handleIndicatorChange}
          placeholder="Select Indicator..."
          style={selectStyles}
        >
          {indicatorData.map((d) => (
            <Select.Option key={d.id} value={d.id}>
              {d.name}
            </Select.Option>
          ))}
        </Select>
      </Space>
    </Card>
  );
};

export default KeyForm;
