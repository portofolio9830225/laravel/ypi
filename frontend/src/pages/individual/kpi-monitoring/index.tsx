import Table from './components/Table';
import KeyForm from './components/KeyForm';
import { useEffect, createContext, useState } from 'react';
import { useIntl } from 'umi';
import { KpiIndicator, Player } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import { message } from 'antd';
import { Moment } from 'moment';

interface DevType {
  edited: boolean;
  key: string;
  no: number;
  date: string;
  date_moment: Moment;
  kpi_indicator_id: number;
  player_id: number;
  group_id: number;
  // pb_1, actual_1, target_1 etc..
}

export interface Record extends DevType {}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.kpi-setup.' + fieldName;
};

export interface ContextType {
  selectedIndicatorId: number | undefined;
  setSelectedIndicatorId: React.Dispatch<React.SetStateAction<number | undefined>>;
  indicatorData: Partial<KpiIndicator>[];
  setIndicatorData: React.Dispatch<React.SetStateAction<Partial<KpiIndicator>[]>>;
  playerData: Partial<Player>[];
  setPlayerData: React.Dispatch<React.SetStateAction<Partial<Player>[]>>;
  selectedPlayerId: number | undefined;
  setSelectedPlayerId: React.Dispatch<React.SetStateAction<number | undefined>>;
}

export const Context = createContext<ContextType>({
  selectedIndicatorId: undefined,
  setSelectedIndicatorId: () => {},
  indicatorData: [],
  setIndicatorData: () => {},
  playerData: [],
  setPlayerData: () => {},
  selectedPlayerId: undefined,
  setSelectedPlayerId: () => {},
});

export default () => {
  const intl = useIntl();
  const [selectedIndicatorId, setSelectedIndicatorId] = useState<number>();
  const [playerData, setPlayerData] = useState<Partial<Player>[]>([]);
  const [indicatorData, setIndicatorData] = useState<Partial<KpiIndicator>[]>([]);
  const [selectedPlayerId, setSelectedPlayerId] = useState<number>();

  const fetchData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const indicatorsRes = await get('kpi_indicators');
      setIndicatorData(indicatorsRes.data);
      const playerRes = await get('master_pemains');
      setPlayerData(playerRes.data);
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const contextValue: ContextType = {
    selectedIndicatorId,
    setSelectedIndicatorId,
    indicatorData,
    setIndicatorData,
    playerData,
    setPlayerData,
    selectedPlayerId,
    setSelectedPlayerId,
  };

  return (
    <>
      <Context.Provider value={contextValue}>
        <KeyForm />
        <Table />
      </Context.Provider>
    </>
  );
};
