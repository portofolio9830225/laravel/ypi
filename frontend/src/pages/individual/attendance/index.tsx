import { Col, message, Row, Form, FormInstance, Tabs, Card } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Table2 from './components/Table2';
import ManualInputForm from './components/Form';
import KeyForm from './components/KeyForm';
import KeyForm2 from './components/KeyForm2';
import { useIntl } from 'umi';
import { Attendance as Item, GroupClassification, Coach, AttendanceStatus } from '@/types/ypi';
import { Moment } from 'moment';

export const tableName = 'training_attendances';

export interface KeyForm {
  kelompok_id: number;
  master_pelatih_id: number;
  pemain_id: number;
}

export interface KeyForm2 {
  group_id: number;
  coach_id: number;
  session: number;
  date_moment: Moment;
}

interface DevType {
  edited: boolean;
  key: string;
}

export interface RecordDaily extends DevType {
  player_id: number;
  player_name: string;
  presence_id: number;
  note: string;
  disable_note: boolean;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.attendance.' + fieldName;
};

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: (playerId: number, coachId: number, groupId: number) => void;
  moduleName: string;
  groupData: GroupClassification[];
  coachData: Coach[];
  keyForm: FormInstance<KeyForm> | undefined;
  keyData2: KeyForm2 | undefined;
  attendanceStatusData: AttendanceStatus[];
  setKeyData2: React.Dispatch<React.SetStateAction<KeyForm2 | undefined>>;
  setAttendanceStatusData: React.Dispatch<React.SetStateAction<AttendanceStatus[]>>;
  dailyData: RecordDaily[];
  setDailyData: React.Dispatch<React.SetStateAction<RecordDaily[]>>;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
  groupData: [],
  coachData: [],
  keyForm: undefined,
  keyData2: undefined,
  setKeyData2: () => {},
  attendanceStatusData: [],
  setAttendanceStatusData: () => {},
  dailyData: [],
  setDailyData: () => {},
});

export default () => {
  const intl = useIntl();
  const pageHeaderHeight = 250;
  const [data, setData] = useState<Item[]>([]);
  const [dailyData, setDailyData] = useState<RecordDaily[]>([]);
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const [groupData, setGroupData] = useState<GroupClassification[]>([]);
  const [coachData, setCoachData] = useState<Coach[]>([]);
  const [moduleName] = useState<string>(intl.formatMessage({ id: 'menu.individual.attendance' }));
  const [keyForm] = Form.useForm<KeyForm>();
  const [keyData2, setKeyData2] = useState<KeyForm2>();
  const [attendanceStatusData, setAttendanceStatusData] = useState<AttendanceStatus[]>([]);

  const getTableData = async (playerId: number, coachId: number, groupId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get(
        `${tableName}?kelompok_id=${groupId}&master_pelatih_id=${coachId}&pemain_id=${playerId}`,
      );
      setData(
        (dataRes.data as Item[]).map((d: Item, index) => {
          return {
            ...d,
            key: d.id.toString(),
            tanggal: d.tanggal.substring(0, 10),
          };
        }),
      );
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Table Data' }));
    }
    hideLoading();
  };

  const fetchDependenciesData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const playerRes = await get('tabel_kelompoks');
      setGroupData(playerRes.data);
      const coachRes = await get('master_pelatihs');
      setCoachData(coachRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    moduleName,
    groupData,
    coachData,
    keyForm,
    attendanceStatusData,
    setAttendanceStatusData,
    dailyData,
    setDailyData,
    keyData2,
    setKeyData2,
  };

  return (
    <>
      <Card>
        <Context.Provider value={contextValue}>
          <Tabs defaultActiveKey="2" type="card">
            <Tabs.TabPane tab="Manual Input" key="1">
              <Row gutter={[16, 16]}>
                <Col span={12}>
                  <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px)`} />
                </Col>
                <Col span={12}>
                  <KeyForm />
                  <ManualInputForm />
                </Col>
              </Row>
            </Tabs.TabPane>
            <Tabs.TabPane key="2" tab="Daily Input">
              <KeyForm2 />
              <Table2 />
            </Tabs.TabPane>
          </Tabs>
        </Context.Provider>
      </Card>
    </>
  );
};
