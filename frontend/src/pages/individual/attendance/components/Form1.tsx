import {
  Form,
  DatePicker,
  Select,
  Input,
  FormInstance,
  message,
  Divider,
  Space,
  Button,
} from 'antd';
import { FormattedMessage, useIntl, useHistory } from 'umi';
import { useState, useEffect, useContext } from 'react';
import { Attendance, AttendanceSetting } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import { Context } from '../index';
import moment, { Moment } from 'moment';
import { EditOutlined } from '@ant-design/icons';

const { Option } = Select;

export default ({ form }: { form: FormInstance }) => {
  const intl = useIntl();
  const history = useHistory();
  const { editedData, attendanceStatusData, keyForm } = useContext(Context);
  const [disableNote, setDisableNote] = useState<boolean>(false);
  const [attendanceSettingData, setAttendanceSettingData] = useState<AttendanceSetting[]>([]);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: (
          <FormattedMessage
            id="message.pleaseInputField"
            values={{ fieldName: intl.formatMessage({ id: 'pages.attendance.' + fieldName }) }}
          />
        ),
      },
    ];
  };

  const onValuesChange = (changedValues: any, values: Attendance) => {
    if (changedValues.presence_id) {
      const selectedStatus = attendanceStatusData.find((d) => d.id == changedValues.presence_id);
      setDisableNote(selectedStatus?.presence as boolean);
      form.setFieldsValue({ note: '-' });
    }
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue({ ...editedData, tanggal_moment: moment(editedData.tanggal) });
      fetchAttendanceSetting(editedData.tanggal as string);
    }
  }, [editedData]);

  const fetchAttendanceSetting = (dateString: string) => {
    const keyData = keyForm?.getFieldsValue();
    get(
      `setting_attendances?kelompok=${keyData?.kelompok_id}&kode_pelatih=${keyData?.master_pelatih_id}&tanggal=${dateString}`,
    )
      .then((res) => setAttendanceSettingData(res.data))
      .catch((err) =>
        message.error(
          intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Session Data' }),
        ),
      );
  };

  const handleDateChange = (date: Moment | null, dateString: string) => {
    fetchAttendanceSetting(dateString);

    if (date == null) {
      setAttendanceSettingData([]);
      form.setFieldsValue({ session: undefined });
    } else {
      form.setFieldsValue({ tanggal: dateString });
    }
  };

  const handleToSettingAttendance = () => {
    history.push('/setting/attendance-setting');
  };

  return (
    <Form
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      name="form"
      form={form}
      layout="horizontal"
      onValuesChange={onValuesChange}
    >
      <Form.Item name="id" hidden />

      <Form.Item
        name="tanggal_moment"
        label={<FormattedMessage id="pages.attendance.date" />}
        rules={getRequiredRule('date')}
      >
        <DatePicker onChange={handleDateChange} />
      </Form.Item>

      <Form.Item name="tanggal" hidden />

      <Form.Item
        name="session"
        label={<FormattedMessage id="pages.attendance.session" />}
        rules={getRequiredRule('session')}
      >
        <Select
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider />
              <Space style={{ padding: '0 8px 4px' }}>
                <Button type="primary" onClick={handleToSettingAttendance} icon={<EditOutlined />}>
                  <FormattedMessage id="crud.editData" />
                </Button>
              </Space>
            </>
          )}
        >
          {attendanceSettingData.map((d) => (
            <Option key={d.id} value={d.session}>
              {d.session}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name="presence_id"
        label={<FormattedMessage id="pages.attendance.status" />}
        rules={getRequiredRule('status')}
      >
        <Select
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider />
              <Space style={{ padding: '0 8px 4px' }}>
                <Button type="primary" onClick={handleToSettingAttendance} icon={<EditOutlined />}>
                  <FormattedMessage id="crud.editData" />
                </Button>
              </Space>
            </>
          )}
        >
          {attendanceStatusData.map((d) => (
            <Option key={d.id} value={d.id}>
              {d.keterangan}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        initialValue={'-'}
        name="note"
        label={<FormattedMessage id="pages.attendance.note" />}
      >
        <Input disabled={disableNote} />
      </Form.Item>
    </Form>
  );
};
