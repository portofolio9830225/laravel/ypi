import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
  Checkbox,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage, useHistory } from 'umi';
import { getLocaleKey, Context, RecordDaily as Record } from '../index';
import {
  EditOutlined,
  SaveOutlined,
  SettingOutlined,
  CheckOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import { create } from '@/services/ypi/api';
import styles from './table.less';

interface CustomRowProps {
  index: number;
  record: Record;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
  Checkbox,
}

const EditableContext = createContext<FormInstance<Record> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, record, ...props }) => {
  const [form] = Form.useForm();
  const { setDailyData, dailyData, keyData2 } = useContext(Context);
  const [filledForm, setFilledForm] = useState<boolean>(false);

  // useEffect(() => {
  //   if (filledForm == false) {
  //   form.setFieldsValue(dailyData[index]);
  //   setFilledForm(true);
  //   }
  // }, [dailyData]);

  // useEffect(() => {
  //   form.setFieldsValue(dailyData[index]);
  // }, [keyData2]);

  const handleValuesChange = (changedValues: any, values: any) => {
    // check one disable other checkbox
    if (
      Object.keys(changedValues).some(function (k) {
        return ~k.indexOf('status_');
      })
    ) {
      for (const property in values) {
        if (property.startsWith('status_') && property !== Object.keys(changedValues)[0]) {
          form.setFieldsValue({ [property]: undefined });
        }
        if (property === 'status_' + record.presence_id) {
          setDailyData((prev) => {
            const newData = [...prev];
            newData[index].disable_note = changedValues['status_' + record.presence_id];
            return [...newData];
          });
        }
      }
    }

    // if presence disable note
    if (Object.keys(changedValues)[0] === 'status_' + record.presence_id) {
      setDailyData((prev) => {
        const newData = [...prev];
        newData[index].disable_note = changedValues['status_' + record.presence_id];
        return [...newData];
      });
    }

    if (changedValues['status_' + record.presence_id] === true) {
      form.setFieldsValue({ note: '-' });
    }
  };

  return (
    <Form form={form} onValuesChange={handleValuesChange} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: string;
  inputType: InputType;
  fieldIndex: number;
  record: Record;
  handleToggleEdit: (form: FormInstance<Record> | null, record: Record) => void;
  handleSave: (form: FormInstance<Record> | null, record: Record) => Promise<void>;
  handleDelete: (record: Record) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (dataIndex === 'player_name') {
    return (
      <td {...props}>
        <>
          <Form.Item hidden name="player_name" />
          <Form.Item hidden name="player_id" />
          <Form.Item hidden name="presence_id" />
          {childNode}
        </>
      </td>
    );
  }

  if (inputType === InputType.Checkbox && !record.edited) {
    if (record[dataIndex]) {
      return (
        <td {...props}>
          <CheckOutlined />
        </td>
      );
    }
    return <td {...props}>{childNode}</td>;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;
    let valuePropName = undefined;

    if (inputType === InputType.Text)
      inputNode = <Input style={{ width: '100%' }} disabled={record.disable_note} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;
    if (inputType === InputType.Checkbox) {
      inputNode = <Checkbox />;
      valuePropName = 'checked';
    }

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex} valuePropName={valuePropName}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const history = useHistory();
  const { attendanceStatusData, dailyData, setDailyData, keyData2 } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);
  const data = dailyData;
  const setData = setDailyData;

  useEffect(() => {
    setIsEditMode(false);
    console.log(dailyData);
  }, [dailyData]);

  const handleToSettingAttendance = () => {
    history.push('/setting/attendance-setting');
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.player' }),
      dataIndex: 'player_name',
    },
    {
      title: (
        <Space>
          <FormattedMessage id={getLocaleKey('status')} />
          <Tooltip title="Setting">
            <Button shape="circle" icon={<SettingOutlined />} onClick={handleToSettingAttendance} />
          </Tooltip>
        </Space>
      ),
      children: attendanceStatusData.map((d) => ({
        title: d.inisial,
        inputType: InputType.Checkbox,
        dataIndex: 'status_' + d.id,
      })),
    },
    {
      title: intl.formatMessage({ id: 'commonField.note' }),
      inputType: InputType.Text,
      dataIndex: 'note',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      width: '20%',
    },
  ];

  const handleToggleEdit = (form: FormInstance<Record> | null, record: Record) => {
    const editedDataIndex = data.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    form?.resetFields();
    form?.setFieldsValue(editedData);
    setIsEditMode(!isEditMode);
    const currentData = [...data];
    currentData[editedDataIndex] = editedData;
    setData(currentData);
  };

  const handleSave = async (form: FormInstance<Record> | null, record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = data.findIndex((d) => d.key === record.key);
      const values = form?.getFieldsValue();
      const payload = {
        ...values,
        ...keyData2,
        date: keyData2?.date_moment.format('YYYY-MM-DD'),
      };
      const createRes = await create('training_attendances/store_daily', payload);
      const savedData = createRes.data;
      let currentData = [...data];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      hideLoading();
      setIsEditMode(false);
      setData(currentData);
    } catch (error) {
      setIsEditMode(false);
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = data.findIndex((d) => d.key === record.key);
      const payload = {
        ...record,
        ...keyData2,
        date: keyData2?.date_moment.format('YYYY-MM-DD'),
      };
      const createRes = await create('training_attendances/delete_daily', payload);
      const savedData = createRes.data;
      let currentData = [...data];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const mapColumns = (col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Record) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    if (col.children) {
      newCol.children = col.children.map(mapColumns);
    }
    return newCol;
  };

  const processedColumns = columns.map(mapColumns);

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      className={styles.custom_table}
      dataSource={data}
      columns={processedColumns}
      onRow={(record, index) => ({ record, index })}
      scroll={{ x: 'max-content' }}
      pagination={isEditMode ? false : { size: 'small' }}
      rowClassName={styles.densed_row}
    />
  );
};

export default EditableTable;
