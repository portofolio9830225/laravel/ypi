import { Form, Divider, Row, Col, Space, Select, DatePicker, Button, message } from 'antd';
import { Context, KeyForm, getLocaleKey, RecordDaily } from '../index';
import { useContext, useState } from 'react';
import { FormattedMessage, useAccess, useHistory, useIntl } from 'umi';
import { AttendanceSetting } from '@/types/ypi';
const { Option } = Select;
import { create, get } from '@/services/ypi/api';
import { EditOutlined } from '@ant-design/icons';
import { Moment } from 'moment';

interface FormType {
  group_id: number;
  coach_id: number;
  session: number;
  date_moment: Moment;
}

export default () => {
  const intl = useIntl();
  const history = useHistory();
  const [form] = Form.useForm<FormType>();
  const { groupData, coachData, setAttendanceStatusData, setDailyData, setKeyData2 } =
    useContext(Context);
  const [attendanceSettingData, setAttendanceSettingData] = useState<AttendanceSetting[]>([]);
  const [disableDate, setDisableDate] = useState<boolean>(true);
  const [disableSession, setDisableSession] = useState<boolean>(true);
  const access = useAccess();

  const lblGroup = intl.formatMessage({ id: 'pages.attendance.group' });
  const lblCoach = intl.formatMessage({ id: 'pages.attendance.coach' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchAttendanceStatus = async (kelompok_id: number, master_pelatih_id: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const attendanceStatusRes = await get(
        `setting_presences?kelompok_id=${kelompok_id}&pelatih_id=${master_pelatih_id}`,
      );
      setAttendanceStatusData(attendanceStatusRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Attendance Status' }),
      );
    }
    hideLoading();
  };

  const onValuesChange = (changedValues: any, values: FormType) => {
    if (changedValues.group_id || changedValues.coach_id) {
      if (values.group_id && values.coach_id) {
        fetchAttendanceStatus(values.group_id, values.coach_id);
        setDisableDate(false);
      }
    }
  };

  const fetchAttendanceSetting = async (dateString: string) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const keyData = form.getFieldsValue();
      const attendanceSetRes = await get(
        `setting_attendances?kelompok=${keyData?.group_id}&kode_pelatih=${keyData?.coach_id}&tanggal=${dateString}`,
      );
      setAttendanceSettingData(attendanceSetRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Attendance Settings' }),
      );
    }
    hideLoading();
  };

  const handleDateChange = (date: Moment | null, dateString: string) => {
    fetchAttendanceSetting(dateString);

    if (date == null) {
      setAttendanceSettingData([]);
      form.setFieldsValue({ session: undefined });
      setDisableSession(true);
    } else {
      setDisableSession(false);
    }
  };

  const handleToSettingAttendance = () => {
    history.push('/setting/attendance-setting');
  };

  const handleProcess = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formValues = await form.validateFields();
      setKeyData2(formValues);
      const data = {
        ...formValues,
        date: formValues.date_moment.format('YYYY-MM-DD'),
      };
      const dailyRes = await create('training_attendances/show_daily', data);
      setDailyData(
        (dailyRes.data as RecordDaily[]).map((d, i) => ({
          ...d,
          key: i.toString(),
          edited: false,
        })),
      );
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  return (
    <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
      <Row gutter={12}>
        <Col span={4}>
          <Form.Item name="group_id" label={lblGroup} rules={getRequiredRule(lblGroup)}>
            <Select>
              {groupData.map((d) => (
                <Option key={d.id} value={d.id}>
                  {d.kelompok}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            initialValue={access.coachId || undefined}
            name="coach_id"
            label={lblCoach}
            rules={getRequiredRule(lblCoach)}
          >
            <Select disabled={access.coachId ? true : false}>
              {coachData.map((d) => (
                <Option key={d.id} value={d.id}>
                  {d.nama_lengkap}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            name="date_moment"
            label={<FormattedMessage id="commonField.date" />}
            rules={getRequiredRule('Date')}
          >
            <DatePicker
              disabled={disableDate}
              onChange={handleDateChange}
              style={{ width: '100%' }}
            />
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item
            name="session"
            label={<FormattedMessage id="pages.attendance.session" />}
            rules={getRequiredRule('Session')}
          >
            <Select
              disabled={disableSession}
              dropdownRender={(menu) => (
                <>
                  {menu}
                  <Divider />
                  <Space style={{ padding: '0 8px 4px' }}>
                    <Button
                      type="primary"
                      onClick={handleToSettingAttendance}
                      icon={<EditOutlined />}
                    >
                      <FormattedMessage id="crud.editData" />
                    </Button>
                  </Space>
                </>
              )}
            >
              {attendanceSettingData.map((d) => (
                <Option key={d.id} value={d.session}>
                  {d.session}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item label=" ">
            <Button onClick={handleProcess}>
              <FormattedMessage id="crud.process" />
            </Button>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
