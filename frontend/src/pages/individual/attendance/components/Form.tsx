import { Button, Modal, Tabs, message, Form } from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context, tableName } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { Attendance } from '@/types/ypi';
import { create, update } from '@/services/ypi/api';
import Form1 from './Form1';
import Form2 from './Form2';
import { Moment } from 'moment';

export interface Form1 {
  id: number;
  tanggal_moment: Moment;
  session: number;
  presence_id: number;
  note: string;
}

export interface Form2 {
  date_range: [Moment, Moment] | null;
  sessions: number[];
  presence_id: number;
  note: string;
}

export default () => {
  const [form1] = Form.useForm<Form1>();
  const [form2] = Form.useForm<Form2>();
  const { moduleName, getTableData, editedData, setEditedData, keyForm } = useContext(Context);
  const intl = useIntl();
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [activeTabKey, setActiveTabKey] = useState<string>('1');

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    form1.resetFields();
    form2.resetFields();
    setEditedData(null);
  };

  useEffect(() => {
    if (editedData) {
      openModal();
    }
  }, [editedData]);

  const saveForm1 = async () => {
    try {
      const values: Form1 = await form1.validateFields();
      const keyData = await keyForm?.validateFields();

      const data = {
        ...values,
        pemain_id: keyData?.pemain_id,
        master_pelatih_id: keyData?.master_pelatih_id,
        kelompok_id: keyData?.kelompok_id,
      };

      if (values.id) {
        await update(tableName, data.id, data);
      } else {
        await create(tableName, data);
      }
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
      getTableData(
        keyData?.pemain_id as number,
        keyData?.master_pelatih_id as number,
        keyData?.kelompok_id as number,
      );
      closeModal();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  const saveForm2 = async () => {
    try {
      const values = await form2.validateFields();
      const keyData = await keyForm?.validateFields();

      const data = {
        ...values,
        player_id: keyData?.pemain_id,
        coach_id: keyData?.master_pelatih_id,
        group_id: keyData?.kelompok_id,
        start_date: values.date_range?.[0].format('YYYY-MM-DD'),
        end_date: values.date_range?.[1].format('YYYY-MM-DD'),
      };

      await create('training_attendances/batch_save', data);

      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
      getTableData(
        keyData?.pemain_id as number,
        keyData?.master_pelatih_id as number,
        keyData?.kelompok_id as number,
      );
      closeModal();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  const handleSave = () => {
    if (activeTabKey === '1') {
      saveForm1();
    } else {
      saveForm2();
    }
  };

  const handleAddNewData = async () => {
    try {
      await keyForm?.validateFields();
      openModal();
    } catch (error) {}
  };

  const handleTabChange = (activeKey: string) => {
    setActiveTabKey(activeKey);
  };

  const modalFooter = [
    <Button key="cancel" onClick={closeModal}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button key="next" type="primary" htmlType="button" onClick={handleSave}>
      <FormattedMessage id="crud.save" />
    </Button>,
  ];

  return (
    <div>
      <Button type="primary" htmlType="button" onClick={handleAddNewData}>
        <FormattedMessage id="crud.addNewData" />
      </Button>
      <Modal
        title={<FormattedMessage id={editedData ? 'crud.editData' : 'crud.addNewData'} />}
        visible={modalVisible}
        footer={modalFooter}
        onCancel={closeModal}
        centered
        bodyStyle={{ overflowY: 'scroll', maxHeight: '80%' }}
      >
        <Tabs defaultActiveKey="1" type="card" activeKey={activeTabKey} onChange={handleTabChange}>
          <Tabs.TabPane tab="Single Input" key="1">
            <Form1 form={form1} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Range Input" key="2">
            <Form2 form={form2} />
          </Tabs.TabPane>
        </Tabs>
      </Modal>
    </div>
  );
};
