import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext, useEffect, useState } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context } from '../index';
import { useIntl } from 'umi';
import { Attendance as Item } from '@/types/ypi';
import styles from './table.less';

export default ({ data, tableHeight }: { data: Item[]; tableHeight: string }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName, keyData } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData(
          keyData?.pemain_id as number,
          keyData?.master_pelatih_id as number,
          keyData?.kelompok_id as number,
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'pages.attendance.date' }),
      dataIndex: 'tanggal',
      key: 'tanggal',
    },
    {
      title: intl.formatMessage({ id: 'pages.attendance.session' }),
      dataIndex: 'session',
      key: 'session',
    },
    {
      title: intl.formatMessage({ id: 'pages.attendance.status' }),
      dataIndex: ['status', 'keterangan'],
      key: 'presence_id',
    },
    {
      title: intl.formatMessage({ id: 'pages.attendance.note' }),
      dataIndex: 'note',
      key: 'note',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="link"
              size="small"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="text" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      scroll={{ y: tableHeight }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
