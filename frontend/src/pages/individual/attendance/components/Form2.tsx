import {
  Form,
  DatePicker,
  Select,
  Input,
  FormInstance,
  message,
  Divider,
  Space,
  Button,
} from 'antd';
import { FormattedMessage, useIntl, useHistory } from 'umi';
import { useState, useContext } from 'react';
import { create } from '@/services/ypi/api';
import { Context } from '../index';
import { Moment } from 'moment';
import { Form2 } from './Form';
import { RangeValue } from 'rc-picker/lib/interface';
import { EditOutlined } from '@ant-design/icons';

const { Option } = Select;

export default ({ form }: { form: FormInstance<Form2> }) => {
  const intl = useIntl();
  const history = useHistory();
  const { keyForm, attendanceStatusData } = useContext(Context);
  const [disableNote, setDisableNote] = useState<boolean>(false);
  const [disableSessions, setDisableSessions] = useState<boolean>(true);
  const [sessions, setSessions] = useState<number[]>([]);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: (
          <FormattedMessage
            id="message.pleaseInputField"
            values={{ fieldName: intl.formatMessage({ id: 'pages.attendance.' + fieldName }) }}
          />
        ),
      },
    ];
  };

  const onValuesChange = (changedValues: any, values: Form2) => {
    if (changedValues.presence_id) {
      const selectedStatus = attendanceStatusData.find((d) => d.id == changedValues.presence_id);
      setDisableNote(selectedStatus?.presence as boolean);
      form.setFieldsValue({ note: '-' });
    }

    if (values.date_range === null) {
      setDisableSessions(true);
    } else if (values.date_range[0] && values.date_range[1] && disableSessions === true) {
      setDisableSessions(false);
    }
  };

  const fetchSessions = async (dates: [Moment, Moment]) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const keyValues = keyForm?.getFieldsValue();

      const sessionsRes = await create('training_attendances/sessions_range', {
        start_date: dates[0].format('YYYY-MM-DD'),
        end_date: dates[1].format('YYYY-MM-DD'),
        group_id: keyValues?.kelompok_id,
        coach_id: keyValues?.master_pelatih_id,
      });

      setSessions(sessionsRes.data);
    } catch (error) {}
    hideLoading();
  };

  const handleDateChange = (dates: RangeValue<Moment> | null, dateStrings: [string, string]) => {
    if (dates == null) {
      form.setFieldsValue({ sessions: undefined });
    } else {
      fetchSessions([dates[0] as Moment, dates[1] as Moment]);
    }
  };

  const handleToSettingAttendance = () => {
    history.push('/setting/attendance-setting');
  };

  return (
    <Form
      labelCol={{ span: 6 }}
      wrapperCol={{ span: 18 }}
      name="form"
      form={form}
      layout="horizontal"
      onValuesChange={onValuesChange}
    >
      <Form.Item
        name="date_range"
        label={<FormattedMessage id="pages.attendance.date" />}
        rules={getRequiredRule('date')}
      >
        <DatePicker.RangePicker onChange={handleDateChange} />
      </Form.Item>

      <Form.Item
        name="sessions"
        label={<FormattedMessage id="pages.attendance.session" />}
        rules={getRequiredRule('session')}
      >
        <Select
          mode="multiple"
          disabled={disableSessions}
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider />
              <Space style={{ padding: '0 8px 4px' }}>
                <Button type="primary" onClick={handleToSettingAttendance} icon={<EditOutlined />}>
                  <FormattedMessage id="crud.editData" />
                </Button>
              </Space>
            </>
          )}
        >
          {sessions.map((d, i) => (
            <Option key={i} value={d}>
              {d}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        name="presence_id"
        label={<FormattedMessage id="pages.attendance.status" />}
        rules={getRequiredRule('status')}
      >
        <Select
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider />
              <Space style={{ padding: '0 8px 4px' }}>
                <Button type="primary" onClick={handleToSettingAttendance} icon={<EditOutlined />}>
                  <FormattedMessage id="crud.editData" />
                </Button>
              </Space>
            </>
          )}
        >
          {attendanceStatusData.map((d) => (
            <Option key={d.id} value={d.id}>
              {d.keterangan}
            </Option>
          ))}
        </Select>
      </Form.Item>

      <Form.Item
        initialValue={'-'}
        name="note"
        label={<FormattedMessage id="pages.attendance.note" />}
      >
        <Input disabled={disableNote} />
      </Form.Item>
    </Form>
  );
};
