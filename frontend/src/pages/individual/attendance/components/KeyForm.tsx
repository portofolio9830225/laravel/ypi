import { Form, Card, Row, Col, message, Select } from 'antd';
import { Context, KeyForm } from '../index';
import { useContext, useState } from 'react';
import { useAccess, useIntl } from 'umi';
import { Player } from '@/types/ypi';
const { Option } = Select;
import { get } from '@/services/ypi/api';

export default () => {
  const intl = useIntl();
  const { getTableData, groupData, coachData, keyForm, setAttendanceStatusData } =
    useContext(Context);
  const access = useAccess();
  const [playerData, setPlayerData] = useState<Player[]>([]);

  const lblGroup = intl.formatMessage({ id: 'pages.attendance.group' });
  const lblCoach = intl.formatMessage({ id: 'pages.attendance.coach' });
  const lblPlayer = intl.formatMessage({ id: 'pages.attendance.player_name' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchDepData = async (kelompok_id: number, master_pelatih_id: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const statusRes = await get(
        `setting_presences?kelompok_id=${kelompok_id}&pelatih_id=${master_pelatih_id}`,
      );
      setAttendanceStatusData(statusRes.data);
      const playerRes = await get(
        `master_pemains?coach_id=${master_pelatih_id}&group_id=${kelompok_id}`,
      );
      setPlayerData(playerRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
    hideLoading();
  };

  const onValuesChange = (changedValues: any, values: KeyForm) => {
    if (values.kelompok_id && values.pemain_id && values.master_pelatih_id) {
      getTableData(values.pemain_id, values.master_pelatih_id, values.kelompok_id);
    }

    if (changedValues.kelompok_id || changedValues.master_pelatih_id) {
      if (values.kelompok_id && values.master_pelatih_id) {
        fetchDepData(values.kelompok_id, values.master_pelatih_id);
      }
    }
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form onValuesChange={onValuesChange} form={keyForm} name="form" layout="vertical">
        <Row gutter={12}>
          <Col span={8}>
            <Form.Item name="kelompok_id" label={lblGroup} rules={getRequiredRule(lblGroup)}>
              <Select>
                {groupData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.kelompok}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              name="master_pelatih_id"
              initialValue={access.coachId || undefined}
              label={lblCoach}
              rules={getRequiredRule(lblCoach)}
            >
              <Select disabled={access.coachId ? true : false}>
                {coachData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item name="pemain_id" label={lblPlayer} rules={getRequiredRule(lblPlayer)}>
              <Select>
                {playerData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
