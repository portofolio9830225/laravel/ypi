import {
  Form,
  Space,
  Card,
  Button,
  Row,
  Col,
  message,
  DatePicker,
  Select,
  FormInstance,
} from 'antd';
import { getLocaleKey } from '../index';
import { create, get, destroy } from '@/services/ypi/api';
import { createContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import {
  KpiEvent,
  KpiPhysic,
  Player,
  CompetitionDetail,
  ResultCategory,
  HeaderKpi,
} from '@/types/ypi';
import { Moment } from 'moment';
import EventTable from './EventTable';
import PhysicTable from './PhysicTable';
import moment from 'moment';
import CopyDialog from './CopyDialog';

const { Option } = Select;
const NEW_DATA = 'NEW_DATA';

interface Kpi {
  header_kpi_id: number;
  player_id: number;
  date: string;
  date_moment: Moment;
  keterangans: {
    keterangan_1: string;
    keterangan_2: string;
    keterangan_3: string;
    keterangan_4: string;
  };
  tanggals: {
    tanggal_1: string;
    tanggal_2: string;
    tanggal_3: string;
    tanggal_4: string;
  };
  tanggals_moment: {
    tanggal_moment_1: Moment;
    tanggal_moment_2: Moment;
    tanggal_moment_3: Moment;
    tanggal_moment_4: Moment;
  };
  events: KpiEvent[];
  deletedEvents?: KpiEvent[];
  physics: KpiPhysic[];
  deletedPhysics: KpiPhysic[];
}

export interface ContextType {
  form: FormInstance<Kpi> | undefined;
  competitionData: string[] | undefined;
  resultData: string[] | undefined;
  getRequiredRule: (fieldName: string) => Array<{}>;
}

export const Context = createContext<ContextType>({
  form: undefined,
  competitionData: undefined,
  resultData: undefined,
  getRequiredRule: () => [],
});

export default () => {
  const [form] = Form.useForm<Kpi>();
  const intl = useIntl();
  const [playerData, setPlayerData] = useState<Player[]>();
  const date = Form.useWatch('date', form);
  const [competitionData, setCompetitionData] = useState<string[]>();
  const [resultData, setResultData] = useState<string[]>();
  const [headerKpiData, setHeaderKpiData] = useState<HeaderKpi[]>();
  const [visibleCopyDialog, setVisibleCopyDialog] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const playerRes = await get('master_pemains');
      setPlayerData(playerRes.data);

      const competitionRes = await get('master_achievements');
      const competitionData: CompetitionDetail[] | undefined = competitionRes.data;
      setCompetitionData(competitionData?.map((d) => d.nama_event));

      const resultRes = await get('result_categories');
      const resultData: ResultCategory[] | undefined = resultRes.data;
      setResultData(resultData?.map((d) => d.result));
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const handleSave = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    // console.log(form.getFieldsValue());
    // return;
    try {
      const data = await form.validateFields();
      if (data.date_moment) {
        data.date = data.date_moment.format('YYYY-MM-DD');
      }
      if (data.events) {
        data.events = data.events.map((d) => {
          const event = {
            ...d,
            ...data.keterangans,
            tanggal_kpi: data.date,
            pemain_id: data.player_id,
          };
          for (const key in event) {
            if (!event[key]) delete event[key];
          }
          return event;
        });
      }
      const physicDates = {};
      for (const key in data.tanggals_moment) {
        if (data.tanggals_moment[key]) {
          const date: Moment = data.tanggals_moment[key];
          physicDates[key.replace('_moment', '')] = date.format('YYYY-MM-DD');
        }
      }
      if (data.physics) {
        data.physics = data.physics.map((d) => {
          return { ...d, ...physicDates, tanggal_kpi: data.date, pemain_id: data.player_id };
        });
      }
      console.log(data);
      await create('kpi', data);
      hideLoading();
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Data' }));
      form.resetFields();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
      hideLoading();
    }
  };

  const handleCancel = () => {
    form.resetFields();
  };

  const handleDelete = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const date = form.getFieldValue('date');
      if (date && date !== NEW_DATA) {
        const selectedHeaderKpi = headerKpiData?.find((d) => d.tanggal_kpi == date);
        if (selectedHeaderKpi) await destroy('kpi', selectedHeaderKpi.id);
        form.resetFields();
        hideLoading();
        message.success(
          intl.formatMessage({ id: 'message.successDelete' }, { moduleName: 'Data' }),
        );
      }
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
      hideLoading();
    }
  };

  const handleCopyToPlayer = () => {
    setVisibleCopyDialog(true);
  };

  const handleCopyCancel = () => {
    setVisibleCopyDialog(false);
  };

  const handleCopyOk = (playerId: number) => {
    form.setFieldsValue({ player_id: playerId });
    setVisibleCopyDialog(false);
  };

  const fetchDatesData = async (playerId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const headerKpiRes = await get(`header_kpis?pemain_id=${playerId}`);
      setHeaderKpiData(headerKpiRes.data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const fetchData = async (playerId: number, date: string) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const kpiRes = await get(`kpi?pemain_id=${playerId}&tanggal_kpi=${date}`);
      const kpi: Partial<Kpi> = kpiRes.data;
      const events = kpi.events?.map((d, i) => {
        return { ...d, key: i.toString(), fieldIndex: i, no: i + 1 };
      });
      const physics = kpi.physics?.map((d, i) => {
        return { ...d, key: i.toString(), fieldIndex: i, no: i + 1 };
      });
      if (events && events.length > 0) {
        form.setFieldsValue({ events, keterangans: { ...events[0] } });
      }
      if (physics && physics.length > 0) {
        form.setFieldsValue({
          physics,
          tanggals_moment: {
            tanggal_moment_1: physics[0].tanggal_1.startsWith('-')
              ? undefined
              : moment(physics[0].tanggal_1.substring(0, 10)),
            tanggal_moment_2: physics[0].tanggal_2.startsWith('-')
              ? undefined
              : moment(physics[0].tanggal_2.substring(0, 10)),
            tanggal_moment_3: physics[0].tanggal_3.startsWith('-')
              ? undefined
              : moment(physics[0].tanggal_3.substring(0, 10)),
            tanggal_moment_4: physics[0].tanggal_4.startsWith('-')
              ? undefined
              : moment(physics[0].tanggal_4.substring(0, 10)),
          },
        });
      }
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const onValuesChange = (changedValues: Partial<Kpi>, values: Kpi) => {
    if (changedValues.player_id) {
      fetchDatesData(changedValues.player_id);
    }

    if (changedValues.date && changedValues.date !== NEW_DATA) {
      fetchData(values.player_id, changedValues.date);
    }
  };

  const contextValue: ContextType = { form, competitionData, resultData, getRequiredRule };

  return (
    <Context.Provider value={contextValue}>
      <Card>
        <Form
          form={form}
          initialValues={{
            events: [{ key: '0', fieldIndex: 0, no: 1 }],
            physics: [{ key: '0', fieldIndex: 0, no: 1 }],
            deletedEvents: [],
            deletedPhysics: [],
          }}
          name="form"
          layout="horizontal"
          onValuesChange={onValuesChange}
        >
          <Form.Item name="header_kpi_id" hidden />
          <Form.Item name="events" hidden />
          <Form.Item name="physics" hidden />
          <Form.Item name="deletedEvents" hidden />
          <Form.Item name="deletedPhysics" hidden />

          <Row gutter={6}>
            <Col span={12}>
              <Form.Item
                name="player_id"
                label={<FormattedMessage id="commonField.player" />}
                rules={getRequiredRule('player')}
              >
                <Select>
                  {playerData?.map((d, i) => (
                    <Option key={i} value={d.id}>
                      {d.nama_lengkap}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                name="date"
                label={<FormattedMessage id="commonField.date" />}
                rules={getRequiredRule('date')}
              >
                <Select>
                  <Option value={NEW_DATA}>
                    <FormattedMessage id="crud.addNewData" />
                  </Option>
                  {headerKpiData?.map((d, i) => (
                    <Option key={i} value={d.tanggal_kpi}>
                      {d.tanggal_kpi.substring(0, 10)}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="date_moment">
                <DatePicker disabled={date !== NEW_DATA} style={{ width: '100%' }} />
              </Form.Item>
            </Col>
          </Row>

          <EventTable />
          <PhysicTable />

          <Form.Item>
            <Space>
              <Button type="primary" onClick={handleSave}>
                <FormattedMessage id="crud.save" />
              </Button>
              <Button type="primary" onClick={handleDelete} danger>
                <FormattedMessage id="crud.delete" />
              </Button>
              <Button onClick={handleCancel}>
                <FormattedMessage id="crud.cancel" />
              </Button>
              <Button type="primary" onClick={handleCopyToPlayer}>
                <FormattedMessage id="crud.copy_to_next_player" />
              </Button>
            </Space>
          </Form.Item>
        </Form>
        <CopyDialog
          visible={visibleCopyDialog}
          playerData={playerData || []}
          onCancel={handleCopyCancel}
          onOk={handleCopyOk}
        />
      </Card>
    </Context.Provider>
  );
};
