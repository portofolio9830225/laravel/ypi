import { FC, useContext } from 'react';
import { FormattedMessage } from 'umi';
import { Modal, Form, Select } from 'antd';
import { Player } from '@/types/ypi';
import { Context } from './Form';

const { Option } = Select;

interface FormType {
  player_id: number;
}

interface CopyDialogProps {
  visible: boolean;
  playerData: Player[];
  onOk: (playerId: number) => void;
  onCancel: () => void;
}

const CopyDialog: FC<CopyDialogProps> = ({ visible, playerData, onOk, onCancel }) => {
  const [form] = Form.useForm<FormType>();
  const { getRequiredRule } = useContext(Context);

  const handleOk = async () => {
    const values = await form.validateFields();
    onOk(values.player_id);
  };

  const handleCancel = () => {
    form.resetFields();
    onCancel();
  };

  return (
    <Modal centered visible={visible} closable={false} onOk={handleOk} onCancel={handleCancel}>
      <Form form={form}>
        <Form.Item
          name="player_id"
          label={<FormattedMessage id="commonField.player" />}
          rules={getRequiredRule('player')}
        >
          <Select>
            {playerData?.map((d, i) => (
              <Option key={i} value={d.id}>
                {d.nama_lengkap}
              </Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default CopyDialog;
