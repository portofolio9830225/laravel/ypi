import { KpiPhysic } from '@/types/ypi';
import {
  DatePicker,
  Space,
  Tooltip,
  Popconfirm,
  Table,
  Form,
  FormInstance,
  InputNumber,
  Input,
  Button,
  TimePicker,
} from 'antd';
import { useState, FC, useEffect, useContext } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { getLocaleKey } from '../index';
import { Context } from './Form';
import { DeleteOutlined, PlusOutlined, MinusOutlined } from '@ant-design/icons';
import { Moment } from 'moment';

const FIELD_NAME = 'physics';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
}

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  return <tr {...props} />;
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof KpiPhysic;
  inputType: InputType;
  fieldIndex: number;
}

const CustomCell: FC<CellProps> = ({ children, dataIndex, fieldIndex, inputType, ...props }) => {
  const intl = useIntl();
  let childNode = children;
  const { form } = useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const disabledDate = (currentDate: Moment): boolean => {
    const start = form?.getFieldValue(['tanggal_awal']);
    const end = form?.getFieldValue(['tanggal_akhir']);
    return !currentDate.isBetween(start, end);
  };

  if (inputType !== InputType.None && inputType !== undefined) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Date)
      inputNode = <DatePicker disabledDate={disabledDate} style={{ width: '100%' }} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;

    if (dataIndex === 'keterangan') rules = getRequiredRule('physic_test');

    childNode = (
      <Form.Item rules={rules} style={style} name={[FIELD_NAME, fieldIndex, dataIndex]}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const Component: FC = () => {
  const { form } = useContext(Context);
  const intl = useIntl();
  const kpiEvents = Form.useWatch(FIELD_NAME, form);

  //   useEffect(() => {
  //     console.log(kpiEvents);
  //   }, [kpiEvents]);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      inputType: InputType.None,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('physic_test') }),
      dataIndex: 'keterangan',
      inputType: InputType.Text,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('pb') }),
      dataIndex: 'pb_1',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_1',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_1',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('pb') }),
      dataIndex: 'pb_2',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_2',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_2',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('pb') }),
      dataIndex: 'pb_3',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_3',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_3',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('pb') }),
      dataIndex: 'pb_4',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_4',
      inputType: InputType.Number,
      width: 70,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_4',
      inputType: InputType.Number,
      width: 70,
    },
  ];

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: KpiPhysic) => ({
        // record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        fieldIndex: record.fieldIndex,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    let newData: Partial<KpiPhysic>[] = form?.getFieldValue([FIELD_NAME]);
    if (newData) {
      newData.push({
        key: newData.length.toString(),
        fieldIndex: newData.length,
        no: newData.length + 1,
      });
      form?.setFieldsValue({ physics: newData });
    }
  };

  const handleDeleteRow = () => {
    let currentData: Partial<KpiPhysic>[] = form?.getFieldValue([FIELD_NAME]);
    let deletedData: Partial<KpiPhysic>[] = form?.getFieldValue(['deletedPhysics']);
    const deleted = currentData.pop();
    if (deleted && deleted.id) deletedData.push(deleted);

    form?.setFieldsValue({ deletedPhysics: deletedData });
  };

  const summary = () => {
    return (
      <Table.Summary.Row>
        <Table.Summary.Cell colSpan={2} index={0}></Table.Summary.Cell>
        <Table.Summary.Cell colSpan={3} index={2}>
          <Form.Item name={['tanggals_moment', 'tanggal_moment_1']}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>
        </Table.Summary.Cell>
        <Table.Summary.Cell colSpan={3} index={2}>
          <Form.Item name={['tanggals_moment', 'tanggal_moment_2']}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>
        </Table.Summary.Cell>
        <Table.Summary.Cell colSpan={3} index={2}>
          <Form.Item name={['tanggals_moment', 'tanggal_moment_3']}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>
        </Table.Summary.Cell>
        <Table.Summary.Cell colSpan={3} index={2}>
          <Form.Item name={['tanggals_moment', 'tanggal_moment_4']}>
            <DatePicker style={{ width: '100%' }} />
          </Form.Item>
        </Table.Summary.Cell>
      </Table.Summary.Row>
    );
  };

  const footer = () => {
    return (
      <Space>
        <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
          <Button
            onClick={handleAddRow}
            type="primary"
            size="small"
            shape="circle"
            icon={<PlusOutlined />}
          />
        </Tooltip>
        {kpiEvents && kpiEvents?.length > 1 && (
          <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
            <Button
              onClick={handleDeleteRow}
              type="primary"
              size="small"
              danger
              shape="circle"
              icon={<MinusOutlined />}
            />
          </Tooltip>
        )}
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      dataSource={kpiEvents}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      summary={summary}
    />
  );
};

export default Component;
