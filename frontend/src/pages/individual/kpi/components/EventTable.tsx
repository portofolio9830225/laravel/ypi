import { KpiEvent } from '@/types/ypi';
import {
  DatePicker,
  Space,
  Tooltip,
  Table,
  Form,
  InputNumber,
  Input,
  Button,
  TimePicker,
  Select,
  Divider,
  Typography,
} from 'antd';
import { FC, useContext, useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { getLocaleKey } from '../index';
import { Context } from './Form';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
import { Moment } from 'moment';

const { Option } = Select;
const FIELD_NAME = 'events';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  CustomDropdown,
}

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  return <tr {...props} />;
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof KpiEvent;
  inputType: InputType;
  fieldIndex: number;
}

const CustomCell: FC<CellProps> = ({ children, dataIndex, fieldIndex, inputType, ...props }) => {
  const intl = useIntl();
  let childNode = children;
  const { form, competitionData, resultData } = useContext(Context);
  const [options, setOptions] = useState<string[]>([]);
  const [newOption, setNewOption] = useState<string>('');

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const onNewOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewOption(event.target.value);
  };

  const addItem = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    if (newOption) {
      setOptions([...options, newOption]);
      setNewOption('');
    }
  };

  useEffect(() => {
    if (competitionData && dataIndex === 'event_name') {
      setOptions(competitionData);
    }
  }, [competitionData]);

  useEffect(() => {
    if (resultData && (dataIndex.startsWith('actual') || dataIndex.startsWith('target'))) {
      setOptions(resultData);
    }
  }, [resultData]);

  if (inputType !== InputType.None && inputType !== undefined) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Date) inputNode = <DatePicker style={{ width: '100%' }} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;
    if (inputType === InputType.CustomDropdown) {
      inputNode = (
        <Select
          style={{ width: '100%' }}
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider style={{ margin: '8px 0' }} />
              <Space align="center" style={{ padding: '0 8px 4px', width: '100%' }}>
                <Input
                  placeholder={intl.formatMessage({ id: 'message.enter_new_item' })}
                  value={newOption}
                  style={{ width: '100%' }}
                  onChange={onNewOptionChange}
                />
                <Typography.Link onClick={addItem} style={{ whiteSpace: 'nowrap' }}>
                  <Tooltip title={intl.formatMessage({ id: 'crud.add_item' })}>
                    <PlusOutlined />
                  </Tooltip>
                </Typography.Link>
              </Space>
            </>
          )}
        >
          {options?.map((d, i) => (
            <Option value={d} key={i}>
              {d}
            </Option>
          ))}
        </Select>
      );
    }

    if (dataIndex === 'event_name') rules = getRequiredRule('event_name');

    childNode = (
      <Form.Item rules={rules} style={style} name={[FIELD_NAME, fieldIndex, dataIndex]}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const Component: FC = () => {
  const { form } = useContext(Context);
  const intl = useIntl();
  const kpiEvents = Form.useWatch(FIELD_NAME, form);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      inputType: InputType.None,
      width: 50,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('event_name') }),
      dataIndex: 'event_name',
      inputType: InputType.CustomDropdown,
      width: 200,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_1',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_1',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_2',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_2',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_3',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_3',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('target') }),
      dataIndex: 'target_4',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual') }),
      dataIndex: 'actual_4',
      inputType: InputType.CustomDropdown,
      width: 150,
    },
  ];

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: KpiEvent) => ({
        // record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        fieldIndex: record.fieldIndex,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    let newData: Partial<KpiEvent>[] = form?.getFieldValue([FIELD_NAME]);
    if (newData) {
      newData.push({
        key: newData.length.toString(),
        fieldIndex: newData.length,
        no: newData.length + 1,
      });
      form?.setFieldsValue({ events: newData });
    }
  };

  const handleDeleteRow = () => {
    let currentData: Partial<KpiEvent>[] = form?.getFieldValue([FIELD_NAME]);
    let deletedData: Partial<KpiEvent>[] = form?.getFieldValue(['deletedEvents']);
    const deleted = currentData.pop();
    if (deleted && deleted.id) deletedData.push(deleted);

    form?.setFieldsValue({ deletedEvents: deletedData as KpiEvent[] });
  };

  const summary = () => {
    return (
      <Table.Summary.Row>
        <Table.Summary.Cell colSpan={2} index={0}></Table.Summary.Cell>
        <Table.Summary.Cell colSpan={2} index={2}>
          <Form.Item name={['keterangans', 'keterangan_1']}>
            <Input placeholder={intl.formatMessage({ id: getLocaleKey('partai') })} />
          </Form.Item>
        </Table.Summary.Cell>
        <Table.Summary.Cell colSpan={2} index={2}>
          <Form.Item name={['keterangans', 'keterangan_2']}>
            <Input placeholder={intl.formatMessage({ id: getLocaleKey('partai') })} />
          </Form.Item>
        </Table.Summary.Cell>
        <Table.Summary.Cell colSpan={2} index={2}>
          <Form.Item name={['keterangans', 'keterangan_3']}>
            <Input placeholder={intl.formatMessage({ id: getLocaleKey('partai') })} />
          </Form.Item>
        </Table.Summary.Cell>
        <Table.Summary.Cell colSpan={2} index={2}>
          <Form.Item name={['keterangans', 'keterangan_4']}>
            <Input placeholder={intl.formatMessage({ id: getLocaleKey('partai') })} />
          </Form.Item>
        </Table.Summary.Cell>
      </Table.Summary.Row>
    );
  };

  const footer = () => {
    return (
      <Space>
        <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
          <Button
            onClick={handleAddRow}
            type="primary"
            size="small"
            shape="circle"
            icon={<PlusOutlined />}
          />
        </Tooltip>
        {kpiEvents && kpiEvents?.length > 1 && (
          <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
            <Button
              onClick={handleDeleteRow}
              type="primary"
              size="small"
              danger
              shape="circle"
              icon={<MinusOutlined />}
            />
          </Tooltip>
        )}
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      dataSource={kpiEvents}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      summary={summary}
    />
  );
};

export default Component;
