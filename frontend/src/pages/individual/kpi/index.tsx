import Form from './components/Form';
import { useIntl, history } from 'umi';
import { LeftOutlined } from '@ant-design/icons';

export interface Item {
  key: string;
  id: number;
  gym_description: string;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.kpi.' + fieldName;
};

export default () => {
  const intl = useIntl();

  return <Form />;
};
