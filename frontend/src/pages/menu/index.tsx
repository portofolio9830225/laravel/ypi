import { Row, Col, Steps, Space, Button, Avatar } from 'antd';
import styles from './index.less';
import bg from './images/bg.png';
import logo from './images/logo_bypro.png';
import menuOutline from './images/menu_out.png';
import menuBg from './images/menu_bg.png';
import { useIntl, Link, FormattedMessage, useModel, history, useAccess } from 'umi';
import { useEffect, useMemo, useState } from 'react';
import moment from 'moment';
import type { Moment } from 'moment';
import { EditFilled, CloseCircleFilled, UserOutlined } from '@ant-design/icons';
import { logout } from '@/services/ypi/api';
import { User } from '@/types/ypi';
import {
  MENU_FILE_COLOR,
  MENU_REPORT_COLOR,
  MENU_INDIVIDUAL_COLOR,
  MENU_SYSTEM_COLOR,
  MENU_MASTER_PLAN_COLOR,
  MENU_DATA_MANAGEMENT_COLOR,
} from '@/utils/constant';
moment.locale('en-US');

const { Step } = Steps;

enum ListMenu {
  Idle = 1,
  File,
  Individual,
  DataManagement,
  MasterPlan,
  System,
  Report,
}

interface Menu {
  title: string;
  link: string;
}

interface MenuData {
  name: ListMenu;
  color: string;
  menus: Menu[];
}

const Clock = ({ time }: { time: Moment }) => {
  return (
    <div className={styles.clock}>
      <img src={logo} alt="logo_bypro" />
      <h1>{time.format('HH:mm')}</h1>
      <h5>{time.format('ddd, DD-MM-YYYY')}</h5>
    </div>
  );
};

const SubMenu = ({
  activeMenu,
  title,
  color,
  link,
}: {
  activeMenu: ListMenu;
  title: string;
  color: string;
  link: string;
}) => {
  return (
    <div style={{ backgroundColor: color }} className={styles.subMenu}>
      <Link to={link}>{title}</Link>
    </div>
  );
};

export default () => {
  const intl = useIntl();
  const [time, setTime] = useState<Moment>(moment());
  const [activeMenu, setActiveMenu] = useState<ListMenu>(ListMenu.Idle);
  const [menuData, setMenuData] = useState<MenuData>();
  const [currentUser, setCurrentUser] = useState<Partial<User>>();
  const { setInitialState, initialState } = useModel('@@initialState');
  const access = useAccess();

  const menusData = useMemo(() => {
    const userManagementMenu = access.canAdmin
      ? [
          {
            title: intl.formatMessage({ id: 'menu.system.users-management' }),
            link: '/system/users-management',
          },
          {
            title: intl.formatMessage({ id: 'menu.system.clubs-management' }),
            link: '/system/clubs-management',
          },
        ]
      : [];

    const editClubMenu = access.canEditClub
      ? [
          {
            title: intl.formatMessage({ id: 'menu.system.club-profile' }),
            link: '/system/club-profile',
          },
        ]
      : [];

    const kpiSettingMenu = access.canEditClub
      ? [
          {
            title: intl.formatMessage({ id: 'menu.system.kpi-setup' }),
            link: '/individual/kpi-setup',
          },
        ]
      : [];

    return [
      {
        name: ListMenu.File,
        color: MENU_FILE_COLOR,
        menus: [
          {
            title: intl.formatMessage({ id: 'menu.file.group-classification' }),
            link: '/file/group-classification',
          },
          {
            title: intl.formatMessage({ id: 'menu.file.competition-detail.competition-detail' }),
            link: '/file/competition-detail/competition-detail',
          },
          {
            title: intl.formatMessage({ id: 'menu.file.phonebook' }),
            link: '/file/phonebook',
          },
          {
            title: intl.formatMessage({ id: 'menu.file.coach-data' }),
            link: '/file/coach-data',
          },
          {
            title: intl.formatMessage({ id: 'menu.file.player-data' }),
            link: '/file/player-data',
          },
          {
            title: intl.formatMessage({ id: 'menu.file.group-player-by-coach' }),
            link: '/file/group-player-by-coach',
          },
          {
            title: intl.formatMessage({ id: 'menu.setting.national-height' }),
            link: '/setting/national-height',
          },
        ],
      },
      {
        name: ListMenu.Individual,
        color: MENU_INDIVIDUAL_COLOR,
        menus: [
          {
            title: intl.formatMessage({ id: 'menu.individual.competition-report' }),
            link: '/individual/competition-report/competition-report',
          },
          {
            title: intl.formatMessage({ id: 'menu.individual.medical-record' }),
            link: '/individual/medical-record',
          },
          {
            title: intl.formatMessage({ id: 'menu.setting.attendance-setting' }),
            link: '/setting/attendance-setting',
          },
          {
            title: intl.formatMessage({ id: 'menu.individual.attendance' }),
            link: '/individual/attendance',
          },
          ...kpiSettingMenu,
          {
            title: intl.formatMessage({ id: 'menu.individual.kpi' }),
            link: '/individual/kpi',
          },
        ],
      },
      {
        name: ListMenu.DataManagement,
        color: MENU_DATA_MANAGEMENT_COLOR,
        menus: [
          {
            title: intl.formatMessage({ id: 'menu.setting.sub-sc-setting' }),
            link: '/setting/sub-sc-setting',
          },
          {
            title: intl.formatMessage({ id: 'menu.data-management.strength-conditioning' }),
            link: '/data-management/strength-conditioning',
          },
          {
            title: intl.formatMessage({ id: 'menu.data-management.strength-conditioning-data' }),
            link: '/data-management/strength-conditioning-data',
          },
          {
            title: intl.formatMessage({ id: 'menu.data-management.fitness-monitor-tool' }),
            link: '/data-management/fitness-monitor-tool',
          },
          {
            title: intl.formatMessage({ id: 'menu.data-management.fitness-test' }),
            link: '/data-management/fitness-test',
          },
          {
            title: intl.formatMessage({ id: 'menu.data-management.maturational-status' }),
            link: '/data-management/maturational-status',
          },
        ],
      },
      {
        name: ListMenu.MasterPlan,
        color: MENU_MASTER_PLAN_COLOR,
        menus: [
          {
            title: intl.formatMessage({ id: 'menu.master-plan.ypi' }),
            link: '/master-plan/ypi',
          },
          {
            title: intl.formatMessage({ id: 'menu.setting.coach-agenda-setting' }),
            link: '/setting/coach-agenda-setting',
          },
          {
            title: intl.formatMessage({ id: 'menu.master-plan.coach-agenda' }),
            link: '/master-plan/coach-agenda',
          },
          {
            title: intl.formatMessage({ id: 'menu.master-plan.ytp' }),
            link: '/master-plan/ytp',
          },
          {
            title: intl.formatMessage({ id: 'menu.master-plan.wtp' }),
            link: '/master-plan/wtp',
          },
        ],
      },
      {
        name: ListMenu.System,
        color: MENU_SYSTEM_COLOR,
        menus: [
          {
            title: intl.formatMessage({ id: 'menu.system.edit-profile' }),
            link: '/system/edit-profile',
          },
          ...editClubMenu,
          ...userManagementMenu,
        ],
      },
      {
        name: ListMenu.Report,
        color: MENU_REPORT_COLOR,
        menus: [
          {
            title: intl.formatMessage({ id: 'menu.report.vo2max-ca' }),
            link: '/report/vo2max-ca',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.benchpress-squat' }),
            link: '/report/benchpress-squat',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.kpi-comparison' }),
            link: '/report/kpi-comparison',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.data-player' }),
            link: '/report/data-player',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.tournament-detail' }),
            link: '/report/tournament-detail',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.medical-record' }),
            link: '/report/medical-record',
          },
          // {
          //   title: intl.formatMessage({ id: 'menu.report.attendance' }),
          //   link: '/report/attendance',
          // },
          {
            title: intl.formatMessage({ id: 'menu.report.fitness-test' }),
            link: '/report/fitness-test',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.progress-fitness' }),
            link: '/report/progress-fitness',
          },
          {
            title: intl.formatMessage({ id: 'menu.report.fitness-tool' }),
            link: '/report/fitness-tool',
          },
        ],
      },
    ];
  }, [intl]);

  useEffect(() => {
    const interval = setInterval(() => setTime(moment()), 60000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  useEffect(() => {
    if (initialState && initialState.currentUser) {
      setCurrentUser(initialState.currentUser);
    }
  }, [initialState]);

  useEffect(() => {
    const selectedMenuData: MenuData | undefined = menusData.find((d) => d.name == activeMenu);
    if (selectedMenuData) setMenuData(selectedMenuData);
  }, [activeMenu, menusData]);

  const customDot = () => (
    <div style={{ backgroundColor: menuData?.color }} className={styles.customDot} />
  );

  const loginOut = async () => {
    try {
      await logout();
    } catch (error) {
      console.log(error);
    }
    // const { query = {}, pathname } = history.location;
    // const { redirect } = query;
    // Note: There may be security issues, please note
    if (window.location.pathname !== '/user/login') {
      history.replace({
        pathname: '/user/login',
        // search: stringify({
        //   redirect: pathname,
        // }),
      });
    }
  };

  const clearStorage = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
  };

  const handleLogout = () => {
    loginOut();
    setInitialState((s) => ({ ...s, currentUser: undefined }));
    clearStorage();
  };

  const handleEditProfile = () => {
    history.push('/system/edit-profile');
  };

  return (
    <>
      <div className={styles.header}>
        <div className={styles.headerContent}>
          <div className={styles.headerLeft}>
            <h3>
              <FormattedMessage id="pages.menu.ypi_online" />
            </h3>
          </div>
          <div className={styles.headerRight}>
            <div>
              <p>
                <FormattedMessage id="pages.menu.hello" />
                <span style={{ textTransform: 'capitalize' }}>{currentUser?.name || ''}</span>
              </p>
              <div>
                <Button
                  onClick={handleEditProfile}
                  style={{ color: '#4EBDFB' }}
                  icon={<EditFilled />}
                  type="text"
                >
                  <FormattedMessage id="pages.menu.edit_profile" />
                </Button>
                <Space>
                  <Button
                    icon={<CloseCircleFilled />}
                    style={{ color: '#FE1C2A', marginLeft: '0.4rem' }}
                    type="text"
                    onClick={handleLogout}
                  >
                    <FormattedMessage id="pages.menu.logout" />
                  </Button>
                </Space>
              </div>
            </div>
            <Avatar
              size={64}
              src={currentUser?.foto}
              icon={currentUser?.foto ? <></> : <UserOutlined />}
            />
          </div>
        </div>
      </div>
      <div className={styles.content} style={{ backgroundImage: `url(${bg})` }}>
        <Row style={{ marginTop: '4rem' }}>
          <Col span={12}>
            <div className={styles.menuContainer}>
              <div className={styles.menu} style={{ backgroundImage: menuOutline }}>
                <div className={styles.bg} style={{ backgroundImage: menuBg }}>
                  <div onClick={() => setActiveMenu(ListMenu.System)} className={styles.system} />
                  <div onClick={() => setActiveMenu(ListMenu.File)} className={styles.file} />
                  <div onClick={() => setActiveMenu(ListMenu.Report)} className={styles.report} />
                  <div
                    onClick={() => setActiveMenu(ListMenu.DataManagement)}
                    className={styles.dataManagement}
                  />
                  <div
                    onClick={() => setActiveMenu(ListMenu.Individual)}
                    className={styles.individual}
                  />
                  <div
                    onClick={() => setActiveMenu(ListMenu.MasterPlan)}
                    className={styles.masterPlan}
                  />
                </div>
              </div>
            </div>
          </Col>
          <Col span={12}>
            {activeMenu == ListMenu.Idle ? (
              <Clock time={time} />
            ) : (
              <Steps progressDot={customDot} direction="vertical" current={0}>
                {menuData?.menus.map((d, i) => (
                  <Step
                    key={i}
                    title={
                      <SubMenu
                        activeMenu={activeMenu}
                        link={d.link}
                        title={d.title}
                        color={menuData.color}
                      />
                    }
                  />
                ))}
              </Steps>
            )}
          </Col>
        </Row>
      </div>
      <span className={styles.copyright}>Property of BYPRO</span>
    </>
  );
};
