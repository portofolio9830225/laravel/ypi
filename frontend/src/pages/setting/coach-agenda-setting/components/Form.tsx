import {
  Form,
  Input,
  Card,
  Button,
  message,
  Space,
  Row,
  Col,
  Select,
  Tooltip,
  Divider,
} from 'antd';
import { tableName, Context, Item } from '../index';
import { create, get } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage, useHistory } from 'umi';
import { PlusOutlined, MinusOutlined, EditOutlined } from '@ant-design/icons';
const { Option } = Select;
import { Item as GroupActivityType } from '../../group-activity';
import { Item as YpiType } from '../../../file/ypi';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const history = useHistory();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [ypiData, setYpiData] = useState<YpiType[]>([]);
  const [groupActivityData, setGroupActivityData] = useState<GroupActivityType[]>([]);
  const [ypiDates, setYpiDates] = useState<[string, string]>(['', '']);
  const [deletedIds, setDeletedIds] = useState<number[]>([]);

  const lblYpi = intl.formatMessage({
    id: 'pages.coach-agenda-setting.master_ypi_id',
  });
  const lblGroupActivity = intl.formatMessage({
    id: 'pages.coach-agenda-setting.kelompok',
  });
  const lblDescription = intl.formatMessage({
    id: 'pages.coach-agenda-setting.description',
  });
  const lblColor = intl.formatMessage({
    id: 'pages.coach-agenda-setting.color',
  });
  const lblAction = intl.formatMessage({
    id: 'pages.coach-agenda-setting.action',
  });

  interface FormType extends Item {
    listData: Item[];
  }

  const getDependencyData = async () => {
    try {
      const ypiRes = await get('master_ypis');
      setYpiData(ypiRes.data);
      const activityRes = await get('group_activities');
      setGroupActivityData(activityRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependency Data' }),
      );
    }
  };

  useEffect(() => {
    getDependencyData();
  }, []);

  const onFinish = (values: FormType) => {
    const data = values.listData;

    data.map((d, index) => {
      data[index].master_ypi_id = values.master_ypi_id;
      data[index].kelompok = values.kelompok;
    });

    const payload = { data, deletedIds };

    create(tableName + '/batchStore', payload)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
      });

    onCancel();
  };

  useEffect(() => {
    if (editedData) {
      get(
        tableName + `?ypi_id=${editedData.master_ypi_id}&group_activity_id=${editedData.kelompok}`,
      )
        .then((res) => {
          const data: Item[] = res.data;
          form.setFieldsValue({ listData: data });
          setYpiDates([
            editedData.ypi.tanggal_mulai.substring(0, 10),
            editedData.ypi.tanggal_selesai.substring(0, 10),
          ]);
        })
        .catch((err) => {
          console.log(err);
          message.error(
            intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'National Heights' }),
          );
        });
      form.setFieldsValue(editedData);
    }
  }, [editedData]);

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
    setYpiDates(['', '']);
  };

  const onValuesChange = (changedValues: any, values: FormType) => {
    if (changedValues.master_ypi_id) {
      ypiData.map((ypi) => {
        if (ypi.id == changedValues.master_ypi_id) {
          setYpiDates([ypi.tanggal_mulai.substring(0, 10), ypi.tanggal_selesai.substring(0, 10)]);
        }
      });
    }
  };

  const handleOpenGroupActivity = () => {
    history.push('/setting/group-activity');
  };

  const handleListDelete = (listIndex: number) => {
    const deletedItem = form.getFieldValue(['listData', listIndex]);
    console.log(form.getFieldsValue());
    console.log('deleted item', deletedItem);
    if (deletedItem.id) {
      setDeletedIds((prev) => [...prev, deletedItem.id]);
    }
  };

  return (
    <Card title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}>
      <Form
        form={form}
        name="form"
        onFinish={onFinish}
        layout="vertical"
        onValuesChange={onValuesChange}
        initialValues={{ listData: [{ id: undefined }] }}
      >
        <Form.Item name="id" hidden />
        <Row gutter={24}>
          <Col span={11}>
            <Form.Item
              name="master_ypi_id"
              label={lblYpi}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblYpi },
                  ),
                },
              ]}
            >
              <Select>
                {ypiData.map((ypi) => (
                  <Option key={ypi.id} value={ypi.id}>
                    {ypi.keterangan}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={11}>
            <Form.Item
              name="kelompok"
              label={lblGroupActivity}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblGroupActivity },
                  ),
                },
              ]}
            >
              <Select
                dropdownRender={(menu) => (
                  <>
                    {menu}
                    <Divider />
                    <Space style={{ padding: '0 8px 4px' }}>
                      <Button
                        type="primary"
                        onClick={handleOpenGroupActivity}
                        icon={<EditOutlined />}
                      >
                        <FormattedMessage id="crud.editData" />
                      </Button>
                    </Space>
                  </>
                )}
              >
                {groupActivityData.map((activity) => (
                  <Option key={activity.id} value={activity.id}>
                    {activity.group}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col span={11}>
            <Form.Item label={intl.formatMessage({ id: 'commonField.startFrom' })}>
              <Input value={ypiDates[0]} disabled />
            </Form.Item>
          </Col>
          <Col span={11}>
            <Form.Item label={intl.formatMessage({ id: 'commonField.end' })}>
              <Input value={ypiDates[1]} disabled />
            </Form.Item>
          </Col>
        </Row>
        <Form.List
          name="listData"
          rules={[
            {
              validator: async (_, names) => {
                if (!names || names.length < 1) {
                  return Promise.reject(
                    new Error(
                      intl.formatMessage(
                        { id: 'message.atLeast1' },
                        { fieldName: 'Activity Data' },
                      ),
                    ),
                  );
                }
              },
            },
          ]}
        >
          {(fields, { add, remove }, { errors }) => (
            <>
              {fields.map(({ key, name, ...restField }, index) => (
                <Row key={key} gutter={24} align={index === 0 ? 'middle' : undefined}>
                  <Form.Item name={[name, 'id']} hidden initialValue={null} />
                  <Col span={2}>
                    <Form.Item
                      {...restField}
                      name={[name, 'number']}
                      label={index === 0 ? intl.formatMessage({ id: 'commonField.no' }) : ''}
                    >
                      {index + 1}
                    </Form.Item>
                  </Col>
                  <Col span={16}>
                    <Form.Item
                      {...restField}
                      name={[name, 'description']}
                      label={index === 0 ? lblDescription : ''}
                      rules={[
                        {
                          required: true,
                          message: intl.formatMessage(
                            { id: 'message.pleaseInputField' },
                            { fieldName: lblDescription },
                          ),
                        },
                      ]}
                    >
                      <Input />
                    </Form.Item>
                  </Col>
                  <Col span={3}>
                    <Form.Item
                      {...restField}
                      name={[name, 'color']}
                      label={index === 0 ? lblColor : ''}
                      rules={[
                        {
                          required: true,
                          message: intl.formatMessage(
                            { id: 'message.pleaseInputField' },
                            { fieldName: lblColor },
                          ),
                        },
                      ]}
                    >
                      <Input type="color" />
                    </Form.Item>
                  </Col>
                  <Col span={3}>
                    {fields.length > 1 ? (
                      <Form.Item
                        {...restField}
                        name={[name, 'action']}
                        label={index === 0 ? lblAction : ''}
                      >
                        <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
                          <Button
                            type="primary"
                            danger
                            onClick={() => {
                              handleListDelete(name);
                              remove(name);
                            }}
                            shape="circle"
                            icon={<MinusOutlined />}
                            size="small"
                            style={{
                              marginLeft: '8px',
                            }}
                          />
                        </Tooltip>
                      </Form.Item>
                    ) : null}
                  </Col>
                </Row>
              ))}
              <Form.ErrorList errors={errors} />
              <Form.Item>
                <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
                  <Button
                    style={{ backgroundColor: '#52c41a', borderColor: '#52c41a' }}
                    type="primary"
                    onClick={() => add()}
                    shape="circle"
                    icon={<PlusOutlined />}
                  />
                </Tooltip>
              </Form.Item>
            </>
          )}
        </Form.List>
        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
