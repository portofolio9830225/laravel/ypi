import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext, useEffect, useState } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, Item } from '../index';
import { useIntl } from 'umi';
import styles from './table.less';

export default ({ data, tableHeight }: { data: Item[]; tableHeight: string }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);

  const [editItemId, setEditItemId] = useState<number>(-1);

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName + `/1?master_ypi_id=${record.master_ypi_id}&kelompok=${record.kelompok}`)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'pages.coach-agenda-setting.master_ypi_id' }),
      dataIndex: ['ypi', 'keterangan'],
      key: 'master_ypi_id',
      width: 135,
    },
    {
      title: intl.formatMessage({ id: 'pages.coach-agenda-setting.kelompok' }),
      dataIndex: ['group_activity', 'group'],
      key: 'kelompok',
      width: 135,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      width: 35,
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              size="small"
              onClick={() => onEdit(record)}
              type="link"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button size="small" type="text" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      scroll={{ y: tableHeight }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
