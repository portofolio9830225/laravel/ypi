import { Col, message, Row } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { useIntl } from 'umi';
import { Item as YpiType } from '../../file/ypi';

export interface Item {
  key: string;
  id: number;
  master_ypi_id: number;
  kelompok: number;
  description: string;
  color: string;
  ypi: YpiType;
}

export interface ContextType {
  editedData: Item | null;
  setEditedData: React.Dispatch<React.SetStateAction<Item | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'annual_plan_settings';

export default () => {
  const pageHeaderHeight = 250;
  const [data, setData] = useState<Item[]>([]);
  const [editedData, setEditedData] = useState<Item | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.setting.coach-agenda-setting' }),
  );

  useEffect(() => {
    getTableData();
  }, []);

  useEffect(() => {
    console.log(editedData);
  }, [editedData]);

  const getTableData = () => {
    get(tableName + '?groupedList=true')
      .then((res) => {
        const fetchedData: Item[] = res.data;
        setData(
          fetchedData.map((d: Item) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <Context.Provider value={contextValue}>
      <Row gutter={[16, 16]}>
        <Col span={12}>
          <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px)`} />
        </Col>
        <Col span={12}>
          <Form />
        </Col>
      </Row>
    </Context.Provider>
  );
};
