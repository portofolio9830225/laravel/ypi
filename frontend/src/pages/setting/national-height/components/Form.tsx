import { Form, Input, Card, Button, message, Space, Row, Col, Select, Tooltip } from 'antd';
import { tableName, Context, Item } from '../index';
import { create, get } from '@/services/ypi/api';
import { useContext, useEffect } from 'react';
import { Access, useAccess, useIntl } from 'umi';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
const { Option } = Select;

const layout = {
  // labelCol: { span: 2 },
};

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const access = useAccess();

  const lblNation = intl.formatMessage({
    id: 'pages.national-height.nation',
  });
  const lblGender = intl.formatMessage({
    id: 'pages.national-height.gender',
  });
  const lblAge = intl.formatMessage({
    id: 'pages.national-height.age',
  });
  const lblHeight = intl.formatMessage({
    id: 'pages.national-height.height',
  });
  const lblAction = intl.formatMessage({
    id: 'pages.national-height.action',
  });

  interface heightFormType extends Item {
    listData: Item[];
  }

  const onFinish = (values: heightFormType) => {
    // console.log(values);
    // return;
    const data = values.listData;

    data.map((d, index) => {
      data[index].nation = values.nation;
      data[index].gender = values.gender;
    });

    create(tableName + '/batchStore', data)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
      });

    onCancel();
  };

  useEffect(() => {
    if (editedData) {
      get(tableName + `?nation=${editedData.nation}&gender=${editedData.gender}`)
        .then((res) => {
          const data: Item[] = res.data;
          form.setFieldsValue({ listData: data });
        })
        .catch((err) => {
          console.log(err);
          message.error(
            intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'National Heights' }),
          );
        });
      form.setFieldsValue(editedData);
    }
  }, [editedData]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  return (
    <Card title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}>
      <Form
        {...layout}
        form={form}
        name="form"
        onFinish={onFinish}
        layout="vertical"
        initialValues={{ listData: [{ id: undefined }] }}
      >
        <Form.Item name="id" hidden />
        <Row gutter={24}>
          <Col span={11}>
            <Form.Item
              name="nation"
              label={lblNation}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblNation },
                  ),
                },
              ]}
            >
              <Input disabled={!access.canAdmin} />
            </Form.Item>
          </Col>
          <Col span={11}>
            <Form.Item
              name="gender"
              label={lblGender}
              rules={[
                {
                  required: true,
                  message: intl.formatMessage(
                    { id: 'message.pleaseInputField' },
                    { fieldName: lblGender },
                  ),
                },
              ]}
            >
              <Select disabled={!access.canAdmin}>
                <Option value="Male">{intl.formatMessage({ id: 'commonField.male' })}</Option>
                <Option value="Female">{intl.formatMessage({ id: 'commonField.female' })}</Option>
              </Select>
            </Form.Item>
          </Col>
        </Row>
        <Form.List
          name="listData"
          rules={[
            {
              validator: async (_, names) => {
                if (!names || names.length < 1) {
                  return Promise.reject(
                    new Error(
                      intl.formatMessage({ id: 'message.atLeast1' }, { fieldName: 'Height Data' }),
                    ),
                  );
                }
              },
            },
          ]}
        >
          {(fields, { add, remove }, { errors }) => (
            <>
              {fields.map(({ key, name, ...restField }, index) => (
                <Row key={key} gutter={24} align={index === 0 ? 'middle' : undefined}>
                  <Form.Item name={[name, 'id']} hidden />
                  <Col span={2}>
                    <Form.Item
                      {...restField}
                      name={[name, 'number']}
                      label={index === 0 ? intl.formatMessage({ id: 'commonField.no' }) : ''}
                    >
                      {index + 1}
                    </Form.Item>
                  </Col>
                  <Col span={9}>
                    <Form.Item
                      {...restField}
                      name={[name, 'age']}
                      label={index === 0 ? lblAge : ''}
                      rules={[
                        {
                          required: true,
                          message: intl.formatMessage(
                            { id: 'message.pleaseInputField' },
                            { fieldName: lblAge },
                          ),
                        },
                      ]}
                    >
                      <Input type="number" disabled={!access.canAdmin} />
                    </Form.Item>
                  </Col>
                  <Col span={9}>
                    <Form.Item
                      {...restField}
                      name={[name, 'height']}
                      label={index === 0 ? lblHeight : ''}
                      rules={[
                        {
                          required: true,
                          message: intl.formatMessage(
                            { id: 'message.pleaseInputField' },
                            { fieldName: lblHeight },
                          ),
                        },
                      ]}
                    >
                      <Input type="number" disabled={!access.canAdmin} />
                    </Form.Item>
                  </Col>
                  <Access accessible={access.canAdmin}>
                    <Col span={3}>
                      {fields.length > 1 ? (
                        <Form.Item
                          {...restField}
                          name={[name, 'action']}
                          label={index === 0 ? lblAction : ''}
                        >
                          <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
                            <Button
                              type="primary"
                              danger
                              onClick={() => remove(name)}
                              shape="circle"
                              size="small"
                              icon={<MinusOutlined />}
                              style={{
                                marginLeft: '8px',
                              }}
                            />
                          </Tooltip>
                        </Form.Item>
                      ) : null}
                    </Col>
                  </Access>
                </Row>
              ))}
              <Form.ErrorList errors={errors} />
              <Form.Item>
                <Access accessible={access.canAdmin}>
                  <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
                    <Button
                      style={{ backgroundColor: '#52c41a', borderColor: '#52c41a' }}
                      type="primary"
                      onClick={() => add()}
                      shape="circle"
                      icon={<PlusOutlined />}
                    />
                  </Tooltip>
                </Access>
              </Form.Item>
            </>
          )}
        </Form.List>
        <Form.Item>
          <Access accessible={access.canAdmin}>
            <Space>
              <Button type="primary" htmlType="submit">
                {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
              </Button>
              <Button
                type="primary"
                htmlType="button"
                onClick={onCancel}
                style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
              >
                {intl.formatMessage({ id: 'crud.cancel' })}
              </Button>
            </Space>
          </Access>
        </Form.Item>
      </Form>
      {/* <HeightsTable data={heightsData} setData={setHeightsData} /> */}
    </Card>
  );
};
