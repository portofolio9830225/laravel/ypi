import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message, Input } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, Item } from '../index';
import { useIntl } from 'umi';

export default ({
  data,
  setData,
}: {
  data: Item[];
  setData: React.Dispatch<React.SetStateAction<Item[]>>;
}) => {
  const intl = useIntl();
  const { setEditedData, getTableData, moduleName } = useContext(Context);

  const onEdit = (record: Item) => {
    setEditedData(record);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      key: 'no',
    },
    {
      title: intl.formatMessage({ id: 'pages.national-height.age' }),
      dataIndex: 'age',
      key: 'age',
      render: (value, record, index) => <Input type="number" value={value} />,
    },
    {
      title: intl.formatMessage({ id: 'pages.national-height.height' }),
      dataIndex: 'height',
      key: 'height',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      width: '20%',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="primary" danger shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return <Table columns={columns} dataSource={data} />;
};
