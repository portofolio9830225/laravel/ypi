import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message, Input } from 'antd';
import { EditOutlined, DeleteOutlined, SearchOutlined } from '@ant-design/icons';
import { useContext, useEffect, useRef, useState } from 'react';
import type { FilterConfirmProps } from 'antd/lib/table/interface';
import type { ColumnType } from 'antd/lib/table';
import type { InputRef } from 'antd';
import { destroy } from '@/services/ypi/api';
import { tableName, Context, Item } from '../index';
import { useIntl } from 'umi';
import Highlighter from 'react-highlight-words';
import styles from './table.less';

type DataIndex = keyof Item;

export default ({ data, tableHeight }: { data: Item[]; tableHeight: string }) => {
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);
  const [searchText, setSearchText] = useState<string>('');
  const [searchedColumn, setSearchedColumn] = useState<string>('');
  const searchInput = useRef<InputRef>(null);

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then(() => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: DataIndex,
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex: DataIndex): ColumnType<Item> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={searchInput}
          placeholder="Search"
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <Button
        type={filtered ? 'primary' : 'default'}
        size="small"
        shape="circle"
        icon={<SearchOutlined />}
      />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'pages.sc-joint-action.gym_description' }),
      dataIndex: 'gym_description',
      key: 'gym_description',
      width: 200,
      ...getColumnSearchProps('gym_description'),
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      width: '20%',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="primary"
              shape="circle"
              size="small"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="primary" shape="circle" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      scroll={{ y: tableHeight }}
      rowClassName={(record: Item) =>
        `${styles.densed_row} ${record.id === editItemId ? styles.selected_row : ''}`
      }
    />
  );
};
