import { Form, Input, Card, Button, message, Space } from 'antd';
import { tableName, Context, Item } from '../index';
import { create, update } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl } from 'umi';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);

  const lblGroup = intl.formatMessage({
    id: 'pages.group-activity.group',
  });

  const onFinish = (values: Item) => {
    if (editedData) {
      update(tableName, values.id, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    } else {
      create(tableName, values)
        .then((res) => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch((err) => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    }
    onCancel();
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
    }
  }, [editedData, form]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  return (
    <Card title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}>
      <Form form={form} name="form" onFinish={onFinish} layout="vertical">
        <Form.Item name="id" hidden />
        <Form.Item
          name="group"
          label={lblGroup}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: lblGroup },
              ),
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
