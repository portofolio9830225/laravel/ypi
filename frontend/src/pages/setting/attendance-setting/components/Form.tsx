import { Form, Select, Row, Col, FormInstance } from 'antd';
import { Moment } from 'moment';
import { useAccess, useIntl } from 'umi';
import { Item } from '../index';
import { get } from '@/services/ypi/api';
import { useEffect, useState } from 'react';
import { Item as Coach } from '../../../file/coach-data';
import { Item as Group } from '../../../file/group-classification';

export default ({
  selectedDate,
  onFormChange,
  formInstance,
}: {
  selectedDate: Moment;
  onFormChange: (month: number, year: number, group_id: number, coach_id: number) => void;
  formInstance: FormInstance;
}) => {
  const start = 0;
  const end = 12;
  const monthOptions = [];
  const current = selectedDate.clone();
  const localeData = selectedDate.localeData();
  const months = [];
  for (let i = 0; i < 12; i++) {
    current.month(i);
    months.push(localeData.months(current));
  }
  for (let index = start; index < end; index++) {
    monthOptions.push(
      <Select.Option value={index} key={`${index}`}>
        {months[index]}
      </Select.Option>,
    );
  }
  const month = selectedDate.month();
  const year = selectedDate.year();
  const yearOptions = [];
  for (let i = year - 10; i < year + 10; i += 1) {
    yearOptions.push(
      <Select.Option key={i} value={i}>
        {i}
      </Select.Option>,
    );
  }
  const intl = useIntl();
  const [coachOptions, setCoachOptions] = useState<React.ReactElement[]>([]);
  const [groupOptions, setGroupOptions] = useState<React.ReactElement[]>([]);
  const access = useAccess();

  useEffect(() => {
    get('master_pelatihs').then((res) => {
      const data: Coach[] = res.data;
      const options: React.ReactElement[] = [];
      data.map((coach) => {
        options.push(
          <Select.Option value={coach.id} key={`${coach.id}`}>
            {coach.nama_lengkap}
          </Select.Option>,
        );
      });
      setCoachOptions(options);
    });
    get('tabel_kelompoks').then((res) => {
      const data: Group[] = res.data;
      const options: React.ReactElement[] = [];
      data.map((group) => {
        options.push(
          <Select.Option value={group.id} key={`${group.id}`}>
            {group.kelompok}
          </Select.Option>,
        );
      });
      setGroupOptions(options);
    });
  }, []);

  const onValuesChange = (changedValues: any, values: Item) => {
    if (values.bulan && values.tahun && values.kode_pelatih && values.kelompok) {
      onFormChange(values.bulan, values.tahun, values.kelompok, values.kode_pelatih);
    }
  };

  return (
    <Form
      scrollToFirstError
      form={formInstance}
      layout="vertical"
      initialValues={{ bulan: month, tahun: year }}
      onValuesChange={onValuesChange}
      id="attendance-setting-form"
    >
      <Row gutter={6}>
        <Col span={6}>
          <Form.Item
            name="bulan"
            label={intl.formatMessage({ id: 'pages.attendance-setting.bulan' })}
            rules={[{ required: true }]}
          >
            <Select>{monthOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="tahun"
            label={intl.formatMessage({ id: 'pages.attendance-setting.tahun' })}
            rules={[{ required: true }]}
          >
            <Select>{yearOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="kode_pelatih"
            label={intl.formatMessage({ id: 'pages.attendance-setting.kode_pelatih' })}
            rules={[{ required: true }]}
            initialValue={access.coachId || undefined}
          >
            <Select disabled={access.coachId ? true : false}>{coachOptions}</Select>
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="kelompok"
            label={intl.formatMessage({ id: 'pages.attendance-setting.kelompok' })}
            rules={[{ required: true }]}
          >
            <Select>{groupOptions}</Select>
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
