import {
  Form,
  Button,
  Input,
  FormInstance,
  Row,
  Col,
  Tooltip,
  Typography,
  Switch,
  Card,
} from 'antd';
import { useIntl } from 'umi';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
import { AttendanceStatus } from '../index';

export default ({ formInstance }: { formInstance: FormInstance }) => {
  const intl = useIntl();
  const lblPresence = intl.formatMessage({
    id: 'pages.attendance-setting.presence',
  });
  const lblAttendanceStatus = intl.formatMessage({
    id: 'pages.attendance-setting.attendace_status',
  });
  const lblInitial = intl.formatMessage({
    id: 'pages.attendance-setting.initial',
  });

  const onValuesChange = (
    changedValues: { listData: AttendanceStatus[] },
    allValues: { listData: AttendanceStatus[] },
  ) => {
    let changedItem: Partial<AttendanceStatus> = { presence: false };
    let changedItemIndex: number = -1;
    changedValues.listData.map((d, index) => {
      if (d) {
        changedItem = d;
        changedItemIndex = index;
      }
    });
    if ((changedItem as AttendanceStatus).presence) {
      const listData = allValues.listData;
      listData.map((d, index) => {
        if (index != changedItemIndex) listData[index].presence = false;
      });
      formInstance.setFieldsValue({ listData });
    }
  };

  const handleRemoveStatus = (id: number) => {
    // console.log(formInstance.getFieldsValue());
    // return;
    const deletedIds: number[] = formInstance.getFieldValue('deleted_ids');

    deletedIds.push(id);
    formInstance.setFieldsValue({ deleted_ids: deletedIds });
  };

  return (
    <Card style={{ marginTop: '1rem' }}>
      <Typography.Title level={4}>
        {intl.formatMessage({
          id: 'pages.attendance-setting.attendace_status',
        })}
      </Typography.Title>
      <Form
        form={formInstance}
        name="settingPresenceList"
        layout="vertical"
        onValuesChange={onValuesChange}
        initialValues={{ listData: [{ id: undefined, presence: true }] }}
      >
        <Form.Item name="deleted_ids" initialValue={[]} hidden />

        <Form.List
          name="listData"
          rules={[
            {
              validator: async (_, names: AttendanceStatus[]) => {
                let presenceExist = false;
                names.map((d) => {
                  if (d.presence) presenceExist = true;
                });
                if (!presenceExist) {
                  return Promise.reject(
                    new Error(intl.formatMessage({ id: 'pages.attendance-setting.must_presence' })),
                  );
                }
              },
            },
          ]}
        >
          {(fields, { add, remove }, { errors }) => (
            <>
              {fields.map(({ key, name, ...restField }, index) => {
                // if (formInstance.getFieldsValue().listData[index].deleted) return;
                return (
                  <Row key={key} gutter={24} align={index === 0 ? 'middle' : undefined}>
                    <Form.Item name={[name, 'id']} hidden />
                    <Col span={2}>
                      <Form.Item
                        label={index === 0 ? intl.formatMessage({ id: 'commonField.no' }) : ''}
                      >
                        {index + 1}
                      </Form.Item>
                    </Col>
                    <Col span={10}>
                      <Form.Item
                        {...restField}
                        name={[name, 'keterangan']}
                        label={index === 0 ? lblAttendanceStatus : ''}
                        rules={[
                          {
                            required: true,
                            message: intl.formatMessage(
                              { id: 'message.pleaseInputField' },
                              { fieldName: lblAttendanceStatus },
                            ),
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      <Form.Item
                        {...restField}
                        name={[name, 'inisial']}
                        label={index === 0 ? lblInitial : ''}
                        rules={[
                          {
                            required: true,
                            message: intl.formatMessage(
                              { id: 'message.pleaseInputField' },
                              { fieldName: lblInitial },
                            ),
                          },
                        ]}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={5}>
                      <Form.Item
                        {...restField}
                        name={[name, 'presence']}
                        label={index === 0 ? lblPresence : ''}
                        valuePropName="checked"
                      >
                        <Switch />
                      </Form.Item>
                    </Col>
                    <Col span={2}>
                      {fields.length > 1 ? (
                        <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
                          <Button
                            type="primary"
                            danger
                            onClick={() => {
                              handleRemoveStatus(
                                formInstance.getFieldValue(['listData', name, 'id']),
                              );
                              remove(name);
                            }}
                            shape="circle"
                            icon={<MinusOutlined />}
                          />
                        </Tooltip>
                      ) : null}
                    </Col>
                  </Row>
                );
              })}
              <Form.ErrorList errors={errors} />
              <Form.Item>
                <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
                  <Button
                    style={{ backgroundColor: '#52c41a', borderColor: '#52c41a' }}
                    type="primary"
                    onClick={() => add({ presence: false })}
                    shape="circle"
                    icon={<PlusOutlined />}
                  />
                </Tooltip>
              </Form.Item>
            </>
          )}
        </Form.List>
      </Form>
    </Card>
  );
};
