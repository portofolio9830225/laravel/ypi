import { Calendar, Modal, message, Switch, Button, Form, Space, Popconfirm, Drawer } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get, create, destroy } from '@/services/ypi/api';
import AttendanceSettingForm from './components/Form';
import AttendanceStatusForm from './components/AttendanceStatus';
import { Access, useIntl } from 'umi';
import moment, { Moment } from 'moment';
moment.locale('en-us');
import styles from './index.less';
export interface Item {
  key: string;
  id: number;
  bulan: number;
  tahun: number;
  kelompok: number;
  kode_pelatih: number;
  tanggal: string;
  session: number;
  am_pm: number;
  am_pm_string?: string;
  deleted?: boolean;
}

export interface AttendanceStatus {
  id: number;
  kelompok_id: number;
  pelatih_id: number;
  keterangan: string;
  inisial: string;
  presence: boolean;
  deleted: boolean;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'setting_attendances';

const headerCalendar = () => {
  return <div />;
};

export default () => {
  const intl = useIntl();
  const [attendanceSettingForm] = Form.useForm<Item>();
  const [attendanceStatusForm] = Form.useForm<{ listData: AttendanceStatus[] }>();
  const [selectedDate, setSelectedDate] = useState<Moment>(moment());
  const [cellsData, setCellsData] = useState<Item[]>([]);
  const [inputSessionData, setInputSessionData] = useState<Partial<Item>[]>([]);
  const [modalVisible, setModalVisible] = useState<boolean>(false);
  const [deleteVisible, setDeleteVisible] = useState<boolean>(false);
  const [editMode, setEditMode] = useState<boolean>(false);

  const disabledDate = (currentDate: Moment) => {
    return !currentDate.isSame(selectedDate, 'month');
  };

  const storeAttendanceSetting = () => {
    const data: Item[] = [];
    const formAttendanceData = {
      bulan: attendanceSettingForm.getFieldValue('bulan') + 1,
      tahun: attendanceSettingForm.getFieldValue('tahun'),
      kelompok: attendanceSettingForm.getFieldValue('kelompok'),
      kode_pelatih: attendanceSettingForm.getFieldValue('kode_pelatih'),
    };
    cellsData.map((d) => {
      data.push({ ...d, ...formAttendanceData, deleted: d.deleted ? true : false });
    });

    create(tableName + '/batchStore', data)
      .then((res) => {
        message.success(
          intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Attendance Setting' }),
        );
      })
      .catch((err) => {
        message.error(
          intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Attendance Setting' }),
        );
      });
  };

  const storeAttendanceStatus = () => {
    const data = attendanceStatusForm.getFieldsValue();
    const attendanceSettingData = attendanceSettingForm.getFieldsValue();

    data.listData.map((d, index) => {
      data.listData[index].kelompok_id = attendanceSettingData.kelompok;
      data.listData[index].pelatih_id = attendanceSettingData.kode_pelatih;
    });

    create('setting_presences/batchStore', data)
      .then((res) => {
        message.success(
          intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Attendance Status' }),
        );
      })
      .catch((err) => {
        message.error(
          intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Attendance Status' }),
        );
      });
  };

  const onSelect = (date: Moment) => {
    if (date.isSame(selectedDate, 'month')) {
      setSelectedDate(date);
      setInputSessionData(getCellData(date));
      setModalVisible(true);
    }
  };

  const onFormChange = (month: number, year: number, group_id: number, coach_id: number) => {
    setEditMode(true);
    setSelectedDate(moment([year, month, 1]));
    setDeleteVisible(true);
    get(`${tableName}?month=${month + 1}&year=${year}&group_id=${group_id}&coach_id=${coach_id}`)
      .then((res) => {
        setCellsData(res.data);
      })
      .catch((err) => {
        message.error(
          intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Setting Attendance' }),
        );
      });
    get(`setting_presences?kelompok_id=${group_id}&pelatih_id=${coach_id}`)
      .then((res) => {
        if (res.data.length > 0) {
          attendanceStatusForm.setFieldsValue({ listData: res.data });
        } else {
          attendanceStatusForm.setFieldsValue({ listData: [{ id: undefined, presence: true }] });
        }
      })
      .catch((err) => {
        message.error(
          intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Setting Attendance' }),
        );
      });
  };

  const getCellData = (value: Moment): Item[] => {
    let data: Item[] = [];
    if (cellsData.length > 0) {
      cellsData.map((d) => {
        const date = d.tanggal.substring(0, 10);
        if (value.format('YYYY-MM-DD') == date) {
          const am_pm_string = d.am_pm == 0 ? 'AM' : 'PM';
          data.push({ ...d, am_pm_string });
        }
      });
    }
    return data;
  };

  const dateCellRender = (value: Moment) => {
    const data = getCellData(value);

    if (!value.isSame(selectedDate, 'month')) {
      return <></>;
    }

    return (
      <table className={styles.table_cell}>
        <thead>
          <tr>
            <th className={styles.table_head}>
              {intl.formatMessage({ id: 'pages.attendance-setting.session' })}
            </th>
            <th className={styles.table_head}>AM/PM</th>
          </tr>
        </thead>
        <tbody>
          {data.map((d, index) => {
            if (d.deleted) return null;
            return (
              <tr key={`${index}`}>
                <td>{d.session}</td>
                <td>{d.am_pm_string}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  const handleOk = () => {
    inputSessionData.map((sessionData) => {
      setCellsData((prevState) => {
        const index = cellsData.findIndex(
          (value) =>
            value.tanggal == sessionData.tanggal &&
            value.session == sessionData.session &&
            !value.deleted,
        );

        const newState = [...prevState];
        if (index >= 0) {
          newState[index] = sessionData as Item;
        } else {
          newState.push(sessionData as Item);
        }
        return newState;
      });
    });
    setModalVisible(false);
  };

  const handleCancel = () => {
    setModalVisible(false);
  };

  const InputSessionBody = () => {
    return (
      <table className={styles.table_cell}>
        <thead>
          <tr>
            <th className={styles.table_head}>
              {intl.formatMessage({ id: 'pages.attendance-setting.session' })}
            </th>
            <th className={styles.table_head}>AM/PM</th>
          </tr>
        </thead>
        <tbody>
          {inputSessionData.map((d, index) => {
            if (d.deleted) return null;
            return (
              <tr key={`${index}`}>
                <td className={styles.table_body}>{d.session}</td>
                <td className={styles.table_body}>
                  <Switch
                    checkedChildren="AM"
                    unCheckedChildren="PM"
                    checked={d.am_pm == 0 ? true : false}
                    onClick={() => {
                      setInputSessionData((prevState) => {
                        const newState = [...prevState];
                        const index = newState.findIndex((value) => value.session == d.session);
                        if (newState[index].am_pm == 0) {
                          newState[index].am_pm = 1;
                        } else {
                          newState[index].am_pm = 0;
                        }

                        return newState;
                      });
                    }}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  };

  const onAddSession = () => {
    setInputSessionData((prevState) => {
      const newState = [...prevState];
      const activeSession = newState.filter((value) => !value.deleted);
      const newSession = {
        session: activeSession.length + 1,
        am_pm: 0,
        tanggal: selectedDate.format('YYYY-MM-DD'),
        deleted: false,
      };
      newState.push(newSession);
      return newState;
    });
  };

  const onRemoveSession = () => {
    setInputSessionData((prevState) => {
      const newState = [...prevState];
      let lastDeletedIndex = 1;
      do {
        if (newState[newState.length - lastDeletedIndex].deleted) {
          lastDeletedIndex++;
        } else {
          newState[newState.length - lastDeletedIndex].deleted = true;
          lastDeletedIndex = 0;
        }
      } while (lastDeletedIndex != 0);

      return newState;
    });
  };

  const onSave = async () => {
    try {
      await attendanceSettingForm.validateFields();
      storeAttendanceSetting();
      await attendanceStatusForm.validateFields();
      storeAttendanceStatus();
      window.location.reload();
    } catch (error: any) {
      console.log(error);

      message.error(
        intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Attendance Setting' }),
      );
    }
  };

  const onDelete = () => {
    const formData = attendanceSettingForm.getFieldsValue();
    destroy(
      `${tableName}/1?month=${formData.bulan + 1}&year=${formData.tahun}&group_id=${
        formData.kelompok
      }&coach_id=${formData.kode_pelatih}`,
    )
      .then((res) => {
        setCellsData(res.data);
      })
      .catch((err) => {
        message.error(
          intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Setting Attendance' }),
        );
      });
    destroy(
      `setting_presences/1?kelompok_id=${formData.kelompok}&pelatih_id=${formData.kode_pelatih}`,
    )
      .then((res) => {
        attendanceStatusForm.setFieldsValue({ listData: res.data });
      })
      .catch((err) => {
        message.error(
          intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Setting Attendance' }),
        );
      });
    window.location.reload();
  };

  const modalFooter = [
    <Button type="primary" className={styles.button_add} key="add" onClick={onAddSession}>
      {intl.formatMessage({ id: 'pages.attendance-setting.add_session' })}
    </Button>,
    <Button type="primary" danger key="remove" onClick={onRemoveSession}>
      {intl.formatMessage({ id: 'pages.attendance-setting.remove_session' })}
    </Button>,
    <Button key="back" onClick={handleCancel}>
      {intl.formatMessage({ id: 'crud.cancel' })}
    </Button>,
    <Button key="submit" type="primary" onClick={handleOk}>
      {intl.formatMessage({ id: 'crud.save' })}
    </Button>,
  ];

  return (
    <>
      <AttendanceSettingForm
        formInstance={attendanceSettingForm}
        selectedDate={selectedDate}
        onFormChange={onFormChange}
      />
      <Access accessible={editMode}>
        <Calendar
          value={selectedDate}
          headerRender={headerCalendar}
          disabledDate={disabledDate}
          mode="month"
          onSelect={onSelect}
          dateCellRender={dateCellRender}
        />
        <Modal
          title={intl.formatMessage({ id: 'pages.attendance-setting.input_session' })}
          visible={modalVisible}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={modalFooter}
          bodyStyle={{ overflowY: 'scroll', maxHeight: '50vh' }}
        >
          <InputSessionBody />
        </Modal>
        <AttendanceStatusForm formInstance={attendanceStatusForm} />
        <div style={{ height: '5rem' }} />
        <Drawer visible={true} placement="bottom" closable={false} mask={false} height="5rem">
          <Space>
            <Button type="primary" htmlType="button" onClick={onSave}>
              {intl.formatMessage({ id: 'crud.save' })}
            </Button>
            <Popconfirm
              title={intl.formatMessage(
                { id: 'message.confirmDeleteMessage' },
                { moduleName: 'Attendace Setting' },
              )}
              onConfirm={() => onDelete()}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              {deleteVisible && (
                <Button type="primary" danger htmlType="button">
                  {intl.formatMessage({ id: 'crud.delete' })}
                </Button>
              )}
            </Popconfirm>
          </Space>
        </Drawer>
      </Access>
    </>
  );
};
