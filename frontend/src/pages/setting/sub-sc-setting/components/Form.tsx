import { Form, Input, Card, Button, message, Space, Select, Divider } from 'antd';
import { tableName, Context, Item } from '../index';
import { create, update, get } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, useHistory, FormattedMessage } from 'umi';
import { Item as JointActionType } from '../../sc-joint-action';
import { Item as MuscleGroupType } from '../../sc-muscle-group';
import { EditOutlined } from '@ant-design/icons';

const { Option } = Select;

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const history = useHistory();
  const { editedData, setEditedData, getTableData, moduleName } = useContext(Context);
  const [jointActionData, setJointActionData] = useState<JointActionType[]>([]);
  const [muscleGroupData, setMuscleGroupData] = useState<MuscleGroupType[]>([]);

  const lblSubExercise = intl.formatMessage({
    id: 'pages.sub-sc-setting.excercise_desc',
  });
  const lblJointAction = intl.formatMessage({
    id: 'pages.sub-sc-setting.gym_type_id',
  });
  const lblReps = intl.formatMessage({
    id: 'pages.sub-sc-setting.tut_per_rep',
  });
  const lblMuscleGroup = intl.formatMessage({
    id: 'pages.sub-sc-setting.exercise_category_id',
  });

  const onFinish = (values: Item) => {
    if (editedData) {
      update(tableName, values.id, values)
        .then(() => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch(() => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    } else {
      create(tableName, values)
        .then(() => {
          message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
          getTableData();
          onReset();
        })
        .catch(() => {
          message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
        });
    }
    onCancel();
  };

  useEffect(() => {
    get('gym_types')
      .then((res) => {
        setJointActionData(res.data);
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errGet' }, { moduleName: 'Joint Action' }));
      });
    get('exercise_categories')
      .then((res) => {
        setMuscleGroupData(res.data);
      })
      .catch(() => {
        message.error(intl.formatMessage({ id: 'message.errGet' }, { moduleName: 'Muscle Group' }));
      });
  }, []);

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
    }
  }, [form, editedData]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
  };

  const handleOpenJoinAction = () => {
    history.push('/setting/sc-joint-action');
  };

  const handleOpenMuscleGroup = () => {
    history.push('/setting/sc-muscle-group');
  };

  return (
    <Card title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}>
      <Form form={form} name="form" onFinish={onFinish} layout="vertical">
        <Form.Item name="id" hidden />
        <Form.Item
          name="excercise_desc"
          label={lblSubExercise}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: lblSubExercise },
              ),
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          name="gym_type_id"
          label={lblJointAction}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: lblJointAction },
              ),
            },
          ]}
        >
          <Select
            dropdownRender={(menu) => (
              <>
                {menu}
                <Divider />
                <Space style={{ padding: '0 8px 4px' }}>
                  <Button type="primary" onClick={handleOpenJoinAction} icon={<EditOutlined />}>
                    <FormattedMessage id="crud.editData" />
                  </Button>
                </Space>
              </>
            )}
          >
            {jointActionData.map((joint) => (
              <Option key={joint.id} value={joint.id}>
                {joint.gym_description}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          name="tut_per_rep"
          label={lblReps}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: lblReps },
              ),
            },
          ]}
        >
          <Input type="number" />
        </Form.Item>
        <Form.Item
          name="exercise_category_id"
          label={lblMuscleGroup}
          rules={[
            {
              required: true,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: lblMuscleGroup },
              ),
            },
          ]}
        >
          <Select
            dropdownRender={(menu) => (
              <>
                {menu}
                <Divider />
                <Space style={{ padding: '0 8px 4px' }}>
                  <Button type="primary" onClick={handleOpenMuscleGroup} icon={<EditOutlined />}>
                    <FormattedMessage id="crud.editData" />
                  </Button>
                </Space>
              </>
            )}
          >
            {muscleGroupData.map((muscle) => (
              <Option key={muscle.id} value={muscle.id}>
                {muscle.category}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Space>
            <Button type="primary" htmlType="submit">
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </Card>
  );
};
