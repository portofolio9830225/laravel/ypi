import { Col, message, Row } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { useIntl, history } from 'umi';

export interface Item {
  key: string;
  id: number;
  excercise_desc: string;
  gym_type_id: number;
  tut_per_rep: number;
  exercise_category_id: number;
}

export interface ContextType {
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
});

export const tableName = 'sub_exercises';

export default () => {
  const pageHeaderHeight = 250;
  const [data, setData] = useState<Item[]>([]);
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(intl.formatMessage({ id: 'menu.setting.sub-sc-setting' }));

  useEffect(() => {
    getTableData();
  }, []);

  useEffect(() => {
    console.log(editedData);
  }, [editedData]);

  const getTableData = () => {
    get(tableName)
      .then((res) => {
        const fetchedData: Item[] = res.data;
        setData(
          fetchedData.map((d: Item) => {
            return { ...d, key: d.id.toString() };
          }),
        );
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
        console.log(err);
      });
  };

  const contextValue: ContextType = { editedData, setEditedData, getTableData, moduleName };

  return (
    <Context.Provider value={contextValue}>
      <Row gutter={[16, 16]}>
        <Col span={12}>
          <Table data={data} tableHeight={`calc(100vh - ${pageHeaderHeight}px)`} />
        </Col>
        <Col span={12}>
          <Form />
        </Col>
      </Row>
    </Context.Provider>
  );
};
