import { Layout, Menu, MenuProps, Row, Col, Form, Input, Button, message } from 'antd';
import styles from './index.less';
import bg from './images/bg.png';
import byproLogo from './images/logo_bypro.png';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { history, FormattedMessage, useModel, useIntl } from 'umi';
import { get, login } from '@/services/ypi/api';
import { Club, User } from '@/types/ypi';

const { Header, Content } = Layout;

export default () => {
  const { setInitialState } = useModel('@@initialState');
  const intl = useIntl();

  const items: MenuProps['items'] = [
    { key: '1', label: 'Blog' },
    { key: '2', label: 'Service' },
    { key: '3', label: 'About Us' },
    { key: '4', label: 'Contact' },
  ];

  const onFinish = async (values: any) => {
    try {
      message.loading({
        content: intl.formatMessage({ id: 'message.pleaseWait' }),
        duration: 0,
        key: 'loading-message',
      });
      const loginRes = await login({ ...values });

      if (loginRes.message === 'Login Success') {
        const currentUser: User = loginRes.user;
        const currentClub: Club = loginRes.club;

        await setInitialState((s) => ({
          ...s,
          currentUser,
          currentClub,
        }));

        localStorage.setItem('token', loginRes.access_token);
        localStorage.setItem('user', JSON.stringify(loginRes.user));
        if (currentClub) localStorage.setItem('club', JSON.stringify(currentClub));

        const defaultLoginSuccessMessage = intl.formatMessage({
          id: 'pages.login.success',
        });
        message.success(defaultLoginSuccessMessage);
        if (!history) return;
        const { query } = history.location;
        const { redirect } = query as { redirect: string };
        history.push(redirect || '/');
        window.location.reload();
        return;
      }
    } catch (error) {
      const defaultLoginFailureMessage = intl.formatMessage({
        id: 'pages.login.failure',
      });
      message.error(defaultLoginFailureMessage);
    }

    message.destroy('loading-message');
  };

  return (
    <Layout className={styles.layout} style={{ backgroundImage: `url(${bg})` }}>
      <Header className={styles.header}>
        <Menu className={styles.menu} mode="horizontal" items={items} />
        <img height={64} src={byproLogo} alt="logo_bypro" />
      </Header>
      <Content className={styles.content}>
        <div id="container">
          <Row>
            <Col span={15}>
              <div className={styles.title}>
                <h3>
                  <FormattedMessage id="pages.login.welcome_to" />
                </h3>
                <h1>
                  <FormattedMessage id="pages.login.ypi_online" />
                </h1>
              </div>
            </Col>
            <Col span={9}>
              <div className={styles.login}>
                <div>
                  <h3>
                    <FormattedMessage id="pages.login.user_login" />
                  </h3>
                  <Form name="login-form" onFinish={onFinish}>
                    <Form.Item name="email" rules={[{ required: true }]}>
                      <Input
                        prefix={<UserOutlined />}
                        type="email"
                        placeholder="Email or Username"
                      />
                    </Form.Item>
                    <Form.Item name="password">
                      <Input.Password
                        prefix={<LockOutlined />}
                        type="password"
                        placeholder="Password"
                      />
                    </Form.Item>
                    <div className={styles.inline_item}>
                      <Form.Item name="password">
                        <Button type="text">
                          <FormattedMessage id="pages.login.forgotPassword" />
                        </Button>
                      </Form.Item>
                    </div>
                    <Form.Item>
                      <Button type="primary" shape="round" htmlType="submit">
                        <FormattedMessage id="pages.login.login" />
                      </Button>
                    </Form.Item>
                    <Form.Item>
                      <Button type="primary" shape="round">
                        <FormattedMessage id="pages.login.sign_up" />
                      </Button>
                    </Form.Item>
                  </Form>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </Content>
    </Layout>
  );
};
