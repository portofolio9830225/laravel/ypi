import React, { useState, createContext, useEffect } from 'react';
import Table from './components/Table';
import { Club, Coach, User } from '@/types/ypi';
import { useIntl } from 'umi';
import { message, Form, FormInstance } from 'antd';
import { get } from '@/services/ypi/api';
import FormUser from './components/Form';

interface DevType {
  edited: boolean;
  key: string;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.competition-level.' + fieldName;
};

export interface Record extends User, DevType {
  club: {
    id: number;
    nama: string;
  };
}

export interface ContextType {
  data: Partial<Record>[];
  setData: React.Dispatch<React.SetStateAction<Partial<Record>[]>>;
  formOpen: boolean;
  setFormOpen: React.Dispatch<React.SetStateAction<boolean>>;
  getTableData: () => Promise<void>;
  formUser: FormInstance<User> | undefined;
  clubs: Club[];
  coaches: Coach[];
}

export const Context = createContext<ContextType>({
  data: [],
  setData: () => {},
  formOpen: false,
  setFormOpen: () => {},
  getTableData: async () => {},
  formUser: undefined,
  clubs: [],
  coaches: [],
});

export default () => {
  const intl = useIntl();
  const [data, setData] = useState<Partial<Record>[]>([]);
  const [formOpen, setFormOpen] = useState<boolean>(false);
  const [formUser] = Form.useForm<User>();
  const [clubs, setClubs] = useState<Club[]>([]);
  const [coaches, setCoaches] = useState<Coach[]>([]);

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get('users');
      const d: Record[] = dataRes.data;
      setData(
        d.map((d, i) => {
          return { ...d, key: i.toString() };
        }),
      );
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  const getDependencyData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const clubRes = await get('club');
      setClubs(clubRes.data);
      const coachesRes = await get('master_pelatihs');
      setCoaches(coachesRes.data);
      const dataRes = await get('users');
      const d: Record[] = dataRes.data;
      setData(
        d.map((d, i) => {
          return { ...d, key: i.toString() };
        }),
      );
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    getDependencyData();
  }, []);

  const contextValue: ContextType = {
    data,
    setData,
    formOpen,
    setFormOpen,
    getTableData,
    formUser,
    clubs,
    coaches,
  };

  return (
    <Context.Provider value={contextValue}>
      <Table />
      <FormUser />
    </Context.Provider>
  );
};
