import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
  Avatar,
} from 'antd';
import { FC, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage, useModel, useHistory } from 'umi';
import { getLocaleKey, Context, Record } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined } from '@ant-design/icons';
import { destroy } from '@/services/ypi/api';
import styles from './table.less';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
  Avatar,
  Password,
  Email,
}

const EditableContext = createContext<FormInstance<Record> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof Record | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: Record;
  handleToggleEdit: (form: FormInstance<Record> | null, record: Record) => void;
  handleSave: (form: FormInstance<Record> | null, record: Record) => Promise<void>;
  handleDelete: (record: Record) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.role !== 'superadmin'}>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage(
                { id: 'message.confirmDeleteMessage' },
                { moduleName: 'Data' },
              )}
              onConfirm={() => handleDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Access>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (dataIndex === 'role') {
    if (record.role === 'superadmin') {
      return (
        <td {...props}>
          <FormattedMessage id="commonField.superadmin" />
        </td>
      );
    } else if (record.role === 'club_admin') {
      return (
        <td {...props}>
          <FormattedMessage id="commonField.club_admin" />
        </td>
      );
    } else {
      return (
        <td {...props}>
          <FormattedMessage id="commonField.coach" />
        </td>
      );
    }
  }

  if (inputType === InputType.Avatar) {
    return (
      <td {...props}>
        <>
          <Form.Item name="id" hidden />
          <Avatar size={64} shape="square" src={record.foto} />
        </>
      </td>
    );
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;

    rules = getRequiredRule('level');

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const history = useHistory();
  const { data, setFormOpen, formUser, getTableData } = useContext(Context);
  const { initialState } = useModel('@@initialState');

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.image' }),
      dataIndex: 'foto',
      inputType: InputType.Avatar,
    },
    {
      title: intl.formatMessage({ id: 'commonField.name' }),
      dataIndex: 'name',
    },
    {
      title: intl.formatMessage({ id: 'commonField.email' }),
      dataIndex: 'email',
    },
    {
      title: intl.formatMessage({ id: 'pages.edit-profile.active_club' }),
      dataIndex: ['club', 'nama'],
    },
    {
      title: intl.formatMessage({ id: 'commonField.role' }),
      dataIndex: 'role',
    },
    {
      title: intl.formatMessage({ id: 'pages.users-management.as_coach' }),
      dataIndex: ['as_coach', 'nama_lengkap'],
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      width: '20%',
    },
  ];

  const handleToggleEdit = (form: FormInstance<Record> | null, record: Record) => {
    if (initialState?.currentUser?.id === record.id) {
      history.push('/system/edit-profile');
    } else {
      setFormOpen(true);
      formUser?.setFieldsValue(record);
    }
  };

  const handleDelete = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      if (record.id) {
        await destroy('users', record.id);
      }
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
    getTableData();
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Record) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleDelete,
      }),
    };
    return newCol;
  });

  const handleOpenForm = () => {
    setFormOpen(true);
  };

  const footer = () => {
    return (
      <Space>
        <Button onClick={handleOpenForm} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      className={styles.file_child_content}
      components={components}
      bordered
      dataSource={data}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={{ size: 'small' }}
    />
  );
};

export default EditableTable;
