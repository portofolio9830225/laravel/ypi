import { FC, useContext, useEffect, useState } from 'react';
import { Avatar, Row, Col, Space, Form, Input, Button, message, Modal, Select } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styles from './form.less';
import { FormattedMessage, useIntl } from 'umi';
import { Context } from '../index';
import { create } from '@/services/ypi/api';
import { User } from '@/types/ypi';

const EditProfile: FC = () => {
  const intl = useIntl();
  const [asCoach, setAsCoach] = useState<boolean>(false);
  const { formOpen, setFormOpen, formUser, clubs, coaches } = useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleSave = async () => {
    // console.log(formUser?.getFieldsValue());
    // return;
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formData = new FormData();
      const formValues = await formUser?.validateFields();
      if (formValues && !formValues.password) {
        delete formValues.password;
      }
      for (const key in formValues) {
        formData.append(key, formValues[key]);
      }
      formData.set('foto', document.getElementById('file').files[0]);
      await create('users', formData);
      window.location.reload();
      handleCancel();
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Profile' }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Profile' }));
    }
    hideLoading();
  };

  const handleCancel = () => {
    setFormOpen(false);
    formUser?.resetFields();
  };

  const Footer = () => {
    return (
      <Space>
        <Button onClick={handleCancel}>
          <FormattedMessage id="crud.cancel" />
        </Button>
        <Button onClick={handleSave} type="primary">
          <FormattedMessage id="crud.save" />
        </Button>
      </Space>
    );
  };

  const handleValuesChange = (changedValues: Partial<User>, values: User) => {
    if (changedValues.role || changedValues.active_club_id) {
      if (values.role === 'coach' && values.active_club_id) {
        setAsCoach(true);
        if (values.as_coach_id === 0) {
          formUser?.setFieldsValue({ as_coach_id: null });
        }
      } else {
        setAsCoach(false);
        formUser?.setFieldsValue({ as_coach_id: null });
      }
    }
  };

  useEffect(() => {
    if (formOpen) {
      const formValues = formUser?.getFieldsValue();
      if (formValues?.role === 'coach' && formValues.active_club_id) {
        setAsCoach(true);
      } else {
        setAsCoach(false);
      }
    }
  }, [formOpen]);

  const userImage = formUser?.getFieldValue('foto');
  const userName = formUser?.getFieldValue('name');

  return (
    <Modal closable={false} footer={<Footer />} visible={formOpen} centered>
      <Row justify="center">
        <Col>
          <Row justify="center">
            <Space style={{ textAlign: 'center' }} direction="vertical">
              <Avatar size={128} src={userImage} icon={userImage ? <></> : <UserOutlined />} />
              <h3 className={styles.username_display}>{userName}</h3>
            </Space>
          </Row>
          <Form layout="vertical" form={formUser} onValuesChange={handleValuesChange}>
            <Form.Item name="id" hidden />
            <Row gutter={6}>
              <Col span={12}>
                <Form.Item label={intl.formatMessage({ id: 'commonField.photo' })}>
                  <Input id="file" type="file" />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="name"
                  label={intl.formatMessage({ id: 'commonField.username' })}
                  rules={getRequiredRule('Name')}
                >
                  <Input />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={6}>
              <Col span={12}>
                <Form.Item
                  name="email"
                  label={intl.formatMessage({ id: 'commonField.email' })}
                  rules={getRequiredRule('Email')}
                >
                  <Input />
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  name="password"
                  label={intl.formatMessage({ id: 'commonField.password' })}
                >
                  <Input.Password autoComplete="new-password" />
                </Form.Item>
              </Col>
            </Row>
            <Row gutter={6}>
              <Col span={12}>
                <Form.Item
                  rules={getRequiredRule('Club')}
                  name="active_club_id"
                  label={<FormattedMessage id="pages.edit-profile.active_club" />}
                >
                  <Select>
                    {clubs.map((d, i) => (
                      <Select.Option key={i} value={d.id}>
                        {d.nama}
                      </Select.Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={12}>
                <Form.Item
                  rules={getRequiredRule('Role')}
                  name="role"
                  label={<FormattedMessage id="commonField.role" />}
                >
                  <Select>
                    <Select.Option value="superadmin">
                      <FormattedMessage id="commonField.superadmin" />
                    </Select.Option>
                    <Select.Option value="club_admin">
                      <FormattedMessage id="commonField.club_admin" />
                    </Select.Option>
                    <Select.Option value="coach">
                      <FormattedMessage id="commonField.coach" />
                    </Select.Option>
                  </Select>
                </Form.Item>
              </Col>
            </Row>
            {asCoach ? (
              <Row gutter={6}>
                <Col span={12}>
                  <Form.Item
                    rules={getRequiredRule('Coach')}
                    name="as_coach_id"
                    label={<FormattedMessage id="pages.users-management.as_coach" />}
                  >
                    <Select>
                      {coaches.map((d, i) => (
                        <Select.Option value={d.id} key={i}>
                          {d.nama_lengkap}
                        </Select.Option>
                      ))}
                    </Select>
                  </Form.Item>
                </Col>
              </Row>
            ) : (
              <Form.Item hidden name="as_coach_id" />
            )}
          </Form>
        </Col>
      </Row>
    </Modal>
  );
};

export default EditProfile;
