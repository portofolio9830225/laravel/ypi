import { FC, useState, useEffect, useContext } from 'react';
import { Avatar, Row, Col, Space, Form, Input, Button, message, Drawer, Modal } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styles from './form.less';
import { FormattedMessage, useIntl, useModel } from 'umi';
import { Context } from '../index';
import { create, update } from '@/services/ypi/api';
import { Club } from '@/types/ypi';

const EditProfile: FC = () => {
  const intl = useIntl();
  const [form] = Form.useForm<Club>();
  const [currentClub, setCurrentClub] = useState<Partial<Club>>();
  const { setInitialState, initialState } = useModel('@@initialState');

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleSave = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formData = new FormData();
      const formValues = form.getFieldsValue();
      for (const key in formValues) {
        formData.append(key, formValues[key]);
      }
      formData.set('image', document.getElementById('file').files[0]);
      const updateRes = await create('club', formData);
      const updateData: Club | undefined = updateRes.data;
      localStorage.setItem('club', JSON.stringify(updateData));
      await setInitialState((s) => ({
        ...s,
        currentClub: updateData,
      }));
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Club' }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Club' }));
    }
    hideLoading();
    window.location.reload();
  };

  useEffect(() => {
    if (initialState && initialState.currentClub) {
      const clubData = initialState.currentClub;
      setCurrentClub(clubData);
      form.setFieldsValue(clubData);
    }
  }, [initialState]);

  return (
    <Row justify="center">
      <Col>
        <Row justify="center">
          <Avatar
            size={128}
            shape="square"
            src={currentClub?.image}
            icon={currentClub?.image ? <></> : <UserOutlined />}
          />
        </Row>
        <Form layout="vertical" form={form}>
          <Form.Item name="id" hidden />
          <Form.Item
            name="nama"
            label={intl.formatMessage({ id: 'pages.club-profile.club_name' })}
            rules={getRequiredRule('Club Name')}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="alamat"
            label={intl.formatMessage({ id: 'commonField.location' })}
            rules={getRequiredRule('Location')}
          >
            <Input />
          </Form.Item>
          <Form.Item label={intl.formatMessage({ id: 'commonField.image' })}>
            <Input id="file" type="file" />
          </Form.Item>
          <Form.Item>
            <Button onClick={handleSave} type="primary" style={{ width: '100%' }}>
              {intl.formatMessage({ id: 'crud.save' })}
            </Button>
          </Form.Item>
        </Form>
      </Col>
    </Row>
  );
};

export default EditProfile;
