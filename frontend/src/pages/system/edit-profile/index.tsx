import { FC, useState, useEffect } from 'react';
import { Avatar, Row, Col, Space, Form, Input, Button, message, Select } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styles from './index.less';
import { FormattedMessage, useAccess, useIntl, useModel } from 'umi';
import { Club, Coach, User } from '@/types/ypi';
import { create, get } from '@/services/ypi/api';

const EditProfile: FC = () => {
  const intl = useIntl();
  const [form] = Form.useForm<User>();
  const access = useAccess();
  const [currentUser, setCurrentUser] = useState<Partial<User>>();
  const [clubs, setClubs] = useState<Club[]>([]);
  const [coaches, setCoaches] = useState<Coach[]>([]);
  const { setInitialState, initialState } = useModel('@@initialState');

  const fetchDepData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const clubRes = await get('clubs');
      setClubs(clubRes.data);
      const coachRes = await get('master_pelatihs');
      setCoaches(coachRes.data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDepData();
  }, []);

  useEffect(() => {
    if (initialState && initialState.currentUser) {
      const userData = initialState.currentUser;
      setCurrentUser(userData);
      form.setFieldsValue(userData);
    }
  }, [initialState]);

  const handleSave = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formData = new FormData();
      const formValues = form.getFieldsValue();
      if (!formValues.password) {
        delete formValues.password;
      }
      for (const key in formValues) {
        formData.append(key, formValues[key]);
      }
      formData.set('foto', document.getElementById('file').files[0]);
      const updateRes = await create('users', formData);
      const updateData: User | undefined = updateRes.data;
      const selectedClub = clubs.find((d) => d.id === formValues.active_club_id);
      localStorage.setItem('user', JSON.stringify(updateData));
      localStorage.setItem('club', JSON.stringify(selectedClub));
      await setInitialState((s) => ({
        ...s,
        currentUser: updateData,
        currentClub: selectedClub,
      }));
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Profile' }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Profile' }));
    }
    hideLoading();
    window.location.reload();
  };

  return (
    <Row justify="center">
      <Col>
        <Row justify="center">
          <Space style={{ textAlign: 'center' }} direction="vertical">
            <Avatar
              size={128}
              src={currentUser?.foto}
              icon={currentUser?.foto ? <></> : <UserOutlined />}
            />
            <h3 className={styles.username_display}>{currentUser?.name}</h3>
          </Space>
        </Row>
        <Form layout="vertical" form={form}>
          <Form.Item name="id" hidden />
          <Row gutter={6}>
            <Col span={12}>
              <Form.Item label={intl.formatMessage({ id: 'commonField.photo' })}>
                <Input id="file" type="file" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="name" label={intl.formatMessage({ id: 'commonField.username' })}>
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={6}>
            <Col span={12}>
              <Form.Item name="email" label={intl.formatMessage({ id: 'commonField.email' })}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="password" label={intl.formatMessage({ id: 'commonField.password' })}>
                <Input.Password autoComplete="new-password" />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={6}>
            <Col span={12}>
              <Form.Item
                name="active_club_id"
                label={<FormattedMessage id="pages.edit-profile.active_club" />}
              >
                <Select disabled={!access.canAdmin}>
                  {clubs?.map((d, i) => (
                    <Select.Option key={i} value={d.id}>
                      {d.nama}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="as_coach_id"
                label={intl.formatMessage({ id: 'pages.users-management.as_coach' })}
              >
                <Select disabled={true}>
                  {coaches?.map((d, i) => (
                    <Select.Option key={i} value={d.id}>
                      {d.nama_lengkap}
                    </Select.Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item label=" ">
                <Button onClick={handleSave} type="primary" style={{ width: '100%' }}>
                  {intl.formatMessage({ id: 'crud.save' })}
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Col>
    </Row>
  );
};

export default EditProfile;
