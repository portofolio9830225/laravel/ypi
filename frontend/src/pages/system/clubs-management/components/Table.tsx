import {
  Space,
  Tooltip,
  Table,
  Form,
  Popconfirm,
  InputNumber,
  Input,
  Button,
  FormInstance,
  TimePicker,
  message,
  Avatar,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { getLocaleKey, Context, Record } from '../index';
import { EditOutlined, SaveOutlined, DeleteOutlined, DatabaseOutlined } from '@ant-design/icons';
import { create, destroy, get, update } from '@/services/ypi/api';
import styles from './table.less';
import { SECONDARY_BUTTON_COLOR } from '@/utils/constant';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
  Id,
  Avatar,
}

const EditableContext = createContext<FormInstance<Record> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof Record | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: Record;
  handleToggleEdit: (form: FormInstance<Record> | null, record: Record) => void;
  handleSave: (form: FormInstance<Record> | null, record: Record) => Promise<void>;
  handleDelete: (record: Record) => Promise<void>;
  isEditMode: boolean;
  handlePopulateData: (record: Record) => void;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  handlePopulateData,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const form = useContext(EditableContext);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(form, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (form) handleSave(form, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Tooltip title="Populate Data">
          <Button
            style={{ backgroundColor: SECONDARY_BUTTON_COLOR, borderColor: SECONDARY_BUTTON_COLOR }}
            size="small"
            type="primary"
            shape="circle"
            onClick={() => handlePopulateData(record)}
            icon={<DatabaseOutlined />}
          />
        </Tooltip>
        <Tooltip title="Delete">
          <Popconfirm
            title={intl.formatMessage(
              { id: 'message.confirmDeleteMessage' },
              { moduleName: 'Data' },
            )}
            onConfirm={() => handleDelete(record)}
            okText={intl.formatMessage({ id: 'crud.yes' })}
            cancelText={intl.formatMessage({ id: 'crud.no' })}
          >
            <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
          </Popconfirm>
        </Tooltip>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Avatar) {
    return (
      <td {...props}>
        <>
          <Form.Item name="id" hidden />
          <Avatar size={64} shape="square" src={record.image} />
        </>
      </td>
    );
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;

    rules = getRequiredRule('level');

    childNode = (
      <Form.Item rules={rules} style={style} name={dataIndex}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const { data, setData, setFormOpen, formClub, getTableData, setSelectedClub } =
    useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.image' }),
      dataIndex: 'image',
      inputType: InputType.Avatar,
    },
    {
      title: intl.formatMessage({ id: 'pages.club-profile.club_name' }),
      dataIndex: 'nama',
      inputType: InputType.Text,
    },
    {
      title: intl.formatMessage({ id: 'commonField.location' }),
      dataIndex: 'alamat',
      inputType: InputType.Number,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      width: '20%',
    },
  ];

  const handleToggleEdit = (form: FormInstance<Record> | null, record: Record) => {
    setFormOpen(true);
    formClub?.setFieldsValue(record);
  };

  const handlePopulateData = (record: Record) => {
    setSelectedClub(record);
  };

  const handleDelete = async (record: Record) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      if (record.id) {
        await destroy('club', record.id);
      }
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
    getTableData();
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: Record) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handlePopulateData,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const handleOpenForm = () => {
    setFormOpen(true);
  };

  const footer = () => {
    if (isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleOpenForm} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      className={styles.file_child_content}
      components={components}
      bordered
      dataSource={data}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={isEditMode ? false : { size: 'small' }}
    />
  );
};

export default EditableTable;
