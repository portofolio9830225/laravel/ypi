import { FC, useState, useEffect, useContext } from 'react';
import { Avatar, Row, Col, Space, Form, Input, Button, message, Drawer, Modal } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import styles from './form.less';
import { FormattedMessage, useIntl, useModel } from 'umi';
import { Context } from '../index';
import { create, update } from '@/services/ypi/api';
import { Club } from '@/types/ypi';

const EditProfile: FC = () => {
  const intl = useIntl();
  const { formOpen, getTableData, setFormOpen, formClub } = useContext(Context);
  const { setInitialState } = useModel('@@initialState');

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleSave = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formData = new FormData();
      const formValues = formClub?.getFieldsValue();
      for (const key in formValues) {
        formData.append(key, formValues[key]);
      }
      formData.set('image', document.getElementById('file').files[0]);
      await create('club', formData);
      setFormOpen(false);
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Club' }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Club' }));
    }
    getTableData();
    hideLoading();
  };

  const handleCancel = () => {
    setFormOpen(false);
    formClub?.resetFields();
  };

  const Footer = () => {
    return (
      <Space>
        <Button onClick={handleCancel}>
          <FormattedMessage id="crud.cancel" />
        </Button>
        <Button onClick={handleSave} type="primary">
          <FormattedMessage id="crud.save" />
        </Button>
      </Space>
    );
  };

  return (
    <Modal closable={false} footer={<Footer />} visible={formOpen} centered>
      <Row justify="center">
        <Col>
          <Row justify="center">
            <Avatar size={128} shape="square" src={formClub?.getFieldValue('image')} />
          </Row>
          <Form layout="vertical" form={formClub}>
            <Form.Item name="id" hidden />
            <Form.Item
              name="nama"
              label={intl.formatMessage({ id: 'pages.club-profile.club_name' })}
              rules={getRequiredRule('Club Name')}
            >
              <Input />
            </Form.Item>
            <Form.Item
              name="alamat"
              label={intl.formatMessage({ id: 'commonField.location' })}
              rules={getRequiredRule('Location')}
            >
              <Input />
            </Form.Item>
            <Form.Item label={intl.formatMessage({ id: 'commonField.image' })}>
              <Input id="file" type="file" />
            </Form.Item>
          </Form>
        </Col>
      </Row>
    </Modal>
  );
};

export default EditProfile;
