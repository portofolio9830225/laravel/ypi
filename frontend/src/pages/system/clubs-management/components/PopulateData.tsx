import { FC, useState, useEffect, useContext } from 'react';
import { Space, Form, Button, Modal, Select, message } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { Context } from '../index';
import { get } from '@/services/ypi/api';

interface FormType {
  tamplate_data: string;
}

const EditProfile: FC = () => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const { selectedClub, setSelectedClub, templatesName } = useContext(Context);
  const [visible, setVisible] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleProcess = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const formValues = await form.validateFields();
      const headers = {
        Authorization: `Bearer ${localStorage.getItem('token') || sessionStorage.getItem('token')}`,
        Accept: `application/json`,
        'X-Tenant': selectedClub?.id,
      };
      await get(`templates_data/${formValues.tamplate_data}`, { headers });
      setSelectedClub(undefined);
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Club' }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Club' }));
    }
    hideLoading();
  };

  const handleCancel = () => {
    setSelectedClub(undefined);
  };

  useEffect(() => {
    if (selectedClub) {
      setVisible(true);
    } else {
      setVisible(false);
    }
  }, [selectedClub]);

  const Footer = () => {
    return (
      <Space>
        <Button onClick={handleCancel}>
          <FormattedMessage id="crud.cancel" />
        </Button>
        <Button onClick={handleProcess} type="primary">
          <FormattedMessage id="crud.process" />
        </Button>
      </Space>
    );
  };

  return (
    <Modal closable={false} footer={<Footer />} visible={visible} centered>
      <Form layout="vertical" form={form}>
        <Form.Item
          name="tamplate_data"
          label="Template Data"
          rules={getRequiredRule('Template Data')}
        >
          <Select>
            {templatesName.map((d, i) => (
              <Select.Option key={i} value={d}>
                {d}
              </Select.Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default EditProfile;
