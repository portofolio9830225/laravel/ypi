import React, { useState, createContext, useEffect } from 'react';
import Table from './components/Table';
import { Club } from '@/types/ypi';
import { useIntl } from 'umi';
import { message, Form, FormInstance } from 'antd';
import { get } from '@/services/ypi/api';
import FormClub from './components/Form';
import PopulateData from './components/PopulateData';

interface DevType {
  edited: boolean;
  key: string;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.competition-level.' + fieldName;
};

interface FormType extends Club {}

export interface Record extends Club, DevType {}

export interface ContextType {
  data: Partial<Record>[];
  setData: React.Dispatch<React.SetStateAction<Partial<Record>[]>>;
  formOpen: boolean;
  setFormOpen: React.Dispatch<React.SetStateAction<boolean>>;
  getTableData: () => Promise<void>;
  formClub: FormInstance<FormType> | undefined;
  selectedClub: Club | undefined;
  setSelectedClub: React.Dispatch<React.SetStateAction<Club | undefined>>;
  templatesName: string[];
}

export const Context = createContext<ContextType>({
  data: [],
  setData: () => {},
  formOpen: false,
  setFormOpen: () => {},
  getTableData: async () => {},
  formClub: undefined,
  selectedClub: undefined,
  setSelectedClub: () => {},
  templatesName: [],
});

export default () => {
  const intl = useIntl();
  const [data, setData] = useState<Partial<Record>[]>([]);
  const [formOpen, setFormOpen] = useState<boolean>(false);
  const [formClub] = Form.useForm<FormType>();
  const [selectedClub, setSelectedClub] = useState<Club>();
  const [templatesName, setTemplatesName] = useState<string[]>([]);

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get('club');
      const d: Record[] = dataRes.data;
      setData(
        d.map((d, i) => {
          return { ...d, key: i.toString() };
        }),
      );
      const templatesRes = await get('templates_data');
      setTemplatesName(templatesRes.data);
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    getTableData();
  }, []);

  const contextValue: ContextType = {
    data,
    setData,
    formOpen,
    setFormOpen,
    getTableData,
    formClub,
    selectedClub,
    setSelectedClub,
    templatesName,
  };

  return (
    <Context.Provider value={contextValue}>
      <Table />
      <FormClub />
      <PopulateData />
    </Context.Provider>
  );
};
