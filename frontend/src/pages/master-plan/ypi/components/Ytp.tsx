import { Table, TableColumnsType, TableColumnType, message, Form } from 'antd';
import { useContext, useState, useEffect, useMemo } from 'react';
import { Context, getLocaleKey } from '../index';
import { useIntl } from 'umi';
import { DateSeasonPlanner } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import moment from 'moment';
import styles from './ytp.less';
// import { YTP_DATE_COLOR, YTP_SEASON_PLAN_COLOR } from '@/utils/constant';

interface CustomCellProps {
  record: RecordType;
  children: React.ReactNode;
  week: number;
}

const CustomCell: React.FC<CustomCellProps> = ({ record, children, week, ...props }) => {
  const { eventData } = useContext(Context);

  if (record && record.isEventData) {
    const event = eventData.find((d) => d.nama_event == record['week_' + week]);

    return (
      <td {...props} style={{ background: event?.color }}>
        {children}
      </td>
    );
  }

  return <td {...props}>{children}</td>;
};

interface RecordType {
  key: string;
  color: string;
  seasonPlanner: string;
  seasonPlannerDetail: string;
  isEventData: boolean;
  isDateData: boolean;
  seasonPlanId: number;
}

export default () => {
  const intl = useIntl();
  const [data, setData] = useState<Partial<RecordType>[]>([]);
  const [emptyValues, setEmptyValues] = useState<any>();
  const { form, seasonPlanValues, ypiEvents, eventData, constantValues } = useContext(Context);
  const dateYpi = Form.useWatch('tanggal_mulai_moment', form);

  const dateName: Partial<RecordType>[] = useMemo(() => {
    return [
      {
        key: '0',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.year' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '1',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.month' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '2',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.monday' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '3',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.sunday' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '4',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.week' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '5',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.event' }),
        // color: YTP_DATE_COLOR,
        isEventData: true,
      },
    ];
  }, [constantValues]);

  useEffect(() => {
    setEmptyValues(() => {
      const empty = {};
      for (let i = 1; i <= 53; i++) {
        empty['week_' + i] = '';
      }
      return empty;
    });
  }, []);

  const processDatesSeasonPlans = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const startYpiDate = dateYpi.format('YYYY-MM-DD');
    const endYpiDate = moment(dateYpi).add(1, 'years').format('YYYY-MM-DD');
    const seasonPlannerRes = await get(
      `annual_plans/seasonPlanner?start=${startYpiDate}&end=${endYpiDate}`,
    );
    const dateSeasonPlanner: DateSeasonPlanner[] = seasonPlannerRes.data || [];

    let newData = [...dateName];

    let yearData = { ...newData[0] };
    let monthData = { ...newData[1] };
    let mondayData = { ...newData[2] };
    let saturdayData = { ...newData[3] };
    let weekData = { ...newData[4] };
    let eventYtpData = { ...newData[5] };
    const emptyVal = {};
    for (let i = 0; i < dateSeasonPlanner.length; i++) {
      const val = dateSeasonPlanner[i];
      emptyVal['week_' + (i + 1)] = '';
      yearData['week_' + (i + 1)] = val.year;
      monthData['week_' + (i + 1)] = val.month;
      mondayData['week_' + (i + 1)] = val.monday;
      saturdayData['week_' + (i + 1)] = val.sunday;
      weekData['week_' + (i + 1)] = val.week;
      eventYtpData['week_' + (i + 1)] = '';
    }

    const startPeriod = moment(startYpiDate);

    ypiEvents?.forEach((evt) => {
      if(!evt.active) return;
      
      const event = eventData.find((dd) => dd.id === evt.master_achievement_id);
      if(!event) return;

      const startEvent = moment(event.tanggal_mulai);
      const endEvent = moment(event.tanggal_akhir);
      let startWeekEvent = startEvent.diff(startPeriod, 'weeks');
      let endWeekEvent = endEvent.diff(startPeriod, 'weeks');

      if(startWeekEvent < 0 )
        startWeekEvent = 0;

      if(endWeekEvent > dateSeasonPlanner.length )
        endWeekEvent = dateSeasonPlanner.length;

      for(let i = startWeekEvent; i <= endWeekEvent; i++) {
        eventYtpData['week_' + (i + 1)] = event.nama_event;
      }
    });

    newData[0] = yearData;
    newData[1] = monthData;
    newData[2] = mondayData;
    newData[3] = saturdayData;
    newData[4] = weekData;
    newData[5] = eventYtpData;
    setEmptyValues(emptyVal);

    setData((prevData) => {
      const processedData = newData.map((d) => {
        return { ...d, isDateData: true };
      });
      const currentSpData = [...prevData].filter((d) => d.isDateData === false);
      const combinedData = [...processedData, ...currentSpData].map((d, i) => {
        return { ...d, key: i.toString() };
      });
      return combinedData;
    });
    hideLoading();
  };

  const processTableData = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }));
    setData((prevData) => {
      const currentDateData = [...prevData].filter((d) => d.isDateData === true);
      const sortedSpv = [...seasonPlanValues].sort(
        (a, b) => (a.season_plan_id as number) - (b.season_plan_id as number),
      );
      const activeSpv = sortedSpv.filter((d) => d.active === true);
      const editedData: Partial<RecordType>[] = activeSpv.map((d) => {
        return {
          color: d.bg_color,
          seasonPlanner: d.seasonPlan,
          seasonPlannerDetail: d.seasonPlanDetail,
          isEventData: false,
          isDateData: false,
          seasonPlanId: d.season_plan_id,
          ...emptyValues,
        };
      });
      let combinedData = [...currentDateData, ...editedData];
      combinedData = combinedData.map((d, i) => {
        return {
          ...d,
          key: i.toString(),
        };
      });
      return combinedData;
    });
    hideLoading();
  };

  useEffect(() => {
    if (dateYpi) processDatesSeasonPlans();
  }, [dateYpi, ypiEvents]);

  useEffect(() => {
    if (seasonPlanValues.length > 0) {
      processTableData();
    } else {
      setData([]);
    }
  }, [seasonPlanValues]);

  let weekColumns = [];
  let value: string[] = [];
  let index: number = 0;
  for (let i = 1; i <= 53; i++) {
    weekColumns.push({
      colSpan: 0,
      rowSpan: 0,
      dataIndex: 'week_' + i,
      onCell: (val: RecordType, indexRow: number) => {
        const currentValue = val['week_' + i];
        const nextValue = val['week_' + (i + 1)];
        let props = {};

        if (currentValue !== '') {
          if (index !== indexRow) {
            index = indexRow;
            value = [];
          }

          value.push(currentValue);
          if (currentValue == nextValue) {
            props = { colSpan: 0, col_span: 0 };
          } else {
            props = { colSpan: value.length, col_span: value.length };
            value = [];
          }
        }

        if (val.isEventData) props = { ...props, record: val };
        if (val.isDateData) props = { ...props };

        return {
          ...props,
          week: i,
        };
      },
    });
  }

  const columns: TableColumnsType<Partial<RecordType>> = [
    {
      title: intl.formatMessage({ id: getLocaleKey('season_planner') }),
      dataIndex: 'seasonPlanner',
      colSpan: 0,
      onCell: (_, index) => {
        let props = {};
        if (index != undefined) {
          let prevValue = 'undefined';
          if (data[index - 1]) {
            prevValue = data[index - 1].seasonPlanner as string;
          }
          const currentValue = data[index].seasonPlanner;

          let rowSpan = 1;
          if (currentValue != prevValue) {
            for (let i = index; i < data.length; i++) {
              let nextValue = undefined;
              if (data[i + 1]) nextValue = data[i + 1].seasonPlanner;
              if (data[i].seasonPlanner != nextValue) {
                break;
              } else {
                rowSpan++;
              }
            }
          }
          if (currentValue == prevValue) {
            rowSpan = 0;
          }

          props = { style: { background: constantValues?.YTP_SEASON_PLAN_COLOR }, rowSpan };
        }
        return props;
      },
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('season_planner_detail') }),
      dataIndex: 'seasonPlannerDetail',
      colSpan: 0,
      ellipsis: true,
    },
    ...(weekColumns as TableColumnType<Partial<RecordType>>[]),
  ];

  const components = {
    body: {
      cell: CustomCell,
    },
  };

  return (
    <Table
      onRow={(record: Partial<RecordType>, rowIndex) => {
        return { style: { background: record.color } };
      }}
      bordered
      size="small"
      columns={columns}
      components={components}
      scroll={{ x: true }}
      dataSource={data}
      pagination={false}
      rowClassName={styles.cell}
      className={styles.table}
      summary={() => {
        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell colSpan={60} index={0}>
                <div style={{ height: '1rem' }}></div>
              </Table.Summary.Cell>
            </Table.Summary.Row>
          </>
        );
      }}
    />
  );
};
