import { Table, TableColumnsType, Space, Tooltip, Button, Popconfirm, message } from 'antd';
import { useContext } from 'react';
import { Context, getLocaleKey, YpiForm } from '../index';
import { useIntl } from 'umi';
import { Ypi } from '@/types/ypi';
import { get, destroy } from '@/services/ypi/api';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import styles from './table.less';
import moment from 'moment';

export default () => {
  const intl = useIntl();
  const { ypiTableData, form, fetchEventData } = useContext(Context);

  const onEdit = async (record: Ypi) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const ypiRes = await get(`master_ypis/${record.id}?full_data=true`);
      const ypi: YpiForm = ypiRes.data;
      const startDate = moment(ypi.tanggal_mulai.substring(0, 10));
      form?.setFieldsValue({
        ...ypi,
        tanggal_mulai_moment: startDate,
        tanggal_selesai: ypi.tanggal_selesai.substring(0, 10),
      });
      fetchEventData(startDate);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'YPI' }));
    }
    hideLoading();
  };

  const onDelete = async (record: Ypi) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      await destroy('master_ypis', record.id);
      message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName: 'YPI' }));
      window.location.reload();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'YPI' }));
    }
    hideLoading();
  };

  const columns: TableColumnsType<Ypi> = [
    {
      title: intl.formatMessage({ id: 'commonField.id' }),
      dataIndex: 'id',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('tournament_name') }),
      dataIndex: 'keterangan',
    },
    {
      title: intl.formatMessage({ id: 'commonField.start' }),
      dataIndex: 'tanggal_mulai',
    },
    {
      title: intl.formatMessage({ id: 'commonField.end' }),
      dataIndex: 'tanggal_selesai',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('coach') }),
      dataIndex: 'coach_name',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('ypi_season_planner') }),
      dataIndex: 'season_plans_text',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              size="small"
              onClick={() => onEdit(record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage(
                { id: 'message.confirmDeleteMessage' },
                { moduleName: 'Ypi' },
              )}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      className={styles.custom_table}
      rowClassName={styles.densed_row}
      bordered
      size="small"
      columns={columns}
      dataSource={ypiTableData}
      pagination={{ defaultPageSize: 5, size: 'small', showSizeChanger: true }}
      scroll={{ x: 'max-content' }}
    />
  );
};
