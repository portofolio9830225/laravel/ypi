import {
  Space,
  Tooltip,
  Table,
  Form,
  InputNumber,
  Input,
  Button,
  FormInstance,
  message,
  Checkbox,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { Context, SeasonPlanRecord, YpiSeasonPlanValue } from '../index';
import {
  EditOutlined,
  SaveOutlined,
  DoubleRightOutlined,
  CheckOutlined,
  CloseOutlined,
} from '@ant-design/icons';
import { create, destroy, update } from '@/services/ypi/api';
import styles from './table.less';
import { SECONDARY_BUTTON_COLOR } from '@/utils/constant';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';

const TABLE_NAME = 'season_plans';

interface CustomRowProps {
  index: number;
  record: Partial<SeasonPlanRecord>;
}

enum InputType {
  Date = 1,
  Time,
  Text,
  Number,
  None,
  Id,
  Checkbox,
  Color,
}

const EditableContext = createContext<FormInstance<SeasonPlanRecord> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, record, ...props }) => {
  const [form] = Form.useForm<SeasonPlanRecord>();
  const { seasonPlanValues, seasonPlanData, setSeasonPlanData } = useContext(Context);

  useEffect(() => {
    if (seasonPlanValues.length > 0 && record) {
      form.setFieldsValue({ active: false });
      const currentSpv = [...seasonPlanValues].filter((d) => d.season_plan_id === record.id);
      if (currentSpv.length > 0 && currentSpv[0].season_plan_id === record.id) {
        const currentActiveSpv = currentSpv.filter((d) => d.active === true);
        if (currentActiveSpv.length > 0) {
          form.setFieldsValue({ active: true, color: currentActiveSpv[0].bg_color });
          setSeasonPlanData((prevState) => {
            const newData = [...prevState];
            const currentSpIndex = seasonPlanData.findIndex(
              (d) => d.id === currentActiveSpv[0].season_plan_id,
            );
            newData[currentSpIndex] = { ...record, color: currentActiveSpv[0].bg_color };
            return newData;
          });
        } else {
          form.setFieldsValue({ active: false });
        }
      }
    }
  }, [seasonPlanValues]);

  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof SeasonPlanRecord | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: SeasonPlanRecord;
  handleToggleEdit: (form: FormInstance<SeasonPlanRecord> | null, record: SeasonPlanRecord) => void;
  handleSave: (
    form: FormInstance<SeasonPlanRecord> | null,
    record: SeasonPlanRecord,
  ) => Promise<void>;
  handleDelete: (record: SeasonPlanRecord) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const rowForm = useContext(EditableContext);
  const { setActiveSpId, setSeasonPlanValues } = useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleSeasonPlannerDetailOpen = (index: number) => {
    setActiveSpId(index);
  };

  const handleCheckboxChange = (e: CheckboxChangeEvent) => {
    if (e.target.checked) {
      setActiveSpId(record.id);
    } else {
      setSeasonPlanValues((prevState) => {
        let newData = [...prevState];
        newData = newData.map((d) => {
          if (d.season_plan_id === record.id) return { ...d, active: false };
          return d;
        });
        return newData;
      });
    }
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={!isEditMode}>
          <Tooltip title="Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(rowForm, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Canel">
            <Button
              size="small"
              onClick={() => handleToggleEdit(rowForm, record)}
              type="primary"
              shape="circle"
              danger
              icon={<CloseOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Ok">
            <Button
              size="small"
              onClick={() => {
                if (rowForm) handleSave(rowForm, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#52C419', borderColor: '#52C419' }}
              icon={<CheckOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={!isEditMode}>
          <Tooltip title="Season Plan Detail">
            <Button
              size="small"
              onClick={() => {
                handleSeasonPlannerDetailOpen(record.id);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
              icon={<DoubleRightOutlined />}
            />
          </Tooltip>
        </Access>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Checkbox) {
    childNode = (
      <>
        <Form.Item hidden name="id" />
        <Form.Item
          style={{ margin: 0, textAlign: 'center' }}
          name={dataIndex}
          valuePropName="checked"
          initialValue={false}
        >
          <Checkbox onChange={handleCheckboxChange} disabled={isEditMode} />
        </Form.Item>
      </>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Color && !record.edited) {
    return <td {...props} style={{ backgroundColor: record.color }} />;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;
    let valuePropName = undefined;
    let initialValue = undefined;

    if (inputType === InputType.Text) {
      inputNode = <Input style={{ width: '100%' }} />;
      rules = getRequiredRule('Name');
    }
    if (inputType === InputType.Color) {
      inputNode = <Input type="color" style={{ width: '100%' }} />;
      initialValue = SECONDARY_BUTTON_COLOR;
      rules = getRequiredRule('Color');
    }
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;

    childNode = (
      <Form.Item
        initialValue={initialValue}
        rules={rules}
        style={style}
        name={dataIndex}
        valuePropName={valuePropName}
      >
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const { seasonPlanData, setSeasonPlanData, setActiveSpId, setSeasonPlanValues } =
    useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.active' }),
      dataIndex: 'active',
      inputType: InputType.Checkbox,
    },
    {
      title: intl.formatMessage({ id: 'commonField.color' }),
      dataIndex: 'color',
      inputType: InputType.Color,
    },
    {
      title: intl.formatMessage({ id: 'commonField.name' }),
      dataIndex: 'name',
      inputType: InputType.Text,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      dataIndex: 'action',
      inputType: InputType.None,
      width: '20%',
    },
  ];

  const handleToggleEdit = (
    form: FormInstance<SeasonPlanRecord> | null,
    record: SeasonPlanRecord,
  ) => {
    const editedDataIndex = seasonPlanData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...seasonPlanData];
    currentData[editedDataIndex] = editedData;
    setSeasonPlanData(currentData);
  };

  const handleSave = async (
    form: FormInstance<SeasonPlanRecord> | null,
    record: SeasonPlanRecord,
  ) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = seasonPlanData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();

      let savedData: Partial<YpiSeasonPlanValue>;
      if (values && values.id) {
        const updateRes = await update(TABLE_NAME, values.id, values);
        savedData = updateRes.data;
      } else {
        const createRes = await create(TABLE_NAME, values);
        savedData = createRes.data;
      }
      savedData.color = values?.color;

      let currentData = [...seasonPlanData];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });

      // change color
      setSeasonPlanValues((prevValue) => {
        let editedSpv = [...prevValue];
        editedSpv = editedSpv.map((d) => {
          if (d.season_plan_id === savedData.id) return { ...d, bg_color: savedData.color };
          return { ...d };
        });
        return editedSpv;
      });

      setIsEditMode(false);
      setSeasonPlanData(currentData);
      setActiveSpId(savedData.id);
      hideLoading();
    } catch (error) {
      hideLoading();
      if ((error as any).errorFields) return;

      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: SeasonPlanRecord) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = seasonPlanData.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy(TABLE_NAME, record.id);
      }
      let currentData = [...seasonPlanData];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setSeasonPlanData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: SeasonPlanRecord) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    const newData = [...seasonPlanData];
    newData.push({
      id: undefined,
      edited: true,
      key: newData.length.toString(),
    });
    setIsEditMode(!isEditMode);
    setSeasonPlanData([...newData]);
  };

  const footer = () => {
    if (isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary">
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      rowClassName={styles.densed_row}
      dataSource={seasonPlanData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={isEditMode ? false : { size: 'small' }}
      // @ts-ignore: Unreachable code error
      onRow={(record: Partial<SeasonPlanRecord>, index?: number | undefined) => ({ record })}
    />
  );
};

export default EditableTable;
