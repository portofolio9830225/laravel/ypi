import { YpiDefaultTarget } from '@/types/ypi';
import {
  Modal,
  TableColumnsType,
  Table,
  Form,
  FormInstance,
  Select,
  Space,
  Divider,
  Typography,
  Input,
  Button,
  InputNumber,
} from 'antd';
import { useState, FC, useEffect, useContext } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { getLocaleKey, Context, YpiForm } from '../index';
import './volTarget.css';
import { PlusOutlined } from '@ant-design/icons';

interface CustomRowProps {
  index: number;
  form: FormInstance<YpiDefaultTarget>;
}

const CustomRow: React.FC<CustomRowProps> = ({ index, form, ...props }) => {
  return <tr {...props} />;
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof YpiDefaultTarget;
}

const CustomCell: FC<CellProps> = ({ children, dataIndex, ...props }) => {
  const [items, setItems] = useState([0, 15, 30, 60, 90, 120, 150, 180, 210, 240]);
  const [editedItem, setEditedItem] = useState(0);

  let childNode = children;

  const onEditedItemChange = (value: number) => {
    setEditedItem(value);
  };

  const addItem = (e: React.MouseEvent<HTMLAnchorElement>) => {
    e.preventDefault();
    if (editedItem) {
      setItems([...items, editedItem]);
      setEditedItem(0);
    }
  };

  if (dataIndex !== 'session')
    childNode = (
      <Form.Item style={{ margin: 0 }} name={dataIndex}>
        {/* <InputNumber style={{ width: '75px' }} controls={false} /> */}
        <Select
          style={{ width: 100 }}
          dropdownRender={(menu) => (
            <>
              {menu}
              <Divider style={{ margin: '8px 0' }} />
              <Space align="center" style={{ padding: '0 8px 4px' }}>
                <InputNumber
                  type="number"
                  placeholder="Please enter item"
                  value={editedItem}
                  onChange={onEditedItemChange}
                  controls={false}
                  style={{ width: '100%' }}
                />
                <Typography.Link onClick={addItem} style={{ whiteSpace: 'nowrap' }}>
                  <PlusOutlined />
                </Typography.Link>
              </Space>
            </>
          )}
        >
          {items.map((item) => (
            <Select.Option key={item}>{item}</Select.Option>
          ))}
        </Select>
      </Form.Item>
    );
  return <td {...props}>{childNode}</td>;
};

interface Props {
  visible: boolean;
  handleOk: () => void;
  handleCancel: () => void;
  mainForm: FormInstance<YpiForm> | undefined;
}

const VoltargetModal: FC<Props> = ({ visible, handleCancel, handleOk, mainForm }) => {
  const intl = useIntl();
  const [data, setData] = useState<Partial<YpiDefaultTarget>>({ key: '1', session: 'minutes' });
  const [totalVolume, setTotalVolume] = useState<number>(0);
  const [formVoltarget] = Form.useForm<YpiDefaultTarget>();
  const { form } = useContext(Context);

  const getTotalMinutes = (values: Partial<YpiDefaultTarget>): number => {
    let totalHours: number = 0;
    for (const property in values) {
      if (
        property == 'key' ||
        property == 'session' ||
        property == 'id' ||
        property == 'created_at' ||
        property == 'deleted_at' ||
        property == 'updated_at' ||
        property == 'master_ypi_id'
      )
        continue;
      if (!values[property]) continue;
      totalHours += parseInt(values[property]);
    }
    return totalHours;
  };

  useEffect(() => {
    if (visible) {
      const defaultTarget = form?.getFieldValue('default_target');
      formVoltarget.setFieldsValue(defaultTarget);
      setTotalVolume(getTotalMinutes(defaultTarget));
    } else {
      formVoltarget.resetFields();
      setTotalVolume(0);
    }
  }, [visible]);

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  const getDayColumns = (day: string) => {
    return [
      { title: intl.formatMessage({ id: 'commonField.am' }), dataIndex: day + '1' },
      { title: intl.formatMessage({ id: 'commonField.am' }), dataIndex: day + '2' },
      { title: intl.formatMessage({ id: 'commonField.pm' }), dataIndex: day + '3' },
      { title: intl.formatMessage({ id: 'commonField.pm' }), dataIndex: day + '4' },
    ];
  };

  const columns: TableColumnsType<YpiDefaultTarget> = [
    {
      title: intl.formatMessage({ id: getLocaleKey('session') }),
      children: [
        { title: intl.formatMessage({ id: getLocaleKey('per_week') }), dataIndex: 'session' },
      ],
    },
    {
      title: intl.formatMessage({ id: 'commonField.monday' }),
      children: getDayColumns('mon'),
      width: 50,
    },
    {
      title: intl.formatMessage({ id: 'commonField.tuesday' }),
      children: getDayColumns('tue'),
      width: 50,
    },
    {
      title: intl.formatMessage({ id: 'commonField.wednesday' }),
      children: getDayColumns('wed'),
      width: 50,
    },
    {
      title: intl.formatMessage({ id: 'commonField.thursday' }),
      children: getDayColumns('thu'),
      width: 50,
    },
    {
      title: intl.formatMessage({ id: 'commonField.friday' }),
      children: getDayColumns('fri'),
      width: 50,
    },
    {
      title: intl.formatMessage({ id: 'commonField.saturday' }),
      children: getDayColumns('sat'),
      width: 50,
    },
    {
      title: intl.formatMessage({ id: 'commonField.sunday' }),
      children: getDayColumns('sun'),
      width: 50,
    },
  ];

  const mapColumns = (col: any) => {
    const newCol = {
      ...col,
      onCell: (record: YpiDefaultTarget) => ({
        record,
        dataIndex: col.dataIndex,
        // title: col.title,
        // handleSave: this.handleSave
      }),
    };
    if (col.children) {
      newCol.children = col.children.map(mapColumns);
    }
    return newCol;
  };

  const processedColumns = columns.map(mapColumns);

  const footer = [
    <span key="volume">
      <FormattedMessage id={getLocaleKey('total_volume')} />:{' '}
      <Input style={{ width: '4rem', marginRight: '1rem' }} disabled value={totalVolume} />
    </span>,
    <Button key="back" onClick={handleCancel}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button
      key="submit"
      type="primary"
      onClick={() => {
        const values = formVoltarget.getFieldsValue();
        const totalMinutes = getTotalMinutes(values);
        mainForm?.setFieldsValue({ default_target: values, voltarget: totalMinutes });
        handleOk();
      }}
    >
      <FormattedMessage id="crud.process" />
    </Button>,
  ];

  const onValuesChange = (changedValues: any, values: YpiDefaultTarget) => {
    const totalMinutes = getTotalMinutes(values);
    setTotalVolume(totalMinutes);
  };

  return (
    <Modal
      footer={footer}
      bodyStyle={{ overflowX: 'auto' }}
      width="unset"
      visible={visible}
      closable={false}
    >
      <Form onValuesChange={onValuesChange} form={formVoltarget} component={false}>
        <Table
          pagination={false}
          size="small"
          components={components}
          bordered
          dataSource={[data as YpiDefaultTarget]}
          columns={processedColumns}
          rowClassName="densed-row"
          onHeaderRow={() => {
            return { className: 'densed-row' };
          }}
          scroll={{ x: 'max-content' }}
        />
      </Form>
    </Modal>
  );
};

export default VoltargetModal;
