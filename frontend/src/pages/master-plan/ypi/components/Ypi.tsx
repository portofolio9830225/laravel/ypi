import {
  Form,
  Input,
  InputNumber,
  Card,
  Row,
  Col,
  Tooltip,
  Button,
  Space,
  Select,
  DatePicker,
  message,
  Drawer,
} from 'antd';
import { Context, YpiForm } from '../index';
import { useContext, useState } from 'react';
import { useIntl, FormattedMessage, useAccess } from 'umi';
import { PlusOutlined, MinusOutlined, DoubleRightOutlined } from '@ant-design/icons';
import moment, { Moment } from 'moment';
import VolTargetModal from './VolTargetModal';
import SeasonPlannerDetailModal from './SeasonPlannerDetailModal';
import { getLocaleKey } from '../index';
import { MENU_MASTER_PLAN_COLOR } from '@/utils/constant';
import SeasonPlanTable from './SeasonPlanTable';
import { update, create } from '@/services/ypi/api';
import EventTable from './EventTable';
import { YpiEvent, SeasonPlanValue } from '@/types/ypi';

const { Option } = Select;

export default () => {
  const intl = useIntl();
  const { coachData, form, fetchEventData, seasonPlanValues, ypiEvents, fetchInitialData } =
    useContext(Context);
  const [volTargetVisible, setVolTargetVisible] = useState<boolean>(false);
  const access = useAccess();

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const onFinish = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const values = await form?.validateFields();
      if (values) {
        values.season_plan_values = seasonPlanValues as SeasonPlanValue[];
        values.ypi_events = ypiEvents as YpiEvent[];
        if (values.id) {
          await update('master_ypis', values.id, values);
        } else {
          await create('master_ypis', values);
        }
      }
      fetchInitialData();
      hideLoading();
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'YPI' }));
    } catch (error) {
      hideLoading();
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'YPI' }));
    }
  };

  const handleReset = () => {
    window.location.reload();
  };

  const disabledDate = (currentDate: Moment): boolean => {
    if (currentDate.format('d') != '1') return true;
    return false;
  };

  const getInitialValues = () => {
    return {
      ypi_goals: [{ id: undefined }],
    };
  };

  const onValuesChange = (changedValues: Partial<YpiForm>, values: YpiForm) => {
    if (changedValues.tanggal_mulai_moment) {
      const selectedDate = moment(changedValues.tanggal_mulai_moment);
      const nextYear = moment(selectedDate).add(1, 'y');
      const nextYearString = nextYear.format('YYYY-MM-DD');
      form?.setFieldsValue({
        tanggal_selesai: nextYearString,
        tanggal_mulai: selectedDate.format('YYYY-MM-DD'),
      });
      fetchEventData(selectedDate);
    }
  };

  return (
    <>
      <Card>
        <Form
          initialValues={getInitialValues()}
          form={form}
          name="form"
          layout="vertical"
          onValuesChange={onValuesChange}
        >
          <Form.Item hidden name="id" />
          <Form.Item hidden name="default_target" />
          <Form.Item hidden name="ypi_events" />
          <Form.Item hidden name="season_plan_values" />
          <Form.Item hidden name="deleted_ypi_goals" />

          <Form.Item
            name="keterangan"
            label={intl.formatMessage({ id: getLocaleKey('ypi_detail') })}
            rules={getRequiredRule('ypi_detail')}
          >
            <Input />
          </Form.Item>

          <Form.List
            name="ypi_goals"
            rules={[
              {
                validator: async (_, names) => {
                  if (!names || names.length < 1) {
                    return Promise.reject(
                      new Error(
                        intl.formatMessage(
                          { id: 'message.atLeast1' },
                          { fieldName: 'YpiForm Goals' },
                        ),
                      ),
                    );
                  }
                },
              },
            ]}
          >
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map(({ key, name, ...restField }, index) => (
                  <Row key={key} gutter={6} align={index === 0 ? 'middle' : undefined}>
                    <Form.Item name={[name, 'id']} hidden />
                    <Col span={21}>
                      <Form.Item
                        {...restField}
                        name={[name, 'goal']}
                        label={
                          index === 0 ? <FormattedMessage id={getLocaleKey('ypi_goals')} /> : ''
                        }
                        rules={getRequiredRule('ypi_goals')}
                      >
                        <Input />
                      </Form.Item>
                    </Col>
                    <Col span={2}>
                      {fields.length > 1 ? (
                        <Tooltip title={intl.formatMessage({ id: getLocaleKey('remove_goal') })}>
                          <Button
                            type="primary"
                            danger
                            onClick={() => {
                              const removedItemId = form?.getFieldValue(['ypi_goals', name, 'id']);
                              let deletedYpiGoals = form?.getFieldValue(['deleted_ypi_goals']);
                              if (removedItemId) {
                                if (deletedYpiGoals === undefined) {
                                  deletedYpiGoals = [removedItemId];
                                } else {
                                  (deletedYpiGoals as number[]).push(removedItemId);
                                }
                                form?.setFieldsValue({ deleted_ypi_goals: deletedYpiGoals });
                              }
                              remove(name);
                            }}
                            icon={<MinusOutlined />}
                          />
                        </Tooltip>
                      ) : null}
                    </Col>
                  </Row>
                ))}
                <Form.ErrorList errors={errors} />
                <Form.Item>
                  <Tooltip title={intl.formatMessage({ id: getLocaleKey('add_goal') })}>
                    <Button
                      style={{ backgroundColor: '#52c41a', borderColor: '#52c41a' }}
                      type="primary"
                      onClick={() => add()}
                      icon={<PlusOutlined />}
                    />
                  </Tooltip>
                </Form.Item>
              </>
            )}
          </Form.List>

          <Row gutter={6}>
            <Col span={12}>
              <Form.Item
                name="kode_pelatih"
                label={intl.formatMessage({ id: getLocaleKey('coach') })}
                rules={getRequiredRule('coach')}
                initialValue={access.coachId || undefined}
              >
                <Select disabled={access.coachId ? true : false}>
                  {coachData.map((d) => (
                    <Option key={d.id} value={d.id}>
                      {d.nama_lengkap}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>
            <Col span={9}>
              <Form.Item
                name="voltarget"
                label={intl.formatMessage({ id: getLocaleKey('default_vol') })}
                rules={getRequiredRule('default_vol')}
              >
                <InputNumber disabled type="number" style={{ width: '100%' }} />
              </Form.Item>
            </Col>
            <Col span={3}>
              <Form.Item label=" ">
                <Tooltip title={intl.formatMessage({ id: getLocaleKey('input_default_vol') })}>
                  <Button
                    type="primary"
                    onClick={() => setVolTargetVisible(true)}
                    icon={<DoubleRightOutlined />}
                  />
                </Tooltip>
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={6}>
            <Col span={8}>
              <Form.Item hidden name="tanggal_mulai" />
              <Form.Item
                name="tanggal_mulai_moment"
                label={intl.formatMessage({ id: getLocaleKey('ypi_start') })}
                rules={getRequiredRule('ypi_start')}
              >
                <DatePicker disabledDate={disabledDate} style={{ width: '100%' }} />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                name="tanggal_selesai"
                label={intl.formatMessage({ id: getLocaleKey('ypi_to') })}
                rules={getRequiredRule('ypi_to')}
              >
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                name="default_intensity"
                label={intl.formatMessage({ id: getLocaleKey('default_intensity') })}
                rules={getRequiredRule('default_intensity')}
              >
                <InputNumber type="number" style={{ width: '100%' }} />
              </Form.Item>
            </Col>
          </Row>

          <Card
            headStyle={{ backgroundColor: MENU_MASTER_PLAN_COLOR, color: 'white' }}
            title={<FormattedMessage id={getLocaleKey('ypi_season_planner')} />}
            size="small"
          >
            <SeasonPlanTable />
          </Card>

          <Card
            headStyle={{ backgroundColor: MENU_MASTER_PLAN_COLOR, color: 'white' }}
            title={<FormattedMessage id={getLocaleKey('ypi_events')} />}
            size="small"
          >
            <EventTable />
          </Card>
        </Form>
      </Card>
      <VolTargetModal
        mainForm={form}
        visible={volTargetVisible}
        handleOk={() => setVolTargetVisible(false)}
        handleCancel={() => setVolTargetVisible(false)}
      />
      <SeasonPlannerDetailModal />
      <div style={{ height: '8rem' }} />
      <Drawer visible={true} placement="bottom" closable={false} mask={false} height="5rem">
        <Space>
          <Button type="primary" htmlType="button" onClick={onFinish}>
            {intl.formatMessage({ id: 'crud.save' })}
          </Button>
          <Button
            type="primary"
            htmlType="button"
            style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            onClick={handleReset}
          >
            {intl.formatMessage({ id: 'crud.reset' })}
          </Button>
        </Space>
      </Drawer>
    </>
  );
};
