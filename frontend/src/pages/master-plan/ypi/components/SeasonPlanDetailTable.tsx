import {
  Space,
  Tooltip,
  Table,
  Form,
  InputNumber,
  Input,
  Button,
  FormInstance,
  message,
  Checkbox,
  Select,
} from 'antd';
import { FC, useState, createContext, useContext, useEffect } from 'react';
import { useIntl, Access, FormattedMessage } from 'umi';
import { Context, SeasonPlanDetailRecord } from '../index';
import { EditOutlined, SaveOutlined } from '@ant-design/icons';
import { create, destroy, update } from '@/services/ypi/api';
import styles from './table.less';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';

const TABLE_NAME = 'season_plan_details';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date = 1,
  Time,
  Text,
  Number,
  None,
  Id,
  Checkbox,
  Color,
  InputType,
}

const EditableContext = createContext<FormInstance<SeasonPlanDetailRecord> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  const [form] = Form.useForm();
  const { seasonPlanDetailData } = useContext(Context);

  useEffect(() => {
    if (seasonPlanDetailData.length > 0) {
      form.setFieldsValue(seasonPlanDetailData[index]);
    }
  }, [seasonPlanDetailData]);

  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof SeasonPlanDetailRecord | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: SeasonPlanDetailRecord;
  handleToggleEdit: (
    form: FormInstance<SeasonPlanDetailRecord> | null,
    record: SeasonPlanDetailRecord,
  ) => void;
  handleSave: (
    form: FormInstance<SeasonPlanDetailRecord> | null,
    record: SeasonPlanDetailRecord,
  ) => Promise<void>;
  handleDelete: (record: SeasonPlanDetailRecord) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const rowForm = useContext(EditableContext);
  const { seasonPlanDetailData, setSeasonPlanDetailData, activeSpId, constantValues } =
    useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const isFixedSeasonPlanDetail = (spdId: number): boolean => {
    if (
      activeSpId &&
      constantValues &&
      constantValues.FIXED_SEASON_PLAN_DETAILS_ID?.includes(spdId)
    )
      return true;
    return false;
  };

  const isFixedSeasonPlan = (): boolean => {
    if (activeSpId && constantValues && constantValues.FIXED_SEASON_PLANS_ID?.includes(activeSpId))
      return true;
    return false;
  };

  if (dataIndex === 'action' && !isFixedSeasonPlanDetail(record.id)) {
    childNode = (
      <Space>
        <Access accessible={record.edited || !isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(rowForm, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (rowForm) handleSave(rowForm, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  const handleCheckboxChange = (e: CheckboxChangeEvent) => {
    const currentSpd = [...seasonPlanDetailData];
    const currentRecord: SeasonPlanDetailRecord = { ...record, active: e.target.checked };
    currentSpd[fieldIndex] = currentRecord;
    setSeasonPlanDetailData(currentSpd);
  };

  if (inputType === InputType.Checkbox) {
    childNode = (
      <>
        <Form.Item hidden name="id" />
        <Form.Item
          style={{ margin: 0, textAlign: 'center' }}
          name="active"
          valuePropName="checked"
          initialValue={true}
        >
          <Checkbox
            disabled={isFixedSeasonPlan() || isFixedSeasonPlanDetail(record.id)}
            onChange={handleCheckboxChange}
          />
        </Form.Item>
      </>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;
    let valuePropName = undefined;
    let initialValue = undefined;

    if (inputType === InputType.Text) {
      inputNode = <Input style={{ width: '100%' }} />;
      rules = getRequiredRule('Name');
    }
    if (inputType === InputType.InputType) {
      inputNode = (
        <Select>
          <Select.Option value="text">Text</Select.Option>
          <Select.Option value="number">Number</Select.Option>
        </Select>
      );
      initialValue = 'text';
    }
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;

    childNode = (
      <Form.Item
        initialValue={initialValue}
        rules={rules}
        style={style}
        name={dataIndex}
        valuePropName={valuePropName}
      >
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const { seasonPlanDetailData, setSeasonPlanDetailData, activeSpId, constantValues } =
    useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  useEffect(() => {
    if (!activeSpId) setIsEditMode(false);
  }, [activeSpId]);

  const isFixedSeasonPlan = (): boolean => {
    if (activeSpId && constantValues && constantValues.FIXED_SEASON_PLANS_ID?.includes(activeSpId))
      return true;
    return false;
  };

  const actionColumn = isFixedSeasonPlan()
    ? []
    : [
        {
          title: intl.formatMessage({ id: 'crud.action' }),
          dataIndex: 'action',
          inputType: InputType.None,
          width: '20%',
        },
      ];

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.active' }),
      dataIndex: 'active',
      inputType: InputType.Checkbox,
    },
    {
      title: intl.formatMessage({ id: 'commonField.name' }),
      dataIndex: 'name',
      inputType: InputType.Text,
    },
    {
      title: intl.formatMessage({ id: 'commonField.input_type' }),
      dataIndex: 'input_type',
      inputType: InputType.InputType,
    },
    ...actionColumn,
  ];

  const handleToggleEdit = (
    form: FormInstance<SeasonPlanDetailRecord> | null,
    record: SeasonPlanDetailRecord,
  ) => {
    const editedDataIndex = seasonPlanDetailData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...seasonPlanDetailData];
    currentData[editedDataIndex] = editedData;
    setSeasonPlanDetailData(currentData);
  };

  const handleSave = async (
    form: FormInstance<SeasonPlanDetailRecord> | null,
    record: SeasonPlanDetailRecord,
  ) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = seasonPlanDetailData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();
      if (values) values.season_plan_id = activeSpId || 0;

      let savedData;
      if (values && values.id) {
        const updateRes = await update(TABLE_NAME, values.id, values);
        savedData = updateRes.data;
      } else {
        const createRes = await create(TABLE_NAME, values);
        savedData = createRes.data;
      }

      let currentData = [...seasonPlanDetailData];
      currentData[savedIndex] = { ...savedData, ...values };
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      hideLoading();
      setIsEditMode(false);
      setSeasonPlanDetailData(currentData);
    } catch (error) {
      setIsEditMode(false);
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: SeasonPlanDetailRecord) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = seasonPlanDetailData.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy(TABLE_NAME, record.id);
      }
      let currentData = [...seasonPlanDetailData];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setSeasonPlanDetailData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: SeasonPlanDetailRecord, index: number) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
        fieldIndex: index,
      }),
    };
    return newCol;
  });

  const handleAddRow = () => {
    const newData = [...seasonPlanDetailData];
    newData.push({
      id: undefined,
      edited: true,
      active: true,
      key: newData.length.toString(),
    });
    setIsEditMode(!isEditMode);
    setSeasonPlanDetailData([...newData]);
  };

  const footer = () => {
    if (isEditMode) return <></>;
    return (
      <Space>
        <Button onClick={handleAddRow} type="primary" disabled={isFixedSeasonPlan()}>
          <FormattedMessage id="crud.addNewData" />
        </Button>
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      className={styles.custom_table}
      components={components}
      bordered
      rowClassName={styles.densed_row}
      dataSource={seasonPlanDetailData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={footer}
      pagination={isEditMode ? false : { size: 'small' }}
      // @ts-ignore: Unreachable code error
      onRow={(data: Partial<SeasonPlanDetailRecord>, index?: number | undefined) => ({ index })}
    />
  );
};

export default EditableTable;
