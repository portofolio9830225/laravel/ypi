import {
  Space,
  Tooltip,
  Table,
  Form,
  InputNumber,
  Input,
  Button,
  FormInstance,
  message,
  Checkbox,
} from 'antd';
import { FC, useEffect, useState, createContext, useContext } from 'react';
import { useIntl, Access } from 'umi';
import { Context, EventRecord } from '../index';
import { EditOutlined, SaveOutlined, DoubleRightOutlined } from '@ant-design/icons';
import { create, destroy, update } from '@/services/ypi/api';
import styles from './table.less';
import { SECONDARY_BUTTON_COLOR } from '@/utils/constant';
import type { CheckboxChangeEvent } from 'antd/es/checkbox';

const TABLE_NAME = 'master_achievements';

interface CustomRowProps {
  index: number;
  record: Partial<EventRecord>;
}

enum InputType {
  Date = 1,
  Time,
  Text,
  Number,
  None,
  Id,
  Checkbox,
  Color,
}

const EditableContext = createContext<FormInstance<EventRecord> | null>(null);

const CustomRow: React.FC<CustomRowProps> = ({ index, record, ...props }) => {
  const [form] = Form.useForm<EventRecord>();
  const { ypiEvents } = useContext(Context);

  useEffect(() => {
    if (ypiEvents.length > 0) {
      const matchData = ypiEvents.find((d) => d.master_achievement_id === record.id);
      if (matchData && matchData.active) {
        form.setFieldsValue({ active: true });
      }
    }
  }, [ypiEvents]);

  return (
    <Form form={form} component={false}>
      <EditableContext.Provider value={form}>
        <tr {...props} />
      </EditableContext.Provider>
    </Form>
  );
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof EventRecord | 'action';
  inputType: InputType;
  fieldIndex: number;
  record: EventRecord;
  handleToggleEdit: (form: FormInstance<EventRecord> | null, record: EventRecord) => void;
  handleSave: (form: FormInstance<EventRecord> | null, record: EventRecord) => Promise<void>;
  handleDelete: (record: EventRecord) => Promise<void>;
  isEditMode: boolean;
}

const CustomCell: FC<CellProps> = ({
  children,
  record,
  dataIndex,
  fieldIndex,
  inputType,
  handleToggleEdit,
  handleSave,
  handleDelete,
  isEditMode,
  ...props
}) => {
  const intl = useIntl();
  let childNode = children;
  const rowForm = useContext(EditableContext);
  const { setActiveSpId, setYpiEvents, ypiEvents } = useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const handleSeasonPlannerDetailOpen = (index: number) => {
    setActiveSpId(index);
  };

  const handleCheckboxChange = (e: CheckboxChangeEvent) => {
    const otherYpiEvents = [...ypiEvents].filter((d) => d.id !== record.id);
    const selectedYpiEvent = ypiEvents.find((d) => d.master_achievement_id === record.id);
    if (selectedYpiEvent) selectedYpiEvent.active = e.target.checked;
    else otherYpiEvents.push({ active: true, master_achievement_id: record.id });
    setYpiEvents(otherYpiEvents);
  };

  if (dataIndex === 'action') {
    childNode = (
      <Space>
        <Access accessible={!isEditMode}>
          <Tooltip title="Toggle Edit">
            <Button
              size="small"
              onClick={() => handleToggleEdit(rowForm, record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={record.edited}>
          <Tooltip title="Save">
            <Button
              size="small"
              onClick={() => {
                if (rowForm) handleSave(rowForm, record);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#56f44e', borderColor: '#56f44e' }}
              icon={<SaveOutlined />}
            />
          </Tooltip>
        </Access>
        <Access accessible={!isEditMode}>
          <Tooltip title="Season Plan Detail">
            <Button
              size="small"
              onClick={() => {
                handleSeasonPlannerDetailOpen(record.id);
              }}
              type="primary"
              shape="circle"
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
              icon={<DoubleRightOutlined />}
            />
          </Tooltip>
        </Access>
      </Space>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Checkbox) {
    childNode = (
      <>
        <Form.Item hidden name="id" />
        <Form.Item
          style={{ margin: 0, textAlign: 'center' }}
          name={dataIndex}
          valuePropName="checked"
          initialValue={false}
        >
          <Checkbox onChange={handleCheckboxChange} />
        </Form.Item>
      </>
    );
    return <td {...props}>{childNode}</td>;
  }

  if (inputType === InputType.Color && !record.edited) {
    return <td {...props} style={{ backgroundColor: record.color }} />;
  }

  if (inputType !== InputType.None && inputType !== undefined && record.edited) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;
    let valuePropName = undefined;
    let initialValue = undefined;

    if (inputType === InputType.Text) {
      inputNode = <Input style={{ width: '100%' }} />;
      rules = getRequiredRule('Name');
    }
    if (inputType === InputType.Color) {
      inputNode = <Input type="color" style={{ width: '100%' }} />;
      initialValue = SECONDARY_BUTTON_COLOR;
      rules = getRequiredRule('Color');
    }
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;

    childNode = (
      <Form.Item
        initialValue={initialValue}
        rules={rules}
        style={style}
        name={dataIndex}
        valuePropName={valuePropName}
      >
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

const EditableTable: FC = () => {
  const intl = useIntl();
  const { eventData, setEventData, setActiveSpId } = useContext(Context);
  const [isEditMode, setIsEditMode] = useState<boolean>(false);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.active' }),
      dataIndex: 'active',
      inputType: InputType.Checkbox,
    },
    {
      title: intl.formatMessage({ id: 'pages.competition-info.comp-detail' }),
      dataIndex: 'nama_event',
    },
    {
      title: intl.formatMessage({ id: 'commonField.start' }),
      dataIndex: 'tanggal_mulai',
    },
    {
      title: intl.formatMessage({ id: 'commonField.end' }),
      dataIndex: 'tanggal_akhir',
    },
  ];

  const handleToggleEdit = (form: FormInstance<EventRecord> | null, record: EventRecord) => {
    const editedDataIndex = eventData.findIndex((d) => d.key === record.key);
    const editedData = { ...record };
    editedData.edited = !editedData.edited;
    setIsEditMode(!isEditMode);
    form?.setFieldsValue(editedData);

    const currentData = [...eventData];
    currentData[editedDataIndex] = editedData;
    setEventData(currentData);
  };

  const handleSave = async (form: FormInstance<EventRecord> | null, record: EventRecord) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = eventData.findIndex((d) => d.key === record.key);
      const values = await form?.validateFields();

      let savedData;
      if (values && values.id) {
        const updateRes = await update(TABLE_NAME, values.id, values);
        savedData = updateRes.data;
      } else {
        const createRes = await create(TABLE_NAME, values);
        savedData = createRes.data;
      }
      savedData.color = values?.color;

      let currentData = [...eventData];
      currentData[savedIndex] = savedData;
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setIsEditMode(false);
      setEventData(currentData);
      setActiveSpId(savedData.id);
      hideLoading();
    } catch (error) {
      hideLoading();
      if ((error as any).errorFields) return;

      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const handleDelete = async (record: EventRecord) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const savedIndex = eventData.findIndex((d) => d.key === record.key);
      if (record.id) {
        await destroy(TABLE_NAME, record.id);
      }
      let currentData = [...eventData];
      currentData.splice(savedIndex, 1);
      currentData = currentData.map((d, i) => {
        return { ...d, key: i.toString() };
      });
      setEventData(currentData);
      hideLoading();
      setIsEditMode(false);
    } catch (error) {
      hideLoading();
      setIsEditMode(false);
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'Data' }));
    }
  };

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: EventRecord) => ({
        record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        handleToggleEdit,
        handleSave,
        handleDelete,
        isEditMode,
      }),
    };
    return newCol;
  });

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      rowClassName={styles.densed_row}
      bordered
      dataSource={eventData}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      pagination={isEditMode ? false : { size: 'small' }}
      // @ts-ignore: Unreachable code error
      onRow={(record: Partial<EventRecord>, index?: number | undefined) => ({ record })}
    />
  );
};

export default EditableTable;
