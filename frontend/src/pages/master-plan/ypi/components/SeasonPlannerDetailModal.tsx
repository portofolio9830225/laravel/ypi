import { Modal, Button, message } from 'antd';
import { useEffect, useState, FC, useContext } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { YpiSeasonPlanValue, Context, SeasonPlanDetailRecord, SeasonPlanRecord } from '../index';
import SpdTable from './SeasonPlanDetailTable';
import { get } from '@/services/ypi/api';

const VoltargetModal: FC = () => {
  const intl = useIntl();
  const {
    activeSpId,
    setActiveSpId,
    setSeasonPlanDetailData,
    seasonPlanDetailData,
    seasonPlanData,
    seasonPlanValues,
    setSeasonPlanValues,
  } = useContext(Context);
  const [visible, setVisible] = useState<boolean>(false);
  const [activeSeasonPlan, setActiveSeasonPlan] = useState<Partial<SeasonPlanRecord>>();

  const fetchData = async (seasonPlanId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get('season_plan_details?season_plan_id=' + seasonPlanId);
      let detailData: Partial<SeasonPlanDetailRecord>[] = dataRes.data;
      detailData = detailData.map((d, i) => {
        const dataExist: Partial<YpiSeasonPlanValue> | undefined = seasonPlanValues.find(
          (dd) => dd.season_plan_detail_id === d.id,
        );
        if (dataExist) return { ...d, key: i.toString(), active: dataExist.active };

        return { ...d, key: i.toString(), active: true };
      });
      setSeasonPlanDetailData(detailData);

      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  useEffect(() => {
    if (activeSpId) {
      fetchData(activeSpId);
      const activeSp = seasonPlanData.find((d) => d.id === activeSpId);
      if (activeSp) setActiveSeasonPlan(activeSp);
      setVisible(true);
    } else {
      closeModal();
    }
  }, [activeSpId]);

  const closeModal = () => {
    setVisible(false);
    setSeasonPlanDetailData([]);
  };

  const handleProcess = () => {
    const currentSpv = [...seasonPlanValues];
    const currentSpd = [...seasonPlanDetailData];

    const deletedCurrentSpv = currentSpv.filter((d) => d.season_plan_id !== activeSpId);
    const newSpv: Partial<YpiSeasonPlanValue>[] = currentSpd.map((d) => {
      return {
        season_plan_id: d.season_plan_id,
        season_plan_detail_id: d.id,
        active: d.active,
        bg_color: activeSeasonPlan?.color,
        seasonPlan: activeSeasonPlan?.name,
        seasonPlanDetail: d.name,
      };
    });
    setSeasonPlanValues([...deletedCurrentSpv, ...newSpv]);
    setActiveSpId(undefined);
  };

  const handleCancel = () => {
    setActiveSpId(undefined);
  };

  const footer = [
    <Button key="cancel" onClick={handleCancel}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button key="submit" type="primary" onClick={handleProcess}>
      <FormattedMessage id="crud.process" />
    </Button>,
  ];

  return (
    <Modal
      footer={footer}
      title={activeSeasonPlan?.name}
      centered
      visible={visible}
      closable={false}
    >
      <SpdTable />
    </Modal>
  );
};

export default VoltargetModal;
