import { Row, Col } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Ytp from './components/Ytp';
import FormYpi from './components/Ypi';
import YpiTable from './components/YpiTable';
import { useAccess, useIntl } from 'umi';
import {
  Coach,
  CompetitionDetail,
  SeasonPlan,
  SeasonPlanDetail,
  SeasonPlanValue,
  Ypi,
  YpiEvent,
  YpiDefaultTarget,
  YpiGoal,
  Constant,
} from '@/types/ypi';
import moment, { Moment } from 'moment';
import { message, Form, FormInstance } from 'antd';

export interface YpiTable extends Ypi {
  coach_name?: string;
  season_plans_text?: string;
  key: string;
}

interface DevType {
  edited: boolean;
  key: string;
  active: boolean;
}

export interface SeasonPlanRecord extends SeasonPlan, DevType {
  color: string;
}

export interface SeasonPlanDetailRecord extends SeasonPlanDetail, DevType {}

export interface YpiForm extends Ypi {
  tanggal_mulai_moment: Moment;
  season_plan_values: SeasonPlanValue[];
  ypi_events: YpiEvent[];
  default_target: YpiDefaultTarget;
  ypi_goals: YpiGoal[];
  deleted_ypi_goals?: number[];
}

export interface YpiSeasonPlanValue extends SeasonPlanValue {
  seasonPlan: string;
  seasonPlanDetail: string;
  color: string;
}

export interface EventRecord extends CompetitionDetail, DevType {}

export interface ConstantValue {
  FIXED_SEASON_PLANS_ID: number[];
  FIXED_SEASON_PLAN_DETAILS_ID: number[];
  DEFAULT_SEASON_PLANS: YpiSeasonPlanValue[];
  SPD_ACADEMY_INDEX_ID: number;
  SPD_SPORT_INDEX_ID: number;
  SPD_INTENSITY_ID: number;
  SPD_VOLUME_ID: number;
  SPD_SPESIFIC_ID: number;
  YTP_DATE_COLOR: string;
  YTP_SEASON_PLAN_COLOR: string;
}

export interface ContextType {
  ypiTableData: YpiTable[];
  coachData: Coach[];
  seasonPlanData: Partial<SeasonPlanRecord>[];
  setSeasonPlanData: React.Dispatch<React.SetStateAction<Partial<SeasonPlanRecord>[]>>;
  form: FormInstance<YpiForm> | undefined;
  activeSpId: number | undefined;
  setActiveSpId: React.Dispatch<React.SetStateAction<number | undefined>>;
  seasonPlanDetailData: Partial<SeasonPlanDetailRecord>[];
  setSeasonPlanDetailData: React.Dispatch<React.SetStateAction<Partial<SeasonPlanDetailRecord>[]>>;
  seasonPlanValues: Partial<YpiSeasonPlanValue>[];
  setSeasonPlanValues: React.Dispatch<React.SetStateAction<Partial<YpiSeasonPlanValue>[]>>;
  eventData: Partial<EventRecord>[];
  setEventData: React.Dispatch<React.SetStateAction<Partial<EventRecord>[]>>;
  ypiEvents: Partial<YpiEvent>[];
  setYpiEvents: React.Dispatch<React.SetStateAction<Partial<YpiEvent>[]>>;
  fetchEventData: (startDate: Moment) => Promise<void>;
  fetchInitialData: () => Promise<void>;
  constantValues: Partial<ConstantValue> | undefined;
}

export const Context = createContext<ContextType>({
  ypiTableData: [],
  coachData: [],
  seasonPlanData: [],
  setSeasonPlanData: () => {},
  form: undefined,
  activeSpId: -1,
  setActiveSpId: () => {},
  seasonPlanDetailData: [],
  setSeasonPlanDetailData: () => {},
  seasonPlanValues: [],
  setSeasonPlanValues: () => {},
  eventData: [],
  setEventData: () => {},
  ypiEvents: [],
  setYpiEvents: () => {},
  fetchEventData: async () => {},
  fetchInitialData: async () => {},
  constantValues: undefined,
});

export const getLocaleKey = (fieldName: string) => {
  return 'pages.ypi.' + fieldName;
};

export default () => {
  const intl = useIntl();
  const access = useAccess();
  const [form] = Form.useForm<YpiForm>();
  const [ypiTableData, setYpiTableData] = useState<YpiTable[]>([]);
  const [seasonPlanValues, setSeasonPlanValues] = useState<Partial<YpiSeasonPlanValue>[]>([]);
  const [seasonPlanData, setSeasonPlanData] = useState<Partial<SeasonPlanRecord>[]>([]);
  const [seasonPlanDetailData, setSeasonPlanDetailData] = useState<
    Partial<SeasonPlanDetailRecord>[]
  >([]);
  const [eventData, setEventData] = useState<Partial<EventRecord>[]>([]);
  const [ypiEvents, setYpiEvents] = useState<Partial<YpiEvent>[]>([]);
  const [coachData, setCoachData] = useState<Coach[]>([]);
  const [activeSpId, setActiveSpId] = useState<number>();
  const [constantValues, setConstantValues] = useState<Partial<ConstantValue>>();

  const fetchInitialData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const coachRes = await get('master_pelatihs');
      setCoachData(coachRes.data);

      let ypiRes;
      if (access.coachId) {
        ypiRes = await get('master_ypis?forTableData=true&coach_id=' + access.coachId);
      } else {
        ypiRes = await get('master_ypis?forTableData=true');
      }

      setYpiTableData(
        (ypiRes.data as YpiTable[]).map((d, i) => ({
          ...d,
          key: i.toString(),
          tanggal_mulai: d.tanggal_mulai.substring(0, 10),
          tanggal_selesai: d.tanggal_selesai.substring(0, 10),
        })),
      );
      const seasonPlanRes = await get('season_plans');
      const seasonPlan: SeasonPlanRecord[] = seasonPlanRes.data;
      setSeasonPlanData(
        seasonPlan.map((d, i) => {
          return { ...d, key: i.toString() };
        }),
      );

      const constantRes = await get('constants');
      const constants: Constant[] = constantRes.data;
      let constVal: Partial<ConstantValue> = {};

      const defaultSpRes = await get('constants/default_season_plans');
      const defaultSp: YpiSeasonPlanValue[] = defaultSpRes.data;
      setSeasonPlanValues(defaultSp);
      constVal = { ...constVal, DEFAULT_SEASON_PLANS: defaultSp };

      const fixedSp = constants.find((d) => d.key === 'FIXED_SEASON_PLANS_ID');
      if (fixedSp) {
        const fixedSpArray = fixedSp.value.split(',').map((d) => parseInt(d));
        constVal = { ...constVal, FIXED_SEASON_PLANS_ID: fixedSpArray };
      }

      const fixedSpd = constants.find((d) => d.key === 'FIXED_SEASON_PLAN_DETAILS_ID');
      if (fixedSpd) {
        const fixedSpdArray = fixedSpd.value.split(',').map((d) => parseInt(d));
        constVal = { ...constVal, FIXED_SEASON_PLAN_DETAILS_ID: fixedSpdArray };
      }

      const ytpDateColor = constants.find((d) => d.key === 'YTP_DATE_COLOR');
      if (ytpDateColor) constVal = { ...constVal, YTP_DATE_COLOR: ytpDateColor.value };

      const ytpSpColor = constants.find((d) => d.key === 'YTP_SEASON_PLAN_COLOR');
      if (ytpSpColor) constVal = { ...constVal, YTP_SEASON_PLAN_COLOR: ytpSpColor.value };

      setConstantValues(constVal);
    } catch (error) {
      console.log(error);

      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependency Data' }),
      );
    }
    hideLoading();
  };

  const fetchEventData = async (startDate: Moment) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const start = startDate.format('YYYY-MM-DD');
      const end = moment(startDate).add(1, 'y').format('YYYY-MM-DD');
      const eventRes = await get(`master_achievements?start=${start}&end=${end}`);
      let eventData: CompetitionDetail[] = eventRes.data;
      eventData = eventData.map((d, i) => ({
        ...d,
        key: i.toString(),
        tanggal_mulai: d.tanggal_mulai.substring(0, 10),
        tanggal_akhir: d.tanggal_akhir.substring(0, 10),
      }));
      setEventData(eventData);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Events Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    fetchInitialData();
  }, []);

  useEffect(() => {
    if (eventData) {
      const ypi_events = form.getFieldValue('ypi_events');
      if (ypi_events) setYpiEvents(ypi_events);
    }
  }, [eventData]);

  useEffect(() => {
    if (ypiEvents) {
      const spValues = form.getFieldValue('season_plan_values');
      if (spValues) setSeasonPlanValues(spValues);
    }
  }, [ypiEvents]);

  const contextValue: ContextType = {
    ypiTableData,
    coachData,
    seasonPlanData,
    setSeasonPlanData,
    form,
    activeSpId,
    setActiveSpId,
    seasonPlanDetailData,
    setSeasonPlanDetailData,
    seasonPlanValues,
    setSeasonPlanValues,
    eventData,
    setEventData,
    ypiEvents,
    setYpiEvents,
    fetchEventData,
    fetchInitialData,
    constantValues,
  };

  return (
    <Context.Provider value={contextValue}>
      <YpiTable />
      <Row gutter={6}>
        <Col span={12}>
          <FormYpi />
        </Col>
        <Col span={12}>
          <Ytp />
        </Col>
      </Row>
    </Context.Provider>
  );
};
