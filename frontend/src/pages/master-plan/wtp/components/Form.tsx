import { Button, Form, Input, Row, Col, message, FormInstance, Space, Select } from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context, getLocaleKey, Wtp } from '../index';
import { FormattedMessage, useIntl, Access } from 'umi';
import { DetailWeeklyPlan, HeaderWeeklyPlan, Ypi, DateSeasonPlanner } from '@/types/ypi';
import { create, get, destroy } from '@/services/ypi/api';
import Table from './Table';
import moment from 'moment';
import CopyDialog from './CopyDialog';
import { CopyOutlined, PrinterOutlined } from '@ant-design/icons';

const { Option } = Select;

interface RelatedData {
  phase_periodization: string;
  tech_devs: string;
  tactics: string;
  physics: string;
  mentals: string;
  intensity: string;
  volume: string;
  spesific: string;
  no_hour_week: string;
  total_no_hour_week: string;
}

export default () => {
  const { form, setPrintVisible, printData, setPrintData, ypiData } = useContext(Context);
  const intl = useIntl();
  const [weeksData, setWeeksData] = useState<DateSeasonPlanner[]>();
  const [copyVisible, setCopyVisible] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const fillFields = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const masterYpiId = form?.getFieldValue('master_ypi_id');
      const weekIndex = form?.getFieldValue('minggu');

      const currentWeek = weeksData?.find((d) => d.week === weekIndex);

      const relatedDataRes = await get(
        `header_weekly_plans/related_data?ypi_id=${masterYpiId}&week=${weekIndex}`,
      );
      const relatedData: RelatedData = relatedDataRes.data;

      const processedData = {
        tanggal_awal: `${currentWeek?.monday}-${currentWeek?.month_number_str}-${currentWeek?.year}`,
        tanggal_akhir: `${currentWeek?.sunday}-${currentWeek?.month_number_str}-${currentWeek?.year}`,
        periodization: relatedData.phase_periodization,
        technical_development: relatedData.tech_devs,
        tactic: relatedData.tactics,
        physical: relatedData.physics,
        mental: relatedData.mentals,
        specificity: parseInt(relatedData.spesific) || 0,
        volume: parseInt(relatedData.volume) || 0,
        intensity: parseInt(relatedData.intensity) || 0,
        target_tr_vol: parseInt(relatedData.no_hour_week) || 0,
        target_tr_vol_prosen: parseInt(relatedData.volume) || 0,
        target_tr_intensity: parseInt(relatedData.intensity) || 0,
        target_tr_load: parseInt(relatedData.total_no_hour_week) || 0,
      };

      form?.setFieldsValue(processedData);
      setPrintData((prev) => ({
        ...prev,
        relatedData: { ...prev?.relatedData, ...processedData },
      }));
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  const fetchWeekData = async (ypiId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const selectedYpi = ypiData?.find((d) => d.id === ypiId);
      if (selectedYpi) {
        const weekDataRes = await get(
          `annual_plans/seasonPlanner?start=${selectedYpi.tanggal_mulai}&end=${selectedYpi.tanggal_selesai}`,
        );
        setWeeksData(weekDataRes.data);
      }
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  const handleSave = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    // console.log(form?.getFieldsValue());
    // return;

    try {
      const values = await form?.validateFields();
      if (values?.detailsWtp) {
        let detailsWtp = [...values?.detailsWtp];
        detailsWtp = detailsWtp.map((d) => {
          return {
            ...d,
            master_ypi_id: values.master_ypi_id,
            minggu: values.minggu,
            tanggal: d.tanggal_moment?.format('YYYY-MM-DD'),
            jam: d.jam_moment?.format('HH:mm:ss'),
          };
        });
        values.detailsWtp = detailsWtp;
      }
      await create('header_weekly_plans/withDetails', values);
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Data' }));
      form?.resetFields();
      hideLoading();
    } catch (error) {
      hideLoading();
      if ((error as any).errorFields) return;
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const calculateIntensity = (totalSet: number): number => {
    if (totalSet < 5) return 1;
    if (totalSet < 9) return 2;
    if (totalSet < 13) return 3;
    if (totalSet < 17) return 4;
    return 5;
  };

  const findGreatestSessions = (tableData: DetailWeeklyPlan[]) => {
    const sessions: number[] = [];
    for (const d of tableData) {
      if (d.session) sessions.push(d.session);
    }

    if (sessions.length > 0) {
      const sessionPerWeek = Math.max(...sessions);
      form?.setFieldsValue({ session_per_week: sessionPerWeek });
      setPrintData((prev) => ({ ...prev, session_per_week: sessionPerWeek }));
    }
  };

  const externalCalculation = (currentDetailsWtp: DetailWeeklyPlan[], values: HeaderWeeklyPlan) => {
    // external calculation (sum)
    let actualTrVol = 0;
    let actualTrLoad = 0;
    let trLoadPerDuration = 0;
    let actualTrIntensity = 0;
    for (const val of currentDetailsWtp) {
      actualTrVol += val.duration || 0;
      actualTrLoad += val.tr_load || 0;
      if (val.duration > 0) {
        trLoadPerDuration = trLoadPerDuration + val.tr_load / val.duration;
      } else {
        trLoadPerDuration = trLoadPerDuration;
      }
    }
    const targetTrVol = values.target_tr_vol || 0;
    let actualTrVolPercent = 0;
    if (targetTrVol > 0) actualTrVolPercent = (actualTrVol / targetTrVol) * 100;
    if (currentDetailsWtp.length > 1) {
      actualTrIntensity = (trLoadPerDuration / (currentDetailsWtp.length + 1 - 1)) * 20;
    }

    const processedData = {
      actual_tr_vol: actualTrVol,
      actual_tr_load: actualTrLoad,
      actual_tr_intensity: actualTrIntensity,
      actual_tr_vol_prosen: Math.round(actualTrVolPercent),
    };

    form?.setFieldsValue(processedData);
    setPrintData((prev) => ({ ...prev, relatedData: { ...prev?.relatedData, ...processedData } }));
  };

  const fetchData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const masterYpiId = form?.getFieldValue('master_ypi_id');
      const weekIndex = form?.getFieldValue('minggu');
      const data = await get(
        `header_weekly_plans?master_ypi_id=${masterYpiId}&minggu=${weekIndex}`,
      );

      hideLoading();
      if (data.data) {
        const existingData: Wtp = { ...data.data };
        existingData.tanggal_awal = existingData.tanggal_awal.substring(0, 10);
        existingData.tanggal_akhir = existingData.tanggal_akhir.substring(0, 10);
        const currentYpi = ypiData.find((d) => d.id === existingData.master_ypi_id);
        if (currentYpi) {
          existingData.ypi_name = currentYpi.keterangan;
          existingData.coach_name = currentYpi.coach.nama_lengkap;
        }
        const detailsWtp = existingData.detailsWtp?.map((d, i) => {
          return {
            ...d,
            key: i.toString(),
            no: i + 1,
            fieldIndex: i,
            tanggal_moment: moment(d.tanggal),
            jam_moment: moment(d.jam, 'HH:mm:ss'),
          };
        });

        const processedData = { ...existingData, detailsWtp };
        form?.setFieldsValue(processedData);
        setPrintData((prev) => ({ ...existingData, relatedData: prev?.relatedData }));
        findGreatestSessions(detailsWtp as DetailWeeklyPlan[]);
        externalCalculation(detailsWtp as DetailWeeklyPlan[], existingData);
      } else {
        form?.resetFields(['detailsWtp', 'notes', 'weekly_training_goal']);
      }
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  const onValuesChange = (changedValues: HeaderWeeklyPlan, values: HeaderWeeklyPlan) => {
    if (changedValues.master_ypi_id) {
      fetchWeekData(changedValues.master_ypi_id);
    }

    if (changedValues.minggu || changedValues.master_ypi_id) {
      if (values.minggu && values.master_ypi_id) {
        fetchData();
        fillFields();
      }
    }

    if (changedValues.detailsWtp && values.detailsWtp) {
      const currentDetailsWtp = values.detailsWtp;
      for (const key in changedValues.detailsWtp) {
        const detailWtp = { ...currentDetailsWtp[key] };
        const workTime = detailWtp.work_time;
        const set = detailWtp.set;
        const restInterval = detailWtp.rest_interval || 0;
        const restPeriod = detailWtp.rest_period || 0;

        // internal calculation
        if (workTime && set) {
          const duration = (workTime * set + restInterval * (set - 1) + restPeriod) / 60;

          const intensity = calculateIntensity(set);
          const trLoad = duration * intensity;

          detailWtp.duration = Math.round(duration);
          detailWtp.intensity = Math.round(intensity);
          detailWtp.tr_load = Math.round(trLoad);
          currentDetailsWtp[key] = detailWtp;
          form?.setFieldsValue({ detailsWtp: currentDetailsWtp });
        }

        // new data calculation
        const keyInt = parseInt(key);
        if (keyInt > 0) {
          const prevData = { ...currentDetailsWtp[keyInt - 1] };
          if (
            detailWtp.tanggal_moment?.isSame(prevData.tanggal_moment, 'day') &&
            detailWtp.session &&
            prevData.session &&
            detailWtp.session === prevData.session &&
            prevData.jam_moment &&
            prevData.duration
          ) {
            detailWtp.jam_moment = moment(prevData.jam_moment).add(prevData.duration, 'minutes');
            currentDetailsWtp[key] = detailWtp;
            form?.setFieldsValue({ detailsWtp: currentDetailsWtp });
          }
        }

        externalCalculation(currentDetailsWtp as DetailWeeklyPlan[], values);
      }
      findGreatestSessions(currentDetailsWtp as DetailWeeklyPlan[]);
    }
  };

  const handleDelete = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const id = form?.getFieldValue('id');
      if (id) {
        await destroy('header_weekly_plans', id);
      }
      message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName: 'Data' }));
      hideLoading();
      form?.resetFields();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorDelete' }, { fieldName: 'Data' }));
    }
  };

  const handleCancel = async () => {
    form?.resetFields();
  };

  const handleCopyOpen = () => {
    setCopyVisible(true);
  };

  const handleOpenPrint = () => {
    setPrintVisible(true);
  };

  const handleCopyOk = async (weekData: DateSeasonPlanner, ypi_id: number, week_index: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);

    const startDateString = form?.getFieldValue('tanggal_awal');
    const sourceStartDate = moment(startDateString, 'DD-MM-YYYY');
    const targetStartDate = moment(
      `${weekData.year}-${weekData.month_number_str}-${weekData.monday}`,
    );
    const currentDetailsWtp: Partial<DetailWeeklyPlan>[] = form?.getFieldValue('detailsWtp');
    const newDetailsWtp: Partial<DetailWeeklyPlan>[] = currentDetailsWtp.map((d, i) => {
      const processedDate = moment(targetStartDate).add(
        d.tanggal_moment?.diff(sourceStartDate, 'd'),
        'd',
      );
      return {
        ...d,
        id: undefined,
        key: i.toString(),
        tanggal_moment: processedDate,
        tanggal: processedDate.format('YYYY-MM-DD'),
      };
    });
    const weekGoal = form?.getFieldValue('weekly_training_goal');
    const notes = form?.getFieldValue('notes');

    form?.setFieldsValue({ master_ypi_id: ypi_id, minggu: week_index });
    await Promise.all([fetchData(), fillFields()]);
    form?.setFieldsValue({ detailsWtp: newDetailsWtp, weekly_training_goal: weekGoal, notes });

    hideLoading();
    setCopyVisible(false);
  };

  const handleCopyCancel = () => {
    setCopyVisible(false);
  };

  return (
    <>
      <Form
        form={form}
        layout="vertical"
        initialValues={{ detailsWtp: [{ key: '0', fieldIndex: 0, no: 1 }], deletedDetailsWtp: [] }}
        onValuesChange={onValuesChange}
      >
        <Form.Item name="id" hidden />
        <Form.Item name="detailsWtp" hidden />
        <Form.Item name="deletedDetailsWtp" hidden />

        <Row gutter={6}>
          <Col span={12}>
            <Form.Item
              name="master_ypi_id"
              label={<FormattedMessage id={getLocaleKey('ypi_detail')} />}
              rules={getRequiredRule('ypi_detail')}
            >
              <Select>
                {ypiData?.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.keterangan}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="minggu"
              label={<FormattedMessage id={getLocaleKey('weekly')} />}
              rules={getRequiredRule('weekly')}
            >
              <Select disabled={!weeksData ? true : false}>
                {weeksData?.map((d) => (
                  <Option key={d.week} value={d.week}>
                    {<FormattedMessage id={getLocaleKey('weekly')} />}: {d.week}{' '}
                    {<FormattedMessage id={getLocaleKey('period')} />}: {d.monday}/
                    {d.month_number_str}/{d.year} {<FormattedMessage id="commonField.to" />}{' '}
                    {d.sunday}/{d.month_number_str}/{d.year}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={6}>
          <Col span={3}>
            <Form.Item
              name="periodization"
              label={<FormattedMessage id={getLocaleKey('phase_periodization')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item name="tanggal_awal" label={<FormattedMessage id={getLocaleKey('period')} />}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item name="tanggal_akhir" label={<FormattedMessage id="commonField.to" />}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={15}>
            <Form.Item
              name="weekly_training_goal"
              label={<FormattedMessage id={getLocaleKey('weekly_goal')} />}
            >
              <Input />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={6}>
          <Col span={12}>
            <Form.Item
              name="technical_development"
              label={<FormattedMessage id={getLocaleKey('skills')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="tactic" label={<FormattedMessage id={getLocaleKey('tactic')} />}>
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={6}>
          <Col span={12}>
            <Form.Item name="physical" label={<FormattedMessage id={getLocaleKey('physical')} />}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item name="mental" label={<FormattedMessage id={getLocaleKey('mental')} />}>
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>

        <Row gutter={6}>
          <Col span={6}>
            <Form.Item
              name="session_per_week"
              label={<FormattedMessage id={getLocaleKey('session_per_week')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="specificity"
              label={<FormattedMessage id={getLocaleKey('specificity')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="volume" label={<FormattedMessage id={getLocaleKey('volume')} />}>
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="intensity" label={<FormattedMessage id={getLocaleKey('intensity')} />}>
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>

        <Table form={form as FormInstance<HeaderWeeklyPlan>} />

        <Row gutter={6}>
          <Col span={6}>
            <Form.Item
              name="actual_tr_vol"
              label={<FormattedMessage id={getLocaleKey('actual_tr_vol')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>

          <Col span={6}>
            <Form.Item
              name="actual_tr_vol_prosen"
              label={<FormattedMessage id={getLocaleKey('actual_tr_vol_prosen')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="actual_tr_intensity"
              label={<FormattedMessage id={getLocaleKey('actual_tr_intensity')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="actual_tr_load"
              label={<FormattedMessage id={getLocaleKey('actual_tr_load')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={6}>
          <Col span={6}>
            <Form.Item
              name="target_tr_vol"
              label={<FormattedMessage id={getLocaleKey('target_tr_vol')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="target_tr_vol_prosen"
              label={<FormattedMessage id={getLocaleKey('target_tr_vol_prosen')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="target_tr_intensity"
              label={<FormattedMessage id={getLocaleKey('target_tr_intensity')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="target_tr_load"
              label={<FormattedMessage id={getLocaleKey('target_tr_load')} />}
            >
              <Input disabled />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item name="notes" label={<FormattedMessage id={getLocaleKey('notes')} />}>
          <Input.TextArea />
        </Form.Item>

        <Form.Item>
          <Space>
            <Button type="primary" onClick={handleSave}>
              <FormattedMessage id="crud.save" />
            </Button>
            <Button type="primary" onClick={handleDelete} danger>
              <FormattedMessage id="crud.delete" />
            </Button>
            <Button onClick={handleCancel}>
              <FormattedMessage id="crud.cancel" />
            </Button>
            <Button
              disabled={weeksData ? false : true}
              icon={<CopyOutlined />}
              onClick={handleCopyOpen}
            >
              <FormattedMessage id="commonField.copy" />
            </Button>
            <Button
              disabled={printData ? false : true}
              icon={<PrinterOutlined />}
              onClick={handleOpenPrint}
            >
              Print
            </Button>
          </Space>
        </Form.Item>
      </Form>

      <CopyDialog
        visible={copyVisible}
        ypiData={ypiData}
        onOk={handleCopyOk}
        onCancel={handleCopyCancel}
      />
    </>
  );
};
