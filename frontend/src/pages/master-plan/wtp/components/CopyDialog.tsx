import { FC, useContext, useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { Modal, Form, Select, message, Button } from 'antd';
import { Ypi, DateSeasonPlanner, DetailWeeklyPlan } from '@/types/ypi';
import { getLocaleKey, Context } from '../index';
import { get } from '@/services/ypi/api';

const { Option } = Select;

interface FormType {
  ypi_id: number;
  week_index: number;
}

interface CopyDialogProps {
  visible: boolean;
  ypiData: Ypi[] | undefined;
  onCancel: () => void;
  onOk: (weekData: DateSeasonPlanner, ypi_id: number, week_index: number) => void;
}

const CopyDialog: FC<CopyDialogProps> = ({ visible, ypiData, onCancel, onOk }) => {
  const intl = useIntl();
  const [copyForm] = Form.useForm<FormType>();
  const { form } = useContext(Context);
  const [weeksData, setWeeksData] = useState<DateSeasonPlanner[]>();
  const [isSameDate, setIsSameDate] = useState<boolean>(false);

  const handleOk = async () => {
    const values = await copyForm.validateFields();
    const currentWeekData = weeksData?.find((d) => d.week === values.week_index);
    if (currentWeekData) onOk(currentWeekData, values.ypi_id, values.week_index);
  };

  const handleCancel = () => {
    copyForm.resetFields();
    onCancel();
  };

  const fetchWeekData = async (ypiId: number) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const selectedYpi = ypiData?.find((d) => d.id === ypiId);
      if (selectedYpi) {
        const weekDataRes = await get(
          `annual_plans/seasonPlanner?start=${selectedYpi.tanggal_mulai}&end=${selectedYpi.tanggal_selesai}`,
        );
        setWeeksData(weekDataRes.data);
      }
      hideLoading();
    } catch (error) {
      hideLoading();
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  const onValuesChange = (changedValues: FormType, values: FormType) => {
    if (changedValues.ypi_id) {
      fetchWeekData(changedValues.ypi_id);
    }

    if (values.ypi_id && values.week_index) {
      const masterYpiId = form?.getFieldValue('master_ypi_id');
      const weekIndex = form?.getFieldValue('minggu');

      if (values.ypi_id === masterYpiId && values.week_index === weekIndex) setIsSameDate(true);
      else setIsSameDate(false);
    }
  };

  const footer = [
    <Button key="cancel" onClick={handleCancel}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button disabled={isSameDate} key="ok" type="primary" onClick={handleOk}>
      <FormattedMessage id="crud.process" />
    </Button>,
  ];

  return (
    <Modal
      title={<FormattedMessage id="commonField.copy_to" />}
      centered
      visible={visible}
      closable={false}
      footer={footer}
      // onOk={undefined}
      // onCancel={handleCancel}
    >
      <Form form={copyForm} onValuesChange={onValuesChange}>
        <Form.Item
          label={<FormattedMessage id={getLocaleKey('ypi_detail')} />}
          name="ypi_id"
          rules={[{ required: true }]}
        >
          <Select>
            {ypiData?.map((d) => (
              <Option key={d.id} value={d.id}>
                {d.keterangan}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item
          rules={[{ required: true }]}
          name="week_index"
          label={<FormattedMessage id={getLocaleKey('weekly')} />}
        >
          <Select disabled={!weeksData ? true : false}>
            {weeksData?.map((d) => (
              <Option key={d.week} value={d.week}>
                {<FormattedMessage id={getLocaleKey('weekly')} />}: {d.week}{' '}
                {<FormattedMessage id={getLocaleKey('period')} />}: {d.monday}/{d.month_number_str}/
                {d.year} {<FormattedMessage id="commonField.to" />} {d.sunday}/{d.month_number_str}/
                {d.year}
              </Option>
            ))}
          </Select>
        </Form.Item>
      </Form>
    </Modal>
  );
};

export default CopyDialog;
