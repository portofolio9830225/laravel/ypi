import { DetailWeeklyPlan, HeaderWeeklyPlan } from '@/types/ypi';
import {
  DatePicker,
  Space,
  Tooltip,
  Table,
  Form,
  FormInstance,
  InputNumber,
  Input,
  Button,
  TimePicker,
} from 'antd';
import { FC, useContext, useEffect, useState } from 'react';
import { useIntl } from 'umi';
import { getLocaleKey, Context } from '../index';
import { PlusOutlined, MinusOutlined } from '@ant-design/icons';
import moment, { Moment } from 'moment';

interface CustomRowProps {
  index: number;
}

enum InputType {
  Date,
  Time,
  Text,
  Number,
  None,
}

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  return <tr {...props} />;
};

interface CellProps {
  children: React.ReactNode;
  dataIndex: keyof DetailWeeklyPlan;
  inputType: InputType;
  fieldIndex: number;
}

const CustomCell: FC<CellProps> = ({ children, dataIndex, fieldIndex, inputType, ...props }) => {
  const intl = useIntl();
  let childNode = children;
  const { form } = useContext(Context);
  const startDate = Form.useWatch('tanggal_awal', form);
  const endDate = Form.useWatch('tanggal_akhir', form);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const disabledDate = (currentDate: Moment): boolean => {
    // const start = form?.getFieldValue(['tanggal_awal']);
    // const end = form?.getFieldValue(['tanggal_akhir']);
    return !currentDate.isBetween(
      moment(startDate, 'DD-MM-YYYY').subtract(1, 'd'),
      moment(endDate, 'DD-MM-YYYY').add(1, 'd'),
    );
  };

  const getDefaultPickerValue = (): Moment => {
    return moment(startDate, 'DD-MM-YYYY');
  };

  if (inputType !== InputType.None && inputType !== undefined) {
    let inputNode = <></>;
    let style = { margin: 0 };
    let rules = undefined;

    if (inputType === InputType.Text) inputNode = <Input style={{ width: '100%' }} />;
    if (inputType === InputType.Date)
      inputNode = (
        <DatePicker
          defaultPickerValue={getDefaultPickerValue()}
          disabledDate={disabledDate}
          style={{ width: '100%' }}
        />
      );
    if (inputType === InputType.Number)
      inputNode = <InputNumber style={{ width: '100%' }} type="number" controls={false} />;
    if (inputType === InputType.Time) inputNode = <TimePicker style={{ width: '100%' }} />;

    if (dataIndex === 'tanggal_moment') rules = getRequiredRule('date');
    if (dataIndex === 'session') rules = getRequiredRule('session');
    if (dataIndex === 'jam_moment') rules = getRequiredRule('time');

    childNode = (
      <Form.Item rules={rules} style={style} name={['detailsWtp', fieldIndex, dataIndex]}>
        {inputNode}
      </Form.Item>
    );
    return <td {...props}>{childNode}</td>;
  }

  return <td {...props}>{childNode}</td>;
};

interface Props {
  form: FormInstance<HeaderWeeklyPlan>;
}

const DetailWtp: FC<Props> = () => {
  const { form } = useContext(Context);
  const intl = useIntl();
  const weekIndex = Form.useWatch('minggu', form);
  const [data, setData] = useState<Partial<DetailWeeklyPlan>[]>([]);
  const detailsWtp = Form.useWatch(['detailsWtp'], form);

  useEffect(() => {
    if (detailsWtp) setData(detailsWtp.map((d, i) => ({ ...d, key: i.toString() })));
  }, [detailsWtp]);

  const columns = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      inputType: InputType.None,
    },
    {
      title: intl.formatMessage({ id: 'commonField.date' }),
      dataIndex: 'tanggal_moment',
      inputType: InputType.Date,
      width: 150,
    },
    {
      title: intl.formatMessage({ id: 'commonField.session' }),
      dataIndex: 'session',
      inputType: InputType.Number,
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'commonField.time' }),
      dataIndex: 'jam_moment',
      inputType: InputType.Time,
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('training_desc') }),
      dataIndex: 'details',
      inputType: InputType.Text,
      width: 300,
    },
    {
      title: intl.formatMessage({ id: 'commonField.set' }),
      dataIndex: 'set',
      inputType: InputType.Number,
      width: 75,
    },
    {
      title: intl.formatMessage({ id: 'commonField.rep' }),
      dataIndex: 'rep',
      inputType: InputType.Number,
      width: 75,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('pace') }),
      dataIndex: 'pace',
      inputType: InputType.Text,
      width: 100,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('work_time') }),
      dataIndex: 'work_time',
      inputType: InputType.Number,
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('rest_interval') }),
      dataIndex: 'rest_interval',
      inputType: InputType.Number,
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('rest_period') }),
      dataIndex: 'rest_period',
      inputType: InputType.Number,
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('training_outcome') }),
      dataIndex: 'emphesis_goal',
      inputType: InputType.Text,
      width: 300,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('duration') }),
      dataIndex: 'duration',
      inputType: InputType.None,
      width: 75,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('intensity') }) + ' (1-5)',
      dataIndex: 'intensity',
      inputType: InputType.None,
      width: 75,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('tr_load') }),
      dataIndex: 'tr_load',
      inputType: InputType.None,
      width: 75,
    },
  ];

  const processedColumns = columns.map((col: any) => {
    const newCol = {
      ...col,
      onCell: (record: DetailWeeklyPlan) => ({
        // record,
        dataIndex: col.dataIndex,
        inputType: col.inputType,
        fieldIndex: record.fieldIndex,
      }),
    };
    return newCol;
  });

  const handleAddDetailWtp = () => {
    // if (tableData) {
    let newData: Partial<DetailWeeklyPlan>[] = form?.getFieldValue(['detailsWtp']);

    newData.push({
      key: newData.length.toString(),
      fieldIndex: newData.length,
      no: newData.length + 1,
    });
    form?.setFieldsValue({ detailsWtp: newData });
    // setTableData(newData);
    // }
  };

  const handleDeleteDetailWtp = () => {
    let currentData: Partial<DetailWeeklyPlan>[] = form?.getFieldValue(['detailsWtp']);
    let deletedData: Partial<DetailWeeklyPlan>[] = form?.getFieldValue(['deletedDetailsWtp']);
    const deleted = currentData.pop();
    if (deleted && deleted.id) deletedData.push(deleted);

    console.log('new detail', currentData);
    form?.setFieldsValue({ detailsWtp: currentData, deletedDetailsWtp: deletedData });
  };

  const footer = () => {
    return (
      <Space>
        <Tooltip title={intl.formatMessage({ id: 'crud.addNewData' })}>
          <Button
            onClick={handleAddDetailWtp}
            type="primary"
            size="small"
            shape="circle"
            icon={<PlusOutlined />}
          />
        </Tooltip>
        {detailsWtp && detailsWtp?.length > 1 && (
          <Tooltip title={intl.formatMessage({ id: 'crud.delete' })}>
            <Button
              onClick={handleDeleteDetailWtp}
              type="primary"
              size="small"
              danger
              shape="circle"
              icon={<MinusOutlined />}
            />
          </Tooltip>
        )}
      </Space>
    );
  };

  const components = {
    body: {
      cell: CustomCell,
      row: CustomRow,
    },
  };

  return (
    <Table
      size="small"
      components={components}
      bordered
      dataSource={weekIndex ? data : []}
      columns={processedColumns}
      scroll={{ x: 'max-content' }}
      footer={weekIndex ? footer : undefined}
    />
  );
};

export default DetailWtp;
