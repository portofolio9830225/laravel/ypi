import {
  Drawer,
  Row,
  Col,
  Table,
  TableColumnsType,
  Space,
  Button,
  message,
  Descriptions,
} from 'antd';
import styles from './PrintModal.less';
import { getLocaleKey, Context } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { DetailWeeklyPlan } from '@/types/ypi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { jsPDF } from 'jspdf';
import FooterReport from '@/components/FooterReport';

const Header = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <Space>
        <span className={styles.title}>Weekly</span>
        <div style={{ width: '0.5rem' }} />
        <span className={styles.title}>Training</span>
        <div style={{ width: '0.5rem' }} />
        <span className={styles.title}>Programme</span>
      </Space>
      <br />
      <Descriptions bordered size="small" column={6}>
        <Descriptions.Item label={<FormattedMessage id="commonField.week" />}>
          {printData?.minggu}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('phase')} />}>
          {printData?.relatedData?.periodization}
        </Descriptions.Item>
        <Descriptions.Item span={3} label={<FormattedMessage id={getLocaleKey('weekly_goal')} />}>
          {printData?.weekly_training_goal}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('period')} />}>
          {printData?.relatedData?.tanggal_awal} <FormattedMessage id="commonField.to" />{' '}
          {printData?.relatedData?.tanggal_akhir}
        </Descriptions.Item>

        <Descriptions.Item span={3} label={<FormattedMessage id={getLocaleKey('tactic')} />}>
          {printData?.relatedData?.tactic}
        </Descriptions.Item>
        <Descriptions.Item span={3} label={<FormattedMessage id={getLocaleKey('physical')} />}>
          {printData?.relatedData?.physical}
        </Descriptions.Item>
        <Descriptions.Item span={3} label={<FormattedMessage id={getLocaleKey('skills')} />}>
          {printData?.relatedData?.technical_development}
        </Descriptions.Item>
        <Descriptions.Item span={3} label={<FormattedMessage id={getLocaleKey('mental')} />}>
          {printData?.relatedData?.mental}
        </Descriptions.Item>

        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('session_per_week')} />}>
          {printData?.session_per_week}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('specificity')} />}>
          {printData?.relatedData?.specificity}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('volume')} />}>
          {printData?.relatedData?.volume}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('intensity')} />}>
          {printData?.relatedData?.intensity}
        </Descriptions.Item>
        <Descriptions.Item label="YPI">{printData?.ypi_name}</Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('coach_name')} />}>
          {printData?.coach_name}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

const TableDetail = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [data, setData] = useState<DetailWeeklyPlan[]>([]);

  useEffect(() => {
    if (printData && printData.detailsWtp) {
      console.log(printData);
      setData(
        (printData.detailsWtp as DetailWeeklyPlan[]).map((d, i) => ({
          ...d,
          tanggal: d.tanggal.substring(0, 10),
          key: i.toString(),
        })),
      );
    }
  }, [printData]);

  const columns: TableColumnsType<DetailWeeklyPlan> = [
    {
      title: intl.formatMessage({ id: 'commonField.date' }),
      dataIndex: 'tanggal',
      width: 250,
    },
    {
      title: intl.formatMessage({ id: 'commonField.session' }),
      dataIndex: 'session',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: 'commonField.time' }),
      dataIndex: 'jam',
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('training_desc') }),
      dataIndex: 'details',
      width: 300,
    },
    {
      title: intl.formatMessage({ id: 'commonField.set' }),
      dataIndex: 'set',
      width: 75,
    },
    {
      title: intl.formatMessage({ id: 'commonField.rep' }),
      dataIndex: 'rep',
      width: 75,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('pace') }),
      dataIndex: 'pace',
      width: 100,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('work_time') }),
      dataIndex: 'work_time',
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('rest_interval') }),
      dataIndex: 'rest_interval',
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('rest_period') }),
      dataIndex: 'rest_period',
      width: 110,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('training_outcome') }),
      dataIndex: 'emphesis_goal',
      width: 300,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('duration') }),
      dataIndex: 'duration',
      width: 75,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('intensity') }) + ' (1-5)',
      dataIndex: 'intensity',
      width: 75,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('tr_load') }),
      dataIndex: 'tr_load',
      width: 75,
    },
  ];

  return (
    <Table
      bordered
      // rowClassName={rowClassName}
      columns={columns}
      dataSource={data}
      size="small"
      pagination={false}
    />
  );
};

const Footer = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <Descriptions size="small" column={4} bordered>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('actual_tr_vol')} />}>
          {printData?.relatedData?.actual_tr_vol}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('actual_tr_vol_prosen')} />}>
          {printData?.relatedData?.actual_tr_vol_prosen}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('actual_tr_intensity')} />}>
          {printData?.relatedData?.actual_tr_intensity}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('actual_tr_load')} />}>
          {printData?.relatedData?.actual_tr_load}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('target_tr_vol')} />}>
          {printData?.relatedData?.target_tr_vol}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('target_tr_vol_prosen')} />}>
          {printData?.relatedData?.target_tr_vol_prosen}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('target_tr_intensity')} />}>
          {printData?.relatedData?.target_tr_intensity}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('target_tr_load')} />}>
          {printData?.relatedData?.target_tr_load}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id="commonField.note" />}>
          {printData?.notes}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default () => {
  const intl = useIntl();
  const { printVisible, setPrintVisible } = useContext(Context);

  const handleClose = () => {
    setPrintVisible(false);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('weekly_training_programme.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1500,
      autoPaging: 'text',
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );
  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={printVisible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <br />
          <TableDetail />
          <br />
          <Footer />
          <FooterReport />
        </div>
      </Drawer>
    </>
  );
};
