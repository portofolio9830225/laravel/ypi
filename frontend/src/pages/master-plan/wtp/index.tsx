import { Card, Form, FormInstance, message } from 'antd';
import { createContext, useState, useEffect } from 'react';
import WtpForm from './components/Form';
import { Coach, DetailWeeklyPlan, HeaderWeeklyPlan, Ypi } from '@/types/ypi';
import PrintModal from './components/PrintModal';
import { get } from '@/services/ypi/api';
import { useIntl } from 'umi';

export const getLocaleKey = (fieldName: string) => {
  return 'pages.wtp.' + fieldName;
};

interface RelatedData {
  tanggal_awal: string;
  tanggal_akhir: string;
  periodization: string;
  technical_development: string;
  tactic: string;
  physical: string;
  mental: string;
  specificity: number;
  volume: number;
  intensity: number;
  target_tr_vol: number;
  target_tr_vol_prosen: number;
  target_tr_intensity: number;
  target_tr_load: number;
  actual_tr_vol: number;
  actual_tr_load: number;
  actual_tr_intensity: number;
  actual_tr_vol_prosen: number;
}

export interface Wtp extends HeaderWeeklyPlan {
  detailsWtp: Partial<DetailWeeklyPlan>[];
  relatedData: Partial<RelatedData>;
  ypi_name: string;
  coach_name: string;
  session_per_week: number;
}

interface YpiExtra extends Ypi {
  coach: Coach;
}

export interface ContextType {
  form: FormInstance<HeaderWeeklyPlan> | undefined;
  printVisible: boolean;
  setPrintVisible: React.Dispatch<React.SetStateAction<boolean>>;
  printData: Partial<Wtp> | undefined;
  setPrintData: React.Dispatch<React.SetStateAction<Partial<Wtp> | undefined>>;
  ypiData: YpiExtra[];
}

export const Context = createContext<ContextType>({
  form: undefined,
  printVisible: false,
  setPrintVisible: () => {},
  printData: undefined,
  setPrintData: () => {},
  ypiData: [],
});

export default () => {
  const intl = useIntl();
  const [form] = Form.useForm<HeaderWeeklyPlan>();
  const [printVisible, setPrintVisible] = useState<boolean>(false);
  const [printData, setPrintData] = useState<Partial<Wtp>>();
  const [ypiData, setYpiData] = useState<YpiExtra[]>([]);

  const fetchDependenciesData = async () => {
    try {
      const ypiRes = await get('master_ypis?with_coach=true');
      setYpiData(ypiRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const contextValue: ContextType = {
    form,
    printVisible,
    setPrintVisible,
    printData,
    setPrintData,
    ypiData,
  };

  return (
    <Context.Provider value={contextValue}>
      <Card>
        <WtpForm />
      </Card>
      <PrintModal />
    </Context.Provider>
  );
};
