import { Table, TableColumnsType, TableColumnType, message } from 'antd';
import { useContext, useState, useEffect, createContext, useMemo, useCallback } from 'react';
import { Context, YpiExtra } from '../index';
import { Access, useIntl } from 'umi';
import { DateSeasonPlanner, YpiSeasonPlanner, CompetitionDetail, YtpRecordType } from '@/types/ypi';
import { get } from '@/services/ypi/api';
import moment from 'moment';
import DetailForm from './DetailForm';
import WeekRangeForm from './WeekRangeForm';
import { create } from '@/services/ypi/api';
import styles from './ytp.less';
import { Line } from '@ant-design/plots';
import FooterReport from '@/components/FooterReport';

interface CustomRowProps {
  index: number;
}

interface CustomCellProps {
  record: YtpRecordType;
  children: React.ReactNode;
  week: number;
  col_span: number;
  setEditedSeasonPlanner: React.Dispatch<React.SetStateAction<YpiSeasonPlanner | undefined>>;
  editedSeasonPlanner: YpiSeasonPlanner | undefined;
}

interface HelpDescription {
  value: string;
  fieldIndex: number;
}

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  return <tr {...props} />;
};

const CustomCell: React.FC<CustomCellProps> = ({
  record,
  children,
  week,
  col_span,
  setEditedSeasonPlanner,
  editedSeasonPlanner,
  ...props
}) => {
  const { eventData } = useContext(Context);

  if (record && record.isEventData) {
    const event = eventData.find((d) => d.nama_event == record['week_' + week]);

    return (
      <td {...props} style={{ background: event?.color }}>
        {children}
      </td>
    );
  }

  if (record && record.isEditable) {
    const { setActiveSpId } = useContext(YtpContext);

    const handleClick = () => {
      setActiveSpId(record.seasonPlanId);
    };

    return (
      <td
        onClick={() => {
          handleClick();
        }}
        {...props}
      >
        {children}
      </td>
    );
  }

  return <td {...props}>{children}</td>;
};

interface ContextType {
  ytpData: Partial<YtpRecordType>[];
  setYtpData: React.Dispatch<React.SetStateAction<Partial<YtpRecordType>[]>>;
  handleSave: (data: Partial<YtpRecordType>[]) => Promise<void>;
  descriptionData: HelpDescription | undefined;
  setDescriptionData: React.Dispatch<React.SetStateAction<HelpDescription | undefined>>;
  activeSpId: number | undefined;
  setActiveSpId: React.Dispatch<React.SetStateAction<number | undefined>>;
}

export const YtpContext = createContext<ContextType>({
  activeSpId: undefined,
  setActiveSpId: () => {},
  ytpData: [],
  setYtpData: () => {},
  handleSave: async () => {},
  descriptionData: undefined,
  setDescriptionData: () => {},
});

export default () => {
  const intl = useIntl();
  const [ytpData, setYtpData] = useState<Partial<YtpRecordType>[]>([]);
  const [descriptionData, setDescriptionData] = useState<HelpDescription>();
  const [activeSpId, setActiveSpId] = useState<number>();
  const { selectedYpiId, setEventData, setSelectedYpi, constantValues, printMode } =
    useContext(Context);

  const dateName: Partial<YtpRecordType>[] = useMemo(() => {
    return [
      {
        key: '0',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.year' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '1',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.month' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '2',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.monday' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '3',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.sunday' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '4',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.week' }),
        color: constantValues?.YTP_DATE_COLOR,
      },
      {
        key: '5',
        seasonPlanner: intl.formatMessage({ id: 'commonField.season_planner' }),
        seasonPlannerDetail: intl.formatMessage({ id: 'commonField.event' }),
        isEventData: true,
      },
    ];
  }, [constantValues]);

  const fetchProcessData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const ypiRes = await get(`master_ypis/${selectedYpiId}?full_data=true`);
    let ypi: YpiExtra = ypiRes.data;
    ypi = {
      ...ypi,
      tanggal_mulai: ypi.tanggal_mulai.substring(0, 10),
      tanggal_selesai: ypi.tanggal_selesai.substring(0, 10),
    };
    setSelectedYpi(ypi);
    const startYpiDate = ypi.tanggal_mulai;
    const endYpiDate = moment(ypi.tanggal_mulai).add(1, 'years').format('YYYY-MM-DD');
    const seasonPlannerRes = await get(
      `annual_plans/seasonPlanner?start=${startYpiDate}&end=${endYpiDate}`,
    );
    const dateSeasonPlanner: DateSeasonPlanner[] = seasonPlannerRes.data || [];
    const eventRes = await get(
      `master_achievements?start=${ypi.tanggal_mulai}&end=${ypi.tanggal_selesai}`,
    );
    const eventData: CompetitionDetail[] = eventRes.data || [];
    setEventData(eventData);

    let dateData = [...dateName];

    let yearData = { ...dateData[0] };
    let monthData = { ...dateData[1] };
    let mondayData = { ...dateData[2] };
    let saturdayData = { ...dateData[3] };
    let weekData = { ...dateData[4] };
    let eventYtpData = { ...dateData[5] };
    for (let i = 0; i < dateSeasonPlanner.length; i++) {
      const val = dateSeasonPlanner[i];
      yearData['week_' + (i + 1)] = val.year;
      monthData['week_' + (i + 1)] = val.month;
      mondayData['week_' + (i + 1)] = val.monday;
      saturdayData['week_' + (i + 1)] = val.sunday;
      weekData['week_' + (i + 1)] = val.week;
      eventYtpData['week_' + (i + 1)] = '';
    }

    const startPeriod = moment(startYpiDate);

    ypi.ypi_events?.forEach((evt) => {
      if (!evt.active) return;

      const event = eventData.find((dd) => dd.id === evt.master_achievement_id);
      if (!event) return;

      const startEvent = moment(event.tanggal_mulai);
      const endEvent = moment(event.tanggal_akhir);
      let startWeekEvent = startEvent.diff(startPeriod, 'weeks');
      let endWeekEvent = endEvent.diff(startPeriod, 'weeks');

      if (startWeekEvent < 0) startWeekEvent = 0;

      if (endWeekEvent > dateSeasonPlanner.length) endWeekEvent = dateSeasonPlanner.length;

      for (let i = startWeekEvent; i <= endWeekEvent; i++) {
        eventYtpData['week_' + (i + 1)] = event.nama_event;
      }
    });

    dateData[0] = yearData;
    dateData[1] = monthData;
    dateData[2] = mondayData;
    dateData[3] = saturdayData;
    dateData[4] = weekData;
    dateData[5] = eventYtpData;

    dateData = dateData.map((d) => {
      return { ...d, isDateData: true };
    });

    setYtpData((prevData) => {
      const sortedSpv = [...ypi.season_plan_values].sort(
        (a, b) => (a.season_plan_id as number) - (b.season_plan_id as number),
      );
      const activeSpv = sortedSpv.filter((d) => d.active === true);
      let spData: Partial<YtpRecordType>[] = activeSpv.map((d) => {
        if (d.season_plan_id === constantValues?.SP_PERIODIZATION_ID) {
          return {
            ...d,
            color: d.bg_color,
            seasonPlanner: d.seasonPlan,
            seasonPlannerDetail: d.seasonPlanDetail,
            isEventData: false,
            isDateData: false,
            seasonPlanId: d.season_plan_id,
            seasonPlanDetailId: d.season_plan_detail_id,
            isEditable: true,
            isPeriodization: true,
          };
        }
        return {
          ...d,
          color: d.bg_color,
          seasonPlanner: d.seasonPlan,
          seasonPlannerDetail: d.seasonPlanDetail,
          isEventData: false,
          isDateData: false,
          isPeriodization: false,
          seasonPlanId: d.season_plan_id,
          seasonPlanDetailId: d.season_plan_detail_id,
          isEditable: true,
        };
      });
      let combinedData = [...dateData, ...spData];
      combinedData = combinedData.map((d, i) => {
        return {
          ...d,
          key: i.toString(),
        };
      });
      return combinedData;
    });
    hideLoading();
  };

  useEffect(() => {
    if (selectedYpiId) fetchProcessData();
  }, [selectedYpiId]);

  const handleSave = async (data: Partial<YtpRecordType>[]) => {
    // return;
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      let tableData = data.filter((d) => d.isDateData === false);
      tableData = tableData.map((d) => {
        return {
          ...d,
          ypi_id: selectedYpiId,
          season_plan_id: d.seasonPlanId,
          season_plan_detail_id: d.seasonPlanDetailId,
          bg_color: d.color,
        };
      });
      await create('season_plan_values?batch=true', tableData);
      await fetchProcessData();
      setActiveSpId(undefined);
      message.success(intl.formatMessage({ id: 'successSave' }, { moduleName: 'Data' }));
    } catch (error) {
      message.error(intl.formatMessage({ id: 'errorSave' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const ytpChart = useCallback(() => {
    const detailData = [];
    const academyIndexData = ytpData?.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_ACADEMY_INDEX_ID,
    );
    if (academyIndexData) {
      for (const key in academyIndexData) {
        if (key.startsWith('week_')) {
          detailData.push({
            name: academyIndexData.seasonPlannerDetail,
            value: academyIndexData[key] !== '' ? parseInt(academyIndexData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }
    const sportIndexData = ytpData?.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_SPORT_INDEX_ID,
    );
    if (sportIndexData) {
      for (const key in sportIndexData) {
        if (key.startsWith('week_')) {
          detailData.push({
            name: sportIndexData.seasonPlannerDetail,
            value: sportIndexData[key] !== '' ? parseInt(sportIndexData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }

    const workoutData = [];
    const intensityData = ytpData?.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_INTENSITY_ID,
    );
    if (intensityData) {
      for (const key in intensityData) {
        if (key.startsWith('week_')) {
          workoutData.push({
            name: intensityData.seasonPlannerDetail,
            value: intensityData[key] !== '' ? parseInt(intensityData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }
    const volumeData = ytpData?.find((d) => d.seasonPlanDetailId === constantValues?.SPD_VOLUME_ID);
    if (volumeData) {
      for (const key in volumeData) {
        if (key.startsWith('week_')) {
          workoutData.push({
            name: volumeData.seasonPlannerDetail,
            value: volumeData[key] !== '' ? parseInt(volumeData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }
    const spesificData = ytpData?.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_SPESIFIC_ID,
    );
    if (spesificData) {
      for (const key in spesificData) {
        if (key.startsWith('week_')) {
          workoutData.push({
            name: spesificData.seasonPlannerDetail,
            value: spesificData[key] !== '' ? parseInt(spesificData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }

    const config1 = {
      data: detailData,
      xField: 'week',
      yField: 'value',
      seriesField: 'name',
      height: 200,
      color: ['#9e1fe2', '#ff0000'],
      point: {
        size: 4,
        shape: 'square',
        style: {
          lineWidth: 2,
        },
      },
      theme: {
        styleSheet: {
          backgroundColor: '#ffffff',
        },
      },
    };

    const config2 = {
      data: workoutData,
      xField: 'week',
      yField: 'value',
      seriesField: 'name',
      height: 200,
      color: ['#00ff00', '#0000ff', '#ff0000'],
      theme: {
        styleSheet: {
          backgroundColor: '#ffffff',
        },
      },
    };

    return (
      <>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}></Table.Summary.Cell>
          <Table.Summary.Cell index={2} colSpan={53}>
            <Line {...config1} />
          </Table.Summary.Cell>
        </Table.Summary.Row>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}></Table.Summary.Cell>
          <Table.Summary.Cell index={2} colSpan={53}>
            <Line {...config2} />
          </Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  }, [ytpData]);

  let weekColumns = [];
  let value: string[] = [];
  let index: number = 0;
  for (let i = 1; i <= 53; i++) {
    weekColumns.push({
      colSpan: 0,
      rowSpan: 0,
      dataIndex: 'week_' + i,
      onCell: (val: YtpRecordType, indexRow: number) => {
        const currentValue = val['week_' + i];
        const nextValue = val['week_' + (i + 1)];
        let props = {};
        let style = {};

        if (currentValue !== '') {
          if (index !== indexRow) {
            index = indexRow;
            value = [];
          }

          value.push(currentValue);
          if (currentValue == nextValue) {
            props = { colSpan: 0, col_span: 0 };
          } else {
            props = { colSpan: value.length, col_span: value.length };
            value = [];
          }
        }
        if (val.isEditable || val.isEventData) props = { ...props, record: val };
        if (val.isEditable) style = { cursor: 'pointer' };

        style = { ...style, textAlign: 'center' };
        return {
          ...props,
          style,
          week: i,
        };
      },
    });
  }

  const columns: TableColumnsType<Partial<YtpRecordType>> = [
    {
      title: 'season_planner',
      fixed: printMode ? false : 'left',
      dataIndex: 'seasonPlanner',
      // width: '6rem',
      colSpan: 0,
      onCell: (val, index) => {
        let props = {};
        if (index != undefined) {
          let prevValue;
          if (ytpData && ytpData[index - 1]) {
            prevValue = ytpData[index - 1].seasonPlanner;
          }
          const currentValue = ytpData ? ytpData[index].seasonPlanner : undefined;

          let rowSpan = 1;
          if (currentValue != prevValue && ytpData) {
            for (let i = index; i < ytpData.length; i++) {
              let nextValue = undefined;
              if (ytpData[i + 1]) nextValue = ytpData[i + 1].seasonPlanner;
              if (ytpData[i].seasonPlanner != nextValue) {
                break;
              } else {
                rowSpan++;
              }
            }
          }
          if (currentValue == prevValue) {
            rowSpan = 0;
          }

          props = { rowSpan };
        }

        // if (val.isEditable) props = { ...props, style: { background: val.color } };
        props = { ...props, style: { background: constantValues?.YTP_SEASON_PLAN_COLOR } };
        return props;
      },
    },
    {
      colSpan: 0,
      rowSpan: 0,
      fixed: printMode ? false : 'left',
      // width: '8rem',
      dataIndex: 'seasonPlannerDetail',
      // ellipsis: true,
      onCell: (record: Partial<YtpRecordType>) => {
        return { style: { background: record.color } };
      },
    },
    ...(weekColumns as TableColumnType<Partial<YtpRecordType>>[]),
  ];

  const components = {
    body: {
      row: CustomRow,
      cell: CustomCell,
    },
  };

  const contextValue: ContextType = {
    activeSpId,
    setActiveSpId,
    ytpData,
    setYtpData,
    handleSave,
    descriptionData,
    setDescriptionData,
  };

  return (
    <YtpContext.Provider value={contextValue}>
      <Access accessible={ytpData.length > 0}>
        <Table
          onRow={(record: Partial<YtpRecordType>, rowIndex) => {
            return {
              style: { background: record.color },
            };
          }}
          components={components}
          bordered
          size="small"
          columns={columns}
          scroll={printMode ? undefined : { x: true }}
          dataSource={ytpData}
          pagination={false}
          rowClassName={styles.cell}
          className={styles.table}
          summary={ytpChart}
        />
        {printMode && <FooterReport />}
      </Access>
      <DetailForm />
      <WeekRangeForm />
    </YtpContext.Provider>
  );
};
