import { YtpRecordType, YpiSeasonPlanner, YpiSeasonPlannerDetail } from '@/types/ypi';
import {
  Drawer,
  Space,
  Form,
  Row,
  InputNumber,
  Input,
  Button,
  Col,
  FormInstance,
  message,
  Popconfirm,
} from 'antd';
import { useState, FC, useEffect, useContext } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { getLocaleKey, Context } from '../index';
import { QuestionOutlined, CheckOutlined, DeleteOutlined } from '@ant-design/icons';
import DescriptionModal from '@/components/DescriptionModal';
import { create } from '@/services/ypi/api';

interface Item {
  season_planner_detail_description: string;
  start_week: number;
  end_week: number;
  season_planner_detail_id: number;
  value: string | number;
}
interface FormType {
  values: Item[];
}

interface FormItemProp {
  data: YpiSeasonPlannerDetail;
  index: number;
  form: FormInstance<FormType>;
  setDescriptionData: React.Dispatch<React.SetStateAction<DescriptionData | undefined>>;
  handleItemSubmit: (key: number, seasonPlannerDetailId: number) => Promise<void>;
  handleItemDelete: (key: number) => void;
}

const FormItem: FC<FormItemProp> = ({
  index,
  form,
  handleItemSubmit,
  setDescriptionData,
  handleItemDelete,
  data,
}) => {
  const intl = useIntl();
  const fieldPrefixName = 'values';
  const { editedCell } = useContext(Context);

  const startWeek = Form.useWatch([fieldPrefixName, index, 'start_week'], form);
  const endWeek = Form.useWatch([fieldPrefixName, index, 'end_week'], form);

  const updateFields = () => {
    let formValues: Partial<Item>[] = form.getFieldValue(fieldPrefixName);

    if (!formValues) {
      formValues = [];
    }
    if (!formValues[index]) {
      formValues[index] = {};
    }

    formValues[index].season_planner_detail_id = data.id;
    formValues[index].season_planner_detail_description = data.description;
    if (formValues[index].season_planner_detail_id === editedCell?.seasonPlannerDetailId) {
      formValues[index].value = editedCell?.content;
      formValues[index].start_week = editedCell?.startWeek;
      formValues[index].end_week = editedCell?.endWeek;
    } else {
      formValues[index].value = undefined;
      formValues[index].start_week = undefined;
      formValues[index].end_week = undefined;
    }
    form.setFieldsValue({ values: formValues });
  };

  useEffect(() => {
    updateFields();
  }, [data]);

  useEffect(() => {
    // form.resetFields();
    if (editedCell) updateFields();
    // else form.resetFields();
  }, [editedCell]);

  return (
    <Row justify="center" gutter={6}>
      <Form.Item name={[fieldPrefixName, index, 'season_planner_detail_id']} hidden>
        <Input />
      </Form.Item>
      <Form.Item name={[fieldPrefixName, index, 'season_planner_detail_description']} hidden />
      <Col span={1}>
        <Form.Item>
          <Button
            onClick={() =>
              setDescriptionData({
                visible: true,
                index,
                value: form.getFieldValue([
                  fieldPrefixName,
                  index,
                  'season_planner_detail_description',
                ]),
              })
            }
            size="small"
            type="primary"
            shape="circle"
            icon={<QuestionOutlined />}
          />
        </Form.Item>
      </Col>
      <Col span={7}>
        <Form.Item name={[fieldPrefixName, index, 'value']} label={data.name}>
          {data.type === 'number' ? (
            <InputNumber style={{ width: '100%' }} type="number" />
          ) : (
            <Input />
          )}
        </Form.Item>
      </Col>
      <Col span={7}>
        <Form.Item
          name={[fieldPrefixName, index, 'start_week']}
          label={<FormattedMessage id={getLocaleKey('week')} />}
          rules={[
            {
              type: 'number',
              max: endWeek,
              message: intl.formatMessage({ id: 'message.must_less' }, { variable: 'End Week' }),
            },
            {
              type: 'number',
              min: 1,
              message: intl.formatMessage({ id: 'message.must_greater' }, { variable: '0' }),
            },
          ]}
        >
          <InputNumber type="number" style={{ width: '100%' }} />
        </Form.Item>
      </Col>
      <Col span={7}>
        <Form.Item
          name={[fieldPrefixName, index, 'end_week']}
          label={<FormattedMessage id={getLocaleKey('to_week')} />}
          rules={[
            {
              type: 'number',
              min: startWeek,
              message: intl.formatMessage(
                { id: 'message.must_greater' },
                { variable: 'Start Week' },
              ),
            },
            {
              type: 'number',
              min: 1,
              message: intl.formatMessage({ id: 'message.must_greater' }, { variable: '0' }),
            },
            {
              type: 'number',
              required: startWeek > 0 ? true : false,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: 'End Week' },
              ),
            },
          ]}
        >
          <InputNumber type="number" style={{ width: '100%' }} />
        </Form.Item>
      </Col>
      <Col span={2}>
        <Space>
          <Form.Item>
            <Button
              onClick={() => handleItemSubmit(index, data.id)}
              size="small"
              type="primary"
              shape="circle"
              icon={<CheckOutlined />}
            />
          </Form.Item>
          <Form.Item>
            <Button
              onClick={() => handleItemDelete(index)}
              danger
              size="small"
              type="primary"
              shape="circle"
              icon={<DeleteOutlined />}
            />
          </Form.Item>
        </Space>
      </Col>
    </Row>
  );
};

interface Props {
  editedSeasonPlanner: YpiSeasonPlanner | undefined;
  setEditedSeasonPlanner: React.Dispatch<React.SetStateAction<YpiSeasonPlanner | undefined>>;
  fetchProcessData: () => Promise<void>;
  data: Partial<YtpRecordType>[];
  setData: React.Dispatch<React.SetStateAction<Partial<YtpRecordType>[]>>;
  tempData: Partial<YtpRecordType>[] | undefined;
  setTempData: React.Dispatch<React.SetStateAction<Partial<YtpRecordType>[] | undefined>>;
}

interface DescriptionData {
  index: number;
  value: string;
  visible: boolean;
}

const YtpForm: FC<Props> = ({
  editedSeasonPlanner,
  setEditedSeasonPlanner,
  fetchProcessData,
  data,
  setData,
  tempData,
  setTempData,
}) => {
  const intl = useIntl();
  const [form] = Form.useForm<FormType>();
  const [descriptionData, setDescriptionData] = useState<DescriptionData>();
  const [visible, setVisible] = useState<boolean>(false);
  const [items, setItems] = useState<Item[]>([]);
  const { getTableData } = useContext(Context);

  useEffect(() => {
    setVisible(editedSeasonPlanner ? true : false);
  }, [editedSeasonPlanner]);

  // useEffect(() => {
  //   console.log(items);
  // }, [items]);

  const handleCancel = () => {
    setEditedSeasonPlanner(undefined);
    if (tempData) {
      setData([...tempData]);
      setTempData(undefined);
    }
  };

  const handleOk = async () => {
    // return;
    try {
      let tableData = data.filter((d) => d.seasonPlannerDetailId);
      tableData = tableData.map((d) => {
        return { ...d, masterYpiId: editedSeasonPlanner?.master_ypi_id };
      });
      console.log('data', tableData);
      await create('season_planner_detail_values', tableData);
      await getTableData();
      await fetchProcessData();
      message.success(intl.formatMessage({ id: 'successSave' }, { moduleName: 'Data' }));
      setEditedSeasonPlanner(undefined);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'errorSave' }, { fieldName: 'Data' }));
    }
  };

  const Extra = () => (
    <Space>
      <Button type="primary" htmlType="button" onClick={handleOk}>
        {intl.formatMessage({ id: 'crud.save' })}
      </Button>

      <Popconfirm
        title={intl.formatMessage({ id: 'message.your_change_lost' })}
        onConfirm={handleCancel}
        okText={intl.formatMessage({ id: 'crud.yes' })}
        cancelText={intl.formatMessage({ id: 'crud.no' })}
      >
        <Button
          type="primary"
          htmlType="button"
          style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
        >
          {intl.formatMessage({ id: 'crud.cancel' })}
        </Button>
      </Popconfirm>
    </Space>
  );

  const handleDescriptionModalOk = (value: string) => {
    let formValues: Partial<Item>[] = form.getFieldValue('values');

    if (descriptionData) {
      formValues[descriptionData.index].season_planner_detail_description = value;
    }

    form.setFieldsValue({ values: formValues });
    setDescriptionData(undefined);
  };

  const handleDescriptionModalCancel = () => {
    setDescriptionData(undefined);
  };

  const handleFormChange = (changedValues: FormType, values: FormType) => {
    // console.log('changedValues', changedValues);
    // console.log('values', values);
    // for (const key in changedValues.values) {
    // }
  };

  const handleItemSubmit = async (key: number, seasonPlannerDetailId: number) => {
    try {
      const namePathField = ['values', key, 'start_week'];
      const colorPathField = ['values', key, 'end_week'];
      await form.validateFields([namePathField, colorPathField]);
      const val: Item[] = form.getFieldValue('values');
      const startWeek = val[key].start_week;
      const endWeek = val[key].end_week;
      const value = val[key].value;
      const description = val[key].season_planner_detail_description;
      if (value && startWeek && endWeek && startWeek <= endWeek) {
        const editedDataIndex = data.findIndex(
          (d) => d.seasonPlannerDetailId === val[key].season_planner_detail_id,
        );
        const newData = [...data];
        const editedData = { ...newData[editedDataIndex] };
        for (let i = startWeek; i <= endWeek; i++) {
          editedData['week_' + i] = value;
        }

        newData[editedDataIndex] = editedData;
        setData([...newData]);

        const newItem = [...items];
        const itemExist = newItem.find((d) => d.season_planner_detail_id === seasonPlannerDetailId);
        if (!itemExist) {
          newItem.push({
            end_week: endWeek,
            start_week: startWeek,
            season_planner_detail_description: description,
            season_planner_detail_id: seasonPlannerDetailId,
            value,
          });
        }
        setItems(newItem);
      }
    } catch (error) {}
  };

  const handleItemDelete = async (key: number) => {
    try {
      const namePathField = ['values', key, 'start_week'];
      const colorPathField = ['values', key, 'end_week'];
      await form.validateFields([namePathField, colorPathField]);
      const val: Item[] = form.getFieldValue('values');
      const startWeek = val[key].start_week;
      const endWeek = val[key].end_week;
      if (startWeek && endWeek && startWeek <= endWeek) {
        const editedDataIndex = data.findIndex(
          (d) => d.seasonPlannerDetailId === val[key].season_planner_detail_id,
        );
        const newData = [...data];
        const editedData = { ...newData[editedDataIndex] };
        for (let i = startWeek; i <= endWeek; i++) {
          editedData['week_' + i] = '';
        }

        newData[editedDataIndex] = editedData;
        setData([...newData]);
      }
    } catch (error) {}
  };

  return (
    <>
      <Drawer
        height="35%"
        title={editedSeasonPlanner?.name}
        bodyStyle={{ overflowX: 'auto' }}
        visible={visible}
        placement="bottom"
        mask={false}
        headerStyle={{ padding: '10px 20px' }}
        extra={<Extra />}
        closable={false}
      >
        <Form form={form} layout="horizontal" onValuesChange={handleFormChange}>
          {editedSeasonPlanner?.season_planner_details?.map((d, i) => (
            <FormItem
              data={d}
              handleItemSubmit={handleItemSubmit}
              handleItemDelete={handleItemDelete}
              key={i}
              index={i}
              form={form}
              setDescriptionData={setDescriptionData}
            />
          ))}
        </Form>
      </Drawer>
      <DescriptionModal
        visible={descriptionData?.visible ? true : false}
        value={descriptionData?.value}
        onOk={handleDescriptionModalOk}
        onCancel={handleDescriptionModalCancel}
      />
    </>
  );
};

export default YtpForm;
