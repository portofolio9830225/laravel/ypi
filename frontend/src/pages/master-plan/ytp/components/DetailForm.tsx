import { YtpRecordType } from '@/types/ypi';
import {
  Drawer,
  Row,
  Col,
  Form,
  Button,
  Select,
  Space,
  InputNumber,
  Table,
  DatePicker,
  message,
} from 'antd';
import { useEffect, useState, useContext } from 'react';
import { FormattedMessage, useIntl } from 'umi';
import { getLocaleKey, Context } from '../index';
import { YtpContext } from './Ytp';
import Ytp from '@/components/Ytp';
import { Line } from '@ant-design/plots';
import moment, { Moment } from 'moment';

const { Option } = Select;
const { RangePicker } = DatePicker;

interface FormType {
  week: number;
  target: string;
  important_tournament: string;
  fitness_date: string;
  fitness_date_moment: [Moment, Moment];
  sport_index: number;
  academy_index: number;
  public_holiday: string;
  public_holiday_moment: [Moment, Moment];
}

export default () => {
  const intl = useIntl();
  const [visible, setVisible] = useState<boolean>(false);
  const [form] = Form.useForm<FormType>();
  const { eventData, constantValues } = useContext(Context);
  const { activeSpId, setActiveSpId, ytpData, handleSave } = useContext(YtpContext);
  const [tableData, setTableData] = useState<Partial<YtpRecordType>[]>([]);
  const [weekRange, setWeekRange] = useState<[Moment, Moment]>();
  const [spName, setSpName] = useState<string>();
  const weekIndex = Form.useWatch('week', form);
  const [fitnessDateValidateStatus, setFitnessDateValidateStatus] = useState<'error'>();
  const [publicHolidayValidateStatus, setPublicHolidayValidateStatus] = useState<'error'>();

  const processTableData = () => {
    if (ytpData) {
      const dateRecords = [...ytpData].filter((d) => d.isDateData === true);
      const focusSeasonPlannerRecords = ytpData.filter(
        (d) => d.seasonPlanId === constantValues?.SP_STUDY_BALANCE_ID,
      );
      if (focusSeasonPlannerRecords.length > 0)
        setSpName(focusSeasonPlannerRecords[0].seasonPlanner);
      setTableData([...dateRecords, ...focusSeasonPlannerRecords]);
    }
  };

  useEffect(() => {
    if (activeSpId === constantValues?.SP_STUDY_BALANCE_ID) {
      processTableData();
      setVisible(true);
    } else {
      setVisible(false);
      form.resetFields();
    }
  }, [activeSpId, constantValues]);

  if (activeSpId === undefined || activeSpId !== constantValues?.SP_STUDY_BALANCE_ID) return <></>;

  const closeForm = () => {
    setVisible(false);
    setActiveSpId(undefined);
    form.resetFields();
  };

  const handleBeforeSave = () => {
    if (fitnessDateValidateStatus || publicHolidayValidateStatus) {
      message.error(intl.formatMessage({ id: 'message.date_not_in_range' }));
    } else {
      handleSave(tableData);
    }
  };

  const Extra = () => {
    return (
      <Space>
        <Button key="back" onClick={closeForm}>
          <FormattedMessage id="crud.cancel" />
        </Button>
        <Button key="submit" type="primary" onClick={handleBeforeSave}>
          <FormattedMessage id="crud.save" />
        </Button>
      </Space>
    );
  };

  const fillWeekRange = (week: number) => {
    const year = tableData[0]['week_' + week];
    const month = tableData[1]['week_' + week];
    const startWeek = tableData[2]['week_' + week];
    console.log({ year, month, startWeek });

    const startWeekMoment = moment(`${year}-${month}-${startWeek}`);
    setWeekRange([startWeekMoment, moment(startWeekMoment).add(6, 'd')]);
  };

  const onCellClick = (weekIndex: number) => {
    fillWeekRange(weekIndex);

    const spd1Index = tableData.findIndex(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_TARGET_ID,
    );
    const spd2Index = tableData.findIndex(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_RATE_IMPORTANT_ID,
    );
    const spd3Index = tableData.findIndex(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_DATE_FITNESS_ID,
    );
    const spd4Index = tableData.findIndex(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_ACADEMY_INDEX_ID,
    );
    const spd5Index = tableData.findIndex(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_SPORT_INDEX_ID,
    );
    const spd6Index = tableData.findIndex(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_PUBLIC_HOLIDAY_ID,
    );

    const fitnessDate = tableData[spd3Index]['week_' + weekIndex];
    let fintessDateMoment: [Moment, Moment] | undefined = undefined;
    const year = tableData[0]['week_' + weekIndex];
    const month = tableData[1]['week_' + weekIndex];
    if (fitnessDate) {
      const dates = (fitnessDate as string).split('-');
      const startDate = moment(`${year}-${month}-${dates[0]}`);
      const endDate = moment(`${year}-${month}-${dates[1]}`);
      fintessDateMoment = [startDate, endDate];
    }
    const publicHoliday = tableData[spd6Index]['week_' + weekIndex];
    let publicHolidayMoment: [Moment, Moment] | undefined = undefined;
    if (publicHoliday) {
      const dates = (publicHoliday as string).split('-');
      const startDate = moment(`${year}-${month}-${dates[0]}`);
      const endDate = moment(`${year}-${month}-${dates[1]}`);
      publicHolidayMoment = [startDate, endDate];
    }

    form.setFieldsValue({
      week: weekIndex,
      target: tableData[spd1Index]['week_' + weekIndex],
      important_tournament: tableData[spd2Index]['week_' + weekIndex],
      fitness_date: tableData[spd3Index]['week_' + weekIndex],
      fitness_date_moment: fintessDateMoment,
      academy_index: tableData[spd4Index]['week_' + weekIndex],
      sport_index: tableData[spd5Index]['week_' + weekIndex],
      public_holiday: tableData[spd6Index]['week_' + weekIndex],
      public_holiday_moment: publicHolidayMoment,
    });
  };

  const onValuesChange = (changedValues: any, values: FormType) => {
    if (changedValues.week) {
      fillWeekRange(changedValues.week);
    }

    if (values.week) {
      setTableData((data) => {
        const spd1Index = tableData.findIndex(
          (d) => d.seasonPlanDetailId === constantValues?.SPD_TARGET_ID,
        );
        const spd2Index = tableData.findIndex(
          (d) => d.seasonPlanDetailId === constantValues?.SPD_RATE_IMPORTANT_ID,
        );
        const spd3Index = tableData.findIndex(
          (d) => d.seasonPlanDetailId === constantValues?.SPD_DATE_FITNESS_ID,
        );
        const spd4Index = tableData.findIndex(
          (d) => d.seasonPlanDetailId === constantValues?.SPD_ACADEMY_INDEX_ID,
        );
        const spd5Index = tableData.findIndex(
          (d) => d.seasonPlanDetailId === constantValues?.SPD_SPORT_INDEX_ID,
        );
        const spd6Index = tableData.findIndex(
          (d) => d.seasonPlanDetailId === constantValues?.SPD_PUBLIC_HOLIDAY_ID,
        );

        const newData = [...data];

        const spd1Row = { ...newData[spd1Index] };
        spd1Row['week_' + values.week] = values.target;
        newData[spd1Index] = spd1Row;

        const spd2Row = { ...newData[spd2Index] };
        spd2Row['week_' + values.week] = values.important_tournament;
        newData[spd2Index] = spd2Row;

        if (values.fitness_date_moment) {
          const spd3Row = { ...newData[spd3Index] };
          spd3Row['week_' + values.week] = `${values.fitness_date_moment[0].format(
            'DD',
          )}-${values.fitness_date_moment[1].format('DD')}`;
          newData[spd3Index] = spd3Row;
        }

        const spd4Row = { ...newData[spd4Index] };
        spd4Row['week_' + values.week] = values.academy_index;
        newData[spd4Index] = spd4Row;

        const spd5Row = { ...newData[spd5Index] };
        spd5Row['week_' + values.week] = values.sport_index;
        newData[spd5Index] = spd5Row;

        if (values.public_holiday_moment) {
          const spd6Row = { ...newData[spd6Index] };
          spd6Row['week_' + values.week] = `${values.public_holiday_moment[0].format(
            'DD',
          )}-${values.fitness_date_moment[1].format('DD')}`;
          newData[spd6Index] = spd6Row;
        }

        return [...newData];
      });
    }

    if (changedValues.fitness_date_moment) {
      const fitnessDate: [Moment, Moment] = changedValues.fitness_date_moment;
      if (
        fitnessDate[0].isBetween(weekRange?.[0], weekRange?.[1]) === false ||
        fitnessDate[1].isBetween(weekRange?.[0], moment(weekRange?.[1]).add(1, 'd')) === false
      ) {
        setFitnessDateValidateStatus('error');
      } else {
        setFitnessDateValidateStatus(undefined);
      }
    }

    if (changedValues.public_holiday_moment) {
      const publicHoliday: [Moment, Moment] = changedValues.public_holiday_moment;
      if (
        publicHoliday[0].isBetween(weekRange?.[0], weekRange?.[1]) === false ||
        publicHoliday[1].isBetween(weekRange?.[0], moment(weekRange?.[1]).add(1, 'd')) === false
      ) {
        setPublicHolidayValidateStatus('error');
      } else {
        setPublicHolidayValidateStatus(undefined);
      }
    }

    console.log({
      range1: weekRange?.[0].format('YYYY-MM-DD'),
      range2: weekRange?.[1].format('YYYY-MM-DD'),
    });
  };

  const ytpChart = () => {
    const data = [];
    const academyIndexData = tableData.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_ACADEMY_INDEX_ID,
    );
    if (academyIndexData) {
      for (const key in academyIndexData) {
        if (key.startsWith('week_')) {
          data.push({
            name: academyIndexData.seasonPlannerDetail,
            value: academyIndexData[key] !== '' ? parseInt(academyIndexData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }
    const sportIndexData = tableData.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_SPORT_INDEX_ID,
    );
    if (sportIndexData) {
      for (const key in sportIndexData) {
        if (key.startsWith('week_')) {
          data.push({
            name: sportIndexData.seasonPlannerDetail,
            value: sportIndexData[key] !== '' ? parseInt(sportIndexData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }

    const config = {
      data,
      xField: 'week',
      yField: 'value',
      seriesField: 'name',
      height: 200,
      color: ['#9e1fe2', '#ff0000'],
      point: {
        size: 4,
        shape: 'square',
        style: {
          lineWidth: 2,
        },
      },
    };

    return (
      <>
        <Table.Summary.Row>
          <Table.Summary.Cell index={0}></Table.Summary.Cell>
          <Table.Summary.Cell index={1}></Table.Summary.Cell>
          <Table.Summary.Cell index={2} colSpan={53}>
            <Line {...config} />
          </Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  };

  return (
    <Drawer
      title={spName}
      // width="100%"
      visible={visible}
      closable={false}
      placement="bottom"
      height="100%"
    >
      <div style={{ height: '7rem', width: '100%' }} />
      <Ytp
        mergeCells={false}
        onCellClick={onCellClick}
        tableData={tableData}
        eventData={eventData}
        ytpChart={ytpChart}
      />
      {visible && <div style={{ height: '50vh', width: '100%' }} />}
      <Drawer
        closable={false}
        title=" "
        visible={visible}
        mask={false}
        extra={<Extra />}
        placement="bottom"
        headerStyle={{ padding: '5px 12px' }}
        height="14rem"
      >
        <Form layout="vertical" form={form} onValuesChange={onValuesChange}>
          <Row gutter={6}>
            <Col span={6}>
              <Form.Item name="week" label={<FormattedMessage id="commonField.week" />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="target" label={<FormattedMessage id={getLocaleKey('target')} />}>
                <Select disabled={!weekIndex}>
                  <Option value="QR">Qualifying Round</Option>
                  <Option value="1st">First round</Option>
                  <Option value="2nd">Second round</Option>
                  <Option value="3nd">Third round / top-32</Option>
                  <Option value="T16">Top 16</Option>
                  <Option value="QF">Quarter-final</Option>
                  <Option value="SF">Semi-final</Option>
                  <Option value="F">Final</Option>
                  <Option value="C">Champion</Option>
                  <Option value="">
                    <FormattedMessage id="commonField.empty" />
                  </Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                name="important_tournament"
                label={<FormattedMessage id={getLocaleKey('important_tournament')} />}
              >
                <Select disabled={!weekIndex}>
                  <Option value="1*">1*</Option>
                  <Option value="2*">2*</Option>
                  <Option value="3*">3*</Option>
                  <Option value="4*">4*</Option>
                  <Option value="5*">5*</Option>
                  <Option value="">
                    <FormattedMessage id="commonField.empty" />
                  </Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item hidden name="fitness_date" />
              <Form.Item
                name="fitness_date_moment"
                label={<FormattedMessage id={getLocaleKey('fitness_date')} />}
                validateStatus={fitnessDateValidateStatus}
                help={
                  fitnessDateValidateStatus && <FormattedMessage id="message.date_not_in_range" />
                }
              >
                <RangePicker disabled={!weekIndex} />
              </Form.Item>
            </Col>
          </Row>
          <Row gutter={6}>
            <Col span={6}>
              <Form.Item
                name="academy_index"
                label={<FormattedMessage id={getLocaleKey('academy_index')} />}
              >
                <Select disabled={!weekIndex}>
                  <Option value="1">1</Option>
                  <Option value="2">2</Option>
                  <Option value="3">3</Option>
                  <Option value="4">4</Option>
                  <Option value="5">5</Option>
                  <Option value="">
                    <FormattedMessage id="commonField.empty" />
                  </Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item
                name="sport_index"
                label={<FormattedMessage id={getLocaleKey('sport_index')} />}
              >
                <Select disabled={!weekIndex}>
                  <Option value="1">1</Option>
                  <Option value="2">2</Option>
                  <Option value="3">3</Option>
                  <Option value="4">4</Option>
                  <Option value="5">5</Option>
                  <Option value="">
                    <FormattedMessage id="commonField.empty" />
                  </Option>
                </Select>
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item hidden name="public_holiday" />
              <Form.Item
                name="public_holiday_moment"
                validateTrigger="onBlur"
                label={<FormattedMessage id={getLocaleKey('public_holiday')} />}
                validateStatus={publicHolidayValidateStatus}
                help={
                  publicHolidayValidateStatus && <FormattedMessage id="message.date_not_in_range" />
                }
              >
                <RangePicker disabled={!weekIndex} />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </Drawer>
  );
};
