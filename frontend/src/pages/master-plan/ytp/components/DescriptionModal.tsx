import { Modal, Tabs, message } from 'antd';
import { FormattedMessage, useIntl } from 'umi';
import { useState, useEffect, FC, useContext } from 'react';
import '@wangeditor/editor/dist/css/style.css';
import { Editor, Toolbar } from '@wangeditor/editor-for-react';
import { IDomEditor, i18nChangeLanguage } from '@wangeditor/editor';
i18nChangeLanguage('en');
import { Context } from '../index';
import { get, update } from '@/services/ypi/api';
import { SeasonPlanDetail } from '@/types/ypi';

const DescriptionModal: FC = () => {
  const intl = useIntl();
  const [editorEn, setEditorEn] = useState<IDomEditor | null>(null);
  const [editorId, setEditorId] = useState<IDomEditor | null>(null);
  const [contentEn, setContentEn] = useState('');
  const [contentId, setContentId] = useState('');
  const [spdName, setSpdName] = useState('');
  const [visible, setVisible] = useState<boolean>(false);
  const { activeDescSpdId, setActiveDescSpdId } = useContext(Context);

  // Timely destroy editor, important!
  useEffect(() => {
    return () => {
      if (editorEn == null) return;
      editorEn.destroy();
      setEditorEn(null);
    };
  }, [editorEn]);

  useEffect(() => {
    return () => {
      if (editorId == null) return;
      editorId.destroy();
      setEditorId(null);
    };
  }, [editorId]);

  const fetchData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const spDetailRes = await get('season_plan_details/' + activeDescSpdId);
      const spDetail: SeasonPlanDetail = spDetailRes.data;
      setSpdName(spDetail.name);
      setContentEn(spDetail.desc_en);
      setContentId(spDetail.desc_id);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'errorSave' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  useEffect(() => {
    if (activeDescSpdId) {
      fetchData();
      setVisible(true);
    } else {
      setVisible(false);
    }
  }, [activeDescSpdId]);

  const handleOk = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const data = { desc_en: contentEn, desc_id: contentId };
    try {
      if (activeDescSpdId) {
        await update('season_plan_details', activeDescSpdId, data);
      }
      setActiveDescSpdId(undefined);
      editorEn?.clear();
      editorId?.clear();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'errorSave' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const handleCancel = () => {
    editorEn?.clear();
    editorId?.clear();
    setActiveDescSpdId(undefined);
  };

  return (
    <Modal
      title={`${intl.formatMessage({ id: 'commonField.desc' })}: ${spdName}`}
      onOk={handleOk}
      visible={visible}
      onCancel={handleCancel}
      centered={true}
      width="80%"
    >
      <Tabs size="small" defaultActiveKey="2">
        <Tabs.TabPane tab="English" key="1">
          <div style={{ border: '1px solid #ccc', zIndex: 100 }}>
            <Toolbar
              editor={editorEn}
              // defaultConfig={toolbarConfig}
              mode="default"
              style={{ borderBottom: '1px solid #ccc' }}
            />
            <Editor
              // defaultConfig={editorConfig}
              value={contentEn}
              onCreated={setEditorEn}
              onChange={(editor) => setContentEn(editor.getHtml())}
              mode="default"
              style={{ height: '70%', overflowY: 'hidden' }}
            />
          </div>
        </Tabs.TabPane>
        <Tabs.TabPane tab="Indonesian" key="2">
          <div style={{ border: '1px solid #ccc', zIndex: 100 }}>
            <Toolbar
              editor={editorId}
              // defaultConfig={toolbarConfig}
              mode="default"
              style={{ borderBottom: '1px solid #ccc' }}
            />
            <Editor
              // defaultConfig={editorConfig}
              value={contentId}
              onCreated={setEditorId}
              onChange={(editor) => setContentId(editor.getHtml())}
              mode="default"
              style={{ height: '70%', overflowY: 'hidden' }}
            />
          </div>
        </Tabs.TabPane>
      </Tabs>
    </Modal>
  );
};

export default DescriptionModal;
