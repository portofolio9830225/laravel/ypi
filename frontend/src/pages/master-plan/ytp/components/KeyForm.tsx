import { Form, Card, Select, Button, message } from 'antd';
import { getLocaleKey, Context } from '../index';
import { useContext } from 'react';
import { useIntl, FormattedMessage, history } from 'umi';
import { Ypi } from '@/types/ypi';
const { Option } = Select;

export default ({ data }: { data: Ypi[] }) => {
  const [form] = Form.useForm<Ypi>();
  const intl = useIntl();
  const { setSelectedYpiId, setPrintMode, selectedYpiId } = useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const handleProcess = async () => {
    const values = await form.validateFields();
    try {
      setSelectedYpiId(values.id);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'YPI' }));
    }
  };

  const handleReset = () => {
    window.location.reload();
  };

  const handleWtp = () => {
    history.push('/master-plan/wtp');
  };

  const handlePrint = () => {
    setPrintMode(true);
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form form={form} name="form" layout="inline">
        <Form.Item
          name="id"
          label={<FormattedMessage id={getLocaleKey('ypi_detail')} />}
          rules={getRequiredRule('ypi_detail')}
        >
          <Select style={{ width: '15rem' }}>
            {data.map((d, i) => (
              <Option key={i} value={d.id}>
                {d.keterangan}
              </Option>
            ))}
          </Select>
        </Form.Item>
        <Form.Item>
          <Button onClick={handleProcess}>
            <FormattedMessage id="crud.process" />
          </Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={handleReset}>
            <FormattedMessage id="crud.reset" />
          </Button>
        </Form.Item>
        <Form.Item>
          <Button onClick={handleWtp}>
            <FormattedMessage id={getLocaleKey('weekly')} />
          </Button>
        </Form.Item>
        <Form.Item>
          <Button disabled={selectedYpiId ? false : true} onClick={handlePrint}>
            <FormattedMessage id="crud.print" />
          </Button>
        </Form.Item>
        <Form.Item>
          <Button disabled>
            <FormattedMessage id="crud.help" />
          </Button>
        </Form.Item>
      </Form>
    </Card>
  );
};
