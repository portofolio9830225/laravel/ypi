import { YtpRecordType } from '@/types/ypi';
import {
  Drawer,
  Row,
  Col,
  Form,
  Input,
  Button,
  Select,
  Space,
  FormInstance,
  Table,
  InputNumber,
} from 'antd';
import { useEffect, useState, useContext, FC, ReactNode, useCallback } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { getLocaleKey, Context } from '../index';
import { YtpContext } from './Ytp';
import Ytp from '@/components/Ytp';
import { BulbOutlined, CheckOutlined, DeleteOutlined } from '@ant-design/icons';
import { Line } from '@ant-design/plots';
import DescriptionModal from './DescriptionModal';

const { Option } = Select;

interface FormItemProp {
  index: number;
  form: FormInstance<FormType>;
  spdId?: number;
  spdType?: string;
  spdName: string;
  handleItemSubmit: (key: number, seasonPlannerDetailId: number) => Promise<void>;
  handleItemDelete: (key: number) => void;
}

const FormItem: FC<FormItemProp> = ({
  index,
  form,
  handleItemSubmit,
  handleItemDelete,
  spdId,
  spdName,
  spdType,
}) => {
  const intl = useIntl();
  const fieldPrefixName = 'values';
  const { editedCell, setActiveDescSpdId, constantValues } = useContext(Context);
  const startWeek = Form.useWatch([fieldPrefixName, index, 'start_week'], form);
  const endWeek = Form.useWatch([fieldPrefixName, index, 'end_week'], form);

  const updateFields = () => {
    let formValues: Partial<Item>[] = form.getFieldValue(fieldPrefixName);

    if (!formValues) {
      formValues = [];
    }
    if (!formValues[index]) {
      formValues[index] = {};
    }

    formValues[index].season_planner_detail_id = spdId;
    if (formValues[index].season_planner_detail_id === editedCell?.seasonPlannerDetailId) {
      formValues[index].value = editedCell?.content;
      formValues[index].start_week = editedCell?.startWeek;
      formValues[index].end_week = editedCell?.endWeek;
    } else {
      formValues[index].value = undefined;
      formValues[index].start_week = undefined;
      formValues[index].end_week = undefined;
    }
    form.setFieldsValue({ values: formValues });
  };

  useEffect(() => {
    updateFields();
  }, [spdId]);

  useEffect(() => {
    if (editedCell) updateFields();
  }, [editedCell]);

  const getInputValue = (): ReactNode => {
    if (spdId === constantValues?.SPD_TRAINING_PHASE_ID) {
      return (
        <Select>
          <Option value="Preparatory">
            <FormattedMessage id={getLocaleKey('Preparatory')} />
          </Option>
          <Option value="Competition">
            <FormattedMessage id={getLocaleKey('Competition')} />
          </Option>
          <Option value="Transition">
            <FormattedMessage id={getLocaleKey('Transition')} />
          </Option>
        </Select>
      );
    } else if (spdId === constantValues?.SPD_SUB_PHASE_ID) {
      return (
        <Select>
          <Option value="General preparation">
            <FormattedMessage id={getLocaleKey('General preparation')} />
          </Option>
          <Option value="Specific Preparation">
            <FormattedMessage id={getLocaleKey('Specific Preparation')} />
          </Option>
          <Option value="Pre-Competition">
            <FormattedMessage id={getLocaleKey('Pre-Competition')} />
          </Option>
          <Option value="Competition">
            <FormattedMessage id={getLocaleKey('Competition')} />
          </Option>
          <Option value="Transition">
            <FormattedMessage id={getLocaleKey('Transition')} />
          </Option>
        </Select>
      );
    } else {
      if (spdType === 'number') {
        return <InputNumber style={{ width: '100%' }} type="number" />;
      }
      return <Input />;
    }
  };

  return (
    <Row justify="center" gutter={6}>
      <Col span={1}>
        <Form.Item>
          <Button
            onClick={() => {
              setActiveDescSpdId(spdId);
            }}
            size="small"
            type="primary"
            shape="circle"
            icon={<BulbOutlined />}
          />
        </Form.Item>
      </Col>
      <Col span={7}>
        <Form.Item name={[fieldPrefixName, index, 'value']} label={spdName}>
          {getInputValue()}
        </Form.Item>
      </Col>
      <Col span={7}>
        <Form.Item
          name={[fieldPrefixName, index, 'start_week']}
          label={<FormattedMessage id={getLocaleKey('week')} />}
          rules={[
            {
              type: 'number',
              max: endWeek,
              message: intl.formatMessage({ id: 'message.must_less' }, { variable: 'End Week' }),
            },
            {
              type: 'number',
              min: 1,
              message: intl.formatMessage({ id: 'message.must_greater' }, { variable: '0' }),
            },
          ]}
        >
          <InputNumber type="number" style={{ width: '100%' }} />
        </Form.Item>
      </Col>
      <Col span={7}>
        <Form.Item
          name={[fieldPrefixName, index, 'end_week']}
          label={<FormattedMessage id={getLocaleKey('to_week')} />}
          rules={[
            {
              type: 'number',
              min: startWeek,
              message: intl.formatMessage(
                { id: 'message.must_greater' },
                { variable: 'Start Week' },
              ),
            },
            {
              type: 'number',
              min: 1,
              message: intl.formatMessage({ id: 'message.must_greater' }, { variable: '0' }),
            },
            {
              type: 'number',
              required: startWeek > 0 ? true : false,
              message: intl.formatMessage(
                { id: 'message.pleaseInputField' },
                { fieldName: 'End Week' },
              ),
            },
          ]}
        >
          <InputNumber type="number" style={{ width: '100%' }} />
        </Form.Item>
      </Col>
      <Col span={2}>
        <Space>
          <Form.Item>
            <Button
              onClick={() => handleItemSubmit(index, spdId || -1)}
              size="small"
              type="primary"
              shape="circle"
              icon={<CheckOutlined />}
            />
          </Form.Item>
          <Form.Item>
            <Button
              onClick={() => handleItemDelete(index)}
              danger
              size="small"
              type="primary"
              shape="circle"
              icon={<DeleteOutlined />}
            />
          </Form.Item>
        </Space>
      </Col>
    </Row>
  );
};

interface Item {
  season_planner_detail_description: string;
  start_week: number;
  end_week: number;
  season_planner_detail_id: number;
  value: string | number;
}
interface FormType {
  values: Item[];
}

interface FocusSeasonPlanner extends YtpRecordType {
  fieldIndex: number;
}

export default () => {
  const [visible, setVisible] = useState<boolean>(false);
  const [form] = Form.useForm<FormType>();
  const { ytpData, handleSave, activeSpId, setActiveSpId } = useContext(YtpContext);
  const [tableData, setTableData] = useState<Partial<YtpRecordType>[]>([]);
  const [focusSpData, setFocusSpData] = useState<Partial<FocusSeasonPlanner>[]>([]);
  const { eventData, constantValues } = useContext(Context);
  const [spName, setSpName] = useState<string>();

  const processTableData = () => {
    if (ytpData && activeSpId) {
      const headerRecord = [...ytpData].filter((d) => d.isDateData === true);
      let periodizationRecord =
        activeSpId != constantValues?.SP_PERIODIZATION_ID
          ? [...ytpData].filter((d) => d.isPeriodization === true)
          : [];
      const focusSeasonPlannerRecords = [...ytpData].filter((d) => d.seasonPlanId === activeSpId);

      if (focusSeasonPlannerRecords.length > 0)
        setSpName(focusSeasonPlannerRecords[0].seasonPlanner);

      setTableData([...headerRecord, ...periodizationRecord, ...focusSeasonPlannerRecords]);
      setFocusSpData(
        focusSeasonPlannerRecords.map((d, i) => {
          return { ...d, fieldIndex: i };
        }),
      );
    }
  };

  useEffect(() => {
    if (activeSpId && activeSpId !== constantValues?.SP_STUDY_BALANCE_ID) {
      processTableData();
      setVisible(true);
    } else {
      setVisible(false);
      form.resetFields();
    }
  }, [activeSpId]);

  const ytpChart = useCallback(() => {
    const data = [];
    const intensityData = tableData.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_INTENSITY_ID,
    );
    if (intensityData) {
      for (const key in intensityData) {
        if (key.startsWith('week_')) {
          data.push({
            name: intensityData.seasonPlannerDetail,
            value: intensityData[key] !== '' ? parseInt(intensityData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }
    const volumeData = tableData.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_VOLUME_ID,
    );
    if (volumeData) {
      for (const key in volumeData) {
        if (key.startsWith('week_')) {
          data.push({
            name: volumeData.seasonPlannerDetail,
            value: volumeData[key] !== '' ? parseInt(volumeData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }
    const spesificData = tableData.find(
      (d) => d.seasonPlanDetailId === constantValues?.SPD_SPESIFIC_ID,
    );
    if (spesificData) {
      for (const key in spesificData) {
        if (key.startsWith('week_')) {
          data.push({
            name: spesificData.seasonPlannerDetail,
            value: spesificData[key] !== '' ? parseInt(spesificData[key]) : 0,
            week: parseInt(key.substring(5)),
          });
        }
      }
    }

    const config = {
      data,
      xField: 'week',
      yField: 'value',
      seriesField: 'name',
      height: 200,
      color: ['#00ff00', '#0000ff', '#ff0000'],
    };

    return (
      <>
        <Table.Summary.Row>
          <Table.Summary.Cell index={0}></Table.Summary.Cell>
          <Table.Summary.Cell index={1}></Table.Summary.Cell>
          <Table.Summary.Cell index={2} colSpan={53}>
            <Line {...config} />
          </Table.Summary.Cell>
        </Table.Summary.Row>
      </>
    );
  }, [tableData]);

  if (activeSpId === undefined || activeSpId === constantValues?.SP_STUDY_BALANCE_ID) return <></>;

  const closeForm = () => {
    setVisible(false);
    setActiveSpId(undefined);
    form.resetFields();
  };

  const Extra = () => {
    return (
      <Space>
        <Button key="back" onClick={closeForm}>
          <FormattedMessage id="crud.cancel" />
        </Button>
        <Button
          key="submit"
          type="primary"
          onClick={() => {
            handleSave(tableData);
          }}
        >
          <FormattedMessage id="crud.save" />
        </Button>
      </Space>
    );
  };

  const onCellClick = (startWeek: number, endWeek?: number, rowId?: number, content?: string) => {
    const currentRow = focusSpData.find((d) => d.seasonPlanDetailId === rowId);
    if (currentRow && currentRow.fieldIndex !== undefined) {
      const formValues: Item[] = form.getFieldValue('values');
      formValues[currentRow?.fieldIndex].start_week = startWeek;
      if (endWeek) formValues[currentRow?.fieldIndex].end_week = endWeek;
      if (content) formValues[currentRow?.fieldIndex].value = content;
      form.setFieldsValue({ values: formValues });
    }
  };

  const handleItemSubmit = async (key: number, seasonPlannerDetailId: number) => {
    try {
      const namePathField = ['values', key, 'start_week'];
      const colorPathField = ['values', key, 'end_week'];
      await form.validateFields([namePathField, colorPathField]);
      const val: Item[] = form.getFieldValue('values');
      const startWeek = val[key].start_week;
      const endWeek = val[key].end_week;
      const value = val[key].value;
      if (value && startWeek && endWeek && startWeek <= endWeek) {
        const editedDataIndex = tableData.findIndex(
          (d) => d.seasonPlanDetailId === val[key].season_planner_detail_id,
        );
        const newData = [...tableData];
        const editedData = { ...newData[editedDataIndex] };
        for (let i = startWeek; i <= endWeek; i++) {
          editedData['week_' + i] = value;
        }

        newData[editedDataIndex] = editedData;
        setTableData([...newData]);
      }
    } catch (error) {}
  };

  const handleItemDelete = async (key: number) => {
    try {
      const namePathField = ['values', key, 'start_week'];
      const colorPathField = ['values', key, 'end_week'];
      await form.validateFields([namePathField, colorPathField]);
      const val: Item[] = form.getFieldValue('values');
      const startWeek = val[key].start_week;
      const endWeek = val[key].end_week;
      if (startWeek && endWeek && startWeek <= endWeek) {
        const editedDataIndex = tableData.findIndex(
          (d) => d.seasonPlanDetailId === val[key].season_planner_detail_id,
        );
        const newData = [...tableData];
        const editedData = { ...newData[editedDataIndex] };
        for (let i = startWeek; i <= endWeek; i++) {
          editedData['week_' + i] = '';
        }

        newData[editedDataIndex] = editedData;
        setTableData([...newData]);
      }
    } catch (error) {}
  };

  return (
    <>
      <Drawer
        title={spName}
        // width="100%"
        visible={visible}
        closable={false}
        placement="bottom"
        height="100%"
      >
        <div style={{ height: '7rem', width: '100%' }} />
        <Ytp
          mergeCells={true}
          onCellClick={onCellClick}
          tableData={tableData}
          eventData={eventData}
          ytpChart={activeSpId === constantValues?.SP_WORK_TIME_ID ? ytpChart : undefined}
        />
        {visible && <div style={{ height: '50vh', width: '100%' }} />}
        <Drawer
          closable={false}
          title=" "
          visible={visible}
          mask={false}
          extra={<Extra />}
          placement="bottom"
          headerStyle={{ padding: '5px 12px' }}
          height="14rem"
        >
          <Form layout="horizontal" form={form}>
            {focusSpData.map((d, i) => (
              <FormItem
                form={form}
                index={i}
                key={i}
                spdId={d.seasonPlanDetailId}
                handleItemSubmit={handleItemSubmit}
                spdName={d.seasonPlannerDetail || ''}
                spdType={d.inputType}
                handleItemDelete={handleItemDelete}
              />
            ))}
          </Form>
        </Drawer>
      </Drawer>
      <DescriptionModal />
    </>
  );
};
