import { Context, getLocaleKey } from '../index';
import { useContext } from 'react';
import { Card, Row, Col } from 'antd';
import { FormattedMessage } from 'umi';
import styles from './YtpHeader.less';

export default () => {
  const { selectedYpi } = useContext(Context);

  if (!selectedYpi) return <></>;

  return (
    <Card>
      <Row gutter={12}>
        <Col span={12}>
          <span className={styles.ytp}>YTP </span>
          <span className={styles.ytp2}>Yearly Training Program</span>
          <br />
          <span>
            <FormattedMessage id={getLocaleKey('coach_name')} />: {selectedYpi.coach_name}
          </span>
          <br />
          <span>
            <FormattedMessage id={getLocaleKey('schedule')} />: {selectedYpi.tanggal_mulai}{' '}
            <FormattedMessage id="commonField.to" /> {selectedYpi.tanggal_selesai}
          </span>
          <br />
          <span>
            <FormattedMessage id={getLocaleKey('description')} />: {selectedYpi.keterangan}
          </span>
        </Col>
        <Col span={12}>
          <div className={styles.goals}>
            <div>
              <FormattedMessage id={getLocaleKey('goal')} />
            </div>
            <div>
              {selectedYpi.ypi_goals?.map((d, i) => (
                <div key={i}>
                  <span>
                    {i + 1}. {d.goal}
                  </span>
                  <br />
                </div>
              ))}
            </div>
          </div>
        </Col>
      </Row>
    </Card>
  );
};
