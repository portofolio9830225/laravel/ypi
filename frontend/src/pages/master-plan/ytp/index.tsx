import { useState, useEffect, createContext } from 'react';
import Ytp from './components/Ytp';
import { useIntl } from 'umi';
import {
  Ypi,
  SeasonPlanValue,
  YpiEvent,
  YpiDefaultTarget,
  YpiGoal,
  CompetitionDetail,
  Constant,
} from '@/types/ypi';
import KeyForm from './components/KeyForm';
import { get } from '@/services/ypi/api';
import YtpHeader from './components/YtpHeader';
import { message } from 'antd';
import { Moment } from 'moment';
import { jsPDF } from 'jspdf';

export const getLocaleKey = (fieldName: string) => {
  return 'pages.ytp.' + fieldName;
};

export interface EditedCell {
  content: string;
  startWeek: number;
  endWeek: number;
  seasonPlannerDetailId: number;
}

export interface YpiSeasonPlanValue extends SeasonPlanValue {
  seasonPlan: string;
  seasonPlanDetail: string;
}

export interface ConstantValue {
  FIXED_SEASON_PLANS_ID: number[];
  FIXED_SEASON_PLAN_DETAILS_ID: number[];
  DEFAULT_SEASON_PLANS: YpiSeasonPlanValue[];
  SP_STUDY_BALANCE_ID: number;
  SP_PERIODIZATION_ID: number;
  SPD_ACADEMY_INDEX_ID: number;
  SPD_SPORT_INDEX_ID: number;
  SPD_INTENSITY_ID: number;
  SPD_VOLUME_ID: number;
  SPD_SPESIFIC_ID: number;
  SPD_TARGET_ID: number;
  SPD_DATE_FITNESS_ID: number;
  SPD_PUBLIC_HOLIDAY_ID: number;
  SPD_RATE_IMPORTANT_ID: number;
  SPD_TRAINING_PHASE_ID: number;
  SPD_SUB_PHASE_ID: number;
  SP_WORK_TIME_ID: number;
  YTP_DATE_COLOR: string;
  YTP_SEASON_PLAN_COLOR: string;
}

export interface YpiExtra extends Ypi {
  tanggal_mulai_moment: Moment;
  season_plan_values: YpiSeasonPlanValue[];
  ypi_events: YpiEvent[];
  default_target: YpiDefaultTarget;
  ypi_goals: YpiGoal[];
  deleted_ypi_goals?: number[];
  coach_name: string;
}

export interface ContextType {
  selectedYpiId: number | undefined;
  setSelectedYpiId: React.Dispatch<React.SetStateAction<number | undefined>>;
  editedCell: EditedCell | undefined;
  setEditedCell: React.Dispatch<React.SetStateAction<EditedCell | undefined>>;
  eventData: Partial<CompetitionDetail>[];
  setEventData: React.Dispatch<React.SetStateAction<Partial<CompetitionDetail>[]>>;
  selectedYpi: YpiExtra | undefined;
  setSelectedYpi: React.Dispatch<React.SetStateAction<YpiExtra | undefined>>;
  activeDescSpdId: number | undefined;
  setActiveDescSpdId: React.Dispatch<React.SetStateAction<number | undefined>>;
  constantValues: Partial<ConstantValue> | undefined;
  printMode: boolean;
  setPrintMode: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Context = createContext<ContextType>({
  selectedYpiId: undefined,
  setSelectedYpiId: () => {},
  selectedYpi: undefined,
  setSelectedYpi: () => {},
  activeDescSpdId: undefined,
  setActiveDescSpdId: () => {},
  editedCell: undefined,
  setEditedCell: () => {},
  eventData: [],
  setEventData: () => {},
  constantValues: undefined,
  printMode: false,
  setPrintMode: () => {},
});

export default () => {
  const intl = useIntl();
  const [ypis, setYpis] = useState<Ypi[]>([]);
  const [selectedYpiId, setSelectedYpiId] = useState<number>();
  const [activeDescSpdId, setActiveDescSpdId] = useState<number>();
  const [selectedYpi, setSelectedYpi] = useState<YpiExtra>();
  const [editedCell, setEditedCell] = useState<EditedCell>();
  const [eventData, setEventData] = useState<Partial<CompetitionDetail>[]>([]);
  const [constantValues, setConstantValues] = useState<Partial<ConstantValue>>();
  const [printMode, setPrintMode] = useState<boolean>(false);

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const ypiRes = await get('master_ypis');
      const ypiData: Ypi[] = ypiRes.data;
      const processedYpiData = ypiData.map((d) => {
        return {
          ...d,
          tanggal_mulai: d.tanggal_mulai.substring(0, 10),
          tanggal_selesai: d.tanggal_selesai.substring(0, 10),
        };
      });
      setYpis(processedYpiData);

      const constantRes = await get('constants');
      const constants: Constant[] = constantRes.data;
      let constVal: Partial<ConstantValue> = {};

      const spStudyBal = constants.find((d) => d.key === 'SP_STUDY_BALANCE_ID');
      if (spStudyBal) constVal = { ...constVal, SP_STUDY_BALANCE_ID: parseInt(spStudyBal.value) };

      const spPeriodization = constants.find((d) => d.key === 'SP_PERIODIZATION_ID');
      if (spPeriodization)
        constVal = { ...constVal, SP_PERIODIZATION_ID: parseInt(spPeriodization.value) };

      const spdTarget = constants.find((d) => d.key === 'SPD_TARGET_ID');
      if (spdTarget) constVal = { ...constVal, SPD_TARGET_ID: parseInt(spdTarget.value) };

      const spdRate = constants.find((d) => d.key === 'SPD_RATE_IMPORTANT_ID');
      if (spdRate) constVal = { ...constVal, SPD_RATE_IMPORTANT_ID: parseInt(spdRate.value) };

      const spdFitness = constants.find((d) => d.key === 'SPD_DATE_FITNESS_ID');
      if (spdFitness) constVal = { ...constVal, SPD_DATE_FITNESS_ID: parseInt(spdFitness.value) };

      const spdAcademy = constants.find((d) => d.key === 'SPD_ACADEMY_INDEX_ID');
      if (spdAcademy) constVal = { ...constVal, SPD_ACADEMY_INDEX_ID: parseInt(spdAcademy.value) };

      const spdSport = constants.find((d) => d.key === 'SPD_SPORT_INDEX_ID');
      if (spdSport) constVal = { ...constVal, SPD_SPORT_INDEX_ID: parseInt(spdSport.value) };

      const spdHoliday = constants.find((d) => d.key === 'SPD_PUBLIC_HOLIDAY_ID');
      if (spdHoliday) constVal = { ...constVal, SPD_PUBLIC_HOLIDAY_ID: parseInt(spdHoliday.value) };

      const spdTraining = constants.find((d) => d.key === 'SPD_TRAINING_PHASE_ID');
      if (spdTraining)
        constVal = { ...constVal, SPD_TRAINING_PHASE_ID: parseInt(spdTraining.value) };

      const spdSubphase = constants.find((d) => d.key === 'SPD_SUB_PHASE_ID');
      if (spdSubphase) constVal = { ...constVal, SPD_SUB_PHASE_ID: parseInt(spdSubphase.value) };

      const spWorktime = constants.find((d) => d.key === 'SP_WORK_TIME_ID');
      if (spWorktime) constVal = { ...constVal, SP_WORK_TIME_ID: parseInt(spWorktime.value) };

      const spdIntensity = constants.find((d) => d.key === 'SPD_INTENSITY_ID');
      if (spdIntensity) constVal = { ...constVal, SPD_INTENSITY_ID: parseInt(spdIntensity.value) };

      const spdVolume = constants.find((d) => d.key === 'SPD_VOLUME_ID');
      if (spdVolume) constVal = { ...constVal, SPD_VOLUME_ID: parseInt(spdVolume.value) };

      const spdSpesific = constants.find((d) => d.key === 'SPD_SPESIFIC_ID');
      if (spdSpesific) constVal = { ...constVal, SPD_SPESIFIC_ID: parseInt(spdSpesific.value) };

      const ytpDateColor = constants.find((d) => d.key === 'YTP_DATE_COLOR');
      if (ytpDateColor) constVal = { ...constVal, YTP_DATE_COLOR: ytpDateColor.value };

      const ytpSpColor = constants.find((d) => d.key === 'YTP_SEASON_PLAN_COLOR');
      if (ytpSpColor) constVal = { ...constVal, YTP_SEASON_PLAN_COLOR: ytpSpColor.value };

      setConstantValues(constVal);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependency Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    getTableData();
  }, []);

  const print = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    setTimeout(() => {
      const doc = new jsPDF({
        orientation: 'landscape',
        format: 'a1',
      });
      const ytpTableHtml = document.getElementById('ytp-table') || '<div></div>';
      doc.html(ytpTableHtml, {
        callback: function (doc) {
          doc.save('ytp.pdf');
          hideLoading();
          setPrintMode(false);
        },
        width: 841,
        windowWidth: 3000,
        autoPaging: 'text',
      });
    }, 1000);
  };

  useEffect(() => {
    if (printMode) {
      print();
    }
  }, [printMode]);

  const contextValue: ContextType = {
    selectedYpiId,
    setSelectedYpiId,
    editedCell,
    setEditedCell,
    eventData,
    setEventData,
    setSelectedYpi,
    selectedYpi,
    activeDescSpdId,
    setActiveDescSpdId,
    constantValues,
    printMode,
    setPrintMode,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm data={ypis} />
      <div id="ytp-table" style={printMode ? { width: '3000px' } : undefined}>
        {/* <div id="ytp-table" style={{ width: '3000px' }}> */}
        <YtpHeader />
        <Ytp />
      </div>
    </Context.Provider>
  );
};
