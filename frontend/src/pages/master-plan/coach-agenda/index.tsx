import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import KeyForm from './components/KeyForm';
import { useIntl, Access } from 'umi';
import {
  CoachAgenda,
  CompetitionDetail,
  CoachAgendaSetting,
  DateSeasonPlanner,
  Ypi,
  YpiEvent,
  SeasonPlanValue,
} from '@/types/ypi';
import Header from './components/Header';
import { jsPDF } from 'jspdf';
import moment, { Moment } from 'moment';
import { FooterReport } from '@/components/FooterReport';

interface KeyData {
  master_ypi_id: number;
  ypi_name: string;
  start_ypi: string;
  end_ypi: string;
}

interface YpiSeasonPlanValue extends SeasonPlanValue {
  seasonPlan: string;
  seasonPlanDetail: string;
}

export interface YpiExtra extends Ypi {
  tanggal_mulai_moment: Moment;
  ypi_events: YpiEvent[];
  season_plan_values: YpiSeasonPlanValue[];
}

export interface ContextType {
  keyData: KeyData | undefined;
  setKeyData: React.Dispatch<React.SetStateAction<KeyData | undefined>> | (() => void);
  editedData: Partial<CoachAgenda> | undefined;
  setEditedData:
    | React.Dispatch<React.SetStateAction<Partial<CoachAgenda> | undefined>>
    | (() => void);
  getTableData: () => void;
  printMode: boolean;
  setPrintMode: React.Dispatch<React.SetStateAction<boolean>>;
  eventData: Partial<CompetitionDetail>[];
  coachAgendaData: CoachAgenda[];
}

export const Context = createContext<ContextType>({
  keyData: undefined,
  setKeyData: () => {},
  editedData: undefined,
  setEditedData: () => {},
  getTableData: () => {},
  printMode: false,
  setPrintMode: () => {},
  eventData: [],
  coachAgendaData: [],
});

export interface Data extends DateSeasonPlanner, CoachAgendaSetting, CoachAgenda {
  annual_plan_setting_id: number;
  key: number;
  activity: string;
  week_1: string;
  season_planner: string;
  isEventData: boolean;
  isEditable: boolean;
}

export default () => {
  const intl = useIntl();
  const [data, setData] = useState<Partial<Data>[]>([]);
  const [keyData, setKeyData] = useState<KeyData | undefined>();
  const [printMode, setPrintMode] = useState<boolean>(false);
  const [eventData, setEventData] = useState<Partial<CompetitionDetail>[]>([]);
  const [editedData, setEditedData] = useState<Partial<CoachAgenda> | undefined>();
  const [coachAgendaData, setCoachAgendaData] = useState<CoachAgenda[]>([]);

  const dateName: Partial<Data>[] = [
    {
      key: 0,
      season_planner: intl.formatMessage({ id: 'pages.coach-agenda.season_planner' }),
      activity: intl.formatMessage({ id: 'pages.coach-agenda.year' }),
    },
    {
      key: 1,
      season_planner: '1',
      activity: intl.formatMessage({ id: 'pages.coach-agenda.month' }),
    },
    {
      key: 2,
      season_planner: '2',
      activity: intl.formatMessage({ id: 'pages.coach-agenda.monday' }),
    },
    {
      key: 3,
      season_planner: '3',
      activity: intl.formatMessage({ id: 'pages.coach-agenda.saturday' }),
    },
    {
      key: 4,
      season_planner: '4',
      activity: intl.formatMessage({ id: 'pages.coach-agenda.week' }),
    },
    {
      key: 5,
      activity: intl.formatMessage({ id: 'commonField.event' }),
      isEventData: true,
    },
  ];

  const fetchData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const ypiRes = await get(`master_ypis/${keyData?.master_ypi_id}?full_data=true`);
    let ypi: YpiExtra = ypiRes.data;
    ypi = {
      ...ypi,
      tanggal_mulai: ypi.tanggal_mulai.substring(0, 10),
      tanggal_selesai: ypi.tanggal_selesai.substring(0, 10),
    };

    const eventRes = await get(
      `master_achievements?start=${ypi.tanggal_mulai}&end=${ypi.tanggal_selesai}`,
    );
    const eventData: CompetitionDetail[] = eventRes.data || [];
    setEventData(eventData);

    const seasonPlannerRes = await get(
      `annual_plans/seasonPlanner?start=${keyData?.start_ypi}&end=${keyData?.end_ypi}`,
    );
    const seasonPlannerData: DateSeasonPlanner[] = seasonPlannerRes.data || [];

    let newData = [...dateName];

    let yearData = { ...newData[0] };
    let monthData = { ...newData[1] };
    let mondayData = { ...newData[2] };
    let saturdayData = { ...newData[3] };
    let weekData = { ...newData[4] };
    let eventYtpData = { ...newData[5] };
    for (let i = 0; i < seasonPlannerData.length; i++) {
      const val = seasonPlannerData[i];
      yearData['week_' + (i + 1)] = val.year;
      monthData['week_' + (i + 1)] = val.month;
      mondayData['week_' + (i + 1)] = val.monday;
      saturdayData['week_' + (i + 1)] = val.saturday;
      weekData['week_' + (i + 1)] = val.week;
      eventYtpData['week_' + (i + 1)] = '';
    }

    const startPeriod = moment(keyData?.start_ypi);

    ypi.ypi_events?.forEach((evt) => {
      if (!evt.active) return;

      const event = eventData.find((dd) => dd.id === evt.master_achievement_id);
      if (!event) return;

      const startEvent = moment(event.tanggal_mulai);
      const endEvent = moment(event.tanggal_akhir);
      let startWeekEvent = startEvent.diff(startPeriod, 'weeks');
      let endWeekEvent = endEvent.diff(startPeriod, 'weeks');

      if (startWeekEvent < 0) startWeekEvent = 0;

      if (endWeekEvent > seasonPlannerData.length) endWeekEvent = seasonPlannerData.length;

      for (let i = startWeekEvent; i <= endWeekEvent; i++) {
        eventYtpData['week_' + (i + 1)] = event.nama_event;
      }
    });

    newData[0] = yearData;
    newData[1] = monthData;
    newData[2] = mondayData;
    newData[3] = saturdayData;
    newData[4] = weekData;
    newData[5] = eventYtpData;

    newData = newData.map((d) => ({ ...d, isEditable: false }));

    // Add Coach Agenda Setting row
    const coachAgendaSettingRes = await get(
      'annual_plan_settings?master_ypi_id=' + keyData?.master_ypi_id,
    );
    const coachAgendaSettingData: CoachAgendaSetting[] = coachAgendaSettingRes.data || [];
    const addedRow: Partial<Data>[] = [];
    for (let i = 0; i < coachAgendaSettingData.length; i++) {
      const val = coachAgendaSettingData[i];
      addedRow.push({
        color: val.color,
        master_ypi_id: val.master_ypi_id,
        annual_plan_setting_id: val.id,
        season_planner: val.group_activity?.group as string,
        activity: val.description,
        isEditable: true,
      });
    }

    const periodizationIdRes = await get('constants?key=SP_PERIODIZATION_ID');
    const periodizationId = parseInt(periodizationIdRes.data[0].value);

    const periodizationData = [...ypi.season_plan_values].filter(
      (d) => d.season_plan_id === periodizationId,
    );

    const periodization: Partial<Data>[] = periodizationData.map((d) => ({
      ...d,
      color: d.bg_color,
      season_planner: d.seasonPlan,
      activity: d.seasonPlanDetail,
      isEditable: false,
    }));

    // Fill Coach Agenda Data or empty
    const coachAgendaRes = await get('annual_plans?master_ypi_id=' + keyData?.master_ypi_id);
    const caData: CoachAgenda[] = coachAgendaRes.data || [];
    setCoachAgendaData(caData);
    for (let index = 0; index < addedRow.length; index++) {
      const currentRow = { ...addedRow[index] };
      for (let i = 0; i < seasonPlannerData.length; i++) {
        const matchCoachAgenda = caData.find(
          (d) => d.annual_plan_setting_id == currentRow.annual_plan_setting_id && d.week == i + 1,
        );
        currentRow['week_' + (i + 1)] = matchCoachAgenda?.plan_description || undefined;
      }
      addedRow[index] = currentRow;
    }

    newData = [...newData, ...periodization, ...addedRow].map((d, i) => ({ ...d, key: i }));
    setData(newData);
    hideLoading();
  };

  useEffect(() => {
    if (keyData) {
      fetchData();
    }
  }, [keyData]);

  const print = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    setTimeout(() => {
      const doc = new jsPDF({
        orientation: 'landscape',
        format: 'a1',
      });
      const ytpTableHtml = document.getElementById('print-area') || '<div></div>';
      doc.html(ytpTableHtml, {
        callback: function (doc) {
          doc.save('coach-agenda.pdf');
          hideLoading();
          setPrintMode(false);
        },
        width: 841,
        margin: 16,
        windowWidth: 3000,
        autoPaging: 'text',
      });
    }, 1000);
  };

  useEffect(() => {
    if (printMode) {
      print();
    }
  }, [printMode]);

  const getTableData = () => {
    fetchData();
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    keyData,
    setKeyData,
    printMode,
    setPrintMode,
    eventData,
    coachAgendaData,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm />
      <Access accessible={keyData ? true : false}>
        <div id="print-area" style={printMode ? { width: '3000px' } : undefined}>
          <Header />
          <Table data={data as Data[]} />
          {printMode && <FooterReport />}
        </div>
        <Access accessible={editedData ? true : false}>
          <div style={{ height: '50vh' }} />
        </Access>
      </Access>
      <Form />
    </Context.Provider>
  );
};
