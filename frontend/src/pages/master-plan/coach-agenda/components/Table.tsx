import { Table, TableColumnsType, TableColumnType } from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context, Data } from '../index';
import styles from './table.less';

interface CustomRowProps {
  index: number;
}

interface CustomCell {
  record: Data;
  children: React.ReactNode;
  plan_description: string;
  week: number;
}

const CustomRow: React.FC<CustomRowProps> = ({ index, ...props }) => {
  return <tr {...props} />;
};

const CustomCell: React.FC<CustomCell> = ({
  record,
  children,
  plan_description,
  week,
  ...props
}) => {
  const { setEditedData, editedData, eventData, coachAgendaData } = useContext(Context);
  const [isEdited, setIsEdited] = useState<boolean>(false);
  const isClickable = record && record.color;

  if (record && record.isEventData) {
    const event = eventData.find((d) => d.nama_event == record['week_' + week]);

    return (
      <td {...props} style={{ background: event?.color }}>
        {children}
      </td>
    );
  }

  if (record && !record.isEditable) {
    return <td {...props}>{children}</td>;
  }

  if (!isClickable) {
    return <td {...props}>{children}</td>;
  }

  useEffect(() => {
    if (editedData && record) {
      // return;
      if (
        editedData.annual_plan_setting_id === record.annual_plan_setting_id &&
        editedData.week == week
      ) {
        setIsEdited(true);
      } else {
        setIsEdited(false);
      }
    } else if (!editedData) setIsEdited(false);
  }, [editedData]);

  const handleClick = () => {
    if (isClickable) {
      setEditedData({
        master_ypi_id: record.master_ypi_id,
        annual_plan_setting_id: record.annual_plan_setting_id,
        week,
        plan_description,
      });
    }
  };

  const matchData = coachAgendaData.find(
    (d) => d.annual_plan_setting_id === record.annual_plan_setting_id && week === d.week,
  );
  let bgColor = record ? record.color : '#ffffff';
  if (matchData) bgColor = matchData.color;

  return (
    <td
      onClick={handleClick}
      {...props}
      style={{ background: isEdited ? '#ffffff' : bgColor, cursor: 'pointer' }}
    >
      {children}
    </td>
  );
};

export default ({ data }: { data: Data[] }) => {
  const { printMode } = useContext(Context);

  let weekColumns = [];
  let value: string[] = [];
  let index: number = 0;
  for (let i = 1; i <= 53; i++) {
    weekColumns.push({
      colSpan: 0,
      rowSpan: 0,
      dataIndex: 'week_' + i,
      onCell: (val: Data, indexRow: number) => {
        const currentValue = val['week_' + i];
        const nextValue = val['week_' + (i + 1)];
        let props = {};
        if (val.annual_plan_setting_id)
          return { record: val, plan_description: currentValue, week: i };

        if (currentValue !== '') {
          if (index !== indexRow) {
            index = indexRow;
            value = [];
          }

          value.push(currentValue);
          if (currentValue == nextValue) {
            props = { colSpan: 0 };
          } else {
            props = { colSpan: value.length };
            value = [];
          }
        }
        return { ...props, record: val, plan_description: currentValue, week: i };
      },
    });
  }

  const columns: TableColumnsType<Data> = [
    {
      title: 'season_planner',
      fixed: printMode ? false : 'left',
      dataIndex: 'season_planner',
      colSpan: 0,
      onCell: (_, index) => {
        let props = {};
        if (index != undefined) {
          let prevValue = 'undefined';
          if (data[index - 1]) {
            prevValue = data[index - 1].season_planner as string;
          }
          const currentValue = data[index].season_planner;

          let rowSpan = 1;
          if (currentValue != prevValue) {
            for (let i = index; i < data.length; i++) {
              let nextValue = undefined;
              if (data[i + 1]) nextValue = data[i + 1].season_planner;
              if (data[i].season_planner != nextValue) {
                break;
              } else {
                rowSpan++;
              }
            }
          }
          if (currentValue == prevValue) {
            rowSpan = 0;
          }

          props = { rowSpan };
        }
        if (index === 0) {
          props = { rowSpan: 6 };
        } else if ((index as number) < 6) {
          props = { rowSpan: 0 };
        }
        return props;
      },
    },
    {
      colSpan: 0,
      rowSpan: 0,
      fixed: printMode ? false : 'left',
      dataIndex: 'activity',
      onCell: (record: Data) => {
        return { style: { background: record.color } };
      },
    },
    ...(weekColumns as TableColumnType<Data>[]),
  ];

  const components = {
    body: {
      row: CustomRow,
      cell: CustomCell,
    },
  };

  return (
    <Table
      onRow={(record: Data, rowIndex) => {
        return { style: { background: record.color } };
      }}
      components={components}
      bordered
      size="small"
      columns={columns}
      scroll={printMode ? undefined : { x: true }}
      dataSource={data}
      pagination={false}
      rowClassName={styles.cell}
      className={styles.table}
    />
  );
};
