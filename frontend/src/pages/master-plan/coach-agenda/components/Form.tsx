import { Button, Col, Drawer, Form, Input, message, Row, Space } from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { CoachAgenda } from '@/types/ypi';
import { create } from '@/services/ypi/api';
import { DeleteOutlined } from '@ant-design/icons';

export default () => {
  const { editedData, setEditedData, getTableData } = useContext(Context);
  const [form] = Form.useForm<CoachAgenda>();
  const intl = useIntl();
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    setEditedData(undefined);
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue({ ...editedData });
      openModal();
    }
  }, [editedData]);

  const handleSave = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const values = form.getFieldsValue();
      if (!values.plan_description) values.plan_description = '';
      await create('annual_plans', values);
      getTableData();
      closeModal();
    } catch (error) {
      if ((error as any).errorFields) return;
      console.log(error);
      message.error(
        intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Coach Agenda' }),
      );
    }
    hideLoading();
  };

  const handleDelete = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const values = form.getFieldsValue();
      await create('annual_plans/condition_delete', values);
      getTableData();
      closeModal();
    } catch (error) {
      if ((error as any).errorFields) return;
      console.log(error);
      message.error(
        intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Coach Agenda' }),
      );
    }
    hideLoading();
  };

  const extra = (
    <Space>
      <Button key="cancel" onClick={closeModal}>
        <FormattedMessage id="crud.cancel" />
      </Button>
      <Button key="next" type="primary" htmlType="button" onClick={handleSave}>
        <FormattedMessage id="crud.save" />
      </Button>
      <Button type="primary" danger icon={<DeleteOutlined />} onClick={handleDelete}>
        <FormattedMessage id="crud.delete" />
      </Button>
    </Space>
  );

  return (
    <div>
      <Drawer
        title={<FormattedMessage id="crud.editData" />}
        visible={modalVisible}
        placement="bottom"
        mask={false}
        onClose={closeModal}
        headerStyle={{ padding: '10px 20px' }}
        extra={extra}
        height="10rem"
      >
        <Form name="form" form={form} layout="horizontal">
          <Form.Item name="master_ypi_id" hidden />
          <Form.Item name="annual_plan_setting_id" hidden />
          <Form.Item name="week" hidden />
          <Form.Item name="id" hidden />

          <Row gutter={8}>
            <Col span={18}>
              <Form.Item
                name="plan_description"
                label={<FormattedMessage id="pages.coach-agenda.value" />}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={6}>
              <Form.Item name="color" label={<FormattedMessage id="commonField.color" />}>
                <Input type="color" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </div>
  );
};
