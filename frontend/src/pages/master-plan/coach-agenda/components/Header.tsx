import { Context } from '../index';
import { useContext } from 'react';
import { Card, Row, Col, Form, Input } from 'antd';
import { FormattedMessage } from 'umi';
import styles from './header.less';

export default () => {
  const { keyData } = useContext(Context);

  return (
    <Card>
      <Row gutter={12}>
        <Col>
          <span className={styles.header}>Coach Agenda</span>
        </Col>
      </Row>
      <Row>
        <Col>
          <div style={{ width: '20rem' }}>
            <Row>
              <Col span={6}>
                <span>YPI</span>
              </Col>
              <Col>
                <span>: {keyData?.ypi_name}</span>
              </Col>
            </Row>
            <Row>
              <Col span={6}>
                <span>
                  <FormattedMessage id="commonField.schedule" />
                </span>
              </Col>
              <Col>
                <span>
                  : {keyData?.start_ypi} <FormattedMessage id="commonField.to" /> {keyData?.end_ypi}
                </span>
              </Col>
            </Row>
          </div>
        </Col>
      </Row>
    </Card>
  );
};
