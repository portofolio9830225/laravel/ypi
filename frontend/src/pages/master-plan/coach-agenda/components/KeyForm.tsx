import { Form, Input, Card, Row, Col, message, Select, Button } from 'antd';
import { Context } from '../index';
import { useContext, useEffect, useState } from 'react';
import { useIntl } from 'umi';
import { MedicalRecord, Ypi } from '@/types/ypi';
const { Option } = Select;
import { get } from '@/services/ypi/api';
import { PrinterOutlined } from '@ant-design/icons';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { setKeyData, setPrintMode, keyData } = useContext(Context);
  const [ypiData, setYpiData] = useState<Ypi[]>([]);

  const lblYpi = intl.formatMessage({ id: 'pages.medical-record.ypi' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    try {
      const ypiRes = await get('master_ypis');
      setYpiData(ypiRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const onValuesChange = (changedValues: any, values: MedicalRecord) => {
    if (changedValues.master_ypi_id) {
      const selectedYpiData = ypiData.find((d) => d.id == changedValues.master_ypi_id);
      const startDate = selectedYpiData?.tanggal_mulai.substring(0, 10);
      const endDate = selectedYpiData?.tanggal_selesai.substring(0, 10);
      form.setFieldsValue({
        start_date: startDate,
        end_date: endDate,
      });
      setKeyData({
        master_ypi_id: changedValues.master_ypi_id,
        start_ypi: startDate as string,
        end_ypi: endDate as string,
        ypi_name: selectedYpiData?.keterangan || '',
      });
    }
  };

  const handlePrint = () => {
    setPrintMode(true);
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
        <Row gutter={12}>
          <Col span={12}>
            <Form.Item name="master_ypi_id" label={lblYpi} rules={getRequiredRule(lblYpi)}>
              <Select>
                {ypiData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.keterangan}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              name="start_date"
              label={intl.formatMessage({ id: 'pages.medical-record.time_schedule' })}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item
              name="end_date"
              label={intl.formatMessage({ id: 'pages.medical-record.end' })}
            >
              <Input disabled />
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item label=" ">
              <Button
                onClick={handlePrint}
                icon={<PrinterOutlined />}
                disabled={keyData ? false : true}
              >
                Print
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
