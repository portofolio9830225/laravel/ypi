import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { create, get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import KeyForm from './components/KeyForm';
import RmCalculation from './components/RmCalculation';
import { useIntl } from 'umi';
import { StrengthConditioningData } from '@/types/ypi';
import DescriptionModal from '@/components/DescriptionModal';

interface KeyData extends StrengthConditioningData {
  openRmCalculation: boolean;
}

export interface ContextType {
  keyData: Partial<KeyData> | undefined;
  editedData: Partial<StrengthConditioningData> | null;
  setEditedData:
    | React.Dispatch<React.SetStateAction<Partial<StrengthConditioningData> | null>>
    | (() => void);
  getTableData: () => void;
  setKeyData: React.Dispatch<React.SetStateAction<Partial<KeyData> | undefined>> | (() => void);
  setHelp: React.Dispatch<React.SetStateAction<string | undefined>>;
  setHelpVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Context = createContext<ContextType>({
  keyData: undefined,
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  setKeyData: () => {},
  setHelp: () => {},
  setHelpVisible: () => {},
});

export default () => {
  const [data, setData] = useState<Partial<StrengthConditioningData>[]>([]);
  const [keyData, setKeyData] = useState<Partial<KeyData> | undefined>();
  const [editedData, setEditedData] = useState<Partial<StrengthConditioningData> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.data-management.strength-conditioning' }),
  );
  const [help, setHelp] = useState<string>();
  const [helpVisible, setHelpVisible] = useState<boolean>(false);

  useEffect(() => {
    if (keyData) {
      getTableData();
    }
  }, [keyData]);

  const handleHelpCancel = () => {
    setHelpVisible(false);
  };

  const handleHelpOk = async (html: string) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      await create('constants/help/SC_PROGRAM', { value: html });
      handleHelpCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dataRes = await get(
        `strength_conditioning_datas?pemain_id=${keyData?.pemain_id}&gym_type=${keyData?.gym_type}&master_ypi_id=${keyData?.master_ypi_id}&ypi_end=${keyData?.ypi_end}&ypi_start=${keyData?.ypi_start}`,
      );
      setData(
        (dataRes.data as StrengthConditioningData[]).map((d) => {
          return {
            ...d,
            key: d.id.toString(),
            tanggal: d.tanggal.substring(0, 10),
          };
        }),
      );
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { moduleName }));
    }
    hideLoading();
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    keyData,
    setKeyData,
    setHelp,
    setHelpVisible,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm />
      <Table data={data as StrengthConditioningData[]} />
      <div style={{ height: '50vh', width: '100%' }} />
      <Form />
      <RmCalculation />
      <DescriptionModal
        visible={helpVisible}
        value={help}
        onCancel={handleHelpCancel}
        onOk={handleHelpOk}
      />
    </Context.Provider>
  );
};
