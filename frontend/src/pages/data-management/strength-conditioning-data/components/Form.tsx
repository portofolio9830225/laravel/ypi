import {
  Form,
  Button,
  message,
  Space,
  Input,
  Drawer,
  Row,
  Col,
  Select,
  InputNumber,
  Divider,
  Typography,
} from 'antd';
import { Context } from '../index';
import { get, update } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { StrengthConditioningData, SubScSetting } from '@/types/ypi';
import { PlusOutlined } from '@ant-design/icons';

const { Option } = Select;

const getLocaleKey = (fieldName: string) => {
  return 'pages.strength-conditioning-data.' + fieldName;
};

const calculateTutExercise = (data: StrengthConditioningData): number => {
  let reps = 0;
  for (let i = 1; i <= 6; i++) {
    reps += +data['rep_' + i];
  }
  return reps * +data.tut_rep;
};

const calculateTonnage = (data: StrengthConditioningData): number => {
  let tonnnage = 0;
  for (let i = 1; i <= 6; i++) {
    tonnnage += +data['weight_' + i] * +data['rep_' + i];
  }
  return tonnnage;
};

const calculateAverageKg = (data: StrengthConditioningData): number => {
  const avg: number[] = [];
  for (let i = 1; i <= 6; i++) {
    if (data['weight_' + i]) avg.push(+data['weight_' + i]);
  }
  const total = avg.reduce((a, b) => a + b, 0);
  return total / avg.length;
};

const calculatePhysicalLoad = (data: StrengthConditioningData): number => {
  let load = 0;
  let totalReps = 0;
  for (let i = 1; i <= 6; i++) {
    totalReps += +data['rep_' + i];
  }
  load = totalReps * data.tut_rep * data.tonnage * (data.average / data.actual_1rm);
  return load;
};

const calculateAvg1Rm = (data: StrengthConditioningData): number => {
  let avg = 0;
  if (data.average && data.actual_1rm) {
    avg = Math.round((data.average / data.actual_1rm) * 100);
  }
  return avg;
};

export default () => {
  const [form] = Form.useForm<StrengthConditioningData>();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, keyData } = useContext(Context);
  const [openForm, setOpenForm] = useState<boolean>(false);
  const [commentsData, setCommentsData] = useState<SubScSetting[]>([]);
  const [customComment, setCustomComment] = useState<string>('');

  const onFinish = async () => {
    try {
      const values = await form.validateFields();
      values.tut_exercise = calculateTutExercise(values);
      values.tonnage = calculateTonnage(values);
      values.average = calculateAverageKg(values);
      values.pemain_id = keyData?.pemain_id as number;
      values.master_ypi_id = keyData?.master_ypi_id as number;
      values.gym_type = keyData?.gym_type as number;
      values.physical_load = calculatePhysicalLoad(values);
      values.average_1rm = calculateAvg1Rm(values);

      await update('strength_conditioning_datas', values.id, values);
      message.success(intl.formatMessage({ id: 'message.succesSave' }, { moduleName: 'Data' }));
      getTableData();
      onCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
      setOpenForm(true);
    }
  }, [editedData]);

  useEffect(() => {
    if (keyData?.openRmCalculation) {
      onCancel();
    }
  }, [keyData]);

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
    setOpenForm(false);
  };

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const addComment = (e: any) => {
    e.preventDefault();
    // setItems([...items, name || `New item ${index++}`]);
    const newComment = { excercise_desc: customComment } as SubScSetting;
    setCommentsData([...commentsData, newComment]);
    setCustomComment('');
  };

  const onCommentChange = (event: any) => {
    setCustomComment(event.target.value);
  };

  const extra = (
    <Space>
      <Button type="primary" htmlType="button" onClick={onFinish}>
        {intl.formatMessage({ id: 'crud.save' })}
      </Button>
      <Button
        type="primary"
        htmlType="button"
        onClick={onCancel}
        style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
      >
        {intl.formatMessage({ id: 'crud.cancel' })}
      </Button>
    </Space>
  );

  const fetchDependenciesData = async () => {
    try {
      const commentRes = await get('sub_exercises?gym_type_id=' + keyData?.gym_type);
      setCommentsData(commentRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, [keyData]);

  const onValuesChange = (
    changedValues: StrengthConditioningData,
    values: StrengthConditioningData,
  ) => {
    if (changedValues.comments) {
      const selectedComment = commentsData.find((d) => d.excercise_desc == changedValues.comments);
      form.setFieldsValue({ tut_rep: selectedComment?.tut_per_rep });
    }
  };

  return (
    <>
      <Drawer
        title={
          <span style={{ color: 'white' }}>{intl.formatMessage({ id: 'crud.editData' })}</span>
        }
        placement="bottom"
        visible={openForm}
        mask={false}
        closable={false}
        onClose={onCancel}
        headerStyle={{ padding: '6px 10px', backgroundColor: '#00B4ED' }}
        height="40%"
        extra={extra}
      >
        <Form
          form={form}
          onValuesChange={onValuesChange}
          name="form"
          onFinish={onFinish}
          layout="vertical"
          size="small"
        >
          <Form.Item name="id" hidden />
          <Form.Item name="week" hidden />

          <Row gutter={12}>
            <Col span={4}>
              <Form.Item
                name="comments"
                label={<FormattedMessage id={getLocaleKey('comments')} />}
                rules={getRequiredRule('comments')}
              >
                <Select
                  listHeight={100}
                  placement="topLeft"
                  dropdownRender={(menu) => (
                    <>
                      {menu}
                      <Divider style={{ margin: '8px 0' }} />
                      <Space align="center" style={{ padding: '0 8px 4px' }}>
                        <Input
                          placeholder="Please enter item"
                          value={customComment}
                          onChange={onCommentChange}
                        />
                        <Typography.Link onClick={addComment} style={{ whiteSpace: 'nowrap' }}>
                          <PlusOutlined /> Add item
                        </Typography.Link>
                      </Space>
                    </>
                  )}
                >
                  {commentsData.map((d) => (
                    <Option key={d.id} value={d.excercise_desc}>
                      {d.excercise_desc}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="tut_rep"
                label={<FormattedMessage id={getLocaleKey('tut_per_reps')} />}
                rules={getRequiredRule('tut_per_reps')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="predicted_1rm"
                label={<FormattedMessage id={getLocaleKey('rest_interval')} />}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="average_1rm_predict"
                label={<FormattedMessage id={getLocaleKey('rest_period')} />}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="actual_1rm"
                label={<FormattedMessage id={getLocaleKey('actual_1rm')} />}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
          </Row>

          <Row gutter={12}>
            <Col span={2}>
              <Form.Item name="weight_1" label={<FormattedMessage id={getLocaleKey('set1w')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="rep_1" label={<FormattedMessage id={getLocaleKey('set1r')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="weight_2" label={<FormattedMessage id={getLocaleKey('set2w')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="rep_2" label={<FormattedMessage id={getLocaleKey('set2r')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="weight_3" label={<FormattedMessage id={getLocaleKey('set3w')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="rep_3" label={<FormattedMessage id={getLocaleKey('set3r')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="weight_4" label={<FormattedMessage id={getLocaleKey('set4w')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="rep_4" label={<FormattedMessage id={getLocaleKey('set4r')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="weight_5" label={<FormattedMessage id={getLocaleKey('set5w')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="rep_5" label={<FormattedMessage id={getLocaleKey('set5r')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="weight_6" label={<FormattedMessage id={getLocaleKey('set6w')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={2}>
              <Form.Item name="rep_6" label={<FormattedMessage id={getLocaleKey('set6r')} />}>
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
    </>
  );
};
