import {
  Form,
  Button,
  message,
  Space,
  Input,
  Drawer,
  Row,
  Col,
  Select,
  InputNumber,
  Divider,
  Typography,
} from 'antd';
import { Context } from '../index';
import { create, get, update } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { RmCalculation } from '@/types/ypi';

const { Option } = Select;

const getLocaleKey = (fieldName: string) => {
  return 'pages.strength-conditioning-data.' + fieldName;
};

const weeks: number[] = [];
for (let i = 1; i <= 53; i++) {
  weeks.push(i);
}

export default () => {
  const [form] = Form.useForm<RmCalculation>();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, keyData } = useContext(Context);
  const [openForm, setOpenForm] = useState<boolean>(false);

  const onFinish = async () => {
    try {
      const values = await form.validateFields();

      await create('strength_conditioning_datas/updateRm', {
        ...values,
        pemain_id: keyData?.pemain_id,
        gym_type: keyData?.gym_type,
        master_ypi_id: keyData?.master_ypi_id,
      });
      message.success(intl.formatMessage({ id: 'message.succesSave' }, { moduleName: 'Data' }));
      getTableData();
      onFormClose();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const calculateRm = (values: RmCalculation) => {
    const RM1 = (100 * values.weight) / (102.78 - 2.78 * values.reps);
    const RM2 = (1 + 0.0333 * values.reps) * values.weight;
    const RM3 = (100 * values.weight) / (101.3 - 2.67123 * values.reps);
    const RM4 = values.weight * Math.pow(values.reps, 0.1);
    const RM5 = values.weight * (1 + 0.025 * values.reps);

    const rmArray: number[] = [RM1, RM2, RM3, RM4, RM5];

    const totalRm = rmArray.reduce((prevValue, currentValue) => prevValue + currentValue, 0);
    const RMMean = totalRm / 5;
    const RMRange1 = Math.min(...rmArray);
    const RMRange2 = Math.max(...rmArray);

    form.setFieldsValue({
      mean: +RMMean.toFixed(2),
      range_max: +RMRange2.toFixed(2),
      range_min: +RMRange1.toFixed(2),
      c1: +RM1.toFixed(2),
      c2: +RM2.toFixed(2),
      c3: +RM3.toFixed(2),
      c4: +RM4.toFixed(2),
      c5: +RM5.toFixed(2),
      rm_calculated: Math.round(RMMean),
    });
  };

  const onFormClose = () => {
    form.resetFields();
    setEditedData(null);
    setOpenForm(false);
  };

  const onFormOpen = () => {
    setOpenForm(true);
  };

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const extra = (
    <Space>
      <Button type="primary" htmlType="button" onClick={onFinish}>
        {<FormattedMessage id={getLocaleKey('process')} />}
      </Button>
      <Button
        type="primary"
        htmlType="button"
        onClick={onFormClose}
        style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
      >
        {intl.formatMessage({ id: 'crud.cancel' })}
      </Button>
    </Space>
  );

  useEffect(() => {
    if (keyData?.openRmCalculation) onFormOpen();
  }, [keyData]);

  useEffect(() => {
    if (editedData) onFormClose();
  }, [editedData]);

  const onValuesChange = (changedValues: RmCalculation, values: RmCalculation) => {
    if (values.weight && values.reps && values.week) {
      calculateRm(values);
    }
  };

  return (
    <>
      <Drawer
        title={<FormattedMessage id={getLocaleKey('1rm_calc')} />}
        visible={openForm}
        placement="bottom"
        mask={false}
        onClose={onFormClose}
        headerStyle={{ padding: '10px 20px' }}
        height="35%"
        extra={extra}
      >
        <Form
          form={form}
          onValuesChange={onValuesChange}
          name="form"
          onFinish={onFinish}
          layout="vertical"
        >
          <Form.Item name="id" hidden />
          <Form.Item name="week" hidden />
          <Form.Item name="rm_calculated" hidden />

          <Row gutter={12}>
            <Col span={4}>
              <Form.Item
                name="week"
                label={<FormattedMessage id={getLocaleKey('week_test')} />}
                rules={getRequiredRule('week_test')}
              >
                <Select listHeight={128}>
                  {weeks.map((d) => (
                    <Option key={d} value={d}>
                      {d}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="weight"
                label={<FormattedMessage id={getLocaleKey('weight')} />}
                rules={getRequiredRule('weight')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="reps"
                label={<FormattedMessage id={getLocaleKey('reps')} />}
                rules={getRequiredRule('reps')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item name="mean" label={<FormattedMessage id={getLocaleKey('mean')} />}>
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="range_min"
                label={<FormattedMessage id={getLocaleKey('range_min')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                name="range_max"
                label={<FormattedMessage id={getLocaleKey('range_max')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
          </Row>
          <Divider plain>
            <FormattedMessage id={getLocaleKey('player_calculated_rm')} />
          </Divider>
          <Row gutter={12}>
            <Col span={4}>
              <Form.Item
                extra={<FormattedMessage id={getLocaleKey('c1_sub')} />}
                name="c1"
                label={<FormattedMessage id={getLocaleKey('c1_label')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                extra={<FormattedMessage id={getLocaleKey('c2_sub')} />}
                name="c2"
                label={<FormattedMessage id={getLocaleKey('c2_label')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                extra={<FormattedMessage id={getLocaleKey('c3_sub')} />}
                name="c3"
                label={<FormattedMessage id={getLocaleKey('c3_label')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                extra={<FormattedMessage id={getLocaleKey('c4_sub')} />}
                name="c4"
                label={<FormattedMessage id={getLocaleKey('c4_label')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>

            <Col span={4}>
              <Form.Item
                extra={<FormattedMessage id={getLocaleKey('c5_sub')} />}
                name="c5"
                label={<FormattedMessage id={getLocaleKey('c5_label')} />}
              >
                <InputNumber disabled style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
      {openForm && <div style={{ height: '50vh', width: '100%' }} />}
    </>
  );
};
