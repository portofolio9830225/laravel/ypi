import { Table, TableColumnsType, Button, Space, Tooltip } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { Context } from '../index';
import { useIntl } from 'umi';
import styles from './table.less';
import { StrengthConditioningData } from '@/types/ypi';

const getLocaleKey = (fieldName: string) => {
  return 'pages.strength-conditioning-data.' + fieldName;
};

export default ({ data }: { data: StrengthConditioningData[] }) => {
  const intl = useIntl();
  const { setEditedData, getTableData, editedData } = useContext(Context);

  const onEdit = (record: StrengthConditioningData) => {
    setEditedData(record);
  };

  const columns = [
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning-data.date' }),
      dataIndex: 'tanggal',
      key: 'tanggal',
      fixed: 'left',
      ellipsis: true,
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning-data.day' }),
      dataIndex: 'hari',
      key: 'hari',
      editable: true,
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning-data.comments' }),
      dataIndex: 'comments',
      key: 'comments',
      ellipsis: true,
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning-data.set1' }),
      editable: true,
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.weight' }),
          dataIndex: 'weight_1',
          key: 'weight_1',
          editable: true,
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.reps' }),
          dataIndex: 'rep_1',
          key: 'rep_1',
          editable: true,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('set2') }),
      editable: true,
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.weight' }),
          dataIndex: 'weight_2',
          key: 'weight_2',
          editable: true,
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.reps' }),
          dataIndex: 'rep_2',
          key: 'rep_2',
          editable: true,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('set3') }),
      editable: true,
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.weight' }),
          dataIndex: 'weight_3',
          key: 'weight_3',
          editable: true,
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.reps' }),
          dataIndex: 'rep_3',
          key: 'rep_3',
          editable: true,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('set4') }),
      editable: true,
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.weight' }),
          dataIndex: 'weight_4',
          key: 'weight_4',
          editable: true,
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.reps' }),
          dataIndex: 'rep_4',
          key: 'rep_4',
          editable: true,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('set5') }),
      editable: true,
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.weight' }),
          dataIndex: 'weight_5',
          key: 'weight_5',
          editable: true,
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.reps' }),
          dataIndex: 'rep_5',
          key: 'rep_5',
          editable: true,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('set6') }),
      editable: true,
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.weight' }),
          dataIndex: 'weight_6',
          key: 'weight_6',
          editable: true,
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning-data.reps' }),
          dataIndex: 'rep_6',
          key: 'rep_6',
          editable: true,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('tut_per_reps') }),
      dataIndex: 'tut_rep',
      key: 'tut_rep',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('tut_per_exer') }),
      dataIndex: 'tut_exercise',
      key: 'tut_exercise',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('tonnage') }),
      dataIndex: 'tonnage',
      key: 'tonnage',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('avg_kg') }),
      dataIndex: 'average',
      key: 'average',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('physical_load') }),
      dataIndex: 'physical_load',
      key: 'physical_load',
      render: (value: number) => <>{value.toLocaleString()}</>,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('rest_interval') }),
      dataIndex: 'predicted_1rm',
      key: 'predicted_1rm',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('rest_period') }),
      dataIndex: 'average_1rm_predict',
      key: 'average_1rm_predict',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('actual_1rm') }),
      dataIndex: 'actual_1rm',
      key: 'actual_1rm',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('avg_1rm') }),
      dataIndex: 'average_1rm',
      key: 'average_1rm',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('weekly_tut') }),
      dataIndex: 'weekly_tut',
      key: 'weekly_tut',
      render: (value: number) => <>{value.toLocaleString()}</>,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('weekly_tonnage') }),
      dataIndex: 'weekly_tonnage',
      key: 'weekly_tonnage',
      render: (value: number) => <>{value.toLocaleString()}</>,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('weekly_avg') }),
      dataIndex: 'weekly_average',
      key: 'weekly_average',
      render: (value: number) => <>{value.toLocaleString()}</>,
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('weekly_load') }),
      dataIndex: 'weekly_load',
      key: 'weekly_load',
      render: (value: number) => <>{value.toLocaleString()}</>,
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_: any, record: StrengthConditioningData) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="primary"
              size="small"
              icon={<EditOutlined />}
              shape="circle"
            />
          </Tooltip>
        </Space>
      ),
    },
  ];

  const rowClassName = (record: StrengthConditioningData, index: number) => {
    if (editedData?.id == record.id) {
      console.log(record);
      return styles.record_edit;
    } else {
      return undefined;
    }
  };

  return (
    <Table
      className={styles.custom_table}
      size="small"
      bordered
      rowClassName={rowClassName}
      columns={columns as TableColumnsType<StrengthConditioningData>}
      scroll={{ x: true }}
      dataSource={data}
    />
  );
};
