import {
  Form,
  Card,
  Row,
  Col,
  message,
  Select,
  Input,
  Button,
  Divider,
  Space,
  Tooltip,
} from 'antd';
import { Context } from '../index';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage, useHistory, useAccess } from 'umi';
import { StrengthConditioningData, Player, ScJointAction, Ypi } from '@/types/ypi';
import { BulbOutlined, EditOutlined } from '@ant-design/icons';
import { get } from '@/services/ypi/api';

const { Option } = Select;

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const history = useHistory();
  const access = useAccess();
  const { setKeyData, keyData, setHelp, setHelpVisible } = useContext(Context);

  // sc = StrengthConditionings
  const [playerData, setPlayerData] = useState<Player[]>([]);
  const [scJointData, setScJointData] = useState<ScJointAction[]>([]);
  const [ypiData, setYpiData] = useState<Ypi[]>([]);

  const lblPlayerName = intl.formatMessage({ id: 'pages.strength-conditioning-data.player_name' });
  const lblExerciseType = intl.formatMessage({
    id: 'pages.strength-conditioning-data.exercise_type',
  });
  const lblYpi = intl.formatMessage({ id: 'pages.strength-conditioning-data.ypi' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    try {
      let playerRes;
      if (access.coachId) {
        playerRes = await get('master_pemains?coach_id=' + access.coachId);
      } else {
        playerRes = await get('master_pemains');
      }
      setPlayerData(playerRes.data);
      const scJointRes = await get('gym_types');
      setScJointData(scJointRes.data);
      const ypiRes = await get('master_ypis');
      setYpiData(ypiRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const handleHelp = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const helpRes = await get('constants/help/SC_PROGRAM');
      setHelpVisible(true);
      setHelp(helpRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const onValuesChange = (changedValues: any, values: StrengthConditioningData) => {
    if (changedValues.master_ypi_id) {
      const selectedYpi = ypiData.find((d) => d.id == changedValues.master_ypi_id);
      form.setFieldsValue({
        ypi_start: selectedYpi?.tanggal_mulai.substring(0, 10),
        ypi_end: selectedYpi?.tanggal_selesai.substring(0, 10),
      });

      if (values.master_ypi_id && values.pemain_id && values.gym_type) {
        setKeyData({
          ...values,
          ypi_start: selectedYpi?.tanggal_mulai.substring(0, 10),
          ypi_end: selectedYpi?.tanggal_selesai.substring(0, 10),
        });
      }
    }

    if (values.master_ypi_id && values.pemain_id && values.gym_type && values.ypi_start) {
      setKeyData(values);
    }
  };

  const handleOpenJoinAction = () => {
    history.push('/setting/sc-joint-action');
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
        <Row gutter={12}>
          <Col span={4}>
            <Form.Item
              name="pemain_id"
              label={lblPlayerName}
              rules={getRequiredRule(lblPlayerName)}
            >
              <Select>
                {playerData.map((d, i) => (
                  <Option key={i} value={d.id}>
                    {d.nama_lengkap}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item
              name="gym_type"
              label={lblExerciseType}
              rules={getRequiredRule(lblExerciseType)}
            >
              <Select
                dropdownRender={(menu) => (
                  <>
                    {menu}
                    <Divider />
                    <Space style={{ padding: '0 8px 4px' }}>
                      <Button type="primary" onClick={handleOpenJoinAction} icon={<EditOutlined />}>
                        <FormattedMessage id="crud.editData" />
                      </Button>
                    </Space>
                  </>
                )}
              >
                {scJointData.map((d, i) => (
                  <Option key={i} value={d.id}>
                    {d.gym_description}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item name="master_ypi_id" label={lblYpi} rules={getRequiredRule(lblYpi)}>
              <Select>
                {ypiData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.keterangan}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>

          <Col span={3}>
            <Form.Item name="ypi_start" label={<FormattedMessage id="commonField.startFrom" />}>
              <Input disabled />
            </Form.Item>
          </Col>

          <Col span={3}>
            <Form.Item name="ypi_end" label={<FormattedMessage id="commonField.end" />}>
              <Input disabled />
            </Form.Item>
          </Col>

          <Col span={4}>
            <Form.Item label=" ">
              <Button
                disabled={!keyData}
                onClick={() => setKeyData({ ...keyData, openRmCalculation: true })}
              >
                <FormattedMessage id="pages.strength-conditioning-data.1rm_calc" />
              </Button>
            </Form.Item>
          </Col>

          <Col span={2}>
            <Form.Item label=" ">
              <Tooltip title="Help">
                <Button
                  onClick={handleHelp}
                  size="small"
                  type="primary"
                  shape="circle"
                  icon={<BulbOutlined />}
                />
              </Tooltip>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
