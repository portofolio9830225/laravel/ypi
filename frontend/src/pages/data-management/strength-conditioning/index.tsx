import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { create, get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import Form2 from './components/Form2';
import KeyForm from './components/KeyForm';
import { useIntl, history } from 'umi';
import {
  StrengthConditioning,
  StrengthConditionPlayer,
  Player,
  StrengthConditionValue,
} from '@/types/ypi';
import { LeftOutlined } from '@ant-design/icons';

interface KeyData extends StrengthConditioning {
  isNewData: boolean;
}

export interface ContextType {
  keyData: Partial<StrengthConditioning> | undefined;
  editedData: Partial<StrengthConditionPlayer> | null;
  setEditedData:
    | React.Dispatch<React.SetStateAction<Partial<StrengthConditionPlayer> | null>>
    | (() => void);
  editedData2: Partial<StrengthConditionPlayer> | null;
  setEditedData2:
    | React.Dispatch<React.SetStateAction<Partial<StrengthConditionPlayer> | null>>
    | (() => void);
  getTableData: () => void;
  setKeyData: React.Dispatch<React.SetStateAction<Partial<KeyData> | undefined>> | (() => void);
  childData: Partial<StrengthConditionValue>[][];
  setChildData:
    | React.Dispatch<React.SetStateAction<Partial<StrengthConditionValue>[][]>>
    | (() => void);
  activeIndex: number | undefined;
  setActiveIndex: React.Dispatch<React.SetStateAction<number | undefined>> | (() => void);
  activeRecord: Partial<StrengthConditionPlayer> | undefined;
  setActiveRecord:
    | React.Dispatch<React.SetStateAction<Partial<StrengthConditionPlayer> | undefined>>
    | (() => void);
}

export const Context = createContext<ContextType>({
  keyData: undefined,
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  setKeyData: () => {},
  childData: [],
  setChildData: () => {},
  activeIndex: undefined,
  setActiveIndex: () => {},
  activeRecord: undefined,
  setActiveRecord: () => {},
  editedData2: null,
  setEditedData2: () => {},
});

export default () => {
  const [data, setData] = useState<Partial<StrengthConditionPlayer>[]>([]);
  const [childData, setChildData] = useState<Partial<StrengthConditionValue>[][]>([]);
  const [activeIndex, setActiveIndex] = useState<number | undefined>();
  const [activeRecord, setActiveRecord] = useState<Partial<StrengthConditionPlayer> | undefined>();
  const [keyData, setKeyData] = useState<Partial<KeyData> | undefined>();
  const [editedData, setEditedData] = useState<Partial<StrengthConditionPlayer> | null>(null);
  const [editedData2, setEditedData2] = useState<Partial<StrengthConditionValue> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.data-management.strength-conditioning' }),
  );

  useEffect(() => {
    console.log(editedData);
  }, [editedData]);

  useEffect(() => {
    getTableData();
  }, [keyData]);

  const getTableData = async () => {
    if (!keyData?.date_test) return;

    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const scRes = await create('strength_conditionings', {
        date_test: keyData?.date_test,
        gender: keyData?.gender,
        age_group_id: keyData?.age_group_id,
      });
      const scData: StrengthConditioning = scRes.data;
      const playerRes = await get(
        `master_pemains?kelompok_id=${keyData?.age_group_id}&gender=${keyData?.gender}`,
      );
      let scPlayerData: StrengthConditionPlayer[] = [];
      const playerData: Player[] = playerRes.data;
      const scValues: StrengthConditionValue[][] = [];

      if (playerData.length > 0) {
        await Promise.all(
          playerData.map(async (d, i) => {
            const scPlayerRes = await create('strength_condition_players', {
              strength_conditioning_id: scData.id,
              pemain_id: d.id,
            });
            const scPlayer: StrengthConditionPlayer = scPlayerRes.data;
            const scValueRes = await get(
              `strength_condition_values?strength_condition_id=${scData.id}&strength_condition_player_id=${scPlayer.id}`,
            );
            const scValue: StrengthConditionValue[] = scValueRes.data;

            scPlayerData.push({
              ...scPlayer,
              key: i.toString(),
              player_name: d.nama_lengkap,
            });

            scValues.push(scValue);
          }),
        );
      } else {
        scPlayerData = [];
      }

      setData(scPlayerData);
      setChildData(scValues);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
    }
    hideLoading();
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    keyData,
    setKeyData,
    childData,
    setChildData,
    activeIndex,
    setActiveIndex,
    activeRecord,
    setActiveRecord,
    editedData2,
    setEditedData2,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm />
      <Table data={data as StrengthConditionPlayer[]} />
      <Form />
      <Form2 />
    </Context.Provider>
  );
};
