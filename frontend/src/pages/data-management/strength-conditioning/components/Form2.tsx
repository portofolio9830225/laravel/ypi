import { Button, Modal, Form, InputNumber, message, Input, Row, Col } from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { StrengthConditionPlayer, StrengthConditionValue } from '@/types/ypi';
import { update, create } from '@/services/ypi/api';

export default () => {
  const { getTableData, editedData2, setEditedData, activeIndex, setActiveIndex, activeRecord } =
    useContext(Context);
  const [form] = Form.useForm<StrengthConditionValue>();
  const intl = useIntl();
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: (
          <FormattedMessage
            id="message.pleaseInputField"
            values={{
              fieldName: intl.formatMessage({ id: 'pages.strength-conditioning.' + fieldName }),
            }}
          />
        ),
      },
    ];
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    form.resetFields();
    setEditedData(null);
    setActiveIndex(undefined);
  };

  useEffect(() => {
    if ((activeIndex as number) >= 0) {
      form.setFieldsValue(editedData2 as StrengthConditionPlayer);
      openModal();
    }
  }, [activeIndex]);

  const handleSave = async () => {
    // console.log('form', form.getFieldsValue());
    // return;
    try {
      const values: StrengthConditionValue = await form.validateFields();

      values.strength_condition_id = activeRecord?.strength_conditioning_id as number;
      values.strength_condition_player_id = activeRecord?.id as number;

      if (values.id) {
        await update('strength_condition_values', values.id, values);
      } else {
        await create('strength_condition_values', values);
      }

      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Data' }));
      getTableData();
      closeModal();
    } catch (error) {
      console.log(error);

      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const onValuesChange = (changedValues: any, values: StrengthConditionValue) => {
    if (values.reps && values.weight) {
      const RM1 = (100 * values.weight) / (102.78 - 2.78 * values.reps);
      const RM2 = (1 + 0.0333 * values.reps) * values.weight;
      const RM3 = (100 * values.weight) / (101.3 - 2.67123 * values.reps);
      const RM4 = values.weight * Math.pow(values.reps, 0.1);
      const RM5 = values.weight * (1 + 0.025 * values.reps);

      const rmArray: number[] = [RM1, RM2, RM3, RM4, RM5];

      // const RMMean = round((array_sum(rmArray)) / 5, 2);
      const totalRm = rmArray.reduce((prevValue, currentValue) => prevValue + currentValue, 0);
      const RMMean = totalRm / 5;
      const RMRange1 = Math.min(...rmArray);
      const RMRange2 = Math.max(...rmArray);

      form.setFieldsValue({
        predict: +RMMean.toFixed(2),
        min: +RMRange1.toFixed(2),
        max: +RMRange2.toFixed(2),
      });
    }

    if (values.predict && activeRecord?.body_weight) {
      form.setFieldsValue({
        strength: +(values.predict / activeRecord?.body_weight).toFixed(2),
      });
    }

    if (values.predict && values.last) {
      form.setFieldsValue({
        diff: +(((values.predict - values.last) / values.last) * 100).toFixed(2),
      });
    }
  };

  const modalFooter = [
    <Button key="cancel" onClick={closeModal}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button key="next" type="primary" htmlType="button" onClick={handleSave}>
      <FormattedMessage id="crud.save" />
    </Button>,
  ];

  return (
    <div>
      <Modal
        title={<FormattedMessage id={editedData2 ? 'crud.editData' : 'crud.addNewData'} />}
        visible={modalVisible}
        footer={modalFooter}
        width="50%"
        onCancel={closeModal}
        centered
        bodyStyle={{ overflowY: 'scroll', maxHeight: '80%' }}
      >
        <Form
          labelCol={{ span: 10 }}
          wrapperCol={{ span: 14 }}
          name="form"
          form={form}
          layout="horizontal"
          labelWrap
          onValuesChange={onValuesChange}
        >
          <Form.Item name="id" hidden />
          <Form.Item name="strength_condition_id" hidden />
          <Form.Item name="strength_condition_player_id" hidden />

          <Form.Item
            labelCol={{ span: 5 }}
            wrapperCol={{ span: 14 }}
            name="name"
            label={<FormattedMessage id="pages.strength-conditioning.exercise" />}
            rules={getRequiredRule('exercise')}
          >
            <Input />
          </Form.Item>

          <Row gutter={12}>
            <Col span={12}>
              <Form.Item
                name="reps"
                label={<FormattedMessage id="pages.strength-conditioning.reps" />}
                rules={getRequiredRule('reps')}
              >
                <InputNumber type="number" />
              </Form.Item>

              <Form.Item
                name="weight"
                label={<FormattedMessage id="pages.strength-conditioning.weight" />}
                rules={getRequiredRule('weight')}
              >
                <InputNumber type="number" />
              </Form.Item>

              <Form.Item
                name="predict"
                label={<FormattedMessage id="pages.strength-conditioning.predicted1rm_mean" />}
              >
                <InputNumber type="number" />
              </Form.Item>

              <Form.Item
                name="min"
                label={<FormattedMessage id="pages.strength-conditioning.predicted1rm_min" />}
              >
                <InputNumber type="number" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                name="max"
                label={<FormattedMessage id="pages.strength-conditioning.predicted1rm_max" />}
              >
                <InputNumber type="number" />
              </Form.Item>

              <Form.Item
                name="strength"
                label={<FormattedMessage id="pages.strength-conditioning.strength" />}
              >
                <InputNumber type="number" />
              </Form.Item>

              <Form.Item
                name="last"
                label={<FormattedMessage id="pages.strength-conditioning.last_known" />}
              >
                <InputNumber type="number" />
              </Form.Item>

              <Form.Item
                name="diff"
                label={<FormattedMessage id="pages.strength-conditioning.difference_persent" />}
              >
                <InputNumber type="number" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Modal>
    </div>
  );
};
