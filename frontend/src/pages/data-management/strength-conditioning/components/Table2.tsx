import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { Context } from '../index';
import { useIntl, FormattedMessage } from 'umi';
import { StrengthConditionPlayer, StrengthConditionValue } from '@/types/ypi';
import { destroy } from '@/services/ypi/api';

export default (
  record: StrengthConditionPlayer,
  index: number,
  indent: number,
  expanded: boolean,
) => {
  const intl = useIntl();
  const { setActiveIndex, childData, setActiveRecord, setEditedData2, getTableData } =
    useContext(Context);

  const onEdit = (recordScValue: StrengthConditionValue) => {
    setActiveIndex(index);
    setActiveRecord(record);
    setEditedData2(recordScValue);
  };

  const onDelete = (record: StrengthConditionValue) => {
    destroy('strength_condition_values', record.id)
      .then((res) => {
        message.success(
          intl.formatMessage(
            { id: 'message.successDelete' },
            { moduleName: 'Strength Condition Value' },
          ),
        );
        getTableData();
      })
      .catch((err) => {
        message.error(
          intl.formatMessage(
            { id: 'message.errorDelete' },
            { moduleName: 'Strength Condition Value' },
          ),
        );
      });
  };

  const columns: TableColumnsType<StrengthConditionValue> = [
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.exercise' }),
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.actual_weight' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.reps' }),
          dataIndex: 'reps',
          key: 'reps',
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.weight' }),
          dataIndex: 'weight',
          key: 'weight',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.predicted' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.rm_mean' }),
          dataIndex: 'predict',
          key: 'predict',
          render: (value: number) => <>{value.toFixed(2)}</>,
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.predicted1rm' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.min' }),
          dataIndex: 'min',
          key: 'min',
        },
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.max' }),
          dataIndex: 'max',
          key: 'max',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.predicted1rm' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.strength' }),
          dataIndex: 'strength',
          key: 'strength',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.last_known' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.one_rm' }),
          dataIndex: 'last',
          key: 'last',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.last_known' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.strength-conditioning.persent' }),
          dataIndex: 'diff',
          key: 'diff',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              size="small"
              onClick={() => onEdit(record)}
              type="link"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage(
                { id: 'message.confirmDeleteMessage' },
                { moduleName: 'Data' },
              )}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="text" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  const handleNewData = () => {
    setActiveIndex(index);
    setActiveRecord(record);
  };

  const summary = () => (
    <Table.Summary fixed>
      <Table.Summary.Row>
        <Table.Summary.Cell index={0}>
          <Button type="primary" htmlType="button" onClick={handleNewData}>
            <FormattedMessage id="crud.addNewData" />
          </Button>
        </Table.Summary.Cell>
      </Table.Summary.Row>
    </Table.Summary>
  );

  //   const fetchTableData = () => {
  //     get('strength_condition_values');
  //   };
  //   useEffect(() => {
  //     get('strength_condition_values')
  //   }, [record]);

  return (
    <Table
      size="small"
      bordered
      summary={summary}
      columns={columns}
      scroll={{ x: true }}
      dataSource={childData[index] as StrengthConditionValue[]}
    />
  );
};
