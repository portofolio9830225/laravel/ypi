import { Form, DatePicker, Card, Row, Col, message, Select } from 'antd';
import { Context } from '../index';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { StrengthConditioning, GroupClassification } from '@/types/ypi';
const { Option } = Select;
import { get } from '@/services/ypi/api';
import { Moment } from 'moment';

const NEW_DATA = 'NEW_DATA';
const MALE = 'Male';
const FEMALE = 'Female';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { setKeyData, getTableData } = useContext(Context);

  // sc = StrengthConditionings
  const [dateTestData, setDateTestData] = useState<string[]>([]);
  const [groupData, setGroupData] = useState<GroupClassification[]>([]);
  const [isNewData, setIsNewData] = useState<boolean>(false);

  const lblDateTest = intl.formatMessage({ id: 'pages.strength-conditioning.date_test' });
  const lblGender = intl.formatMessage({ id: 'pages.strength-conditioning.gender' });
  const lblAgeGroup = intl.formatMessage({ id: 'pages.strength-conditioning.age_group' });
  const lblNewData = intl.formatMessage({ id: 'crud.addNewData' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    try {
      const scRes = await get('strength_conditionings?groupDate=true');
      setDateTestData(
        (scRes.data as StrengthConditioning[]).map((d) => {
          return d.date_test.substring(0, 10);
        }),
      );
      const playerRes = await get('tabel_kelompoks');
      setGroupData(playerRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const onValuesChange = (changedValues: any, values: StrengthConditioning) => {
    // console.log('changedValues', changedValues);
    // console.log('values', values);

    if (changedValues.date_test) {
      if (changedValues.date_test == NEW_DATA) {
        setIsNewData(true);
      } else {
        setIsNewData(false);
        form.setFieldsValue({ date_test_moment: undefined });
      }
    }

    let dateTest: string = values.date_test;
    if (isNewData && values.date_test_moment)
      dateTest = values.date_test_moment.format('YYYY-MM-DD');

    if (values.gender && values.age_group_id) {
      setKeyData({
        gender: values.gender,
        age_group_id: values.age_group_id,
        date_test: dateTest,
        isNewData,
      });
    }
  };

  const disabledDate = (currentDate: Moment) => {
    return dateTestData.includes(currentDate.format('YYYY-MM-DD'));
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
        <Row gutter={12}>
          <Col span={6}>
            <Form.Item name="date_test" label={lblDateTest} rules={getRequiredRule(lblDateTest)}>
              <Select>
                <Option value={NEW_DATA}>
                  <FormattedMessage id="crud.addNewData" />
                </Option>
                {dateTestData.map((d, i) => (
                  <Option key={i} value={d}>
                    {d}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              name="date_test_moment"
              label={isNewData ? lblNewData : ' '}
              rules={isNewData ? getRequiredRule(lblDateTest) : undefined}
            >
              <DatePicker
                disabledDate={disabledDate}
                style={{ width: '100%' }}
                disabled={!isNewData}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="gender" label={lblGender} rules={getRequiredRule(lblGender)}>
              <Select>
                <Option value={MALE}>
                  <FormattedMessage id="commonField.male" />
                </Option>
                <Option value={FEMALE}>
                  <FormattedMessage id="commonField.female" />
                </Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="age_group_id" label={lblAgeGroup} rules={getRequiredRule(lblAgeGroup)}>
              <Select>
                {groupData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.kelompok}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
