import { Button, Modal, Form, InputNumber, message } from 'antd';
import { useContext, useState, useEffect } from 'react';
import { Context } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { StrengthConditionPlayer } from '@/types/ypi';
import { update } from '@/services/ypi/api';

export default () => {
  const { getTableData, editedData, setEditedData } = useContext(Context);
  const [form] = Form.useForm();
  const intl = useIntl();
  const [modalVisible, setModalVisible] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: (
          <FormattedMessage
            id="message.pleaseInputField"
            values={{
              fieldName: intl.formatMessage({ id: 'pages.strength-conditioning.' + fieldName }),
            }}
          />
        ),
      },
    ];
  };

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
    form.resetFields();
    setEditedData(null);
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
      // fetchAttendanceSetting(editedData.tanggal as string);
      openModal();
    }
  }, [editedData]);

  const handleSave = async () => {
    // console.log('form', form.getFieldsValue());
    // return;
    try {
      const values: StrengthConditionPlayer = await form.validateFields();

      await update('strength_condition_players', values.id, values);
      message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Data' }));
      getTableData();
      closeModal();
    } catch (error) {
      console.log(error);

      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  // useEffect(() => {
  //   get(
  //     `setting_presences?kelompok_id=${keyData?.kelompok_id}&pelatih_id=${keyData?.master_pelatih_id}`,
  //   )
  //     .then((res) => setAttendanceStatusData(res.data))
  //     .catch((err) => console.log('error get attendance status', err));
  // }, [keyData]);

  const modalFooter = [
    <Button key="cancel" onClick={closeModal}>
      <FormattedMessage id="crud.cancel" />
    </Button>,
    <Button key="next" type="primary" htmlType="button" onClick={handleSave}>
      <FormattedMessage id="crud.save" />
    </Button>,
  ];

  return (
    <div>
      <Modal
        title={<FormattedMessage id={editedData ? 'crud.editData' : 'crud.addNewData'} />}
        visible={modalVisible}
        footer={modalFooter}
        // width="70%"
        onCancel={closeModal}
        centered
        bodyStyle={{ overflowY: 'scroll', maxHeight: '80%' }}
      >
        <Form
          labelCol={{ span: 8 }}
          wrapperCol={{ span: 16 }}
          name="form"
          form={form}
          layout="horizontal"
        >
          <Form.Item name="id" hidden />
          <Form.Item name="pemain_id" hidden />
          <Form.Item name="strength_conditioning_id" hidden />

          <Form.Item
            name="body_weight"
            label={<FormattedMessage id="pages.strength-conditioning.body_weight" />}
            rules={getRequiredRule('body_weight')}
          >
            <InputNumber type="number" />
          </Form.Item>
        </Form>
      </Modal>
    </div>
  );
};
