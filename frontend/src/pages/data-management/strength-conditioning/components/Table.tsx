import { Table, TableColumnsType, Button, Space, Tooltip } from 'antd';
import { EditOutlined } from '@ant-design/icons';
import { useContext, useEffect, useState } from 'react';
import { Context } from '../index';
import { useIntl } from 'umi';
import { StrengthConditionPlayer as Item } from '@/types/ypi';
import Table2 from './Table2';
import styles from './table.less';

export default ({ data }: { data: Item[] }) => {
  const intl = useIntl();
  const { editedData, setEditedData } = useContext(Context);
  const [editItemId, setEditItemId] = useState<number>(-1);

  useEffect(() => {
    if (editedData === null) {
      setEditItemId(-1);
    }
  }, [editedData]);

  const onEdit = (record: Item) => {
    setEditedData(record);
    setEditItemId(record.id);
  };

  const onDelete = (record: Item) => {
    // destroy(tableName, record.id)
    //   .then((res) => {
    //     message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
    //     getTableData(
    //       keyData?.pemain_id as number,
    //       keyData?.master_pelatih_id as number,
    //       keyData?.kelompok_id as number,
    //     );
    //   })
    //   .catch((err) => {
    //     message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
    //   });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.player_name' }),
      dataIndex: 'player_name',
      key: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'pages.strength-conditioning.body_weight' }),
      dataIndex: 'body_weight',
      key: 'body_weight',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              size="small"
              onClick={() => onEdit(record)}
              type="link"
              icon={<EditOutlined />}
            />
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <Table
      expandable={{ expandedRowRender: Table2 }}
      className={styles.custom_table}
      pagination={{ showSizeChanger: true }}
      bordered
      columns={columns}
      dataSource={data}
      size="small"
      rowClassName={styles.densed_row}
    />
  );
};
