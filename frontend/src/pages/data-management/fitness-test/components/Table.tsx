import { Table, TableColumnsType } from 'antd';
import { FC, useContext } from 'react';
import { useIntl } from 'umi';
import { getLocaleKey, Context, Record } from '../index';
import styles from './table.less';

const EditableTable: FC = () => {
  const intl = useIntl();
  const { tableData } = useContext(Context);

  const columns: TableColumnsType<Partial<Record>> = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('player_code') }),
      dataIndex: 'player_code',
    },
    {
      title: intl.formatMessage({ id: getLocaleKey('player_name') }),
      dataIndex: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'commonField.court_agility' }),
      dataIndex: 'court_agility',
    },
    {
      title: intl.formatMessage({ id: 'commonField.vo2max' }),
      dataIndex: 'vo2max',
    },
    {
      title: intl.formatMessage({ id: 'commonField.bench_press' }),
      dataIndex: 'bench_press',
    },
    {
      title: intl.formatMessage({ id: 'commonField.squat' }),
      dataIndex: 'squat',
    },
  ];

  return (
    <Table
      size="small"
      className={styles.custom_table}
      bordered
      dataSource={tableData}
      columns={columns}
      scroll={{ x: 'max-content' }}
    />
  );
};

export default EditableTable;
