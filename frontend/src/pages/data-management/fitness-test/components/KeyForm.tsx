import { Form, DatePicker, Row, Col, message, Select, Button } from 'antd';
import { Context, Record, KeyFormType } from '../index';
import { useContext, useState } from 'react';
import { useIntl, FormattedMessage, useHistory } from 'umi';
const { Option } = Select;
import { get } from '@/services/ypi/api';
import moment, { Moment } from 'moment';
import { EditOutlined } from '@ant-design/icons';

export default () => {
  const [form] = Form.useForm<KeyFormType>();
  const intl = useIntl();
  const history = useHistory();
  const { savedDateData, groupData, setTableData, setDateTest } = useContext(Context);
  const lblDateTest = intl.formatMessage({ id: 'pages.fitness-test.date_test' });
  const lblAgeGroup = intl.formatMessage({ id: 'pages.fitness-test.group' });
  const [editDataVisible, setEditDataVisible] = useState<boolean>(false);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const onValuesChange = (changedValues: any, values: KeyFormType) => {
    if (changedValues.saved_date_test) {
      form.setFieldsValue({ date_test: moment(values.saved_date_test) });
    }

    if (changedValues.date_test) {
      form.setFieldsValue({ saved_date_test: undefined });
    }
  };

  const handleProcess = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      setEditDataVisible(true);
      const formData = await form.validateFields();
      const tableDataRes = await get(
        `kpi_results/fitness_test/${formData.date_test.format('YYYY-MM-DD')}/${formData.group_id}`,
      );
      setTableData(
        (tableDataRes.data as Record[]).map((d, i) => ({ ...d, key: i.toString(), no: i + 1 })),
      );
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Table Data' }));
    }
    hideLoading();
  };

  const handleEditData = () => {
    history.push('/individual/kpi');
  };

  const handleDateChange = (date: Moment | null, dateString: string) => {
    setDateTest(date);
    form.setFieldsValue({ saved_date_test: undefined });
  };

  return (
    <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
      <Row gutter={12}>
        <Col span={6}>
          <Form.Item name="date_test" label={lblDateTest} rules={getRequiredRule('Date Test')}>
            <DatePicker onChange={handleDateChange} style={{ width: '100%' }} />
          </Form.Item>
        </Col>
        <Col span={6}>
          <Form.Item
            name="saved_date_test"
            label={<FormattedMessage id="commonField.saved_data" />}
          >
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
              }
            >
              {savedDateData.map((d, i) => (
                <Option key={i} value={d}>
                  {d}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={4}>
          <Form.Item name="group_id" label={lblAgeGroup} rules={getRequiredRule('Group')}>
            <Select
              showSearch
              optionFilterProp="children"
              filterOption={(input, option) =>
                (option!.children as unknown as string).toLowerCase().includes(input.toLowerCase())
              }
            >
              {groupData.map((d) => (
                <Option key={d.id} value={d.id}>
                  {d.kelompok}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>
        <Col span={2}>
          <Form.Item label=" ">
            <Button onClick={handleProcess} type="primary">
              <FormattedMessage id="crud.process" />
            </Button>
          </Form.Item>
        </Col>
        <Col span={2}>
          <Form.Item label=" ">
            {editDataVisible && (
              <Button icon={<EditOutlined />} onClick={handleEditData} type="primary">
                <FormattedMessage id="crud.editData" />
              </Button>
            )}
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
