import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { get } from '@/services/ypi/api';
import Table from './components/Table';
import KeyForm from './components/KeyForm';
import { useIntl } from 'umi';
import { FitnessTest, GroupClassification } from '@/types/ypi';
import { Moment } from 'moment';

interface DevType {
  edited: boolean;
  key: string;
}

export interface Record extends DevType, FitnessTest {
  no: number;
  player_code: number;
  player_name: string;
}

export interface KeyFormType {
  date_test: Moment;
  saved_date_test: string;
  group_id: number;
}

export const getLocaleKey = (fieldName: string) => {
  return 'pages.fitness-test.' + fieldName;
};

export interface ContextType {
  moduleName: string;
  savedDateData: string[];
  groupData: GroupClassification[];
  tableData: Partial<Record>[];
  setTableData: React.Dispatch<React.SetStateAction<Partial<Record>[]>>;
  setDateTest: React.Dispatch<any>;
}

export const Context = createContext<ContextType>({
  moduleName: '',
  savedDateData: [],
  groupData: [],
  tableData: [],
  setTableData: () => {},
  setDateTest: () => {},
});

export const tableName = 'fitness_tests';

export default () => {
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.data-management.fitness-test' }),
  );
  const [dateTest, setDateTest] = useState<Moment | null>(null);
  const [tableData, setTableData] = useState<Partial<Record>[]>([]);
  const [savedDateData, setSavedDateData] = useState<string[]>([]);
  const [groupData, setGroupData] = useState<GroupClassification[]>([]);

  const fetchDependenciesData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dateDataRes = await get('kpi_results/dates');
      setSavedDateData(dateDataRes.data);
      const groupRes = await get('tabel_kelompoks');
      setGroupData(groupRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  useEffect(() => {
    console.log(tableData);
  }, [tableData]);

  const contextValue: ContextType = {
    moduleName,
    groupData,
    savedDateData,
    tableData,
    setTableData,
    setDateTest,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm />
      <Table />
    </Context.Provider>
  );
};
