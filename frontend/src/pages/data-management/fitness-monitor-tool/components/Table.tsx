import {
  Table,
  TableColumnsType,
  Button,
  Space,
  Tooltip,
  Popconfirm,
  message,
  Typography,
} from 'antd';
import { EditOutlined, DeleteOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { destroy } from '@/services/ypi/api';
import { tableName, Context } from '../index';
import { useIntl, FormattedMessage } from 'umi';
import { FitnessMonitorTool as Item } from '@/types/ypi';
import styles from './table.less';

const { Text } = Typography;

const getAverageText = (values: number[]) => {
  if (values.length > 0) {
    let total = 0;
    values.forEach((e) => {
      total += e;
    });
    const avg = total / values.length;
    return <Text strong>{avg.toFixed(2)}</Text>;
  }
  return <></>;
};

const getStdDeviationText = (values: number[]) => {
  if (values.length > 0) {
    const n = values.length;
    const mean = values.reduce((a, b) => a + b) / n;
    const result = Math.sqrt(values.map((x) => Math.pow(x - mean, 2)).reduce((a, b) => a + b) / n);
    return <Text strong>{result.toFixed(2)}</Text>;
  }
  return <></>;
};

const getMaximumText = (values: number[]) => {
  if (values.length > 0) {
    return <Text strong>{Math.max(...values)}</Text>;
  }
  return <></>;
};

const getMinimumText = (values: number[]) => {
  if (values.length > 0) {
    return <Text strong>{Math.min(...values)}</Text>;
  }
  return <></>;
};

export default ({ data }: { data: Item[] }) => {
  const intl = useIntl();
  const { setEditedData, getTableData, moduleName, editedData } = useContext(Context);

  const onEdit = (record: Item) => {
    setEditedData(record);
  };

  const onDelete = (record: Item) => {
    destroy(tableName, record.id)
      .then((res) => {
        message.success(intl.formatMessage({ id: 'message.successDelete' }, { moduleName }));
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName }));
      });
  };

  const columns: TableColumnsType<Item> = [
    {
      title: intl.formatMessage({ id: 'commonField.no' }),
      dataIndex: 'no',
      key: 'no',
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.player_name' }),
      dataIndex: 'player_name',
      key: 'player_name',
      fixed: 'left',
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.beep_test' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
          dataIndex: 'beep_test_best',
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.beep_test' }),
          dataIndex: 'beep_test',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.court_agility' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
          dataIndex: 'court_agility_best',
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.best_time' }),
          dataIndex: 'court_agility',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.bench_press' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
          dataIndex: 'bench_press_best',
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.best_record' }),
          dataIndex: 'bench_press',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.squat' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
          dataIndex: 'squat_best',
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.best_record' }),
          dataIndex: 'squat',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.reach' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.reach' }),
          dataIndex: 'reach',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.vertical_jump' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.cj' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
              dataIndex: 'cj_best',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'cj_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'cj_elevation',
            },
          ],
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.sj' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
              dataIndex: 'sq_best',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'sq_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'sq_elevation',
            },
          ],
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.rl' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
              dataIndex: 'rl_best',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'rl_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'rl_elevation',
            },
          ],
        },
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.ll' }),
          children: [
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.personal_best' }),
              dataIndex: 'll_best',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.actual_score' }),
              dataIndex: 'll_actual',
            },
            {
              title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.jump_elevation' }),
              dataIndex: 'll_elevation',
            },
          ],
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.eu' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.ratio' }),
          dataIndex: 'ratio',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.leg_strength' }),
      children: [
        {
          title: intl.formatMessage({ id: 'pages.fitness-monitor-tool.r_vs_l_leg' }),
          dataIndex: 'difference',
        },
      ],
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              onClick={() => onEdit(record)}
              type="primary"
              shape="circle"
              icon={<EditOutlined />}
              size="small"
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage({ id: 'message.confirmDeleteMessage' }, { moduleName })}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button size="small" type="primary" danger shape="circle" icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
        </Space>
      ),
    },
  ];

  const rowClassName = (record: Item, index: number) => {
    if (editedData?.key == record.key) return styles.record_edit;
    return '';
  };

  const summary = (data: readonly Item[]) => {
    let t_beep_test_best: number[] = [];
    let t_beep_test: number[] = [];
    let t_court_best: number[] = [];
    let t_court: number[] = [];
    let t_reach: number[] = [];
    let t_cj_best: number[] = [];
    let t_cj_actual: number[] = [];
    let t_cj_elevation: number[] = [];
    let t_sq_best: number[] = [];
    let t_sq_actual: number[] = [];
    let t_sq_elevation: number[] = [];
    let t_rl_best: number[] = [];
    let t_rl_actual: number[] = [];
    let t_rl_elevation: number[] = [];
    let t_ll_best: number[] = [];
    let t_ll_actual: number[] = [];
    let t_ll_elevation: number[] = [];
    let t_ratio: number[] = [];
    let t_diff: number[] = [];

    data.forEach((value) => {
      value.beep_test_best && t_beep_test_best.push(value.beep_test_best);
      value.beep_test && t_beep_test.push(value.beep_test);
      value.court_agility_best && t_court_best.push(value.court_agility_best);
      value.court_agility && t_court.push(value.court_agility);
      value.reach && t_reach.push(value.reach);
      value.cj_best && t_cj_best.push(value.cj_best);
      value.cj_actual && t_cj_actual.push(value.cj_actual);
      value.cj_elevation && t_cj_elevation.push(value.cj_elevation);
      value.sq_best && t_sq_best.push(value.sq_best);
      value.sq_actual && t_sq_actual.push(value.sq_actual);
      value.sq_elevation && t_sq_elevation.push(value.sq_elevation);
      value.rl_best && t_rl_best.push(value.rl_best);
      value.rl_actual && t_rl_actual.push(value.rl_actual);
      value.rl_elevation && t_rl_elevation.push(value.rl_elevation);
      value.ll_best && t_ll_best.push(value.ll_best);
      value.ll_actual && t_ll_actual.push(value.ll_actual);
      value.ll_elevation && t_ll_elevation.push(value.ll_elevation);
      value.ratio && t_ratio.push(value.ratio);
      value.difference && t_diff.push(value.difference);
    });

    return (
      <Table.Summary fixed>
        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.average" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getAverageText(t_beep_test_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getAverageText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getAverageText(t_court_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getAverageText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getAverageText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getAverageText(t_cj_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getAverageText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getAverageText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getAverageText(t_sq_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getAverageText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getAverageText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getAverageText(t_rl_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getAverageText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getAverageText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={16}>{getAverageText(t_ll_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={17}>{getAverageText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={18}>{getAverageText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={19}>{getAverageText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={20}>{getAverageText(t_diff)}</Table.Summary.Cell>
          <Table.Summary.Cell index={21} />
        </Table.Summary.Row>

        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.std_deviasi" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getStdDeviationText(t_beep_test_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getStdDeviationText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getStdDeviationText(t_court_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getStdDeviationText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getStdDeviationText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getStdDeviationText(t_cj_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getStdDeviationText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getStdDeviationText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getStdDeviationText(t_sq_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getStdDeviationText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getStdDeviationText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getStdDeviationText(t_rl_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getStdDeviationText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getStdDeviationText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={16}>{getStdDeviationText(t_ll_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={17}>{getStdDeviationText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={18}>{getStdDeviationText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={19}>{getStdDeviationText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={20}>{getStdDeviationText(t_diff)}</Table.Summary.Cell>
          <Table.Summary.Cell index={21} />
        </Table.Summary.Row>

        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.maximum" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getMaximumText(t_beep_test_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getMaximumText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getMaximumText(t_court_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getMaximumText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getMaximumText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getMaximumText(t_cj_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getMaximumText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getMaximumText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getMaximumText(t_sq_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getMaximumText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getMaximumText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getMaximumText(t_rl_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getMaximumText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getMaximumText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={16}>{getMaximumText(t_ll_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={17}>{getMaximumText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={18}>{getMaximumText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={19}>{getMaximumText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={20}>{getMaximumText(t_diff)}</Table.Summary.Cell>
          <Table.Summary.Cell index={21} />
        </Table.Summary.Row>

        <Table.Summary.Row>
          <Table.Summary.Cell colSpan={2} index={0}>
            <Text strong>
              <FormattedMessage id="pages.fitness-monitor-tool.minimum" />
            </Text>
          </Table.Summary.Cell>
          <Table.Summary.Cell index={2}>{getMinimumText(t_beep_test_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={3}>{getMinimumText(t_beep_test)}</Table.Summary.Cell>
          <Table.Summary.Cell index={4}>{getMinimumText(t_court_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={5}>{getMinimumText(t_court)}</Table.Summary.Cell>
          <Table.Summary.Cell index={6}>{getMinimumText(t_reach)}</Table.Summary.Cell>
          <Table.Summary.Cell index={7}>{getMinimumText(t_cj_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={8}>{getMinimumText(t_cj_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={9}>{getMinimumText(t_cj_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={10}>{getMinimumText(t_sq_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={11}>{getMinimumText(t_sq_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={12}>{getMinimumText(t_sq_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={13}>{getMinimumText(t_rl_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={14}>{getMinimumText(t_rl_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={15}>{getMinimumText(t_rl_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={16}>{getMinimumText(t_ll_best)}</Table.Summary.Cell>
          <Table.Summary.Cell index={17}>{getMinimumText(t_ll_actual)}</Table.Summary.Cell>
          <Table.Summary.Cell index={18}>{getMinimumText(t_ll_elevation)}</Table.Summary.Cell>
          <Table.Summary.Cell index={19}>{getMinimumText(t_ratio)}</Table.Summary.Cell>
          <Table.Summary.Cell index={20}>{getMinimumText(t_diff)}</Table.Summary.Cell>
          <Table.Summary.Cell index={21} />
        </Table.Summary.Row>
      </Table.Summary>
    );
  };

  return (
    <Table
      bordered
      className={styles.custom_table}
      rowClassName={rowClassName}
      columns={columns}
      scroll={{ x: true }}
      dataSource={data}
      size="small"
      summary={summary}
    />
  );
};
