import { Form, Button, message, Space, InputNumber, Drawer, Row, Col } from 'antd';
import { tableName, Context } from '../index';
import { create, update } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { FitnessMonitorTool } from '@/types/ypi';

export default () => {
  const [form] = Form.useForm();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, moduleName, keyData } = useContext(Context);
  const [openForm, setOpenForm] = useState<boolean>(false);

  const onFinish = async () => {
    try {
      const values: FitnessMonitorTool = await form.validateFields();
      if (keyData) {
        values.tanggal = keyData?.date_moment?.format('YYYY-MM-DD');
        values.gender = keyData.gender;
        values.kelompok_id = keyData.group_id;
      }
      values.cj_elevation = values.cj_actual - values.reach;
      values.sq_elevation = values.sq_actual - values.reach;
      values.rl_elevation = values.rl_actual - values.reach;
      values.ll_elevation = values.ll_actual - values.reach;

      if (values.sq_elevation) values.ratio = values.cj_elevation / values.sq_elevation;
      else values.ratio = 0;

      if (values.rl_elevation)
        values.difference =
          ((values.rl_elevation - values.ll_elevation) / values.ll_elevation) * 100;
      else values.difference = 0;

      if (values.id) {
        update(tableName, values.id, values)
          .then((res) => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch((err) => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      } else {
        create(tableName, values)
          .then((res) => {
            message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName }));
            getTableData();
            onReset();
          })
          .catch((err) => {
            message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
          });
      }
      onCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName }));
    }
  };

  useEffect(() => {
    if (editedData) {
      form.setFieldsValue(editedData);
      setOpenForm(true);
    }
  }, [editedData]);

  const onReset = () => {
    form.resetFields();
  };

  const onCancel = () => {
    form.resetFields();
    setEditedData(null);
    setOpenForm(false);
  };

  const onOpenForm = () => {
    setOpenForm(true);
  };

  const getLocaleKey = (fieldName: string) => {
    return 'pages.fitness-monitor-tool.' + fieldName;
  };

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  return (
    <>
      <Drawer
        title={intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
        visible={openForm}
        placement="bottom"
        mask={false}
        onClose={onCancel}
        headerStyle={{ padding: '10px 20px' }}
        height="14rem"
        extra={
          <Space>
            <Button type="primary" htmlType="button" onClick={onFinish}>
              {intl.formatMessage({ id: editedData ? 'crud.update' : 'crud.save' })}
            </Button>
            <Button
              type="primary"
              htmlType="button"
              onClick={onCancel}
              style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
            >
              {intl.formatMessage({ id: 'crud.cancel' })}
            </Button>
          </Space>
        }
      >
        <Form form={form} name="form" onFinish={onFinish} layout="vertical">
          <Form.Item name="id" hidden />
          <Form.Item name="no" hidden />
          <Form.Item name="pemain_id" hidden />
          <Form.Item name="beep_test_best" hidden />
          <Form.Item name="court_agility_best" hidden />
          <Form.Item name="bench_press_best" hidden />
          <Form.Item name="squat_best" hidden />
          <Form.Item name="cj_best" hidden />
          <Form.Item name="sq_best" hidden />
          <Form.Item name="rl_best" hidden />
          <Form.Item name="ll_best" hidden />

          <Row gutter={12}>
            <Col span={2}>
              <Form.Item
                name="beep_test"
                label={<FormattedMessage id={getLocaleKey('beep_test')} />}
                rules={getRequiredRule('beep_test')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="court_agility"
                label={<FormattedMessage id={getLocaleKey('court_agility')} />}
                rules={getRequiredRule('court_agility')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="bench_press"
                label={<FormattedMessage id={getLocaleKey('bench_press')} />}
                rules={getRequiredRule('bench_press')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="squat"
                label={<FormattedMessage id={getLocaleKey('squat')} />}
                rules={getRequiredRule('squat')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="reach"
                label={<FormattedMessage id={getLocaleKey('reach')} />}
                rules={getRequiredRule('reach')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={4}>
              <Form.Item
                name="cj_actual"
                label={<FormattedMessage id={getLocaleKey('cj')} />}
                rules={getRequiredRule('cj')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="sq_actual"
                label={<FormattedMessage id={getLocaleKey('sj')} />}
                rules={getRequiredRule('sj')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="rl_actual"
                label={<FormattedMessage id={getLocaleKey('rl')} />}
                rules={getRequiredRule('rl')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
            <Col span={2}>
              <Form.Item
                name="ll_actual"
                label={<FormattedMessage id={getLocaleKey('ll')} />}
                rules={getRequiredRule('ll')}
              >
                <InputNumber style={{ width: '100%' }} type="number" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Drawer>
      {openForm && <div style={{ height: '50vh', width: '100%' }} />}
    </>
  );
};
