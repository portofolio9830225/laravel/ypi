import { Form, DatePicker, Card, Row, Col, message, Select, Tooltip, Button } from 'antd';
import { Context, KeyData } from '../index';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { FitnessMonitorTool, GroupClassification } from '@/types/ypi';
const { Option } = Select;
import { get } from '@/services/ypi/api';
import moment, { Moment } from 'moment';
import { BulbOutlined } from '@ant-design/icons';

const NEW_DATA = 'NEW_DATA';
const MALE = 'Male';
const FEMALE = 'Female';

export default () => {
  const [form] = Form.useForm<KeyData>();
  const intl = useIntl();
  const { setKeyData, setHelpVisible, setHelp } = useContext(Context);

  const [dateTestData, setDateTestData] = useState<string[]>([]);
  const [groupData, setGroupData] = useState<GroupClassification[]>([]);
  const [isNewData, setIsNewData] = useState<boolean>(false);

  const lblDateTest = intl.formatMessage({ id: 'pages.fitness-test.date_test' });
  const lblGender = intl.formatMessage({ id: 'pages.fitness-test.gender' });
  const lblAgeGroup = intl.formatMessage({ id: 'pages.fitness-test.group' });
  const lblNewData = intl.formatMessage({ id: 'crud.addNewData' });

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage({ id: 'message.pleaseInputField' }, { fieldName }),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    try {
      const fitnessTestRes = await get('fitness_tools?groupDate=true');
      setDateTestData(
        (fitnessTestRes.data as FitnessMonitorTool[]).map((d) => {
          return d.tanggal.substring(0, 10);
        }),
      );
      const groupRes = await get('tabel_kelompoks');
      setGroupData(groupRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const onValuesChange = (changedValues: any, values: FitnessMonitorTool) => {
    if (changedValues.date_string) {
      form.setFieldsValue({ date_moment: moment(changedValues.date_string) });
    }
  };

  const handleHelp = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const helpRes = await get('constants/help/FITNESS_TOOL');
      setHelpVisible(true);
      setHelp(helpRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const handleProcess = async () => {
    try {
      const formValues = await form.validateFields();
      setKeyData(formValues);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
  };

  return (
    <Card style={{ marginBottom: '1rem' }}>
      <Form onValuesChange={onValuesChange} form={form} name="form" layout="vertical">
        <Row gutter={12}>
          <Col span={6}>
            <Form.Item name="date_moment" label={lblDateTest} rules={getRequiredRule(lblDateTest)}>
              <DatePicker style={{ width: '100%' }} />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item name="date_string" label={<FormattedMessage id="commonField.saved_data" />}>
              <Select>
                {dateTestData.map((d, i) => (
                  <Option key={i} value={d}>
                    {d}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item name="gender" label={lblGender} rules={getRequiredRule(lblGender)}>
              <Select>
                <Option value={MALE}>
                  <FormattedMessage id="commonField.male" />
                </Option>
                <Option value={FEMALE}>
                  <FormattedMessage id="commonField.female" />
                </Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={4}>
            <Form.Item name="group_id" label={lblAgeGroup} rules={getRequiredRule(lblAgeGroup)}>
              <Select>
                {groupData.map((d) => (
                  <Option key={d.id} value={d.id}>
                    {d.kelompok}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item label=" ">
              <Button onClick={handleProcess} type="primary">
                <FormattedMessage id="crud.process" />
              </Button>
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item label=" ">
              <Tooltip title="Help">
                <Button
                  onClick={handleHelp}
                  size="small"
                  type="primary"
                  shape="circle"
                  icon={<BulbOutlined />}
                />
              </Tooltip>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </Card>
  );
};
