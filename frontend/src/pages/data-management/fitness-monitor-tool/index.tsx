import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { create, get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import KeyForm from './components/KeyForm';
import { useIntl } from 'umi';
import { FitnessMonitorTool as Item, Player } from '@/types/ypi';
import DescriptionModal from '@/components/DescriptionModal';
import { Moment } from 'moment';

export interface KeyData {
  date_string?: string;
  date_moment: Moment;
  gender: string;
  group_id: number;
}

export interface ContextType {
  keyData: KeyData | undefined;
  setKeyData: React.Dispatch<React.SetStateAction<KeyData | undefined>>;
  editedData: Partial<Item> | null;
  setEditedData: React.Dispatch<React.SetStateAction<Partial<Item> | null>> | (() => void);
  getTableData: () => void;
  moduleName: string;
  setHelp: React.Dispatch<React.SetStateAction<string | undefined>>;
  setHelpVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Context = createContext<ContextType>({
  keyData: undefined,
  setKeyData: () => {},
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  moduleName: '',
  setHelp: () => {},
  setHelpVisible: () => {},
});

export const tableName = 'fitness_tools';

export default () => {
  const [data, setData] = useState<Partial<Item>[]>([]);
  const [keyData, setKeyData] = useState<KeyData>();
  const [editedData, setEditedData] = useState<Partial<Item> | null>(null);
  const intl = useIntl();
  const [moduleName] = useState<string>(
    intl.formatMessage({ id: 'menu.data-management.fitness-monitor-tool' }),
  );
  const [help, setHelp] = useState<string>();
  const [helpVisible, setHelpVisible] = useState<boolean>(false);

  useEffect(() => {
    if (keyData) {
      getTableData();
    }
  }, [keyData]);

  const handleHelpCancel = () => {
    setHelpVisible(false);
  };

  const handleHelpOk = async (html: string) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      await create('constants/help/FITNESS_TOOL', { value: html });
      handleHelpCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const dateString = keyData?.date_moment?.format('YYYY-MM-DD');
      const playerRes = await get(
        `master_pemains?kelompok_id=${keyData?.group_id}&gender=${keyData?.gender}`,
      );
      const playerData: Player[] = playerRes.data;
      if (playerData.length === 0) {
        message.info('Player Not Found!');
      }
      let data: Partial<Item>[] = [];
      if (playerData.length > 0) {
        await Promise.all(
          playerData.map(async (d, i) => {
            const fitnessMonitorRes = await get(
              `fitness_tools?pemain_id=${d.id}&tanggal=${dateString}&gender=${keyData?.gender}&kelompok_id=${keyData?.group_id}`,
            );
            const fitnessMonitor: Item = fitnessMonitorRes.data;

            data.push({
              ...fitnessMonitor,
              key: i.toString(),
              pemain_id: d.id,
              player_name: d.nama_lengkap,
              no: i + 1,
              id: fitnessMonitor ? fitnessMonitor.id : undefined,
              beep_test_best: fitnessMonitor ? fitnessMonitor.beep_test_best : undefined,
              court_agility_best: fitnessMonitor ? fitnessMonitor.court_agility_best : undefined,
              cj_best: fitnessMonitor ? fitnessMonitor.cj_best : undefined,
              sq_best: fitnessMonitor ? fitnessMonitor.sq_best : undefined,
              rl_best: fitnessMonitor ? fitnessMonitor.rl_best : undefined,
              ll_best: fitnessMonitor ? fitnessMonitor.ll_best : undefined,
            });
          }),
        );
      } else {
        data = [];
      }
      setData(data);
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: moduleName }));
    }
    hideLoading();
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    moduleName,
    keyData,
    setKeyData,
    setHelp,
    setHelpVisible,
  };

  return (
    <Context.Provider value={contextValue}>
      <KeyForm />
      <Table data={data as Item[]} />
      <Form />
      <DescriptionModal
        visible={helpVisible}
        value={help}
        onCancel={handleHelpCancel}
        onOk={handleHelpOk}
      />
    </Context.Provider>
  );
};
