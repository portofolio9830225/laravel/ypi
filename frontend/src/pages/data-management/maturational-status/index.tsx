import { message } from 'antd';
import React, { useState, useEffect, createContext } from 'react';
import { create, get } from '@/services/ypi/api';
import Table from './components/Table';
import Form from './components/Form';
import { useAccess, useIntl } from 'umi';
import { HeightPredicted, HeightData, PubertyData, Player } from '@/types/ypi';
import PrintModal from './components/PrintModal';
import moment from 'moment';
import DescriptionModal from '@/components/DescriptionModal';

export const getLocaleKey = (fieldName: string) => {
  return 'pages.maturational-status.' + fieldName;
};

export const getReorderedMonths = (beginMonth: number) => {
  const monthsShort = moment.monthsShort();

  const slicedMonths = monthsShort.slice(0, beginMonth);
  const restOfmonths = monthsShort.slice(beginMonth);
  return [...restOfmonths, ...slicedMonths];
};

export interface MaturationalStatus {
  heightPredicted: HeightPredicted;
  heightData: HeightData;
  pubertyData: PubertyData;
}

export interface ContextType {
  editedData: Partial<HeightPredicted> | null;
  setEditedData:
    | React.Dispatch<React.SetStateAction<Partial<HeightPredicted> | null>>
    | (() => void);
  getTableData: () => void;
  printData: Partial<HeightPredicted> | undefined;
  setPrintData: React.Dispatch<React.SetStateAction<Partial<HeightPredicted> | undefined>>;
  playerData: Player[];
  help: string | undefined;
  setHelp: React.Dispatch<React.SetStateAction<string | undefined>>;
  setHelpVisible: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Context = createContext<ContextType>({
  editedData: null,
  setEditedData: () => {},
  getTableData: () => {},
  printData: undefined,
  setPrintData: () => {},
  playerData: [],
  help: undefined,
  setHelp: () => {},
  setHelpVisible: () => {},
});

export default () => {
  const intl = useIntl();
  const access = useAccess();
  const [playerData, setPlayerData] = useState<Player[]>([]);
  const [data, setData] = useState<Partial<HeightPredicted>[]>([]);
  const [editedData, setEditedData] = useState<Partial<HeightPredicted> | null>(null);
  const [printData, setPrintData] = useState<Partial<HeightPredicted>>();
  const [help, setHelp] = useState<string>();
  const [helpVisible, setHelpVisible] = useState<boolean>(false);

  const getTableData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const heightPredictedRes = await get('height_predicteds');
      const heightPredicted: HeightPredicted[] = heightPredictedRes.data;
      setData(
        heightPredicted.map((d) => {
          return { ...d, key: d.id.toString(), dob: d.player_data?.tanggal_lahir.substring(0, 10) };
        }),
      );
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
    hideLoading();
  };

  const fetchDependenciesData = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      let playerRes;
      let heightPredictedRes;
      if (access.coachId) {
        playerRes = await get('master_pemains?coach_id=' + access.coachId);
        heightPredictedRes = await get('height_predicteds?coach_id=' + access.coachId);
      } else {
        playerRes = await get('master_pemains');
        heightPredictedRes = await get('height_predicteds');
      }

      setPlayerData(playerRes.data);
      const heightPredicted: HeightPredicted[] = heightPredictedRes.data;
      setData(
        heightPredicted.map((d) => {
          return { ...d, key: d.id.toString(), dob: d.player_data?.tanggal_lahir.substring(0, 10) };
        }),
      );
    } catch (error) {
      console.log(error);
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
    hideLoading();
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  const handleHelpCancel = () => {
    setHelpVisible(false);
  };

  const handleHelpOk = async (html: string) => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      await create('constants/help/MATURATIONAL_STATUS', { value: html });
      handleHelpCancel();
    } catch (error) {
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const contextValue: ContextType = {
    editedData,
    setEditedData,
    getTableData,
    printData,
    setPrintData,
    playerData,
    help,
    setHelp,
    setHelpVisible,
  };

  return (
    <Context.Provider value={contextValue}>
      <Table data={data as HeightPredicted[]} />
      <Form />
      <PrintModal />
      <DescriptionModal
        visible={helpVisible}
        value={help}
        onCancel={handleHelpCancel}
        onOk={handleHelpOk}
      />
    </Context.Provider>
  );
};
