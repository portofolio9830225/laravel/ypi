import { Form, Button, message, Space, Drawer, Tabs, Tooltip } from 'antd';
import { Context, MaturationalStatus } from '../index';
import { create, get } from '@/services/ypi/api';
import { useContext, useEffect, useState } from 'react';
import { useIntl, FormattedMessage } from 'umi';
import { HeightPredicted, HeightData, PubertyData } from '@/types/ypi';
import Form1 from './Form1';
import Form2 from './Form2';
import Form3 from './Form3';
import moment from 'moment';
import { BulbOutlined } from '@ant-design/icons';

const { TabPane } = Tabs;

export default () => {
  const [form1] = Form.useForm<HeightPredicted>();
  const [form2] = Form.useForm<HeightData>();
  const [form3] = Form.useForm<PubertyData>();
  const intl = useIntl();
  const { editedData, setEditedData, getTableData, setHelp, setHelpVisible } = useContext(Context);
  const [openForm, setOpenForm] = useState<boolean>(false);
  const [activeTab, setActiveTab] = useState<string>('1');
  const [formData, setFormData] = useState<Partial<MaturationalStatus>>();

  const onFinish = async () => {
    try {
      if (activeTab == '1') {
        const form1Values = await form1.validateFields();
        form1Values.date_predicted = moment().format('YYYY-MM-DD');
        setFormData({ ...formData, heightPredicted: form1Values });
        setActiveTab('2');
      }
      if (activeTab == '2') {
        const form2Values = await form2.validateFields();
        setFormData({ ...formData, heightData: form2Values });
        setActiveTab('3');
      }
      if (activeTab == '3') {
        const form3Values = await form3.validateFields();
        const data = { ...formData, pubertyData: form3Values };
        setFormData(data);
        await create('height_predicteds/maturationalStatus', data);
        message.success(intl.formatMessage({ id: 'message.successSave' }, { moduleName: 'Data' }));
        getTableData();
        onCancel();
      }
    } catch (error) {
      if ((error as any).errorFields) return;
      message.error(intl.formatMessage({ id: 'message.errorSave' }, { moduleName: 'Data' }));
    }
  };

  const onPrevTab = () => {
    if (activeTab == '2') setActiveTab('1');
    if (activeTab == '3') setActiveTab('2');
  };

  useEffect(() => {
    if (editedData) {
      form1.setFieldsValue(editedData);
      form2.setFieldsValue(editedData.height_data as HeightData);
      form3.setFieldsValue(editedData.puberty_data as PubertyData);
      setOpenForm(true);
    }
  }, [editedData]);

  const onReset = () => {
    form1.resetFields();
    form2.resetFields();
    form3.resetFields();
  };

  const onCancel = () => {
    onReset();
    setEditedData(null);
    setOpenForm(false);
    setActiveTab('1');
  };

  const handleHelp = async () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    try {
      const helpRes = await get('constants/help/MATURATIONAL_STATUS');
      setHelpVisible(true);
      setHelp(helpRes.data);
    } catch (error) {
      console.log(error);
      message.error(intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Data' }));
    }
    hideLoading();
  };

  const getLocaleKey = (fieldName: string) => {
    return 'pages.maturational-status.' + fieldName;
  };

  const extra = (
    <Space>
      <Tooltip title="Help">
        <Button onClick={handleHelp} size="small" shape="circle" icon={<BulbOutlined />} />
      </Tooltip>
      <Button disabled={activeTab == '1'} htmlType="button" onClick={onPrevTab}>
        {intl.formatMessage({ id: 'crud.prev' })}
      </Button>
      <Button type="primary" htmlType="button" onClick={onFinish}>
        {intl.formatMessage({ id: activeTab == '3' ? 'crud.save' : 'crud.next' })}
      </Button>
      <Button
        type="primary"
        htmlType="button"
        onClick={onCancel}
        style={{ backgroundColor: '#ffa940', borderColor: '#ffa940' }}
      >
        {intl.formatMessage({ id: 'crud.cancel' })}
      </Button>
    </Space>
  );

  return (
    <>
      <Drawer
        title={
          <span style={{ color: 'white' }}>
            {intl.formatMessage({ id: editedData ? 'crud.editData' : 'crud.addNewData' })}
          </span>
        }
        visible={true}
        placement="bottom"
        closable={false}
        mask={false}
        onClose={onCancel}
        headerStyle={{ padding: '6px 10px', backgroundColor: '#00B4ED' }}
        height="40%"
        extra={extra}
      >
        <div className="card-container">
          <Tabs activeKey={activeTab} size="small" type="card" tabPosition="left">
            <TabPane
              disabled={activeTab != '1'}
              tab={<FormattedMessage id={getLocaleKey('height_predicted')} />}
              key="1"
            >
              <Form1 form1={form1} form3={form3} />
            </TabPane>
            <TabPane
              disabled={activeTab != '2'}
              tab={<FormattedMessage id={getLocaleKey('height_data')} />}
              key="2"
            >
              <Form2 form2={form2} form3={form3} />
            </TabPane>
            <TabPane
              disabled={activeTab != '3'}
              tab={<FormattedMessage id={getLocaleKey('biological_category')} />}
              key="3"
            >
              <Form3 form3={form3} form1={form1} />
            </TabPane>
          </Tabs>
        </div>
      </Drawer>
      {openForm && <div style={{ height: '50vh', width: '100%' }} />}
    </>
  );
};
