import { Form, Input, Row, Col, Select, FormInstance } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import { HeightData, PubertyData } from '@/types/ypi';
import { useEffect, useState } from 'react';
import moment from 'moment';
import { getReorderedMonths } from '../index';

const { Option } = Select;
const monthsData: string[] = moment.months();

const getLocaleKey = (fieldName: string) => {
  return 'pages.maturational-status.' + fieldName;
};

const getYears = () => {
  const years = [];
  const dateStart = moment().subtract(4, 'y');
  const dateEnd = moment().add(4, 'y');
  while (dateEnd.diff(dateStart, 'years') >= 0) {
    years.push(dateStart.format('YYYY'));
    dateStart.add(1, 'year');
  }
  return years;
};
const yearsData = getYears();

export default ({
  form2,
  form3,
}: {
  form2: FormInstance<HeightData>;
  form3: FormInstance<PubertyData>;
}) => {
  const intl = useIntl();
  const [reorderedMonths, setReorderedMonths] = useState<string[]>(moment.monthsShort());
  const id = Form.useWatch('id', form2);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const setAutoCompleteFields = () => {
    const values = form2.getFieldsValue();

    setReorderedMonths(getReorderedMonths(values.bulan_awal));

    for (let i = 1; i <= 12; i++) {
      const height = parseInt(values['bulan_' + i]) || 0;
      const nextHeight = parseInt(values['bulan_' + (i + 1)]) || 0;
      const heightOffset = nextHeight - height;

      form2.setFieldsValue({ ['bulan_selisih_' + i]: nextHeight && heightOffset });
    }
    let firstHeight = 0;
    let lastHeight = 0;
    for (let i = 1; i <= 12; i++) {
      if (values['bulan_' + i]) {
        firstHeight = parseInt(values['bulan_' + i]);
        break;
      }
    }
    for (let i = 12; i >= 1; i--) {
      if (values['bulan_' + i]) {
        lastHeight = parseInt(values['bulan_' + i]);
        break;
      }
    }
    form2.setFieldsValue({ growth_rate: lastHeight - firstHeight });
    form3.setFieldsValue({ growth_rate: lastHeight - firstHeight, height: lastHeight });
  };

  const onValuesChange = (changedValues: any, values: HeightData) => {
    setAutoCompleteFields();
  };

  useEffect(() => {
    if (id) {
      setAutoCompleteFields();
    }
  }, [id]);

  return (
    <Form size="small" form={form2} name="form_2" onValuesChange={onValuesChange} layout="vertical">
      <Form.Item name="id" hidden />
      <Form.Item name="height_predicted_id" hidden />
      <Form.Item name="pemain_id" hidden />

      <Row gutter={12}>
        <Col span={8}>
          <Form.Item
            name="bulan_awal"
            label={<FormattedMessage id={getLocaleKey('begin_month')} />}
            rules={getRequiredRule('begin_month')}
          >
            <Select>
              {monthsData.map((d, i) => (
                <Option key={i} value={i}>
                  {d}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item
            name="tahun_awal"
            label={<FormattedMessage id={getLocaleKey('year')} />}
            rules={getRequiredRule('year')}
          >
            <Select>
              {yearsData.map((d, i) => (
                <Option key={i} value={d}>
                  {d}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item
            name="growth_rate"
            label={<FormattedMessage id={getLocaleKey('growth_rate_cm')} />}
            rules={getRequiredRule('growth_rate_cm')}
          >
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>

      <table>
        <thead>
          <tr>
            {reorderedMonths.map((d, i) => (
              <td key={i}>{d}</td>
            ))}
          </tr>
        </thead>
        <tbody>
          <tr>
            {reorderedMonths.map((d, i) => (
              <td key={i}>
                <Form.Item name={'bulan_' + (i + 1)}>
                  <Input />
                </Form.Item>
              </td>
            ))}
          </tr>
        </tbody>
      </table>

      <table>
        <thead>
          <tr>
            {reorderedMonths.map((d, i) => {
              if (reorderedMonths.length - 1 == i) return;
              const nextMonth = reorderedMonths[i + 1] || reorderedMonths[i - 1];
              return <td key={i}>{`${nextMonth}-${d}`}</td>;
            })}
          </tr>
        </thead>
        <tbody>
          <tr>
            {monthsData.map((d, i) => {
              if (reorderedMonths.length - 1 == i) return;
              return (
                <td key={i}>
                  <Form.Item name={'bulan_selisih_' + (i + 1)}>
                    <Input disabled />
                  </Form.Item>
                </td>
              );
            })}
          </tr>
        </tbody>
      </table>
    </Form>
  );
};
