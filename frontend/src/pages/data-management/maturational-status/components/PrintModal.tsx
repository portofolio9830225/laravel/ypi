import { Drawer, Row, Col, Table, Space, Button, message, Descriptions } from 'antd';
import styles from './PrintModal.less';
import { getLocaleKey, Context, getReorderedMonths } from '../index';
import { FormattedMessage, useIntl } from 'umi';
import { useContext, useEffect, useState } from 'react';
import { PrinterOutlined } from '@ant-design/icons';
import { jsPDF } from 'jspdf';
import moment from 'moment';

const Header = () => {
  const { printData } = useContext(Context);

  return (
    <div>
      <Row justify="space-between" align="middle">
        <Col>
          <Space>
            <span className={styles.title}>Maturational</span>
            <div className={styles.space} />
            <span className={styles.title}>Status</span>
            <div className={styles.space} />
            <span className={styles.title}>&</span>
            <div className={styles.space} />
            <span className={styles.title}>Height</span>
            <div className={styles.space} />
            <span className={styles.title}>Predicted</span>
          </Space>
        </Col>
        <Col>
          <img className={styles.logo} src="/logo_bypro.png" />
        </Col>
      </Row>
      <br />
      <Descriptions
        title={<FormattedMessage id="commonField.player" />}
        size="small"
        column={3}
        labelStyle={{ fontWeight: 'bold' }}
      >
        <Descriptions.Item label="IC/No">{printData?.player_data?.nomor_ic}</Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id="commonField.player" />}>
          {printData?.player_data?.nama_lengkap}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('dob')} />}>
          {printData?.dob}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('age')} />}>
          {moment().diff(printData?.dob, 'years')}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('gender')} />}>
          {printData?.player_data?.sex}
        </Descriptions.Item>
      </Descriptions>
      <br />
      <Descriptions
        size="small"
        column={3}
        title={<FormattedMessage id={getLocaleKey('height_predicted')} />}
        labelStyle={{ fontWeight: 'bold' }}
      >
        <Descriptions.Item label={<FormattedMessage id="commonField.age" />}>
          {printData?.age}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('height')} />}>
          {printData?.height}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('weight')} />}>
          {printData?.weight}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('f_height')} />}>
          {printData?.fathers_height}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('m_height')} />}>
          {printData?.mothers_height}
        </Descriptions.Item>
        <Descriptions.Item
          label={<FormattedMessage id={getLocaleKey('predicted_mature_height')} />}
        >
          {printData?.predicted_height}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('maturity_status')} />}>
          {printData?.maturity_status}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

const TableDetail = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [data, setData] = useState();
  const [data2, setData2] = useState();
  const [reorderedMonths, setReorderedMonths] = useState<string[]>(moment.monthsShort());

  useEffect(() => {
    if (printData) {
      setReorderedMonths(getReorderedMonths(printData.height_data?.bulan_awal || 0));
    }
  }, [printData]);

  useEffect(() => {
    if (reorderedMonths) {
      setData(() => {
        const monthData = {};
        for (let i = 1; i <= 12; i++) {
          if (printData && printData.height_data)
            monthData['bulan_' + i] = printData?.height_data['bulan_' + i];
          else monthData['bulan_' + i] = 0;
        }
        return [monthData];
      });
      setData2(() => {
        const monthData = {};
        if (printData && printData.height_data) {
          const values = printData.height_data;
          for (let i = 1; i <= 12; i++) {
            const height = parseInt(values['bulan_' + i]) || 0;
            const nextHeight = parseInt(values['bulan_' + (i + 1)]) || 0;
            const heightOffset = nextHeight - height;

            // form2.setFieldsValue({ ['bulan_selisih_' + i]: nextHeight && heightOffset });
            monthData['bulan_selisih_' + i] = nextHeight && heightOffset;
          }
        }
        return [monthData];
      });
    }
  }, [reorderedMonths]);

  const monthColumns = reorderedMonths.map((d, i) => ({
    title: d,
    dataIndex: 'bulan_' + (i + 1),
  }));
  const monthColumns2 = reorderedMonths.slice(0, -1).map((d, i) => {
    const nextMonth = reorderedMonths[i + 1] || reorderedMonths[i - 1];
    return {
      title: `${nextMonth}-${d}`,
      dataIndex: 'bulan_selisih_' + (i + 1),
    };
  });

  const columns = [...monthColumns];
  const columns2 = [...monthColumns2];

  return (
    <>
      <Descriptions
        title={<FormattedMessage id={getLocaleKey('growth_data')} />}
        labelStyle={{ fontWeight: 'bold' }}
      >
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('begin_month')} />}>
          {reorderedMonths[0]}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('growth_rate_cm')} />}>
          {printData?.height_data?.growth_rate}
        </Descriptions.Item>
      </Descriptions>
      <Table bordered columns={columns} dataSource={data} size="small" pagination={false} />
      <Table bordered columns={columns2} dataSource={data2} size="small" pagination={false} />
    </>
  );
};

const Footer = () => {
  const intl = useIntl();
  const { printData } = useContext(Context);
  const [biologicalCategory, setBiologicalCategory] = useState<string>('None');

  useEffect(() => {
    if (printData && printData.puberty_data) {
      if (printData.puberty_data.biological_category == 0) {
        setBiologicalCategory(intl.formatMessage({ id: getLocaleKey('post_puberty') }));
      } else if (printData.puberty_data.biological_category == 1) {
        setBiologicalCategory(intl.formatMessage({ id: getLocaleKey('puberty') }));
      } else if (printData.puberty_data.biological_category == 2) {
        setBiologicalCategory(intl.formatMessage({ id: getLocaleKey('pre_puberty') }));
      }
    }
  }, [printData]);

  return (
    <div>
      <Descriptions
        title={<FormattedMessage id={getLocaleKey('biological_category')} />}
        size="small"
        column={3}
        labelStyle={{ fontWeight: 'bold' }}
      >
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('age')} />}>
          {printData?.puberty_data?.age}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('height')} />}>
          {printData?.puberty_data?.height}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('national_height_avg')} />}>
          {printData?.puberty_data?.national_average}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('growth_rate')} />}>
          {printData?.puberty_data?.growth_rate}
        </Descriptions.Item>
        <Descriptions.Item label={<FormattedMessage id={getLocaleKey('biological_category')} />}>
          {biologicalCategory}
        </Descriptions.Item>
      </Descriptions>
    </div>
  );
};

export default () => {
  const intl = useIntl();
  const { printData, setPrintData } = useContext(Context);
  const [visible, setVisible] = useState<boolean>(false);

  useEffect(() => {
    if (printData) setVisible(true);
  }, [printData]);

  const handleClose = () => {
    setVisible(false);
    setPrintData(undefined);
  };

  const handlePrint = () => {
    const hideLoading = message.loading(intl.formatMessage({ id: 'message.pleaseWait' }), 0);
    const doc = new jsPDF({
      orientation: 'landscape',
      format: 'a3',
    });
    const html = document.getElementById('print-area') || '<div></div>';
    doc.html(html, {
      callback: function (doc) {
        doc.save('maturational_status.pdf');
        hideLoading();
      },
      width: 420,
      windowWidth: 1200,
      autoPaging: 'text',
    });
  };

  const Extra = () => (
    <Space>
      <Button onClick={handleClose}>Cancel</Button>
      <Button icon={<PrinterOutlined />} type="primary" onClick={handlePrint}>
        Print
      </Button>
    </Space>
  );
  return (
    <>
      <Drawer
        extra={<Extra />}
        onClose={handleClose}
        placement="bottom"
        height="100%"
        visible={visible}
      >
        <div className={styles.print_area} id="print-area">
          <Header />
          <br />
          <TableDetail />
          <br />
          <Footer />
        </div>
      </Drawer>
    </>
  );
};
