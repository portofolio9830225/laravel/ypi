import { HeightPredicted } from '@/types/ypi';

export const calculatePredictedHeight = (data: HeightPredicted) => {
  if (data.gender == 'Male') {
    if (data.age <= 10) {
      const height =
        (-11.038 +
          0.97135 * (data.height / 2.54) +
          (-0.0039981 * data.weight) / 0.4536 +
          0.45932 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age == 11) {
      const height =
        (-10.4917 +
          0.81239 * (data.height / 2.54) +
          (-0.002905 * data.weight) / 0.4536 +
          0.54781 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age == 12) {
      const height =
        (-9.3522 +
          0.68325 * (data.height / 2.54) +
          (-0.0020076 * data.weight) / 0.4536 +
          0.60927 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age == 13) {
      const height =
        (-7.8632 +
          0.60818 * (data.height / 2.54) +
          (-0.0013895 * data.weight) / 0.4536 +
          0.62407 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age == 14) {
      const height =
        (-6.4299 +
          0.59151 * (data.height / 2.54) +
          (-0.0009776 * data.weight) / 0.4536 +
          0.58762 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age == 15) {
      const height =
        (-5.1282 +
          0.63757 * (data.height / 2.54) +
          (-0.0006988 * data.weight) / 0.4536 +
          0.49536 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age == 16) {
      const height =
        (-3.9292 +
          0.75069 * (data.height / 2.54) +
          (-0.0004795 * data.weight) / 0.4536 +
          0.34271 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age >= 17) {
      const height =
        (-3.283 +
          0.9352 * (data.height / 2.54) +
          (-0.000247 * data.weight) / 0.4536 +
          0.1251 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    }
  } else {
    if (data.age <= 10) {
      const height =
        (0.33468 +
          0.82771 * (data.height / 2.54) +
          (-0.007397 * data.weight) / 0.4536 +
          0.37312 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if ((data.age = 11)) {
      const height =
        (3.50436 +
          0.67173 * (data.height / 2.54) +
          (-0.006136 * data.weight) / 0.4536 +
          0.42042 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if ((data.age = 12)) {
      const height =
        (4.84365 +
          0.64452 * (data.height / 2.54) +
          (-0.004894 * data.weight) / 0.4536 +
          0.3949 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if ((data.age = 13)) {
      const height =
        (3.21417 +
          0.7226 * (data.height / 2.54) +
          (-0.003661 * data.weight) / 0.4536 +
          0.31163 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if ((data.age = 14)) {
      const height =
        (0.32425 +
          0.85062 * (data.height / 2.54) +
          (-0.0025 * data.weight) / 0.4536 +
          0.20235 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if ((data.age = 15)) {
      const height =
        (-2.35055 +
          0.97319 * (data.height / 2.54) +
          (-0.001477 * data.weight) / 0.4536 +
          0.0988 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if ((data.age = 16)) {
      const height =
        (-3.17885 +
          1.03496 * (data.height / 2.54) +
          (-0.000655 * data.weight) / 0.4536 +
          0.03272 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    } else if (data.age >= 17) {
      const height =
        (-0.65579 +
          0.98054 * (data.height / 2.54) +
          (-0.0001 * data.weight) / 0.4536 +
          0.03584 *
            ((2.316 +
              0.955 * (data.fathers_height / 2.54) +
              (2.803 + 0.953 * (data.mothers_height / 2.54))) /
              2)) *
        2.54;
      return Math.round(height);
    }
  }
  return 0;
};

export const calculateMaturityStatus = (height: number, predictedHeight: number) => {
  const maturity = (height / predictedHeight) * 100;
  return Math.round(maturity);
};
