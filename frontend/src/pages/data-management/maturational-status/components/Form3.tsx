import { Form, Input, InputNumber, Row, Col, Select, FormInstance, message } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import { PubertyData, NationalHeightAvg, HeightPredicted } from '@/types/ypi';
import { useState, useEffect } from 'react';
import { get } from '@/services/ypi/api';

const { Option } = Select;

const getLocaleKey = (fieldName: string) => {
  return 'pages.maturational-status.' + fieldName;
};

export default ({
  form3,
  form1,
}: {
  form3: FormInstance<PubertyData>;
  form1: FormInstance<HeightPredicted>;
}) => {
  const intl = useIntl();
  const [nationData, setNationData] = useState<NationalHeightAvg[]>([]);
  const [heightsData, setHeightsData] = useState<NationalHeightAvg[]>([]);
  const id = Form.useWatch('id', form3);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const fetchDependenciesData = async () => {
    try {
      const nationRes = await get('national_height_avgs?nations=true');
      setNationData(nationRes.data);
    } catch (error) {
      message.error(
        intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Dependecies Data' }),
      );
    }
  };

  useEffect(() => {
    fetchDependenciesData();
  }, []);

  useEffect(() => {
    setNationalAverageAge();
  }, [heightsData]);

  const setAutoCompleteField = () => {
    const values = form3.getFieldsValue();

    if (values.nation_id) {
      const selectedNation = nationData.find((d) => d.id == values.nation_id);
      const gender = form1.getFieldValue('gender');
      get(`national_height_avgs?nation=${selectedNation?.nation}&gender=${gender}`)
        .then((res) => {
          setHeightsData(res.data);
        })
        .catch((err) =>
          message.error(
            intl.formatMessage({ id: 'message.errorGet' }, { fieldName: 'Heights Data' }),
          ),
        );
    }

    if (values.nation_id || values.age) {
      setNationalAverageAge();
    }

    setBiologicalCategory();
  };

  useEffect(() => {
    setAutoCompleteField();
  }, [id]);

  const setNationalAverageAge = () => {
    if (heightsData.length > 0) {
      const age = form3.getFieldValue('age');
      const selectedHeight = heightsData.find((d) => d.age == age);
      if (selectedHeight) {
        form3.setFieldsValue({ national_average: selectedHeight.height });
      } else {
        form3.setFieldsValue({ national_average: heightsData[heightsData.length - 1].height });
      }

      setBiologicalCategory();
    }
  };

  const setBiologicalCategory = () => {
    const values = form3.getFieldsValue();
    if (values.growth_rate && values.height && values.national_average) {
      if (values.growth_rate < 8 && values.height >= values.national_average) {
        form3.setFieldsValue({
          biological_category_text: intl.formatMessage({ id: getLocaleKey('post_puberty') }),
          biological_category: 0,
        });
      } else if (values.growth_rate >= 8) {
        form3.setFieldsValue({
          biological_category_text: intl.formatMessage({ id: getLocaleKey('puberty') }),
          biological_category: 1,
        });
      } else if (values.growth_rate < 8 && values.height < values.national_average) {
        form3.setFieldsValue({
          biological_category_text: intl.formatMessage({ id: getLocaleKey('pre_puberty') }),
          biological_category: 2,
        });
      } else {
        form3.setFieldsValue({ biological_category_text: 'None', biological_category: 99 });
      }
    }
  };

  const onValuesChange = (changedValues: any, values: PubertyData) => {
    setAutoCompleteField();
  };

  return (
    <Form size="small" form={form3} name="form_3" onValuesChange={onValuesChange} layout="vertical">
      <Form.Item name="id" hidden />
      <Form.Item name="height_predicted_id" hidden />
      <Form.Item name="pemain_id" hidden />
      <Form.Item name="dob" hidden />

      <Row gutter={12}>
        <Col span={4}>
          <Form.Item
            name="age"
            label={<FormattedMessage id={getLocaleKey('age')} />}
            rules={getRequiredRule('age')}
          >
            <InputNumber
              type="number"
              style={{ width: '100%' }}
              addonAfter={<FormattedMessage id={getLocaleKey('year')} />}
            />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item
            name="height"
            label={<FormattedMessage id={getLocaleKey('height')} />}
            rules={getRequiredRule('height')}
          >
            <InputNumber
              type="number"
              style={{ width: '100%' }}
              addonAfter={<FormattedMessage id={getLocaleKey('cm')} />}
            />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item
            name="nation_id"
            label={<FormattedMessage id={getLocaleKey('nation')} />}
            rules={getRequiredRule('nation')}
          >
            <Select>
              {nationData.map((d) => (
                <Option key={d.id} value={d.id}>
                  {d.nation}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item
            name="national_average"
            label={<FormattedMessage id={getLocaleKey('national_height_avg')} />}
            rules={getRequiredRule('national_height_avg')}
          >
            <Input disabled addonAfter={<FormattedMessage id={getLocaleKey('cm')} />} />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item
            name="growth_rate"
            label={<FormattedMessage id={getLocaleKey('growth_rate')} />}
            rules={getRequiredRule('growth_rate')}
          >
            <Input disabled addonAfter={<FormattedMessage id={getLocaleKey('cm')} />} />
          </Form.Item>
        </Col>

        <Col span={4}>
          <Form.Item
            name="biological_category_text"
            label={<FormattedMessage id={getLocaleKey('biological_category')} />}
            rules={getRequiredRule('biological_category')}
          >
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>
      <Form.Item name="biological_category" hidden />
    </Form>
  );
};
