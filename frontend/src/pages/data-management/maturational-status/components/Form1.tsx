import { Form, Input, InputNumber, Row, Col, Select, FormInstance } from 'antd';
import { useIntl, FormattedMessage } from 'umi';
import { HeightPredicted, PubertyData } from '@/types/ypi';
import { calculatePredictedHeight, calculateMaturityStatus } from './utils';
import { useEffect, useContext } from 'react';
import moment from 'moment';
import { Context } from '../index';
moment.locale('en-US');

const { Option } = Select;

const getLocaleKey = (fieldName: string) => {
  return 'pages.maturational-status.' + fieldName;
};

export default ({
  form1,
  form3,
}: {
  form1: FormInstance<HeightPredicted>;
  form3: FormInstance<PubertyData>;
}) => {
  const intl = useIntl();
  const playerId = Form.useWatch('pemain_id', form1);
  const { playerData } = useContext(Context);

  const getRequiredRule = (fieldName: string) => {
    return [
      {
        required: true,
        message: intl.formatMessage(
          { id: 'message.pleaseInputField' },
          { fieldName: intl.formatMessage({ id: getLocaleKey(fieldName) }) },
        ),
      },
    ];
  };

  const onPlayerIdChange = () => {
    const selectedPlayer = playerData?.find((d) => d.id == playerId);

    form1.setFieldsValue({
      dob: selectedPlayer?.tanggal_lahir.substring(0, 10),
      gender: selectedPlayer?.sex,
    });

    // form3.setFieldsValue({
    //   age: moment().diff(selectedPlayer?.tanggal_lahir, 'years'),
    // });
  };

  const setPredictMaturityStatus = () => {
    const values = form1.getFieldsValue();
    if (
      values.age &&
      values.height &&
      values.weight &&
      values.fathers_height &&
      values.mothers_height &&
      values.pemain_id
    ) {
      const predictedHight = calculatePredictedHeight(values);
      form1.setFieldsValue({
        predicted_height: predictedHight,
        maturity_status: calculateMaturityStatus(values.height, predictedHight),
      });
    }
  };

  const onForm1Change = (changedValues: any, values: HeightPredicted) => {
    if (changedValues.pemain_id) {
      onPlayerIdChange();
    }

    setPredictMaturityStatus();
  };

  const setAutoCompleteField = async () => {
    if (playerId) {
      onPlayerIdChange();
      setPredictMaturityStatus();
    }
  };

  useEffect(() => {
    setAutoCompleteField();
  }, [playerId]);

  useEffect(() => {
    onPlayerIdChange();
  }, [playerData]);

  return (
    <Form size="small" form={form1} name="form_1" onValuesChange={onForm1Change} layout="vertical">
      <Form.Item name="id" hidden />
      <Form.Item name="date_predicted" hidden />

      <Row gutter={12}>
        <Col span={8}>
          <Form.Item
            name="pemain_id"
            label={<FormattedMessage id={getLocaleKey('player_name')} />}
            rules={getRequiredRule('player_name')}
          >
            <Select>
              {playerData?.map((d) => (
                <Option key={d.id} value={d.id}>
                  {d.nama_lengkap}
                </Option>
              ))}
            </Select>
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item name="dob" label={<FormattedMessage id={getLocaleKey('dob')} />}>
            <Input disabled />
          </Form.Item>
        </Col>

        <Col span={8}>
          <Form.Item name="gender" label={<FormattedMessage id={getLocaleKey('gender')} />}>
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>

      <Row gutter={12}>
        <Col span={3}>
          <Form.Item
            name="age"
            label={<FormattedMessage id={getLocaleKey('age')} />}
            rules={getRequiredRule('age')}
          >
            <InputNumber type="number" style={{ width: '100%' }} />
          </Form.Item>
        </Col>

        <Col span={3}>
          <Form.Item
            name="height"
            label={<FormattedMessage id={getLocaleKey('height')} />}
            rules={getRequiredRule('height')}
          >
            <InputNumber type="number" style={{ width: '100%' }} />
          </Form.Item>
        </Col>

        <Col span={3}>
          <Form.Item
            name="weight"
            label={<FormattedMessage id={getLocaleKey('weight')} />}
            rules={getRequiredRule('weight')}
          >
            <InputNumber type="number" style={{ width: '100%' }} />
          </Form.Item>
        </Col>

        <Col span={3}>
          <Form.Item
            name="fathers_height"
            label={<FormattedMessage id={getLocaleKey('f_height')} />}
            rules={getRequiredRule('f_height')}
          >
            <InputNumber type="number" style={{ width: '100%' }} />
          </Form.Item>
        </Col>

        <Col span={3}>
          <Form.Item
            name="mothers_height"
            label={<FormattedMessage id={getLocaleKey('m_height')} />}
            rules={getRequiredRule('m_height')}
          >
            <InputNumber type="number" style={{ width: '100%' }} />
          </Form.Item>
        </Col>

        <Col span={6}>
          <Form.Item
            name="predicted_height"
            label={<FormattedMessage id={getLocaleKey('predicted_mature_height')} />}
            rules={getRequiredRule('predicted_mature_height')}
          >
            <Input disabled />
          </Form.Item>
        </Col>

        <Col span={3}>
          <Form.Item
            name="maturity_status"
            label={<FormattedMessage id={getLocaleKey('maturity_status')} />}
            rules={getRequiredRule('maturity_status')}
          >
            <Input disabled />
          </Form.Item>
        </Col>
      </Row>
    </Form>
  );
};
