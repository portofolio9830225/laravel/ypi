import { Table, TableColumnsType, Button, Space, Tooltip, Popconfirm, message } from 'antd';
import { EditOutlined, DeleteOutlined, PrinterOutlined } from '@ant-design/icons';
import { useContext } from 'react';
import { destroy } from '@/services/ypi/api';
import { Context } from '../index';
import { useIntl } from 'umi';
import styles from './table.less';
import { HeightPredicted } from '@/types/ypi';

export default ({ data }: { data: HeightPredicted[] }) => {
  const intl = useIntl();
  const { setEditedData, getTableData, editedData, setPrintData } = useContext(Context);

  const onEdit = (record: HeightPredicted) => {
    setEditedData(record);
  };

  const handlePrint = (record: HeightPredicted) => {
    setPrintData(record);
  };

  const onDelete = (record: HeightPredicted) => {
    destroy('height_predicteds', record.id)
      .then((res) => {
        message.success(
          intl.formatMessage({ id: 'message.successDelete' }, { moduleName: 'delete' }),
        );
        getTableData();
      })
      .catch((err) => {
        message.error(intl.formatMessage({ id: 'message.errorDelete' }, { moduleName: 'delete' }));
      });
  };

  const columns: TableColumnsType<HeightPredicted> = [
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.player_name' }),
      dataIndex: ['player_data', 'nama_lengkap'],
      key: 'player_name',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.gender' }),
      dataIndex: ['player_data', 'sex'],
      key: 'gender',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.dob' }),
      dataIndex: 'dob',
      key: 'dob',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.age' }),
      dataIndex: 'age',
      key: 'age',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.height' }),
      dataIndex: 'height',
      key: 'height',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.weight' }),
      dataIndex: 'weight',
      key: 'weight',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.f_height' }),
      dataIndex: 'fathers_height',
      key: 'fathers_height',
    },
    {
      title: intl.formatMessage({ id: 'pages.maturational-status.m_height' }),
      dataIndex: 'mothers_height',
      key: 'mothers_height',
    },
    {
      title: intl.formatMessage({ id: 'crud.action' }),
      key: 'action',
      fixed: 'right',
      render: (_, record) => (
        <Space>
          <Tooltip title="Edit">
            <Button
              type="primary"
              shape="circle"
              onClick={() => onEdit(record)}
              size="small"
              icon={<EditOutlined />}
            />
          </Tooltip>
          <Tooltip title="Delete">
            <Popconfirm
              title={intl.formatMessage(
                { id: 'message.confirmDeleteMessage' },
                { moduleName: 'Data' },
              )}
              onConfirm={() => onDelete(record)}
              okText={intl.formatMessage({ id: 'crud.yes' })}
              cancelText={intl.formatMessage({ id: 'crud.no' })}
            >
              <Button type="primary" shape="circle" size="small" danger icon={<DeleteOutlined />} />
            </Popconfirm>
          </Tooltip>
          <Tooltip title="Print">
            <Button
              onClick={() => handlePrint(record)}
              shape="circle"
              icon={<PrinterOutlined />}
              size="small"
            />
          </Tooltip>
        </Space>
      ),
    },
  ];

  const rowClassName = (record: HeightPredicted, index: number) => {
    if (editedData?.key == record.key) return styles.record_edit;
    return '';
  };

  return (
    <Table
      className={styles.custom_table}
      size="small"
      rowClassName={rowClassName}
      columns={columns}
      scroll={{ x: true }}
      dataSource={data}
    />
  );
};
