import { User } from './types/ypi';

/**
 * @see https://umijs.org/zh-CN/plugins/plugin-access
 * */
export default function access(initialState: { currentUser: User }) {
  const { currentUser } = initialState || {};

  const getCoachId = (user: User): number | null => {
    if (user && user.role === 'coach') {
      return user.as_coach_id;
    }
    return null;
  };

  return {
    canAdmin: currentUser && currentUser.role === 'superadmin',
    canEditClub:
      currentUser && (currentUser.role === 'club_admin' || currentUser.role === 'superadmin'),
    coachId: getCoachId(currentUser),
  };
}
