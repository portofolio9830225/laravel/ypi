import { extend, ResponseError } from 'umi-request';
import { BASE_URL } from './constant';
import { history } from 'umi';

const errorHandler = (error: ResponseError<any>) => {
  if (
    error.response.statusText === 'Unauthenticated' ||
    error.response.statusText === 'Unauthenticated.' ||
    error.response.statusText === 'Unauthorized'
  ) {
    history.push('/user/login');
  }
};

let headers;

if (localStorage.getItem('club') !== null) {
  headers = {
    Authorization: `Bearer ${localStorage.getItem('token') || sessionStorage.getItem('token')}`,
    Accept: `application/json`,
    'X-Tenant': JSON.parse(localStorage.getItem('club') as string).id,
  };
} else {
  headers = {
    Authorization: `Bearer ${localStorage.getItem('token') || sessionStorage.getItem('token')}`,
    Accept: `application/json`,
  };
}

export const request = extend({
  prefix: BASE_URL,
  headers,
  errorHandler,
});
