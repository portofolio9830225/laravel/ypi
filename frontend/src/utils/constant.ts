export const BASE_URL = 'http://127.0.0.1:8000';

// SP = Season Planner
// SPD = Season Planner Detail
export const DEFAULT_SP_COLOR = '#ffe6b5';

export const SP_1 = 'DETAIL AND SPORTS & STUDY BALANCE';
export const SPD_1_1 = 'Target';
export const SPD_1_2 = 'Rate Important of Competition';
export const SPD_1_3 = 'Date for Fitness Test';
export const SPD_1_4 = 'Academy Index (1-5)';
export const SPD_1_5 = 'Sport Index (1-5)';
export const SPD_1_6 = 'Public Holiday';

export const SP_2 = 'PERIODIZATION';
export const SPD_2_1 = 'Training Phase';
export const SPD_2_2 = 'Sub Phase';
export const SPD_2_3 = 'Macro Cycle';
export const SPD_2_4 = 'Messo Cycle';
export const SPD_2_5 = 'Micro Cycle';

export const SP_3 = 'TECHNICAL DEVELOPMENT';
export const SPD_3_1 = 'Front Area';
export const SPD_3_2 = 'Middle Area';
export const SPD_3_3 = 'Baseline Area';
export const SPD_3_4 = 'Badminton Step';
export const SPD_3_5 = 'Micro';

export const SP_4 = 'TACTICAL AND STRATEGY';
export const SPD_4_1 = 'Offensive';
export const SPD_4_2 = 'Devensive';
export const SPD_4_3 = 'Counter Attack';
export const SPD_4_4 = 'Deception';

export const SP_5 = 'PHYSICAL DEVELOPMENT';
export const SPD_5_1 = 'Aerobic Capacity';
export const SPD_5_2 = 'An-Aerobic Capacity';
export const SPD_5_3 = 'Strength';
export const SPD_5_4 = 'Flexibility';
export const SPD_5_5 = 'Speed';
export const SPD_5_6 = 'Nutrition';

export const SP_6 = 'MENTAL SKILLS';
export const SPD_6_1 = 'Team Cohesion';
export const SPD_6_2 = 'Goal Setting';
export const SPD_6_3 = 'Imagery';
export const SPD_6_4 = 'Self Talk';
export const SPD_6_5 = 'Concentration';
export const SPD_6_6 = 'Strategies for Competition';
export const SPD_6_7 = 'Application';

export const SP_7 = 'PROGRESS';
export const SPD_7_1 = 'Monitoring/Evaluation';

export const SP_8 = '% WORKOUT & TIME';
export const SPD_8_1 = '% Intensity';
export const SPD_8_2 = '% Volume';
export const SPD_8_3 = '% Spesific';
export const SPD_8_4 = 'No of hours per week';
export const SPD_8_5 = 'Grand total no of hours';

export const MENU_FILE_COLOR = '#FD646E';
export const MENU_REPORT_COLOR = '#B560C1';
export const MENU_DATA_MANAGEMENT_COLOR = '#00B3ED';
export const MENU_INDIVIDUAL_COLOR = '#00D3AF';
export const MENU_MASTER_PLAN_COLOR = '#FFC345';
export const MENU_SYSTEM_COLOR = '#494949';

export const SECONDARY_BUTTON_COLOR = '#ffa940';

export const YTP_DATE_COLOR = '#99ccff';
export const YTP_SEASON_PLAN_COLOR = '#ff6600';

export const SP_STUDY_BALANCE_ID = 1;
export const SP_WORK_TIME_ID = 14;

export const FIXED_SP_LIST = [SP_STUDY_BALANCE_ID];
// export const FIXED_SP_LIST = []; //for disabling fixed mode

// SPORT STUDY BALANCE
export const SPD_TARGET_ID = 2;
export const SPD_RATE_IMPORTANT_ID = 3;
export const SPD_DATE_FITNESS_ID = 13;
export const SPD_ACADEMY_INDEX_ID = 14;
export const SPD_SPORT_INDEX_ID = 15;
export const SPD_PUBLIC_HOLIDAY_ID = 16;
// PERIODIZATION
export const SPD_TRAINING_PHASE_ID = 4;
export const SPD_SUB_PHASE_ID = 10;
// WORKOUT TIME
export const SPD_INTENSITY_ID = 12;
export const SPD_VOLUME_ID = 20;
export const SPD_SPESIFIC_ID = 21;

export const FIXED_SPD_LIST = [
  SPD_TRAINING_PHASE_ID,
  SPD_SUB_PHASE_ID,
  SPD_INTENSITY_ID,
  SPD_VOLUME_ID,
  SPD_SPESIFIC_ID,
];
// export const FIXED_SPD_LIST = [];
