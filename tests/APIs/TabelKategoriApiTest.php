<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TabelKategori;

class TabelKategoriApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tabel_kategoris', $tabelKategori
        );

        $this->assertApiResponse($tabelKategori);
    }

    /**
     * @test
     */
    public function test_read_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/tabel_kategoris/'.$tabelKategori->id
        );

        $this->assertApiResponse($tabelKategori->toArray());
    }

    /**
     * @test
     */
    public function test_update_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->create();
        $editedTabelKategori = TabelKategori::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tabel_kategoris/'.$tabelKategori->id,
            $editedTabelKategori
        );

        $this->assertApiResponse($editedTabelKategori);
    }

    /**
     * @test
     */
    public function test_delete_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tabel_kategoris/'.$tabelKategori->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tabel_kategoris/'.$tabelKategori->id
        );

        $this->response->assertStatus(404);
    }
}
