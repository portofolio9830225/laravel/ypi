<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetailkpiEvent;

class DetailkpiEventApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detailkpi_events', $detailkpiEvent
        );

        $this->assertApiResponse($detailkpiEvent);
    }

    /**
     * @test
     */
    public function test_read_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detailkpi_events/'.$detailkpiEvent->id
        );

        $this->assertApiResponse($detailkpiEvent->toArray());
    }

    /**
     * @test
     */
    public function test_update_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->create();
        $editedDetailkpiEvent = DetailkpiEvent::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detailkpi_events/'.$detailkpiEvent->id,
            $editedDetailkpiEvent
        );

        $this->assertApiResponse($editedDetailkpiEvent);
    }

    /**
     * @test
     */
    public function test_delete_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detailkpi_events/'.$detailkpiEvent->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detailkpi_events/'.$detailkpiEvent->id
        );

        $this->response->assertStatus(404);
    }
}
