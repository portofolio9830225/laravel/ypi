<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\YpiEvent;

class YpiEventApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ypi_events', $ypiEvent
        );

        $this->assertApiResponse($ypiEvent);
    }

    /**
     * @test
     */
    public function test_read_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ypi_events/'.$ypiEvent->id
        );

        $this->assertApiResponse($ypiEvent->toArray());
    }

    /**
     * @test
     */
    public function test_update_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->create();
        $editedYpiEvent = YpiEvent::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ypi_events/'.$ypiEvent->id,
            $editedYpiEvent
        );

        $this->assertApiResponse($editedYpiEvent);
    }

    /**
     * @test
     */
    public function test_delete_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ypi_events/'.$ypiEvent->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ypi_events/'.$ypiEvent->id
        );

        $this->response->assertStatus(404);
    }
}
