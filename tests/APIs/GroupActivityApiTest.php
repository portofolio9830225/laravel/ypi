<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\GroupActivity;

class GroupActivityApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_group_activity()
    {
        $groupActivity = GroupActivity::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/group_activities', $groupActivity
        );

        $this->assertApiResponse($groupActivity);
    }

    /**
     * @test
     */
    public function test_read_group_activity()
    {
        $groupActivity = GroupActivity::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/group_activities/'.$groupActivity->id
        );

        $this->assertApiResponse($groupActivity->toArray());
    }

    /**
     * @test
     */
    public function test_update_group_activity()
    {
        $groupActivity = GroupActivity::factory()->create();
        $editedGroupActivity = GroupActivity::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/group_activities/'.$groupActivity->id,
            $editedGroupActivity
        );

        $this->assertApiResponse($editedGroupActivity);
    }

    /**
     * @test
     */
    public function test_delete_group_activity()
    {
        $groupActivity = GroupActivity::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/group_activities/'.$groupActivity->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/group_activities/'.$groupActivity->id
        );

        $this->response->assertStatus(404);
    }
}
