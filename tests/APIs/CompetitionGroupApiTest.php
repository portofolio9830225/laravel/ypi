<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CompetitionGroup;

class CompetitionGroupApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/competition_groups', $competitionGroup
        );

        $this->assertApiResponse($competitionGroup);
    }

    /**
     * @test
     */
    public function test_read_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/competition_groups/'.$competitionGroup->id
        );

        $this->assertApiResponse($competitionGroup->toArray());
    }

    /**
     * @test
     */
    public function test_update_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->create();
        $editedCompetitionGroup = CompetitionGroup::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/competition_groups/'.$competitionGroup->id,
            $editedCompetitionGroup
        );

        $this->assertApiResponse($editedCompetitionGroup);
    }

    /**
     * @test
     */
    public function test_delete_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/competition_groups/'.$competitionGroup->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/competition_groups/'.$competitionGroup->id
        );

        $this->response->assertStatus(404);
    }
}
