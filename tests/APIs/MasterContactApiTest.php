<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MasterContact;

class MasterContactApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_master_contact()
    {
        $masterContact = MasterContact::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/master_contacts', $masterContact
        );

        $this->assertApiResponse($masterContact);
    }

    /**
     * @test
     */
    public function test_read_master_contact()
    {
        $masterContact = MasterContact::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/master_contacts/'.$masterContact->id
        );

        $this->assertApiResponse($masterContact->toArray());
    }

    /**
     * @test
     */
    public function test_update_master_contact()
    {
        $masterContact = MasterContact::factory()->create();
        $editedMasterContact = MasterContact::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/master_contacts/'.$masterContact->id,
            $editedMasterContact
        );

        $this->assertApiResponse($editedMasterContact);
    }

    /**
     * @test
     */
    public function test_delete_master_contact()
    {
        $masterContact = MasterContact::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/master_contacts/'.$masterContact->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/master_contacts/'.$masterContact->id
        );

        $this->response->assertStatus(404);
    }
}
