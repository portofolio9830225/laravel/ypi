<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FitnessTest;

class FitnessTestApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/fitness_tests', $fitnessTest
        );

        $this->assertApiResponse($fitnessTest);
    }

    /**
     * @test
     */
    public function test_read_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/fitness_tests/'.$fitnessTest->id
        );

        $this->assertApiResponse($fitnessTest->toArray());
    }

    /**
     * @test
     */
    public function test_update_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->create();
        $editedFitnessTest = FitnessTest::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/fitness_tests/'.$fitnessTest->id,
            $editedFitnessTest
        );

        $this->assertApiResponse($editedFitnessTest);
    }

    /**
     * @test
     */
    public function test_delete_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/fitness_tests/'.$fitnessTest->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/fitness_tests/'.$fitnessTest->id
        );

        $this->response->assertStatus(404);
    }
}
