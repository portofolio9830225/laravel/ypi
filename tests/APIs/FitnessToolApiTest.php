<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\FitnessTool;

class FitnessToolApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/fitness_tools', $fitnessTool
        );

        $this->assertApiResponse($fitnessTool);
    }

    /**
     * @test
     */
    public function test_read_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/fitness_tools/'.$fitnessTool->id
        );

        $this->assertApiResponse($fitnessTool->toArray());
    }

    /**
     * @test
     */
    public function test_update_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->create();
        $editedFitnessTool = FitnessTool::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/fitness_tools/'.$fitnessTool->id,
            $editedFitnessTool
        );

        $this->assertApiResponse($editedFitnessTool);
    }

    /**
     * @test
     */
    public function test_delete_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/fitness_tools/'.$fitnessTool->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/fitness_tools/'.$fitnessTool->id
        );

        $this->response->assertStatus(404);
    }
}
