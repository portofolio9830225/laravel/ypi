<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ResultCategory;

class ResultCategoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_result_category()
    {
        $resultCategory = ResultCategory::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/result_categories', $resultCategory
        );

        $this->assertApiResponse($resultCategory);
    }

    /**
     * @test
     */
    public function test_read_result_category()
    {
        $resultCategory = ResultCategory::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/result_categories/'.$resultCategory->id
        );

        $this->assertApiResponse($resultCategory->toArray());
    }

    /**
     * @test
     */
    public function test_update_result_category()
    {
        $resultCategory = ResultCategory::factory()->create();
        $editedResultCategory = ResultCategory::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/result_categories/'.$resultCategory->id,
            $editedResultCategory
        );

        $this->assertApiResponse($editedResultCategory);
    }

    /**
     * @test
     */
    public function test_delete_result_category()
    {
        $resultCategory = ResultCategory::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/result_categories/'.$resultCategory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/result_categories/'.$resultCategory->id
        );

        $this->response->assertStatus(404);
    }
}
