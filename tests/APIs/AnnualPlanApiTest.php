<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AnnualPlan;

class AnnualPlanApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/annual_plans', $annualPlan
        );

        $this->assertApiResponse($annualPlan);
    }

    /**
     * @test
     */
    public function test_read_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/annual_plans/'.$annualPlan->id
        );

        $this->assertApiResponse($annualPlan->toArray());
    }

    /**
     * @test
     */
    public function test_update_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->create();
        $editedAnnualPlan = AnnualPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/annual_plans/'.$annualPlan->id,
            $editedAnnualPlan
        );

        $this->assertApiResponse($editedAnnualPlan);
    }

    /**
     * @test
     */
    public function test_delete_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/annual_plans/'.$annualPlan->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/annual_plans/'.$annualPlan->id
        );

        $this->response->assertStatus(404);
    }
}
