<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetailGaps;

class DetailGapsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detail_gaps', $detailGaps
        );

        $this->assertApiResponse($detailGaps);
    }

    /**
     * @test
     */
    public function test_read_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detail_gaps/'.$detailGaps->id
        );

        $this->assertApiResponse($detailGaps->toArray());
    }

    /**
     * @test
     */
    public function test_update_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->create();
        $editedDetailGaps = DetailGaps::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detail_gaps/'.$detailGaps->id,
            $editedDetailGaps
        );

        $this->assertApiResponse($editedDetailGaps);
    }

    /**
     * @test
     */
    public function test_delete_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detail_gaps/'.$detailGaps->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detail_gaps/'.$detailGaps->id
        );

        $this->response->assertStatus(404);
    }
}
