<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SubExercise;

class SubExerciseApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_sub_exercise()
    {
        $subExercise = SubExercise::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/sub_exercises', $subExercise
        );

        $this->assertApiResponse($subExercise);
    }

    /**
     * @test
     */
    public function test_read_sub_exercise()
    {
        $subExercise = SubExercise::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/sub_exercises/'.$subExercise->id
        );

        $this->assertApiResponse($subExercise->toArray());
    }

    /**
     * @test
     */
    public function test_update_sub_exercise()
    {
        $subExercise = SubExercise::factory()->create();
        $editedSubExercise = SubExercise::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/sub_exercises/'.$subExercise->id,
            $editedSubExercise
        );

        $this->assertApiResponse($editedSubExercise);
    }

    /**
     * @test
     */
    public function test_delete_sub_exercise()
    {
        $subExercise = SubExercise::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/sub_exercises/'.$subExercise->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/sub_exercises/'.$subExercise->id
        );

        $this->response->assertStatus(404);
    }
}
