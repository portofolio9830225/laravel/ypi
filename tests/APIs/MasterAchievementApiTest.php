<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MasterAchievement;

class MasterAchievementApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/master_achievements', $masterAchievement
        );

        $this->assertApiResponse($masterAchievement);
    }

    /**
     * @test
     */
    public function test_read_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/master_achievements/'.$masterAchievement->id
        );

        $this->assertApiResponse($masterAchievement->toArray());
    }

    /**
     * @test
     */
    public function test_update_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->create();
        $editedMasterAchievement = MasterAchievement::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/master_achievements/'.$masterAchievement->id,
            $editedMasterAchievement
        );

        $this->assertApiResponse($editedMasterAchievement);
    }

    /**
     * @test
     */
    public function test_delete_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/master_achievements/'.$masterAchievement->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/master_achievements/'.$masterAchievement->id
        );

        $this->response->assertStatus(404);
    }
}
