<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeasonPlan;

class SeasonPlanApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/season_plans', $seasonPlan
        );

        $this->assertApiResponse($seasonPlan);
    }

    /**
     * @test
     */
    public function test_read_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/season_plans/'.$seasonPlan->id
        );

        $this->assertApiResponse($seasonPlan->toArray());
    }

    /**
     * @test
     */
    public function test_update_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->create();
        $editedSeasonPlan = SeasonPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/season_plans/'.$seasonPlan->id,
            $editedSeasonPlan
        );

        $this->assertApiResponse($editedSeasonPlan);
    }

    /**
     * @test
     */
    public function test_delete_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/season_plans/'.$seasonPlan->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/season_plans/'.$seasonPlan->id
        );

        $this->response->assertStatus(404);
    }
}
