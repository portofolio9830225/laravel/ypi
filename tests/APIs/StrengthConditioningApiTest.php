<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StrengthConditioning;

class StrengthConditioningApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/strength_conditionings', $strengthConditioning
        );

        $this->assertApiResponse($strengthConditioning);
    }

    /**
     * @test
     */
    public function test_read_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/strength_conditionings/'.$strengthConditioning->id
        );

        $this->assertApiResponse($strengthConditioning->toArray());
    }

    /**
     * @test
     */
    public function test_update_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->create();
        $editedStrengthConditioning = StrengthConditioning::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/strength_conditionings/'.$strengthConditioning->id,
            $editedStrengthConditioning
        );

        $this->assertApiResponse($editedStrengthConditioning);
    }

    /**
     * @test
     */
    public function test_delete_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/strength_conditionings/'.$strengthConditioning->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/strength_conditionings/'.$strengthConditioning->id
        );

        $this->response->assertStatus(404);
    }
}
