<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\YtpCharts;

class YtpChartsApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ytp_charts', $ytpCharts
        );

        $this->assertApiResponse($ytpCharts);
    }

    /**
     * @test
     */
    public function test_read_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ytp_charts/'.$ytpCharts->id
        );

        $this->assertApiResponse($ytpCharts->toArray());
    }

    /**
     * @test
     */
    public function test_update_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->create();
        $editedYtpCharts = YtpCharts::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ytp_charts/'.$ytpCharts->id,
            $editedYtpCharts
        );

        $this->assertApiResponse($editedYtpCharts);
    }

    /**
     * @test
     */
    public function test_delete_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ytp_charts/'.$ytpCharts->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ytp_charts/'.$ytpCharts->id
        );

        $this->response->assertStatus(404);
    }
}
