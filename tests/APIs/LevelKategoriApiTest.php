<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\LevelKategori;

class LevelKategoriApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/level_kategoris', $levelKategori
        );

        $this->assertApiResponse($levelKategori);
    }

    /**
     * @test
     */
    public function test_read_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/level_kategoris/'.$levelKategori->id
        );

        $this->assertApiResponse($levelKategori->toArray());
    }

    /**
     * @test
     */
    public function test_update_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->create();
        $editedLevelKategori = LevelKategori::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/level_kategoris/'.$levelKategori->id,
            $editedLevelKategori
        );

        $this->assertApiResponse($editedLevelKategori);
    }

    /**
     * @test
     */
    public function test_delete_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/level_kategoris/'.$levelKategori->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/level_kategoris/'.$levelKategori->id
        );

        $this->response->assertStatus(404);
    }
}
