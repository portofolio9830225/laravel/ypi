<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeasonPlanDetail;

class SeasonPlanDetailApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/season_plan_details', $seasonPlanDetail
        );

        $this->assertApiResponse($seasonPlanDetail);
    }

    /**
     * @test
     */
    public function test_read_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/season_plan_details/'.$seasonPlanDetail->id
        );

        $this->assertApiResponse($seasonPlanDetail->toArray());
    }

    /**
     * @test
     */
    public function test_update_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->create();
        $editedSeasonPlanDetail = SeasonPlanDetail::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/season_plan_details/'.$seasonPlanDetail->id,
            $editedSeasonPlanDetail
        );

        $this->assertApiResponse($editedSeasonPlanDetail);
    }

    /**
     * @test
     */
    public function test_delete_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/season_plan_details/'.$seasonPlanDetail->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/season_plan_details/'.$seasonPlanDetail->id
        );

        $this->response->assertStatus(404);
    }
}
