<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HeightPredicted;

class HeightPredictedApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/height_predicteds', $heightPredicted
        );

        $this->assertApiResponse($heightPredicted);
    }

    /**
     * @test
     */
    public function test_read_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/height_predicteds/'.$heightPredicted->id
        );

        $this->assertApiResponse($heightPredicted->toArray());
    }

    /**
     * @test
     */
    public function test_update_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->create();
        $editedHeightPredicted = HeightPredicted::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/height_predicteds/'.$heightPredicted->id,
            $editedHeightPredicted
        );

        $this->assertApiResponse($editedHeightPredicted);
    }

    /**
     * @test
     */
    public function test_delete_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/height_predicteds/'.$heightPredicted->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/height_predicteds/'.$heightPredicted->id
        );

        $this->response->assertStatus(404);
    }
}
