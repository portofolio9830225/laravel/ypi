<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MasterPemain;

class MasterPemainApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/master_pemains', $masterPemain
        );

        $this->assertApiResponse($masterPemain);
    }

    /**
     * @test
     */
    public function test_read_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/master_pemains/'.$masterPemain->id
        );

        $this->assertApiResponse($masterPemain->toArray());
    }

    /**
     * @test
     */
    public function test_update_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->create();
        $editedMasterPemain = MasterPemain::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/master_pemains/'.$masterPemain->id,
            $editedMasterPemain
        );

        $this->assertApiResponse($editedMasterPemain);
    }

    /**
     * @test
     */
    public function test_delete_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/master_pemains/'.$masterPemain->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/master_pemains/'.$masterPemain->id
        );

        $this->response->assertStatus(404);
    }
}
