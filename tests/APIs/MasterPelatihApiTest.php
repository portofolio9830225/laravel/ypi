<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MasterPelatih;

class MasterPelatihApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/master_pelatihs', $masterPelatih
        );

        $this->assertApiResponse($masterPelatih);
    }

    /**
     * @test
     */
    public function test_read_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/master_pelatihs/'.$masterPelatih->id
        );

        $this->assertApiResponse($masterPelatih->toArray());
    }

    /**
     * @test
     */
    public function test_update_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->create();
        $editedMasterPelatih = MasterPelatih::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/master_pelatihs/'.$masterPelatih->id,
            $editedMasterPelatih
        );

        $this->assertApiResponse($editedMasterPelatih);
    }

    /**
     * @test
     */
    public function test_delete_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/master_pelatihs/'.$masterPelatih->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/master_pelatihs/'.$masterPelatih->id
        );

        $this->response->assertStatus(404);
    }
}
