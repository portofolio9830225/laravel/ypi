<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StrengthConditionValue;

class StrengthConditionValueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/strength_condition_values', $strengthConditionValue
        );

        $this->assertApiResponse($strengthConditionValue);
    }

    /**
     * @test
     */
    public function test_read_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/strength_condition_values/'.$strengthConditionValue->id
        );

        $this->assertApiResponse($strengthConditionValue->toArray());
    }

    /**
     * @test
     */
    public function test_update_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->create();
        $editedStrengthConditionValue = StrengthConditionValue::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/strength_condition_values/'.$strengthConditionValue->id,
            $editedStrengthConditionValue
        );

        $this->assertApiResponse($editedStrengthConditionValue);
    }

    /**
     * @test
     */
    public function test_delete_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/strength_condition_values/'.$strengthConditionValue->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/strength_condition_values/'.$strengthConditionValue->id
        );

        $this->response->assertStatus(404);
    }
}
