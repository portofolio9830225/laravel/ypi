<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\GymType;

class GymTypeApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_gym_type()
    {
        $gymType = GymType::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/gym_types', $gymType
        );

        $this->assertApiResponse($gymType);
    }

    /**
     * @test
     */
    public function test_read_gym_type()
    {
        $gymType = GymType::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/gym_types/'.$gymType->id
        );

        $this->assertApiResponse($gymType->toArray());
    }

    /**
     * @test
     */
    public function test_update_gym_type()
    {
        $gymType = GymType::factory()->create();
        $editedGymType = GymType::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/gym_types/'.$gymType->id,
            $editedGymType
        );

        $this->assertApiResponse($editedGymType);
    }

    /**
     * @test
     */
    public function test_delete_gym_type()
    {
        $gymType = GymType::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/gym_types/'.$gymType->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/gym_types/'.$gymType->id
        );

        $this->response->assertStatus(404);
    }
}
