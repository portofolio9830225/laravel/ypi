<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StrengthConditioningData;

class StrengthConditioningDataApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/strength_conditioning_datas', $strengthConditioningData
        );

        $this->assertApiResponse($strengthConditioningData);
    }

    /**
     * @test
     */
    public function test_read_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/strength_conditioning_datas/'.$strengthConditioningData->id
        );

        $this->assertApiResponse($strengthConditioningData->toArray());
    }

    /**
     * @test
     */
    public function test_update_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->create();
        $editedStrengthConditioningData = StrengthConditioningData::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/strength_conditioning_datas/'.$strengthConditioningData->id,
            $editedStrengthConditioningData
        );

        $this->assertApiResponse($editedStrengthConditioningData);
    }

    /**
     * @test
     */
    public function test_delete_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/strength_conditioning_datas/'.$strengthConditioningData->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/strength_conditioning_datas/'.$strengthConditioningData->id
        );

        $this->response->assertStatus(404);
    }
}
