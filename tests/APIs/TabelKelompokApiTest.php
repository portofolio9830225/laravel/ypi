<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TabelKelompok;

class TabelKelompokApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/tabel_kelompoks', $tabelKelompok
        );

        $this->assertApiResponse($tabelKelompok);
    }

    /**
     * @test
     */
    public function test_read_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/tabel_kelompoks/'.$tabelKelompok->id
        );

        $this->assertApiResponse($tabelKelompok->toArray());
    }

    /**
     * @test
     */
    public function test_update_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->create();
        $editedTabelKelompok = TabelKelompok::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/tabel_kelompoks/'.$tabelKelompok->id,
            $editedTabelKelompok
        );

        $this->assertApiResponse($editedTabelKelompok);
    }

    /**
     * @test
     */
    public function test_delete_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/tabel_kelompoks/'.$tabelKelompok->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/tabel_kelompoks/'.$tabelKelompok->id
        );

        $this->response->assertStatus(404);
    }
}
