<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\AnnualPlanSetting;

class AnnualPlanSettingApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/annual_plan_settings', $annualPlanSetting
        );

        $this->assertApiResponse($annualPlanSetting);
    }

    /**
     * @test
     */
    public function test_read_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/annual_plan_settings/'.$annualPlanSetting->id
        );

        $this->assertApiResponse($annualPlanSetting->toArray());
    }

    /**
     * @test
     */
    public function test_update_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->create();
        $editedAnnualPlanSetting = AnnualPlanSetting::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/annual_plan_settings/'.$annualPlanSetting->id,
            $editedAnnualPlanSetting
        );

        $this->assertApiResponse($editedAnnualPlanSetting);
    }

    /**
     * @test
     */
    public function test_delete_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/annual_plan_settings/'.$annualPlanSetting->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/annual_plan_settings/'.$annualPlanSetting->id
        );

        $this->response->assertStatus(404);
    }
}
