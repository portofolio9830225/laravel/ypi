<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetailWeeklyPlan;

class DetailWeeklyPlanApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detail_weekly_plans', $detailWeeklyPlan
        );

        $this->assertApiResponse($detailWeeklyPlan);
    }

    /**
     * @test
     */
    public function test_read_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detail_weekly_plans/'.$detailWeeklyPlan->id
        );

        $this->assertApiResponse($detailWeeklyPlan->toArray());
    }

    /**
     * @test
     */
    public function test_update_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->create();
        $editedDetailWeeklyPlan = DetailWeeklyPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detail_weekly_plans/'.$detailWeeklyPlan->id,
            $editedDetailWeeklyPlan
        );

        $this->assertApiResponse($editedDetailWeeklyPlan);
    }

    /**
     * @test
     */
    public function test_delete_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detail_weekly_plans/'.$detailWeeklyPlan->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detail_weekly_plans/'.$detailWeeklyPlan->id
        );

        $this->response->assertStatus(404);
    }
}
