<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\Kpi;

class KpiApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_kpi()
    {
        $kpi = Kpi::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/kpis', $kpi
        );

        $this->assertApiResponse($kpi);
    }

    /**
     * @test
     */
    public function test_read_kpi()
    {
        $kpi = Kpi::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/kpis/'.$kpi->id
        );

        $this->assertApiResponse($kpi->toArray());
    }

    /**
     * @test
     */
    public function test_update_kpi()
    {
        $kpi = Kpi::factory()->create();
        $editedKpi = Kpi::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/kpis/'.$kpi->id,
            $editedKpi
        );

        $this->assertApiResponse($editedKpi);
    }

    /**
     * @test
     */
    public function test_delete_kpi()
    {
        $kpi = Kpi::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/kpis/'.$kpi->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/kpis/'.$kpi->id
        );

        $this->response->assertStatus(404);
    }
}
