<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HeaderWeeklyPlan;

class HeaderWeeklyPlanApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/header_weekly_plans', $headerWeeklyPlan
        );

        $this->assertApiResponse($headerWeeklyPlan);
    }

    /**
     * @test
     */
    public function test_read_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/header_weekly_plans/'.$headerWeeklyPlan->id
        );

        $this->assertApiResponse($headerWeeklyPlan->toArray());
    }

    /**
     * @test
     */
    public function test_update_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->create();
        $editedHeaderWeeklyPlan = HeaderWeeklyPlan::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/header_weekly_plans/'.$headerWeeklyPlan->id,
            $editedHeaderWeeklyPlan
        );

        $this->assertApiResponse($editedHeaderWeeklyPlan);
    }

    /**
     * @test
     */
    public function test_delete_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/header_weekly_plans/'.$headerWeeklyPlan->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/header_weekly_plans/'.$headerWeeklyPlan->id
        );

        $this->response->assertStatus(404);
    }
}
