<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HeightData;

class HeightDataApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_height_data()
    {
        $heightData = HeightData::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/height_datas', $heightData
        );

        $this->assertApiResponse($heightData);
    }

    /**
     * @test
     */
    public function test_read_height_data()
    {
        $heightData = HeightData::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/height_datas/'.$heightData->id
        );

        $this->assertApiResponse($heightData->toArray());
    }

    /**
     * @test
     */
    public function test_update_height_data()
    {
        $heightData = HeightData::factory()->create();
        $editedHeightData = HeightData::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/height_datas/'.$heightData->id,
            $editedHeightData
        );

        $this->assertApiResponse($editedHeightData);
    }

    /**
     * @test
     */
    public function test_delete_height_data()
    {
        $heightData = HeightData::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/height_datas/'.$heightData->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/height_datas/'.$heightData->id
        );

        $this->response->assertStatus(404);
    }
}
