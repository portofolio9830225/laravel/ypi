<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeasonPlanner;

class SeasonPlannerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/season_planners', $seasonPlanner
        );

        $this->assertApiResponse($seasonPlanner);
    }

    /**
     * @test
     */
    public function test_read_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/season_planners/'.$seasonPlanner->id
        );

        $this->assertApiResponse($seasonPlanner->toArray());
    }

    /**
     * @test
     */
    public function test_update_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->create();
        $editedSeasonPlanner = SeasonPlanner::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/season_planners/'.$seasonPlanner->id,
            $editedSeasonPlanner
        );

        $this->assertApiResponse($editedSeasonPlanner);
    }

    /**
     * @test
     */
    public function test_delete_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/season_planners/'.$seasonPlanner->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/season_planners/'.$seasonPlanner->id
        );

        $this->response->assertStatus(404);
    }
}
