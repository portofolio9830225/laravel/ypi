<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\MasterYpi;

class MasterYpiApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/master_ypis', $masterYpi
        );

        $this->assertApiResponse($masterYpi);
    }

    /**
     * @test
     */
    public function test_read_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/master_ypis/'.$masterYpi->id
        );

        $this->assertApiResponse($masterYpi->toArray());
    }

    /**
     * @test
     */
    public function test_update_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->create();
        $editedMasterYpi = MasterYpi::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/master_ypis/'.$masterYpi->id,
            $editedMasterYpi
        );

        $this->assertApiResponse($editedMasterYpi);
    }

    /**
     * @test
     */
    public function test_delete_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/master_ypis/'.$masterYpi->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/master_ypis/'.$masterYpi->id
        );

        $this->response->assertStatus(404);
    }
}
