<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\KpiSubindicator;

class KpiSubindicatorApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/kpi_subindicators', $kpiSubindicator
        );

        $this->assertApiResponse($kpiSubindicator);
    }

    /**
     * @test
     */
    public function test_read_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/kpi_subindicators/'.$kpiSubindicator->id
        );

        $this->assertApiResponse($kpiSubindicator->toArray());
    }

    /**
     * @test
     */
    public function test_update_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->create();
        $editedKpiSubindicator = KpiSubindicator::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/kpi_subindicators/'.$kpiSubindicator->id,
            $editedKpiSubindicator
        );

        $this->assertApiResponse($editedKpiSubindicator);
    }

    /**
     * @test
     */
    public function test_delete_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/kpi_subindicators/'.$kpiSubindicator->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/kpi_subindicators/'.$kpiSubindicator->id
        );

        $this->response->assertStatus(404);
    }
}
