<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\TrainingAttendance;

class TrainingAttendanceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/training_attendances', $trainingAttendance
        );

        $this->assertApiResponse($trainingAttendance);
    }

    /**
     * @test
     */
    public function test_read_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/training_attendances/'.$trainingAttendance->id
        );

        $this->assertApiResponse($trainingAttendance->toArray());
    }

    /**
     * @test
     */
    public function test_update_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->create();
        $editedTrainingAttendance = TrainingAttendance::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/training_attendances/'.$trainingAttendance->id,
            $editedTrainingAttendance
        );

        $this->assertApiResponse($editedTrainingAttendance);
    }

    /**
     * @test
     */
    public function test_delete_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/training_attendances/'.$trainingAttendance->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/training_attendances/'.$trainingAttendance->id
        );

        $this->response->assertStatus(404);
    }
}
