<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CompetitionReport;

class CompetitionReportApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/competition_reports', $competitionReport
        );

        $this->assertApiResponse($competitionReport);
    }

    /**
     * @test
     */
    public function test_read_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/competition_reports/'.$competitionReport->id
        );

        $this->assertApiResponse($competitionReport->toArray());
    }

    /**
     * @test
     */
    public function test_update_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->create();
        $editedCompetitionReport = CompetitionReport::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/competition_reports/'.$competitionReport->id,
            $editedCompetitionReport
        );

        $this->assertApiResponse($editedCompetitionReport);
    }

    /**
     * @test
     */
    public function test_delete_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/competition_reports/'.$competitionReport->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/competition_reports/'.$competitionReport->id
        );

        $this->response->assertStatus(404);
    }
}
