<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SettingAttendance;

class SettingAttendanceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/setting_attendances', $settingAttendance
        );

        $this->assertApiResponse($settingAttendance);
    }

    /**
     * @test
     */
    public function test_read_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/setting_attendances/'.$settingAttendance->id
        );

        $this->assertApiResponse($settingAttendance->toArray());
    }

    /**
     * @test
     */
    public function test_update_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->create();
        $editedSettingAttendance = SettingAttendance::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/setting_attendances/'.$settingAttendance->id,
            $editedSettingAttendance
        );

        $this->assertApiResponse($editedSettingAttendance);
    }

    /**
     * @test
     */
    public function test_delete_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/setting_attendances/'.$settingAttendance->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/setting_attendances/'.$settingAttendance->id
        );

        $this->response->assertStatus(404);
    }
}
