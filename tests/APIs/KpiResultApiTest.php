<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\KpiResult;

class KpiResultApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_kpi_result()
    {
        $kpiResult = KpiResult::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/kpi_results', $kpiResult
        );

        $this->assertApiResponse($kpiResult);
    }

    /**
     * @test
     */
    public function test_read_kpi_result()
    {
        $kpiResult = KpiResult::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/kpi_results/'.$kpiResult->id
        );

        $this->assertApiResponse($kpiResult->toArray());
    }

    /**
     * @test
     */
    public function test_update_kpi_result()
    {
        $kpiResult = KpiResult::factory()->create();
        $editedKpiResult = KpiResult::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/kpi_results/'.$kpiResult->id,
            $editedKpiResult
        );

        $this->assertApiResponse($editedKpiResult);
    }

    /**
     * @test
     */
    public function test_delete_kpi_result()
    {
        $kpiResult = KpiResult::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/kpi_results/'.$kpiResult->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/kpi_results/'.$kpiResult->id
        );

        $this->response->assertStatus(404);
    }
}
