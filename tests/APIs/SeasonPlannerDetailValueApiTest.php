<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeasonPlannerDetailValue;

class SeasonPlannerDetailValueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/season_planner_detail_values', $seasonPlannerDetailValue
        );

        $this->assertApiResponse($seasonPlannerDetailValue);
    }

    /**
     * @test
     */
    public function test_read_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/season_planner_detail_values/'.$seasonPlannerDetailValue->id
        );

        $this->assertApiResponse($seasonPlannerDetailValue->toArray());
    }

    /**
     * @test
     */
    public function test_update_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->create();
        $editedSeasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/season_planner_detail_values/'.$seasonPlannerDetailValue->id,
            $editedSeasonPlannerDetailValue
        );

        $this->assertApiResponse($editedSeasonPlannerDetailValue);
    }

    /**
     * @test
     */
    public function test_delete_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/season_planner_detail_values/'.$seasonPlannerDetailValue->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/season_planner_detail_values/'.$seasonPlannerDetailValue->id
        );

        $this->response->assertStatus(404);
    }
}
