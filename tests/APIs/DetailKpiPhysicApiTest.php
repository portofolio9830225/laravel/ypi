<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetailKpiPhysic;

class DetailKpiPhysicApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detail_kpi_physics', $detailKpiPhysic
        );

        $this->assertApiResponse($detailKpiPhysic);
    }

    /**
     * @test
     */
    public function test_read_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detail_kpi_physics/'.$detailKpiPhysic->id
        );

        $this->assertApiResponse($detailKpiPhysic->toArray());
    }

    /**
     * @test
     */
    public function test_update_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->create();
        $editedDetailKpiPhysic = DetailKpiPhysic::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detail_kpi_physics/'.$detailKpiPhysic->id,
            $editedDetailKpiPhysic
        );

        $this->assertApiResponse($editedDetailKpiPhysic);
    }

    /**
     * @test
     */
    public function test_delete_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detail_kpi_physics/'.$detailKpiPhysic->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detail_kpi_physics/'.$detailKpiPhysic->id
        );

        $this->response->assertStatus(404);
    }
}
