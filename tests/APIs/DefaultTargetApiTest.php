<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DefaultTarget;

class DefaultTargetApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/default_targets', $defaultTarget
        );

        $this->assertApiResponse($defaultTarget);
    }

    /**
     * @test
     */
    public function test_read_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/default_targets/'.$defaultTarget->id
        );

        $this->assertApiResponse($defaultTarget->toArray());
    }

    /**
     * @test
     */
    public function test_update_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->create();
        $editedDefaultTarget = DefaultTarget::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/default_targets/'.$defaultTarget->id,
            $editedDefaultTarget
        );

        $this->assertApiResponse($editedDefaultTarget);
    }

    /**
     * @test
     */
    public function test_delete_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/default_targets/'.$defaultTarget->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/default_targets/'.$defaultTarget->id
        );

        $this->response->assertStatus(404);
    }
}
