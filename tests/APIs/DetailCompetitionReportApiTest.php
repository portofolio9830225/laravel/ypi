<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetailCompetitionReport;

class DetailCompetitionReportApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detail_competition_reports', $detailCompetitionReport
        );

        $this->assertApiResponse($detailCompetitionReport);
    }

    /**
     * @test
     */
    public function test_read_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detail_competition_reports/'.$detailCompetitionReport->id
        );

        $this->assertApiResponse($detailCompetitionReport->toArray());
    }

    /**
     * @test
     */
    public function test_update_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->create();
        $editedDetailCompetitionReport = DetailCompetitionReport::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detail_competition_reports/'.$detailCompetitionReport->id,
            $editedDetailCompetitionReport
        );

        $this->assertApiResponse($editedDetailCompetitionReport);
    }

    /**
     * @test
     */
    public function test_delete_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detail_competition_reports/'.$detailCompetitionReport->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detail_competition_reports/'.$detailCompetitionReport->id
        );

        $this->response->assertStatus(404);
    }
}
