<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\YpiGoal;

class YpiGoalApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/ypi_goals', $ypiGoal
        );

        $this->assertApiResponse($ypiGoal);
    }

    /**
     * @test
     */
    public function test_read_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/ypi_goals/'.$ypiGoal->id
        );

        $this->assertApiResponse($ypiGoal->toArray());
    }

    /**
     * @test
     */
    public function test_update_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->create();
        $editedYpiGoal = YpiGoal::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/ypi_goals/'.$ypiGoal->id,
            $editedYpiGoal
        );

        $this->assertApiResponse($editedYpiGoal);
    }

    /**
     * @test
     */
    public function test_delete_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/ypi_goals/'.$ypiGoal->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/ypi_goals/'.$ypiGoal->id
        );

        $this->response->assertStatus(404);
    }
}
