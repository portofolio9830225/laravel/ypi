<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SettingPresence;

class SettingPresenceApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/setting_presences', $settingPresence
        );

        $this->assertApiResponse($settingPresence);
    }

    /**
     * @test
     */
    public function test_read_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/setting_presences/'.$settingPresence->id
        );

        $this->assertApiResponse($settingPresence->toArray());
    }

    /**
     * @test
     */
    public function test_update_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->create();
        $editedSettingPresence = SettingPresence::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/setting_presences/'.$settingPresence->id,
            $editedSettingPresence
        );

        $this->assertApiResponse($editedSettingPresence);
    }

    /**
     * @test
     */
    public function test_delete_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/setting_presences/'.$settingPresence->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/setting_presences/'.$settingPresence->id
        );

        $this->response->assertStatus(404);
    }
}
