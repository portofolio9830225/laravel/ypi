<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\CompetitionStatus;

class CompetitionStatusApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/competition_statuses', $competitionStatus
        );

        $this->assertApiResponse($competitionStatus);
    }

    /**
     * @test
     */
    public function test_read_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/competition_statuses/'.$competitionStatus->id
        );

        $this->assertApiResponse($competitionStatus->toArray());
    }

    /**
     * @test
     */
    public function test_update_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->create();
        $editedCompetitionStatus = CompetitionStatus::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/competition_statuses/'.$competitionStatus->id,
            $editedCompetitionStatus
        );

        $this->assertApiResponse($editedCompetitionStatus);
    }

    /**
     * @test
     */
    public function test_delete_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/competition_statuses/'.$competitionStatus->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/competition_statuses/'.$competitionStatus->id
        );

        $this->response->assertStatus(404);
    }
}
