<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeasonPlannerDetail;

class SeasonPlannerDetailApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/season_planner_details', $seasonPlannerDetail
        );

        $this->assertApiResponse($seasonPlannerDetail);
    }

    /**
     * @test
     */
    public function test_read_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/season_planner_details/'.$seasonPlannerDetail->id
        );

        $this->assertApiResponse($seasonPlannerDetail->toArray());
    }

    /**
     * @test
     */
    public function test_update_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->create();
        $editedSeasonPlannerDetail = SeasonPlannerDetail::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/season_planner_details/'.$seasonPlannerDetail->id,
            $editedSeasonPlannerDetail
        );

        $this->assertApiResponse($editedSeasonPlannerDetail);
    }

    /**
     * @test
     */
    public function test_delete_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/season_planner_details/'.$seasonPlannerDetail->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/season_planner_details/'.$seasonPlannerDetail->id
        );

        $this->response->assertStatus(404);
    }
}
