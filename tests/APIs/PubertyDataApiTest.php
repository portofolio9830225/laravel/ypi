<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\PubertyData;

class PubertyDataApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_puberty_data()
    {
        $pubertyData = PubertyData::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/puberty_datas', $pubertyData
        );

        $this->assertApiResponse($pubertyData);
    }

    /**
     * @test
     */
    public function test_read_puberty_data()
    {
        $pubertyData = PubertyData::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/puberty_datas/'.$pubertyData->id
        );

        $this->assertApiResponse($pubertyData->toArray());
    }

    /**
     * @test
     */
    public function test_update_puberty_data()
    {
        $pubertyData = PubertyData::factory()->create();
        $editedPubertyData = PubertyData::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/puberty_datas/'.$pubertyData->id,
            $editedPubertyData
        );

        $this->assertApiResponse($editedPubertyData);
    }

    /**
     * @test
     */
    public function test_delete_puberty_data()
    {
        $pubertyData = PubertyData::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/puberty_datas/'.$pubertyData->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/puberty_datas/'.$pubertyData->id
        );

        $this->response->assertStatus(404);
    }
}
