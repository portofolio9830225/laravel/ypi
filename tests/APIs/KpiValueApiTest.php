<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\KpiValue;

class KpiValueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_kpi_value()
    {
        $kpiValue = KpiValue::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/kpi_values', $kpiValue
        );

        $this->assertApiResponse($kpiValue);
    }

    /**
     * @test
     */
    public function test_read_kpi_value()
    {
        $kpiValue = KpiValue::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/kpi_values/'.$kpiValue->id
        );

        $this->assertApiResponse($kpiValue->toArray());
    }

    /**
     * @test
     */
    public function test_update_kpi_value()
    {
        $kpiValue = KpiValue::factory()->create();
        $editedKpiValue = KpiValue::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/kpi_values/'.$kpiValue->id,
            $editedKpiValue
        );

        $this->assertApiResponse($editedKpiValue);
    }

    /**
     * @test
     */
    public function test_delete_kpi_value()
    {
        $kpiValue = KpiValue::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/kpi_values/'.$kpiValue->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/kpi_values/'.$kpiValue->id
        );

        $this->response->assertStatus(404);
    }
}
