<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HeaderKpi;

class HeaderKpiApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/header_kpis', $headerKpi
        );

        $this->assertApiResponse($headerKpi);
    }

    /**
     * @test
     */
    public function test_read_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/header_kpis/'.$headerKpi->id
        );

        $this->assertApiResponse($headerKpi->toArray());
    }

    /**
     * @test
     */
    public function test_update_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->create();
        $editedHeaderKpi = HeaderKpi::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/header_kpis/'.$headerKpi->id,
            $editedHeaderKpi
        );

        $this->assertApiResponse($editedHeaderKpi);
    }

    /**
     * @test
     */
    public function test_delete_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/header_kpis/'.$headerKpi->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/header_kpis/'.$headerKpi->id
        );

        $this->response->assertStatus(404);
    }
}
