<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\KpiParameter;

class KpiParameterApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/kpi_parameters', $kpiParameter
        );

        $this->assertApiResponse($kpiParameter);
    }

    /**
     * @test
     */
    public function test_read_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/kpi_parameters/'.$kpiParameter->id
        );

        $this->assertApiResponse($kpiParameter->toArray());
    }

    /**
     * @test
     */
    public function test_update_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->create();
        $editedKpiParameter = KpiParameter::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/kpi_parameters/'.$kpiParameter->id,
            $editedKpiParameter
        );

        $this->assertApiResponse($editedKpiParameter);
    }

    /**
     * @test
     */
    public function test_delete_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/kpi_parameters/'.$kpiParameter->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/kpi_parameters/'.$kpiParameter->id
        );

        $this->response->assertStatus(404);
    }
}
