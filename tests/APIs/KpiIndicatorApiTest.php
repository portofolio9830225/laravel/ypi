<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\KpiIndicator;

class KpiIndicatorApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/kpi_indicators', $kpiIndicator
        );

        $this->assertApiResponse($kpiIndicator);
    }

    /**
     * @test
     */
    public function test_read_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/kpi_indicators/'.$kpiIndicator->id
        );

        $this->assertApiResponse($kpiIndicator->toArray());
    }

    /**
     * @test
     */
    public function test_update_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->create();
        $editedKpiIndicator = KpiIndicator::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/kpi_indicators/'.$kpiIndicator->id,
            $editedKpiIndicator
        );

        $this->assertApiResponse($editedKpiIndicator);
    }

    /**
     * @test
     */
    public function test_delete_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/kpi_indicators/'.$kpiIndicator->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/kpi_indicators/'.$kpiIndicator->id
        );

        $this->response->assertStatus(404);
    }
}
