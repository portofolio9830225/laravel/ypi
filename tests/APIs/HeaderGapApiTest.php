<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\HeaderGap;

class HeaderGapApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_header_gap()
    {
        $headerGap = HeaderGap::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/header_gaps', $headerGap
        );

        $this->assertApiResponse($headerGap);
    }

    /**
     * @test
     */
    public function test_read_header_gap()
    {
        $headerGap = HeaderGap::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/header_gaps/'.$headerGap->id
        );

        $this->assertApiResponse($headerGap->toArray());
    }

    /**
     * @test
     */
    public function test_update_header_gap()
    {
        $headerGap = HeaderGap::factory()->create();
        $editedHeaderGap = HeaderGap::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/header_gaps/'.$headerGap->id,
            $editedHeaderGap
        );

        $this->assertApiResponse($editedHeaderGap);
    }

    /**
     * @test
     */
    public function test_delete_header_gap()
    {
        $headerGap = HeaderGap::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/header_gaps/'.$headerGap->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/header_gaps/'.$headerGap->id
        );

        $this->response->assertStatus(404);
    }
}
