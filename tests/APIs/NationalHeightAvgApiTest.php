<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\NationalHeightAvg;

class NationalHeightAvgApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/national_height_avgs', $nationalHeightAvg
        );

        $this->assertApiResponse($nationalHeightAvg);
    }

    /**
     * @test
     */
    public function test_read_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/national_height_avgs/'.$nationalHeightAvg->id
        );

        $this->assertApiResponse($nationalHeightAvg->toArray());
    }

    /**
     * @test
     */
    public function test_update_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->create();
        $editedNationalHeightAvg = NationalHeightAvg::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/national_height_avgs/'.$nationalHeightAvg->id,
            $editedNationalHeightAvg
        );

        $this->assertApiResponse($editedNationalHeightAvg);
    }

    /**
     * @test
     */
    public function test_delete_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/national_height_avgs/'.$nationalHeightAvg->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/national_height_avgs/'.$nationalHeightAvg->id
        );

        $this->response->assertStatus(404);
    }
}
