<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\SeasonPlanValue;

class SeasonPlanValueApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/season_plan_values', $seasonPlanValue
        );

        $this->assertApiResponse($seasonPlanValue);
    }

    /**
     * @test
     */
    public function test_read_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/season_plan_values/'.$seasonPlanValue->id
        );

        $this->assertApiResponse($seasonPlanValue->toArray());
    }

    /**
     * @test
     */
    public function test_update_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->create();
        $editedSeasonPlanValue = SeasonPlanValue::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/season_plan_values/'.$seasonPlanValue->id,
            $editedSeasonPlanValue
        );

        $this->assertApiResponse($editedSeasonPlanValue);
    }

    /**
     * @test
     */
    public function test_delete_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/season_plan_values/'.$seasonPlanValue->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/season_plan_values/'.$seasonPlanValue->id
        );

        $this->response->assertStatus(404);
    }
}
