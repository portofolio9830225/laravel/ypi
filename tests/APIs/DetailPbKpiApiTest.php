<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\DetailPbKpi;

class DetailPbKpiApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/detail_pb_kpis', $detailPbKpi
        );

        $this->assertApiResponse($detailPbKpi);
    }

    /**
     * @test
     */
    public function test_read_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/detail_pb_kpis/'.$detailPbKpi->id
        );

        $this->assertApiResponse($detailPbKpi->toArray());
    }

    /**
     * @test
     */
    public function test_update_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->create();
        $editedDetailPbKpi = DetailPbKpi::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/detail_pb_kpis/'.$detailPbKpi->id,
            $editedDetailPbKpi
        );

        $this->assertApiResponse($editedDetailPbKpi);
    }

    /**
     * @test
     */
    public function test_delete_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/detail_pb_kpis/'.$detailPbKpi->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/detail_pb_kpis/'.$detailPbKpi->id
        );

        $this->response->assertStatus(404);
    }
}
