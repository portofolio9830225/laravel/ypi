<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\ExerciseCategory;

class ExerciseCategoryApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/exercise_categories', $exerciseCategory
        );

        $this->assertApiResponse($exerciseCategory);
    }

    /**
     * @test
     */
    public function test_read_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/exercise_categories/'.$exerciseCategory->id
        );

        $this->assertApiResponse($exerciseCategory->toArray());
    }

    /**
     * @test
     */
    public function test_update_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->create();
        $editedExerciseCategory = ExerciseCategory::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/exercise_categories/'.$exerciseCategory->id,
            $editedExerciseCategory
        );

        $this->assertApiResponse($editedExerciseCategory);
    }

    /**
     * @test
     */
    public function test_delete_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/exercise_categories/'.$exerciseCategory->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/exercise_categories/'.$exerciseCategory->id
        );

        $this->response->assertStatus(404);
    }
}
