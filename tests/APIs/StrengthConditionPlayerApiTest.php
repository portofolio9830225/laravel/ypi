<?php namespace Tests\APIs;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;
use App\Models\StrengthConditionPlayer;

class StrengthConditionPlayerApiTest extends TestCase
{
    use ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function test_create_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->make()->toArray();

        $this->response = $this->json(
            'POST',
            '/api/strength_condition_players', $strengthConditionPlayer
        );

        $this->assertApiResponse($strengthConditionPlayer);
    }

    /**
     * @test
     */
    public function test_read_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->create();

        $this->response = $this->json(
            'GET',
            '/api/strength_condition_players/'.$strengthConditionPlayer->id
        );

        $this->assertApiResponse($strengthConditionPlayer->toArray());
    }

    /**
     * @test
     */
    public function test_update_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->create();
        $editedStrengthConditionPlayer = StrengthConditionPlayer::factory()->make()->toArray();

        $this->response = $this->json(
            'PUT',
            '/api/strength_condition_players/'.$strengthConditionPlayer->id,
            $editedStrengthConditionPlayer
        );

        $this->assertApiResponse($editedStrengthConditionPlayer);
    }

    /**
     * @test
     */
    public function test_delete_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->create();

        $this->response = $this->json(
            'DELETE',
             '/api/strength_condition_players/'.$strengthConditionPlayer->id
         );

        $this->assertApiSuccess();
        $this->response = $this->json(
            'GET',
            '/api/strength_condition_players/'.$strengthConditionPlayer->id
        );

        $this->response->assertStatus(404);
    }
}
