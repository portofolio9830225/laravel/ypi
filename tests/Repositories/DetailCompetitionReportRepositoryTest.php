<?php namespace Tests\Repositories;

use App\Models\DetailCompetitionReport;
use App\Repositories\DetailCompetitionReportRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetailCompetitionReportRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetailCompetitionReportRepository
     */
    protected $detailCompetitionReportRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailCompetitionReportRepo = \App::make(DetailCompetitionReportRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->make()->toArray();

        $createdDetailCompetitionReport = $this->detailCompetitionReportRepo->create($detailCompetitionReport);

        $createdDetailCompetitionReport = $createdDetailCompetitionReport->toArray();
        $this->assertArrayHasKey('id', $createdDetailCompetitionReport);
        $this->assertNotNull($createdDetailCompetitionReport['id'], 'Created DetailCompetitionReport must have id specified');
        $this->assertNotNull(DetailCompetitionReport::find($createdDetailCompetitionReport['id']), 'DetailCompetitionReport with given id must be in DB');
        $this->assertModelData($detailCompetitionReport, $createdDetailCompetitionReport);
    }

    /**
     * @test read
     */
    public function test_read_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->create();

        $dbDetailCompetitionReport = $this->detailCompetitionReportRepo->find($detailCompetitionReport->id);

        $dbDetailCompetitionReport = $dbDetailCompetitionReport->toArray();
        $this->assertModelData($detailCompetitionReport->toArray(), $dbDetailCompetitionReport);
    }

    /**
     * @test update
     */
    public function test_update_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->create();
        $fakeDetailCompetitionReport = DetailCompetitionReport::factory()->make()->toArray();

        $updatedDetailCompetitionReport = $this->detailCompetitionReportRepo->update($fakeDetailCompetitionReport, $detailCompetitionReport->id);

        $this->assertModelData($fakeDetailCompetitionReport, $updatedDetailCompetitionReport->toArray());
        $dbDetailCompetitionReport = $this->detailCompetitionReportRepo->find($detailCompetitionReport->id);
        $this->assertModelData($fakeDetailCompetitionReport, $dbDetailCompetitionReport->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detail_competition_report()
    {
        $detailCompetitionReport = DetailCompetitionReport::factory()->create();

        $resp = $this->detailCompetitionReportRepo->delete($detailCompetitionReport->id);

        $this->assertTrue($resp);
        $this->assertNull(DetailCompetitionReport::find($detailCompetitionReport->id), 'DetailCompetitionReport should not exist in DB');
    }
}
