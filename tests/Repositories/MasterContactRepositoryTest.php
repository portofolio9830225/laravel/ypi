<?php namespace Tests\Repositories;

use App\Models\MasterContact;
use App\Repositories\MasterContactRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MasterContactRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MasterContactRepository
     */
    protected $masterContactRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->masterContactRepo = \App::make(MasterContactRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_master_contact()
    {
        $masterContact = MasterContact::factory()->make()->toArray();

        $createdMasterContact = $this->masterContactRepo->create($masterContact);

        $createdMasterContact = $createdMasterContact->toArray();
        $this->assertArrayHasKey('id', $createdMasterContact);
        $this->assertNotNull($createdMasterContact['id'], 'Created MasterContact must have id specified');
        $this->assertNotNull(MasterContact::find($createdMasterContact['id']), 'MasterContact with given id must be in DB');
        $this->assertModelData($masterContact, $createdMasterContact);
    }

    /**
     * @test read
     */
    public function test_read_master_contact()
    {
        $masterContact = MasterContact::factory()->create();

        $dbMasterContact = $this->masterContactRepo->find($masterContact->id);

        $dbMasterContact = $dbMasterContact->toArray();
        $this->assertModelData($masterContact->toArray(), $dbMasterContact);
    }

    /**
     * @test update
     */
    public function test_update_master_contact()
    {
        $masterContact = MasterContact::factory()->create();
        $fakeMasterContact = MasterContact::factory()->make()->toArray();

        $updatedMasterContact = $this->masterContactRepo->update($fakeMasterContact, $masterContact->id);

        $this->assertModelData($fakeMasterContact, $updatedMasterContact->toArray());
        $dbMasterContact = $this->masterContactRepo->find($masterContact->id);
        $this->assertModelData($fakeMasterContact, $dbMasterContact->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_master_contact()
    {
        $masterContact = MasterContact::factory()->create();

        $resp = $this->masterContactRepo->delete($masterContact->id);

        $this->assertTrue($resp);
        $this->assertNull(MasterContact::find($masterContact->id), 'MasterContact should not exist in DB');
    }
}
