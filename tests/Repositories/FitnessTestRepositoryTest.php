<?php namespace Tests\Repositories;

use App\Models\FitnessTest;
use App\Repositories\FitnessTestRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FitnessTestRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FitnessTestRepository
     */
    protected $fitnessTestRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fitnessTestRepo = \App::make(FitnessTestRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->make()->toArray();

        $createdFitnessTest = $this->fitnessTestRepo->create($fitnessTest);

        $createdFitnessTest = $createdFitnessTest->toArray();
        $this->assertArrayHasKey('id', $createdFitnessTest);
        $this->assertNotNull($createdFitnessTest['id'], 'Created FitnessTest must have id specified');
        $this->assertNotNull(FitnessTest::find($createdFitnessTest['id']), 'FitnessTest with given id must be in DB');
        $this->assertModelData($fitnessTest, $createdFitnessTest);
    }

    /**
     * @test read
     */
    public function test_read_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->create();

        $dbFitnessTest = $this->fitnessTestRepo->find($fitnessTest->id);

        $dbFitnessTest = $dbFitnessTest->toArray();
        $this->assertModelData($fitnessTest->toArray(), $dbFitnessTest);
    }

    /**
     * @test update
     */
    public function test_update_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->create();
        $fakeFitnessTest = FitnessTest::factory()->make()->toArray();

        $updatedFitnessTest = $this->fitnessTestRepo->update($fakeFitnessTest, $fitnessTest->id);

        $this->assertModelData($fakeFitnessTest, $updatedFitnessTest->toArray());
        $dbFitnessTest = $this->fitnessTestRepo->find($fitnessTest->id);
        $this->assertModelData($fakeFitnessTest, $dbFitnessTest->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_fitness_test()
    {
        $fitnessTest = FitnessTest::factory()->create();

        $resp = $this->fitnessTestRepo->delete($fitnessTest->id);

        $this->assertTrue($resp);
        $this->assertNull(FitnessTest::find($fitnessTest->id), 'FitnessTest should not exist in DB');
    }
}
