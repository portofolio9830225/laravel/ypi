<?php namespace Tests\Repositories;

use App\Models\HeaderWeeklyPlan;
use App\Repositories\HeaderWeeklyPlanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HeaderWeeklyPlanRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HeaderWeeklyPlanRepository
     */
    protected $headerWeeklyPlanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->headerWeeklyPlanRepo = \App::make(HeaderWeeklyPlanRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->make()->toArray();

        $createdHeaderWeeklyPlan = $this->headerWeeklyPlanRepo->create($headerWeeklyPlan);

        $createdHeaderWeeklyPlan = $createdHeaderWeeklyPlan->toArray();
        $this->assertArrayHasKey('id', $createdHeaderWeeklyPlan);
        $this->assertNotNull($createdHeaderWeeklyPlan['id'], 'Created HeaderWeeklyPlan must have id specified');
        $this->assertNotNull(HeaderWeeklyPlan::find($createdHeaderWeeklyPlan['id']), 'HeaderWeeklyPlan with given id must be in DB');
        $this->assertModelData($headerWeeklyPlan, $createdHeaderWeeklyPlan);
    }

    /**
     * @test read
     */
    public function test_read_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->create();

        $dbHeaderWeeklyPlan = $this->headerWeeklyPlanRepo->find($headerWeeklyPlan->id);

        $dbHeaderWeeklyPlan = $dbHeaderWeeklyPlan->toArray();
        $this->assertModelData($headerWeeklyPlan->toArray(), $dbHeaderWeeklyPlan);
    }

    /**
     * @test update
     */
    public function test_update_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->create();
        $fakeHeaderWeeklyPlan = HeaderWeeklyPlan::factory()->make()->toArray();

        $updatedHeaderWeeklyPlan = $this->headerWeeklyPlanRepo->update($fakeHeaderWeeklyPlan, $headerWeeklyPlan->id);

        $this->assertModelData($fakeHeaderWeeklyPlan, $updatedHeaderWeeklyPlan->toArray());
        $dbHeaderWeeklyPlan = $this->headerWeeklyPlanRepo->find($headerWeeklyPlan->id);
        $this->assertModelData($fakeHeaderWeeklyPlan, $dbHeaderWeeklyPlan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_header_weekly_plan()
    {
        $headerWeeklyPlan = HeaderWeeklyPlan::factory()->create();

        $resp = $this->headerWeeklyPlanRepo->delete($headerWeeklyPlan->id);

        $this->assertTrue($resp);
        $this->assertNull(HeaderWeeklyPlan::find($headerWeeklyPlan->id), 'HeaderWeeklyPlan should not exist in DB');
    }
}
