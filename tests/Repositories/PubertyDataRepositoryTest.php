<?php namespace Tests\Repositories;

use App\Models\PubertyData;
use App\Repositories\PubertyDataRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class PubertyDataRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var PubertyDataRepository
     */
    protected $pubertyDataRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->pubertyDataRepo = \App::make(PubertyDataRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_puberty_data()
    {
        $pubertyData = PubertyData::factory()->make()->toArray();

        $createdPubertyData = $this->pubertyDataRepo->create($pubertyData);

        $createdPubertyData = $createdPubertyData->toArray();
        $this->assertArrayHasKey('id', $createdPubertyData);
        $this->assertNotNull($createdPubertyData['id'], 'Created PubertyData must have id specified');
        $this->assertNotNull(PubertyData::find($createdPubertyData['id']), 'PubertyData with given id must be in DB');
        $this->assertModelData($pubertyData, $createdPubertyData);
    }

    /**
     * @test read
     */
    public function test_read_puberty_data()
    {
        $pubertyData = PubertyData::factory()->create();

        $dbPubertyData = $this->pubertyDataRepo->find($pubertyData->id);

        $dbPubertyData = $dbPubertyData->toArray();
        $this->assertModelData($pubertyData->toArray(), $dbPubertyData);
    }

    /**
     * @test update
     */
    public function test_update_puberty_data()
    {
        $pubertyData = PubertyData::factory()->create();
        $fakePubertyData = PubertyData::factory()->make()->toArray();

        $updatedPubertyData = $this->pubertyDataRepo->update($fakePubertyData, $pubertyData->id);

        $this->assertModelData($fakePubertyData, $updatedPubertyData->toArray());
        $dbPubertyData = $this->pubertyDataRepo->find($pubertyData->id);
        $this->assertModelData($fakePubertyData, $dbPubertyData->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_puberty_data()
    {
        $pubertyData = PubertyData::factory()->create();

        $resp = $this->pubertyDataRepo->delete($pubertyData->id);

        $this->assertTrue($resp);
        $this->assertNull(PubertyData::find($pubertyData->id), 'PubertyData should not exist in DB');
    }
}
