<?php namespace Tests\Repositories;

use App\Models\HeaderKpi;
use App\Repositories\HeaderKpiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HeaderKpiRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HeaderKpiRepository
     */
    protected $headerKpiRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->headerKpiRepo = \App::make(HeaderKpiRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->make()->toArray();

        $createdHeaderKpi = $this->headerKpiRepo->create($headerKpi);

        $createdHeaderKpi = $createdHeaderKpi->toArray();
        $this->assertArrayHasKey('id', $createdHeaderKpi);
        $this->assertNotNull($createdHeaderKpi['id'], 'Created HeaderKpi must have id specified');
        $this->assertNotNull(HeaderKpi::find($createdHeaderKpi['id']), 'HeaderKpi with given id must be in DB');
        $this->assertModelData($headerKpi, $createdHeaderKpi);
    }

    /**
     * @test read
     */
    public function test_read_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->create();

        $dbHeaderKpi = $this->headerKpiRepo->find($headerKpi->id);

        $dbHeaderKpi = $dbHeaderKpi->toArray();
        $this->assertModelData($headerKpi->toArray(), $dbHeaderKpi);
    }

    /**
     * @test update
     */
    public function test_update_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->create();
        $fakeHeaderKpi = HeaderKpi::factory()->make()->toArray();

        $updatedHeaderKpi = $this->headerKpiRepo->update($fakeHeaderKpi, $headerKpi->id);

        $this->assertModelData($fakeHeaderKpi, $updatedHeaderKpi->toArray());
        $dbHeaderKpi = $this->headerKpiRepo->find($headerKpi->id);
        $this->assertModelData($fakeHeaderKpi, $dbHeaderKpi->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_header_kpi()
    {
        $headerKpi = HeaderKpi::factory()->create();

        $resp = $this->headerKpiRepo->delete($headerKpi->id);

        $this->assertTrue($resp);
        $this->assertNull(HeaderKpi::find($headerKpi->id), 'HeaderKpi should not exist in DB');
    }
}
