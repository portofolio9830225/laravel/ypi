<?php namespace Tests\Repositories;

use App\Models\Kpi;
use App\Repositories\KpiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class KpiRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var KpiRepository
     */
    protected $kpiRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->kpiRepo = \App::make(KpiRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_kpi()
    {
        $kpi = Kpi::factory()->make()->toArray();

        $createdKpi = $this->kpiRepo->create($kpi);

        $createdKpi = $createdKpi->toArray();
        $this->assertArrayHasKey('id', $createdKpi);
        $this->assertNotNull($createdKpi['id'], 'Created Kpi must have id specified');
        $this->assertNotNull(Kpi::find($createdKpi['id']), 'Kpi with given id must be in DB');
        $this->assertModelData($kpi, $createdKpi);
    }

    /**
     * @test read
     */
    public function test_read_kpi()
    {
        $kpi = Kpi::factory()->create();

        $dbKpi = $this->kpiRepo->find($kpi->id);

        $dbKpi = $dbKpi->toArray();
        $this->assertModelData($kpi->toArray(), $dbKpi);
    }

    /**
     * @test update
     */
    public function test_update_kpi()
    {
        $kpi = Kpi::factory()->create();
        $fakeKpi = Kpi::factory()->make()->toArray();

        $updatedKpi = $this->kpiRepo->update($fakeKpi, $kpi->id);

        $this->assertModelData($fakeKpi, $updatedKpi->toArray());
        $dbKpi = $this->kpiRepo->find($kpi->id);
        $this->assertModelData($fakeKpi, $dbKpi->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_kpi()
    {
        $kpi = Kpi::factory()->create();

        $resp = $this->kpiRepo->delete($kpi->id);

        $this->assertTrue($resp);
        $this->assertNull(Kpi::find($kpi->id), 'Kpi should not exist in DB');
    }
}
