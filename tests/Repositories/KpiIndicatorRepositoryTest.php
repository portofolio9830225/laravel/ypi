<?php namespace Tests\Repositories;

use App\Models\KpiIndicator;
use App\Repositories\KpiIndicatorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class KpiIndicatorRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var KpiIndicatorRepository
     */
    protected $kpiIndicatorRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->kpiIndicatorRepo = \App::make(KpiIndicatorRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->make()->toArray();

        $createdKpiIndicator = $this->kpiIndicatorRepo->create($kpiIndicator);

        $createdKpiIndicator = $createdKpiIndicator->toArray();
        $this->assertArrayHasKey('id', $createdKpiIndicator);
        $this->assertNotNull($createdKpiIndicator['id'], 'Created KpiIndicator must have id specified');
        $this->assertNotNull(KpiIndicator::find($createdKpiIndicator['id']), 'KpiIndicator with given id must be in DB');
        $this->assertModelData($kpiIndicator, $createdKpiIndicator);
    }

    /**
     * @test read
     */
    public function test_read_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->create();

        $dbKpiIndicator = $this->kpiIndicatorRepo->find($kpiIndicator->id);

        $dbKpiIndicator = $dbKpiIndicator->toArray();
        $this->assertModelData($kpiIndicator->toArray(), $dbKpiIndicator);
    }

    /**
     * @test update
     */
    public function test_update_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->create();
        $fakeKpiIndicator = KpiIndicator::factory()->make()->toArray();

        $updatedKpiIndicator = $this->kpiIndicatorRepo->update($fakeKpiIndicator, $kpiIndicator->id);

        $this->assertModelData($fakeKpiIndicator, $updatedKpiIndicator->toArray());
        $dbKpiIndicator = $this->kpiIndicatorRepo->find($kpiIndicator->id);
        $this->assertModelData($fakeKpiIndicator, $dbKpiIndicator->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_kpi_indicator()
    {
        $kpiIndicator = KpiIndicator::factory()->create();

        $resp = $this->kpiIndicatorRepo->delete($kpiIndicator->id);

        $this->assertTrue($resp);
        $this->assertNull(KpiIndicator::find($kpiIndicator->id), 'KpiIndicator should not exist in DB');
    }
}
