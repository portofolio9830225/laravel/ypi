<?php namespace Tests\Repositories;

use App\Models\TrainingAttendance;
use App\Repositories\TrainingAttendanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TrainingAttendanceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TrainingAttendanceRepository
     */
    protected $trainingAttendanceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->trainingAttendanceRepo = \App::make(TrainingAttendanceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->make()->toArray();

        $createdTrainingAttendance = $this->trainingAttendanceRepo->create($trainingAttendance);

        $createdTrainingAttendance = $createdTrainingAttendance->toArray();
        $this->assertArrayHasKey('id', $createdTrainingAttendance);
        $this->assertNotNull($createdTrainingAttendance['id'], 'Created TrainingAttendance must have id specified');
        $this->assertNotNull(TrainingAttendance::find($createdTrainingAttendance['id']), 'TrainingAttendance with given id must be in DB');
        $this->assertModelData($trainingAttendance, $createdTrainingAttendance);
    }

    /**
     * @test read
     */
    public function test_read_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->create();

        $dbTrainingAttendance = $this->trainingAttendanceRepo->find($trainingAttendance->id);

        $dbTrainingAttendance = $dbTrainingAttendance->toArray();
        $this->assertModelData($trainingAttendance->toArray(), $dbTrainingAttendance);
    }

    /**
     * @test update
     */
    public function test_update_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->create();
        $fakeTrainingAttendance = TrainingAttendance::factory()->make()->toArray();

        $updatedTrainingAttendance = $this->trainingAttendanceRepo->update($fakeTrainingAttendance, $trainingAttendance->id);

        $this->assertModelData($fakeTrainingAttendance, $updatedTrainingAttendance->toArray());
        $dbTrainingAttendance = $this->trainingAttendanceRepo->find($trainingAttendance->id);
        $this->assertModelData($fakeTrainingAttendance, $dbTrainingAttendance->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_training_attendance()
    {
        $trainingAttendance = TrainingAttendance::factory()->create();

        $resp = $this->trainingAttendanceRepo->delete($trainingAttendance->id);

        $this->assertTrue($resp);
        $this->assertNull(TrainingAttendance::find($trainingAttendance->id), 'TrainingAttendance should not exist in DB');
    }
}
