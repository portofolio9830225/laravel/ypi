<?php namespace Tests\Repositories;

use App\Models\DetailWeeklyPlan;
use App\Repositories\DetailWeeklyPlanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetailWeeklyPlanRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetailWeeklyPlanRepository
     */
    protected $detailWeeklyPlanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailWeeklyPlanRepo = \App::make(DetailWeeklyPlanRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->make()->toArray();

        $createdDetailWeeklyPlan = $this->detailWeeklyPlanRepo->create($detailWeeklyPlan);

        $createdDetailWeeklyPlan = $createdDetailWeeklyPlan->toArray();
        $this->assertArrayHasKey('id', $createdDetailWeeklyPlan);
        $this->assertNotNull($createdDetailWeeklyPlan['id'], 'Created DetailWeeklyPlan must have id specified');
        $this->assertNotNull(DetailWeeklyPlan::find($createdDetailWeeklyPlan['id']), 'DetailWeeklyPlan with given id must be in DB');
        $this->assertModelData($detailWeeklyPlan, $createdDetailWeeklyPlan);
    }

    /**
     * @test read
     */
    public function test_read_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->create();

        $dbDetailWeeklyPlan = $this->detailWeeklyPlanRepo->find($detailWeeklyPlan->id);

        $dbDetailWeeklyPlan = $dbDetailWeeklyPlan->toArray();
        $this->assertModelData($detailWeeklyPlan->toArray(), $dbDetailWeeklyPlan);
    }

    /**
     * @test update
     */
    public function test_update_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->create();
        $fakeDetailWeeklyPlan = DetailWeeklyPlan::factory()->make()->toArray();

        $updatedDetailWeeklyPlan = $this->detailWeeklyPlanRepo->update($fakeDetailWeeklyPlan, $detailWeeklyPlan->id);

        $this->assertModelData($fakeDetailWeeklyPlan, $updatedDetailWeeklyPlan->toArray());
        $dbDetailWeeklyPlan = $this->detailWeeklyPlanRepo->find($detailWeeklyPlan->id);
        $this->assertModelData($fakeDetailWeeklyPlan, $dbDetailWeeklyPlan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detail_weekly_plan()
    {
        $detailWeeklyPlan = DetailWeeklyPlan::factory()->create();

        $resp = $this->detailWeeklyPlanRepo->delete($detailWeeklyPlan->id);

        $this->assertTrue($resp);
        $this->assertNull(DetailWeeklyPlan::find($detailWeeklyPlan->id), 'DetailWeeklyPlan should not exist in DB');
    }
}
