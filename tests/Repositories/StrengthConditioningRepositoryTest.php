<?php namespace Tests\Repositories;

use App\Models\StrengthConditioning;
use App\Repositories\StrengthConditioningRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StrengthConditioningRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StrengthConditioningRepository
     */
    protected $strengthConditioningRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->strengthConditioningRepo = \App::make(StrengthConditioningRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->make()->toArray();

        $createdStrengthConditioning = $this->strengthConditioningRepo->create($strengthConditioning);

        $createdStrengthConditioning = $createdStrengthConditioning->toArray();
        $this->assertArrayHasKey('id', $createdStrengthConditioning);
        $this->assertNotNull($createdStrengthConditioning['id'], 'Created StrengthConditioning must have id specified');
        $this->assertNotNull(StrengthConditioning::find($createdStrengthConditioning['id']), 'StrengthConditioning with given id must be in DB');
        $this->assertModelData($strengthConditioning, $createdStrengthConditioning);
    }

    /**
     * @test read
     */
    public function test_read_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->create();

        $dbStrengthConditioning = $this->strengthConditioningRepo->find($strengthConditioning->id);

        $dbStrengthConditioning = $dbStrengthConditioning->toArray();
        $this->assertModelData($strengthConditioning->toArray(), $dbStrengthConditioning);
    }

    /**
     * @test update
     */
    public function test_update_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->create();
        $fakeStrengthConditioning = StrengthConditioning::factory()->make()->toArray();

        $updatedStrengthConditioning = $this->strengthConditioningRepo->update($fakeStrengthConditioning, $strengthConditioning->id);

        $this->assertModelData($fakeStrengthConditioning, $updatedStrengthConditioning->toArray());
        $dbStrengthConditioning = $this->strengthConditioningRepo->find($strengthConditioning->id);
        $this->assertModelData($fakeStrengthConditioning, $dbStrengthConditioning->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_strength_conditioning()
    {
        $strengthConditioning = StrengthConditioning::factory()->create();

        $resp = $this->strengthConditioningRepo->delete($strengthConditioning->id);

        $this->assertTrue($resp);
        $this->assertNull(StrengthConditioning::find($strengthConditioning->id), 'StrengthConditioning should not exist in DB');
    }
}
