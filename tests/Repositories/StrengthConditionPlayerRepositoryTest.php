<?php namespace Tests\Repositories;

use App\Models\StrengthConditionPlayer;
use App\Repositories\StrengthConditionPlayerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StrengthConditionPlayerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StrengthConditionPlayerRepository
     */
    protected $strengthConditionPlayerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->strengthConditionPlayerRepo = \App::make(StrengthConditionPlayerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->make()->toArray();

        $createdStrengthConditionPlayer = $this->strengthConditionPlayerRepo->create($strengthConditionPlayer);

        $createdStrengthConditionPlayer = $createdStrengthConditionPlayer->toArray();
        $this->assertArrayHasKey('id', $createdStrengthConditionPlayer);
        $this->assertNotNull($createdStrengthConditionPlayer['id'], 'Created StrengthConditionPlayer must have id specified');
        $this->assertNotNull(StrengthConditionPlayer::find($createdStrengthConditionPlayer['id']), 'StrengthConditionPlayer with given id must be in DB');
        $this->assertModelData($strengthConditionPlayer, $createdStrengthConditionPlayer);
    }

    /**
     * @test read
     */
    public function test_read_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->create();

        $dbStrengthConditionPlayer = $this->strengthConditionPlayerRepo->find($strengthConditionPlayer->id);

        $dbStrengthConditionPlayer = $dbStrengthConditionPlayer->toArray();
        $this->assertModelData($strengthConditionPlayer->toArray(), $dbStrengthConditionPlayer);
    }

    /**
     * @test update
     */
    public function test_update_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->create();
        $fakeStrengthConditionPlayer = StrengthConditionPlayer::factory()->make()->toArray();

        $updatedStrengthConditionPlayer = $this->strengthConditionPlayerRepo->update($fakeStrengthConditionPlayer, $strengthConditionPlayer->id);

        $this->assertModelData($fakeStrengthConditionPlayer, $updatedStrengthConditionPlayer->toArray());
        $dbStrengthConditionPlayer = $this->strengthConditionPlayerRepo->find($strengthConditionPlayer->id);
        $this->assertModelData($fakeStrengthConditionPlayer, $dbStrengthConditionPlayer->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_strength_condition_player()
    {
        $strengthConditionPlayer = StrengthConditionPlayer::factory()->create();

        $resp = $this->strengthConditionPlayerRepo->delete($strengthConditionPlayer->id);

        $this->assertTrue($resp);
        $this->assertNull(StrengthConditionPlayer::find($strengthConditionPlayer->id), 'StrengthConditionPlayer should not exist in DB');
    }
}
