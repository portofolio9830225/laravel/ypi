<?php namespace Tests\Repositories;

use App\Models\DetailKpiPhysic;
use App\Repositories\DetailKpiPhysicRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetailKpiPhysicRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetailKpiPhysicRepository
     */
    protected $detailKpiPhysicRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailKpiPhysicRepo = \App::make(DetailKpiPhysicRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->make()->toArray();

        $createdDetailKpiPhysic = $this->detailKpiPhysicRepo->create($detailKpiPhysic);

        $createdDetailKpiPhysic = $createdDetailKpiPhysic->toArray();
        $this->assertArrayHasKey('id', $createdDetailKpiPhysic);
        $this->assertNotNull($createdDetailKpiPhysic['id'], 'Created DetailKpiPhysic must have id specified');
        $this->assertNotNull(DetailKpiPhysic::find($createdDetailKpiPhysic['id']), 'DetailKpiPhysic with given id must be in DB');
        $this->assertModelData($detailKpiPhysic, $createdDetailKpiPhysic);
    }

    /**
     * @test read
     */
    public function test_read_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->create();

        $dbDetailKpiPhysic = $this->detailKpiPhysicRepo->find($detailKpiPhysic->id);

        $dbDetailKpiPhysic = $dbDetailKpiPhysic->toArray();
        $this->assertModelData($detailKpiPhysic->toArray(), $dbDetailKpiPhysic);
    }

    /**
     * @test update
     */
    public function test_update_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->create();
        $fakeDetailKpiPhysic = DetailKpiPhysic::factory()->make()->toArray();

        $updatedDetailKpiPhysic = $this->detailKpiPhysicRepo->update($fakeDetailKpiPhysic, $detailKpiPhysic->id);

        $this->assertModelData($fakeDetailKpiPhysic, $updatedDetailKpiPhysic->toArray());
        $dbDetailKpiPhysic = $this->detailKpiPhysicRepo->find($detailKpiPhysic->id);
        $this->assertModelData($fakeDetailKpiPhysic, $dbDetailKpiPhysic->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detail_kpi_physic()
    {
        $detailKpiPhysic = DetailKpiPhysic::factory()->create();

        $resp = $this->detailKpiPhysicRepo->delete($detailKpiPhysic->id);

        $this->assertTrue($resp);
        $this->assertNull(DetailKpiPhysic::find($detailKpiPhysic->id), 'DetailKpiPhysic should not exist in DB');
    }
}
