<?php namespace Tests\Repositories;

use App\Models\ExerciseCategory;
use App\Repositories\ExerciseCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ExerciseCategoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ExerciseCategoryRepository
     */
    protected $exerciseCategoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->exerciseCategoryRepo = \App::make(ExerciseCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->make()->toArray();

        $createdExerciseCategory = $this->exerciseCategoryRepo->create($exerciseCategory);

        $createdExerciseCategory = $createdExerciseCategory->toArray();
        $this->assertArrayHasKey('id', $createdExerciseCategory);
        $this->assertNotNull($createdExerciseCategory['id'], 'Created ExerciseCategory must have id specified');
        $this->assertNotNull(ExerciseCategory::find($createdExerciseCategory['id']), 'ExerciseCategory with given id must be in DB');
        $this->assertModelData($exerciseCategory, $createdExerciseCategory);
    }

    /**
     * @test read
     */
    public function test_read_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->create();

        $dbExerciseCategory = $this->exerciseCategoryRepo->find($exerciseCategory->id);

        $dbExerciseCategory = $dbExerciseCategory->toArray();
        $this->assertModelData($exerciseCategory->toArray(), $dbExerciseCategory);
    }

    /**
     * @test update
     */
    public function test_update_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->create();
        $fakeExerciseCategory = ExerciseCategory::factory()->make()->toArray();

        $updatedExerciseCategory = $this->exerciseCategoryRepo->update($fakeExerciseCategory, $exerciseCategory->id);

        $this->assertModelData($fakeExerciseCategory, $updatedExerciseCategory->toArray());
        $dbExerciseCategory = $this->exerciseCategoryRepo->find($exerciseCategory->id);
        $this->assertModelData($fakeExerciseCategory, $dbExerciseCategory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_exercise_category()
    {
        $exerciseCategory = ExerciseCategory::factory()->create();

        $resp = $this->exerciseCategoryRepo->delete($exerciseCategory->id);

        $this->assertTrue($resp);
        $this->assertNull(ExerciseCategory::find($exerciseCategory->id), 'ExerciseCategory should not exist in DB');
    }
}
