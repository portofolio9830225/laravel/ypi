<?php namespace Tests\Repositories;

use App\Models\AnnualPlanSetting;
use App\Repositories\AnnualPlanSettingRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AnnualPlanSettingRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AnnualPlanSettingRepository
     */
    protected $annualPlanSettingRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->annualPlanSettingRepo = \App::make(AnnualPlanSettingRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->make()->toArray();

        $createdAnnualPlanSetting = $this->annualPlanSettingRepo->create($annualPlanSetting);

        $createdAnnualPlanSetting = $createdAnnualPlanSetting->toArray();
        $this->assertArrayHasKey('id', $createdAnnualPlanSetting);
        $this->assertNotNull($createdAnnualPlanSetting['id'], 'Created AnnualPlanSetting must have id specified');
        $this->assertNotNull(AnnualPlanSetting::find($createdAnnualPlanSetting['id']), 'AnnualPlanSetting with given id must be in DB');
        $this->assertModelData($annualPlanSetting, $createdAnnualPlanSetting);
    }

    /**
     * @test read
     */
    public function test_read_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->create();

        $dbAnnualPlanSetting = $this->annualPlanSettingRepo->find($annualPlanSetting->id);

        $dbAnnualPlanSetting = $dbAnnualPlanSetting->toArray();
        $this->assertModelData($annualPlanSetting->toArray(), $dbAnnualPlanSetting);
    }

    /**
     * @test update
     */
    public function test_update_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->create();
        $fakeAnnualPlanSetting = AnnualPlanSetting::factory()->make()->toArray();

        $updatedAnnualPlanSetting = $this->annualPlanSettingRepo->update($fakeAnnualPlanSetting, $annualPlanSetting->id);

        $this->assertModelData($fakeAnnualPlanSetting, $updatedAnnualPlanSetting->toArray());
        $dbAnnualPlanSetting = $this->annualPlanSettingRepo->find($annualPlanSetting->id);
        $this->assertModelData($fakeAnnualPlanSetting, $dbAnnualPlanSetting->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_annual_plan_setting()
    {
        $annualPlanSetting = AnnualPlanSetting::factory()->create();

        $resp = $this->annualPlanSettingRepo->delete($annualPlanSetting->id);

        $this->assertTrue($resp);
        $this->assertNull(AnnualPlanSetting::find($annualPlanSetting->id), 'AnnualPlanSetting should not exist in DB');
    }
}
