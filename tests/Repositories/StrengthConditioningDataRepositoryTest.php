<?php namespace Tests\Repositories;

use App\Models\StrengthConditioningData;
use App\Repositories\StrengthConditioningDataRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StrengthConditioningDataRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StrengthConditioningDataRepository
     */
    protected $strengthConditioningDataRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->strengthConditioningDataRepo = \App::make(StrengthConditioningDataRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->make()->toArray();

        $createdStrengthConditioningData = $this->strengthConditioningDataRepo->create($strengthConditioningData);

        $createdStrengthConditioningData = $createdStrengthConditioningData->toArray();
        $this->assertArrayHasKey('id', $createdStrengthConditioningData);
        $this->assertNotNull($createdStrengthConditioningData['id'], 'Created StrengthConditioningData must have id specified');
        $this->assertNotNull(StrengthConditioningData::find($createdStrengthConditioningData['id']), 'StrengthConditioningData with given id must be in DB');
        $this->assertModelData($strengthConditioningData, $createdStrengthConditioningData);
    }

    /**
     * @test read
     */
    public function test_read_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->create();

        $dbStrengthConditioningData = $this->strengthConditioningDataRepo->find($strengthConditioningData->id);

        $dbStrengthConditioningData = $dbStrengthConditioningData->toArray();
        $this->assertModelData($strengthConditioningData->toArray(), $dbStrengthConditioningData);
    }

    /**
     * @test update
     */
    public function test_update_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->create();
        $fakeStrengthConditioningData = StrengthConditioningData::factory()->make()->toArray();

        $updatedStrengthConditioningData = $this->strengthConditioningDataRepo->update($fakeStrengthConditioningData, $strengthConditioningData->id);

        $this->assertModelData($fakeStrengthConditioningData, $updatedStrengthConditioningData->toArray());
        $dbStrengthConditioningData = $this->strengthConditioningDataRepo->find($strengthConditioningData->id);
        $this->assertModelData($fakeStrengthConditioningData, $dbStrengthConditioningData->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_strength_conditioning_data()
    {
        $strengthConditioningData = StrengthConditioningData::factory()->create();

        $resp = $this->strengthConditioningDataRepo->delete($strengthConditioningData->id);

        $this->assertTrue($resp);
        $this->assertNull(StrengthConditioningData::find($strengthConditioningData->id), 'StrengthConditioningData should not exist in DB');
    }
}
