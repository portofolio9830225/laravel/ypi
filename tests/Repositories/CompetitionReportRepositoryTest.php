<?php namespace Tests\Repositories;

use App\Models\CompetitionReport;
use App\Repositories\CompetitionReportRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CompetitionReportRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CompetitionReportRepository
     */
    protected $competitionReportRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->competitionReportRepo = \App::make(CompetitionReportRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->make()->toArray();

        $createdCompetitionReport = $this->competitionReportRepo->create($competitionReport);

        $createdCompetitionReport = $createdCompetitionReport->toArray();
        $this->assertArrayHasKey('id', $createdCompetitionReport);
        $this->assertNotNull($createdCompetitionReport['id'], 'Created CompetitionReport must have id specified');
        $this->assertNotNull(CompetitionReport::find($createdCompetitionReport['id']), 'CompetitionReport with given id must be in DB');
        $this->assertModelData($competitionReport, $createdCompetitionReport);
    }

    /**
     * @test read
     */
    public function test_read_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->create();

        $dbCompetitionReport = $this->competitionReportRepo->find($competitionReport->id);

        $dbCompetitionReport = $dbCompetitionReport->toArray();
        $this->assertModelData($competitionReport->toArray(), $dbCompetitionReport);
    }

    /**
     * @test update
     */
    public function test_update_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->create();
        $fakeCompetitionReport = CompetitionReport::factory()->make()->toArray();

        $updatedCompetitionReport = $this->competitionReportRepo->update($fakeCompetitionReport, $competitionReport->id);

        $this->assertModelData($fakeCompetitionReport, $updatedCompetitionReport->toArray());
        $dbCompetitionReport = $this->competitionReportRepo->find($competitionReport->id);
        $this->assertModelData($fakeCompetitionReport, $dbCompetitionReport->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_competition_report()
    {
        $competitionReport = CompetitionReport::factory()->create();

        $resp = $this->competitionReportRepo->delete($competitionReport->id);

        $this->assertTrue($resp);
        $this->assertNull(CompetitionReport::find($competitionReport->id), 'CompetitionReport should not exist in DB');
    }
}
