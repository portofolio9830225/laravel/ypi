<?php namespace Tests\Repositories;

use App\Models\MasterAchievement;
use App\Repositories\MasterAchievementRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MasterAchievementRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MasterAchievementRepository
     */
    protected $masterAchievementRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->masterAchievementRepo = \App::make(MasterAchievementRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->make()->toArray();

        $createdMasterAchievement = $this->masterAchievementRepo->create($masterAchievement);

        $createdMasterAchievement = $createdMasterAchievement->toArray();
        $this->assertArrayHasKey('id', $createdMasterAchievement);
        $this->assertNotNull($createdMasterAchievement['id'], 'Created MasterAchievement must have id specified');
        $this->assertNotNull(MasterAchievement::find($createdMasterAchievement['id']), 'MasterAchievement with given id must be in DB');
        $this->assertModelData($masterAchievement, $createdMasterAchievement);
    }

    /**
     * @test read
     */
    public function test_read_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->create();

        $dbMasterAchievement = $this->masterAchievementRepo->find($masterAchievement->id);

        $dbMasterAchievement = $dbMasterAchievement->toArray();
        $this->assertModelData($masterAchievement->toArray(), $dbMasterAchievement);
    }

    /**
     * @test update
     */
    public function test_update_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->create();
        $fakeMasterAchievement = MasterAchievement::factory()->make()->toArray();

        $updatedMasterAchievement = $this->masterAchievementRepo->update($fakeMasterAchievement, $masterAchievement->id);

        $this->assertModelData($fakeMasterAchievement, $updatedMasterAchievement->toArray());
        $dbMasterAchievement = $this->masterAchievementRepo->find($masterAchievement->id);
        $this->assertModelData($fakeMasterAchievement, $dbMasterAchievement->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_master_achievement()
    {
        $masterAchievement = MasterAchievement::factory()->create();

        $resp = $this->masterAchievementRepo->delete($masterAchievement->id);

        $this->assertTrue($resp);
        $this->assertNull(MasterAchievement::find($masterAchievement->id), 'MasterAchievement should not exist in DB');
    }
}
