<?php namespace Tests\Repositories;

use App\Models\KpiSubindicator;
use App\Repositories\KpiSubindicatorRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class KpiSubindicatorRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var KpiSubindicatorRepository
     */
    protected $kpiSubindicatorRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->kpiSubindicatorRepo = \App::make(KpiSubindicatorRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->make()->toArray();

        $createdKpiSubindicator = $this->kpiSubindicatorRepo->create($kpiSubindicator);

        $createdKpiSubindicator = $createdKpiSubindicator->toArray();
        $this->assertArrayHasKey('id', $createdKpiSubindicator);
        $this->assertNotNull($createdKpiSubindicator['id'], 'Created KpiSubindicator must have id specified');
        $this->assertNotNull(KpiSubindicator::find($createdKpiSubindicator['id']), 'KpiSubindicator with given id must be in DB');
        $this->assertModelData($kpiSubindicator, $createdKpiSubindicator);
    }

    /**
     * @test read
     */
    public function test_read_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->create();

        $dbKpiSubindicator = $this->kpiSubindicatorRepo->find($kpiSubindicator->id);

        $dbKpiSubindicator = $dbKpiSubindicator->toArray();
        $this->assertModelData($kpiSubindicator->toArray(), $dbKpiSubindicator);
    }

    /**
     * @test update
     */
    public function test_update_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->create();
        $fakeKpiSubindicator = KpiSubindicator::factory()->make()->toArray();

        $updatedKpiSubindicator = $this->kpiSubindicatorRepo->update($fakeKpiSubindicator, $kpiSubindicator->id);

        $this->assertModelData($fakeKpiSubindicator, $updatedKpiSubindicator->toArray());
        $dbKpiSubindicator = $this->kpiSubindicatorRepo->find($kpiSubindicator->id);
        $this->assertModelData($fakeKpiSubindicator, $dbKpiSubindicator->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_kpi_subindicator()
    {
        $kpiSubindicator = KpiSubindicator::factory()->create();

        $resp = $this->kpiSubindicatorRepo->delete($kpiSubindicator->id);

        $this->assertTrue($resp);
        $this->assertNull(KpiSubindicator::find($kpiSubindicator->id), 'KpiSubindicator should not exist in DB');
    }
}
