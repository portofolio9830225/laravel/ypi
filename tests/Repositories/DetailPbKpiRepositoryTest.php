<?php namespace Tests\Repositories;

use App\Models\DetailPbKpi;
use App\Repositories\DetailPbKpiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetailPbKpiRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetailPbKpiRepository
     */
    protected $detailPbKpiRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailPbKpiRepo = \App::make(DetailPbKpiRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->make()->toArray();

        $createdDetailPbKpi = $this->detailPbKpiRepo->create($detailPbKpi);

        $createdDetailPbKpi = $createdDetailPbKpi->toArray();
        $this->assertArrayHasKey('id', $createdDetailPbKpi);
        $this->assertNotNull($createdDetailPbKpi['id'], 'Created DetailPbKpi must have id specified');
        $this->assertNotNull(DetailPbKpi::find($createdDetailPbKpi['id']), 'DetailPbKpi with given id must be in DB');
        $this->assertModelData($detailPbKpi, $createdDetailPbKpi);
    }

    /**
     * @test read
     */
    public function test_read_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->create();

        $dbDetailPbKpi = $this->detailPbKpiRepo->find($detailPbKpi->id);

        $dbDetailPbKpi = $dbDetailPbKpi->toArray();
        $this->assertModelData($detailPbKpi->toArray(), $dbDetailPbKpi);
    }

    /**
     * @test update
     */
    public function test_update_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->create();
        $fakeDetailPbKpi = DetailPbKpi::factory()->make()->toArray();

        $updatedDetailPbKpi = $this->detailPbKpiRepo->update($fakeDetailPbKpi, $detailPbKpi->id);

        $this->assertModelData($fakeDetailPbKpi, $updatedDetailPbKpi->toArray());
        $dbDetailPbKpi = $this->detailPbKpiRepo->find($detailPbKpi->id);
        $this->assertModelData($fakeDetailPbKpi, $dbDetailPbKpi->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detail_pb_kpi()
    {
        $detailPbKpi = DetailPbKpi::factory()->create();

        $resp = $this->detailPbKpiRepo->delete($detailPbKpi->id);

        $this->assertTrue($resp);
        $this->assertNull(DetailPbKpi::find($detailPbKpi->id), 'DetailPbKpi should not exist in DB');
    }
}
