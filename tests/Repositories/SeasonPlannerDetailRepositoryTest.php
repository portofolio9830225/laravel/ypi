<?php namespace Tests\Repositories;

use App\Models\SeasonPlannerDetail;
use App\Repositories\SeasonPlannerDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeasonPlannerDetailRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeasonPlannerDetailRepository
     */
    protected $seasonPlannerDetailRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seasonPlannerDetailRepo = \App::make(SeasonPlannerDetailRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->make()->toArray();

        $createdSeasonPlannerDetail = $this->seasonPlannerDetailRepo->create($seasonPlannerDetail);

        $createdSeasonPlannerDetail = $createdSeasonPlannerDetail->toArray();
        $this->assertArrayHasKey('id', $createdSeasonPlannerDetail);
        $this->assertNotNull($createdSeasonPlannerDetail['id'], 'Created SeasonPlannerDetail must have id specified');
        $this->assertNotNull(SeasonPlannerDetail::find($createdSeasonPlannerDetail['id']), 'SeasonPlannerDetail with given id must be in DB');
        $this->assertModelData($seasonPlannerDetail, $createdSeasonPlannerDetail);
    }

    /**
     * @test read
     */
    public function test_read_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->create();

        $dbSeasonPlannerDetail = $this->seasonPlannerDetailRepo->find($seasonPlannerDetail->id);

        $dbSeasonPlannerDetail = $dbSeasonPlannerDetail->toArray();
        $this->assertModelData($seasonPlannerDetail->toArray(), $dbSeasonPlannerDetail);
    }

    /**
     * @test update
     */
    public function test_update_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->create();
        $fakeSeasonPlannerDetail = SeasonPlannerDetail::factory()->make()->toArray();

        $updatedSeasonPlannerDetail = $this->seasonPlannerDetailRepo->update($fakeSeasonPlannerDetail, $seasonPlannerDetail->id);

        $this->assertModelData($fakeSeasonPlannerDetail, $updatedSeasonPlannerDetail->toArray());
        $dbSeasonPlannerDetail = $this->seasonPlannerDetailRepo->find($seasonPlannerDetail->id);
        $this->assertModelData($fakeSeasonPlannerDetail, $dbSeasonPlannerDetail->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_season_planner_detail()
    {
        $seasonPlannerDetail = SeasonPlannerDetail::factory()->create();

        $resp = $this->seasonPlannerDetailRepo->delete($seasonPlannerDetail->id);

        $this->assertTrue($resp);
        $this->assertNull(SeasonPlannerDetail::find($seasonPlannerDetail->id), 'SeasonPlannerDetail should not exist in DB');
    }
}
