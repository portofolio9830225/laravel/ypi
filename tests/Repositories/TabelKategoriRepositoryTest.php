<?php namespace Tests\Repositories;

use App\Models\TabelKategori;
use App\Repositories\TabelKategoriRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TabelKategoriRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TabelKategoriRepository
     */
    protected $tabelKategoriRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tabelKategoriRepo = \App::make(TabelKategoriRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->make()->toArray();

        $createdTabelKategori = $this->tabelKategoriRepo->create($tabelKategori);

        $createdTabelKategori = $createdTabelKategori->toArray();
        $this->assertArrayHasKey('id', $createdTabelKategori);
        $this->assertNotNull($createdTabelKategori['id'], 'Created TabelKategori must have id specified');
        $this->assertNotNull(TabelKategori::find($createdTabelKategori['id']), 'TabelKategori with given id must be in DB');
        $this->assertModelData($tabelKategori, $createdTabelKategori);
    }

    /**
     * @test read
     */
    public function test_read_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->create();

        $dbTabelKategori = $this->tabelKategoriRepo->find($tabelKategori->id);

        $dbTabelKategori = $dbTabelKategori->toArray();
        $this->assertModelData($tabelKategori->toArray(), $dbTabelKategori);
    }

    /**
     * @test update
     */
    public function test_update_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->create();
        $fakeTabelKategori = TabelKategori::factory()->make()->toArray();

        $updatedTabelKategori = $this->tabelKategoriRepo->update($fakeTabelKategori, $tabelKategori->id);

        $this->assertModelData($fakeTabelKategori, $updatedTabelKategori->toArray());
        $dbTabelKategori = $this->tabelKategoriRepo->find($tabelKategori->id);
        $this->assertModelData($fakeTabelKategori, $dbTabelKategori->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tabel_kategori()
    {
        $tabelKategori = TabelKategori::factory()->create();

        $resp = $this->tabelKategoriRepo->delete($tabelKategori->id);

        $this->assertTrue($resp);
        $this->assertNull(TabelKategori::find($tabelKategori->id), 'TabelKategori should not exist in DB');
    }
}
