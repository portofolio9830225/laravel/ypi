<?php namespace Tests\Repositories;

use App\Models\CompetitionGroup;
use App\Repositories\CompetitionGroupRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CompetitionGroupRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CompetitionGroupRepository
     */
    protected $competitionGroupRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->competitionGroupRepo = \App::make(CompetitionGroupRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->make()->toArray();

        $createdCompetitionGroup = $this->competitionGroupRepo->create($competitionGroup);

        $createdCompetitionGroup = $createdCompetitionGroup->toArray();
        $this->assertArrayHasKey('id', $createdCompetitionGroup);
        $this->assertNotNull($createdCompetitionGroup['id'], 'Created CompetitionGroup must have id specified');
        $this->assertNotNull(CompetitionGroup::find($createdCompetitionGroup['id']), 'CompetitionGroup with given id must be in DB');
        $this->assertModelData($competitionGroup, $createdCompetitionGroup);
    }

    /**
     * @test read
     */
    public function test_read_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->create();

        $dbCompetitionGroup = $this->competitionGroupRepo->find($competitionGroup->id);

        $dbCompetitionGroup = $dbCompetitionGroup->toArray();
        $this->assertModelData($competitionGroup->toArray(), $dbCompetitionGroup);
    }

    /**
     * @test update
     */
    public function test_update_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->create();
        $fakeCompetitionGroup = CompetitionGroup::factory()->make()->toArray();

        $updatedCompetitionGroup = $this->competitionGroupRepo->update($fakeCompetitionGroup, $competitionGroup->id);

        $this->assertModelData($fakeCompetitionGroup, $updatedCompetitionGroup->toArray());
        $dbCompetitionGroup = $this->competitionGroupRepo->find($competitionGroup->id);
        $this->assertModelData($fakeCompetitionGroup, $dbCompetitionGroup->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_competition_group()
    {
        $competitionGroup = CompetitionGroup::factory()->create();

        $resp = $this->competitionGroupRepo->delete($competitionGroup->id);

        $this->assertTrue($resp);
        $this->assertNull(CompetitionGroup::find($competitionGroup->id), 'CompetitionGroup should not exist in DB');
    }
}
