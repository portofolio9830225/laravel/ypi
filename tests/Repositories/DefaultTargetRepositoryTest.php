<?php namespace Tests\Repositories;

use App\Models\DefaultTarget;
use App\Repositories\DefaultTargetRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DefaultTargetRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DefaultTargetRepository
     */
    protected $defaultTargetRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->defaultTargetRepo = \App::make(DefaultTargetRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->make()->toArray();

        $createdDefaultTarget = $this->defaultTargetRepo->create($defaultTarget);

        $createdDefaultTarget = $createdDefaultTarget->toArray();
        $this->assertArrayHasKey('id', $createdDefaultTarget);
        $this->assertNotNull($createdDefaultTarget['id'], 'Created DefaultTarget must have id specified');
        $this->assertNotNull(DefaultTarget::find($createdDefaultTarget['id']), 'DefaultTarget with given id must be in DB');
        $this->assertModelData($defaultTarget, $createdDefaultTarget);
    }

    /**
     * @test read
     */
    public function test_read_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->create();

        $dbDefaultTarget = $this->defaultTargetRepo->find($defaultTarget->id);

        $dbDefaultTarget = $dbDefaultTarget->toArray();
        $this->assertModelData($defaultTarget->toArray(), $dbDefaultTarget);
    }

    /**
     * @test update
     */
    public function test_update_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->create();
        $fakeDefaultTarget = DefaultTarget::factory()->make()->toArray();

        $updatedDefaultTarget = $this->defaultTargetRepo->update($fakeDefaultTarget, $defaultTarget->id);

        $this->assertModelData($fakeDefaultTarget, $updatedDefaultTarget->toArray());
        $dbDefaultTarget = $this->defaultTargetRepo->find($defaultTarget->id);
        $this->assertModelData($fakeDefaultTarget, $dbDefaultTarget->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_default_target()
    {
        $defaultTarget = DefaultTarget::factory()->create();

        $resp = $this->defaultTargetRepo->delete($defaultTarget->id);

        $this->assertTrue($resp);
        $this->assertNull(DefaultTarget::find($defaultTarget->id), 'DefaultTarget should not exist in DB');
    }
}
