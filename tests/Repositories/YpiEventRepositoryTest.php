<?php namespace Tests\Repositories;

use App\Models\YpiEvent;
use App\Repositories\YpiEventRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class YpiEventRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var YpiEventRepository
     */
    protected $ypiEventRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ypiEventRepo = \App::make(YpiEventRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->make()->toArray();

        $createdYpiEvent = $this->ypiEventRepo->create($ypiEvent);

        $createdYpiEvent = $createdYpiEvent->toArray();
        $this->assertArrayHasKey('id', $createdYpiEvent);
        $this->assertNotNull($createdYpiEvent['id'], 'Created YpiEvent must have id specified');
        $this->assertNotNull(YpiEvent::find($createdYpiEvent['id']), 'YpiEvent with given id must be in DB');
        $this->assertModelData($ypiEvent, $createdYpiEvent);
    }

    /**
     * @test read
     */
    public function test_read_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->create();

        $dbYpiEvent = $this->ypiEventRepo->find($ypiEvent->id);

        $dbYpiEvent = $dbYpiEvent->toArray();
        $this->assertModelData($ypiEvent->toArray(), $dbYpiEvent);
    }

    /**
     * @test update
     */
    public function test_update_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->create();
        $fakeYpiEvent = YpiEvent::factory()->make()->toArray();

        $updatedYpiEvent = $this->ypiEventRepo->update($fakeYpiEvent, $ypiEvent->id);

        $this->assertModelData($fakeYpiEvent, $updatedYpiEvent->toArray());
        $dbYpiEvent = $this->ypiEventRepo->find($ypiEvent->id);
        $this->assertModelData($fakeYpiEvent, $dbYpiEvent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ypi_event()
    {
        $ypiEvent = YpiEvent::factory()->create();

        $resp = $this->ypiEventRepo->delete($ypiEvent->id);

        $this->assertTrue($resp);
        $this->assertNull(YpiEvent::find($ypiEvent->id), 'YpiEvent should not exist in DB');
    }
}
