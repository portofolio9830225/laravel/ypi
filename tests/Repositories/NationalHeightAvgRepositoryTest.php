<?php namespace Tests\Repositories;

use App\Models\NationalHeightAvg;
use App\Repositories\NationalHeightAvgRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class NationalHeightAvgRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var NationalHeightAvgRepository
     */
    protected $nationalHeightAvgRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->nationalHeightAvgRepo = \App::make(NationalHeightAvgRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->make()->toArray();

        $createdNationalHeightAvg = $this->nationalHeightAvgRepo->create($nationalHeightAvg);

        $createdNationalHeightAvg = $createdNationalHeightAvg->toArray();
        $this->assertArrayHasKey('id', $createdNationalHeightAvg);
        $this->assertNotNull($createdNationalHeightAvg['id'], 'Created NationalHeightAvg must have id specified');
        $this->assertNotNull(NationalHeightAvg::find($createdNationalHeightAvg['id']), 'NationalHeightAvg with given id must be in DB');
        $this->assertModelData($nationalHeightAvg, $createdNationalHeightAvg);
    }

    /**
     * @test read
     */
    public function test_read_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->create();

        $dbNationalHeightAvg = $this->nationalHeightAvgRepo->find($nationalHeightAvg->id);

        $dbNationalHeightAvg = $dbNationalHeightAvg->toArray();
        $this->assertModelData($nationalHeightAvg->toArray(), $dbNationalHeightAvg);
    }

    /**
     * @test update
     */
    public function test_update_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->create();
        $fakeNationalHeightAvg = NationalHeightAvg::factory()->make()->toArray();

        $updatedNationalHeightAvg = $this->nationalHeightAvgRepo->update($fakeNationalHeightAvg, $nationalHeightAvg->id);

        $this->assertModelData($fakeNationalHeightAvg, $updatedNationalHeightAvg->toArray());
        $dbNationalHeightAvg = $this->nationalHeightAvgRepo->find($nationalHeightAvg->id);
        $this->assertModelData($fakeNationalHeightAvg, $dbNationalHeightAvg->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_national_height_avg()
    {
        $nationalHeightAvg = NationalHeightAvg::factory()->create();

        $resp = $this->nationalHeightAvgRepo->delete($nationalHeightAvg->id);

        $this->assertTrue($resp);
        $this->assertNull(NationalHeightAvg::find($nationalHeightAvg->id), 'NationalHeightAvg should not exist in DB');
    }
}
