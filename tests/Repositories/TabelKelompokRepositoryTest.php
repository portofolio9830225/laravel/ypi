<?php namespace Tests\Repositories;

use App\Models\TabelKelompok;
use App\Repositories\TabelKelompokRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class TabelKelompokRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var TabelKelompokRepository
     */
    protected $tabelKelompokRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->tabelKelompokRepo = \App::make(TabelKelompokRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->make()->toArray();

        $createdTabelKelompok = $this->tabelKelompokRepo->create($tabelKelompok);

        $createdTabelKelompok = $createdTabelKelompok->toArray();
        $this->assertArrayHasKey('id', $createdTabelKelompok);
        $this->assertNotNull($createdTabelKelompok['id'], 'Created TabelKelompok must have id specified');
        $this->assertNotNull(TabelKelompok::find($createdTabelKelompok['id']), 'TabelKelompok with given id must be in DB');
        $this->assertModelData($tabelKelompok, $createdTabelKelompok);
    }

    /**
     * @test read
     */
    public function test_read_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->create();

        $dbTabelKelompok = $this->tabelKelompokRepo->find($tabelKelompok->id);

        $dbTabelKelompok = $dbTabelKelompok->toArray();
        $this->assertModelData($tabelKelompok->toArray(), $dbTabelKelompok);
    }

    /**
     * @test update
     */
    public function test_update_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->create();
        $fakeTabelKelompok = TabelKelompok::factory()->make()->toArray();

        $updatedTabelKelompok = $this->tabelKelompokRepo->update($fakeTabelKelompok, $tabelKelompok->id);

        $this->assertModelData($fakeTabelKelompok, $updatedTabelKelompok->toArray());
        $dbTabelKelompok = $this->tabelKelompokRepo->find($tabelKelompok->id);
        $this->assertModelData($fakeTabelKelompok, $dbTabelKelompok->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_tabel_kelompok()
    {
        $tabelKelompok = TabelKelompok::factory()->create();

        $resp = $this->tabelKelompokRepo->delete($tabelKelompok->id);

        $this->assertTrue($resp);
        $this->assertNull(TabelKelompok::find($tabelKelompok->id), 'TabelKelompok should not exist in DB');
    }
}
