<?php namespace Tests\Repositories;

use App\Models\KpiResult;
use App\Repositories\KpiResultRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class KpiResultRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var KpiResultRepository
     */
    protected $kpiResultRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->kpiResultRepo = \App::make(KpiResultRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_kpi_result()
    {
        $kpiResult = KpiResult::factory()->make()->toArray();

        $createdKpiResult = $this->kpiResultRepo->create($kpiResult);

        $createdKpiResult = $createdKpiResult->toArray();
        $this->assertArrayHasKey('id', $createdKpiResult);
        $this->assertNotNull($createdKpiResult['id'], 'Created KpiResult must have id specified');
        $this->assertNotNull(KpiResult::find($createdKpiResult['id']), 'KpiResult with given id must be in DB');
        $this->assertModelData($kpiResult, $createdKpiResult);
    }

    /**
     * @test read
     */
    public function test_read_kpi_result()
    {
        $kpiResult = KpiResult::factory()->create();

        $dbKpiResult = $this->kpiResultRepo->find($kpiResult->id);

        $dbKpiResult = $dbKpiResult->toArray();
        $this->assertModelData($kpiResult->toArray(), $dbKpiResult);
    }

    /**
     * @test update
     */
    public function test_update_kpi_result()
    {
        $kpiResult = KpiResult::factory()->create();
        $fakeKpiResult = KpiResult::factory()->make()->toArray();

        $updatedKpiResult = $this->kpiResultRepo->update($fakeKpiResult, $kpiResult->id);

        $this->assertModelData($fakeKpiResult, $updatedKpiResult->toArray());
        $dbKpiResult = $this->kpiResultRepo->find($kpiResult->id);
        $this->assertModelData($fakeKpiResult, $dbKpiResult->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_kpi_result()
    {
        $kpiResult = KpiResult::factory()->create();

        $resp = $this->kpiResultRepo->delete($kpiResult->id);

        $this->assertTrue($resp);
        $this->assertNull(KpiResult::find($kpiResult->id), 'KpiResult should not exist in DB');
    }
}
