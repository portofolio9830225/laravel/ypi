<?php namespace Tests\Repositories;

use App\Models\AnnualPlan;
use App\Repositories\AnnualPlanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class AnnualPlanRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var AnnualPlanRepository
     */
    protected $annualPlanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->annualPlanRepo = \App::make(AnnualPlanRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->make()->toArray();

        $createdAnnualPlan = $this->annualPlanRepo->create($annualPlan);

        $createdAnnualPlan = $createdAnnualPlan->toArray();
        $this->assertArrayHasKey('id', $createdAnnualPlan);
        $this->assertNotNull($createdAnnualPlan['id'], 'Created AnnualPlan must have id specified');
        $this->assertNotNull(AnnualPlan::find($createdAnnualPlan['id']), 'AnnualPlan with given id must be in DB');
        $this->assertModelData($annualPlan, $createdAnnualPlan);
    }

    /**
     * @test read
     */
    public function test_read_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->create();

        $dbAnnualPlan = $this->annualPlanRepo->find($annualPlan->id);

        $dbAnnualPlan = $dbAnnualPlan->toArray();
        $this->assertModelData($annualPlan->toArray(), $dbAnnualPlan);
    }

    /**
     * @test update
     */
    public function test_update_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->create();
        $fakeAnnualPlan = AnnualPlan::factory()->make()->toArray();

        $updatedAnnualPlan = $this->annualPlanRepo->update($fakeAnnualPlan, $annualPlan->id);

        $this->assertModelData($fakeAnnualPlan, $updatedAnnualPlan->toArray());
        $dbAnnualPlan = $this->annualPlanRepo->find($annualPlan->id);
        $this->assertModelData($fakeAnnualPlan, $dbAnnualPlan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_annual_plan()
    {
        $annualPlan = AnnualPlan::factory()->create();

        $resp = $this->annualPlanRepo->delete($annualPlan->id);

        $this->assertTrue($resp);
        $this->assertNull(AnnualPlan::find($annualPlan->id), 'AnnualPlan should not exist in DB');
    }
}
