<?php namespace Tests\Repositories;

use App\Models\DetailkpiEvent;
use App\Repositories\DetailkpiEventRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetailkpiEventRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetailkpiEventRepository
     */
    protected $detailkpiEventRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailkpiEventRepo = \App::make(DetailkpiEventRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->make()->toArray();

        $createdDetailkpiEvent = $this->detailkpiEventRepo->create($detailkpiEvent);

        $createdDetailkpiEvent = $createdDetailkpiEvent->toArray();
        $this->assertArrayHasKey('id', $createdDetailkpiEvent);
        $this->assertNotNull($createdDetailkpiEvent['id'], 'Created DetailkpiEvent must have id specified');
        $this->assertNotNull(DetailkpiEvent::find($createdDetailkpiEvent['id']), 'DetailkpiEvent with given id must be in DB');
        $this->assertModelData($detailkpiEvent, $createdDetailkpiEvent);
    }

    /**
     * @test read
     */
    public function test_read_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->create();

        $dbDetailkpiEvent = $this->detailkpiEventRepo->find($detailkpiEvent->id);

        $dbDetailkpiEvent = $dbDetailkpiEvent->toArray();
        $this->assertModelData($detailkpiEvent->toArray(), $dbDetailkpiEvent);
    }

    /**
     * @test update
     */
    public function test_update_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->create();
        $fakeDetailkpiEvent = DetailkpiEvent::factory()->make()->toArray();

        $updatedDetailkpiEvent = $this->detailkpiEventRepo->update($fakeDetailkpiEvent, $detailkpiEvent->id);

        $this->assertModelData($fakeDetailkpiEvent, $updatedDetailkpiEvent->toArray());
        $dbDetailkpiEvent = $this->detailkpiEventRepo->find($detailkpiEvent->id);
        $this->assertModelData($fakeDetailkpiEvent, $dbDetailkpiEvent->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detailkpi_event()
    {
        $detailkpiEvent = DetailkpiEvent::factory()->create();

        $resp = $this->detailkpiEventRepo->delete($detailkpiEvent->id);

        $this->assertTrue($resp);
        $this->assertNull(DetailkpiEvent::find($detailkpiEvent->id), 'DetailkpiEvent should not exist in DB');
    }
}
