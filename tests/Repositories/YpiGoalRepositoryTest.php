<?php namespace Tests\Repositories;

use App\Models\YpiGoal;
use App\Repositories\YpiGoalRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class YpiGoalRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var YpiGoalRepository
     */
    protected $ypiGoalRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ypiGoalRepo = \App::make(YpiGoalRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->make()->toArray();

        $createdYpiGoal = $this->ypiGoalRepo->create($ypiGoal);

        $createdYpiGoal = $createdYpiGoal->toArray();
        $this->assertArrayHasKey('id', $createdYpiGoal);
        $this->assertNotNull($createdYpiGoal['id'], 'Created YpiGoal must have id specified');
        $this->assertNotNull(YpiGoal::find($createdYpiGoal['id']), 'YpiGoal with given id must be in DB');
        $this->assertModelData($ypiGoal, $createdYpiGoal);
    }

    /**
     * @test read
     */
    public function test_read_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->create();

        $dbYpiGoal = $this->ypiGoalRepo->find($ypiGoal->id);

        $dbYpiGoal = $dbYpiGoal->toArray();
        $this->assertModelData($ypiGoal->toArray(), $dbYpiGoal);
    }

    /**
     * @test update
     */
    public function test_update_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->create();
        $fakeYpiGoal = YpiGoal::factory()->make()->toArray();

        $updatedYpiGoal = $this->ypiGoalRepo->update($fakeYpiGoal, $ypiGoal->id);

        $this->assertModelData($fakeYpiGoal, $updatedYpiGoal->toArray());
        $dbYpiGoal = $this->ypiGoalRepo->find($ypiGoal->id);
        $this->assertModelData($fakeYpiGoal, $dbYpiGoal->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ypi_goal()
    {
        $ypiGoal = YpiGoal::factory()->create();

        $resp = $this->ypiGoalRepo->delete($ypiGoal->id);

        $this->assertTrue($resp);
        $this->assertNull(YpiGoal::find($ypiGoal->id), 'YpiGoal should not exist in DB');
    }
}
