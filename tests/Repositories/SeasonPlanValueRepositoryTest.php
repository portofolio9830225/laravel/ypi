<?php namespace Tests\Repositories;

use App\Models\SeasonPlanValue;
use App\Repositories\SeasonPlanValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeasonPlanValueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeasonPlanValueRepository
     */
    protected $seasonPlanValueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seasonPlanValueRepo = \App::make(SeasonPlanValueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->make()->toArray();

        $createdSeasonPlanValue = $this->seasonPlanValueRepo->create($seasonPlanValue);

        $createdSeasonPlanValue = $createdSeasonPlanValue->toArray();
        $this->assertArrayHasKey('id', $createdSeasonPlanValue);
        $this->assertNotNull($createdSeasonPlanValue['id'], 'Created SeasonPlanValue must have id specified');
        $this->assertNotNull(SeasonPlanValue::find($createdSeasonPlanValue['id']), 'SeasonPlanValue with given id must be in DB');
        $this->assertModelData($seasonPlanValue, $createdSeasonPlanValue);
    }

    /**
     * @test read
     */
    public function test_read_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->create();

        $dbSeasonPlanValue = $this->seasonPlanValueRepo->find($seasonPlanValue->id);

        $dbSeasonPlanValue = $dbSeasonPlanValue->toArray();
        $this->assertModelData($seasonPlanValue->toArray(), $dbSeasonPlanValue);
    }

    /**
     * @test update
     */
    public function test_update_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->create();
        $fakeSeasonPlanValue = SeasonPlanValue::factory()->make()->toArray();

        $updatedSeasonPlanValue = $this->seasonPlanValueRepo->update($fakeSeasonPlanValue, $seasonPlanValue->id);

        $this->assertModelData($fakeSeasonPlanValue, $updatedSeasonPlanValue->toArray());
        $dbSeasonPlanValue = $this->seasonPlanValueRepo->find($seasonPlanValue->id);
        $this->assertModelData($fakeSeasonPlanValue, $dbSeasonPlanValue->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_season_plan_value()
    {
        $seasonPlanValue = SeasonPlanValue::factory()->create();

        $resp = $this->seasonPlanValueRepo->delete($seasonPlanValue->id);

        $this->assertTrue($resp);
        $this->assertNull(SeasonPlanValue::find($seasonPlanValue->id), 'SeasonPlanValue should not exist in DB');
    }
}
