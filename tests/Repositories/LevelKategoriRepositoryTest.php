<?php namespace Tests\Repositories;

use App\Models\LevelKategori;
use App\Repositories\LevelKategoriRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class LevelKategoriRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var LevelKategoriRepository
     */
    protected $levelKategoriRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->levelKategoriRepo = \App::make(LevelKategoriRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->make()->toArray();

        $createdLevelKategori = $this->levelKategoriRepo->create($levelKategori);

        $createdLevelKategori = $createdLevelKategori->toArray();
        $this->assertArrayHasKey('id', $createdLevelKategori);
        $this->assertNotNull($createdLevelKategori['id'], 'Created LevelKategori must have id specified');
        $this->assertNotNull(LevelKategori::find($createdLevelKategori['id']), 'LevelKategori with given id must be in DB');
        $this->assertModelData($levelKategori, $createdLevelKategori);
    }

    /**
     * @test read
     */
    public function test_read_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->create();

        $dbLevelKategori = $this->levelKategoriRepo->find($levelKategori->id);

        $dbLevelKategori = $dbLevelKategori->toArray();
        $this->assertModelData($levelKategori->toArray(), $dbLevelKategori);
    }

    /**
     * @test update
     */
    public function test_update_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->create();
        $fakeLevelKategori = LevelKategori::factory()->make()->toArray();

        $updatedLevelKategori = $this->levelKategoriRepo->update($fakeLevelKategori, $levelKategori->id);

        $this->assertModelData($fakeLevelKategori, $updatedLevelKategori->toArray());
        $dbLevelKategori = $this->levelKategoriRepo->find($levelKategori->id);
        $this->assertModelData($fakeLevelKategori, $dbLevelKategori->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_level_kategori()
    {
        $levelKategori = LevelKategori::factory()->create();

        $resp = $this->levelKategoriRepo->delete($levelKategori->id);

        $this->assertTrue($resp);
        $this->assertNull(LevelKategori::find($levelKategori->id), 'LevelKategori should not exist in DB');
    }
}
