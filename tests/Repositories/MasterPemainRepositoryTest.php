<?php namespace Tests\Repositories;

use App\Models\MasterPemain;
use App\Repositories\MasterPemainRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MasterPemainRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MasterPemainRepository
     */
    protected $masterPemainRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->masterPemainRepo = \App::make(MasterPemainRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->make()->toArray();

        $createdMasterPemain = $this->masterPemainRepo->create($masterPemain);

        $createdMasterPemain = $createdMasterPemain->toArray();
        $this->assertArrayHasKey('id', $createdMasterPemain);
        $this->assertNotNull($createdMasterPemain['id'], 'Created MasterPemain must have id specified');
        $this->assertNotNull(MasterPemain::find($createdMasterPemain['id']), 'MasterPemain with given id must be in DB');
        $this->assertModelData($masterPemain, $createdMasterPemain);
    }

    /**
     * @test read
     */
    public function test_read_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->create();

        $dbMasterPemain = $this->masterPemainRepo->find($masterPemain->id);

        $dbMasterPemain = $dbMasterPemain->toArray();
        $this->assertModelData($masterPemain->toArray(), $dbMasterPemain);
    }

    /**
     * @test update
     */
    public function test_update_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->create();
        $fakeMasterPemain = MasterPemain::factory()->make()->toArray();

        $updatedMasterPemain = $this->masterPemainRepo->update($fakeMasterPemain, $masterPemain->id);

        $this->assertModelData($fakeMasterPemain, $updatedMasterPemain->toArray());
        $dbMasterPemain = $this->masterPemainRepo->find($masterPemain->id);
        $this->assertModelData($fakeMasterPemain, $dbMasterPemain->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_master_pemain()
    {
        $masterPemain = MasterPemain::factory()->create();

        $resp = $this->masterPemainRepo->delete($masterPemain->id);

        $this->assertTrue($resp);
        $this->assertNull(MasterPemain::find($masterPemain->id), 'MasterPemain should not exist in DB');
    }
}
