<?php namespace Tests\Repositories;

use App\Models\MasterPelatih;
use App\Repositories\MasterPelatihRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MasterPelatihRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MasterPelatihRepository
     */
    protected $masterPelatihRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->masterPelatihRepo = \App::make(MasterPelatihRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->make()->toArray();

        $createdMasterPelatih = $this->masterPelatihRepo->create($masterPelatih);

        $createdMasterPelatih = $createdMasterPelatih->toArray();
        $this->assertArrayHasKey('id', $createdMasterPelatih);
        $this->assertNotNull($createdMasterPelatih['id'], 'Created MasterPelatih must have id specified');
        $this->assertNotNull(MasterPelatih::find($createdMasterPelatih['id']), 'MasterPelatih with given id must be in DB');
        $this->assertModelData($masterPelatih, $createdMasterPelatih);
    }

    /**
     * @test read
     */
    public function test_read_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->create();

        $dbMasterPelatih = $this->masterPelatihRepo->find($masterPelatih->id);

        $dbMasterPelatih = $dbMasterPelatih->toArray();
        $this->assertModelData($masterPelatih->toArray(), $dbMasterPelatih);
    }

    /**
     * @test update
     */
    public function test_update_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->create();
        $fakeMasterPelatih = MasterPelatih::factory()->make()->toArray();

        $updatedMasterPelatih = $this->masterPelatihRepo->update($fakeMasterPelatih, $masterPelatih->id);

        $this->assertModelData($fakeMasterPelatih, $updatedMasterPelatih->toArray());
        $dbMasterPelatih = $this->masterPelatihRepo->find($masterPelatih->id);
        $this->assertModelData($fakeMasterPelatih, $dbMasterPelatih->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_master_pelatih()
    {
        $masterPelatih = MasterPelatih::factory()->create();

        $resp = $this->masterPelatihRepo->delete($masterPelatih->id);

        $this->assertTrue($resp);
        $this->assertNull(MasterPelatih::find($masterPelatih->id), 'MasterPelatih should not exist in DB');
    }
}
