<?php namespace Tests\Repositories;

use App\Models\KpiParameter;
use App\Repositories\KpiParameterRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class KpiParameterRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var KpiParameterRepository
     */
    protected $kpiParameterRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->kpiParameterRepo = \App::make(KpiParameterRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->make()->toArray();

        $createdKpiParameter = $this->kpiParameterRepo->create($kpiParameter);

        $createdKpiParameter = $createdKpiParameter->toArray();
        $this->assertArrayHasKey('id', $createdKpiParameter);
        $this->assertNotNull($createdKpiParameter['id'], 'Created KpiParameter must have id specified');
        $this->assertNotNull(KpiParameter::find($createdKpiParameter['id']), 'KpiParameter with given id must be in DB');
        $this->assertModelData($kpiParameter, $createdKpiParameter);
    }

    /**
     * @test read
     */
    public function test_read_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->create();

        $dbKpiParameter = $this->kpiParameterRepo->find($kpiParameter->id);

        $dbKpiParameter = $dbKpiParameter->toArray();
        $this->assertModelData($kpiParameter->toArray(), $dbKpiParameter);
    }

    /**
     * @test update
     */
    public function test_update_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->create();
        $fakeKpiParameter = KpiParameter::factory()->make()->toArray();

        $updatedKpiParameter = $this->kpiParameterRepo->update($fakeKpiParameter, $kpiParameter->id);

        $this->assertModelData($fakeKpiParameter, $updatedKpiParameter->toArray());
        $dbKpiParameter = $this->kpiParameterRepo->find($kpiParameter->id);
        $this->assertModelData($fakeKpiParameter, $dbKpiParameter->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_kpi_parameter()
    {
        $kpiParameter = KpiParameter::factory()->create();

        $resp = $this->kpiParameterRepo->delete($kpiParameter->id);

        $this->assertTrue($resp);
        $this->assertNull(KpiParameter::find($kpiParameter->id), 'KpiParameter should not exist in DB');
    }
}
