<?php namespace Tests\Repositories;

use App\Models\SettingPresence;
use App\Repositories\SettingPresenceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SettingPresenceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SettingPresenceRepository
     */
    protected $settingPresenceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->settingPresenceRepo = \App::make(SettingPresenceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->make()->toArray();

        $createdSettingPresence = $this->settingPresenceRepo->create($settingPresence);

        $createdSettingPresence = $createdSettingPresence->toArray();
        $this->assertArrayHasKey('id', $createdSettingPresence);
        $this->assertNotNull($createdSettingPresence['id'], 'Created SettingPresence must have id specified');
        $this->assertNotNull(SettingPresence::find($createdSettingPresence['id']), 'SettingPresence with given id must be in DB');
        $this->assertModelData($settingPresence, $createdSettingPresence);
    }

    /**
     * @test read
     */
    public function test_read_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->create();

        $dbSettingPresence = $this->settingPresenceRepo->find($settingPresence->id);

        $dbSettingPresence = $dbSettingPresence->toArray();
        $this->assertModelData($settingPresence->toArray(), $dbSettingPresence);
    }

    /**
     * @test update
     */
    public function test_update_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->create();
        $fakeSettingPresence = SettingPresence::factory()->make()->toArray();

        $updatedSettingPresence = $this->settingPresenceRepo->update($fakeSettingPresence, $settingPresence->id);

        $this->assertModelData($fakeSettingPresence, $updatedSettingPresence->toArray());
        $dbSettingPresence = $this->settingPresenceRepo->find($settingPresence->id);
        $this->assertModelData($fakeSettingPresence, $dbSettingPresence->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_setting_presence()
    {
        $settingPresence = SettingPresence::factory()->create();

        $resp = $this->settingPresenceRepo->delete($settingPresence->id);

        $this->assertTrue($resp);
        $this->assertNull(SettingPresence::find($settingPresence->id), 'SettingPresence should not exist in DB');
    }
}
