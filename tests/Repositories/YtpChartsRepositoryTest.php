<?php namespace Tests\Repositories;

use App\Models\YtpCharts;
use App\Repositories\YtpChartsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class YtpChartsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var YtpChartsRepository
     */
    protected $ytpChartsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->ytpChartsRepo = \App::make(YtpChartsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->make()->toArray();

        $createdYtpCharts = $this->ytpChartsRepo->create($ytpCharts);

        $createdYtpCharts = $createdYtpCharts->toArray();
        $this->assertArrayHasKey('id', $createdYtpCharts);
        $this->assertNotNull($createdYtpCharts['id'], 'Created YtpCharts must have id specified');
        $this->assertNotNull(YtpCharts::find($createdYtpCharts['id']), 'YtpCharts with given id must be in DB');
        $this->assertModelData($ytpCharts, $createdYtpCharts);
    }

    /**
     * @test read
     */
    public function test_read_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->create();

        $dbYtpCharts = $this->ytpChartsRepo->find($ytpCharts->id);

        $dbYtpCharts = $dbYtpCharts->toArray();
        $this->assertModelData($ytpCharts->toArray(), $dbYtpCharts);
    }

    /**
     * @test update
     */
    public function test_update_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->create();
        $fakeYtpCharts = YtpCharts::factory()->make()->toArray();

        $updatedYtpCharts = $this->ytpChartsRepo->update($fakeYtpCharts, $ytpCharts->id);

        $this->assertModelData($fakeYtpCharts, $updatedYtpCharts->toArray());
        $dbYtpCharts = $this->ytpChartsRepo->find($ytpCharts->id);
        $this->assertModelData($fakeYtpCharts, $dbYtpCharts->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_ytp_charts()
    {
        $ytpCharts = YtpCharts::factory()->create();

        $resp = $this->ytpChartsRepo->delete($ytpCharts->id);

        $this->assertTrue($resp);
        $this->assertNull(YtpCharts::find($ytpCharts->id), 'YtpCharts should not exist in DB');
    }
}
