<?php namespace Tests\Repositories;

use App\Models\SeasonPlanner;
use App\Repositories\SeasonPlannerRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeasonPlannerRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeasonPlannerRepository
     */
    protected $seasonPlannerRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seasonPlannerRepo = \App::make(SeasonPlannerRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->make()->toArray();

        $createdSeasonPlanner = $this->seasonPlannerRepo->create($seasonPlanner);

        $createdSeasonPlanner = $createdSeasonPlanner->toArray();
        $this->assertArrayHasKey('id', $createdSeasonPlanner);
        $this->assertNotNull($createdSeasonPlanner['id'], 'Created SeasonPlanner must have id specified');
        $this->assertNotNull(SeasonPlanner::find($createdSeasonPlanner['id']), 'SeasonPlanner with given id must be in DB');
        $this->assertModelData($seasonPlanner, $createdSeasonPlanner);
    }

    /**
     * @test read
     */
    public function test_read_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->create();

        $dbSeasonPlanner = $this->seasonPlannerRepo->find($seasonPlanner->id);

        $dbSeasonPlanner = $dbSeasonPlanner->toArray();
        $this->assertModelData($seasonPlanner->toArray(), $dbSeasonPlanner);
    }

    /**
     * @test update
     */
    public function test_update_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->create();
        $fakeSeasonPlanner = SeasonPlanner::factory()->make()->toArray();

        $updatedSeasonPlanner = $this->seasonPlannerRepo->update($fakeSeasonPlanner, $seasonPlanner->id);

        $this->assertModelData($fakeSeasonPlanner, $updatedSeasonPlanner->toArray());
        $dbSeasonPlanner = $this->seasonPlannerRepo->find($seasonPlanner->id);
        $this->assertModelData($fakeSeasonPlanner, $dbSeasonPlanner->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_season_planner()
    {
        $seasonPlanner = SeasonPlanner::factory()->create();

        $resp = $this->seasonPlannerRepo->delete($seasonPlanner->id);

        $this->assertTrue($resp);
        $this->assertNull(SeasonPlanner::find($seasonPlanner->id), 'SeasonPlanner should not exist in DB');
    }
}
