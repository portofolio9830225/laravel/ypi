<?php namespace Tests\Repositories;

use App\Models\SeasonPlan;
use App\Repositories\SeasonPlanRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeasonPlanRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeasonPlanRepository
     */
    protected $seasonPlanRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seasonPlanRepo = \App::make(SeasonPlanRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->make()->toArray();

        $createdSeasonPlan = $this->seasonPlanRepo->create($seasonPlan);

        $createdSeasonPlan = $createdSeasonPlan->toArray();
        $this->assertArrayHasKey('id', $createdSeasonPlan);
        $this->assertNotNull($createdSeasonPlan['id'], 'Created SeasonPlan must have id specified');
        $this->assertNotNull(SeasonPlan::find($createdSeasonPlan['id']), 'SeasonPlan with given id must be in DB');
        $this->assertModelData($seasonPlan, $createdSeasonPlan);
    }

    /**
     * @test read
     */
    public function test_read_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->create();

        $dbSeasonPlan = $this->seasonPlanRepo->find($seasonPlan->id);

        $dbSeasonPlan = $dbSeasonPlan->toArray();
        $this->assertModelData($seasonPlan->toArray(), $dbSeasonPlan);
    }

    /**
     * @test update
     */
    public function test_update_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->create();
        $fakeSeasonPlan = SeasonPlan::factory()->make()->toArray();

        $updatedSeasonPlan = $this->seasonPlanRepo->update($fakeSeasonPlan, $seasonPlan->id);

        $this->assertModelData($fakeSeasonPlan, $updatedSeasonPlan->toArray());
        $dbSeasonPlan = $this->seasonPlanRepo->find($seasonPlan->id);
        $this->assertModelData($fakeSeasonPlan, $dbSeasonPlan->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_season_plan()
    {
        $seasonPlan = SeasonPlan::factory()->create();

        $resp = $this->seasonPlanRepo->delete($seasonPlan->id);

        $this->assertTrue($resp);
        $this->assertNull(SeasonPlan::find($seasonPlan->id), 'SeasonPlan should not exist in DB');
    }
}
