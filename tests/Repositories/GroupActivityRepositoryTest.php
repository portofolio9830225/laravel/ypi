<?php namespace Tests\Repositories;

use App\Models\GroupActivity;
use App\Repositories\GroupActivityRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GroupActivityRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GroupActivityRepository
     */
    protected $groupActivityRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->groupActivityRepo = \App::make(GroupActivityRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_group_activity()
    {
        $groupActivity = GroupActivity::factory()->make()->toArray();

        $createdGroupActivity = $this->groupActivityRepo->create($groupActivity);

        $createdGroupActivity = $createdGroupActivity->toArray();
        $this->assertArrayHasKey('id', $createdGroupActivity);
        $this->assertNotNull($createdGroupActivity['id'], 'Created GroupActivity must have id specified');
        $this->assertNotNull(GroupActivity::find($createdGroupActivity['id']), 'GroupActivity with given id must be in DB');
        $this->assertModelData($groupActivity, $createdGroupActivity);
    }

    /**
     * @test read
     */
    public function test_read_group_activity()
    {
        $groupActivity = GroupActivity::factory()->create();

        $dbGroupActivity = $this->groupActivityRepo->find($groupActivity->id);

        $dbGroupActivity = $dbGroupActivity->toArray();
        $this->assertModelData($groupActivity->toArray(), $dbGroupActivity);
    }

    /**
     * @test update
     */
    public function test_update_group_activity()
    {
        $groupActivity = GroupActivity::factory()->create();
        $fakeGroupActivity = GroupActivity::factory()->make()->toArray();

        $updatedGroupActivity = $this->groupActivityRepo->update($fakeGroupActivity, $groupActivity->id);

        $this->assertModelData($fakeGroupActivity, $updatedGroupActivity->toArray());
        $dbGroupActivity = $this->groupActivityRepo->find($groupActivity->id);
        $this->assertModelData($fakeGroupActivity, $dbGroupActivity->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_group_activity()
    {
        $groupActivity = GroupActivity::factory()->create();

        $resp = $this->groupActivityRepo->delete($groupActivity->id);

        $this->assertTrue($resp);
        $this->assertNull(GroupActivity::find($groupActivity->id), 'GroupActivity should not exist in DB');
    }
}
