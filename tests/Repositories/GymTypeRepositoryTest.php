<?php namespace Tests\Repositories;

use App\Models\GymType;
use App\Repositories\GymTypeRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class GymTypeRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var GymTypeRepository
     */
    protected $gymTypeRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->gymTypeRepo = \App::make(GymTypeRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_gym_type()
    {
        $gymType = GymType::factory()->make()->toArray();

        $createdGymType = $this->gymTypeRepo->create($gymType);

        $createdGymType = $createdGymType->toArray();
        $this->assertArrayHasKey('id', $createdGymType);
        $this->assertNotNull($createdGymType['id'], 'Created GymType must have id specified');
        $this->assertNotNull(GymType::find($createdGymType['id']), 'GymType with given id must be in DB');
        $this->assertModelData($gymType, $createdGymType);
    }

    /**
     * @test read
     */
    public function test_read_gym_type()
    {
        $gymType = GymType::factory()->create();

        $dbGymType = $this->gymTypeRepo->find($gymType->id);

        $dbGymType = $dbGymType->toArray();
        $this->assertModelData($gymType->toArray(), $dbGymType);
    }

    /**
     * @test update
     */
    public function test_update_gym_type()
    {
        $gymType = GymType::factory()->create();
        $fakeGymType = GymType::factory()->make()->toArray();

        $updatedGymType = $this->gymTypeRepo->update($fakeGymType, $gymType->id);

        $this->assertModelData($fakeGymType, $updatedGymType->toArray());
        $dbGymType = $this->gymTypeRepo->find($gymType->id);
        $this->assertModelData($fakeGymType, $dbGymType->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_gym_type()
    {
        $gymType = GymType::factory()->create();

        $resp = $this->gymTypeRepo->delete($gymType->id);

        $this->assertTrue($resp);
        $this->assertNull(GymType::find($gymType->id), 'GymType should not exist in DB');
    }
}
