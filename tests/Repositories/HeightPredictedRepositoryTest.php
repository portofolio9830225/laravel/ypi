<?php namespace Tests\Repositories;

use App\Models\HeightPredicted;
use App\Repositories\HeightPredictedRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HeightPredictedRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HeightPredictedRepository
     */
    protected $heightPredictedRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->heightPredictedRepo = \App::make(HeightPredictedRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->make()->toArray();

        $createdHeightPredicted = $this->heightPredictedRepo->create($heightPredicted);

        $createdHeightPredicted = $createdHeightPredicted->toArray();
        $this->assertArrayHasKey('id', $createdHeightPredicted);
        $this->assertNotNull($createdHeightPredicted['id'], 'Created HeightPredicted must have id specified');
        $this->assertNotNull(HeightPredicted::find($createdHeightPredicted['id']), 'HeightPredicted with given id must be in DB');
        $this->assertModelData($heightPredicted, $createdHeightPredicted);
    }

    /**
     * @test read
     */
    public function test_read_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->create();

        $dbHeightPredicted = $this->heightPredictedRepo->find($heightPredicted->id);

        $dbHeightPredicted = $dbHeightPredicted->toArray();
        $this->assertModelData($heightPredicted->toArray(), $dbHeightPredicted);
    }

    /**
     * @test update
     */
    public function test_update_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->create();
        $fakeHeightPredicted = HeightPredicted::factory()->make()->toArray();

        $updatedHeightPredicted = $this->heightPredictedRepo->update($fakeHeightPredicted, $heightPredicted->id);

        $this->assertModelData($fakeHeightPredicted, $updatedHeightPredicted->toArray());
        $dbHeightPredicted = $this->heightPredictedRepo->find($heightPredicted->id);
        $this->assertModelData($fakeHeightPredicted, $dbHeightPredicted->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_height_predicted()
    {
        $heightPredicted = HeightPredicted::factory()->create();

        $resp = $this->heightPredictedRepo->delete($heightPredicted->id);

        $this->assertTrue($resp);
        $this->assertNull(HeightPredicted::find($heightPredicted->id), 'HeightPredicted should not exist in DB');
    }
}
