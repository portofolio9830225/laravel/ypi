<?php namespace Tests\Repositories;

use App\Models\SubExercise;
use App\Repositories\SubExerciseRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SubExerciseRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SubExerciseRepository
     */
    protected $subExerciseRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->subExerciseRepo = \App::make(SubExerciseRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_sub_exercise()
    {
        $subExercise = SubExercise::factory()->make()->toArray();

        $createdSubExercise = $this->subExerciseRepo->create($subExercise);

        $createdSubExercise = $createdSubExercise->toArray();
        $this->assertArrayHasKey('id', $createdSubExercise);
        $this->assertNotNull($createdSubExercise['id'], 'Created SubExercise must have id specified');
        $this->assertNotNull(SubExercise::find($createdSubExercise['id']), 'SubExercise with given id must be in DB');
        $this->assertModelData($subExercise, $createdSubExercise);
    }

    /**
     * @test read
     */
    public function test_read_sub_exercise()
    {
        $subExercise = SubExercise::factory()->create();

        $dbSubExercise = $this->subExerciseRepo->find($subExercise->id);

        $dbSubExercise = $dbSubExercise->toArray();
        $this->assertModelData($subExercise->toArray(), $dbSubExercise);
    }

    /**
     * @test update
     */
    public function test_update_sub_exercise()
    {
        $subExercise = SubExercise::factory()->create();
        $fakeSubExercise = SubExercise::factory()->make()->toArray();

        $updatedSubExercise = $this->subExerciseRepo->update($fakeSubExercise, $subExercise->id);

        $this->assertModelData($fakeSubExercise, $updatedSubExercise->toArray());
        $dbSubExercise = $this->subExerciseRepo->find($subExercise->id);
        $this->assertModelData($fakeSubExercise, $dbSubExercise->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_sub_exercise()
    {
        $subExercise = SubExercise::factory()->create();

        $resp = $this->subExerciseRepo->delete($subExercise->id);

        $this->assertTrue($resp);
        $this->assertNull(SubExercise::find($subExercise->id), 'SubExercise should not exist in DB');
    }
}
