<?php namespace Tests\Repositories;

use App\Models\SettingAttendance;
use App\Repositories\SettingAttendanceRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SettingAttendanceRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SettingAttendanceRepository
     */
    protected $settingAttendanceRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->settingAttendanceRepo = \App::make(SettingAttendanceRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->make()->toArray();

        $createdSettingAttendance = $this->settingAttendanceRepo->create($settingAttendance);

        $createdSettingAttendance = $createdSettingAttendance->toArray();
        $this->assertArrayHasKey('id', $createdSettingAttendance);
        $this->assertNotNull($createdSettingAttendance['id'], 'Created SettingAttendance must have id specified');
        $this->assertNotNull(SettingAttendance::find($createdSettingAttendance['id']), 'SettingAttendance with given id must be in DB');
        $this->assertModelData($settingAttendance, $createdSettingAttendance);
    }

    /**
     * @test read
     */
    public function test_read_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->create();

        $dbSettingAttendance = $this->settingAttendanceRepo->find($settingAttendance->id);

        $dbSettingAttendance = $dbSettingAttendance->toArray();
        $this->assertModelData($settingAttendance->toArray(), $dbSettingAttendance);
    }

    /**
     * @test update
     */
    public function test_update_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->create();
        $fakeSettingAttendance = SettingAttendance::factory()->make()->toArray();

        $updatedSettingAttendance = $this->settingAttendanceRepo->update($fakeSettingAttendance, $settingAttendance->id);

        $this->assertModelData($fakeSettingAttendance, $updatedSettingAttendance->toArray());
        $dbSettingAttendance = $this->settingAttendanceRepo->find($settingAttendance->id);
        $this->assertModelData($fakeSettingAttendance, $dbSettingAttendance->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_setting_attendance()
    {
        $settingAttendance = SettingAttendance::factory()->create();

        $resp = $this->settingAttendanceRepo->delete($settingAttendance->id);

        $this->assertTrue($resp);
        $this->assertNull(SettingAttendance::find($settingAttendance->id), 'SettingAttendance should not exist in DB');
    }
}
