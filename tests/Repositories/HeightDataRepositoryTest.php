<?php namespace Tests\Repositories;

use App\Models\HeightData;
use App\Repositories\HeightDataRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HeightDataRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HeightDataRepository
     */
    protected $heightDataRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->heightDataRepo = \App::make(HeightDataRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_height_data()
    {
        $heightData = HeightData::factory()->make()->toArray();

        $createdHeightData = $this->heightDataRepo->create($heightData);

        $createdHeightData = $createdHeightData->toArray();
        $this->assertArrayHasKey('id', $createdHeightData);
        $this->assertNotNull($createdHeightData['id'], 'Created HeightData must have id specified');
        $this->assertNotNull(HeightData::find($createdHeightData['id']), 'HeightData with given id must be in DB');
        $this->assertModelData($heightData, $createdHeightData);
    }

    /**
     * @test read
     */
    public function test_read_height_data()
    {
        $heightData = HeightData::factory()->create();

        $dbHeightData = $this->heightDataRepo->find($heightData->id);

        $dbHeightData = $dbHeightData->toArray();
        $this->assertModelData($heightData->toArray(), $dbHeightData);
    }

    /**
     * @test update
     */
    public function test_update_height_data()
    {
        $heightData = HeightData::factory()->create();
        $fakeHeightData = HeightData::factory()->make()->toArray();

        $updatedHeightData = $this->heightDataRepo->update($fakeHeightData, $heightData->id);

        $this->assertModelData($fakeHeightData, $updatedHeightData->toArray());
        $dbHeightData = $this->heightDataRepo->find($heightData->id);
        $this->assertModelData($fakeHeightData, $dbHeightData->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_height_data()
    {
        $heightData = HeightData::factory()->create();

        $resp = $this->heightDataRepo->delete($heightData->id);

        $this->assertTrue($resp);
        $this->assertNull(HeightData::find($heightData->id), 'HeightData should not exist in DB');
    }
}
