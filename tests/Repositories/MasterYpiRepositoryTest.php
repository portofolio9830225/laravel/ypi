<?php namespace Tests\Repositories;

use App\Models\MasterYpi;
use App\Repositories\MasterYpiRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class MasterYpiRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var MasterYpiRepository
     */
    protected $masterYpiRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->masterYpiRepo = \App::make(MasterYpiRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->make()->toArray();

        $createdMasterYpi = $this->masterYpiRepo->create($masterYpi);

        $createdMasterYpi = $createdMasterYpi->toArray();
        $this->assertArrayHasKey('id', $createdMasterYpi);
        $this->assertNotNull($createdMasterYpi['id'], 'Created MasterYpi must have id specified');
        $this->assertNotNull(MasterYpi::find($createdMasterYpi['id']), 'MasterYpi with given id must be in DB');
        $this->assertModelData($masterYpi, $createdMasterYpi);
    }

    /**
     * @test read
     */
    public function test_read_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->create();

        $dbMasterYpi = $this->masterYpiRepo->find($masterYpi->id);

        $dbMasterYpi = $dbMasterYpi->toArray();
        $this->assertModelData($masterYpi->toArray(), $dbMasterYpi);
    }

    /**
     * @test update
     */
    public function test_update_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->create();
        $fakeMasterYpi = MasterYpi::factory()->make()->toArray();

        $updatedMasterYpi = $this->masterYpiRepo->update($fakeMasterYpi, $masterYpi->id);

        $this->assertModelData($fakeMasterYpi, $updatedMasterYpi->toArray());
        $dbMasterYpi = $this->masterYpiRepo->find($masterYpi->id);
        $this->assertModelData($fakeMasterYpi, $dbMasterYpi->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_master_ypi()
    {
        $masterYpi = MasterYpi::factory()->create();

        $resp = $this->masterYpiRepo->delete($masterYpi->id);

        $this->assertTrue($resp);
        $this->assertNull(MasterYpi::find($masterYpi->id), 'MasterYpi should not exist in DB');
    }
}
