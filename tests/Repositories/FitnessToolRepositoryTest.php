<?php namespace Tests\Repositories;

use App\Models\FitnessTool;
use App\Repositories\FitnessToolRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class FitnessToolRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var FitnessToolRepository
     */
    protected $fitnessToolRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->fitnessToolRepo = \App::make(FitnessToolRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->make()->toArray();

        $createdFitnessTool = $this->fitnessToolRepo->create($fitnessTool);

        $createdFitnessTool = $createdFitnessTool->toArray();
        $this->assertArrayHasKey('id', $createdFitnessTool);
        $this->assertNotNull($createdFitnessTool['id'], 'Created FitnessTool must have id specified');
        $this->assertNotNull(FitnessTool::find($createdFitnessTool['id']), 'FitnessTool with given id must be in DB');
        $this->assertModelData($fitnessTool, $createdFitnessTool);
    }

    /**
     * @test read
     */
    public function test_read_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->create();

        $dbFitnessTool = $this->fitnessToolRepo->find($fitnessTool->id);

        $dbFitnessTool = $dbFitnessTool->toArray();
        $this->assertModelData($fitnessTool->toArray(), $dbFitnessTool);
    }

    /**
     * @test update
     */
    public function test_update_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->create();
        $fakeFitnessTool = FitnessTool::factory()->make()->toArray();

        $updatedFitnessTool = $this->fitnessToolRepo->update($fakeFitnessTool, $fitnessTool->id);

        $this->assertModelData($fakeFitnessTool, $updatedFitnessTool->toArray());
        $dbFitnessTool = $this->fitnessToolRepo->find($fitnessTool->id);
        $this->assertModelData($fakeFitnessTool, $dbFitnessTool->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_fitness_tool()
    {
        $fitnessTool = FitnessTool::factory()->create();

        $resp = $this->fitnessToolRepo->delete($fitnessTool->id);

        $this->assertTrue($resp);
        $this->assertNull(FitnessTool::find($fitnessTool->id), 'FitnessTool should not exist in DB');
    }
}
