<?php namespace Tests\Repositories;

use App\Models\CompetitionStatus;
use App\Repositories\CompetitionStatusRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class CompetitionStatusRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var CompetitionStatusRepository
     */
    protected $competitionStatusRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->competitionStatusRepo = \App::make(CompetitionStatusRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->make()->toArray();

        $createdCompetitionStatus = $this->competitionStatusRepo->create($competitionStatus);

        $createdCompetitionStatus = $createdCompetitionStatus->toArray();
        $this->assertArrayHasKey('id', $createdCompetitionStatus);
        $this->assertNotNull($createdCompetitionStatus['id'], 'Created CompetitionStatus must have id specified');
        $this->assertNotNull(CompetitionStatus::find($createdCompetitionStatus['id']), 'CompetitionStatus with given id must be in DB');
        $this->assertModelData($competitionStatus, $createdCompetitionStatus);
    }

    /**
     * @test read
     */
    public function test_read_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->create();

        $dbCompetitionStatus = $this->competitionStatusRepo->find($competitionStatus->id);

        $dbCompetitionStatus = $dbCompetitionStatus->toArray();
        $this->assertModelData($competitionStatus->toArray(), $dbCompetitionStatus);
    }

    /**
     * @test update
     */
    public function test_update_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->create();
        $fakeCompetitionStatus = CompetitionStatus::factory()->make()->toArray();

        $updatedCompetitionStatus = $this->competitionStatusRepo->update($fakeCompetitionStatus, $competitionStatus->id);

        $this->assertModelData($fakeCompetitionStatus, $updatedCompetitionStatus->toArray());
        $dbCompetitionStatus = $this->competitionStatusRepo->find($competitionStatus->id);
        $this->assertModelData($fakeCompetitionStatus, $dbCompetitionStatus->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_competition_status()
    {
        $competitionStatus = CompetitionStatus::factory()->create();

        $resp = $this->competitionStatusRepo->delete($competitionStatus->id);

        $this->assertTrue($resp);
        $this->assertNull(CompetitionStatus::find($competitionStatus->id), 'CompetitionStatus should not exist in DB');
    }
}
