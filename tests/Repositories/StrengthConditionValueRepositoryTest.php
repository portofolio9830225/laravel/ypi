<?php namespace Tests\Repositories;

use App\Models\StrengthConditionValue;
use App\Repositories\StrengthConditionValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class StrengthConditionValueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var StrengthConditionValueRepository
     */
    protected $strengthConditionValueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->strengthConditionValueRepo = \App::make(StrengthConditionValueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->make()->toArray();

        $createdStrengthConditionValue = $this->strengthConditionValueRepo->create($strengthConditionValue);

        $createdStrengthConditionValue = $createdStrengthConditionValue->toArray();
        $this->assertArrayHasKey('id', $createdStrengthConditionValue);
        $this->assertNotNull($createdStrengthConditionValue['id'], 'Created StrengthConditionValue must have id specified');
        $this->assertNotNull(StrengthConditionValue::find($createdStrengthConditionValue['id']), 'StrengthConditionValue with given id must be in DB');
        $this->assertModelData($strengthConditionValue, $createdStrengthConditionValue);
    }

    /**
     * @test read
     */
    public function test_read_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->create();

        $dbStrengthConditionValue = $this->strengthConditionValueRepo->find($strengthConditionValue->id);

        $dbStrengthConditionValue = $dbStrengthConditionValue->toArray();
        $this->assertModelData($strengthConditionValue->toArray(), $dbStrengthConditionValue);
    }

    /**
     * @test update
     */
    public function test_update_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->create();
        $fakeStrengthConditionValue = StrengthConditionValue::factory()->make()->toArray();

        $updatedStrengthConditionValue = $this->strengthConditionValueRepo->update($fakeStrengthConditionValue, $strengthConditionValue->id);

        $this->assertModelData($fakeStrengthConditionValue, $updatedStrengthConditionValue->toArray());
        $dbStrengthConditionValue = $this->strengthConditionValueRepo->find($strengthConditionValue->id);
        $this->assertModelData($fakeStrengthConditionValue, $dbStrengthConditionValue->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_strength_condition_value()
    {
        $strengthConditionValue = StrengthConditionValue::factory()->create();

        $resp = $this->strengthConditionValueRepo->delete($strengthConditionValue->id);

        $this->assertTrue($resp);
        $this->assertNull(StrengthConditionValue::find($strengthConditionValue->id), 'StrengthConditionValue should not exist in DB');
    }
}
