<?php namespace Tests\Repositories;

use App\Models\ResultCategory;
use App\Repositories\ResultCategoryRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class ResultCategoryRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var ResultCategoryRepository
     */
    protected $resultCategoryRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->resultCategoryRepo = \App::make(ResultCategoryRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_result_category()
    {
        $resultCategory = ResultCategory::factory()->make()->toArray();

        $createdResultCategory = $this->resultCategoryRepo->create($resultCategory);

        $createdResultCategory = $createdResultCategory->toArray();
        $this->assertArrayHasKey('id', $createdResultCategory);
        $this->assertNotNull($createdResultCategory['id'], 'Created ResultCategory must have id specified');
        $this->assertNotNull(ResultCategory::find($createdResultCategory['id']), 'ResultCategory with given id must be in DB');
        $this->assertModelData($resultCategory, $createdResultCategory);
    }

    /**
     * @test read
     */
    public function test_read_result_category()
    {
        $resultCategory = ResultCategory::factory()->create();

        $dbResultCategory = $this->resultCategoryRepo->find($resultCategory->id);

        $dbResultCategory = $dbResultCategory->toArray();
        $this->assertModelData($resultCategory->toArray(), $dbResultCategory);
    }

    /**
     * @test update
     */
    public function test_update_result_category()
    {
        $resultCategory = ResultCategory::factory()->create();
        $fakeResultCategory = ResultCategory::factory()->make()->toArray();

        $updatedResultCategory = $this->resultCategoryRepo->update($fakeResultCategory, $resultCategory->id);

        $this->assertModelData($fakeResultCategory, $updatedResultCategory->toArray());
        $dbResultCategory = $this->resultCategoryRepo->find($resultCategory->id);
        $this->assertModelData($fakeResultCategory, $dbResultCategory->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_result_category()
    {
        $resultCategory = ResultCategory::factory()->create();

        $resp = $this->resultCategoryRepo->delete($resultCategory->id);

        $this->assertTrue($resp);
        $this->assertNull(ResultCategory::find($resultCategory->id), 'ResultCategory should not exist in DB');
    }
}
