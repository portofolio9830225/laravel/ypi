<?php namespace Tests\Repositories;

use App\Models\SeasonPlannerDetailValue;
use App\Repositories\SeasonPlannerDetailValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeasonPlannerDetailValueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeasonPlannerDetailValueRepository
     */
    protected $seasonPlannerDetailValueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seasonPlannerDetailValueRepo = \App::make(SeasonPlannerDetailValueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->make()->toArray();

        $createdSeasonPlannerDetailValue = $this->seasonPlannerDetailValueRepo->create($seasonPlannerDetailValue);

        $createdSeasonPlannerDetailValue = $createdSeasonPlannerDetailValue->toArray();
        $this->assertArrayHasKey('id', $createdSeasonPlannerDetailValue);
        $this->assertNotNull($createdSeasonPlannerDetailValue['id'], 'Created SeasonPlannerDetailValue must have id specified');
        $this->assertNotNull(SeasonPlannerDetailValue::find($createdSeasonPlannerDetailValue['id']), 'SeasonPlannerDetailValue with given id must be in DB');
        $this->assertModelData($seasonPlannerDetailValue, $createdSeasonPlannerDetailValue);
    }

    /**
     * @test read
     */
    public function test_read_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->create();

        $dbSeasonPlannerDetailValue = $this->seasonPlannerDetailValueRepo->find($seasonPlannerDetailValue->id);

        $dbSeasonPlannerDetailValue = $dbSeasonPlannerDetailValue->toArray();
        $this->assertModelData($seasonPlannerDetailValue->toArray(), $dbSeasonPlannerDetailValue);
    }

    /**
     * @test update
     */
    public function test_update_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->create();
        $fakeSeasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->make()->toArray();

        $updatedSeasonPlannerDetailValue = $this->seasonPlannerDetailValueRepo->update($fakeSeasonPlannerDetailValue, $seasonPlannerDetailValue->id);

        $this->assertModelData($fakeSeasonPlannerDetailValue, $updatedSeasonPlannerDetailValue->toArray());
        $dbSeasonPlannerDetailValue = $this->seasonPlannerDetailValueRepo->find($seasonPlannerDetailValue->id);
        $this->assertModelData($fakeSeasonPlannerDetailValue, $dbSeasonPlannerDetailValue->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_season_planner_detail_value()
    {
        $seasonPlannerDetailValue = SeasonPlannerDetailValue::factory()->create();

        $resp = $this->seasonPlannerDetailValueRepo->delete($seasonPlannerDetailValue->id);

        $this->assertTrue($resp);
        $this->assertNull(SeasonPlannerDetailValue::find($seasonPlannerDetailValue->id), 'SeasonPlannerDetailValue should not exist in DB');
    }
}
