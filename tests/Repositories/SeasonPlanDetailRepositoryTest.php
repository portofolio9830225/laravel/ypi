<?php namespace Tests\Repositories;

use App\Models\SeasonPlanDetail;
use App\Repositories\SeasonPlanDetailRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class SeasonPlanDetailRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var SeasonPlanDetailRepository
     */
    protected $seasonPlanDetailRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->seasonPlanDetailRepo = \App::make(SeasonPlanDetailRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->make()->toArray();

        $createdSeasonPlanDetail = $this->seasonPlanDetailRepo->create($seasonPlanDetail);

        $createdSeasonPlanDetail = $createdSeasonPlanDetail->toArray();
        $this->assertArrayHasKey('id', $createdSeasonPlanDetail);
        $this->assertNotNull($createdSeasonPlanDetail['id'], 'Created SeasonPlanDetail must have id specified');
        $this->assertNotNull(SeasonPlanDetail::find($createdSeasonPlanDetail['id']), 'SeasonPlanDetail with given id must be in DB');
        $this->assertModelData($seasonPlanDetail, $createdSeasonPlanDetail);
    }

    /**
     * @test read
     */
    public function test_read_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->create();

        $dbSeasonPlanDetail = $this->seasonPlanDetailRepo->find($seasonPlanDetail->id);

        $dbSeasonPlanDetail = $dbSeasonPlanDetail->toArray();
        $this->assertModelData($seasonPlanDetail->toArray(), $dbSeasonPlanDetail);
    }

    /**
     * @test update
     */
    public function test_update_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->create();
        $fakeSeasonPlanDetail = SeasonPlanDetail::factory()->make()->toArray();

        $updatedSeasonPlanDetail = $this->seasonPlanDetailRepo->update($fakeSeasonPlanDetail, $seasonPlanDetail->id);

        $this->assertModelData($fakeSeasonPlanDetail, $updatedSeasonPlanDetail->toArray());
        $dbSeasonPlanDetail = $this->seasonPlanDetailRepo->find($seasonPlanDetail->id);
        $this->assertModelData($fakeSeasonPlanDetail, $dbSeasonPlanDetail->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_season_plan_detail()
    {
        $seasonPlanDetail = SeasonPlanDetail::factory()->create();

        $resp = $this->seasonPlanDetailRepo->delete($seasonPlanDetail->id);

        $this->assertTrue($resp);
        $this->assertNull(SeasonPlanDetail::find($seasonPlanDetail->id), 'SeasonPlanDetail should not exist in DB');
    }
}
