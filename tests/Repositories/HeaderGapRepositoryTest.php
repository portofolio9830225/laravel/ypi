<?php namespace Tests\Repositories;

use App\Models\HeaderGap;
use App\Repositories\HeaderGapRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class HeaderGapRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var HeaderGapRepository
     */
    protected $headerGapRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->headerGapRepo = \App::make(HeaderGapRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_header_gap()
    {
        $headerGap = HeaderGap::factory()->make()->toArray();

        $createdHeaderGap = $this->headerGapRepo->create($headerGap);

        $createdHeaderGap = $createdHeaderGap->toArray();
        $this->assertArrayHasKey('id', $createdHeaderGap);
        $this->assertNotNull($createdHeaderGap['id'], 'Created HeaderGap must have id specified');
        $this->assertNotNull(HeaderGap::find($createdHeaderGap['id']), 'HeaderGap with given id must be in DB');
        $this->assertModelData($headerGap, $createdHeaderGap);
    }

    /**
     * @test read
     */
    public function test_read_header_gap()
    {
        $headerGap = HeaderGap::factory()->create();

        $dbHeaderGap = $this->headerGapRepo->find($headerGap->id);

        $dbHeaderGap = $dbHeaderGap->toArray();
        $this->assertModelData($headerGap->toArray(), $dbHeaderGap);
    }

    /**
     * @test update
     */
    public function test_update_header_gap()
    {
        $headerGap = HeaderGap::factory()->create();
        $fakeHeaderGap = HeaderGap::factory()->make()->toArray();

        $updatedHeaderGap = $this->headerGapRepo->update($fakeHeaderGap, $headerGap->id);

        $this->assertModelData($fakeHeaderGap, $updatedHeaderGap->toArray());
        $dbHeaderGap = $this->headerGapRepo->find($headerGap->id);
        $this->assertModelData($fakeHeaderGap, $dbHeaderGap->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_header_gap()
    {
        $headerGap = HeaderGap::factory()->create();

        $resp = $this->headerGapRepo->delete($headerGap->id);

        $this->assertTrue($resp);
        $this->assertNull(HeaderGap::find($headerGap->id), 'HeaderGap should not exist in DB');
    }
}
