<?php namespace Tests\Repositories;

use App\Models\KpiValue;
use App\Repositories\KpiValueRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class KpiValueRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var KpiValueRepository
     */
    protected $kpiValueRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->kpiValueRepo = \App::make(KpiValueRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_kpi_value()
    {
        $kpiValue = KpiValue::factory()->make()->toArray();

        $createdKpiValue = $this->kpiValueRepo->create($kpiValue);

        $createdKpiValue = $createdKpiValue->toArray();
        $this->assertArrayHasKey('id', $createdKpiValue);
        $this->assertNotNull($createdKpiValue['id'], 'Created KpiValue must have id specified');
        $this->assertNotNull(KpiValue::find($createdKpiValue['id']), 'KpiValue with given id must be in DB');
        $this->assertModelData($kpiValue, $createdKpiValue);
    }

    /**
     * @test read
     */
    public function test_read_kpi_value()
    {
        $kpiValue = KpiValue::factory()->create();

        $dbKpiValue = $this->kpiValueRepo->find($kpiValue->id);

        $dbKpiValue = $dbKpiValue->toArray();
        $this->assertModelData($kpiValue->toArray(), $dbKpiValue);
    }

    /**
     * @test update
     */
    public function test_update_kpi_value()
    {
        $kpiValue = KpiValue::factory()->create();
        $fakeKpiValue = KpiValue::factory()->make()->toArray();

        $updatedKpiValue = $this->kpiValueRepo->update($fakeKpiValue, $kpiValue->id);

        $this->assertModelData($fakeKpiValue, $updatedKpiValue->toArray());
        $dbKpiValue = $this->kpiValueRepo->find($kpiValue->id);
        $this->assertModelData($fakeKpiValue, $dbKpiValue->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_kpi_value()
    {
        $kpiValue = KpiValue::factory()->create();

        $resp = $this->kpiValueRepo->delete($kpiValue->id);

        $this->assertTrue($resp);
        $this->assertNull(KpiValue::find($kpiValue->id), 'KpiValue should not exist in DB');
    }
}
