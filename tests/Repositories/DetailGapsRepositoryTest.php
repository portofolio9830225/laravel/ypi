<?php namespace Tests\Repositories;

use App\Models\DetailGaps;
use App\Repositories\DetailGapsRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Tests\ApiTestTrait;

class DetailGapsRepositoryTest extends TestCase
{
    use ApiTestTrait, DatabaseTransactions;

    /**
     * @var DetailGapsRepository
     */
    protected $detailGapsRepo;

    public function setUp() : void
    {
        parent::setUp();
        $this->detailGapsRepo = \App::make(DetailGapsRepository::class);
    }

    /**
     * @test create
     */
    public function test_create_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->make()->toArray();

        $createdDetailGaps = $this->detailGapsRepo->create($detailGaps);

        $createdDetailGaps = $createdDetailGaps->toArray();
        $this->assertArrayHasKey('id', $createdDetailGaps);
        $this->assertNotNull($createdDetailGaps['id'], 'Created DetailGaps must have id specified');
        $this->assertNotNull(DetailGaps::find($createdDetailGaps['id']), 'DetailGaps with given id must be in DB');
        $this->assertModelData($detailGaps, $createdDetailGaps);
    }

    /**
     * @test read
     */
    public function test_read_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->create();

        $dbDetailGaps = $this->detailGapsRepo->find($detailGaps->id);

        $dbDetailGaps = $dbDetailGaps->toArray();
        $this->assertModelData($detailGaps->toArray(), $dbDetailGaps);
    }

    /**
     * @test update
     */
    public function test_update_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->create();
        $fakeDetailGaps = DetailGaps::factory()->make()->toArray();

        $updatedDetailGaps = $this->detailGapsRepo->update($fakeDetailGaps, $detailGaps->id);

        $this->assertModelData($fakeDetailGaps, $updatedDetailGaps->toArray());
        $dbDetailGaps = $this->detailGapsRepo->find($detailGaps->id);
        $this->assertModelData($fakeDetailGaps, $dbDetailGaps->toArray());
    }

    /**
     * @test delete
     */
    public function test_delete_detail_gaps()
    {
        $detailGaps = DetailGaps::factory()->create();

        $resp = $this->detailGapsRepo->delete($detailGaps->id);

        $this->assertTrue($resp);
        $this->assertNull(DetailGaps::find($detailGaps->id), 'DetailGaps should not exist in DB');
    }
}
