<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailPbKpi
 * @package App\Models
 * @version March 23, 2022, 1:51 pm UTC
 *
 * @property integer $kpi_physic_id
 * @property string $tanggal
 * @property number $pb
 * @property number $target
 * @property number $actual
 */
class DetailPbKpi extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_pb_kpis';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kpi_physic_id',
        'tanggal',
        'pb',
        'target',
        'actual'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kpi_physic_id' => 'integer',
        'tanggal' => 'date',
        'pb' => 'float',
        'target' => 'float',
        'actual' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kpi_physic_id' => 'required|integer',
        'tanggal' => 'required',
        'pb' => 'required|numeric',
        'target' => 'required|numeric',
        'actual' => 'required|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
