<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class StrengthConditioningData
 * @package App\Models
 * @version March 23, 2022, 7:14 am UTC
 *
 * @property integer $pemain_id
 * @property integer $gym_type
 * @property integer $master_ypi_id
 * @property integer $week
 * @property string $hari
 * @property string $tanggal
 * @property string $comments
 * @property number $rep_1
 * @property number $weight_1
 * @property number $rep_2
 * @property number $weight_2
 * @property number $rep_3
 * @property number $weight_3
 * @property number $rep_4
 * @property number $weight_4
 * @property number $rep_5
 * @property number $weight_5
 * @property number $rep_6
 * @property number $weight_6
 * @property number $tut_rep
 * @property number $tut_exercise
 * @property number $tonnage
 * @property number $average
 * @property number $physical_load
 * @property number $predicted_1rm
 * @property number $average_1rm_predict
 * @property number $actual_1rm
 * @property number $average_1rm
 * @property number $weekly_tut
 * @property number $weekly_tonnage
 * @property number $weekly_average
 * @property number $weekly_load
 */
class StrengthConditioningData extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'strength_conditioning_datas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'gym_type',
        'master_ypi_id',
        'week',
        'hari',
        'tanggal',
        'comments',
        'rep_1',
        'weight_1',
        'rep_2',
        'weight_2',
        'rep_3',
        'weight_3',
        'rep_4',
        'weight_4',
        'rep_5',
        'weight_5',
        'rep_6',
        'weight_6',
        'tut_rep',
        'tut_exercise',
        'tonnage',
        'average',
        'physical_load',
        'predicted_1rm',
        'average_1rm_predict',
        'actual_1rm',
        'average_1rm',
        'weekly_tut',
        'weekly_tonnage',
        'weekly_average',
        'weekly_load'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'gym_type' => 'integer',
        'master_ypi_id' => 'integer',
        'week' => 'integer',
        'hari' => 'string',
        'tanggal' => 'date',
        'comments' => 'string',
        'rep_1' => 'float',
        'weight_1' => 'float',
        'rep_2' => 'float',
        'weight_2' => 'float',
        'rep_3' => 'float',
        'weight_3' => 'float',
        'rep_4' => 'float',
        'weight_4' => 'float',
        'rep_5' => 'float',
        'weight_5' => 'float',
        'rep_6' => 'float',
        'weight_6' => 'float',
        'tut_rep' => 'float',
        'tut_exercise' => 'float',
        'tonnage' => 'float',
        'average' => 'float',
        'physical_load' => 'float',
        'predicted_1rm' => 'float',
        'average_1rm_predict' => 'float',
        'actual_1rm' => 'float',
        'average_1rm' => 'float',
        'weekly_tut' => 'float',
        'weekly_tonnage' => 'float',
        'weekly_average' => 'float',
        'weekly_load' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'integer',
        'gym_type' => 'integer',
        'master_ypi_id' => 'integer',
        'week' => 'integer',
        'hari' => 'string|max:10',
        'tanggal' => '',
        'comments' => 'string|max:50',
        'rep_1' => 'numeric',
        'weight_1' => 'numeric',
        'rep_2' => 'numeric',
        'weight_2' => 'numeric',
        'rep_3' => 'numeric',
        'weight_3' => 'numeric',
        'rep_4' => 'numeric',
        'weight_4' => 'numeric',
        'rep_5' => 'numeric',
        'weight_5' => 'numeric',
        'rep_6' => 'numeric',
        'weight_6' => 'numeric',
        'tut_rep' => 'numeric',
        'tut_exercise' => 'numeric',
        'tonnage' => 'numeric',
        'average' => 'numeric',
        'physical_load' => 'numeric',
        'predicted_1rm' => 'numeric',
        'average_1rm_predict' => 'numeric',
        'actual_1rm' => 'numeric',
        'average_1rm' => 'numeric',
        'weekly_tut' => 'numeric',
        'weekly_tonnage' => 'numeric',
        'weekly_average' => 'numeric',
        'weekly_load' => 'numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
