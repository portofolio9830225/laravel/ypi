<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class TabelKategori
 * @package App\Models
 * @version March 23, 2022, 5:49 am UTC
 *
 * @property string $nama_kategori
 * @property integer $level_kategori_id
 */
class TabelKategori extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'tabel_kategoris';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama_kategori',
        'level_kategori_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_kategori' => 'string',
        'level_kategori_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_kategori' => 'required|string|max:50',
        'level_kategori_id' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function level()
    {
        return $this->belongsTo(LevelKategori::class, 'level_kategori_id');
    }

    public function competition_detail()
    {
        return $this->hasMany(MasterAchievement::class, 'kategori_id');
    }
}
