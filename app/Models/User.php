<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'foto',
        'active_club_id',
        'as_coach_id',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
        'pivot'
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'foto' => 'string',
        'active_club_id' => 'string'
    ];

    public function role_club()
    {
        return $this->belongsToMany(RoleClubMember::class, 'club_member', 'user_id');
    }

    public function club()
    {
        return $this->belongsTo(Club::class, 'active_club_id', 'id');
    }

    public function asCoach()
    {
        return $this->belongsTo(MasterPelatih::class, 'as_coach_id', 'id');
    }
}
