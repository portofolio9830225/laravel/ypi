<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailkpiEvent
 * @package App\Models
 * @version March 23, 2022, 7:07 am UTC
 *
 * @property integer $pemain_id
 * @property string $tanggal_kpi
 * @property string $keterangan_1
 * @property string $keterangan_2
 * @property string $keterangan_3
 * @property string $keterangan_4
 * @property string $event_name
 * @property integer $target_1
 * @property integer $target_2
 * @property integer $target_3
 * @property integer $target_4
 * @property integer $actual_1
 * @property integer $actual_2
 * @property integer $actual_3
 * @property integer $actual_4
 */
class DetailkpiEvent extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_kpi_events';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'tanggal_kpi',
        'keterangan_1',
        'keterangan_2',
        'keterangan_3',
        'keterangan_4',
        'event_name',
        'target_1',
        'target_2',
        'target_3',
        'target_4',
        'actual_1',
        'actual_2',
        'actual_3',
        'actual_4'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'tanggal_kpi' => 'date',
        'keterangan_1' => 'string',
        'keterangan_2' => 'string',
        'keterangan_3' => 'string',
        'keterangan_4' => 'string',
        'event_name' => 'string',
        'target_1' => 'string',
        'target_2' => 'string',
        'target_3' => 'string',
        'target_4' => 'string',
        'actual_1' => 'string',
        'actual_2' => 'string',
        'actual_3' => 'string',
        'actual_4' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'required|integer',
        'tanggal_kpi' => 'required',
        'event_name' => 'required|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
