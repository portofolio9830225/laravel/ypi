<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class SeasonPlanDetail
 * @package App\Models
 * @version August 1, 2022, 6:53 am UTC
 *
 * @property integer $season_plan_id
 * @property string $name
 * @property string $input_type
 * @property string $desc_en
 * @property string $desc_id
 */
class SeasonPlanDetail extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'season_plan_details';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'season_plan_id',
        'name',
        'input_type',
        'desc_en',
        'desc_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'season_plan_id' => 'integer',
        'name' => 'string',
        'input_type' => 'string',
        'desc_en' => 'string',
        'desc_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];


    public function seasonPlan()
    {
        return $this->belongsTo(SeasonPlan::class, 'season_plan_id', 'id');
    }
}
