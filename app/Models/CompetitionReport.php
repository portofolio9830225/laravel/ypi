<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class CompetitionReport
 * @package App\Models
 * @version March 23, 2022, 7:11 am UTC
 *
 * @property integer $pemain_id
 * @property integer $master_achievement_id
 * @property string $venue
 * @property string $specialist
 * @property string $event_level
 * @property integer $result_id
 * @property string $summary_notes
 * @property string $suggestion_notes
 * @property integer $kelompok_id
 * @property integer $partner_id
 */
class CompetitionReport extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'competition_reports';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'master_achievement_id',
        'venue',
        'specialist',
        'event_level',
        'result_id',
        'summary_notes',
        'suggestion_notes',
        'kelompok_id',
        'partner_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'master_achievement_id' => 'integer',
        'venue' => 'string',
        'specialist' => 'string',
        'event_level' => 'string',
        'result_id' => 'integer',
        'summary_notes' => 'string',
        'suggestion_notes' => 'string',
        'kelompok_id' => 'integer',
        'partner_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'integer',
        'master_achievement_id' => 'integer',
        'venue' => 'string|max:50',
        'specialist' => 'string|max:50',
        'event_level' => 'string|max:50',
        'result_id' => 'integer',
        'summary_notes' => 'string',
        'suggestion_notes' => 'string',
        'kelompok_id' => 'integer',
        'partner_id' => 'integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * Get the player that owns the CompetitionReport
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(MasterPemain::class, 'pemain_id');
    }

    public function competitionInfo()
    {
        return $this->belongsTo(MasterAchievement::class, 'master_achievement_id');
    }

    public function resultCategory()
    {
        return $this->belongsTo(ResultCategory::class, 'result_id');
    }
}
