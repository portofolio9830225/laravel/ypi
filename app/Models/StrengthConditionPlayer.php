<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class StrengthConditionPlayer
 * @package App\Models
 * @version March 23, 2022, 1:52 pm UTC
 *
 * @property integer $strength_conditioning_id
 * @property integer $pemain_id
 * @property number $body_weight
 */
class StrengthConditionPlayer extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'strength_condition_players';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'strength_conditioning_id',
        'pemain_id',
        'body_weight'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'strength_conditioning_id' => 'integer',
        'pemain_id' => 'integer',
        'body_weight' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'strength_conditioning_id' => 'integer',
        'pemain_id' => 'integer',
        'body_weight' => '',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
