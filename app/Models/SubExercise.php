<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class SubExercise
 * @package App\Models
 * @version March 23, 2022, 6:07 am UTC
 *
 * @property string $excercise_desc
 * @property integer $gym_type_id
 * @property integer $tut_per_rep
 * @property integer $exercise_category_id
 */
class SubExercise extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'sub_exercises';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'excercise_desc',
        'gym_type_id',
        'tut_per_rep',
        'exercise_category_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'excercise_desc' => 'string',
        'gym_type_id' => 'integer',
        'tut_per_rep' => 'integer',
        'exercise_category_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'excercise_desc' => 'required|string|max:50',
        'gym_type_id' => 'required|integer',
        'tut_per_rep' => 'required|integer',
        'exercise_category_id' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function joinAction()
    {
        return $this->belongsTo(GymType::class, 'gym_type_id');
    }

    public function muscleGroup()
    {
        return $this->belongsTo(ExerciseCategory::class, 'exercise_category_id');
    }
}
