<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailKpiPhysic
 * @package App\Models
 * @version March 23, 2022, 1:51 pm UTC
 *
 * @property integer $pemain_id
 * @property string $tanggal_kpi
 * @property string $keterangan
 */
class DetailKpiPhysic extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_kpi_physics';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'tanggal_kpi',
        'keterangan',
        'tanggal_1',
        'pb_1',
        'target_1',
        'actual_1',
        'tanggal_2',
        'pb_2',
        'target_2',
        'actual_2',
        'tanggal_3',
        'pb_3',
        'target_3',
        'actual_3',
        'tanggal_4',
        'pb_4',
        'target_4',
        'actual_4',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'tanggal_kpi' => 'date',
        'keterangan' => 'string',
        'tanggal_1' => 'date',
        'pb_1' => 'integer',
        'target_1' => 'integer',
        'actual_1' => 'integer',
        'tanggal_2' => 'date',
        'pb_2' => 'integer',
        'target_2' => 'integer',
        'actual_2' => 'integer',
        'tanggal_3' => 'date',
        'pb_3' => 'integer',
        'target_3' => 'integer',
        'actual_3' => 'integer',
        'tanggal_4' => 'date',
        'pb_4' => 'integer',
        'target_4' => 'integer',
        'actual_4' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'required|integer',
        'tanggal_kpi' => 'required',
        'keterangan' => 'required|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
