<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class TrainingAttendance
 * @package App\Models
 * @version March 23, 2022, 7:11 am UTC
 *
 * @property integer $kelompok_id
 * @property integer $master_pelatih_id
 * @property integer $pemain_id
 * @property string $tanggal
 * @property integer $session
 * @property integer $presence_id
 * @property string $note
 */
class TrainingAttendance extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'training_attendances';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kelompok_id',
        'master_pelatih_id',
        'pemain_id',
        'tanggal',
        'session',
        'presence_id',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kelompok_id' => 'integer',
        'master_pelatih_id' => 'integer',
        'pemain_id' => 'integer',
        'tanggal' => 'date',
        'session' => 'integer',
        'presence_id' => 'integer',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kelompok_id' => 'integer',
        'master_pelatih_id' => 'integer',
        'pemain_id' => 'integer',
        'tanggal' => '',
        'session' => 'integer',
        'presence_id' => 'integer',
        'note' => 'string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * Get the status that owns the TrainingAttendance
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status()
    {
        return $this->belongsTo(SettingPresence::class, 'presence_id');
    }
}
