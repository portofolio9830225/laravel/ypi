<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class HeaderKpi
 * @package App\Models
 * @version June 25, 2022, 9:34 am UTC
 *
 * @property integer $pemain_id
 * @property string $tanggal_kpi
 */
class HeaderKpi extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'header_kpi';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'tanggal_kpi'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'tanggal_kpi' => 'date'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'required|integer',
        'tanggal_kpi' => 'required',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
