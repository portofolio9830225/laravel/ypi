<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class Kpi
 * @package App\Models
 * @version July 28, 2022, 7:58 am UTC
 *
 * @property string $date
 * @property integer $player_id
 * @property integer $umur
 * @property number $height
 * @property number $weight
 * @property number $beep_test
 * @property number $vo2max
 * @property number $court_agility_1
 * @property number $court_agility_2
 * @property number $court_agility_3
 * @property number $squad
 * @property number $bench_press
 * @property number $vertical_jump
 * @property number $skipping
 * @property number $run
 * @property string $note
 */
class Kpi extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'kpis';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'date',
        'player_id',
        'umur',
        'height',
        'weight',
        'beep_test',
        'vo2max',
        'court_agility_1',
        'court_agility_2',
        'court_agility_3',
        'squad',
        'bench_press',
        'vertical_jump',
        'skipping',
        'run',
        'note'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'date',
        'player_id' => 'integer',
        'umur' => 'integer',
        'height' => 'float',
        'weight' => 'float',
        'beep_test' => 'float',
        'vo2max' => 'float',
        'court_agility_1' => 'float',
        'court_agility_2' => 'float',
        'court_agility_3' => 'float',
        'squad' => 'float',
        'bench_press' => 'float',
        'vertical_jump' => 'float',
        'skipping' => 'float',
        'run' => 'float',
        'note' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
