<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class SettingAttendance
 * @package App\Models
 * @version March 23, 2022, 6:23 am UTC
 *
 * @property integer $bulan
 * @property integer $tahun
 * @property string $kelompok
 * @property integer $kode_pelatih
 * @property string $tanggal
 * @property integer $session
 * @property integer $am_pm
 */
class SettingAttendance extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'setting_attendances';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'bulan',
        'tahun',
        'kelompok',
        'kode_pelatih',
        'tanggal',
        'session',
        'am_pm'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'bulan' => 'integer',
        'tahun' => 'integer',
        'kelompok' => 'string',
        'kode_pelatih' => 'integer',
        'tanggal' => 'date',
        'session' => 'integer',
        'am_pm' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'bulan' => 'required|integer',
        'tahun' => 'required|integer',
        'kelompok' => 'required|string|max:50',
        'kode_pelatih' => 'required|integer',
        'tanggal' => 'required',
        'session' => 'required|integer',
        'am_pm' => 'required|integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
