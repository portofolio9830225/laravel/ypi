<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class HeightPredicted
 * @package App\Models
 * @version March 23, 2022, 7:15 am UTC
 *
 * @property integer $pemain_id
 * @property integer $age
 * @property string $date_predicted
 * @property number $height
 * @property number $weight
 * @property number $fathers_height
 * @property number $mothers_height
 * @property number $predicted_height
 * @property number $maturity_status
 */
class HeightPredicted extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'height_predicteds';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'age',
        'date_predicted',
        'height',
        'weight',
        'fathers_height',
        'mothers_height',
        'predicted_height',
        'maturity_status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'age' => 'integer',
        'date_predicted' => 'date',
        'height' => 'float',
        'weight' => 'float',
        'fathers_height' => 'float',
        'mothers_height' => 'float',
        'predicted_height' => 'float',
        'maturity_status' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'integer',
        'age' => 'integer',
        'date_predicted' => '',
        'height' => 'numeric',
        'weight' => 'numeric',
        'fathers_height' => 'numeric',
        'mothers_height' => 'numeric',
        'predicted_height' => 'numeric',
        'maturity_status' => 'numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * Get the heightData associated with the HeightPredicted
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function heightData()
    {
        return $this->hasOne(HeightData::class);
    }

    public function pubertyData()
    {
        return $this->hasOne(PubertyData::class);
    }

    public function playerData()
    {
        return $this->belongsTo(MasterPemain::class, 'pemain_id');
    }
}
