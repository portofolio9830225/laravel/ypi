<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class MasterAchievement
 * @package App\Models
 * @version March 23, 2022, 6:01 am UTC
 *
 * @property string $nama_event
 * @property integer $kategori_id
 * @property string $tanggal_mulai
 * @property string $tanggal_akhir
 * @property integer $competition_status_id
 * @property string $color
 */
class MasterAchievement extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'master_achievements';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama_event',
        'kategori_id',
        'tanggal_mulai',
        'tanggal_akhir',
        'competition_status_id',
        'color'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_event' => 'string',
        'kategori_id' => 'integer',
        'tanggal_mulai' => 'date',
        'tanggal_akhir' => 'date',
        'competition_status_id' => 'integer',
        'color' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_event' => 'string|max:100',
        'kategori_id' => 'integer',
        'tanggal_mulai' => '',
        'tanggal_akhir' => '',
        'competition_status_id' => 'integer',
        'color' => 'string|max:15',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function competitionInfo()
    {
        return $this->belongsTo(TabelKategori::class, 'kategori_id');
    }

    public function competitionStatus()
    {
        return $this->belongsTo(CompetitionStatus::class, 'competition_status_id');
    }

    public function competitionGroup()
    {
        return $this->hasOne(CompetitionGroup::class, 'competition_id');
    }
}
