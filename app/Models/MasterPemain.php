<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class MasterPemain
 * @package App\Models
 * @version March 23, 2022, 5:58 am UTC
 *
 * @property string $nomor_ic
 * @property string $nama_lengkap
 * @property string $alamat
 * @property string $negara
 * @property integer $kodepos
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $home_phone
 * @property string $hand_phone
 * @property string $business_phone
 * @property string $nama_sekolah
 * @property string $kelas
 * @property string $wali_kelas
 * @property string $no_ruang
 * @property string $keterangan
 * @property string $golongan_darah
 * @property string $email
 * @property integer $contact_id
 * @property boolean $aktif
 * @property string $sex
 * @property integer $kelompok_id
 * @property boolean $tunggal_putra
 * @property boolean $tunggal_putri
 * @property boolean $ganda_putra
 * @property boolean $ganda_putri
 * @property boolean $ganda_campuran
 */
class MasterPemain extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'master_pemains';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_pelatih_id',
        'nomor_ic',
        'nama_lengkap',
        'alamat',
        'negara',
        'kodepos',
        'tempat_lahir',
        'tanggal_lahir',
        'home_phone',
        'hand_phone',
        'business_phone',
        'nama_sekolah',
        'kelas',
        'wali_kelas',
        'no_ruang',
        'keterangan',
        'golongan_darah',
        'email',
        'contact_id',
        'aktif',
        'sex',
        'kelompok_id',
        'tunggal_putra',
        'tunggal_putri',
        'ganda_putra',
        'ganda_putri',
        'ganda_campuran'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nomor_ic' => 'string',
        'nama_lengkap' => 'string',
        'alamat' => 'string',
        'negara' => 'string',
        'kodepos' => 'integer',
        'tempat_lahir' => 'string',
        'tanggal_lahir' => 'date',
        'home_phone' => 'string',
        'hand_phone' => 'string',
        'business_phone' => 'string',
        'nama_sekolah' => 'string',
        'kelas' => 'string',
        'wali_kelas' => 'string',
        'no_ruang' => 'string',
        'keterangan' => 'string',
        'golongan_darah' => 'string',
        'email' => 'string',
        'contact_id' => 'integer',
        'aktif' => 'boolean',
        'sex' => 'string',
        'kelompok_id' => 'integer',
        'tunggal_putra' => 'boolean',
        'tunggal_putri' => 'boolean',
        'ganda_putra' => 'boolean',
        'ganda_putri' => 'boolean',
        'ganda_campuran' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nomor_ic' => 'required|string|max:50',
        'nama_lengkap' => 'required|string|max:100',
        'alamat' => 'required|string|max:100',
        'negara' => 'required|string|max:50',
        'kodepos' => 'required|integer',
        'tempat_lahir' => 'required|string|max:50',
        'tanggal_lahir' => 'required',
        'home_phone' => 'required|string|max:25',
        'hand_phone' => 'required|string|max:25',
        'business_phone' => 'required|string|max:25',
        'nama_sekolah' => 'required|string|max:50',
        'kelas' => 'required|string|max:10',
        'wali_kelas' => 'required|string|max:50',
        'no_ruang' => 'required|string|max:10',
        'keterangan' => 'required|string',
        'golongan_darah' => 'required|string|max:10',
        'email' => 'required|string|max:255',
        'contact_id' => 'required|integer',
        'aktif' => 'required|boolean',
        'sex' => 'required|string|max:15',
        'kelompok_id' => 'required|integer',
        'tunggal_putra' => 'required|boolean',
        'tunggal_putri' => 'required|boolean',
        'ganda_putra' => 'required|boolean',
        'ganda_putri' => 'required|boolean',
        'ganda_campuran' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function contact()
    {
        return $this->belongsTo(MasterContact::class, 'contact_id');
    }

    public function group()
    {
        return $this->belongsTo(TabelKelompok::class, 'kelompok_id');
    }
    public function coach()
    {
        return $this->belongsTo(MasterPelatih::class, 'master_pelatih_id');
    }
}
