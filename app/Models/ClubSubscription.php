<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClubSubscription extends Model
{
    use HasFactory;
    protected $table = 'club_subscription';
    protected $guarded = ['id'];
}
