<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class AnnualPlanSetting
 * @package App\Models
 * @version March 23, 2022, 6:13 am UTC
 *
 * @property integer $master_ypi_id
 * @property integer $kelompok
 * @property string $description
 * @property string $color
 */
class AnnualPlanSetting extends Model
{
    use BelongsToTenant;

    use HasFactory;

    public $table = 'annual_plan_settings';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $fillable = [
        'master_ypi_id',
        'kelompok',
        'description',
        'color'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'kelompok' => 'integer',
        'description' => 'string',
        'color' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'required|integer',
        'kelompok' => 'required|integer',
        'description' => 'required|string|max:100',
        'color' => 'required|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
    ];

    public function ypi()
    {
        return $this->belongsTo(MasterYpi::class, 'master_ypi_id');
    }

    public function groupActivity()
    {
        return $this->belongsTo(GroupActivity::class, 'kelompok');
    }
}
