<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class MedicalRecord
 * @package App\Models
 * @version March 23, 2022, 7:07 am UTC
 *
 * @property integer $pemain_id
 * @property integer $master_ypi_id
 * @property integer $week
 * @property string $tanggal
 * @property string $hari
 * @property boolean $injured
 * @property string $primary_injured
 * @property string $detail_injured
 * @property boolean $treatment_injured
 * @property integer $injured_days
 * @property integer $treatment_injured_days
 * @property boolean $illied
 * @property string $primary_illied
 * @property string $symptomps
 * @property boolean $treatment_illied
 * @property integer $illied_days
 * @property integer $treatment_illied_days
 */
class MedicalRecord extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'medical_records';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'master_ypi_id',
        'week',
        'tanggal',
        'hari',
        'injured',
        'primary_injured',
        'detail_injured',
        'treatment_injured',
        'injured_days',
        'treatment_injured_days',
        'illied',
        'primary_illied',
        'symptomps',
        'treatment_illied',
        'illied_days',
        'treatment_illied_days'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'master_ypi_id' => 'integer',
        'week' => 'integer',
        'tanggal' => 'date',
        'hari' => 'string',
        'injured' => 'boolean',
        'primary_injured' => 'string',
        'detail_injured' => 'string',
        'treatment_injured' => 'boolean',
        'injured_days' => 'integer',
        'treatment_injured_days' => 'integer',
        'illied' => 'boolean',
        'primary_illied' => 'string',
        'symptomps' => 'string',
        'treatment_illied' => 'boolean',
        'illied_days' => 'integer',
        'treatment_illied_days' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'integer',
        'master_ypi_id' => 'integer',
        'week' => 'integer',
        'tanggal' => '',
        'hari' => 'string|max:10',
        'injured' => 'boolean',
        'primary_injured' => 'string|max:50',
        'detail_injured' => 'string|max:50',
        'treatment_injured' => 'boolean',
        'injured_days' => 'integer',
        'treatment_injured_days' => 'integer',
        'illied' => 'boolean',
        'primary_illied' => 'string|max:50',
        'symptomps' => 'string|max:50',
        'treatment_illied' => 'boolean',
        'illied_days' => 'integer',
        'treatment_illied_days' => 'integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function player()
    {
        return $this->belongsTo(MasterPemain::class, 'pemain_id', 'id');
    }

    public function ypi()
    {
        return $this->belongsTo(MasterYpi::class, 'master_ypi_id', 'id');
    }
}
