<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class DetailCompetitionReport
 * @package App\Models
 * @version March 23, 2022, 1:52 pm UTC
 *
 * @property integer $competition_report_id
 * @property string $round
 * @property string $opponent
 * @property string $nationality
 * @property integer $set1a
 * @property integer $set1b
 * @property integer $set2a
 * @property integer $set2b
 * @property integer $set3a
 * @property integer $set3b
 * @property string $comment
 * @property string $result
 */
class DetailCompetitionReport extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_competition_reports';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'competition_report_id',
        'round',
        'opponent',
        'nationality',
        'set1a',
        'set1b',
        'set2a',
        'set2b',
        'set3a',
        'set3b',
        'comment',
        'result'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'competition_report_id' => 'integer',
        'round' => 'string',
        'opponent' => 'string',
        'nationality' => 'string',
        'set1a' => 'integer',
        'set1b' => 'integer',
        'set2a' => 'integer',
        'set2b' => 'integer',
        'set3a' => 'integer',
        'set3b' => 'integer',
        'comment' => 'string',
        'result' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'competition_report_id' => 'required|integer',
        'round' => 'required|string|max:10',
        'opponent' => 'required|string|max:50',
        'nationality' => 'required|string|max:50',
        'set1a' => 'required|integer',
        'set1b' => 'required|integer',
        'set2a' => 'required|integer',
        'set2b' => 'required|integer',
        'set3a' => 'required|integer',
        'set3b' => 'required|integer',
        'comment' => 'required|string|max:65',
        'result' => 'required|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
