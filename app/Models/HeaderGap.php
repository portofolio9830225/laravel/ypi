<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class HeaderGap
 * @package App\Models
 * @version March 23, 2022, 6:29 am UTC
 *
 * @property integer $pemain_id
 * @property string $tanggal
 * @property string $event_name
 * @property string $present
 * @property string $future
 * @property string $player_gap
 * @property number $gap_value1
 * @property number $gap_value2
 * @property number $gap_value3
 * @property number $gap_value4
 * @property number $gap_value5
 * @property string $competition
 */
class HeaderGap extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'header_gaps';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'tanggal',
        'event_name',
        'present',
        'future',
        'player_gap',
        'gap_value1',
        'gap_value2',
        'gap_value3',
        'gap_value4',
        'gap_value5',
        'competition'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'tanggal' => 'date',
        'event_name' => 'string',
        'present' => 'string',
        'future' => 'string',
        'player_gap' => 'string',
        'gap_value1' => 'float',
        'gap_value2' => 'float',
        'gap_value3' => 'float',
        'gap_value4' => 'float',
        'gap_value5' => 'float',
        'competition' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'required|integer',
        'tanggal' => 'required',
        'event_name' => 'required|string|max:20',
        'present' => 'required|string|max:50',
        'future' => 'required|string|max:50',
        'player_gap' => 'required|string|max:50',
        'gap_value1' => 'required|numeric',
        'gap_value2' => 'required|numeric',
        'gap_value3' => 'required|numeric',
        'gap_value4' => 'required|numeric',
        'gap_value5' => 'required|numeric',
        'competition' => 'required|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
