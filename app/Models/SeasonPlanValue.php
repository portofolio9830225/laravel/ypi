<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class SeasonPlanValue
 * @package App\Models
 * @version August 1, 2022, 7:04 am UTC
 *
 * @property integer $ypi_id
 * @property integer $season_plan_id
 * @property integer $season_plan_detail_id
 * @property string $bg_color
 * @property boolean $active
 * @property string $week_1
 * @property string $week_2
 * @property string $week_3
 * @property string $week_4
 * @property string $week_5
 * @property string $week_6
 * @property string $week_7
 * @property string $week_8
 * @property string $week_9
 * @property string $week_10
 * @property string $week_11
 * @property string $week_12
 * @property string $week_13
 * @property string $week_14
 * @property string $week_15
 * @property string $week_16
 * @property string $week_17
 * @property string $week_18
 * @property string $week_19
 * @property string $week_20
 * @property string $week_21
 * @property string $week_22
 * @property string $week_23
 * @property string $week_24
 * @property string $week_25
 * @property string $week_26
 * @property string $week_27
 * @property string $week_28
 * @property string $week_29
 * @property string $week_30
 * @property string $week_31
 * @property string $week_32
 * @property string $week_33
 * @property string $week_34
 * @property string $week_35
 * @property string $week_36
 * @property string $week_37
 * @property string $week_38
 * @property string $week_39
 * @property string $week_40
 * @property string $week_41
 * @property string $week_42
 * @property string $week_43
 * @property string $week_44
 * @property string $week_45
 * @property string $week_46
 * @property string $week_47
 * @property string $week_48
 * @property string $week_49
 * @property string $week_50
 * @property string $week_51
 * @property string $week_52
 * @property string $week_53
 * @property string $week_54
 * @property string $week_55
 */
class SeasonPlanValue extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'season_plan_values';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'ypi_id',
        'season_plan_id',
        'season_plan_detail_id',
        'bg_color',
        'active',
        'week_1',
        'week_2',
        'week_3',
        'week_4',
        'week_5',
        'week_6',
        'week_7',
        'week_8',
        'week_9',
        'week_10',
        'week_11',
        'week_12',
        'week_13',
        'week_14',
        'week_15',
        'week_16',
        'week_17',
        'week_18',
        'week_19',
        'week_20',
        'week_21',
        'week_22',
        'week_23',
        'week_24',
        'week_25',
        'week_26',
        'week_27',
        'week_28',
        'week_29',
        'week_30',
        'week_31',
        'week_32',
        'week_33',
        'week_34',
        'week_35',
        'week_36',
        'week_37',
        'week_38',
        'week_39',
        'week_40',
        'week_41',
        'week_42',
        'week_43',
        'week_44',
        'week_45',
        'week_46',
        'week_47',
        'week_48',
        'week_49',
        'week_50',
        'week_51',
        'week_52',
        'week_53',
        'week_54',
        'week_55'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ypi_id' => 'integer',
        'season_plan_id' => 'integer',
        'season_plan_detail_id' => 'integer',
        'bg_color' => 'string',
        'active' => 'boolean',
        'week_1' => 'string',
        'week_2' => 'string',
        'week_3' => 'string',
        'week_4' => 'string',
        'week_5' => 'string',
        'week_6' => 'string',
        'week_7' => 'string',
        'week_8' => 'string',
        'week_9' => 'string',
        'week_10' => 'string',
        'week_11' => 'string',
        'week_12' => 'string',
        'week_13' => 'string',
        'week_14' => 'string',
        'week_15' => 'string',
        'week_16' => 'string',
        'week_17' => 'string',
        'week_18' => 'string',
        'week_19' => 'string',
        'week_20' => 'string',
        'week_21' => 'string',
        'week_22' => 'string',
        'week_23' => 'string',
        'week_24' => 'string',
        'week_25' => 'string',
        'week_26' => 'string',
        'week_27' => 'string',
        'week_28' => 'string',
        'week_29' => 'string',
        'week_30' => 'string',
        'week_31' => 'string',
        'week_32' => 'string',
        'week_33' => 'string',
        'week_34' => 'string',
        'week_35' => 'string',
        'week_36' => 'string',
        'week_37' => 'string',
        'week_38' => 'string',
        'week_39' => 'string',
        'week_40' => 'string',
        'week_41' => 'string',
        'week_42' => 'string',
        'week_43' => 'string',
        'week_44' => 'string',
        'week_45' => 'string',
        'week_46' => 'string',
        'week_47' => 'string',
        'week_48' => 'string',
        'week_49' => 'string',
        'week_50' => 'string',
        'week_51' => 'string',
        'week_52' => 'string',
        'week_53' => 'string',
        'week_54' => 'string',
        'week_55' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    public function seasonPlan()
    {
        return $this->hasOne(SeasonPlan::class, 'id', 'season_plan_id');
    }

    public function seasonPlanDetail()
    {
        return $this->hasOne(SeasonPlanDetail::class, 'id', 'season_plan_detail_id');
    }
}
