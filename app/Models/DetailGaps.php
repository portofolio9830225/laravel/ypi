<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class DetailGaps
 * @package App\Models
 * @version March 23, 2022, 1:51 pm UTC
 *
 * @property integer $header_gap_id
 * @property integer $category
 * @property string $name
 * @property number $value
 */
class DetailGaps extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_gaps';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'header_gap_id',
        'category',
        'name',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'header_gap_id' => 'integer',
        'category' => 'integer',
        'name' => 'string',
        'value' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'header_gap_id' => 'required|integer',
        'category' => 'required|integer',
        'name' => 'required|string|max:30',
        'value' => 'required|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
