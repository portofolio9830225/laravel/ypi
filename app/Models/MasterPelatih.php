<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class MasterPelatih
 * @package App\Models
 * @version March 23, 2022, 5:57 am UTC
 *
 * @property string $nomor_ic
 * @property string $nama_lengkap
 * @property string $alamat
 * @property string $negara
 * @property integer $kodepos
 * @property string $tempat_lahir
 * @property string $tanggal_lahir
 * @property string $home_phone
 * @property string $hand_phone
 * @property string $business_phone
 * @property string $email
 * @property string $golongan_darah
 * @property string $keterangan
 */
class MasterPelatih extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'master_pelatihs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nomor_ic',
        'nama_lengkap',
        'alamat',
        'negara',
        'kodepos',
        'tempat_lahir',
        'tanggal_lahir',
        'home_phone',
        'hand_phone',
        'business_phone',
        'email',
        'golongan_darah',
        'keterangan'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nomor_ic' => 'string',
        'nama_lengkap' => 'string',
        'alamat' => 'string',
        'negara' => 'string',
        'kodepos' => 'integer',
        'tempat_lahir' => 'string',
        'tanggal_lahir' => 'date',
        'home_phone' => 'string',
        'hand_phone' => 'string',
        'business_phone' => 'string',
        'email' => 'string',
        'golongan_darah' => 'string',
        'keterangan' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nomor_ic' => 'required|string|max:50',
        'nama_lengkap' => 'required|string|max:100',
        'alamat' => 'required|string|max:100',
        'negara' => 'required|string|max:50',
        'kodepos' => 'required|integer',
        'tempat_lahir' => 'required|string|max:50',
        'tanggal_lahir' => 'required',
        'home_phone' => 'required|string|max:25',
        'hand_phone' => 'required|string|max:25',
        'business_phone' => 'required|string|max:25',
        'email' => 'required|string|max:255',
        'golongan_darah' => 'required|string|max:10',
        'keterangan' => 'required|string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
