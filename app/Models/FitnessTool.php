<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class FitnessTool
 * @package App\Models
 * @version March 23, 2022, 7:17 am UTC
 *
 * @property integer $pemain_id
 * @property integer $kelompok_id
 * @property boolean $gender
 * @property string $tanggal
 * @property number $beep_test
 * @property number $court_agility
 * @property number $reach
 * @property number $cj_actual
 * @property number $cj_elevation
 * @property number $sq_actual
 * @property number $sq_elevation
 * @property number $rl_actual
 * @property number $rl_elevation
 * @property number $ll_actual
 * @property number $ll_elevation
 * @property number $ratio
 * @property number $difference
 */
class FitnessTool extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'fitness_tools';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'pemain_id',
        'kelompok_id',
        'gender',
        'tanggal',
        'bench_press',
        'squat',
        'beep_test',
        'court_agility',
        'reach',
        'cj_actual',
        'cj_elevation',
        'sq_actual',
        'sq_elevation',
        'rl_actual',
        'rl_elevation',
        'll_actual',
        'll_elevation',
        'ratio',
        'difference'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'pemain_id' => 'integer',
        'kelompok_id' => 'integer',
        'gender' => 'string',
        'tanggal' => 'date',
        'beep_test' => 'float',
        'court_agility' => 'float',
        'bench_press' => 'float',
        'squat' => 'float',
        'reach' => 'float',
        'cj_actual' => 'float',
        'cj_elevation' => 'float',
        'sq_actual' => 'float',
        'sq_elevation' => 'float',
        'rl_actual' => 'float',
        'rl_elevation' => 'float',
        'll_actual' => 'float',
        'll_elevation' => 'float',
        'ratio' => 'float',
        'difference' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'pemain_id' => 'integer',
        'kelompok_id' => 'integer',
        'gender' => 'string',
        'tanggal' => '',
        'beep_test' => 'numeric',
        'court_agility' => 'numeric',
        'reach' => 'numeric',
        'bench_press' => 'numeric',
        'squat' => 'numeric',
        'cj_actual' => 'numeric',
        'cj_elevation' => 'numeric',
        'sq_actual' => 'numeric',
        'sq_elevation' => 'numeric',
        'rl_actual' => 'numeric',
        'rl_elevation' => 'numeric',
        'll_actual' => 'numeric',
        'll_elevation' => 'numeric',
        'ratio' => 'numeric',
        'difference' => 'numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function player()
    {
        return $this->belongsTo(MasterPemain::class, 'pemain_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(TabelKelompok::class, 'kelompok_id', 'id');
    }
}
