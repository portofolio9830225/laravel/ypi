<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class AnnualPlan
 * @package App\Models
 * @version May 10, 2022, 12:31 pm UTC
 *
 * @property integer $master_ypi_id
 * @property integer $kelompok
 * @property integer $annual_plan_setting_id
 * @property integer $week
 * @property string $plan_description
 */
class AnnualPlan extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'annual_plans';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_ypi_id',
        'kelompok',
        'annual_plan_setting_id',
        'week',
        'plan_description',
        'color',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'kelompok' => 'integer',
        'annual_plan_setting_id' => 'integer',
        'week' => 'integer',
        'color' => 'string',
        'plan_description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'integer',
        'kelompok' => 'integer',
        'annual_plan_setting_id' => 'integer',
        'week' => 'integer',
        'plan_description' => 'string|max:30',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
