<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class NationalHeightAvg
 * @package App\Models
 * @version March 23, 2022, 6:10 am UTC
 *
 * @property integer $nation_id
 * @property string $nation
 * @property integer $age
 * @property number $height
 * @property string $gender
 */
class NationalHeightAvg extends Model
{
    use BelongsToTenant;

    use HasFactory;

    public $table = 'national_height_avgs';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'nation_id',
        'nation',
        'age',
        'height',
        'gender'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nation_id' => 'integer',
        'nation' => 'string',
        'age' => 'integer',
        'height' => 'float',
        'gender' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nation_id' => 'required|integer',
        'nation' => 'required|string|max:50',
        'age' => 'required|integer',
        'height' => 'required|numeric',
        'gender' => 'required|string|max:10',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
    ];
}
