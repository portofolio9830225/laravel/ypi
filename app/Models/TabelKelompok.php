<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class TabelKelompok
 * @package App\Models
 * @version March 23, 2022, 5:55 am UTC
 *
 * @property string $kelompok
 */
class TabelKelompok extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'tabel_kelompoks';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kelompok',
        'age_min',
        'age_max',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kelompok' => 'string',
        'age_min' => 'integer',
        'age_max' => 'integer',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kelompok' => 'required|string|max:50',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
