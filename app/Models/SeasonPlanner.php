<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SeasonPlanner
 * @package App\Models
 * @version March 23, 2022, 1:49 pm UTC
 *
 * @property integer $master_ypi_id
 * @property string $background_color
 * @property string $name
 * @property string $description
 */
class SeasonPlanner extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'season_planners';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_ypi_id',
        'background_color',
        'name',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'background_color' => 'string',
        'name' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'required|integer',
        'background_color' => 'required|string|max:15',
        'name' => 'required|string|max:30',
        'description' => 'required|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    public function season_planner_details()
    {
        return $this->hasMany(SeasonPlannerDetail::class, 'season_planner_id');
    }
}
