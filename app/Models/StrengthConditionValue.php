<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class StrengthConditionValue
 * @package App\Models
 * @version March 23, 2022, 1:52 pm UTC
 *
 * @property integer $strength_condition_id
 * @property integer $strength_condition_player_id
 * @property string $name
 * @property number $reps
 * @property number $weight
 * @property number $predict
 * @property number $min
 * @property number $max
 * @property number $strength
 * @property number $last
 * @property number $diff
 */
class StrengthConditionValue extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'strength_condition_values';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'strength_condition_id',
        'strength_condition_player_id',
        'name',
        'reps',
        'weight',
        'predict',
        'min',
        'max',
        'strength',
        'last',
        'diff'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'strength_condition_id' => 'integer',
        'strength_condition_player_id' => 'integer',
        'name' => 'string',
        'reps' => 'float',
        'weight' => 'float',
        'predict' => 'float',
        'min' => 'float',
        'max' => 'float',
        'strength' => 'float',
        'last' => 'float',
        'diff' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'strength_condition_id' => 'integer',
        'strength_condition_player_id' => 'integer',
        'name' => 'string|max:20',
        'reps' => 'numeric',
        'weight' => 'numeric',
        'predict' => 'numeric',
        'min' => 'numeric',
        'max' => 'numeric',
        'strength' => 'numeric',
        'last' => 'numeric',
        'diff' => 'numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
