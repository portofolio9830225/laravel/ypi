<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class FitnessTest
 * @package App\Models
 * @version March 23, 2022, 7:18 am UTC
 *
 * @property string $tanggal_test
 * @property boolean $gender
 * @property integer $kelompok_id
 * @property integer $pemain_id
 * @property number $court_agility
 * @property number $vo2max
 */
class FitnessTest extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'fitness_tests';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'tanggal_test',
        'gender',
        'kelompok_id',
        'pemain_id',
        'court_agility',
        'vo2max',
        'bench_press',
        'squat'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'tanggal_test' => 'date',
        'gender' => 'string',
        'kelompok_id' => 'integer',
        'pemain_id' => 'integer',
        'court_agility' => 'float',
        'vo2max' => 'float',
        'bench_press' => 'float',
        'squat' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'tanggal_test' => '',
        'gender' => 'string',
        'kelompok_id' => 'integer',
        'pemain_id' => 'integer',
        'court_agility' => 'numeric',
        'vo2max' => 'numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    /**
     * Get the user that owns the FitnessTest
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function player()
    {
        return $this->belongsTo(MasterPemain::class, 'pemain_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(TabelKelompok::class, 'kelompok_id', 'id');
    }
}
