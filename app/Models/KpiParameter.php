<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class KpiParameter
 * @package App\Models
 * @version August 30, 2022, 1:46 pm UTC
 *
 * @property string $name
 * @property string $input_type
 */
class KpiParameter extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'kpi_parameters';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'input_type',
        'column_order',
        'calculate_statistic'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'input_type' => 'string',
        'column_order' => 'integer',
        'calculate_statistic' => 'boolean',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
