<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class KpiValue
 * @package App\Models
 * @version July 4, 2022, 2:13 am UTC
 *
 * @property integer $kpi_indicator_id
 * @property integer $kpi_subindicator_id
 * @property string $date
 * @property integer $pb
 * @property integer $target
 * @property integer $actual
 */
class KpiValue extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'kpi_values';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'kpi_indicator_id',
        'kpi_subindicator_id',
        'player_id',
        'group_id',
        'date',
        'pb',
        'target',
        'actual'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kpi_indicator_id' => 'integer',
        'kpi_subindicator_id' => 'integer',
        'player_id' => 'integer',
        'group_id' => 'integer',
        'date' => 'date',
        'pb' => 'integer',
        'target' => 'integer',
        'actual' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
