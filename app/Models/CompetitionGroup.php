<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class CompetitionGroup
 * @package App\Models
 * @version June 30, 2022, 6:31 am UTC
 *
 * @property integer $competition_id
 * @property integer $group_id
 */
class CompetitionGroup extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'competition_groups';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'competition_id',
        'groups_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'competition_id' => 'integer',
        'groups_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];
}
