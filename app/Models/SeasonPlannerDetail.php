<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SeasonPlannerDetail
 * @package App\Models
 * @version March 23, 2022, 1:50 pm UTC
 *
 * @property integer $master_ypi_id
 * @property integer $season_planner_id
 * @property string $name
 * @property string $description
 * @property string $type
 * @property boolean $inChart
 * @property string $chartType
 * @property string $chartColor
 * @property integer $chartNumber
 * @property string $chartName
 */
class SeasonPlannerDetail extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'season_planner_details';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];

    public $fillable = [
        'master_ypi_id',
        'season_planner_id',
        'name',
        'type',
        'inChart',
        'chartType',
        'chartColor',
        'chartNumber',
        'chartName',
        'desc_en',
        'desc_id',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'season_planner_id' => 'integer',
        'name' => 'string',
        'desc_en' => 'string',
        'desc_id' => 'string',
        'type' => 'string',
        'inChart' => 'boolean',
        'chartType' => 'string',
        'chartColor' => 'string',
        'chartNumber' => 'integer',
        'chartName' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    // public static $rules = [
    //     'master_ypi_id' => 'required|integer',
    //     'season_planner_id' => 'required|integer',
    //     'name' => 'required|string|max:40',
    //     'description' => 'required|string|max:100',
    //     'type' => 'required|string',
    //     'inChart' => 'required|boolean',
    //     'chartType' => 'required|string',
    //     'chartColor' => 'required|string|max:15',
    //     'chartNumber' => 'required|integer',
    //     'chartName' => 'required|string|max:50',
    //     'created_at' => 'nullable',
    //     'updated_at' => 'nullable',
    //     'deleted_at' => 'nullable'
    // ];


}
