<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class PubertyData
 * @package App\Models
 * @version March 23, 2022, 1:53 pm UTC
 *
 * @property integer $height_predicted_id
 * @property integer $pemain_id
 * @property string $dob
 * @property integer $age
 * @property number $height
 * @property number $national_average
 * @property number $growth_rate
 * @property integer $biological_category
 * @property integer $nation_id
 * @property integer $bulan_awal
 * @property integer $tahun_awal
 */
class PubertyData extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'puberty_datas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'height_predicted_id',
        'pemain_id',
        'dob',
        'age',
        'height',
        'national_average',
        'growth_rate',
        'biological_category',
        'nation_id',
        'bulan_awal',
        'tahun_awal'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'height_predicted_id' => 'integer',
        'pemain_id' => 'integer',
        'dob' => 'date',
        'age' => 'integer',
        'height' => 'float',
        'national_average' => 'float',
        'growth_rate' => 'float',
        'biological_category' => 'integer',
        'nation_id' => 'integer',
        'bulan_awal' => 'integer',
        'tahun_awal' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'height_predicted_id' => 'integer',
        'pemain_id' => 'integer',
        'dob' => '',
        'age' => 'integer',
        'height' => 'numeric',
        'national_average' => 'numeric',
        'growth_rate' => 'numeric',
        'biological_category' => 'integer',
        'nation_id' => 'integer',
        'bulan_awal' => 'integer',
        'tahun_awal' => 'integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
