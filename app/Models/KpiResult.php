<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class KpiResult
 * @package App\Models
 * @version August 30, 2022, 1:50 pm UTC
 *
 * @property string $date
 * @property integer $player_id
 * @property integer $kpi_parameter_id
 * @property string $value
 */
class KpiResult extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'kpi_results';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'date',
        'player_id',
        'kpi_parameter_id',
        'group_id',
        'value'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'date' => 'string',
        'player_id' => 'integer',
        'kpi_parameter_id' => 'integer',
        'value' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [];

    /**
     * Get the kpiParameter that owns the KpiResult
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kpiParameter()
    {
        return $this->belongsTo(KpiParameter::class, 'kpi_parameter_id', 'id');
    }

    public function player()
    {
        return $this->belongsTo(MasterPemain::class, 'player_id', 'id');
    }

    public function group()
    {
        return $this->belongsTo(TabelKelompok::class, 'group_id', 'id');
    }
}
