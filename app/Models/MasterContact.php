<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class MasterContact
 * @package App\Models
 * @version March 23, 2022, 5:56 am UTC
 *
 * @property string $nama_lengkap
 * @property string $alamat
 * @property string $negara
 * @property string $kodepos
 * @property string $telepon
 * @property string $email
 * @property string $keterangan
 * @property number $father_height
 * @property number $father_weight
 * @property number $mother_height
 * @property number $mother_weight
 */
class MasterContact extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'master_contacts';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nama_lengkap',
        'alamat',
        'negara',
        'kodepos',
        'telepon',
        'email',
        'keterangan',
        'father_height',
        'father_weight',
        'mother_height',
        'mother_weight'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nama_lengkap' => 'string',
        'alamat' => 'string',
        'negara' => 'string',
        'kodepos' => 'string',
        'telepon' => 'string',
        'email' => 'string',
        'keterangan' => 'string',
        'father_height' => 'float',
        'father_weight' => 'float',
        'mother_height' => 'float',
        'mother_weight' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nama_lengkap' => 'required|string|max:50',
        'alamat' => 'required|string|max:100',
        'negara' => 'required|string|max:50',
        'kodepos' => 'required|string|max:10',
        'telepon' => 'required|string|max:25',
        'email' => 'required|string|max:255',
        'keterangan' => 'required|string|max:250',
        'father_height' => 'required|numeric',
        'father_weight' => 'required|numeric',
        'mother_height' => 'required|numeric',
        'mother_weight' => 'required|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
