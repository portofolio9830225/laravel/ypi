<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class YtpCharts
 * @package App\Models
 * @version May 21, 2022, 3:29 am UTC
 *
 * @property string $name
 */
class YtpCharts extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'ytp_charts';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
