<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Stancl\Tenancy\Database\Models\Tenant as BaseTenant;
use Stancl\Tenancy\Contracts\TenantWithDatabase;

class Club extends BaseTenant
{
    use HasFactory;
    protected $table = 'club';
    protected $guarded = ['id'];
    protected $hidden = ['pivot'];

    public function user()
    {
        return $this->belongsToMany(User::class, 'club_member');
    }

    public static function getCustomColumns(): array
    {
        return [
            'id',
            'nama',
            'alamat',
            'image',
            'subscription_type',
        ];
    }
}
