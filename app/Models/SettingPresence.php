<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class SettingPresence
 * @package App\Models
 * @version March 23, 2022, 1:49 pm UTC
 *
 * @property integer $kelompok_id
 * @property integer $pelatih_id
 * @property string $keterangan
 * @property string $inisial
 * @property boolean $presence
 */
class SettingPresence extends Model
{
    use BelongsToTenant;

    use HasFactory;

    public $table = 'setting_presences';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';



    public $fillable = [
        'kelompok_id',
        'pelatih_id',
        'keterangan',
        'inisial',
        'presence'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'kelompok_id' => 'integer',
        'pelatih_id' => 'integer',
        'keterangan' => 'string',
        'inisial' => 'string',
        'presence' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'kelompok_id' => 'required|integer',
        'pelatih_id' => 'required|integer',
        'keterangan' => 'required|string|max:50',
        'inisial' => 'required|string|max:2',
        'presence' => 'required|boolean',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
    ];
}
