<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class DetailWeeklyPlan
 * @package App\Models
 * @version March 23, 2022, 1:50 pm UTC
 *
 * @property integer $master_ypi_id
 * @property integer $minggu
 * @property string $tanggal
 * @property integer $session
 * @property time $jam
 * @property string $details
 * @property integer $set
 * @property integer $rep
 * @property string $pace
 * @property integer $work_time
 * @property integer $rest_interval
 * @property integer $rest_period
 * @property string $emphesis_goal
 * @property integer $duration
 * @property integer $intensity
 * @property integer $tr_load
 */
class DetailWeeklyPlan extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_weekly_plans';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_ypi_id',
        'minggu',
        'tanggal',
        'session',
        'jam',
        'details',
        'set',
        'rep',
        'pace',
        'work_time',
        'rest_interval',
        'rest_period',
        'emphesis_goal',
        'duration',
        'intensity',
        'tr_load'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'minggu' => 'integer',
        'tanggal' => 'date',
        'session' => 'integer',
        'details' => 'string',
        'set' => 'integer',
        'rep' => 'integer',
        'pace' => 'string',
        'work_time' => 'integer',
        'rest_interval' => 'integer',
        'rest_period' => 'integer',
        'emphesis_goal' => 'string',
        'duration' => 'integer',
        'intensity' => 'integer',
        'tr_load' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'integer',
        'minggu' => 'integer',
        'tanggal' => '',
        'session' => 'integer',
        'jam' => '',
        'details' => 'string|max:50',
        'set' => 'integer',
        'rep' => 'integer',
        'pace' => 'string|max:10',
        'work_time' => 'integer',
        'rest_interval' => 'integer',
        'rest_period' => 'integer',
        'emphesis_goal' => 'string|max:50',
        'duration' => 'integer',
        'intensity' => 'integer',
        'tr_load' => 'integer',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
