<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class KpiIndicator
 * @package App\Models
 * @version July 4, 2022, 2:10 am UTC
 *
 * @property string $name
 */
class KpiIndicator extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'kpi_indicators';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
