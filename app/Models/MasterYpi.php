<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class MasterYpi
 * @package App\Models
 * @version March 23, 2022, 6:04 am UTC
 *
 * @property string $keterangan
 * @property integer $kode_pelatih
 * @property number $voltarget
 * @property string|\Carbon\Carbon $tanggal_mulai
 * @property string|\Carbon\Carbon $tanggal_selesai
 * @property number $default_intensity
 */
class MasterYpi extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'master_ypis';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'keterangan',
        'kode_pelatih',
        'voltarget',
        'tanggal_mulai',
        'tanggal_selesai',
        'default_intensity'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'keterangan' => 'string',
        'kode_pelatih' => 'integer',
        'voltarget' => 'float',
        'tanggal_mulai' => 'datetime',
        'tanggal_selesai' => 'datetime',
        'default_intensity' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'keterangan' => 'required|string|max:100',
        'kode_pelatih' => 'required|integer',
        'voltarget' => 'required|numeric',
        'tanggal_mulai' => 'required',
        'tanggal_selesai' => 'required',
        'default_intensity' => 'required|numeric'
    ];


    public function coach()
    {
        return $this->belongsTo(MasterPelatih::class, 'kode_pelatih');
    }

    public function default_target()
    {
        return $this->hasOne(DefaultTarget::class, 'master_ypi_id');
    }

    public function ypi_goals()
    {
        return $this->hasMany(YpiGoal::class, 'master_ypi_id');
    }

    public function season_planners()
    {
        return $this->hasMany(SeasonPlanner::class, 'master_ypi_id');
    }

    public function ypi_events()
    {
        return $this->hasMany(YpiEvent::class, 'master_ypi_id');
    }
}
