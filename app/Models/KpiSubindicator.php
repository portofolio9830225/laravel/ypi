<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class KpiSubindicator
 * @package App\Models
 * @version July 2, 2022, 8:36 am UTC
 *
 * @property integer $kpi_indicator_id
 * @property string $name
 * @property string $indicator_type
 * @property string $color
 */
class KpiSubindicator extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'kpi_subindicators';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'kpi_indicator_id',
        'name',
        'indicator_type',
        'color'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'kpi_indicator_id' => 'integer',
        'name' => 'string',
        'indicator_type' => 'string',
        'color' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        
    ];

    
}
