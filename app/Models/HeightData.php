<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class HeightData
 * @package App\Models
 * @version March 23, 2022, 1:52 pm UTC
 *
 * @property integer $height_predicted_id
 * @property integer $pemain_id
 * @property string $dob
 * @property integer $age
 * @property integer $bulan_awal
 * @property integer $tahun_awal
 * @property integer $bulan_akhir
 * @property integer $tahun_akhir
 * @property number $bulan_1
 * @property number $bulan_2
 * @property number $bulan_3
 * @property number $bulan_4
 * @property number $bulan_5
 * @property number $bulan_6
 * @property number $bulan_7
 * @property number $bulan_8
 * @property number $bulan_9
 * @property number $bulan_10
 * @property number $bulan_11
 * @property number $bulan_12
 * @property number $growth_rate
 */
class HeightData extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'height_datas';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'height_predicted_id',
        'pemain_id',
        'dob',
        'age',
        'bulan_awal',
        'tahun_awal',
        'bulan_akhir',
        'tahun_akhir',
        'bulan_1',
        'bulan_2',
        'bulan_3',
        'bulan_4',
        'bulan_5',
        'bulan_6',
        'bulan_7',
        'bulan_8',
        'bulan_9',
        'bulan_10',
        'bulan_11',
        'bulan_12',
        'growth_rate'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'height_predicted_id' => 'integer',
        'pemain_id' => 'integer',
        'dob' => 'date',
        'age' => 'integer',
        'bulan_awal' => 'integer',
        'tahun_awal' => 'integer',
        'bulan_akhir' => 'integer',
        'tahun_akhir' => 'integer',
        'bulan_1' => 'float',
        'bulan_2' => 'float',
        'bulan_3' => 'float',
        'bulan_4' => 'float',
        'bulan_5' => 'float',
        'bulan_6' => 'float',
        'bulan_7' => 'float',
        'bulan_8' => 'float',
        'bulan_9' => 'float',
        'bulan_10' => 'float',
        'bulan_11' => 'float',
        'bulan_12' => 'float',
        'growth_rate' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'height_predicted_id' => 'integer',
        'pemain_id' => 'integer',
        'dob' => '',
        'age' => 'integer',
        'bulan_awal' => 'integer',
        'tahun_awal' => 'integer',
        'bulan_akhir' => 'integer',
        'tahun_akhir' => 'integer',
        'bulan_1' => 'numeric',
        'bulan_2' => 'numeric',
        'bulan_3' => 'numeric',
        'bulan_4' => 'numeric',
        'bulan_5' => 'numeric',
        'bulan_6' => 'numeric',
        'bulan_7' => 'numeric',
        'bulan_8' => 'numeric',
        'bulan_9' => 'numeric',
        'bulan_10' => 'numeric',
        'bulan_11' => 'numeric',
        'bulan_12' => 'numeric',
        'growth_rate' => 'numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
