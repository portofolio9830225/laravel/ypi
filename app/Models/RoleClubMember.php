<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RoleClubMember extends Model
{
    use HasFactory;
    protected $table = 'role_club_member';
    protected $guarded = ['id'];
    protected $hidden = ['pivot'];
}
