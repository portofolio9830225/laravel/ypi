<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

/**
 * Class SeasonPlannerDetailValue
 * @package App\Models
 * @version March 23, 2022, 1:50 pm UTC
 *
 * @property integer $master_ypi_id
 * @property integer $season_planner_id
 * @property integer $season_planner_detail_id
 * @property integer $week_index
 * @property string $value
 * @property string $text_color
 * @property string $description
 */
class SeasonPlannerDetailValue extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'season_planner_detail_values';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_ypi_id',
        'season_planner_id',
        'season_planner_detail_id',
        'week_index',
        'value',
        'text_color',
        'description'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'season_planner_id' => 'integer',
        'season_planner_detail_id' => 'integer',
        'week_index' => 'integer',
        'value' => 'string',
        'text_color' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'required|integer',
        'season_planner_id' => 'required|integer',
        'season_planner_detail_id' => 'required|integer',
        'week_index' => 'required|integer',
        'value' => 'required|string|max:100',
        'text_color' => 'required|string|max:15',
        'description' => 'required|string|max:100',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];

    
}
