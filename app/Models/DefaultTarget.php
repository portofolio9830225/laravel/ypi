<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class DefaultTarget
 * @package App\Models
 * @version March 23, 2022, 1:46 pm UTC
 *
 * @property integer $master_ypi_id
 * @property number $mon1
 * @property number $mon2
 * @property number $mon3
 * @property number $mon4
 * @property number $tue1
 * @property number $tue2
 * @property number $tue3
 * @property number $tue4
 * @property number $wed1
 * @property number $wed2
 * @property number $wed3
 * @property number $wed4
 * @property number $thu1
 * @property number $thu2
 * @property number $thu3
 * @property number $thu4
 * @property number $fri1
 * @property number $fri2
 * @property number $fri3
 * @property number $fri4
 * @property number $sat1
 * @property number $sat2
 * @property number $sat3
 * @property number $sat4
 * @property number $sun1
 * @property number $sun2
 * @property number $sun3
 * @property number $sun4
 */
class DefaultTarget extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'default_targets';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_ypi_id',
        'mon1',
        'mon2',
        'mon3',
        'mon4',
        'tue1',
        'tue2',
        'tue3',
        'tue4',
        'wed1',
        'wed2',
        'wed3',
        'wed4',
        'thu1',
        'thu2',
        'thu3',
        'thu4',
        'fri1',
        'fri2',
        'fri3',
        'fri4',
        'sat1',
        'sat2',
        'sat3',
        'sat4',
        'sun1',
        'sun2',
        'sun3',
        'sun4'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'mon1' => 'float',
        'mon2' => 'float',
        'mon3' => 'float',
        'mon4' => 'float',
        'tue1' => 'float',
        'tue2' => 'float',
        'tue3' => 'float',
        'tue4' => 'float',
        'wed1' => 'float',
        'wed2' => 'float',
        'wed3' => 'float',
        'wed4' => 'float',
        'thu1' => 'float',
        'thu2' => 'float',
        'thu3' => 'float',
        'thu4' => 'float',
        'fri1' => 'float',
        'fri2' => 'float',
        'fri3' => 'float',
        'fri4' => 'float',
        'sat1' => 'float',
        'sat2' => 'float',
        'sat3' => 'float',
        'sat4' => 'float',
        'sun1' => 'float',
        'sun2' => 'float',
        'sun3' => 'float',
        'sun4' => 'float'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'required|integer',
        'mon1' => 'required|numeric',
        'mon2' => 'required|numeric',
        'mon3' => 'required|numeric',
        'mon4' => 'required|numeric',
        'tue1' => 'required|numeric',
        'tue2' => 'required|numeric',
        'tue3' => 'required|numeric',
        'tue4' => 'required|numeric',
        'wed1' => 'required|numeric',
        'wed2' => 'required|numeric',
        'wed3' => 'required|numeric',
        'wed4' => 'required|numeric',
        'thu1' => 'required|numeric',
        'thu2' => 'required|numeric',
        'thu3' => 'required|numeric',
        'thu4' => 'required|numeric',
        'fri1' => 'required|numeric',
        'fri2' => 'required|numeric',
        'fri3' => 'required|numeric',
        'fri4' => 'required|numeric',
        'sat1' => 'required|numeric',
        'sat2' => 'required|numeric',
        'sat3' => 'required|numeric',
        'sat4' => 'required|numeric',
        'sun1' => 'required|numeric',
        'sun2' => 'required|numeric',
        'sun3' => 'required|numeric',
        'sun4' => 'required|numeric',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
