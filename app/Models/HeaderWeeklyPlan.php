<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stancl\Tenancy\Database\Concerns\BelongsToTenant;

/**
 * Class HeaderWeeklyPlan
 * @package App\Models
 * @version March 23, 2022, 6:26 am UTC
 *
 * @property integer $master_ypi_id
 * @property integer $minggu
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property integer $actual_tr_vol
 * @property integer $actual_tr_vol_prosen
 * @property integer $actual_tr_intensity
 * @property integer $actual_tr_load
 * @property integer $target_tr_vol
 * @property integer $target_tr_vol_prosen
 * @property integer $target_tr_intensity
 * @property integer $target_tr_load
 * @property string $weekly_training_goal
 * @property string $notes
 */
class HeaderWeeklyPlan extends Model
{
    use BelongsToTenant;

    use SoftDeletes;

    use HasFactory;

    public $table = 'header_weekly_plans';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'master_ypi_id',
        'minggu',
        'tanggal_awal',
        'tanggal_akhir',
        'actual_tr_vol',
        'actual_tr_vol_prosen',
        'actual_tr_intensity',
        'actual_tr_load',
        'target_tr_vol',
        'target_tr_vol_prosen',
        'target_tr_intensity',
        'target_tr_load',
        'weekly_training_goal',
        'notes'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'master_ypi_id' => 'integer',
        'minggu' => 'integer',
        'tanggal_awal' => 'date',
        'tanggal_akhir' => 'date',
        'actual_tr_vol' => 'integer',
        'actual_tr_vol_prosen' => 'integer',
        'actual_tr_intensity' => 'integer',
        'actual_tr_load' => 'integer',
        'target_tr_vol' => 'integer',
        'target_tr_vol_prosen' => 'integer',
        'target_tr_intensity' => 'integer',
        'target_tr_load' => 'integer',
        'weekly_training_goal' => 'string',
        'notes' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'master_ypi_id' => 'integer',
        'minggu' => 'integer',
        'tanggal_awal' => '',
        'tanggal_akhir' => '',
        'actual_tr_vol' => 'integer',
        'actual_tr_vol_prosen' => 'integer',
        'actual_tr_intensity' => 'integer',
        'actual_tr_load' => 'integer',
        'target_tr_vol' => 'integer',
        'target_tr_vol_prosen' => 'integer',
        'target_tr_intensity' => 'integer',
        'target_tr_load' => 'integer',
        'weekly_training_goal' => 'string|max:100',
        'notes' => 'string',
        'created_at' => 'nullable',
        'updated_at' => 'nullable',
        'deleted_at' => 'nullable'
    ];
}
