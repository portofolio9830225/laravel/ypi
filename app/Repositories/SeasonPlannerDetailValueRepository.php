<?php

namespace App\Repositories;

use App\Models\SeasonPlannerDetailValue;
use App\Repositories\BaseRepository;

/**
 * Class SeasonPlannerDetailValueRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:50 pm UTC
*/

class SeasonPlannerDetailValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'season_planner_id',
        'season_planner_detail_id',
        'week_index',
        'value',
        'text_color',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeasonPlannerDetailValue::class;
    }
}
