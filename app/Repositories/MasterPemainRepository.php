<?php

namespace App\Repositories;

use App\Models\MasterPemain;
use App\Repositories\BaseRepository;

/**
 * Class MasterPemainRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:58 am UTC
*/

class MasterPemainRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nomor_ic',
        'nama_lengkap',
        'alamat',
        'negara',
        'kodepos',
        'tempat_lahir',
        'tanggal_lahir',
        'home_phone',
        'hand_phone',
        'business_phone',
        'nama_sekolah',
        'kelas',
        'wali_kelas',
        'no_ruang',
        'keterangan',
        'golongan_darah',
        'email',
        'contact_id',
        'aktif',
        'sex',
        'kelompok_id',
        'tunggal_putra',
        'tunggal_putri',
        'ganda_putra',
        'ganda_putri',
        'ganda_campuran'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterPemain::class;
    }
}
