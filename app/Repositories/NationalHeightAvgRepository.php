<?php

namespace App\Repositories;

use App\Models\NationalHeightAvg;
use App\Repositories\BaseRepository;

/**
 * Class NationalHeightAvgRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:10 am UTC
*/

class NationalHeightAvgRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nation_id',
        'nation',
        'age',
        'height',
        'gender'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return NationalHeightAvg::class;
    }
}
