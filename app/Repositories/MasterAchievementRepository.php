<?php

namespace App\Repositories;

use App\Models\MasterAchievement;
use App\Repositories\BaseRepository;

/**
 * Class MasterAchievementRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:01 am UTC
*/

class MasterAchievementRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_event',
        'kategori_id',
        'tanggal_mulai',
        'tanggal_akhir',
        'competition_status_id',
        'color'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterAchievement::class;
    }
}
