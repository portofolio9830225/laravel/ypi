<?php

namespace App\Repositories;

use App\Models\YpiEvent;
use App\Repositories\BaseRepository;

/**
 * Class YpiEventRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:48 pm UTC
*/

class YpiEventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'master_achievement_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return YpiEvent::class;
    }
}
