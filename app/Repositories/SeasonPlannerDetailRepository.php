<?php

namespace App\Repositories;

use App\Models\SeasonPlannerDetail;
use App\Repositories\BaseRepository;

/**
 * Class SeasonPlannerDetailRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:50 pm UTC
*/

class SeasonPlannerDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'season_planner_id',
        'name',
        'description',
        'type',
        'inChart',
        'chartType',
        'chartColor',
        'chartNumber',
        'chartName'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeasonPlannerDetail::class;
    }
}
