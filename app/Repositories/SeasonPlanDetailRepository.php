<?php

namespace App\Repositories;

use App\Models\SeasonPlanDetail;
use App\Repositories\BaseRepository;

/**
 * Class SeasonPlanDetailRepository
 * @package App\Repositories
 * @version August 1, 2022, 6:53 am UTC
*/

class SeasonPlanDetailRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'season_plan_id',
        'name',
        'input_type',
        'desc_en',
        'desc_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeasonPlanDetail::class;
    }
}
