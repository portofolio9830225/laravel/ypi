<?php

namespace App\Repositories;

use App\Models\ResultCategory;
use App\Repositories\BaseRepository;

/**
 * Class ResultCategoryRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:10 am UTC
*/

class ResultCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'result'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ResultCategory::class;
    }
}
