<?php

namespace App\Repositories;

use App\Models\ExerciseCategory;
use App\Repositories\BaseRepository;

/**
 * Class ExerciseCategoryRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:06 am UTC
*/

class ExerciseCategoryRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'category'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return ExerciseCategory::class;
    }
}
