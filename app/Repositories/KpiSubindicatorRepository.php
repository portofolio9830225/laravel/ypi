<?php

namespace App\Repositories;

use App\Models\KpiSubindicator;
use App\Repositories\BaseRepository;

/**
 * Class KpiSubindicatorRepository
 * @package App\Repositories
 * @version July 2, 2022, 8:36 am UTC
*/

class KpiSubindicatorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kpi_indicator_id',
        'name',
        'indicator_type',
        'color'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return KpiSubindicator::class;
    }
}
