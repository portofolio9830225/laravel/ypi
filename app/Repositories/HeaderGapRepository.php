<?php

namespace App\Repositories;

use App\Models\HeaderGap;
use App\Repositories\BaseRepository;

/**
 * Class HeaderGapRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:29 am UTC
*/

class HeaderGapRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'tanggal',
        'event_name',
        'present',
        'future',
        'player_gap',
        'gap_value1',
        'gap_value2',
        'gap_value3',
        'gap_value4',
        'gap_value5',
        'competition'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HeaderGap::class;
    }
}
