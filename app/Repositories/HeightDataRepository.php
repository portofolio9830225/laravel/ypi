<?php

namespace App\Repositories;

use App\Models\HeightData;
use App\Repositories\BaseRepository;

/**
 * Class HeightDataRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:52 pm UTC
*/

class HeightDataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'height_predicted_id',
        'pemain_id',
        'dob',
        'age',
        'bulan_awal',
        'tahun_awal',
        'bulan_akhir',
        'tahun_akhir',
        'bulan_1',
        'bulan_2',
        'bulan_3',
        'bulan_4',
        'bulan_5',
        'bulan_6',
        'bulan_7',
        'bulan_8',
        'bulan_9',
        'bulan_10',
        'bulan_11',
        'bulan_12',
        'growth_rate'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HeightData::class;
    }
}
