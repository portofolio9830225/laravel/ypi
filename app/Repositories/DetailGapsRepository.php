<?php

namespace App\Repositories;

use App\Models\DetailGaps;
use App\Repositories\BaseRepository;

/**
 * Class DetailGapsRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:51 pm UTC
*/

class DetailGapsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'header_gap_id',
        'category',
        'name',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailGaps::class;
    }
}
