<?php

namespace App\Repositories;

use App\Models\StrengthConditioningData;
use App\Repositories\BaseRepository;

/**
 * Class StrengthConditioningDataRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:14 am UTC
*/

class StrengthConditioningDataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'gym_type',
        'master_ypi_id',
        'week',
        'hari',
        'tanggal',
        'comments',
        'rep_1',
        'weight_1',
        'rep_2',
        'weight_2',
        'rep_3',
        'weight_3',
        'rep_4',
        'weight_4',
        'rep_5',
        'weight_5',
        'rep_6',
        'weight_6',
        'tut_rep',
        'tut_exercise',
        'tonnage',
        'average',
        'physical_load',
        'predicted_1rm',
        'average_1rm_predict',
        'actual_1rm',
        'average_1rm',
        'weekly_tut',
        'weekly_tonnage',
        'weekly_average',
        'weekly_load'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StrengthConditioningData::class;
    }
}
