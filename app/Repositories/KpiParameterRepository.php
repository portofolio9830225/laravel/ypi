<?php

namespace App\Repositories;

use App\Models\KpiParameter;
use App\Repositories\BaseRepository;

/**
 * Class KpiParameterRepository
 * @package App\Repositories
 * @version August 30, 2022, 1:46 pm UTC
*/

class KpiParameterRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'input_type'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return KpiParameter::class;
    }
}
