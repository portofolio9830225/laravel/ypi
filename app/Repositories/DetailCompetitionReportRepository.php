<?php

namespace App\Repositories;

use App\Models\DetailCompetitionReport;
use App\Repositories\BaseRepository;

/**
 * Class DetailCompetitionReportRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:52 pm UTC
*/

class DetailCompetitionReportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'competition_report_id',
        'round',
        'opponent',
        'nationality',
        'set1a',
        'set1b',
        'set2a',
        'set2b',
        'set3a',
        'set3b',
        'comment',
        'result'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailCompetitionReport::class;
    }
}
