<?php

namespace App\Repositories;

use App\Models\HeightPredicted;
use App\Repositories\BaseRepository;

/**
 * Class HeightPredictedRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:15 am UTC
*/

class HeightPredictedRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'age',
        'date_predicted',
        'height',
        'weight',
        'fathers_height',
        'mothers_height',
        'predicted_height',
        'maturity_status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HeightPredicted::class;
    }
}
