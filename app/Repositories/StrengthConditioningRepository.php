<?php

namespace App\Repositories;

use App\Models\StrengthConditioning;
use App\Repositories\BaseRepository;

/**
 * Class StrengthConditioningRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:13 am UTC
*/

class StrengthConditioningRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date_test',
        'gender',
        'age_group_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StrengthConditioning::class;
    }
}
