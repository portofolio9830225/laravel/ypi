<?php

namespace App\Repositories;

use App\Models\SettingAttendance;
use App\Repositories\BaseRepository;

/**
 * Class SettingAttendanceRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:23 am UTC
*/

class SettingAttendanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'bulan',
        'tahun',
        'kelompok',
        'kode_pelatih',
        'tanggal',
        'session',
        'am_pm'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SettingAttendance::class;
    }
}
