<?php

namespace App\Repositories;

use App\Models\Constant;
use App\Repositories\BaseRepository;

/**
 * Class ConstantRepository
 * @package App\Repositories
 * @version August 10, 2022, 3:04 am UTC
*/

class ConstantRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'key',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Constant::class;
    }
}
