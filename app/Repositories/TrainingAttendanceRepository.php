<?php

namespace App\Repositories;

use App\Models\TrainingAttendance;
use App\Repositories\BaseRepository;

/**
 * Class TrainingAttendanceRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:11 am UTC
*/

class TrainingAttendanceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kelompok_id',
        'master_pelatih_id',
        'pemain_id',
        'tanggal',
        'session',
        'presence_id',
        'note'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TrainingAttendance::class;
    }
}
