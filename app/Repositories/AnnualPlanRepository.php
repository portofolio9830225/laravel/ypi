<?php

namespace App\Repositories;

use App\Models\AnnualPlan;
use App\Repositories\BaseRepository;

/**
 * Class AnnualPlanRepository
 * @package App\Repositories
 * @version May 10, 2022, 12:31 pm UTC
*/

class AnnualPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'kelompok',
        'no_urut',
        'week',
        'plan_description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AnnualPlan::class;
    }
}
