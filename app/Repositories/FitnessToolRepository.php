<?php

namespace App\Repositories;

use App\Models\FitnessTool;
use App\Repositories\BaseRepository;

/**
 * Class FitnessToolRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:17 am UTC
*/

class FitnessToolRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'kelompok_id',
        'gender',
        'tanggal',
        'beep_test',
        'court_agility',
        'reach',
        'cj_actual',
        'cj_elevation',
        'sq_actual',
        'sq_elevation',
        'rl_actual',
        'rl_elevation',
        'll_actual',
        'll_elevation',
        'ratio',
        'difference'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FitnessTool::class;
    }
}
