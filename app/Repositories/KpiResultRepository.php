<?php

namespace App\Repositories;

use App\Models\KpiResult;
use App\Repositories\BaseRepository;

/**
 * Class KpiResultRepository
 * @package App\Repositories
 * @version August 30, 2022, 1:50 pm UTC
 */

class KpiResultRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'player_id',
        'kpi_parameter_id',
        'group_id',
        'value'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return KpiResult::class;
    }
}
