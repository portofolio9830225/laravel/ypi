<?php

namespace App\Repositories;

use App\Models\StrengthConditionPlayer;
use App\Repositories\BaseRepository;

/**
 * Class StrengthConditionPlayerRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:52 pm UTC
*/

class StrengthConditionPlayerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'strength_conditioning_id',
        'pemain_id',
        'body_weight'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StrengthConditionPlayer::class;
    }
}
