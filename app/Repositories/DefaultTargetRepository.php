<?php

namespace App\Repositories;

use App\Models\DefaultTarget;
use App\Repositories\BaseRepository;

/**
 * Class DefaultTargetRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:46 pm UTC
*/

class DefaultTargetRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'mon1',
        'mon2',
        'mon3',
        'mon4',
        'tue1',
        'tue2',
        'tue3',
        'tue4',
        'wed1',
        'wed2',
        'wed3',
        'wed4',
        'thu1',
        'thu2',
        'thu3',
        'thu4',
        'fri1',
        'fri2',
        'fri3',
        'fri4',
        'sat1',
        'sat2',
        'sat3',
        'sat4',
        'sun1',
        'sun2',
        'sun3',
        'sun4'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DefaultTarget::class;
    }
}
