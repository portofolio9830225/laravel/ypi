<?php

namespace App\Repositories;

use App\Models\MasterPelatih;
use App\Repositories\BaseRepository;

/**
 * Class MasterPelatihRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:57 am UTC
*/

class MasterPelatihRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nomor_ic',
        'nama_lengkap',
        'alamat',
        'negara',
        'kodepos',
        'tempat_lahir',
        'tanggal_lahir',
        'home_phone',
        'hand_phone',
        'business_phone',
        'email',
        'golongan_darah',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterPelatih::class;
    }
}
