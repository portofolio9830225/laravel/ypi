<?php

namespace App\Repositories;

use App\Models\YpiGoal;
use App\Repositories\BaseRepository;

/**
 * Class YpiGoalRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:47 pm UTC
*/

class YpiGoalRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'goal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return YpiGoal::class;
    }
}
