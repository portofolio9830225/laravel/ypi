<?php

namespace App\Repositories;

use App\Models\KpiIndicator;
use App\Repositories\BaseRepository;

/**
 * Class KpiIndicatorRepository
 * @package App\Repositories
 * @version July 4, 2022, 2:10 am UTC
*/

class KpiIndicatorRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return KpiIndicator::class;
    }
}
