<?php

namespace App\Repositories;

use App\Models\SeasonPlanner;
use App\Repositories\BaseRepository;

/**
 * Class SeasonPlannerRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:49 pm UTC
*/

class SeasonPlannerRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'background_color',
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeasonPlanner::class;
    }
}
