<?php

namespace App\Repositories;

use App\Models\SeasonPlanValue;
use App\Repositories\BaseRepository;

/**
 * Class SeasonPlanValueRepository
 * @package App\Repositories
 * @version August 1, 2022, 7:04 am UTC
*/

class SeasonPlanValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'ypi_id',
        'season_plan_id',
        'season_plan_detail_id',
        'bg_color',
        'active',
        'week_1',
        'week_2',
        'week_3',
        'week_4',
        'week_5',
        'week_6',
        'week_7',
        'week_8',
        'week_9',
        'week_10',
        'week_11',
        'week_12',
        'week_13',
        'week_14',
        'week_15',
        'week_16',
        'week_17',
        'week_18',
        'week_19',
        'week_20',
        'week_21',
        'week_22',
        'week_23',
        'week_24',
        'week_25',
        'week_26',
        'week_27',
        'week_28',
        'week_29',
        'week_30',
        'week_31',
        'week_32',
        'week_33',
        'week_34',
        'week_35',
        'week_36',
        'week_37',
        'week_38',
        'week_39',
        'week_40',
        'week_41',
        'week_42',
        'week_43',
        'week_44',
        'week_45',
        'week_46',
        'week_47',
        'week_48',
        'week_49',
        'week_50',
        'week_51',
        'week_52',
        'week_53',
        'week_54',
        'week_55'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeasonPlanValue::class;
    }
}
