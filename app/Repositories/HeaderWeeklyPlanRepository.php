<?php

namespace App\Repositories;

use App\Models\HeaderWeeklyPlan;
use App\Repositories\BaseRepository;

/**
 * Class HeaderWeeklyPlanRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:26 am UTC
*/

class HeaderWeeklyPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'minggu',
        'tanggal_awal',
        'tanggal_akhir',
        'actual_tr_vol',
        'actual_tr_vol_prosen',
        'actual_tr_intensity',
        'actual_tr_load',
        'target_tr_vol',
        'target_tr_vol_prosen',
        'target_tr_intensity',
        'target_tr_load',
        'weekly_training_goal',
        'notes'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HeaderWeeklyPlan::class;
    }
}
