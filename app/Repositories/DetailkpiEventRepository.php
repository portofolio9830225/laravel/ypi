<?php

namespace App\Repositories;

use App\Models\DetailkpiEvent;
use App\Repositories\BaseRepository;

/**
 * Class DetailkpiEventRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:07 am UTC
*/

class DetailkpiEventRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'tanggal_kpi',
        'keterangan_1',
        'keterangan_2',
        'keterangan_3',
        'keterangan_4',
        'event_name',
        'target_1',
        'target_2',
        'target_3',
        'target_4',
        'actual_1',
        'actual_2',
        'actual_3',
        'actual_4'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailkpiEvent::class;
    }
}
