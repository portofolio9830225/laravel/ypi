<?php

namespace App\Repositories;

use App\Models\SeasonPlan;
use App\Repositories\BaseRepository;

/**
 * Class SeasonPlanRepository
 * @package App\Repositories
 * @version August 1, 2022, 6:50 am UTC
*/

class SeasonPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SeasonPlan::class;
    }
}
