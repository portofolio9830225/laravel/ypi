<?php

namespace App\Repositories;

use App\Models\HeaderKpi;
use App\Repositories\BaseRepository;

/**
 * Class HeaderKpiRepository
 * @package App\Repositories
 * @version June 25, 2022, 9:34 am UTC
*/

class HeaderKpiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'tanggal_kpi'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return HeaderKpi::class;
    }
}
