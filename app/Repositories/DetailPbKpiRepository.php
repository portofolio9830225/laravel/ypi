<?php

namespace App\Repositories;

use App\Models\DetailPbKpi;
use App\Repositories\BaseRepository;

/**
 * Class DetailPbKpiRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:51 pm UTC
*/

class DetailPbKpiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kpi_physic_id',
        'tanggal',
        'pb',
        'target',
        'actual'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailPbKpi::class;
    }
}
