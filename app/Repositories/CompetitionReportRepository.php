<?php

namespace App\Repositories;

use App\Models\CompetitionReport;
use App\Repositories\BaseRepository;

/**
 * Class CompetitionReportRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:11 am UTC
*/

class CompetitionReportRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'master_achievement_id',
        'venue',
        'specialist',
        'event_level',
        'result_id',
        'summary_notes',
        'suggestion_notes',
        'kelompok_id',
        'partner_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CompetitionReport::class;
    }
}
