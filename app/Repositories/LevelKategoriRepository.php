<?php

namespace App\Repositories;

use App\Models\LevelKategori;
use App\Repositories\BaseRepository;

/**
 * Class LevelKategoriRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:51 am UTC
*/

class LevelKategoriRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'level'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return LevelKategori::class;
    }
}
