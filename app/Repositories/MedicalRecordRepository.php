<?php

namespace App\Repositories;

use App\Models\MedicalRecord;
use App\Repositories\BaseRepository;

/**
 * Class MedicalRecordRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:07 am UTC
*/

class MedicalRecordRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'master_ypi_id',
        'week',
        'tanggal',
        'hari',
        'injured',
        'primary_injured',
        'detail_injured',
        'treatment_injured',
        'injured_days',
        'treatment_injured_days',
        'illied',
        'primary_illied',
        'symptomps',
        'treatment_illied',
        'illied_days',
        'treatment_illied_days'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MedicalRecord::class;
    }
}
