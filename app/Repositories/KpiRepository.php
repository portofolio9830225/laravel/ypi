<?php

namespace App\Repositories;

use App\Models\Kpi;
use App\Repositories\BaseRepository;

/**
 * Class KpiRepository
 * @package App\Repositories
 * @version July 28, 2022, 7:58 am UTC
*/

class KpiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'date',
        'player_id',
        'umur',
        'height',
        'weight',
        'beep_test',
        'vo2max',
        'court_agility_1',
        'court_agility_2',
        'court_agility_3',
        'squad',
        'bench_press',
        'vertical_jump',
        'skipping',
        'run',
        'note'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Kpi::class;
    }
}
