<?php

namespace App\Repositories;

use App\Models\TabelKelompok;
use App\Repositories\BaseRepository;

/**
 * Class TabelKelompokRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:55 am UTC
*/

class TabelKelompokRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kelompok'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TabelKelompok::class;
    }
}
