<?php

namespace App\Repositories;

use App\Models\KpiValue;
use App\Repositories\BaseRepository;

/**
 * Class KpiValueRepository
 * @package App\Repositories
 * @version July 4, 2022, 2:13 am UTC
*/

class KpiValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kpi_indicator_id',
        'kpi_subindicator_id',
        'date',
        'pb',
        'target',
        'actual'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return KpiValue::class;
    }
}
