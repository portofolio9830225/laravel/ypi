<?php

namespace App\Repositories;

use App\Models\GroupActivity;
use App\Repositories\BaseRepository;

/**
 * Class GroupActivityRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:13 am UTC
*/

class GroupActivityRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'kelompok',
        'description',
        'color'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GroupActivity::class;
    }
}
