<?php

namespace App\Repositories;

use App\Models\DetailKpiPhysic;
use App\Repositories\BaseRepository;

/**
 * Class DetailKpiPhysicRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:51 pm UTC
*/

class DetailKpiPhysicRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'pemain_id',
        'tanggal_kpi',
        'keterangan'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailKpiPhysic::class;
    }
}
