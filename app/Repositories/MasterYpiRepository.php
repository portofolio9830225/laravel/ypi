<?php

namespace App\Repositories;

use App\Models\MasterYpi;
use App\Repositories\BaseRepository;

/**
 * Class MasterYpiRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:04 am UTC
*/

class MasterYpiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'keterangan',
        'kode_pelatih',
        'voltarget',
        'tanggal_mulai',
        'tanggal_selesai',
        'default_intensity'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterYpi::class;
    }
}
