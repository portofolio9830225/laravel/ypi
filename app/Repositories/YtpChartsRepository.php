<?php

namespace App\Repositories;

use App\Models\YtpCharts;
use App\Repositories\BaseRepository;

/**
 * Class YtpChartsRepository
 * @package App\Repositories
 * @version May 21, 2022, 3:29 am UTC
*/

class YtpChartsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return YtpCharts::class;
    }
}
