<?php

namespace App\Repositories;

use App\Models\CompetitionGroup;
use App\Repositories\BaseRepository;

/**
 * Class CompetitionGroupRepository
 * @package App\Repositories
 * @version June 30, 2022, 6:31 am UTC
*/

class CompetitionGroupRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'competition_id',
        'group_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return CompetitionGroup::class;
    }
}
