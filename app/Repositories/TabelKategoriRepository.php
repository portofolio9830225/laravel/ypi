<?php

namespace App\Repositories;

use App\Models\TabelKategori;
use App\Repositories\BaseRepository;

/**
 * Class TabelKategoriRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:49 am UTC
*/

class TabelKategoriRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_kategori',
        'level_kategori_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return TabelKategori::class;
    }
}
