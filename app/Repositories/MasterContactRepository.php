<?php

namespace App\Repositories;

use App\Models\MasterContact;
use App\Repositories\BaseRepository;

/**
 * Class MasterContactRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:56 am UTC
*/

class MasterContactRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nama_lengkap',
        'alamat',
        'negara',
        'kodepos',
        'telepon',
        'email',
        'keterangan',
        'father_height',
        'father_weight',
        'mother_height',
        'mother_weight'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return MasterContact::class;
    }
}
