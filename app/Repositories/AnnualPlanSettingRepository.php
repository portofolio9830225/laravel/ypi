<?php

namespace App\Repositories;

use App\Models\AnnualPlanSetting;
use App\Repositories\BaseRepository;

/**
 * Class AnnualPlanSettingRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:13 am UTC
*/

class AnnualPlanSettingRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'kelompok',
        'description',
        'color'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return AnnualPlanSetting::class;
    }
}
