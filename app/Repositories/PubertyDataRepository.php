<?php

namespace App\Repositories;

use App\Models\PubertyData;
use App\Repositories\BaseRepository;

/**
 * Class PubertyDataRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:53 pm UTC
*/

class PubertyDataRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'height_predicted_id',
        'pemain_id',
        'dob',
        'age',
        'height',
        'national_average',
        'growth_rate',
        'biological_category',
        'nation_id',
        'bulan_awal',
        'tahun_awal'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return PubertyData::class;
    }
}
