<?php

namespace App\Repositories;

use App\Models\SubExercise;
use App\Repositories\BaseRepository;

/**
 * Class SubExerciseRepository
 * @package App\Repositories
 * @version March 23, 2022, 6:07 am UTC
*/

class SubExerciseRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'excercise_desc',
        'gym_type_id',
        'tut_per_rep',
        'exercise_category_id'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SubExercise::class;
    }
}
