<?php

namespace App\Repositories;

use App\Models\FitnessTest;
use App\Repositories\BaseRepository;

/**
 * Class FitnessTestRepository
 * @package App\Repositories
 * @version March 23, 2022, 7:18 am UTC
*/

class FitnessTestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'tanggal_test',
        'gender',
        'kelompok_id',
        'pemain_id',
        'court_agility',
        'vo2max'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return FitnessTest::class;
    }
}
