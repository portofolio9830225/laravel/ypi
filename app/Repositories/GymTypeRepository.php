<?php

namespace App\Repositories;

use App\Models\GymType;
use App\Repositories\BaseRepository;

/**
 * Class GymTypeRepository
 * @package App\Repositories
 * @version March 23, 2022, 5:35 am UTC
*/

class GymTypeRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'gym_description'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return GymType::class;
    }
}
