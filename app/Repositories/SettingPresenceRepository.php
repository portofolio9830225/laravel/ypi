<?php

namespace App\Repositories;

use App\Models\SettingPresence;
use App\Repositories\BaseRepository;

/**
 * Class SettingPresenceRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:49 pm UTC
*/

class SettingPresenceRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'kelompok_id',
        'pelatih_id',
        'keterangan',
        'inisial',
        'presence'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return SettingPresence::class;
    }
}
