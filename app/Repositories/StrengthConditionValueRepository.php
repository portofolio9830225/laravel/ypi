<?php

namespace App\Repositories;

use App\Models\StrengthConditionValue;
use App\Repositories\BaseRepository;

/**
 * Class StrengthConditionValueRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:52 pm UTC
*/

class StrengthConditionValueRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'strength_condition_id',
        'strength_condition_player_id',
        'name',
        'reps',
        'weight',
        'predict',
        'min',
        'max',
        'strength',
        'last',
        'diff'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return StrengthConditionValue::class;
    }
}
