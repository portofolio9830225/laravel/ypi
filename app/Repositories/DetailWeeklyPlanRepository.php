<?php

namespace App\Repositories;

use App\Models\DetailWeeklyPlan;
use App\Repositories\BaseRepository;

/**
 * Class DetailWeeklyPlanRepository
 * @package App\Repositories
 * @version March 23, 2022, 1:50 pm UTC
*/

class DetailWeeklyPlanRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'master_ypi_id',
        'minggu',
        'tanggal',
        'session',
        'jam',
        'details',
        'set',
        'rep',
        'pace',
        'work_time',
        'rest_interval',
        'rest_period',
        'emphesis_goal',
        'duration',
        'intensity',
        'tr_load'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailWeeklyPlan::class;
    }
}
