<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSettingPresenceAPIRequest;
use App\Http\Requests\API\UpdateSettingPresenceAPIRequest;
use App\Models\SettingPresence;
use App\Repositories\SettingPresenceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SettingPresenceController
 * @package App\Http\Controllers\API
 */

class SettingPresenceAPIController extends AppBaseController
{
    /** @var  SettingPresenceRepository */
    private $settingPresenceRepository;

    public function __construct(SettingPresenceRepository $settingPresenceRepo)
    {
        $this->settingPresenceRepository = $settingPresenceRepo;
    }

    /**
     * Display a listing of the SettingPresence.
     * GET|HEAD /settingPresences
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('kelompok_id') != null && $request->query('pelatih_id') != null) {
            $settingPresences = SettingPresence::where('kelompok_id', $request->query('kelompok_id'))
                ->where('pelatih_id', $request->query('pelatih_id'))
                ->get();

            return $this->sendResponse($settingPresences->toArray(), 'Setting Attendances retrieved successfully');
        }

        $settingPresences = $this->settingPresenceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($settingPresences->toArray(), 'Setting Presences retrieved successfully');
    }

    /**
     * Store a newly created SettingPresence in storage.
     * POST /settingPresences
     *
     * @param CreateSettingPresenceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSettingPresenceAPIRequest $request)
    {
        $input = $request->all();

        $settingPresence = $this->settingPresenceRepository->create($input);

        return $this->sendResponse($settingPresence->toArray(), 'Setting Presence saved successfully');
    }

    public function batchStore(Request $request)
    {
        $input = $request->all();


        foreach ($input['listData'] as $key => $value) {
            if (isset($value['id'])) {
                $presence = SettingPresence::find($value['id']);
                $presence->fill($value);
                $presence->save();
            } else {
                SettingPresence::create($value);
            }
        }

        if (isset($input['deleted_ids'])) {
            foreach ($input['deleted_ids'] as $key => $value) {
                $presence = SettingPresence::where('id', $value)->delete();
            }
        }

        return $this->sendResponse($input, 'Annual Plan Setting saved successfully');
    }

    /**
     * Display the specified SettingPresence.
     * GET|HEAD /settingPresences/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SettingPresence $settingPresence */
        $settingPresence = $this->settingPresenceRepository->find($id);

        if (empty($settingPresence)) {
            return $this->sendError('Setting Presence not found');
        }

        return $this->sendResponse($settingPresence->toArray(), 'Setting Presence retrieved successfully');
    }

    /**
     * Update the specified SettingPresence in storage.
     * PUT/PATCH /settingPresences/{id}
     *
     * @param int $id
     * @param UpdateSettingPresenceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSettingPresenceAPIRequest $request)
    {
        $input = $request->all();

        /** @var SettingPresence $settingPresence */
        $settingPresence = $this->settingPresenceRepository->find($id);

        if (empty($settingPresence)) {
            return $this->sendError('Setting Presence not found');
        }

        $settingPresence = $this->settingPresenceRepository->update($input, $id);

        return $this->sendResponse($settingPresence->toArray(), 'SettingPresence updated successfully');
    }

    /**
     * Remove the specified SettingPresence from storage.
     * DELETE /settingPresences/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /** @var SettingPresence $settingPresence */

        if ($request->query('kelompok_id') != null && $request->query('pelatih_id') != null) {
            $settingPresences = SettingPresence::where('kelompok_id', $request->query('kelompok_id'))
                ->where('pelatih_id', $request->query('pelatih_id'))
                ->delete();

            return $this->sendSuccess('Setting Presence deleted successfully');
        }

        $settingPresence = $this->settingPresenceRepository->find($id);

        if (empty($settingPresence)) {
            return $this->sendError('Setting Presence not found');
        }

        $settingPresence->delete();

        return $this->sendSuccess('Setting Presence deleted successfully');
    }
}
