<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateYpiEventAPIRequest;
use App\Http\Requests\API\UpdateYpiEventAPIRequest;
use App\Models\YpiEvent;
use App\Repositories\YpiEventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class YpiEventController
 * @package App\Http\Controllers\API
 */

class YpiEventAPIController extends AppBaseController
{
    /** @var  YpiEventRepository */
    private $ypiEventRepository;

    public function __construct(YpiEventRepository $ypiEventRepo)
    {
        $this->ypiEventRepository = $ypiEventRepo;
    }

    /**
     * Display a listing of the YpiEvent.
     * GET|HEAD /ypiEvents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $ypiEvents = $this->ypiEventRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ypiEvents->toArray(), 'Ypi Events retrieved successfully');
    }

    /**
     * Store a newly created YpiEvent in storage.
     * POST /ypiEvents
     *
     * @param CreateYpiEventAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateYpiEventAPIRequest $request)
    {
        $input = $request->all();

        $ypiEvent = $this->ypiEventRepository->create($input);

        return $this->sendResponse($ypiEvent->toArray(), 'Ypi Event saved successfully');
    }

    /**
     * Display the specified YpiEvent.
     * GET|HEAD /ypiEvents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var YpiEvent $ypiEvent */
        $ypiEvent = $this->ypiEventRepository->find($id);

        if (empty($ypiEvent)) {
            return $this->sendError('Ypi Event not found');
        }

        return $this->sendResponse($ypiEvent->toArray(), 'Ypi Event retrieved successfully');
    }

    /**
     * Update the specified YpiEvent in storage.
     * PUT/PATCH /ypiEvents/{id}
     *
     * @param int $id
     * @param UpdateYpiEventAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateYpiEventAPIRequest $request)
    {
        $input = $request->all();

        /** @var YpiEvent $ypiEvent */
        $ypiEvent = $this->ypiEventRepository->find($id);

        if (empty($ypiEvent)) {
            return $this->sendError('Ypi Event not found');
        }

        $ypiEvent = $this->ypiEventRepository->update($input, $id);

        return $this->sendResponse($ypiEvent->toArray(), 'YpiEvent updated successfully');
    }

    /**
     * Remove the specified YpiEvent from storage.
     * DELETE /ypiEvents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var YpiEvent $ypiEvent */
        $ypiEvent = $this->ypiEventRepository->find($id);

        if (empty($ypiEvent)) {
            return $this->sendError('Ypi Event not found');
        }

        $ypiEvent->delete();

        return $this->sendSuccess('Ypi Event deleted successfully');
    }
}
