<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStrengthConditionPlayerAPIRequest;
use App\Http\Requests\API\UpdateStrengthConditionPlayerAPIRequest;
use App\Models\StrengthConditionPlayer;
use App\Repositories\StrengthConditionPlayerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StrengthConditionPlayerController
 * @package App\Http\Controllers\API
 */

class StrengthConditionPlayerAPIController extends AppBaseController
{
    /** @var  StrengthConditionPlayerRepository */
    private $strengthConditionPlayerRepository;

    public function __construct(StrengthConditionPlayerRepository $strengthConditionPlayerRepo)
    {
        $this->strengthConditionPlayerRepository = $strengthConditionPlayerRepo;
    }

    /**
     * Display a listing of the StrengthConditionPlayer.
     * GET|HEAD /strengthConditionPlayers
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('strength_conditioning_id') != null && $request->query('pemain_id') != null) {
            $strengthConditionPlayers = StrengthConditionPlayer::where('strength_conditioning_id', $request->query('strength_conditioning_id'))
                ->where('pemain_id', $request->query('pemain_id'))
                ->first();

            if ($strengthConditionPlayers == null) {
                return $this->sendResponse(null, 'Strength Condition Players retrieved successfully');
            }

            return $this->sendResponse($strengthConditionPlayers->toArray(), 'Strength Condition Players retrieved successfully');
        }

        $strengthConditionPlayers = $this->strengthConditionPlayerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($strengthConditionPlayers->toArray(), 'Strength Condition Players retrieved successfully');
    }

    /**
     * Store a newly created StrengthConditionPlayer in storage.
     * POST /strengthConditionPlayers
     *
     * @param CreateStrengthConditionPlayerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStrengthConditionPlayerAPIRequest $request)
    {
        $input = $request->all();

        // $strengthConditionPlayer = $this->strengthConditionPlayerRepository->create($input);
        $strengthConditionPlayer = StrengthConditionPlayer::updateOrCreate(
            [
                'strength_conditioning_id' => $input['strength_conditioning_id'],
                'pemain_id' => $input['pemain_id'],
            ],
            [$input]
        );

        return $this->sendResponse($strengthConditionPlayer->toArray(), 'Strength Condition Player saved successfully');
    }

    /**
     * Display the specified StrengthConditionPlayer.
     * GET|HEAD /strengthConditionPlayers/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StrengthConditionPlayer $strengthConditionPlayer */
        $strengthConditionPlayer = $this->strengthConditionPlayerRepository->find($id);

        if (empty($strengthConditionPlayer)) {
            return $this->sendError('Strength Condition Player not found');
        }

        return $this->sendResponse($strengthConditionPlayer->toArray(), 'Strength Condition Player retrieved successfully');
    }

    /**
     * Update the specified StrengthConditionPlayer in storage.
     * PUT/PATCH /strengthConditionPlayers/{id}
     *
     * @param int $id
     * @param UpdateStrengthConditionPlayerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStrengthConditionPlayerAPIRequest $request)
    {
        $input = $request->all();

        /** @var StrengthConditionPlayer $strengthConditionPlayer */
        $strengthConditionPlayer = $this->strengthConditionPlayerRepository->find($id);

        if (empty($strengthConditionPlayer)) {
            return $this->sendError('Strength Condition Player not found');
        }

        $strengthConditionPlayer = $this->strengthConditionPlayerRepository->update($input, $id);

        return $this->sendResponse($strengthConditionPlayer->toArray(), 'StrengthConditionPlayer updated successfully');
    }

    /**
     * Remove the specified StrengthConditionPlayer from storage.
     * DELETE /strengthConditionPlayers/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StrengthConditionPlayer $strengthConditionPlayer */
        $strengthConditionPlayer = $this->strengthConditionPlayerRepository->find($id);

        if (empty($strengthConditionPlayer)) {
            return $this->sendError('Strength Condition Player not found');
        }

        $strengthConditionPlayer->delete();

        return $this->sendSuccess('Strength Condition Player deleted successfully');
    }
}
