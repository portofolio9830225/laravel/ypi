<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStrengthConditioningAPIRequest;
use App\Http\Requests\API\UpdateStrengthConditioningAPIRequest;
use App\Models\StrengthConditioning;
use App\Repositories\StrengthConditioningRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StrengthConditioningController
 * @package App\Http\Controllers\API
 */

class StrengthConditioningAPIController extends AppBaseController
{
    /** @var  StrengthConditioningRepository */
    private $strengthConditioningRepository;

    public function __construct(StrengthConditioningRepository $strengthConditioningRepo)
    {
        $this->strengthConditioningRepository = $strengthConditioningRepo;
    }

    /**
     * Display a listing of the StrengthConditioning.
     * GET|HEAD /strengthConditionings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('groupDate') != null) {
            $strengthConditionings = StrengthConditioning::groupBy('date_test')
                ->get();

            return $this->sendResponse($strengthConditionings->toArray(), 'Setting Attendances retrieved successfully');
        }

        $strengthConditionings = $this->strengthConditioningRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($strengthConditionings->toArray(), 'Strength Conditionings retrieved successfully');
    }

    /**
     * Store a newly created StrengthConditioning in storage.
     * POST /strengthConditionings
     *
     * @param CreateStrengthConditioningAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStrengthConditioningAPIRequest $request)
    {
        $input = $request->all();

        // $strengthConditioning = $this->strengthConditioningRepository->create($input);
        $strengthConditioning = StrengthConditioning::updateOrCreate(
            ['date_test' => $input['date_test'], 'gender' => $input['gender'], 'age_group_id' => $input['age_group_id']],
            [$input]
        );

        return $this->sendResponse($strengthConditioning->toArray(), 'Strength Conditioning saved successfully');
    }

    /**
     * Display the specified StrengthConditioning.
     * GET|HEAD /strengthConditionings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StrengthConditioning $strengthConditioning */
        $strengthConditioning = $this->strengthConditioningRepository->find($id);

        if (empty($strengthConditioning)) {
            return $this->sendError('Strength Conditioning not found');
        }

        return $this->sendResponse($strengthConditioning->toArray(), 'Strength Conditioning retrieved successfully');
    }

    /**
     * Update the specified StrengthConditioning in storage.
     * PUT/PATCH /strengthConditionings/{id}
     *
     * @param int $id
     * @param UpdateStrengthConditioningAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStrengthConditioningAPIRequest $request)
    {
        $input = $request->all();

        /** @var StrengthConditioning $strengthConditioning */
        $strengthConditioning = $this->strengthConditioningRepository->find($id);

        if (empty($strengthConditioning)) {
            return $this->sendError('Strength Conditioning not found');
        }

        $strengthConditioning = $this->strengthConditioningRepository->update($input, $id);

        return $this->sendResponse($strengthConditioning->toArray(), 'StrengthConditioning updated successfully');
    }

    /**
     * Remove the specified StrengthConditioning from storage.
     * DELETE /strengthConditionings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StrengthConditioning $strengthConditioning */
        $strengthConditioning = $this->strengthConditioningRepository->find($id);

        if (empty($strengthConditioning)) {
            return $this->sendError('Strength Conditioning not found');
        }

        $strengthConditioning->delete();

        return $this->sendSuccess('Strength Conditioning deleted successfully');
    }
}
