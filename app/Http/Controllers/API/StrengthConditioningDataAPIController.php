<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStrengthConditioningDataAPIRequest;
use App\Http\Requests\API\UpdateStrengthConditioningDataAPIRequest;
use App\Models\StrengthConditioningData;
use App\Repositories\StrengthConditioningDataRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Response;
use DatePeriod;
use DateTime;
use DateInterval;

/**
 * Class StrengthConditioningDataController
 * @package App\Http\Controllers\API
 */

class StrengthConditioningDataAPIController extends AppBaseController
{
    /** @var  StrengthConditioningDataRepository */
    private $strengthConditioningDataRepository;

    public function __construct(StrengthConditioningDataRepository $strengthConditioningDataRepo)
    {
        $this->strengthConditioningDataRepository = $strengthConditioningDataRepo;
    }

    /**
     * Display a listing of the StrengthConditioningData.
     * GET|HEAD /strengthConditioningDatas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (
            $request->query('pemain_id') != null
            && $request->query('gym_type') != null
            && $request->query('master_ypi_id') != null
            && $request->query('ypi_start') != null
            && $request->query('ypi_end') != null
        ) {
            // DB::transaction(function () use ($request) {

            $strengthConditioningDatas = StrengthConditioningData::where('pemain_id', $request->query('pemain_id'))
                ->where('gym_type', $request->query('gym_type'))
                ->where('master_ypi_id', $request->query('master_ypi_id'))
                ->first();
            // return $this->sendResponse($strengthConditioningDatas, 'Strength');
            if ($strengthConditioningDatas == null) {
                $dates = new DatePeriod(
                    new DateTime($request->query('ypi_start')),
                    new DateInterval('P1D'),
                    new DateTime($request->query('ypi_end'))
                );

                $data = [];
                $week = 1;
                foreach ($dates as $k => $date) {
                    $newData = [
                        'week' => $week,
                        'tanggal' => $date->format('d-m-Y'),
                        'hari' => $date->format('D'),
                        'pemain_id' => $request->query('pemain_id'),
                        'gym_type' => $request->query('gym_type'),
                        'master_ypi_id' => $request->query('master_ypi_id'),
                    ];
                    $data[] = $newData;
                    StrengthConditioningData::create($newData);

                    if (($k + 1) % 7 == 0) {
                        $week += 1;
                    }
                }
            }
            $strengthConditioningDatas = StrengthConditioningData::where('pemain_id', $request->query('pemain_id'))
                ->where('gym_type', $request->query('gym_type'))
                ->where('master_ypi_id', $request->query('master_ypi_id'))
                ->get();

            return $this->sendResponse($strengthConditioningDatas->toArray(), 'Strength Conditioning Datas retrieved successfully');
            // });
        }

        $strengthConditioningDatas = $this->strengthConditioningDataRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        return $this->sendResponse(['test'], 'Strength Conditioning Datas retrieved successfully');
    }

    /**
     * Store a newly created StrengthConditioningData in storage.
     * POST /strengthConditioningDatas
     *
     * @param CreateStrengthConditioningDataAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStrengthConditioningDataAPIRequest $request)
    {
        if (
            $request->query('pemain_id') != null
            && $request->query('gym_type') != null
            && $request->query('master_ypi_id') != null
            && $request->query('ypi_start') != null
            && $request->query('ypi_end') != null
        ) {

            $strengthConditioningDatas = StrengthConditioningData::where('pemain_id', $request->query('pemain_id'))
                ->where('gym_type', $request->query('gym_type'))
                ->where('master_ypi_id', $request->query('master_ypi_id'))
                ->first();
            return $this->sendResponse($request->query('ypi_start'), 'Strength');
            if ($strengthConditioningDatas == null) {
                $dates = new DatePeriod(
                    new DateTime($request->query('ypi_start')),
                    new DateInterval('P1D'),
                    new DateTime($request->query('ypi_end'))
                );

                $data = [];
                $week = 1;
                foreach ($dates as $k => $date) {
                    $newData = [
                        'week' => $week,
                        'tanggal' => $date->format('d-m-Y'),
                        'hari' => $date->format('D'),
                        'pemain_id' => $request->query('pemain_id'),
                        'gym_type' => $request->query('gym_type'),
                        'master_ypi_id' => $request->query('master_ypi_id'),
                    ];
                    $data[] = $newData;
                    StrengthConditioningData::create($newData);

                    if (($k + 1) % 7 == 0) {
                        $week += 1;
                    }
                }
            }
            $strengthConditioningDatas = StrengthConditioningData::where('pemain_id', $request->query('pemain_id'))
                ->where('gym_type', $request->query('gym_type'))
                ->where('master_ypi_id', $request->query('master_ypi_id'))
                ->get();

            return $this->sendResponse($strengthConditioningDatas->toArray(), 'Strength Conditioning Datas retrieved successfully');
        }

        $input = $request->all();

        $strengthConditioningData = $this->strengthConditioningDataRepository->create($input);

        return $this->sendResponse('aa', 'Strength Conditioning Data saved successfully');
    }

    /**
     * Display the specified StrengthConditioningData.
     * GET|HEAD /strengthConditioningDatas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StrengthConditioningData $strengthConditioningData */
        $strengthConditioningData = $this->strengthConditioningDataRepository->find($id);

        if (empty($strengthConditioningData)) {
            return $this->sendError('Strength Conditioning Data not found');
        }

        return $this->sendResponse($strengthConditioningData->toArray(), 'Strength Conditioning Data retrieved successfully');
    }

    /**
     * Update the specified StrengthConditioningData in storage.
     * PUT/PATCH /strengthConditioningDatas/{id}
     *
     * @param int $id
     * @param UpdateStrengthConditioningDataAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStrengthConditioningDataAPIRequest $request)
    {
        $input = $request->all();

        /** @var StrengthConditioningData $strengthConditioningData */
        $strengthConditioningData = $this->strengthConditioningDataRepository->find($id);

        $scDataPerWeek = StrengthConditioningData::where('pemain_id', $input['pemain_id'])
            ->where('gym_type', $input['gym_type'])
            ->where('master_ypi_id', $input['master_ypi_id'])
            ->where('week', $input['week'])
            ->get()->toArray();

        $currentIndexData = array_search($input['id'], array_column($scDataPerWeek, 'id'));
        $scDataPerWeek[$currentIndexData] = $input;

        $weeklyTut = array_sum(array_column($scDataPerWeek, 'tut_exercise'));
        $weeklyTonnage = array_sum(array_column($scDataPerWeek, 'tonnage'));
        $weeklyLoad = array_sum(array_column($scDataPerWeek, 'physical_load'));

        $avg1rmArray = array_column($scDataPerWeek, 'average_1rm');
        $avg1rmExist = [];
        foreach ($avg1rmArray as $key => $value) {
            if ($value > 0) {
                $avg1rmExist[] = $value;
            }
        }
        $avg1rm = array_sum($avg1rmExist) / count($avg1rmExist);

        StrengthConditioningData::where('pemain_id', $input['pemain_id'])
            ->where('gym_type', $input['gym_type'])
            ->where('master_ypi_id', $input['master_ypi_id'])
            ->where('week', $input['week'])
            ->update([
                'weekly_tut' => $weeklyTut,
                'weekly_tonnage' => $weeklyTonnage,
                'weekly_load' => $weeklyLoad,
                'weekly_average' => $avg1rm,
            ]);

        if (empty($strengthConditioningData)) {
            return $this->sendError('Strength Conditioning Data not found');
        }

        $strengthConditioningData = $this->strengthConditioningDataRepository->update($input, $id);

        return $this->sendResponse($strengthConditioningData->toArray(), 'StrengthConditioningData updated successfully');
    }

    public function updateRmCalculated(Request $request)
    {
        $input = $request->all();

        StrengthConditioningData::where('pemain_id', $input['pemain_id'])
            ->where('gym_type', $input['gym_type'])
            ->where('master_ypi_id', $input['master_ypi_id'])
            ->where('week', $input['week'])
            ->update([
                'actual_1rm' => $input['rm_calculated'],
            ]);

        return $this->sendResponse([], 'StrengthConditioningData updated successfully');
    }

    /**
     * Remove the specified StrengthConditioningData from storage.
     * DELETE /strengthConditioningDatas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StrengthConditioningData $strengthConditioningData */
        $strengthConditioningData = $this->strengthConditioningDataRepository->find($id);

        if (empty($strengthConditioningData)) {
            return $this->sendError('Strength Conditioning Data not found');
        }

        $strengthConditioningData->delete();

        return $this->sendSuccess('Strength Conditioning Data deleted successfully');
    }
}
