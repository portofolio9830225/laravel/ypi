<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHeightDataAPIRequest;
use App\Http\Requests\API\UpdateHeightDataAPIRequest;
use App\Models\HeightData;
use App\Repositories\HeightDataRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class HeightDataController
 * @package App\Http\Controllers\API
 */

class HeightDataAPIController extends AppBaseController
{
    /** @var  HeightDataRepository */
    private $heightDataRepository;

    public function __construct(HeightDataRepository $heightDataRepo)
    {
        $this->heightDataRepository = $heightDataRepo;
    }

    /**
     * Display a listing of the HeightData.
     * GET|HEAD /heightDatas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $heightDatas = $this->heightDataRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($heightDatas->toArray(), 'Height Datas retrieved successfully');
    }

    /**
     * Store a newly created HeightData in storage.
     * POST /heightDatas
     *
     * @param CreateHeightDataAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHeightDataAPIRequest $request)
    {
        $input = $request->all();

        $heightData = $this->heightDataRepository->create($input);

        return $this->sendResponse($heightData->toArray(), 'Height Data saved successfully');
    }

    /**
     * Display the specified HeightData.
     * GET|HEAD /heightDatas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HeightData $heightData */
        $heightData = $this->heightDataRepository->find($id);

        if (empty($heightData)) {
            return $this->sendError('Height Data not found');
        }

        return $this->sendResponse($heightData->toArray(), 'Height Data retrieved successfully');
    }

    /**
     * Update the specified HeightData in storage.
     * PUT/PATCH /heightDatas/{id}
     *
     * @param int $id
     * @param UpdateHeightDataAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHeightDataAPIRequest $request)
    {
        $input = $request->all();

        /** @var HeightData $heightData */
        $heightData = $this->heightDataRepository->find($id);

        if (empty($heightData)) {
            return $this->sendError('Height Data not found');
        }

        $heightData = $this->heightDataRepository->update($input, $id);

        return $this->sendResponse($heightData->toArray(), 'HeightData updated successfully');
    }

    /**
     * Remove the specified HeightData from storage.
     * DELETE /heightDatas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HeightData $heightData */
        $heightData = $this->heightDataRepository->find($id);

        if (empty($heightData)) {
            return $this->sendError('Height Data not found');
        }

        $heightData->delete();

        return $this->sendSuccess('Height Data deleted successfully');
    }
}
