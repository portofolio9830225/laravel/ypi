<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterYpiAPIRequest;
use App\Http\Requests\API\UpdateMasterYpiAPIRequest;
use App\Models\MasterYpi;
use App\Repositories\MasterYpiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\AnnualPlanSetting;
use App\Models\DefaultTarget;
use App\Models\MasterAchievement;
use App\Models\MasterPelatih;
use App\Models\SeasonPlanner;
use App\Models\SeasonPlanValue;
use App\Models\YpiEvent;
use App\Models\YpiGoal;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class MasterYpiController
 * @package App\Http\Controllers\API
 */

class MasterYpiAPIController extends AppBaseController
{
    /** @var  MasterYpiRepository */
    private $masterYpiRepository;

    public function __construct(MasterYpiRepository $masterYpiRepo)
    {
        $this->masterYpiRepository = $masterYpiRepo;
    }

    /**
     * Display a listing of the MasterYpi.
     * GET|HEAD /masterYpis
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->has('with_coach')) {
            $masterYpis = MasterYpi::with('coach')->get();
            return $this->sendResponse($masterYpis, 'Master Ypis retrieved successfully');
        }

        if ($request->query('full_data') != null) {
            $masterYpis = MasterYpi::with('coach', 'default_target', 'ypi_goals')->get();
            $masterYpisArray = $masterYpis->toArray();
            foreach ($masterYpisArray as $key => $value) {
                $masterAchievements = MasterAchievement::where('tanggal_mulai', '>=', $value['tanggal_mulai'])
                    ->where('tanggal_akhir', '<=', $value['tanggal_selesai'])
                    ->get();
                $masterYpisArray[$key]['events'] = $masterAchievements;

                $seasonPlanners = SeasonPlanner::with('season_planner_details')->where('master_ypi_id', $value['id'])->get();
                $masterYpisArray[$key]['season_planners'] = $seasonPlanners;

                $ypiEvents = YpiEvent::with('competition_details')->where('master_ypi_id', $value['id'])->get();
                $masterYpisArray[$key]['ypi_events'] = $ypiEvents;
            }

            return $this->sendResponse($masterYpisArray, 'Master Ypis retrieved successfully');
        }


        $masterYpis = $this->masterYpiRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        if ($request->has('forTableData')) {
            if ($request->has('coach_id')) {
                $masterYpis = MasterYpi::where('kode_pelatih', $request->query('coach_id'))->get()->toArray();
            } else {

                $masterYpis = MasterYpi::get()->toArray();
            }
            foreach ($masterYpis as $key => $value) {
                $coach = MasterPelatih::find($value['kode_pelatih']);
                $masterYpis[$key]['coach_name'] = $coach->nama_lengkap;

                $seasonPlansText = '';
                $seasonPlanValues = SeasonPlanValue::with('seasonPlan', 'seasonPlanDetail')->where('ypi_id', $value['id'])->groupBy('season_plan_id')->get()->toArray();
                foreach ($seasonPlanValues as $k => $v) {
                    if ($k == (count($seasonPlanValues) - 1)) {
                        $seasonPlansText = $seasonPlansText . $v['season_plan']['name'];
                    } else {
                        $seasonPlansText = $seasonPlansText . $v['season_plan']['name'] . ', ';
                    }
                }
                $masterYpis[$key]['season_plans_text'] = $seasonPlansText;
            }

            return $this->sendResponse($masterYpis, 'Master Ypis retrieved successfully');
        }

        return $this->sendResponse($masterYpis->toArray(), 'Master Ypis retrieved successfully');
    }

    /**
     * Store a newly created MasterYpi in storage.
     * POST /masterYpis
     *
     * @param CreateMasterYpiAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        DB::transaction(function () use ($input) {
            // master ypi
            $ypi = MasterYpi::create($input);

            // ypi goals
            foreach ($input['ypi_goals'] as $key => $value) {
                $val = $value;
                $val['master_ypi_id'] = $ypi->id;
                YpiGoal::create($val);
            }

            // default target
            $defaultTargetData = $input['default_target'];
            $defaultTargetData['master_ypi_id'] = $ypi->id;
            DefaultTarget::create($defaultTargetData);

            // season plan value
            foreach ($input['season_plan_values'] as $key => $value) {
                $newData = $value;
                $newData['ypi_id'] = $ypi->id;
                SeasonPlanValue::updateOrCreate([
                    'ypi_id' => $ypi->id,
                    'season_plan_id' => $value['season_plan_id'],
                    'season_plan_detail_id' => $value['season_plan_detail_id'],
                ], $newData);
            }

            // ypi events
            foreach ($input['ypi_events'] as $key => $value) {
                $newData = $value;
                $newData['ypi_id'] = $ypi->id;
                YpiEvent::updateOrCreate([
                    'master_ypi_id' => $ypi->id,
                    'master_achievement_id' => $value['master_achievement_id']
                ], $newData);
            }
        });

        return $this->sendResponse('success', 'Master Ypi saved successfully');
    }

    /**
     * Display the specified MasterYpi.
     * GET|HEAD /masterYpis/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id, Request $request)
    {
        if ($request->has('full_data')) {
            $masterYpi = MasterYpi::find($id);
            $voltargets = DefaultTarget::where('master_ypi_id', $id)->first();
            $masterYpi['default_target'] = $voltargets;
            $spValues = SeasonPlanValue::where('ypi_id', $id)->with('seasonPlan', 'seasonPlanDetail')->get()->toArray();
            foreach ($spValues as $key => $value) {
                $spValues[$key]['seasonPlan'] = $value['season_plan']['name'];
                $spValues[$key]['seasonPlanDetail'] = $value['season_plan_detail']['name'];
                $spValues[$key]['inputType'] = $value['season_plan_detail']['input_type'];
            }
            $masterYpi['season_plan_values'] = $spValues;
            $ypiEvents = YpiEvent::where('master_ypi_id', $id)->get();
            $masterYpi['ypi_events'] = $ypiEvents;
            $ypiGoals = YpiGoal::where('master_ypi_id', $id)->get();
            $masterYpi['ypi_goals'] = $ypiGoals;
            $coach = MasterPelatih::where('id', $masterYpi->kode_pelatih)->first();
            $masterYpi['coach_name'] = $coach['nama_lengkap'];

            return $this->sendResponse($masterYpi, 'Master Ypi retrieved successfully');
        }

        /** @var MasterYpi $masterYpi */
        $masterYpi = $this->masterYpiRepository->find($id);

        if (empty($masterYpi)) {
            return $this->sendError('Master Ypi not found');
        }

        return $this->sendResponse($request->query(), 'Master Ypi retrieved successfully');
    }

    /**
     * Update the specified MasterYpi in storage.
     * PUT/PATCH /masterYpis/{id}
     *
     * @param int $id
     * @param UpdateMasterYpiAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        DB::transaction(function () use ($input) {
            // master ypi
            $ypi = MasterYpi::find($input['id']);
            $ypi->fill($input);
            $ypi->save();

            // ypi goals
            foreach ($input['ypi_goals'] as $key => $value) {
                if (isset($value['id'])) {
                    YpiGoal::where('id', $value['id'])->update($value);
                } else {
                    $val = $value;
                    $val['master_ypi_id'] = $ypi->id;
                    YpiGoal::create($val);
                }
            }

            // deleted ypi goals
            if (isset($input['deleted_ypi_goals'])) {
                foreach ($input['deleted_ypi_goals'] as $key => $value) {
                    YpiGoal::where('id', $value)->delete();
                }
            }

            // default target
            DefaultTarget::where('id', $input['default_target']['id'])->update($input['default_target']);

            // season plans
            foreach ($input['season_plan_values'] as $key => $value) {
                $newData = $value;
                $newData['ypi_id'] = $ypi->id;
                SeasonPlanValue::updateOrCreate([
                    'ypi_id' => $ypi->id,
                    'season_plan_id' => $value['season_plan_id'],
                    'season_plan_detail_id' => $value['season_plan_detail_id'],
                ], $newData);
            }

            // ypi events
            foreach ($input['ypi_events'] as $key => $value) {
                $newData = $value;
                $newData['ypi_id'] = $ypi->id;
                YpiEvent::updateOrCreate([
                    'master_ypi_id' => $ypi->id,
                    'master_achievement_id' => $value['master_achievement_id']
                ], $newData);
            }
        });

        return $this->sendResponse('success', 'Master Ypi updated successfully');
    }

    /**
     * Remove the specified MasterYpi from storage.
     * DELETE /masterYpis/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            // season plan values
            SeasonPlanValue::where('ypi_id', $id)->delete();

            // ypi goals
            YpiGoal::where('master_ypi_id', $id)->delete();

            // default targets
            DefaultTarget::where('master_ypi_id', $id)->delete();

            // ypi events
            YpiEvent::where('master_ypi_id', $id)->delete();

            // coach agenda setting
            AnnualPlanSetting::where('master_ypi_id', $id)->delete();

            // master ypi
            MasterYpi::destroy($id);
        });

        return $this->sendSuccess('Master Ypi deleted successfully');
    }
}
