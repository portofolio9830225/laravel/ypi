<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\CompetitionStatus;
use App\Models\MasterAchievement;
use App\Models\TabelKategori;
use Illuminate\Http\Request;
use App\Reports\CompetitionReport;
use Illuminate\Support\Arr;
use TCPDF;

class ReportCompetitionAPIController extends Controller
{
    private $CompetitionReport;

    public function __construct(CompetitionReport $CompetitionReport)
    {
        $this->CompetitionReport = $CompetitionReport;
    }

    public function global()
    {
        // return request();
        $input = Arr::only(request()->all(), ['report_type', 'start_year', 'end_year', 'club_image']);

        if (request()->id_competition_detail != 'All') {
            $this->CompetitionReport->global($input, request()->id_competition_detail);
        } else {
            $this->CompetitionReport->global($input);
        }

        $pathToFile = storage_path('/app/competition.pdf');
        return response()->download($pathToFile);
    }

    public function group_by_category()
    {
        $IdCompetitionDetail = MasterAchievement::whereRaw("DATE_FORMAT(tanggal_mulai,'%Y') >= " . request()->start_year)
            ->whereRaw("DATE_FORMAT(tanggal_akhir,'%Y') <= " . request()->end_year)->get()->pluck('id')->toArray();

        $KategoriIdCompetitionDetail = MasterAchievement::whereRaw("DATE_FORMAT(tanggal_mulai,'%Y') >= " . request()->start_year)
            ->whereRaw("DATE_FORMAT(tanggal_akhir,'%Y') <= " . request()->end_year)->get()->pluck('kategori_id')->toArray();

        $tabelKategori = TabelKategori::get();
        // return $KategoriIdCompetitionDetail;
        $data = [];
        $no = 0;

        foreach ($tabelKategori as $item) {

            if (in_array($item->id, $KategoriIdCompetitionDetail)) {
                $data[$no]['nama_kategori'] = $item->nama_kategori;
                $arr = [];

                foreach ($IdCompetitionDetail as $val) {
                    $master = MasterAchievement::where('id', $val)->where('kategori_id', $item->id)->first();
                    if ($master) {
                        $arr[] = $master;
                    }
                }

                $data[$no]['competition_detail'] = $arr;
                $no += 1;
            }
        }

        // return $data;

        $this->CompetitionReport->groupByCategory(request()->input('club_image'), $data);
        $pathToFile = storage_path('/app/competitionGroup.pdf');
        return response()->download($pathToFile);
    }
}
