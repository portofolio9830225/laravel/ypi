<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetailWeeklyPlanAPIRequest;
use App\Http\Requests\API\UpdateDetailWeeklyPlanAPIRequest;
use App\Models\DetailWeeklyPlan;
use App\Repositories\DetailWeeklyPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DetailWeeklyPlanController
 * @package App\Http\Controllers\API
 */

class DetailWeeklyPlanAPIController extends AppBaseController
{
    /** @var  DetailWeeklyPlanRepository */
    private $detailWeeklyPlanRepository;

    public function __construct(DetailWeeklyPlanRepository $detailWeeklyPlanRepo)
    {
        $this->detailWeeklyPlanRepository = $detailWeeklyPlanRepo;
    }

    /**
     * Display a listing of the DetailWeeklyPlan.
     * GET|HEAD /detailWeeklyPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $detailWeeklyPlans = $this->detailWeeklyPlanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailWeeklyPlans->toArray(), 'Detail Weekly Plans retrieved successfully');
    }

    /**
     * Store a newly created DetailWeeklyPlan in storage.
     * POST /detailWeeklyPlans
     *
     * @param CreateDetailWeeklyPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailWeeklyPlanAPIRequest $request)
    {
        $input = $request->all();

        $detailWeeklyPlan = $this->detailWeeklyPlanRepository->create($input);

        return $this->sendResponse($detailWeeklyPlan->toArray(), 'Detail Weekly Plan saved successfully');
    }

    /**
     * Display the specified DetailWeeklyPlan.
     * GET|HEAD /detailWeeklyPlans/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailWeeklyPlan $detailWeeklyPlan */
        $detailWeeklyPlan = $this->detailWeeklyPlanRepository->find($id);

        if (empty($detailWeeklyPlan)) {
            return $this->sendError('Detail Weekly Plan not found');
        }

        return $this->sendResponse($detailWeeklyPlan->toArray(), 'Detail Weekly Plan retrieved successfully');
    }

    /**
     * Update the specified DetailWeeklyPlan in storage.
     * PUT/PATCH /detailWeeklyPlans/{id}
     *
     * @param int $id
     * @param UpdateDetailWeeklyPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailWeeklyPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetailWeeklyPlan $detailWeeklyPlan */
        $detailWeeklyPlan = $this->detailWeeklyPlanRepository->find($id);

        if (empty($detailWeeklyPlan)) {
            return $this->sendError('Detail Weekly Plan not found');
        }

        $detailWeeklyPlan = $this->detailWeeklyPlanRepository->update($input, $id);

        return $this->sendResponse($detailWeeklyPlan->toArray(), 'DetailWeeklyPlan updated successfully');
    }

    /**
     * Remove the specified DetailWeeklyPlan from storage.
     * DELETE /detailWeeklyPlans/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailWeeklyPlan $detailWeeklyPlan */
        $detailWeeklyPlan = $this->detailWeeklyPlanRepository->find($id);

        if (empty($detailWeeklyPlan)) {
            return $this->sendError('Detail Weekly Plan not found');
        }

        $detailWeeklyPlan->delete();

        return $this->sendSuccess('Detail Weekly Plan deleted successfully');
    }
}
