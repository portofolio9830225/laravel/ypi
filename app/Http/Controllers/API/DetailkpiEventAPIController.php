<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetailkpiEventAPIRequest;
use App\Http\Requests\API\UpdateDetailkpiEventAPIRequest;
use App\Models\DetailkpiEvent;
use App\Repositories\DetailkpiEventRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DetailkpiEventController
 * @package App\Http\Controllers\API
 */

class DetailkpiEventAPIController extends AppBaseController
{
    /** @var  DetailkpiEventRepository */
    private $detailkpiEventRepository;

    public function __construct(DetailkpiEventRepository $detailkpiEventRepo)
    {
        $this->detailkpiEventRepository = $detailkpiEventRepo;
    }

    /**
     * Display a listing of the DetailkpiEvent.
     * GET|HEAD /detailkpiEvents
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $detailkpiEvents = $this->detailkpiEventRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailkpiEvents->toArray(), 'Detailkpi Events retrieved successfully');
    }

    /**
     * Store a newly created DetailkpiEvent in storage.
     * POST /detailkpiEvents
     *
     * @param CreateDetailkpiEventAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailkpiEventAPIRequest $request)
    {
        $input = $request->all();

        $detailkpiEvent = $this->detailkpiEventRepository->create($input);

        return $this->sendResponse($detailkpiEvent->toArray(), 'Detailkpi Event saved successfully');
    }

    /**
     * Display the specified DetailkpiEvent.
     * GET|HEAD /detailkpiEvents/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailkpiEvent $detailkpiEvent */
        $detailkpiEvent = $this->detailkpiEventRepository->find($id);

        if (empty($detailkpiEvent)) {
            return $this->sendError('Detailkpi Event not found');
        }

        return $this->sendResponse($detailkpiEvent->toArray(), 'Detailkpi Event retrieved successfully');
    }

    /**
     * Update the specified DetailkpiEvent in storage.
     * PUT/PATCH /detailkpiEvents/{id}
     *
     * @param int $id
     * @param UpdateDetailkpiEventAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailkpiEventAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetailkpiEvent $detailkpiEvent */
        $detailkpiEvent = $this->detailkpiEventRepository->find($id);

        if (empty($detailkpiEvent)) {
            return $this->sendError('Detailkpi Event not found');
        }

        $detailkpiEvent = $this->detailkpiEventRepository->update($input, $id);

        return $this->sendResponse($detailkpiEvent->toArray(), 'DetailkpiEvent updated successfully');
    }

    /**
     * Remove the specified DetailkpiEvent from storage.
     * DELETE /detailkpiEvents/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailkpiEvent $detailkpiEvent */
        $detailkpiEvent = $this->detailkpiEventRepository->find($id);

        if (empty($detailkpiEvent)) {
            return $this->sendError('Detailkpi Event not found');
        }

        $detailkpiEvent->delete();

        return $this->sendSuccess('Detailkpi Event deleted successfully');
    }
}
