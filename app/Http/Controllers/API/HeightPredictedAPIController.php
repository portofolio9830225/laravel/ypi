<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHeightPredictedAPIRequest;
use App\Http\Requests\API\UpdateHeightPredictedAPIRequest;
use App\Models\HeightPredicted;
use App\Repositories\HeightPredictedRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\HeightData;
use App\Models\PubertyData;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class HeightPredictedController
 * @package App\Http\Controllers\API
 */

class HeightPredictedAPIController extends AppBaseController
{
    /** @var  HeightPredictedRepository */
    private $heightPredictedRepository;

    public function __construct(HeightPredictedRepository $heightPredictedRepo)
    {
        $this->heightPredictedRepository = $heightPredictedRepo;
    }

    /**
     * Display a listing of the HeightPredicted.
     * GET|HEAD /heightPredicteds
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $heightPredicteds = $this->heightPredictedRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
        if ($request->has('coach_id')) {
            $heightPredicteds = HeightPredicted::with('heightData', 'pubertyData', 'playerData')
                ->whereHas('playerData', function ($query) use ($request) {
                    return $query->where('master_pelatih_id', '=', $request->query('coach_id'));
                })->get();
            return $this->sendResponse($heightPredicteds->toArray(), 'Height Predicteds retrieved successfully');
        }

        $heightPredicteds = HeightPredicted::with('heightData', 'pubertyData', 'playerData')->get();

        return $this->sendResponse($heightPredicteds->toArray(), 'Height Predicteds retrieved successfully');
    }

    /**
     * Store a newly created HeightPredicted in storage.
     * POST /heightPredicteds
     *
     * @param CreateHeightPredictedAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHeightPredictedAPIRequest $request)
    {
        $input = $request->all();

        $heightPredicted = $this->heightPredictedRepository->create($input);

        return $this->sendResponse($heightPredicted->toArray(), 'Height Predicted saved successfully');
    }

    public function storeMaturationalStatus(Request $request)
    {
        $input = $request->all();

        try {
            DB::transaction(function () use ($input) {
                if (isset($input['heightPredicted']['id'])) {
                    $heightPredicted = HeightPredicted::find($input['heightPredicted']['id']);
                    $heightPredicted->fill($input['heightPredicted']);
                    $heightPredicted->save();

                    $heightData = HeightData::where('height_predicted_id', $heightPredicted->id)->first();
                    $heightData->fill($input['heightData']);
                    $heightData->save();

                    $pubertyData = PubertyData::where('height_predicted_id', $heightPredicted->id)->first();
                    $pubertyData->fill($input['pubertyData']);
                    $pubertyData->save();
                } else {
                    $newHeightPredicted = HeightPredicted::create($input['heightPredicted']);

                    $heightData = $input['heightData'];
                    $heightData['height_predicted_id'] = $newHeightPredicted->id;
                    HeightData::create($heightData);

                    $pubertyData = $input['pubertyData'];
                    $pubertyData['height_predicted_id'] = $newHeightPredicted->id;
                    PubertyData::create($pubertyData);
                }
            });
            return $this->sendResponse($input, 'Height Predicted saved successfully');
        } catch (\Throwable $th) {
            //throw $th;
            return $this->sendError($th);
        }
    }

    /**
     * Display the specified HeightPredicted.
     * GET|HEAD /heightPredicteds/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HeightPredicted $heightPredicted */
        $heightPredicted = $this->heightPredictedRepository->find($id);

        if (empty($heightPredicted)) {
            return $this->sendError('Height Predicted not found');
        }

        return $this->sendResponse($heightPredicted->toArray(), 'Height Predicted retrieved successfully');
    }

    /**
     * Update the specified HeightPredicted in storage.
     * PUT/PATCH /heightPredicteds/{id}
     *
     * @param int $id
     * @param UpdateHeightPredictedAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHeightPredictedAPIRequest $request)
    {
        $input = $request->all();

        /** @var HeightPredicted $heightPredicted */
        $heightPredicted = $this->heightPredictedRepository->find($id);

        if (empty($heightPredicted)) {
            return $this->sendError('Height Predicted not found');
        }

        $heightPredicted = $this->heightPredictedRepository->update($input, $id);

        return $this->sendResponse($heightPredicted->toArray(), 'HeightPredicted updated successfully');
    }

    /**
     * Remove the specified HeightPredicted from storage.
     * DELETE /heightPredicteds/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HeightPredicted $heightPredicted */
        $heightPredicted = $this->heightPredictedRepository->find($id);
        $heightData = HeightData::where('height_predicted_id', $heightPredicted->id)->first();
        $pubertyData = PubertyData::where('height_predicted_id', $heightPredicted->id)->first();

        if (empty($heightPredicted)) {
            return $this->sendError('Height Predicted not found');
        }

        $heightPredicted->delete();
        $heightData->delete();
        $pubertyData->delete();

        return $this->sendSuccess('Height Predicted deleted successfully');
    }
}
