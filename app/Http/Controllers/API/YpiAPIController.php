<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\DefaultTarget;
use App\Models\MasterYpi;
use App\Models\SeasonPlanner;
use App\Models\SeasonPlannerDetail;
use App\Models\YpiEvent;
use App\Models\YpiGoal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class YpiAPIController extends Controller
{
    public function store()
    {
        // return request();

        // Master YPI
        $master_ypi = MasterYpi::create([
            'keterangan'        => request()->keterangan,
            'kode_pelatih'      => request()->kode_pelatih,
            'voltarget'         => request()->voltarget,
            'tanggal_mulai'     => request()->tanggal_mulai,
            'tanggal_selesai'   => request()->tanggal_selesai,
            'default_intensity' => request()->default_intensity,
        ]);

        // default_target
        DefaultTarget::create([
            'master_ypi_id' => $master_ypi->id,
            'mon1' => request()->default_target['mon1'],
            'tue3' => request()->default_target['tue3'],
            'tue4' => request()->default_target['tue4'],
        ]);

        // ypi_goals
        if (count(request()->ypi_goals) != 0) {
            foreach (request()->ypi_goals as $item) {
                YpiGoal::create([
                    'master_ypi_id' => $master_ypi->id,
                    'goal'          => $item['goal']
                ]);
            }
        }

        // season_planners
        if (count(request()->season_planners) != 0) {
            foreach (request()->season_planners as $item) {
                if (isset($item['active'])) {
                    $season_planner = SeasonPlanner::create([
                        'master_ypi_id'     => $master_ypi->id,
                        'background_color'  => $item['background_color'],
                        'name'              => $item['name'],
                    ]);

                    // season_planner_details
                    foreach ($item['season_planner_details'] as $val) {
                        $data = [];
                        $data['master_ypi_id'] = $master_ypi->id;
                        $data['season_planner_id'] = $season_planner->id;
                        $data['type'] = $val['type'];
                        $data['name'] = $val['name'];

                        if (isset($val['inChart'])) {
                            $data['inChart']        = $val['inChart'];
                        }

                        if (isset($val['ytp_charts_id'])) {
                            $data['ytp_charts_id']  = $val['ytp_charts_id'];
                        }

                        if (isset($val['chartType'])) {
                            $data['chartType']     = $val['chartType'];
                        }

                        if (isset($val['chartColor'])) {
                            $data['chartColor']     = $val['chartColor'];
                        }

                        SeasonPlannerDetail::create($data);
                    }
                }
            }
        }

        // events
        if (count(request()->events) != 0) {
            foreach (request()->events as $item) {

                if (isset($item['checked'])) {
                    YpiEvent::create([
                        'master_ypi_id'             => $master_ypi->id,
                        'master_achievement_id'     => $item['id'],
                    ]);
                }
            }
        }

        return response()->json([
            'message'   => 'Create YPI Success',
            'data'      => $master_ypi
        ]);
    }

    public function destroy($id)
    {
        // season planer
        $season_planner = SeasonPlanner::where('master_ypi_id', $id)->first();

        // season planer detail
        SeasonPlannerDetail::where('master_ypi_id', $id)->where('season_planner_id', $season_planner->id)->delete();
        $season_planner->delete();

        // ypi goals
        YpiGoal::where('master_ypi_id', $id)->delete();

        // default targets
        DefaultTarget::where('master_ypi_id', $id)->delete();

        // ypi events
        YpiEvent::where('master_ypi_id', $id)->delete();

        // master ypi
        MasterYpi::destroy($id);

        return response()->json('Delete YPI Success');
    }
}
