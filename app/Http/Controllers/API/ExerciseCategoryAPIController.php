<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateExerciseCategoryAPIRequest;
use App\Http\Requests\API\UpdateExerciseCategoryAPIRequest;
use App\Models\ExerciseCategory;
use App\Repositories\ExerciseCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ExerciseCategoryController
 * @package App\Http\Controllers\API
 */

class ExerciseCategoryAPIController extends AppBaseController
{
    /** @var  ExerciseCategoryRepository */
    private $exerciseCategoryRepository;

    public function __construct(ExerciseCategoryRepository $exerciseCategoryRepo)
    {
        $this->exerciseCategoryRepository = $exerciseCategoryRepo;
    }

    /**
     * Display a listing of the ExerciseCategory.
     * GET|HEAD /exerciseCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $exerciseCategories = $this->exerciseCategoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($exerciseCategories->toArray(), 'Exercise Categories retrieved successfully');
    }

    /**
     * Store a newly created ExerciseCategory in storage.
     * POST /exerciseCategories
     *
     * @param CreateExerciseCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateExerciseCategoryAPIRequest $request)
    {
        $input = $request->all();

        $exerciseCategory = $this->exerciseCategoryRepository->create($input);

        return $this->sendResponse($exerciseCategory->toArray(), 'Exercise Category saved successfully');
    }

    /**
     * Display the specified ExerciseCategory.
     * GET|HEAD /exerciseCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ExerciseCategory $exerciseCategory */
        $exerciseCategory = $this->exerciseCategoryRepository->find($id);

        if (empty($exerciseCategory)) {
            return $this->sendError('Exercise Category not found');
        }

        return $this->sendResponse($exerciseCategory->toArray(), 'Exercise Category retrieved successfully');
    }

    /**
     * Update the specified ExerciseCategory in storage.
     * PUT/PATCH /exerciseCategories/{id}
     *
     * @param int $id
     * @param UpdateExerciseCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateExerciseCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var ExerciseCategory $exerciseCategory */
        $exerciseCategory = $this->exerciseCategoryRepository->find($id);

        if (empty($exerciseCategory)) {
            return $this->sendError('Exercise Category not found');
        }

        $exerciseCategory = $this->exerciseCategoryRepository->update($input, $id);

        return $this->sendResponse($exerciseCategory->toArray(), 'ExerciseCategory updated successfully');
    }

    /**
     * Remove the specified ExerciseCategory from storage.
     * DELETE /exerciseCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ExerciseCategory $exerciseCategory */
        $exerciseCategory = $this->exerciseCategoryRepository->find($id);

        if (empty($exerciseCategory)) {
            return $this->sendError('Exercise Category not found');
        }

        $exerciseCategory->delete();

        return $this->sendSuccess('Exercise Category deleted successfully');
    }
}
