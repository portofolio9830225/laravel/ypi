<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKpiSubindicatorAPIRequest;
use App\Http\Requests\API\UpdateKpiSubindicatorAPIRequest;
use App\Models\KpiSubindicator;
use App\Repositories\KpiSubindicatorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class KpiSubindicatorController
 * @package App\Http\Controllers\API
 */

class KpiSubindicatorAPIController extends AppBaseController
{
    /** @var  KpiSubindicatorRepository */
    private $kpiSubindicatorRepository;

    public function __construct(KpiSubindicatorRepository $kpiSubindicatorRepo)
    {
        $this->kpiSubindicatorRepository = $kpiSubindicatorRepo;
    }

    /**
     * Display a listing of the KpiSubindicator.
     * GET|HEAD /kpiSubindicators
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $kpiSubindicators = $this->kpiSubindicatorRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($kpiSubindicators->toArray(), 'Kpi Subindicators retrieved successfully');
    }

    /**
     * Store a newly created KpiSubindicator in storage.
     * POST /kpiSubindicators
     *
     * @param CreateKpiSubindicatorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateKpiSubindicatorAPIRequest $request)
    {
        $input = $request->all();

        $kpiSubindicator = $this->kpiSubindicatorRepository->create($input);

        return $this->sendResponse($kpiSubindicator->toArray(), 'Kpi Subindicator saved successfully');
    }

    /**
     * Display the specified KpiSubindicator.
     * GET|HEAD /kpiSubindicators/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var KpiSubindicator $kpiSubindicator */
        $kpiSubindicator = $this->kpiSubindicatorRepository->find($id);

        if (empty($kpiSubindicator)) {
            return $this->sendError('Kpi Subindicator not found');
        }

        return $this->sendResponse($kpiSubindicator->toArray(), 'Kpi Subindicator retrieved successfully');
    }

    /**
     * Update the specified KpiSubindicator in storage.
     * PUT/PATCH /kpiSubindicators/{id}
     *
     * @param int $id
     * @param UpdateKpiSubindicatorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKpiSubindicatorAPIRequest $request)
    {
        $input = $request->all();

        /** @var KpiSubindicator $kpiSubindicator */
        $kpiSubindicator = $this->kpiSubindicatorRepository->find($id);

        if (empty($kpiSubindicator)) {
            return $this->sendError('Kpi Subindicator not found');
        }

        $kpiSubindicator = $this->kpiSubindicatorRepository->update($input, $id);

        return $this->sendResponse($kpiSubindicator->toArray(), 'KpiSubindicator updated successfully');
    }

    /**
     * Remove the specified KpiSubindicator from storage.
     * DELETE /kpiSubindicators/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var KpiSubindicator $kpiSubindicator */
        $kpiSubindicator = $this->kpiSubindicatorRepository->find($id);

        if (empty($kpiSubindicator)) {
            return $this->sendError('Kpi Subindicator not found');
        }

        $kpiSubindicator->delete();

        return $this->sendSuccess('Kpi Subindicator deleted successfully');
    }
}
