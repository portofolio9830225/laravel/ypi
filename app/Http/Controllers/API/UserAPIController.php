<?php

namespace App\Http\Controllers\API;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class UserAPIController extends AppBaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('club:id,nama', 'asCoach:id,nama_lengkap')->get()->toArray();
        return $this->sendResponse($users, 'User retrived successfully');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        $user = null;
        if ($input['id'] != 'undefined' && $request->has('id')) {
            $id = $input['id'];
            Log::info('id ada', [$id]);
            $user = User::find($id);
        } else {
            $user = new User;
            $id = $user->id;
            Log::info('id gak ada', [$id]);
        }

        if ($request->hasFile('foto')) {
            $foto = $request->file('foto');
            $fileName = 'pic_' . $id . '.' . $foto->getClientOriginalExtension();
            $foto->move(public_path('avatars'), $fileName);

            $user->fill($input);

            if (isset($input['password'])) {
                $user->password = Hash::make($input['password']);
            }
            $user->foto = url('/') . '/avatars/' . $fileName;
            $user->save();
            return $this->sendResponse($user, 'User saved successfully');
        }
        unset($input['foto']);
        $user->fill($input);
        if (isset($input['password'])) {
            $user->password = Hash::make($input['password']);
        }
        $user->save();
        return $this->sendResponse($user, 'User saved successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return response()->json([
            'message'   => 'Show User',
            'data'      => $user
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return $this->sendSuccess('Success delete user');
    }

    public function updateProfile(Request $request)
    {
    }
}
