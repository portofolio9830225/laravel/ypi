<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterAchievementAPIRequest;
use App\Http\Requests\API\UpdateMasterAchievementAPIRequest;
use App\Models\MasterAchievement;
use App\Repositories\MasterAchievementRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\CompetitionGroup;
use App\Models\TabelKelompok;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class MasterAchievementController
 * @package App\Http\Controllers\API
 */

class MasterAchievementAPIController extends AppBaseController
{
    /** @var  MasterAchievementRepository */
    private $masterAchievementRepository;

    public function __construct(MasterAchievementRepository $masterAchievementRepo)
    {
        $this->masterAchievementRepository = $masterAchievementRepo;
    }

    /**
     * Display a listing of the MasterAchievement.
     * GET|HEAD /masterAchievements
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('start') != null && $request->query('end') != null) {
            $masterAchievements = MasterAchievement::where('tanggal_mulai', '>=', $request->query('start'))
                ->where('tanggal_akhir', '<=', $request->query('end'))
                ->get();

            return $this->sendResponse($masterAchievements->toArray(), 'Master Achievements retrieved successfully');
        }

        $masterAchievements = MasterAchievement::with('competitionInfo', 'competitionStatus', 'competitionGroup')->get()->toArray();
        foreach ($masterAchievements as $key => $value) {
            if (isset($value['competition_group']['groups_id']) && $value['competition_group']['groups_id'] !== '') {
                $groupsId = array_map('intval', explode(',', $value['competition_group']['groups_id']));
                $masterAchievements[$key]['groups_id'] = $groupsId;
                $groupClassifications = [];
                foreach ($groupsId as $k => $v) {
                    $groupClassification = TabelKelompok::where('id', $v)->first();
                    if (isset($groupClassification))
                        $groupClassifications[] = $groupClassification->kelompok;
                }
                $masterAchievements[$key]['group_classifications'] = implode(', ', $groupClassifications);
            } else {
                $masterAchievements[$key]['group_classifications'] = '';
            }
        }

        return $this->sendResponse($masterAchievements, 'Master Achievements retrieved successfully');
    }

    /**
     * Store a newly created MasterAchievement in storage.
     * POST /masterAchievements
     *
     * @param CreateMasterAchievementAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMasterAchievementAPIRequest $request)
    {
        $input = $request->all();

        $competition = MasterAchievement::create($input);
        $groups_id = $input['groups_id'];
        $groupsId = '';
        foreach ($groups_id as $key => $value) {
            if ($key !== (count($groups_id) - 1)) {
                $groupsId .= $value . ',';
            } else {
                $groupsId .= $value;
            }
        }
        CompetitionGroup::create([
            'competition_id' => $competition->id,
            'groups_id' => $groupsId,
        ]);

        return $this->sendResponse($input, 'Master Achievement saved successfully');
    }

    /**
     * Display the specified MasterAchievement.
     * GET|HEAD /masterAchievements/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MasterAchievement $masterAchievement */
        $masterAchievement = $this->masterAchievementRepository->find($id);

        if (empty($masterAchievement)) {
            return $this->sendError('Master Achievement not found');
        }

        return $this->sendResponse($masterAchievement->toArray(), 'Master Achievement retrieved successfully');
    }

    /**
     * Update the specified MasterAchievement in storage.
     * PUT/PATCH /masterAchievements/{id}
     *
     * @param int $id
     * @param UpdateMasterAchievementAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var MasterAchievement $masterAchievement */
        // $masterAchievement = $this->masterAchievementRepository->find($id);

        // if (empty($masterAchievement)) {
        //     return $this->sendError('Master Achievement not found');
        // }

        $masterAchievement = MasterAchievement::where('id', $id)->first();
        $masterAchievement->fill($input);
        $masterAchievement->save();

        $groups_id = $input['groups_id'];
        $groupsId = '';
        foreach ($groups_id as $key => $value) {
            if ($key !== (count($groups_id) - 1)) {
                $groupsId .= $value . ',';
            } else {
                $groupsId .= $value;
            }
        }
        $competitionGroup = CompetitionGroup::where('competition_id', $id)->first();
        if (isset($competitionGroup)) {
            $competitionGroup->groups_id = $groupsId;
            $competitionGroup->save();
        } else {
            CompetitionGroup::create([
                'competition_id' => $id,
                'groups_id' => $groupsId,
            ]);
        }

        return $this->sendResponse(['success'], 'MasterAchievement updated successfully');
    }

    /**
     * Remove the specified MasterAchievement from storage.
     * DELETE /masterAchievements/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MasterAchievement $masterAchievement */
        $masterAchievement = $this->masterAchievementRepository->find($id);

        if (empty($masterAchievement)) {
            return $this->sendError('Master Achievement not found');
        }

        $masterAchievement->delete();

        return $this->sendSuccess('Master Achievement deleted successfully');
    }
}
