<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeasonPlannerDetailValueAPIRequest;
use App\Http\Requests\API\UpdateSeasonPlannerDetailValueAPIRequest;
use App\Models\SeasonPlannerDetailValue;
use App\Repositories\SeasonPlannerDetailValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\SeasonPlanner;
use App\Models\SeasonPlannerDetail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class SeasonPlannerDetailValueController
 * @package App\Http\Controllers\API
 */

class SeasonPlannerDetailValueAPIController extends AppBaseController
{
    /** @var  SeasonPlannerDetailValueRepository */
    private $seasonPlannerDetailValueRepository;

    public function __construct(SeasonPlannerDetailValueRepository $seasonPlannerDetailValueRepo)
    {
        $this->seasonPlannerDetailValueRepository = $seasonPlannerDetailValueRepo;
    }

    /**
     * Display a listing of the SeasonPlannerDetailValue.
     * GET|HEAD /seasonPlannerDetailValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seasonPlannerDetailValues = $this->seasonPlannerDetailValueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seasonPlannerDetailValues->toArray(), 'Season Planner Detail Values retrieved successfully');
    }

    public function valuesPerWeek(Request $request)
    {
        $query = $request->all();

        if (isset($query['master_ypi_id'], $query['season_planner_name'], $query['week_index'])) {
            $seasonPlanner = SeasonPlanner::where('master_ypi_id', $query['master_ypi_id'])
                ->where('name', $query['season_planner_name'])
                ->first();

            $perWeek = SeasonPlannerDetailValue::where('master_ypi_id', $query['master_ypi_id'])
                ->where('season_planner_id', $seasonPlanner->id)
                ->where('week_index',  $query['week_index'])
                ->get();

            $perWeekArray = $perWeek->toArray();
            $perWeekString = '';
            if (count($perWeekArray) > 0) {
                foreach ($perWeekArray as $key => $value) {
                    if ($key !== (count($perWeekArray) - 1)) {
                        $perWeekString = $perWeekString . $value['value'] . ', ';
                    } else {
                        $perWeekString = $perWeekString . $value['value'];
                    }
                }
            }
            return $this->sendResponse($perWeekString, 'Season Planner Detail Values retrieved successfully');
        }
        return $this->sendError('Internal Server Error');
    }

    public function valuePerWeek(Request $request)
    {
        $query = $request->all();

        if (isset($query['master_ypi_id'], $query['season_planner_name'], $query['season_planner_detail_name'], $query['week_index'])) {
            $seasonPlanner = SeasonPlanner::where('master_ypi_id', $query['master_ypi_id'])
                ->where('name', $query['season_planner_name'])
                ->first();

            $seasonPlannerDetail = SeasonPlannerDetail::where('master_ypi_id', $query['master_ypi_id'])
                ->where('season_planner_id', $seasonPlanner->id)
                ->where('name', $query['season_planner_detail_name'])
                ->first();

            $phasePeriodization = SeasonPlannerDetailValue::where('master_ypi_id', $query['master_ypi_id'])
                ->where('season_planner_id', $seasonPlanner->id)
                ->where('season_planner_detail_id',  $seasonPlannerDetail->id)
                ->where('week_index',  $query['week_index'])
                ->first();

            $value = null;
            if (isset($phasePeriodization['value'])) {
                $value = $phasePeriodization['value'];
            }

            return $this->sendResponse($value, 'Season Planner Detail Values retrieved successfully');
        }
        return $this->sendResponse($query, 'Season Planner Detail Values retrieved successfully');
        // return $this->sendError('Internal Server Error');
    }

    /**
     * Store a newly created SeasonPlannerDetailValue in storage.
     * POST /seasonPlannerDetailValues
     *
     * @param CreateSeasonPlannerDetailValueAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->input();
        $locale = $request->query('locale');

        DB::transaction(function () use ($input, $locale) {
            foreach ($input as $key => $value) {
                if (isset($value['seasonPlannerDetailDescription'])) {
                    $spd = SeasonPlannerDetail::find($value['seasonPlannerDetailId']);
                    if ($locale === 'en-US') $spd->desc_en = $value['seasonPlannerDetailDescription'];
                    else $spd->desc_id = $value['seasonPlannerDetailDescription'];
                    $spd->save();
                }
                if (gettype($value) === 'string') continue;
                foreach ($value as $k => $v) {
                    if (isset($v) && str_starts_with($k, 'week_')) {
                        $data = [
                            'master_ypi_id' => $value['masterYpiId'],
                            'season_planner_id' => $value['seasonPlannerId'],
                            'season_planner_detail_id' => $value['seasonPlannerDetailId'],
                            'week_index' => (int) str_replace('week_', '', $k),
                            'value' => $v,
                        ];
                        SeasonPlannerDetailValue::updateOrCreate(
                            [
                                'master_ypi_id' => $value['masterYpiId'],
                                'season_planner_id' => $value['seasonPlannerId'],
                                'season_planner_detail_id' => $value['seasonPlannerDetailId'],
                                'week_index' => (int) str_replace('week_', '', $k)
                            ],
                            $data
                        );
                    }
                    if (str_starts_with($k, 'week_') && !isset($v)) {
                        $deletedData = SeasonPlannerDetailValue::where('master_ypi_id', $value['masterYpiId'])
                            ->where('season_planner_id', $value['seasonPlannerId'])
                            ->where('season_planner_detail_id', $value['seasonPlannerDetailId'])
                            ->where('week_index', (int) str_replace('week_', '', $k))
                            ->first();
                        if (isset($deletedData)) {
                            $deletedData->delete();
                        }
                    }
                }
            }
        });


        return $this->sendResponse(['success'], 'Season Planner Detail Value saved successfully');
    }

    /**
     * Display the specified SeasonPlannerDetailValue.
     * GET|HEAD /seasonPlannerDetailValues/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SeasonPlannerDetailValue $seasonPlannerDetailValue */
        $seasonPlannerDetailValue = $this->seasonPlannerDetailValueRepository->find($id);

        if (empty($seasonPlannerDetailValue)) {
            return $this->sendError('Season Planner Detail Value not found');
        }

        return $this->sendResponse($seasonPlannerDetailValue->toArray(), 'Season Planner Detail Value retrieved successfully');
    }

    /**
     * Update the specified SeasonPlannerDetailValue in storage.
     * PUT/PATCH /seasonPlannerDetailValues/{id}
     *
     * @param int $id
     * @param UpdateSeasonPlannerDetailValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeasonPlannerDetailValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeasonPlannerDetailValue $seasonPlannerDetailValue */
        $seasonPlannerDetailValue = $this->seasonPlannerDetailValueRepository->find($id);

        if (empty($seasonPlannerDetailValue)) {
            return $this->sendError('Season Planner Detail Value not found');
        }

        $seasonPlannerDetailValue = $this->seasonPlannerDetailValueRepository->update($input, $id);

        return $this->sendResponse($seasonPlannerDetailValue->toArray(), 'SeasonPlannerDetailValue updated successfully');
    }

    /**
     * Remove the specified SeasonPlannerDetailValue from storage.
     * DELETE /seasonPlannerDetailValues/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SeasonPlannerDetailValue $seasonPlannerDetailValue */
        $seasonPlannerDetailValue = $this->seasonPlannerDetailValueRepository->find($id);

        if (empty($seasonPlannerDetailValue)) {
            return $this->sendError('Season Planner Detail Value not found');
        }

        $seasonPlannerDetailValue->delete();

        return $this->sendSuccess('Season Planner Detail Value deleted successfully');
    }
}
