<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\GapAnalysist;
use App\Models\MasterAchievement;
use App\Models\MasterPemain;
use Illuminate\Http\Request;

class GapAnalysistAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'status'    => 'SUCCESS',
            'data'      => GapAnalysist::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return request();

        $player = MasterPemain::find(request()->player_id);
        if ($player == '') {
            return response()->json([
                'status'    => 'ERROR',
                'data'      => 'Player Nothing'
            ]);
        }

        $tournament = MasterAchievement::find(request()->tournament_id);
        if ($tournament == '') {
            return response()->json([
                'status'    => 'ERROR',
                'data'      => 'Tournamet Nothing'
            ]);
        }

        $gap = GapAnalysist::create(request()->all());
        return response()->json([
            'status'    => 'SUCCESS',
            'data'      => $gap
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'status'    => 'SUCCESS',
            'data'      => GapAnalysist::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $player = MasterPemain::find(request()->player_id);
        if ($player == '') {
            return response()->json([
                'status'    => 'ERROR',
                'data'      => 'Player Nothing'
            ]);
        }

        $tournament = MasterAchievement::find(request()->tournament_id);
        if ($tournament == '') {
            return response()->json([
                'status'    => 'ERROR',
                'data'      => 'Tournamet Nothing'
            ]);
        }

        $gap = GapAnalysist::find($id)->update(request()->all());
        return response()->json([
            'status'    => 'SUCCESS',
            'data'      => $gap
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GapAnalysist::destroy($id);

        return response()->json([
            'status'    => 'SUCCESS',
        ]);
    }
}
