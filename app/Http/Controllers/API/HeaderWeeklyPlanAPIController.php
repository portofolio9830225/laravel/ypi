<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHeaderWeeklyPlanAPIRequest;
use App\Http\Requests\API\UpdateHeaderWeeklyPlanAPIRequest;
use App\Models\HeaderWeeklyPlan;
use App\Repositories\HeaderWeeklyPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Constant;
use App\Models\DetailWeeklyPlan;
use App\Models\SeasonPlanValue;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class HeaderWeeklyPlanController
 * @package App\Http\Controllers\API
 */

class HeaderWeeklyPlanAPIController extends AppBaseController
{
    /** @var  HeaderWeeklyPlanRepository */
    private $headerWeeklyPlanRepository;

    public function __construct(HeaderWeeklyPlanRepository $headerWeeklyPlanRepo)
    {
        $this->headerWeeklyPlanRepository = $headerWeeklyPlanRepo;
    }

    /**
     * Display a listing of the HeaderWeeklyPlan.
     * GET|HEAD /headerWeeklyPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('master_ypi_id') != null && $request->query('minggu') != null) {
            $headerWeeklyPlans = HeaderWeeklyPlan::where('master_ypi_id', $request->query('master_ypi_id'))
                ->where('minggu', $request->query('minggu'))->first();

            if (isset($headerWeeklyPlans)) {
                $detailsWtp = DetailWeeklyPlan::where('master_ypi_id', $request->query('master_ypi_id'))
                    ->where('minggu', $request->query('minggu'))->get();

                $headerWeeklyPlans['detailsWtp'] = $detailsWtp->toArray();
            } else {
                return $this->sendResponse(null, 'Header Weekly Plans retrieved successfully');
            }
            return $this->sendResponse($headerWeeklyPlans->toArray(), 'Header Weekly Plans retrieved successfully');
        }

        $headerWeeklyPlans = $this->headerWeeklyPlanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($headerWeeklyPlans->toArray(), 'Header Weekly Plans retrieved successfully');
    }

    /**
     * Store a newly created HeaderWeeklyPlan in storage.
     * POST /headerWeeklyPlans
     *
     * @param CreateHeaderWeeklyPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHeaderWeeklyPlanAPIRequest $request)
    {
        $input = $request->all();

        $headerWeeklyPlan = $this->headerWeeklyPlanRepository->create($input);

        return $this->sendResponse($headerWeeklyPlan->toArray(), 'Header Weekly Plan saved successfully');
    }

    public function withDetails(Request $request)
    {
        $input = $request->all();

        DB::transaction(function () use ($input) {
            HeaderWeeklyPlan::updateOrCreate([
                'master_ypi_id' => $input['master_ypi_id'],
                'minggu' => $input['minggu'],
            ], $input);

            $detailsWtp = $input['detailsWtp'];
            foreach ($detailsWtp as $key => $value) {
                if (isset($value['id'])) {
                    $detailWtp = DetailWeeklyPlan::find($value['id']);
                    $detailWtp->fill($value);
                    $detailWtp->save();
                } else {
                    if (empty($value['emphesis_goal'])) $value['emphesis_goal'] = '';
                    if (empty($value['pace'])) $value['pace'] = '';
                    DetailWeeklyPlan::create($value);
                }
            }

            $deletedDetailsWtp = $input['deletedDetailsWtp'];
            foreach ($deletedDetailsWtp as $key => $value) {
                DetailWeeklyPlan::where('id', $value['id'])->delete();
            }
        });

        return $this->sendResponse(['success'], 'Header Weekly Plan saved successfully');
    }

    /**
     * Display the specified HeaderWeeklyPlan.
     * GET|HEAD /headerWeeklyPlans/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HeaderWeeklyPlan $headerWeeklyPlan */
        $headerWeeklyPlan = $this->headerWeeklyPlanRepository->find($id);

        if (empty($headerWeeklyPlan)) {
            return $this->sendError('Header Weekly Plan not found');
        }

        return $this->sendResponse($headerWeeklyPlan->toArray(), 'Header Weekly Plan retrieved successfully');
    }

    public function relatedData(Request $request)
    {
        if ($request->has('ypi_id') && $request->has('week')) {
            $ypiId = $request->query('ypi_id');
            $week = $request->query('week');

            $spdTrainingPhaseConstant = Constant::where('key', 'SPD_TRAINING_PHASE_ID')->first();
            $spdTrainingPhaseId = (int) $spdTrainingPhaseConstant->value;
            $spTechDevConstant = Constant::where('key', 'SP_TECH_DEV_ID')->first();
            $spTechDevId = (int) $spTechDevConstant->value;
            $spTacticConstant = Constant::where('key', 'SP_TACTIC_ID')->first();
            $spTacticId = (int) $spTacticConstant->value;
            $spPhysicalConstant = Constant::where('key', 'SP_PHYSICAL_DEV_ID')->first();
            $spPhysicId = (int) $spPhysicalConstant->value;
            $spMentalConstant = Constant::where('key', 'SP_MENTAL_ID')->first();
            $spMentalId = (int) $spMentalConstant->value;
            $spdIntensityConstant = Constant::where('key', 'SPD_INTENSITY_ID')->first();
            $spdIntensityId = (int) $spdIntensityConstant->value;
            $spdVolumeConstant = Constant::where('key', 'SPD_VOLUME_ID')->first();
            $spdVolumeId = (int) $spdVolumeConstant->value;
            $spdSpesificConstant = Constant::where('key', 'SPD_SPESIFIC_ID')->first();
            $spdSpesificId = (int) $spdSpesificConstant->value;
            $spdNoHourWeekConstant = Constant::where('key', 'SPD_NO_HOUR_WEEK_ID')->first();
            $spdNoHourWeekId = (int) $spdNoHourWeekConstant->value;
            $spdTotalHourWeekConstant = Constant::where('key', 'SPD_TOTAL_HOUR_WEEK_ID')->first();
            $spdTotalHourWeekId = (int) $spdTotalHourWeekConstant->value;

            $spvTrainingPhase = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_detail_id', $spdTrainingPhaseId)->first();
            $trainingPhase = $spvTrainingPhase['week_' . $week];
            $spvIntensity = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_detail_id', $spdIntensityId)->first();
            $intensity = $spvIntensity['week_' . $week];
            $spvVolume = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_detail_id', $spdVolumeId)->first();
            $volume = $spvVolume['week_' . $week];
            $spvSpesific = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_detail_id', $spdSpesificId)->first();
            $spesific = $spvSpesific['week_' . $week];
            $spvNoHourWeek = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_detail_id', $spdNoHourWeekId)->first();
            $noHourWeek = $spvNoHourWeek['week_' . $week];
            $spvTotalHourWeek = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_detail_id', $spdTotalHourWeekId)->first();
            $totalNoHourWeek = $spvTotalHourWeek['week_' . $week];

            $spvTechDevs = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_id', $spTechDevId)->get();
            $techDevs = '';
            foreach ($spvTechDevs as $key => $value) {
                if ($value['week_' . $week] === '') continue;
                if ($key === (count($spvTechDevs) - 1)) {
                    $techDevs = $techDevs . $value['week_' . $week];
                } else {
                    $techDevs = $techDevs . $value['week_' . $week] . ',';
                }
            }
            $spvTactic = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_id', $spTacticId)->get();
            $tactics = '';
            foreach ($spvTactic as $key => $value) {
                if ($value['week_' . $week] === '') continue;
                if ($key === (count($spvTactic) - 1)) {
                    $tactics = $tactics . $value['week_' . $week];
                } else {
                    $tactics = $tactics . $value['week_' . $week] . ',';
                }
            }
            $spvTactic = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_id', $spPhysicId)->get();
            $physics = '';
            foreach ($spvTactic as $key => $value) {
                if ($value['week_' . $week] === '') continue;
                if ($key === (count($spvTactic) - 1)) {
                    $physics = $physics . $value['week_' . $week];
                } else {
                    $physics = $physics . $value['week_' . $week] . ',';
                }
            }
            $spvMental = SeasonPlanValue::where('ypi_id', $ypiId)->where('season_plan_id', $spMentalId)->get();
            $mentals = '';
            foreach ($spvMental as $key => $value) {
                if ($value['week_' . $week] === '') continue;
                if ($key === (count($spvMental) - 1)) {
                    $mentals = $mentals . $value['week_' . $week];
                } else {
                    $mentals = $mentals . $value['week_' . $week] . ',';
                }
            }

            $data = [
                'phase_periodization' => $trainingPhase,
                'tech_devs' => $techDevs,
                'tactics' => $tactics,
                'physics' => $physics,
                'mentals' => $mentals,
                'intensity' => $intensity,
                'volume' => $volume,
                'spesific' => $spesific,
                'no_hour_week' => $noHourWeek,
                'total_no_hour_week' => $totalNoHourWeek,
            ];

            return $this->sendResponse($data, 'Header Weekly Plan retrieved successfully');
        }


        return $this->sendResponse([], 'Not Found!');
    }

    /**
     * Update the specified HeaderWeeklyPlan in storage.
     * PUT/PATCH /headerWeeklyPlans/{id}
     *
     * @param int $id
     * @param UpdateHeaderWeeklyPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHeaderWeeklyPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var HeaderWeeklyPlan $headerWeeklyPlan */
        $headerWeeklyPlan = $this->headerWeeklyPlanRepository->find($id);

        if (empty($headerWeeklyPlan)) {
            return $this->sendError('Header Weekly Plan not found');
        }

        $headerWeeklyPlan = $this->headerWeeklyPlanRepository->update($input, $id);

        return $this->sendResponse($headerWeeklyPlan->toArray(), 'HeaderWeeklyPlan updated successfully');
    }

    /**
     * Remove the specified HeaderWeeklyPlan from storage.
     * DELETE /headerWeeklyPlans/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $headerData = HeaderWeeklyPlan::find($id);
            if (empty($headerData)) {
                return $this->sendError('Header Weekly Plan not found');
            }

            DetailWeeklyPlan::where('master_ypi_id', $headerData->master_ypi_id)
                ->where('minggu', $headerData->minggu)->delete();

            $headerData->delete();
        });

        return $this->sendSuccess('Header Weekly Plan deleted successfully');
    }
}
