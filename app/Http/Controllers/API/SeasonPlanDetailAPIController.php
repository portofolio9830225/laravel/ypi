<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeasonPlanDetailAPIRequest;
use App\Http\Requests\API\UpdateSeasonPlanDetailAPIRequest;
use App\Models\SeasonPlanDetail;
use App\Repositories\SeasonPlanDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SeasonPlanDetailController
 * @package App\Http\Controllers\API
 */

class SeasonPlanDetailAPIController extends AppBaseController
{
    /** @var  SeasonPlanDetailRepository */
    private $seasonPlanDetailRepository;

    public function __construct(SeasonPlanDetailRepository $seasonPlanDetailRepo)
    {
        $this->seasonPlanDetailRepository = $seasonPlanDetailRepo;
    }

    /**
     * Display a listing of the SeasonPlanDetail.
     * GET|HEAD /seasonPlanDetails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seasonPlanDetails = $this->seasonPlanDetailRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seasonPlanDetails->toArray(), 'Season Plan Details retrieved successfully');
    }

    /**
     * Store a newly created SeasonPlanDetail in storage.
     * POST /seasonPlanDetails
     *
     * @param CreateSeasonPlanDetailAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSeasonPlanDetailAPIRequest $request)
    {
        $input = $request->all();

        $seasonPlanDetail = $this->seasonPlanDetailRepository->create($input);

        return $this->sendResponse($seasonPlanDetail->toArray(), 'Season Plan Detail saved successfully');
    }

    /**
     * Display the specified SeasonPlanDetail.
     * GET|HEAD /seasonPlanDetails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SeasonPlanDetail $seasonPlanDetail */
        $seasonPlanDetail = $this->seasonPlanDetailRepository->find($id);

        if (empty($seasonPlanDetail)) {
            return $this->sendError('Season Plan Detail not found');
        }

        return $this->sendResponse($seasonPlanDetail->toArray(), 'Season Plan Detail retrieved successfully');
    }

    /**
     * Update the specified SeasonPlanDetail in storage.
     * PUT/PATCH /seasonPlanDetails/{id}
     *
     * @param int $id
     * @param UpdateSeasonPlanDetailAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeasonPlanDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeasonPlanDetail $seasonPlanDetail */
        $seasonPlanDetail = $this->seasonPlanDetailRepository->find($id);

        if (empty($seasonPlanDetail)) {
            return $this->sendError('Season Plan Detail not found');
        }

        $seasonPlanDetail = $this->seasonPlanDetailRepository->update($input, $id);

        return $this->sendResponse($seasonPlanDetail->toArray(), 'SeasonPlanDetail updated successfully');
    }

    /**
     * Remove the specified SeasonPlanDetail from storage.
     * DELETE /seasonPlanDetails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SeasonPlanDetail $seasonPlanDetail */
        $seasonPlanDetail = $this->seasonPlanDetailRepository->find($id);

        if (empty($seasonPlanDetail)) {
            return $this->sendError('Season Plan Detail not found');
        }

        $seasonPlanDetail->delete();

        return $this->sendSuccess('Season Plan Detail deleted successfully');
    }
}
