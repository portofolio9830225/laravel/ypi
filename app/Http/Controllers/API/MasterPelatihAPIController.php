<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterPelatihAPIRequest;
use App\Http\Requests\API\UpdateMasterPelatihAPIRequest;
use App\Models\MasterPelatih;
use App\Repositories\MasterPelatihRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MasterPelatihController
 * @package App\Http\Controllers\API
 */

class MasterPelatihAPIController extends AppBaseController
{
    /** @var  MasterPelatihRepository */
    private $masterPelatihRepository;

    public function __construct(MasterPelatihRepository $masterPelatihRepo)
    {
        $this->masterPelatihRepository = $masterPelatihRepo;
    }

    /**
     * Display a listing of the MasterPelatih.
     * GET|HEAD /masterPelatihs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $masterPelatihs = $this->masterPelatihRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($masterPelatihs->toArray(), 'Master Pelatihs retrieved successfully');
    }

    /**
     * Store a newly created MasterPelatih in storage.
     * POST /masterPelatihs
     *
     * @param CreateMasterPelatihAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $masterPelatih = $this->masterPelatihRepository->create($input);

        return $this->sendResponse($masterPelatih->toArray(), 'Master Pelatih saved successfully');
    }

    /**
     * Display the specified MasterPelatih.
     * GET|HEAD /masterPelatihs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MasterPelatih $masterPelatih */
        $masterPelatih = $this->masterPelatihRepository->find($id);

        if (empty($masterPelatih)) {
            return $this->sendError('Master Pelatih not found');
        }

        return $this->sendResponse($masterPelatih->toArray(), 'Master Pelatih retrieved successfully');
    }

    /**
     * Update the specified MasterPelatih in storage.
     * PUT/PATCH /masterPelatihs/{id}
     *
     * @param int $id
     * @param UpdateMasterPelatihAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var MasterPelatih $masterPelatih */
        $masterPelatih = $this->masterPelatihRepository->find($id);

        if (empty($masterPelatih)) {
            return $this->sendError('Master Pelatih not found');
        }

        $masterPelatih = $this->masterPelatihRepository->update($input, $id);

        return $this->sendResponse($masterPelatih->toArray(), 'MasterPelatih updated successfully');
    }

    /**
     * Remove the specified MasterPelatih from storage.
     * DELETE /masterPelatihs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MasterPelatih $masterPelatih */
        $masterPelatih = $this->masterPelatihRepository->find($id);

        if (empty($masterPelatih)) {
            return $this->sendError('Master Pelatih not found');
        }

        $masterPelatih->delete();

        return $this->sendSuccess('Master Pelatih deleted successfully');
    }
}
