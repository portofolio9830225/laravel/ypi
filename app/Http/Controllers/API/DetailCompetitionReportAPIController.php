<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetailCompetitionReportAPIRequest;
use App\Http\Requests\API\UpdateDetailCompetitionReportAPIRequest;
use App\Models\DetailCompetitionReport;
use App\Repositories\DetailCompetitionReportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DetailCompetitionReportController
 * @package App\Http\Controllers\API
 */

class DetailCompetitionReportAPIController extends AppBaseController
{
    /** @var  DetailCompetitionReportRepository */
    private $detailCompetitionReportRepository;

    public function __construct(DetailCompetitionReportRepository $detailCompetitionReportRepo)
    {
        $this->detailCompetitionReportRepository = $detailCompetitionReportRepo;
    }

    /**
     * Display a listing of the DetailCompetitionReport.
     * GET|HEAD /detailCompetitionReports
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('competition_report_id') != null) {
            $detailCompetitionReports = DetailCompetitionReport::where('competition_report_id', $request->query('competition_report_id'))
                ->get();

            return $this->sendResponse($detailCompetitionReports->toArray(), 'Detail Competition Reports retrieved successfully');
        }

        $detailCompetitionReports = $this->detailCompetitionReportRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailCompetitionReports->toArray(), 'Detail Competition Reports retrieved successfully');
    }

    /**
     * Store a newly created DetailCompetitionReport in storage.
     * POST /detailCompetitionReports
     *
     * @param CreateDetailCompetitionReportAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailCompetitionReportAPIRequest $request)
    {
        $input = $request->all();

        $detailCompetitionReport = $this->detailCompetitionReportRepository->create($input);

        return $this->sendResponse($detailCompetitionReport->toArray(), 'Detail Competition Report saved successfully');
    }

    public function batchUpdate(Request $request)
    {
        $input = $request->all();

        if (isset($input[0]['competition_report_id'])) {
            DetailCompetitionReport::where('competition_report_id', $input[0]['competition_report_id'])
                ->delete();
        }
        foreach ($input as $key => $value) {
            DetailCompetitionReport::create($value);
        }
        return $this->sendResponse($input, 'Annual Plan Setting saved successfully');
    }

    /**
     * Display the specified DetailCompetitionReport.
     * GET|HEAD /detailCompetitionReports/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailCompetitionReport $detailCompetitionReport */
        $detailCompetitionReport = $this->detailCompetitionReportRepository->find($id);

        if (empty($detailCompetitionReport)) {
            return $this->sendError('Detail Competition Report not found');
        }

        return $this->sendResponse($detailCompetitionReport->toArray(), 'Detail Competition Report retrieved successfully');
    }

    /**
     * Update the specified DetailCompetitionReport in storage.
     * PUT/PATCH /detailCompetitionReports/{id}
     *
     * @param int $id
     * @param UpdateDetailCompetitionReportAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailCompetitionReportAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetailCompetitionReport $detailCompetitionReport */
        $detailCompetitionReport = $this->detailCompetitionReportRepository->find($id);

        if (empty($detailCompetitionReport)) {
            return $this->sendError('Detail Competition Report not found');
        }

        $detailCompetitionReport = $this->detailCompetitionReportRepository->update($input, $id);

        return $this->sendResponse($detailCompetitionReport->toArray(), 'DetailCompetitionReport updated successfully');
    }

    /**
     * Remove the specified DetailCompetitionReport from storage.
     * DELETE /detailCompetitionReports/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailCompetitionReport $detailCompetitionReport */
        $detailCompetitionReport = $this->detailCompetitionReportRepository->find($id);

        if (empty($detailCompetitionReport)) {
            return $this->sendError('Detail Competition Report not found');
        }

        $detailCompetitionReport->delete();

        return $this->sendSuccess('Detail Competition Report deleted successfully');
    }
}
