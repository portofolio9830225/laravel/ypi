<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Club;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AuthAPIController extends Controller
{
    public function auth()
    {
        if (!Auth::attempt(request()->only('email', 'password'), true)) {
            return response()
                ->json(['message' => 'Unauthorized'], 401);
        }

        $user = User::where('email', request()->email)->first();
        $club = Club::where('id', $user->active_club_id)->first();
        $token = $user->createToken('auth_token')->plainTextToken;

        return response()->json([
            'message' => 'Login Success',
            'user' => $user,
            'access_token' => $token,
            'token_type' => 'Bearer',
            'club' => $club,
        ]);
    }

    public function logout()
    {
        auth()->user()->tokens()->delete();

        return response()->json('You have successfully logged out and the token was successfully deleted');
    }

    public function register()
    {
        $validator = Validator::make(request()->all(), [
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => 'Register Gagal',
                'data' => $validator->errors()
            ]);
        }

        $user = User::create([
            'name' => request()->name,
            'email' => request()->email,
            'password' => Hash::make(request()->password),
            'level' => 'user',
        ]);

        return response()->json([
            'message' => 'Register Sukses',
            'data' => $user
        ]);
    }
}
