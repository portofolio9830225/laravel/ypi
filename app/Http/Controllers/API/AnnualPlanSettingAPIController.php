<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAnnualPlanSettingAPIRequest;
use App\Http\Requests\API\UpdateAnnualPlanSettingAPIRequest;
use App\Models\AnnualPlanSetting;
use App\Repositories\AnnualPlanSettingRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class AnnualPlanSettingController
 * @package App\Http\Controllers\API
 */

class AnnualPlanSettingAPIController extends AppBaseController
{
    /** @var  AnnualPlanSettingRepository */
    private $annualPlanSettingRepository;

    public function __construct(AnnualPlanSettingRepository $annualPlanSettingRepo)
    {
        $this->annualPlanSettingRepository = $annualPlanSettingRepo;
    }

    /**
     * Display a listing of the AnnualPlanSetting.
     * GET|HEAD /annualPlanSettings
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('groupedList') != null) {
            $annualPlanSettings = AnnualPlanSetting::with('ypi', 'groupActivity')->groupBy('master_ypi_id')->groupBy('kelompok')->get();
            return $this->sendResponse($annualPlanSettings->toArray(), 'Annual Plan Settings retrieved successfully');
        }

        if ($request->query('ypi_id') != null && $request->query('group_activity_id') != null) {
            $annualPlanSettings = AnnualPlanSetting::with('ypi', 'groupActivity')
                ->where('master_ypi_id', $request->query('ypi_id'))
                ->where('kelompok', $request->query('group_activity_id'))->get();
            return $this->sendResponse($annualPlanSettings->toArray(), 'Annual Plan Settings retrieved successfully');
        }

        if ($request->query('master_ypi_id') != null) {
            $annualPlanSettings = AnnualPlanSetting::with('groupActivity')
                ->where('master_ypi_id', $request->query('master_ypi_id'))
                ->orderBy('kelompok')
                ->get();
            $result = $annualPlanSettings->toArray();
            // foreach ($annualPlanSettings->toArray() as $val) {
            //     $result[$val['kelompok']][] = $val;
            // }
            return $this->sendResponse($result, 'Annual Plan Settings retrieved successfully');
        }

        $annualPlanSettings = $this->annualPlanSettingRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($annualPlanSettings->toArray(), 'Annual Plan Settings retrieved successfully');
    }

    /**
     * Store a newly created AnnualPlanSetting in storage.
     * POST /annualPlanSettings
     *
     * @param CreateAnnualPlanSettingAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAnnualPlanSettingAPIRequest $request)
    {
        $input = $request->all();

        $annualPlanSetting = $this->annualPlanSettingRepository->create($input);

        // return $this->sendResponse($annualPlanSetting->toArray(), 'Annual Plan Setting saved successfully');
        return $this->sendResponse($request->all(), 'Annual Plan Setting saved successfully');
    }

    public function batchStore(Request $request)
    {
        $input = $request->all();

        foreach ($input['data'] as $key => $value) {
            AnnualPlanSetting::updateOrCreate([
                'id' => $value['id'],
                'master_ypi_id' => $value['master_ypi_id'],
                'kelompok' => $value['kelompok']
            ], $value);
        }

        foreach ($input['deletedIds'] as $key => $value) {
            AnnualPlanSetting::where('id', $value)->delete();
        }

        return $this->sendResponse($input, 'Annual Plan Setting saved successfully');
    }

    /**
     * Display the specified AnnualPlanSetting.
     * GET|HEAD /annualPlanSettings/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AnnualPlanSetting $annualPlanSetting */
        $annualPlanSetting = $this->annualPlanSettingRepository->find($id);

        if (empty($annualPlanSetting)) {
            return $this->sendError('Annual Plan Setting not found');
        }

        return $this->sendResponse($annualPlanSetting->toArray(), 'Annual Plan Setting retrieved successfully');
    }

    /**
     * Update the specified AnnualPlanSetting in storage.
     * PUT/PATCH /annualPlanSettings/{id}
     *
     * @param int $id
     * @param UpdateAnnualPlanSettingAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnnualPlanSettingAPIRequest $request)
    {
        $input = $request->all();

        /** @var AnnualPlanSetting $annualPlanSetting */
        $annualPlanSetting = $this->annualPlanSettingRepository->find($id);

        if (empty($annualPlanSetting)) {
            return $this->sendError('Annual Plan Setting not found');
        }

        $annualPlanSetting = $this->annualPlanSettingRepository->update($input, $id);

        return $this->sendResponse($annualPlanSetting->toArray(), 'AnnualPlanSetting updated successfully');
    }

    /**
     * Remove the specified AnnualPlanSetting from storage.
     * DELETE /annualPlanSettings/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /** @var AnnualPlanSetting $annualPlanSetting */
        if ($request->query('master_ypi_id') != null && $request->query('kelompok') != null) {
            AnnualPlanSetting::where('master_ypi_id', $request->query('master_ypi_id'))
                ->where('kelompok', $request->query('kelompok'))->delete();
            return $this->sendSuccess('Annual Plan Setting deleted successfully');
        }

        $annualPlanSetting = $this->annualPlanSettingRepository->find($id);

        if (empty($annualPlanSetting)) {
            return $this->sendError('Annual Plan Setting not found');
        }

        $annualPlanSetting->delete();

        return $this->sendSuccess('Annual Plan Setting deleted successfully');
    }
}
