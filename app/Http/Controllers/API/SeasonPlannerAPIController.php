<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeasonPlannerAPIRequest;
use App\Http\Requests\API\UpdateSeasonPlannerAPIRequest;
use App\Models\SeasonPlanner;
use App\Repositories\SeasonPlannerRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SeasonPlannerController
 * @package App\Http\Controllers\API
 */

class SeasonPlannerAPIController extends AppBaseController
{
    /** @var  SeasonPlannerRepository */
    private $seasonPlannerRepository;

    public function __construct(SeasonPlannerRepository $seasonPlannerRepo)
    {
        $this->seasonPlannerRepository = $seasonPlannerRepo;
    }

    /**
     * Display a listing of the SeasonPlanner.
     * GET|HEAD /seasonPlanners
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seasonPlanners = $this->seasonPlannerRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seasonPlanners->toArray(), 'Season Planners retrieved successfully');
    }

    /**
     * Store a newly created SeasonPlanner in storage.
     * POST /seasonPlanners
     *
     * @param CreateSeasonPlannerAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSeasonPlannerAPIRequest $request)
    {
        $input = $request->all();

        $seasonPlanner = $this->seasonPlannerRepository->create($input);

        return $this->sendResponse($seasonPlanner->toArray(), 'Season Planner saved successfully');
    }

    /**
     * Display the specified SeasonPlanner.
     * GET|HEAD /seasonPlanners/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SeasonPlanner $seasonPlanner */
        $seasonPlanner = $this->seasonPlannerRepository->find($id);

        if (empty($seasonPlanner)) {
            return $this->sendError('Season Planner not found');
        }

        return $this->sendResponse($seasonPlanner->toArray(), 'Season Planner retrieved successfully');
    }

    /**
     * Update the specified SeasonPlanner in storage.
     * PUT/PATCH /seasonPlanners/{id}
     *
     * @param int $id
     * @param UpdateSeasonPlannerAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeasonPlannerAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeasonPlanner $seasonPlanner */
        $seasonPlanner = $this->seasonPlannerRepository->find($id);

        if (empty($seasonPlanner)) {
            return $this->sendError('Season Planner not found');
        }

        $seasonPlanner = $this->seasonPlannerRepository->update($input, $id);

        return $this->sendResponse($seasonPlanner->toArray(), 'SeasonPlanner updated successfully');
    }

    /**
     * Remove the specified SeasonPlanner from storage.
     * DELETE /seasonPlanners/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SeasonPlanner $seasonPlanner */
        $seasonPlanner = $this->seasonPlannerRepository->find($id);

        if (empty($seasonPlanner)) {
            return $this->sendError('Season Planner not found');
        }

        $seasonPlanner->delete();

        return $this->sendSuccess('Season Planner deleted successfully');
    }
}
