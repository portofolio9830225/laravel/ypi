<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\ClubSubscription;
use App\Models\Subscription;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClubSubscriptionAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message'   => 'All Club Subscription',
            'data'      => ClubSubscription::latest()->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'id_subscription' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message'   => 'Create Club Subscription Failed',
                'data'      => $validator->errors()
            ]);
        }

        $subscription = Subscription::find(request()->id_subscription);

        if (is_null($subscription)) {
            return response()->json([
                'message'   => 'Create Club Subscription Failed',
                'data'      => 'Id Subscription ' . request()->id_subscription . ' Nothing'
            ]);
        }

        $start_time = time();
        if ($subscription->type == 'harian') {
            $end_time = $start_time + (24 * 60 * 60);

            $data = ClubSubscription::create([
                'nama'          => $subscription->nama,
                'type'          => $subscription->type,
                'deskripsi'     => $subscription->deskripsi,
                'harga'          => $subscription->harga,
                'start_time' => date('d-m-Y H:i:s', $start_time),
                'end_time' => date('d-m-Y H:i:s', $end_time),
            ]);
        } elseif ($subscription->type == 'bulanan') {
            $end_time = $start_time + (30 * 24 * 60 * 60);

            // return response()->json([
            //     'start_time' => date('d-m-Y H:i:s', $start_time),
            //     'end_time' => date('d-m-Y H:i:s', $end_time),
            // ]);
            $data = ClubSubscription::create([
                'nama'          => $subscription->nama,
                'type'          => $subscription->type,
                'deskripsi'     => $subscription->deskripsi,
                'harga'         => $subscription->harga,
                'start_time'    => date('d-m-Y H:i:s', $start_time),
                'end_time'      => date('d-m-Y H:i:s', $end_time),
            ]);
        }

        return response()->json([
            'message'   => 'Create Club Subscription Success',
            'data'      => $data
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ClubSubcription  $clubSubcription
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message'   => 'Show Club Subscription',
            'data'      => ClubSubscription::find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ClubSubcription  $clubSubcription
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ClubSubcription  $clubSubcription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ClubSubcription  $clubSubcription
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClubSubscription::destroy($id);
        return response()->json('Delete Club Subscription Success');
    }
}
