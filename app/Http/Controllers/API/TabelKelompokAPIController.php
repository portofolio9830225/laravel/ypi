<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTabelKelompokAPIRequest;
use App\Http\Requests\API\UpdateTabelKelompokAPIRequest;
use App\Models\TabelKelompok;
use App\Repositories\TabelKelompokRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TabelKelompokController
 * @package App\Http\Controllers\API
 */

class TabelKelompokAPIController extends AppBaseController
{
    /** @var  TabelKelompokRepository */
    private $tabelKelompokRepository;

    public function __construct(TabelKelompokRepository $tabelKelompokRepo)
    {
        $this->tabelKelompokRepository = $tabelKelompokRepo;
    }

    /**
     * Display a listing of the TabelKelompok.
     * GET|HEAD /tabelKelompoks
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tabelKelompoks = $this->tabelKelompokRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($tabelKelompoks->toArray(), 'Tabel Kelompoks retrieved successfully');
    }

    /**
     * Store a newly created TabelKelompok in storage.
     * POST /tabelKelompoks
     *
     * @param CreateTabelKelompokAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTabelKelompokAPIRequest $request)
    {
        $input = $request->all();

        $tabelKelompok = $this->tabelKelompokRepository->create($input);

        return $this->sendResponse($tabelKelompok->toArray(), 'Tabel Kelompok saved successfully');
    }

    /**
     * Display the specified TabelKelompok.
     * GET|HEAD /tabelKelompoks/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TabelKelompok $tabelKelompok */
        $tabelKelompok = $this->tabelKelompokRepository->find($id);

        if (empty($tabelKelompok)) {
            return $this->sendError('Tabel Kelompok not found');
        }

        return $this->sendResponse($tabelKelompok->toArray(), 'Tabel Kelompok retrieved successfully');
    }

    /**
     * Update the specified TabelKelompok in storage.
     * PUT/PATCH /tabelKelompoks/{id}
     *
     * @param int $id
     * @param UpdateTabelKelompokAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTabelKelompokAPIRequest $request)
    {
        $input = $request->all();

        /** @var TabelKelompok $tabelKelompok */
        $tabelKelompok = $this->tabelKelompokRepository->find($id);

        if (empty($tabelKelompok)) {
            return $this->sendError('Tabel Kelompok not found');
        }

        $tabelKelompok = $this->tabelKelompokRepository->update($input, $id);

        return $this->sendResponse($tabelKelompok->toArray(), 'TabelKelompok updated successfully');
    }

    /**
     * Remove the specified TabelKelompok from storage.
     * DELETE /tabelKelompoks/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TabelKelompok $tabelKelompok */
        $tabelKelompok = $this->tabelKelompokRepository->find($id);

        if (empty($tabelKelompok)) {
            return $this->sendError('Tabel Kelompok not found');
        }

        $tabelKelompok->delete();

        return $this->sendSuccess('Tabel Kelompok deleted successfully');
    }
}
