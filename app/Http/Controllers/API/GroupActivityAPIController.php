<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGroupActivityAPIRequest;
use App\Http\Requests\API\UpdateGroupActivityAPIRequest;
use App\Models\GroupActivity;
use App\Repositories\GroupActivityRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class GroupActivityController
 * @package App\Http\Controllers\API
 */

class GroupActivityAPIController extends AppBaseController
{
    /** @var  GroupActivityRepository */
    private $groupActivityRepository;

    public function __construct(GroupActivityRepository $groupActivityRepo)
    {
        $this->groupActivityRepository = $groupActivityRepo;
    }

    /**
     * Display a listing of the GroupActivity.
     * GET|HEAD /groupActivities
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $groupActivities = $this->groupActivityRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($groupActivities->toArray(), 'Group Activities retrieved successfully');
    }

    /**
     * Store a newly created GroupActivity in storage.
     * POST /groupActivities
     *
     * @param CreateGroupActivityAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGroupActivityAPIRequest $request)
    {
        $input = $request->all();

        $groupActivity = $this->groupActivityRepository->create($input);

        return $this->sendResponse($groupActivity->toArray(), 'Group Activity saved successfully');
    }

    /**
     * Display the specified GroupActivity.
     * GET|HEAD /groupActivities/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var GroupActivity $groupActivity */
        $groupActivity = $this->groupActivityRepository->find($id);

        if (empty($groupActivity)) {
            return $this->sendError('Group Activity not found');
        }

        return $this->sendResponse($groupActivity->toArray(), 'Group Activity retrieved successfully');
    }

    /**
     * Update the specified GroupActivity in storage.
     * PUT/PATCH /groupActivities/{id}
     *
     * @param int $id
     * @param UpdateGroupActivityAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGroupActivityAPIRequest $request)
    {
        $input = $request->all();

        /** @var GroupActivity $groupActivity */
        $groupActivity = $this->groupActivityRepository->find($id);

        if (empty($groupActivity)) {
            return $this->sendError('Group Activity not found');
        }

        $groupActivity = $this->groupActivityRepository->update($input, $id);

        return $this->sendResponse($groupActivity->toArray(), 'GroupActivity updated successfully');
    }

    /**
     * Remove the specified GroupActivity from storage.
     * DELETE /groupActivities/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var GroupActivity $groupActivity */
        $groupActivity = $this->groupActivityRepository->find($id);

        if (empty($groupActivity)) {
            return $this->sendError('Group Activity not found');
        }

        $groupActivity->delete();

        return $this->sendSuccess('Group Activity deleted successfully');
    }
}
