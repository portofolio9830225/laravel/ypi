<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTabelKategoriAPIRequest;
use App\Http\Requests\API\UpdateTabelKategoriAPIRequest;
use App\Models\TabelKategori;
use App\Repositories\TabelKategoriRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class TabelKategoriController
 * @package App\Http\Controllers\API
 */

class TabelKategoriAPIController extends AppBaseController
{
    /** @var  TabelKategoriRepository */
    private $tabelKategoriRepository;

    public function __construct(TabelKategoriRepository $tabelKategoriRepo)
    {
        $this->tabelKategoriRepository = $tabelKategoriRepo;
    }

    /**
     * Display a listing of the TabelKategori.
     * GET|HEAD /tabelKategoris
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $tabelKategoris = $this->tabelKategoriRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        $tabelKategoris = TabelKategori::with('level')->get();

        return $this->sendResponse($tabelKategoris->toArray(), 'Tabel Kategoris retrieved successfully');
    }

    /**
     * Store a newly created TabelKategori in storage.
     * POST /tabelKategoris
     *
     * @param CreateTabelKategoriAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTabelKategoriAPIRequest $request)
    {
        $input = $request->all();

        $tabelKategori = $this->tabelKategoriRepository->create($input);

        return $this->sendResponse($tabelKategori->toArray(), 'Tabel Kategori saved successfully');
    }

    /**
     * Display the specified TabelKategori.
     * GET|HEAD /tabelKategoris/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TabelKategori $tabelKategori */
        $tabelKategori = $this->tabelKategoriRepository->find($id);

        if (empty($tabelKategori)) {
            return $this->sendError('Tabel Kategori not found');
        }

        return $this->sendResponse($tabelKategori->toArray(), 'Tabel Kategori retrieved successfully');
    }

    /**
     * Update the specified TabelKategori in storage.
     * PUT/PATCH /tabelKategoris/{id}
     *
     * @param int $id
     * @param UpdateTabelKategoriAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTabelKategoriAPIRequest $request)
    {
        $input = $request->all();

        /** @var TabelKategori $tabelKategori */
        $tabelKategori = $this->tabelKategoriRepository->find($id);

        if (empty($tabelKategori)) {
            return $this->sendError('Tabel Kategori not found');
        }

        $tabelKategori = $this->tabelKategoriRepository->update($input, $id);

        return $this->sendResponse($tabelKategori->toArray(), 'TabelKategori updated successfully');
    }

    /**
     * Remove the specified TabelKategori from storage.
     * DELETE /tabelKategoris/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TabelKategori $tabelKategori */
        $tabelKategori = $this->tabelKategoriRepository->find($id);

        if (empty($tabelKategori)) {
            return $this->sendError('Tabel Kategori not found');
        }

        $tabelKategori->delete();

        return $this->sendSuccess('Tabel Kategori deleted successfully');
    }
}
