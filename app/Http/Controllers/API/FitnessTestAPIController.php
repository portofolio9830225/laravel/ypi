<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFitnessTestAPIRequest;
use App\Http\Requests\API\UpdateFitnessTestAPIRequest;
use App\Models\FitnessTest;
use App\Repositories\FitnessTestRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\MasterPemain;
use App\Models\TabelKelompok;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class FitnessTestController
 * @package App\Http\Controllers\API
 */

class FitnessTestAPIController extends AppBaseController
{
    /** @var  FitnessTestRepository */
    private $fitnessTestRepository;

    public function __construct(FitnessTestRepository $fitnessTestRepo)
    {
        $this->fitnessTestRepository = $fitnessTestRepo;
    }

    /**
     * Display a listing of the FitnessTest.
     * GET|HEAD /fitnessTests
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (
            $request->query('tanggal_test') != null
            && $request->query('gender') != null
            && $request->query('kelompok_id') != null
            && $request->query('pemain_id') != null
        ) {
            $fitnessTests = FitnessTest::where('tanggal_test', $request->query('tanggal_test'))
                ->where('gender', $request->query('gender'))
                ->where('kelompok_id', $request->query('kelompok_id'))
                ->where('pemain_id', $request->query('pemain_id'))
                ->first();

            $res = $fitnessTests;
            if ($res != null) $res = $res->toArray();

            return $this->sendResponse($res, 'Fitness Tests retrieved successfully');
        }

        if ($request->query('groupDate') != null) {
            $fitnessTests = FitnessTest::groupBy('tanggal_test')
                ->get();

            return $this->sendResponse($fitnessTests->toArray(), 'Fitness Tests retrieved successfully');
        }

        $fitnessTests = $this->fitnessTestRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($fitnessTests->toArray(), 'Fitness Tests retrieved successfully');
    }

    /**
     * Store a newly created FitnessTest in storage.
     * POST /fitnessTests
     *
     * @param CreateFitnessTestAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFitnessTestAPIRequest $request)
    {
        $input = $request->all();

        $fitnessTest = $this->fitnessTestRepository->create($input);

        return $this->sendResponse($fitnessTest->toArray(), 'Fitness Test saved successfully');
    }

    public function tableDataReport(Request $request)
    {
        $data = FitnessTest::join('tabel_kelompoks', 'fitness_tests.kelompok_id', '=', 'tabel_kelompoks.id')
            ->select(DB::raw('tanggal_test as date, gender, kelompok_id as group_id, count(*) as count, tabel_kelompoks.kelompok as group_name'))
            ->groupBy('tanggal_test', 'kelompok_id', 'gender')->get();
        return $this->sendResponse($data, 'Fitness Test retrived successfully');
    }

    public function tableDetailReport($date, $gender, $group_id)
    {
        $tableData = FitnessTest::join('master_pemains', 'fitness_tests.pemain_id', '=', 'master_pemains.id')
            ->select(DB::raw('fitness_tests.*, master_pemains.nama_lengkap as player_name'))
            ->where('tanggal_test', $date)
            ->where('gender', $gender)
            ->where('fitness_tests.kelompok_id', $group_id)
            ->where('court_agility', '!=', 0)
            ->where('vo2max', '!=', 0)
            ->where('bench_press', '!=', 0)
            ->where('squat', '!=', 0)
            ->with('player', 'group')
            ->get();

        $presencePlayersId = [];
        foreach ($tableData as $key => $value) {
            array_push($presencePlayersId, $value->pemain_id);
        }

        $playerAbsent = MasterPemain::where('kelompok_id', $group_id)
            ->whereNotIn('id', $presencePlayersId)->get();

        $absentPlayersName = '';
        foreach ($playerAbsent as $key => $value) {
            if ($key === count($playerAbsent) - 1) {
                $absentPlayersName .= $value->nama_lengkap;
            } else {
                $absentPlayersName .= $value->nama_lengkap . ', ';
            }
        }

        $group = TabelKelompok::where('id', $group_id)->first();

        $data = [
            'table_data' => $tableData,
            'players_absent' => $absentPlayersName,
            'date' => $date,
            'gender' => $gender,
            'group_name' => $group->kelompok,
        ];
        return $this->sendResponse($data, 'Fitness Test retrived successfully');
    }

    public function tableData(Request $request)
    {
        $input = $request->all();
        $tableData = [];
        $players = MasterPemain::where('kelompok_id', $input['group_id'])->get();
        foreach ($players as $key => $value) {
            $data = FitnessTest::where('tanggal_test', $input['date_test'])
                ->where('gender', $input['gender'])
                ->where('kelompok_id', $input['group_id'])
                ->where('pemain_id', $value->id)
                ->first();
            if (!isset($data)) {
                $data = [
                    "tanggal_test" => $input['date_test'],
                    "gender" => $input['gender'],
                    "kelompok_id" => $input['group_id'],
                    "pemain_id" => $value->id,
                    "court_agility" => 0,
                    "vo2max" => 0,
                    "bench_press" => 0,
                    "squat" => 0
                ];
            } else {
                $data = $data->toArray();
            }
            $data['player_name'] = $value->nama_lengkap;
            $data['player_code'] = $value->nomor_ic;
            array_push($tableData, $data);
        }
        return $this->sendResponse($tableData, 'Fitness Test saved successfully');
    }

    public function progressReport(Request $request)
    {
        if ($request->has(['group_id', 'player_id', 'gender', 'start_year', 'end_year'])) {
            $input = $request->all();
            $from = date($input['start_year']);
            $to = date($input['end_year']);
            $data = FitnessTest::where('kelompok_id', $input['group_id'])
                ->where('pemain_id', $input['player_id'])
                ->where('gender', $input['gender'])
                ->whereBetween('tanggal_test', [$from, $to])
                ->get();
            return $this->sendResponse($data, 'Fitness Test saved successfully');
        }
        return $this->sendError('Fitness Test not found');
    }

    /**
     * Display the specified FitnessTest.
     * GET|HEAD /fitnessTests/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FitnessTest $fitnessTest */
        $fitnessTest = $this->fitnessTestRepository->find($id);

        if (empty($fitnessTest)) {
            return $this->sendError('Fitness Test not found');
        }

        return $this->sendResponse($fitnessTest->toArray(), 'Fitness Test retrieved successfully');
    }

    /**
     * Update the specified FitnessTest in storage.
     * PUT/PATCH /fitnessTests/{id}
     *
     * @param int $id
     * @param UpdateFitnessTestAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFitnessTestAPIRequest $request)
    {
        $input = $request->all();

        /** @var FitnessTest $fitnessTest */
        $fitnessTest = $this->fitnessTestRepository->find($id);

        if (empty($fitnessTest)) {
            return $this->sendError('Fitness Test not found');
        }

        $fitnessTest = $this->fitnessTestRepository->update($input, $id);

        return $this->sendResponse($fitnessTest->toArray(), 'FitnessTest updated successfully');
    }

    /**
     * Remove the specified FitnessTest from storage.
     * DELETE /fitnessTests/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FitnessTest $fitnessTest */
        $fitnessTest = $this->fitnessTestRepository->find($id);

        if (empty($fitnessTest)) {
            return $this->sendError('Fitness Test not found');
        }

        $fitnessTest->delete();

        return $this->sendSuccess('Fitness Test deleted successfully');
    }
}
