<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeasonPlanValueAPIRequest;
use App\Http\Requests\API\UpdateSeasonPlanValueAPIRequest;
use App\Models\SeasonPlanValue;
use App\Repositories\SeasonPlanValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class SeasonPlanValueController
 * @package App\Http\Controllers\API
 */

class SeasonPlanValueAPIController extends AppBaseController
{
    /** @var  SeasonPlanValueRepository */
    private $seasonPlanValueRepository;

    public function __construct(SeasonPlanValueRepository $seasonPlanValueRepo)
    {
        $this->seasonPlanValueRepository = $seasonPlanValueRepo;
    }

    /**
     * Display a listing of the SeasonPlanValue.
     * GET|HEAD /seasonPlanValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seasonPlanValues = $this->seasonPlanValueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );
        // $seasonPlanValues = SeasonPlanValue::with('seasonPlan', 'seasonPlanDetail')->get();

        return $this->sendResponse($seasonPlanValues->toArray(), 'Season Plan Values retrieved successfully');
    }

    /**
     * Store a newly created SeasonPlanValue in storage.
     * POST /seasonPlanValues
     *
     * @param CreateSeasonPlanValueAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        if ($request->has('batch')) {
            DB::transaction(function () use ($input) {
                foreach ($input as $key => $value) {
                    if (gettype($value) === 'array') {
                        $spv = SeasonPlanValue::where('ypi_id', $value['ypi_id'])
                            ->where('season_plan_id', $value['season_plan_id'])
                            ->where('season_plan_detail_id', $value['season_plan_detail_id'])
                            ->first();
                        $spv->fill($value);
                        $spv->save();
                    }
                }
            });
            return $this->sendResponse(['success'], 'Season Plan Value saved successfully');
        }

        $seasonPlanValue = $this->seasonPlanValueRepository->create($input);

        return $this->sendResponse($seasonPlanValue->toArray(), 'Season Plan Value saved successfully');
    }

    /**
     * Display the specified SeasonPlanValue.
     * GET|HEAD /seasonPlanValues/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SeasonPlanValue $seasonPlanValue */
        $seasonPlanValue = $this->seasonPlanValueRepository->find($id);

        if (empty($seasonPlanValue)) {
            return $this->sendError('Season Plan Value not found');
        }

        return $this->sendResponse($seasonPlanValue->toArray(), 'Season Plan Value retrieved successfully');
    }

    /**
     * Update the specified SeasonPlanValue in storage.
     * PUT/PATCH /seasonPlanValues/{id}
     *
     * @param int $id
     * @param UpdateSeasonPlanValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeasonPlanValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeasonPlanValue $seasonPlanValue */
        $seasonPlanValue = $this->seasonPlanValueRepository->find($id);

        if (empty($seasonPlanValue)) {
            return $this->sendError('Season Plan Value not found');
        }

        $seasonPlanValue = $this->seasonPlanValueRepository->update($input, $id);

        return $this->sendResponse($seasonPlanValue->toArray(), 'SeasonPlanValue updated successfully');
    }

    /**
     * Remove the specified SeasonPlanValue from storage.
     * DELETE /seasonPlanValues/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SeasonPlanValue $seasonPlanValue */
        $seasonPlanValue = $this->seasonPlanValueRepository->find($id);

        if (empty($seasonPlanValue)) {
            return $this->sendError('Season Plan Value not found');
        }

        $seasonPlanValue->delete();

        return $this->sendSuccess('Season Plan Value deleted successfully');
    }
}
