<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHeaderKpiAPIRequest;
use App\Http\Requests\API\UpdateHeaderKpiAPIRequest;
use App\Models\HeaderKpi;
use App\Repositories\HeaderKpiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class HeaderKpiController
 * @package App\Http\Controllers\API
 */

class HeaderKpiAPIController extends AppBaseController
{
    /** @var  HeaderKpiRepository */
    private $headerKpiRepository;

    public function __construct(HeaderKpiRepository $headerKpiRepo)
    {
        $this->headerKpiRepository = $headerKpiRepo;
    }

    /**
     * Display a listing of the HeaderKpi.
     * GET|HEAD /headerKpis
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('pemain_id') !== null) {
            $headerKpis = HeaderKpi::where('pemain_id', $request->query('pemain_id'))->get();
            return $this->sendResponse($headerKpis->toArray(), 'Header Kpis retrieved successfully');
        }

        $headerKpis = $this->headerKpiRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($headerKpis->toArray(), 'Header Kpis retrieved successfully');
    }

    /**
     * Store a newly created HeaderKpi in storage.
     * POST /headerKpis
     *
     * @param CreateHeaderKpiAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHeaderKpiAPIRequest $request)
    {
        $input = $request->all();

        $headerKpi = $this->headerKpiRepository->create($input);

        return $this->sendResponse($headerKpi->toArray(), 'Header Kpi saved successfully');
    }

    /**
     * Display the specified HeaderKpi.
     * GET|HEAD /headerKpis/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HeaderKpi $headerKpi */
        $headerKpi = $this->headerKpiRepository->find($id);

        if (empty($headerKpi)) {
            return $this->sendError('Header Kpi not found');
        }

        return $this->sendResponse($headerKpi->toArray(), 'Header Kpi retrieved successfully');
    }

    /**
     * Update the specified HeaderKpi in storage.
     * PUT/PATCH /headerKpis/{id}
     *
     * @param int $id
     * @param UpdateHeaderKpiAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHeaderKpiAPIRequest $request)
    {
        $input = $request->all();

        /** @var HeaderKpi $headerKpi */
        $headerKpi = $this->headerKpiRepository->find($id);

        if (empty($headerKpi)) {
            return $this->sendError('Header Kpi not found');
        }

        $headerKpi = $this->headerKpiRepository->update($input, $id);

        return $this->sendResponse($headerKpi->toArray(), 'HeaderKpi updated successfully');
    }

    /**
     * Remove the specified HeaderKpi from storage.
     * DELETE /headerKpis/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HeaderKpi $headerKpi */
        $headerKpi = $this->headerKpiRepository->find($id);

        if (empty($headerKpi)) {
            return $this->sendError('Header Kpi not found');
        }

        $headerKpi->delete();

        return $this->sendSuccess('Header Kpi deleted successfully');
    }
}
