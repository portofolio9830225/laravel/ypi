<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatePubertyDataAPIRequest;
use App\Http\Requests\API\UpdatePubertyDataAPIRequest;
use App\Models\PubertyData;
use App\Repositories\PubertyDataRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class PubertyDataController
 * @package App\Http\Controllers\API
 */

class PubertyDataAPIController extends AppBaseController
{
    /** @var  PubertyDataRepository */
    private $pubertyDataRepository;

    public function __construct(PubertyDataRepository $pubertyDataRepo)
    {
        $this->pubertyDataRepository = $pubertyDataRepo;
    }

    /**
     * Display a listing of the PubertyData.
     * GET|HEAD /pubertyDatas
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $pubertyDatas = $this->pubertyDataRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($pubertyDatas->toArray(), 'Puberty Datas retrieved successfully');
    }

    /**
     * Store a newly created PubertyData in storage.
     * POST /pubertyDatas
     *
     * @param CreatePubertyDataAPIRequest $request
     *
     * @return Response
     */
    public function store(CreatePubertyDataAPIRequest $request)
    {
        $input = $request->all();

        $pubertyData = $this->pubertyDataRepository->create($input);

        return $this->sendResponse($pubertyData->toArray(), 'Puberty Data saved successfully');
    }

    /**
     * Display the specified PubertyData.
     * GET|HEAD /pubertyDatas/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var PubertyData $pubertyData */
        $pubertyData = $this->pubertyDataRepository->find($id);

        if (empty($pubertyData)) {
            return $this->sendError('Puberty Data not found');
        }

        return $this->sendResponse($pubertyData->toArray(), 'Puberty Data retrieved successfully');
    }

    /**
     * Update the specified PubertyData in storage.
     * PUT/PATCH /pubertyDatas/{id}
     *
     * @param int $id
     * @param UpdatePubertyDataAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatePubertyDataAPIRequest $request)
    {
        $input = $request->all();

        /** @var PubertyData $pubertyData */
        $pubertyData = $this->pubertyDataRepository->find($id);

        if (empty($pubertyData)) {
            return $this->sendError('Puberty Data not found');
        }

        $pubertyData = $this->pubertyDataRepository->update($input, $id);

        return $this->sendResponse($pubertyData->toArray(), 'PubertyData updated successfully');
    }

    /**
     * Remove the specified PubertyData from storage.
     * DELETE /pubertyDatas/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var PubertyData $pubertyData */
        $pubertyData = $this->pubertyDataRepository->find($id);

        if (empty($pubertyData)) {
            return $this->sendError('Puberty Data not found');
        }

        $pubertyData->delete();

        return $this->sendSuccess('Puberty Data deleted successfully');
    }
}
