<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKpiParameterAPIRequest;
use App\Http\Requests\API\UpdateKpiParameterAPIRequest;
use App\Models\KpiParameter;
use App\Repositories\KpiParameterRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class KpiParameterController
 * @package App\Http\Controllers\API
 */

class KpiParameterAPIController extends AppBaseController
{
    /** @var  KpiParameterRepository */
    private $kpiParameterRepository;

    public function __construct(KpiParameterRepository $kpiParameterRepo)
    {
        $this->kpiParameterRepository = $kpiParameterRepo;
    }

    /**
     * Display a listing of the KpiParameter.
     * GET|HEAD /kpiParameters
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $kpiParameters = $this->kpiParameterRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($kpiParameters->toArray(), 'Kpi Parameters retrieved successfully');
    }

    /**
     * Store a newly created KpiParameter in storage.
     * POST /kpiParameters
     *
     * @param CreateKpiParameterAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateKpiParameterAPIRequest $request)
    {
        $input = $request->all();

        $kpiParameter = $this->kpiParameterRepository->create($input);

        return $this->sendResponse($kpiParameter->toArray(), 'Kpi Parameter saved successfully');
    }

    /**
     * Display the specified KpiParameter.
     * GET|HEAD /kpiParameters/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var KpiParameter $kpiParameter */
        $kpiParameter = $this->kpiParameterRepository->find($id);

        if (empty($kpiParameter)) {
            return $this->sendError('Kpi Parameter not found');
        }

        return $this->sendResponse($kpiParameter->toArray(), 'Kpi Parameter retrieved successfully');
    }

    /**
     * Update the specified KpiParameter in storage.
     * PUT/PATCH /kpiParameters/{id}
     *
     * @param int $id
     * @param UpdateKpiParameterAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKpiParameterAPIRequest $request)
    {
        $input = $request->all();

        /** @var KpiParameter $kpiParameter */
        $kpiParameter = $this->kpiParameterRepository->find($id);

        if (empty($kpiParameter)) {
            return $this->sendError('Kpi Parameter not found');
        }

        $kpiParameter = $this->kpiParameterRepository->update($input, $id);

        return $this->sendResponse($kpiParameter->toArray(), 'KpiParameter updated successfully');
    }

    /**
     * Remove the specified KpiParameter from storage.
     * DELETE /kpiParameters/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var KpiParameter $kpiParameter */
        $kpiParameter = $this->kpiParameterRepository->find($id);

        if (empty($kpiParameter)) {
            return $this->sendError('Kpi Parameter not found');
        }

        $kpiParameter->delete();

        return $this->sendSuccess('Kpi Parameter deleted successfully');
    }
}
