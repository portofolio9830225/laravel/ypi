<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSettingAttendanceAPIRequest;
use App\Http\Requests\API\UpdateSettingAttendanceAPIRequest;
use App\Models\SettingAttendance;
use App\Repositories\SettingAttendanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SettingAttendanceController
 * @package App\Http\Controllers\API
 */

class SettingAttendanceAPIController extends AppBaseController
{
    /** @var  SettingAttendanceRepository */
    private $settingAttendanceRepository;

    public function __construct(SettingAttendanceRepository $settingAttendanceRepo)
    {
        $this->settingAttendanceRepository = $settingAttendanceRepo;
    }

    /**
     * Display a listing of the SettingAttendance.
     * GET|HEAD /settingAttendances
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('kelompok') != null && $request->query('kode_pelatih') != null && $request->query('tanggal')) {
            $settingAttendances = SettingAttendance::where('kelompok', $request->query('kelompok'))
                ->where('kode_pelatih', $request->query('kode_pelatih'))
                ->where('tanggal', $request->query('tanggal'))
                ->get();

            return $this->sendResponse($settingAttendances->toArray(), 'Setting Attendances retrieved successfully');
        }

        if ($request->query('month') != null && $request->query('year') != null && $request->query('group_id') && $request->query('coach_id')) {
            $settingAttendances = SettingAttendance::where('bulan', $request->query('month'))
                ->where('tahun', $request->query('year'))
                ->where('kelompok', $request->query('group_id'))
                ->where('kode_pelatih', $request->query('coach_id'))
                ->get();

            return $this->sendResponse($settingAttendances->toArray(), 'Setting Attendances retrieved successfully');
        }

        $settingAttendances = $this->settingAttendanceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($settingAttendances->toArray(), 'Setting Attendances retrieved successfully');
    }

    /**
     * Store a newly created SettingAttendance in storage.
     * POST /settingAttendances
     *
     * @param CreateSettingAttendanceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSettingAttendanceAPIRequest $request)
    {
        $input = $request->all();

        $settingAttendance = $this->settingAttendanceRepository->create($input);

        return $this->sendResponse($settingAttendance->toArray(), 'Setting Attendance saved successfully');
    }

    public function batchStore(Request $request)
    {
        $input = $request->all();
        foreach ($input as $key => $value) {
            if ($value['deleted'] == true && isset($value['id'])) {
                $data = SettingAttendance::find($value['id']);
                $data->delete();
            } elseif (isset($value['id'])) {
                $data = SettingAttendance::find($value['id']);
                $data->fill($value);
                $data->save();
            } else {
                SettingAttendance::create($value);
            }
        }
        return $this->sendResponse($input, 'Annual Plan Setting saved successfully');
    }

    /**
     * Display the specified SettingAttendance.
     * GET|HEAD /settingAttendances/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SettingAttendance $settingAttendance */
        $settingAttendance = $this->settingAttendanceRepository->find($id);

        if (empty($settingAttendance)) {
            return $this->sendError('Setting Attendance not found');
        }

        return $this->sendResponse($settingAttendance->toArray(), 'Setting Attendance retrieved successfully');
    }

    /**
     * Update the specified SettingAttendance in storage.
     * PUT/PATCH /settingAttendances/{id}
     *
     * @param int $id
     * @param UpdateSettingAttendanceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSettingAttendanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var SettingAttendance $settingAttendance */
        $settingAttendance = $this->settingAttendanceRepository->find($id);

        if (empty($settingAttendance)) {
            return $this->sendError('Setting Attendance not found');
        }

        $settingAttendance = $this->settingAttendanceRepository->update($input, $id);

        return $this->sendResponse($settingAttendance->toArray(), 'SettingAttendance updated successfully');
    }

    /**
     * Remove the specified SettingAttendance from storage.
     * DELETE /settingAttendances/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /** @var SettingAttendance $settingAttendance */

        if ($request->query('month') != null && $request->query('year') != null && $request->query('group_id') && $request->query('coach_id')) {
            $settingAttendances = SettingAttendance::where('bulan', $request->query('month'))
                ->where('tahun', $request->query('year'))
                ->where('kelompok', $request->query('group_id'))
                ->where('kode_pelatih', $request->query('coach_id'))
                ->delete();

            return $this->sendSuccess('Setting Attendance deleted successfully');
        }

        $settingAttendance = $this->settingAttendanceRepository->find($id);

        if (empty($settingAttendance)) {
            return $this->sendError('Setting Attendance not found');
        }

        $settingAttendance->delete();

        return $this->sendSuccess('Setting Attendance deleted successfully');
    }
}
