<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCompetitionGroupAPIRequest;
use App\Http\Requests\API\UpdateCompetitionGroupAPIRequest;
use App\Models\CompetitionGroup;
use App\Repositories\CompetitionGroupRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CompetitionGroupController
 * @package App\Http\Controllers\API
 */

class CompetitionGroupAPIController extends AppBaseController
{
    /** @var  CompetitionGroupRepository */
    private $competitionGroupRepository;

    public function __construct(CompetitionGroupRepository $competitionGroupRepo)
    {
        $this->competitionGroupRepository = $competitionGroupRepo;
    }

    /**
     * Display a listing of the CompetitionGroup.
     * GET|HEAD /competitionGroups
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $competitionGroups = $this->competitionGroupRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($competitionGroups->toArray(), 'Competition Groups retrieved successfully');
    }

    /**
     * Store a newly created CompetitionGroup in storage.
     * POST /competitionGroups
     *
     * @param CreateCompetitionGroupAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCompetitionGroupAPIRequest $request)
    {
        $input = $request->all();

        $competitionGroup = $this->competitionGroupRepository->create($input);

        return $this->sendResponse($competitionGroup->toArray(), 'Competition Group saved successfully');
    }

    /**
     * Display the specified CompetitionGroup.
     * GET|HEAD /competitionGroups/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CompetitionGroup $competitionGroup */
        $competitionGroup = $this->competitionGroupRepository->find($id);

        if (empty($competitionGroup)) {
            return $this->sendError('Competition Group not found');
        }

        return $this->sendResponse($competitionGroup->toArray(), 'Competition Group retrieved successfully');
    }

    /**
     * Update the specified CompetitionGroup in storage.
     * PUT/PATCH /competitionGroups/{id}
     *
     * @param int $id
     * @param UpdateCompetitionGroupAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompetitionGroupAPIRequest $request)
    {
        $input = $request->all();

        /** @var CompetitionGroup $competitionGroup */
        $competitionGroup = $this->competitionGroupRepository->find($id);

        if (empty($competitionGroup)) {
            return $this->sendError('Competition Group not found');
        }

        $competitionGroup = $this->competitionGroupRepository->update($input, $id);

        return $this->sendResponse($competitionGroup->toArray(), 'CompetitionGroup updated successfully');
    }

    /**
     * Remove the specified CompetitionGroup from storage.
     * DELETE /competitionGroups/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CompetitionGroup $competitionGroup */
        $competitionGroup = $this->competitionGroupRepository->find($id);

        if (empty($competitionGroup)) {
            return $this->sendError('Competition Group not found');
        }

        $competitionGroup->delete();

        return $this->sendSuccess('Competition Group deleted successfully');
    }
}
