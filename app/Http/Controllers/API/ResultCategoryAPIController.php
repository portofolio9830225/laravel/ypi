<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateResultCategoryAPIRequest;
use App\Http\Requests\API\UpdateResultCategoryAPIRequest;
use App\Models\ResultCategory;
use App\Repositories\ResultCategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ResultCategoryController
 * @package App\Http\Controllers\API
 */

class ResultCategoryAPIController extends AppBaseController
{
    /** @var  ResultCategoryRepository */
    private $resultCategoryRepository;

    public function __construct(ResultCategoryRepository $resultCategoryRepo)
    {
        $this->resultCategoryRepository = $resultCategoryRepo;
    }

    /**
     * Display a listing of the ResultCategory.
     * GET|HEAD /resultCategories
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $resultCategories = $this->resultCategoryRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($resultCategories->toArray(), 'Result Categories retrieved successfully');
    }

    /**
     * Store a newly created ResultCategory in storage.
     * POST /resultCategories
     *
     * @param CreateResultCategoryAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateResultCategoryAPIRequest $request)
    {
        $input = $request->all();

        $resultCategory = $this->resultCategoryRepository->create($input);

        return $this->sendResponse($resultCategory->toArray(), 'Result Category saved successfully');
    }

    /**
     * Display the specified ResultCategory.
     * GET|HEAD /resultCategories/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var ResultCategory $resultCategory */
        $resultCategory = $this->resultCategoryRepository->find($id);

        if (empty($resultCategory)) {
            return $this->sendError('Result Category not found');
        }

        return $this->sendResponse($resultCategory->toArray(), 'Result Category retrieved successfully');
    }

    /**
     * Update the specified ResultCategory in storage.
     * PUT/PATCH /resultCategories/{id}
     *
     * @param int $id
     * @param UpdateResultCategoryAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateResultCategoryAPIRequest $request)
    {
        $input = $request->all();

        /** @var ResultCategory $resultCategory */
        $resultCategory = $this->resultCategoryRepository->find($id);

        if (empty($resultCategory)) {
            return $this->sendError('Result Category not found');
        }

        $resultCategory = $this->resultCategoryRepository->update($input, $id);

        return $this->sendResponse($resultCategory->toArray(), 'ResultCategory updated successfully');
    }

    /**
     * Remove the specified ResultCategory from storage.
     * DELETE /resultCategories/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var ResultCategory $resultCategory */
        $resultCategory = $this->resultCategoryRepository->find($id);

        if (empty($resultCategory)) {
            return $this->sendError('Result Category not found');
        }

        $resultCategory->delete();

        return $this->sendSuccess('Result Category deleted successfully');
    }
}
