<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKpiResultAPIRequest;
use App\Http\Requests\API\UpdateKpiResultAPIRequest;
use App\Models\KpiResult;
use App\Repositories\KpiResultRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Constant;
use App\Models\KpiParameter;
use App\Models\MasterPemain;
use App\Models\TabelKelompok;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class KpiResultController
 * @package App\Http\Controllers\API
 */

class KpiResultAPIController extends AppBaseController
{
    /** @var  KpiResultRepository */
    private $kpiResultRepository;
    private $constantApiController;

    public function __construct(KpiResultRepository $kpiResultRepo, ConstantAPIController $constantApiCont)
    {
        $this->kpiResultRepository = $kpiResultRepo;
        $this->constantApiController = $constantApiCont;
    }

    /**
     * Display a listing of the KpiResult.
     * GET|HEAD /kpiResults
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $kpiResults = $this->kpiResultRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($kpiResults->toArray(), 'Kpi Results retrieved successfully');
    }

    public function fitnessTestIndex($date, $group_id)
    {
        $players = MasterPemain::where('kelompok_id', $group_id)->get();
        $ca1IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_1_ID')->first();
        $ca1Id = $ca1IdConstant->value;
        $ca2IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_2_ID')->first();
        $ca2Id = $ca2IdConstant->value;
        $ca3IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_3_ID')->first();
        $ca3Id = $ca3IdConstant->value;
        $benchpressIdConstant = Constant::where('key', 'KPI_BENCH_PRESS_ID')->first();
        $benchpressId = $benchpressIdConstant->value;
        $squatIdConstant = Constant::where('key', 'KPI_SQUAT_ID')->first();
        $squatId = $squatIdConstant->value;
        $vo2maxConstant = Constant::where('key', 'KPI_VO2MAX_ID')->first();
        $vo2maxId = $vo2maxConstant->value;

        $data = [];
        foreach ($players as $key => $value) {
            $pemain_id = $value->id;
            $tanggal = $date;
            $item = [
                'player_code' => $value->nomor_ic,
                'player_name' => $value->nama_lengkap,
            ];

            $ca1 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca1Id)
                ->first();
            $ca2 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca2Id)
                ->first();
            $ca3 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca3Id)
                ->first();

            $caArray = [];
            if (isset($ca1)) {
                array_push($caArray, $ca1->value);
            }
            if (isset($ca2)) {
                array_push($caArray, $ca2->value);
            }
            if (isset($ca3)) {
                array_push($caArray, $ca3->value);
            }
            if (count($caArray) > 0) {
                $item['court_agility'] = min($caArray);
            }

            $vo2max = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $vo2maxId)
                ->first();
            $benchpress = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $benchpressId)
                ->first();
            $squat = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $squatId)
                ->first();
            if (isset($vo2max)) {
                $item['vo2max'] = $vo2max->value;
            }
            if (isset($benchpress)) {
                $item['bench_press'] = $benchpress->value;
            }
            if (isset($benchpress)) {
                $item['squat'] = $squat->value;
            }
            array_push($data, $item);
        }

        return $this->sendResponse($data, 'Kpi Results retrieved successfully');
    }

    /**
     * Store a newly created KpiResult in storage.
     * POST /kpiResults
     *
     * @param CreateKpiResultAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateKpiResultAPIRequest $request)
    {
        $input = $request->all();

        $kpiResult = $this->kpiResultRepository->create($input);

        return $this->sendResponse($kpiResult->toArray(), 'Kpi Result saved successfully');
    }

    public function batchStore(Request $request)
    {
        $input = $request->all();

        foreach ($input as $key => $value) {
            if ($key === 'player_id' || $key === 'date' || $key === 'group_id') continue;
            $kpiParameter = KpiParameter::where('name', $key)->first();
            if ($kpiParameter) {
                KpiResult::updateOrCreate([
                    'player_id' => $input['player_id'],
                    'group_id' => $input['group_id'],
                    'date' => $input['date'],
                    'kpi_parameter_id' => $kpiParameter->id,
                ], ['value' => $value]);
            }
        }

        return $this->sendResponse($input, 'Kpi Result saved successfully');
    }

    /**
     * Display the specified KpiResult.
     * GET|HEAD /kpiResults/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var KpiResult $kpiResult */
        $kpiResult = $this->kpiResultRepository->find($id);

        if (empty($kpiResult)) {
            return $this->sendError('Kpi Result not found');
        }

        return $this->sendResponse($kpiResult->toArray(), 'Kpi Result retrieved successfully');
    }

    public function batchShow($date, $group_id)
    {
        $playersByGroup = MasterPemain::where('kelompok_id', $group_id)->get();
        $group = TabelKelompok::where('id', $group_id)->get()->first();
        $kpiAgeConstant = Constant::where('key', 'KPI_AGE_ID')->first();
        $kpiAgeParameter = KpiParameter::find($kpiAgeConstant->value);
        $kpiHeightConstant = Constant::where('key', 'KPI_HEIGHT_ID')->first();
        $kpiHeightParameter = KpiParameter::find($kpiHeightConstant->value);
        $kpiWeightConstant = Constant::where('key', 'KPI_WEIGHT_ID')->first();
        $kpiWeightParameter = KpiParameter::find($kpiWeightConstant->value);

        $data = [];
        foreach ($playersByGroup as $key => $player) {
            $kpiResults = KpiResult::where('date', $date)
                ->where('player_id', $player['id'])->get();
            $rowData = [];
            $rowData['player_id'] = $player['id'];
            $rowData['player_name'] = $player['nama_lengkap'];
            $rowData['group_id'] = $group->id;
            $rowData['group_name'] = $group->kelompok;
            if (count($kpiResults) > 0) {
                foreach ($kpiResults as $key => $kpiResult) {
                    $kpiParameter = KpiParameter::where('id', $kpiResult['kpi_parameter_id'])->get()->first();
                    $rowData[$kpiParameter->name] = $kpiResult['value'];
                }
            }
            $rowData[$kpiAgeParameter->name] = Carbon::parse($player['tanggal_lahir'])->age;

            $kpiResHeight = KpiResult::where('kpi_parameter_id', $kpiHeightParameter->id)
                ->where('player_id', $player['id'])
                ->where('group_id', $group->id)
                ->where('date', $date)
                ->first();
            if (empty($kpiResHeight)) {
                $kpiResHeightLatest = KpiResult::where('kpi_parameter_id', $kpiHeightParameter->id)
                    ->where('player_id', $player['id'])
                    ->where('group_id', $group->id)
                    ->latest('date')->first();
                if (isset($kpiResHeightLatest)) {
                    $rowData[$kpiHeightParameter->name] = $kpiResHeightLatest->value;
                }
            }
            $kpiResWeight = KpiResult::where('kpi_parameter_id', $kpiWeightParameter->id)
                ->where('player_id', $player['id'])
                ->where('group_id', $group->id)
                ->where('date', $date)
                ->first();
            if (empty($kpiResWeight)) {
                $kpiResWeightLatest = KpiResult::where('kpi_parameter_id', $kpiWeightParameter->id)
                    ->where('player_id', $player['id'])
                    ->where('group_id', $group->id)
                    ->latest('date')->first();
                Log::info('test', [$kpiWeightParameter->id, $player['id']], $group->id);
                if (isset($kpiResWeightLatest)) {
                    $rowData[$kpiWeightParameter->name] = $kpiResWeightLatest->value;
                }
            }

            array_push($data, $rowData);
        }
        return $this->sendResponse($data, 'Kpi Result retrieved successfully');
    }

    public function dates()
    {
        $groupByDate = KpiResult::groupBy('date')
            ->where('group_id', '<>', 0)->get()->toArray();
        $data = [];
        foreach ($groupByDate as $key => $value) {
            array_push($data, $value['date']);
        }
        return $this->sendResponse($data, 'Kpi Result retrieved successfully');
    }

    public function compare(Request $request)
    {
        $input = $request->all();


        if ($request->has(['players_id', 'parameters_id'])) {
            $latestData = DB::table('kpi_results')
                ->select('player_id', DB::raw('MAX(date) as date'))
                ->whereIn('player_id', $input['players_id'])
                ->groupBy('player_id')->get()->toArray();

            $data = [];
            foreach ($latestData as $key => $value) {
                if ($input['parameters_id'] == 0) {
                    $kpiResults = KpiResult::with('kpiParameter', 'player')
                        ->where('player_id', $value->player_id)
                        ->where('date', $value->date)
                        ->get()->toArray();
                } else {
                    $kpiResults = KpiResult::with('kpiParameter', 'player')
                        ->where('player_id', $value->player_id)
                        ->where('date', $value->date)
                        ->whereIn('kpi_parameter_id', $input['parameters_id'])
                        ->get()->toArray();
                }

                $chartData = [];
                foreach ($kpiResults as $k => $v) {
                    if ($v['kpi_parameter']['input_type'] == 'number') {
                        array_push($chartData, [
                            "player_name" => $v['player']['nama_lengkap'],
                            "parameter" => $v['kpi_parameter']['name'],
                            "value" => (float) $v['value'],
                        ]);
                    }
                }

                if ($request->has('custom_parameters_id')) {
                    foreach ($input['custom_parameters_id'] as $k => $v) {
                        $customParameterObj = $this->constantApiController->findParameter('KPI_COMPARISON', $v, false);
                        $customParameter = json_decode(json_encode($customParameterObj), true);
                        if ($customParameter['type'] === 'max') {
                            $kpiResult = KpiResult::select('player_id', DB::raw('MAX(value) as value'))
                                ->with('player')
                                ->where('player_id', $value->player_id)
                                ->where('date', $value->date)
                                ->whereIn('kpi_parameter_id', $customParameter['parameters_id'])
                                ->first();

                            if (isset($kpiResult->player_id)) {
                                array_push($chartData, [
                                    "player_name" => $kpiResult['player']['nama_lengkap'],
                                    "parameter" => $customParameter['name'],
                                    "value" => (float) $kpiResult['value'],
                                ]);
                            }
                        } else if ($customParameter['type'] === 'min') {
                            $kpiResult = KpiResult::select('player_id', DB::raw('MIN(value) as value'))
                                ->with('player')
                                ->where('player_id', $value->player_id)
                                ->where('date', $value->date)
                                ->whereIn('kpi_parameter_id', $customParameter['parameters_id'])
                                ->first();

                            if (isset($kpiResult->player_id)) {
                                array_push($chartData, [
                                    "player_name" => $kpiResult['player']['nama_lengkap'],
                                    "parameter" => $customParameter['name'],
                                    "value" => (float) $kpiResult['value'],
                                ]);
                            }
                        } else if ($customParameter['type'] === 'avg') {
                            $kpiResult = KpiResult::select('player_id', DB::raw('AVG(value) as value'))
                                ->with('player')
                                ->where('player_id', $value->player_id)
                                ->where('date', $value->date)
                                ->whereIn('kpi_parameter_id', $customParameter['parameters_id'])
                                ->first();

                            if (isset($kpiResult->player_id)) {
                                array_push($chartData, [
                                    "player_name" => $kpiResult['player']['nama_lengkap'],
                                    "parameter" => $customParameter['name'],
                                    "value" => (float) $kpiResult['value'],
                                ]);
                            }
                        }
                    }
                }

                $data = array_merge($data, $chartData);
            }

            return $this->sendResponse($data, 'Kpi Result retrieved successfully');
        }

        return $this->sendResponse([], 'Not Found');
    }

    /**
     * Update the specified KpiResult in storage.
     * PUT/PATCH /kpiResults/{id}
     *
     * @param int $id
     * @param UpdateKpiResultAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKpiResultAPIRequest $request)
    {
        $input = $request->all();

        /** @var KpiResult $kpiResult */
        $kpiResult = $this->kpiResultRepository->find($id);

        if (empty($kpiResult)) {
            return $this->sendError('Kpi Result not found');
        }

        $kpiResult = $this->kpiResultRepository->update($input, $id);

        return $this->sendResponse($kpiResult->toArray(), 'KpiResult updated successfully');
    }

    /**
     * Remove the specified KpiResult from storage.
     * DELETE /kpiResults/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var KpiResult $kpiResult */
        $kpiResult = $this->kpiResultRepository->find($id);

        if (empty($kpiResult)) {
            return $this->sendError('Kpi Result not found');
        }

        $kpiResult->delete();

        return $this->sendSuccess('Kpi Result deleted successfully');
    }

    public function batchDestroy($date, $player_id)
    {
        KpiResult::where('date', $date)->where('player_id', $player_id)->delete();

        $kpiAgeConstant = Constant::where('key', 'KPI_AGE_ID')->first();
        $kpiAgeParameter = KpiParameter::find($kpiAgeConstant->value);
        $player = MasterPemain::with('group')
            ->where('id', $player_id)
            ->first();

        if (isset($player)) {
            $rowData = [];
            $rowData['player_id'] = $player['id'];
            $rowData['player_name'] = $player['nama_lengkap'];
            $rowData['group_id'] = $player['group']['id'];
            $rowData['group_name'] = $player['group']['kelompok'];
            $rowData[$kpiAgeParameter->name] = Carbon::parse($player['tanggal_lahir'])->age;

            return $this->sendResponse($rowData, 'Kpi Result deleted successfully');
        }

        return $this->sendError('Kpi Result deleted failed');
    }

    public function fitnessTestReportSummary()
    {
        $data = KpiResult::with('group:id,kelompok')
            ->where('group_id', '<>', 0)
            ->groupBy('date', 'group_id')->get();

        return $this->sendResponse($data, 'Kpi Result deleted successfully');
    }

    public function fitnessTestReportDetail($date, $group_id)
    {
        $group = TabelKelompok::find($group_id);
        $players = MasterPemain::where('kelompok_id', $group_id)->get();
        $ca1IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_1_ID')->first();
        $ca1Id = $ca1IdConstant->value;
        $ca2IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_2_ID')->first();
        $ca2Id = $ca2IdConstant->value;
        $ca3IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_3_ID')->first();
        $ca3Id = $ca3IdConstant->value;
        $benchpressIdConstant = Constant::where('key', 'KPI_BENCH_PRESS_ID')->first();
        $benchpressId = $benchpressIdConstant->value;
        $squatIdConstant = Constant::where('key', 'KPI_SQUAT_ID')->first();
        $squatId = $squatIdConstant->value;
        $vo2maxConstant = Constant::where('key', 'KPI_VO2MAX_ID')->first();
        $vo2maxId = $vo2maxConstant->value;

        $tableData = [];
        $playerAbsentNames = '';
        foreach ($players as $key => $value) {
            $pemain_id = $value->id;
            $tanggal = $date;
            $item = [
                'player_name' => $value->nama_lengkap,
                'gender' => $value->sex,
            ];

            $ca1 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca1Id)
                ->first();
            $ca2 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca2Id)
                ->first();
            $ca3 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca3Id)
                ->first();

            $caArray = [];
            if (isset($ca1)) {
                array_push($caArray, $ca1->value);
            }
            if (isset($ca2)) {
                array_push($caArray, $ca2->value);
            }
            if (isset($ca3)) {
                array_push($caArray, $ca3->value);
            }
            if (count($caArray) > 0) {
                $item['court_agility'] = (float) min($caArray);
            }

            $vo2max = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $vo2maxId)
                ->first();
            $benchpress = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $benchpressId)
                ->first();
            $squat = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $squatId)
                ->first();
            if (isset($vo2max)) {
                $item['vo2max'] = (float) $vo2max->value;
            }
            if (isset($benchpress)) {
                $item['bench_press'] = (float) $benchpress->value;
            }
            if (isset($benchpress)) {
                $item['squat'] = (float) $squat->value;
            }
            if (count($caArray) == 0 && empty($vo2max) && empty($benchpress) && empty($squat)) {
                $playerAbsentNames .= $value->nama_lengkap . ', ';
            } else {
                array_push($tableData, $item);
            }
        }

        $data = [
            'table_data' => $tableData,
            'players_absent' => $playerAbsentNames,
            'date' => $date,
            'group_name' => $group->kelompok,
        ];
        return $this->sendResponse($data, 'Kpi Result deleted successfully');
    }
}
