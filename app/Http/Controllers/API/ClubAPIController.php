<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Club;
use App\Models\ClubMember;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Mockery\Undefined;

class ClubAPIController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'message' => 'All Club',
            'data' => Club::get(),
        ]);
    }
    public function showAll()
    {
        return response()->json([
            'message' => 'All Club',
            'data' => Club::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $club = null;
        if ($input['id'] != 'undefined' && $request->has('id')) {
            $id = $input['id'];
            Log::info('id ada', [$id]);
            $club = Club::find($id);
        } else {
            $club = new Club;
            $id = $club->id;
            Log::info('id gak ada', [$id]);
            ClubMember::create([
                'club_id' => $club->id,
                'user_id' => Auth::id(),
                'role_club_member_id' => 1
            ]);
        }

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = 'pic_' . $id . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('club_image'), $fileName);

            $club->fill(['nama' => $input['nama'], 'alamat' => $input['alamat']]);

            $club->image = url('/') . '/club_image/' . $fileName;
            $club->save();
        } else {
            unset($input['image']);
            $club->fill($input);
            $club->save();
        }

        return response()->json([
            'message' => 'Create Club Success',
            'data' => $club
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            'message' => 'Show Club',
            'data' => Club::with('user', 'user.role_club')->find($id)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $clubMember = ClubMember::where('club_id', $id)->where('user_id', Auth::id())->where('role_club_member_id', 1)->first();
        if (is_null($clubMember)) {
            $returnData = array(
                'status' => 'error',
                'message' => 'An error occurred!'
            );
            return Response::json($returnData, 500);
        }

        $input = $request->all();
        $club = Club::find($id);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $fileName = 'pic_' . $id . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('club_image'), $fileName);

            $club->fill($input);

            $club->image = url('/') . '/club_image/' . $fileName;
            $club->save();
        } else {
            unset($input['image']);
            $club->fill($input);
            $club->save();
        }

        return response()->json([
            'message' => 'Update Club Success',
            'data' => $club,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $clubMember = ClubMember::where('club_id', $id)->where('user_id', Auth::id())->where('role_club_member_id', 1)->first();
        // if (is_null($clubMember)) {
        //     return response()->json('Anda Tidak Punya Hak Akses');
        // }

        DB::beginTransaction();
        Club::destroy($id);
        ClubMember::where('club_id', $id)->delete();
        DB::commit();

        return response()->json('Berhasil Delete Club');
    }
}
