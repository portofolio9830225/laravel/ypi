<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\AppBaseController;
use Response;

class TemplateDataAPIController extends AppBaseController
{
    public $templatesList = [
        'Standard' => \Database\Seeders\Templates\StandardSeeder::class,
    ];

    public function templates()
    {
        return Response::json([
            'success' => true,
            'message' => 'Template Data retrived successfully.',
            'data' => array_keys($this->templatesList),
        ], 200);
    }

    public function populateTenantData($name)
    {
        if (isset($this->templatesList[$name])) {
            $seeder = new $this->templatesList[$name];
            $seeder->run(tenant('id'));

            return Response::json([
                'success' => true,
                'message' => 'Template Data retrived successfully.',
                'data' => $seeder,
            ], 200);
        } else {
            return Response::json([
                'success' => false,
                'message' => 'Template Data Not Found.',
            ], 500);
        }
    }
}
