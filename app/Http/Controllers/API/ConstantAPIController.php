<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateConstantAPIRequest;
use App\Http\Requests\API\UpdateConstantAPIRequest;
use App\Models\Constant;
use App\Repositories\ConstantRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\SeasonPlan;
use App\Models\SeasonPlanDetail;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class ConstantController
 * @package App\Http\Controllers\API
 */

class ConstantAPIController extends AppBaseController
{
    /** @var  ConstantRepository */
    private $constantRepository;

    public function __construct(ConstantRepository $constantRepo)
    {
        $this->constantRepository = $constantRepo;
    }

    /**
     * Display a listing of the Constant.
     * GET|HEAD /constants
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $constants = $this->constantRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($constants->toArray(), 'Constants retrieved successfully');
    }

    /**
     * Store a newly created Constant in storage.
     * POST /constants
     *
     * @param CreateConstantAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateConstantAPIRequest $request)
    {
        $input = $request->all();

        $constant = $this->constantRepository->create($input);

        return $this->sendResponse($constant->toArray(), 'Constant saved successfully');
    }

    /**
     * Display the specified Constant.
     * GET|HEAD /constants/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Constant $constant */
        $constant = $this->constantRepository->find($id);

        if (empty($constant)) {
            return $this->sendError('Constant not found');
        }

        return $this->sendResponse($constant->toArray(), 'Constant retrieved successfully');
    }

    public function defaultSeasonPlans()
    {
        $defaultSpConstant = Constant::where('key', 'DEFAULT_SEASON_PLANS')->first();
        $defaultSp = json_decode($defaultSpConstant['value']);
        $defaultSpArray = json_decode(json_encode($defaultSp), true);

        foreach ($defaultSpArray as $key => $value) {
            $valueArray = json_decode(json_encode($value), true);
            $sp = SeasonPlan::find($valueArray['season_plan_id']);
            $spd = SeasonPlanDetail::find($valueArray['season_plan_detail_id']);

            $defaultSpArray[$key]['seasonPlan'] = $sp->name;
            $defaultSpArray[$key]['seasonPlanDetail'] = $spd->name;
        }

        return $this->sendResponse($defaultSpArray, 'Constant retrieved successfully');
    }

    /**
     * Update the specified Constant in storage.
     * PUT/PATCH /constants/{id}
     *
     * @param int $id
     * @param UpdateConstantAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConstantAPIRequest $request)
    {
        $input = $request->all();

        /** @var Constant $constant */
        $constant = $this->constantRepository->find($id);

        if (empty($constant)) {
            return $this->sendError('Constant not found');
        }

        $constant = $this->constantRepository->update($input, $id);

        return $this->sendResponse($constant->toArray(), 'Constant updated successfully');
    }

    public function indexParameter($module)
    {
        $keyName = 'PARAMETER_' . $module;
        $constantData = Constant::where('key', $keyName)->first();
        if (isset($constantData)) {
            $data = json_decode($constantData->value);
            $filteredData = [];
            foreach ($data as $key => $value) {
                if (!isset($value->deleted)) {
                    array_push($filteredData, $value);
                }
            }
            return $this->sendResponse($filteredData, 'Constant retrived successfully');
        } else {
            return $this->sendResponse([], 'Constant retrived successfully');
            // return $this->sendError('Constant not found');
        }
    }

    public function findParameter($module, $id, $asAPI = true)
    {
        $keyName = 'PARAMETER_' . $module;
        $constantData = Constant::where('key', $keyName)->first();
        if (isset($constantData)) {
            $data = json_decode($constantData->value);
            $found_key = array_search($id, array_column($data, 'id'));
            $customParameter = $data[$found_key];
            if ($asAPI) return $this->sendResponse($customParameter, 'Constant saved successfully');
            return $customParameter;
        } else {
            return $this->sendError('Constant not found');
        }
    }

    public function destroyParameter($module, $id)
    {
        $keyName = 'PARAMETER_' . $module;
        $constantData = Constant::where('key', $keyName)->first();
        if (isset($constantData)) {
            $data = json_decode($constantData->value);
            foreach ($data as $key => $value) {
                if ($value->id == $id) {
                    $deletedData = $value;
                    $deletedData->deleted = true;
                    $data[$key] = $deletedData;
                }
            }
            $constantData->value = json_encode($data);
            $constantData->save();
            return $this->sendResponse($data, 'Constant saved successfully');
        } else {
            return $this->sendError('Constant not found');
        }
    }

    public function storeParameter($module, Request $request)
    {
        $input = $request->all();
        $keyName = 'PARAMETER_' . $module;

        if (!$request->has(['name', 'type', 'parameters_id'])) {
            return $this->sendError('Constant not found');
        }

        $constantData = Constant::where('key', $keyName)->first();
        if (isset($constantData)) {
            $data = json_decode($constantData->value);
            if ($request->has('id')) {
                foreach ($data as $key => $value) {
                    if ($value->id === $input['id']) {
                        $newData = $input;
                        $data[$key] = $newData;
                    }
                }
            } else {
                $newData = [
                    'id' => count($data) + 1,
                    'name' => $input['name'],
                    'type' => $input['type'],
                    'parameters_id' => $input['parameters_id'],
                ];
                array_push($data, $newData);
            }
            $constantData->value = json_encode($data);
            $constantData->save();
        } else {
            $constantData = new Constant();
            $data = [];
            $newData = [
                'id' => 1,
                'name' => $input['name'],
                'type' => $input['type'],
                'parameters_id' => $input['parameters_id'],
            ];
            array_push($data, $newData);
            $constantData->key = $keyName;
            $constantData->value = json_encode($data);
            $constantData->save();
        }

        return $this->sendResponse($newData, 'Constant updated successfully');
    }

    public function storeHelp($name, Request $request)
    {
        $input = $request->all();

        $help = Constant::updateOrCreate(
            ['key' => 'HELP_' . $name],
            ['value' => $input['value'],]
        );

        return $this->sendResponse($help, 'Help saved successfully');
    }

    public function showHelp($name)
    {
        $help = Constant::where('key', 'HELP_' . $name)->first();
        if (isset($help)) {
            $value = $help->value;
            return $this->sendResponse($value, 'Help retrived successfully');
        }
        return $this->sendError('Constant not found');
    }

    /**
     * Remove the specified Constant from storage.
     * DELETE /constants/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Constant $constant */
        $constant = $this->constantRepository->find($id);

        if (empty($constant)) {
            return $this->sendError('Constant not found');
        }

        $constant->delete();

        return $this->sendSuccess('Constant deleted successfully');
    }
}
