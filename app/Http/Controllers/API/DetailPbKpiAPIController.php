<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetailPbKpiAPIRequest;
use App\Http\Requests\API\UpdateDetailPbKpiAPIRequest;
use App\Models\DetailPbKpi;
use App\Repositories\DetailPbKpiRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailkpiEvent;
use App\Models\DetailKpiPhysic;
use App\Models\HeaderKpi;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class DetailPbKpiController
 * @package App\Http\Controllers\API
 */

class DetailPbKpiAPIController extends AppBaseController
{
    /** @var  DetailPbKpiRepository */
    private $detailPbKpiRepository;

    public function __construct(DetailPbKpiRepository $detailPbKpiRepo)
    {
        $this->detailPbKpiRepository = $detailPbKpiRepo;
    }

    /**
     * Display a listing of the DetailPbKpi.
     * GET|HEAD /detailPbKpis
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('pemain_id') !== null && $request->query('tanggal_kpi') !== null) {
            $kpi = [];

            $events = DetailkpiEvent::where('pemain_id', $request->query('pemain_id'))
                ->where('tanggal_kpi', $request->query('tanggal_kpi'))->get()->toArray();

            $physics = DetailKpiPhysic::where('pemain_id', $request->query('pemain_id'))
                ->where('tanggal_kpi', $request->query('tanggal_kpi'))->get()->toArray();

            $kpi['events'] = $events;
            $kpi['physics'] = $physics;

            return $this->sendResponse($kpi, 'Kpis retrieved successfully');
        }

        $detailPbKpis = $this->detailPbKpiRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailPbKpis->toArray(), 'Detail Pb Kpis retrieved successfully');
    }

    /**
     * Store a newly created DetailPbKpi in storage.
     * POST /detailPbKpis
     *
     * @param CreateDetailPbKpiAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        // return $this->sendResponse($input, 'Kpi saved successfully');

        DB::transaction(function () use ($input) {
            HeaderKpi::updateOrCreate(['pemain_id' => $input['player_id'], 'tanggal_kpi' => $input['date']], $input);

            $events = $input['events'];
            foreach ($events as $key => $value) {
                if (isset($value['id'])) {
                    $event = DetailkpiEvent::find($value['id']);
                    $event->fill($value);
                    $event->save();
                } else {
                    $event = new DetailkpiEvent;
                    $event->fill($value);
                    $event->save();
                }
            }

            $physics = $input['physics'];
            foreach ($physics as $key => $value) {
                if (isset($value['id'])) {
                    $physic = DetailKpiPhysic::find($value['id']);
                    $physic->fill($value);
                    $physic->save();
                } else {
                    DetailKpiPhysic::create($value);
                }
            }

            $deletedEvents = $input['deletedEvents'];
            foreach ($deletedEvents as $key => $value) {
                DetailkpiEvent::where('id', $value['id'])->delete();
            }

            $deletedPhysics = $input['deletedPhysics'];
            foreach ($deletedPhysics as $key => $value) {
                DetailKpiPhysic::where('id', $value['id'])->delete();
            }
        });

        return $this->sendResponse($input, 'Kpi saved successfully');
    }

    /**
     * Display the specified DetailPbKpi.
     * GET|HEAD /detailPbKpis/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailPbKpi $detailPbKpi */
        $detailPbKpi = $this->detailPbKpiRepository->find($id);

        if (empty($detailPbKpi)) {
            return $this->sendError('Detail Pb Kpi not found');
        }

        return $this->sendResponse($detailPbKpi->toArray(), 'Detail Pb Kpi retrieved successfully');
    }

    /**
     * Update the specified DetailPbKpi in storage.
     * PUT/PATCH /detailPbKpis/{id}
     *
     * @param int $id
     * @param UpdateDetailPbKpiAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailPbKpiAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetailPbKpi $detailPbKpi */
        $detailPbKpi = $this->detailPbKpiRepository->find($id);

        if (empty($detailPbKpi)) {
            return $this->sendError('Detail Pb Kpi not found');
        }

        $detailPbKpi = $this->detailPbKpiRepository->update($input, $id);

        return $this->sendResponse($detailPbKpi->toArray(), 'DetailPbKpi updated successfully');
    }

    /**
     * Remove the specified DetailPbKpi from storage.
     * DELETE /detailPbKpis/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        DB::transaction(function () use ($id) {
            $headerKpi = HeaderKpi::find($id);
            DetailkpiEvent::where('pemain_id', $headerKpi->pemain_id)
                ->where('tanggal_kpi', $headerKpi->tanggal_kpi)->delete();
            DetailKpiPhysic::where('pemain_id', $headerKpi->pemain_id)
                ->where('tanggal_kpi', $headerKpi->tanggal_kpi)->delete();
            $headerKpi->delete();
        });

        return $this->sendSuccess('Detail Pb Kpi deleted successfully');
    }
}
