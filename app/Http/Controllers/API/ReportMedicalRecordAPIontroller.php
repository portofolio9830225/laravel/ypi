<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\MedicalRecord;
use App\Reports\MedicalRecordReport;
use Illuminate\Http\Request;

class ReportMedicalRecordAPIontroller extends Controller
{
    public function global()
    {
        return response()->json([
            'message'   => 'Show Medical Record',
            'data'      => MedicalRecord::where('master_ypi_id', request()->ypi_id)->where('pemain_id', request()->player_name_id)->get()
        ]);
    }
}
