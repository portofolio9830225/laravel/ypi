<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateFitnessToolAPIRequest;
use App\Http\Requests\API\UpdateFitnessToolAPIRequest;
use App\Models\FitnessTool;
use App\Repositories\FitnessToolRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\Constant;
use App\Models\KpiResult;
use App\Models\MasterPemain;
use App\Models\TabelKelompok;
use Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class FitnessToolController
 * @package App\Http\Controllers\API
 */

class FitnessToolAPIController extends AppBaseController
{
    /** @var  FitnessToolRepository */
    private $fitnessToolRepository;

    public function __construct(FitnessToolRepository $fitnessToolRepo)
    {
        $this->fitnessToolRepository = $fitnessToolRepo;
    }

    /**
     * Display a listing of the FitnessTool.
     * GET|HEAD /fitnessTools
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (
            $request->query('tanggal') != null
            && $request->query('gender') != null
            && $request->query('kelompok_id') != null
            && $request->query('pemain_id') != null
        ) {

            $pemain_id = $request->query('pemain_id');
            $kelompok_id = $request->query('kelompok_id');
            $gender = $request->query('gender');
            $tanggal = $request->query('tanggal');

            $result = DB::select("SELECT 
            MAX(squat) AS squat_best,
            MAX(bench_press) AS bench_press_best,
            MAX(beep_test) AS beep_test_best,
            MAX(court_agility) AS court_agility_best,
            MAX(cj_actual) AS cj_best,
            MAX(sq_actual) AS sq_best,
            MAX(rl_actual) AS rl_best,
            MAX(ll_actual) AS ll_best
        FROM
            " . DB::connection()->getDatabaseName() . ".fitness_tools
        WHERE
            pemain_id = " . $pemain_id . " AND kelompok_id = " . $kelompok_id . "
                AND gender = '" . $gender . "';");

            $fitnessTools = FitnessTool::where('tanggal', $tanggal)
                ->where('gender', $gender)
                ->where('kelompok_id', $kelompok_id)
                ->where('pemain_id', $pemain_id)
                ->first();

            $res = $fitnessTools;
            $resultArray = json_decode(json_encode($result), true);

            $beepTestIdConstant = Constant::where('key', 'KPI_BEEP_TEST_ID')->first();
            $beepTestId = $beepTestIdConstant->value;
            $ca1IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_1_ID')->first();
            $ca1Id = $ca1IdConstant->value;
            $ca2IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_2_ID')->first();
            $ca2Id = $ca2IdConstant->value;
            $ca3IdConstant = Constant::where('key', 'KPI_COURT_AGILITY_3_ID')->first();
            $ca3Id = $ca3IdConstant->value;
            $benchpressIdConstant = Constant::where('key', 'KPI_BENCH_PRESS_ID')->first();
            $benchpressId = $benchpressIdConstant->value;
            $squatIdConstant = Constant::where('key', 'KPI_SQUAT_ID')->first();
            $squatId = $squatIdConstant->value;

            $beepTest = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $beepTestId)
                ->first();
            $ca1 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca1Id)
                ->first();
            $ca2 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca2Id)
                ->first();
            $ca3 = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $ca3Id)
                ->first();
            $benchpress = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $benchpressId)
                ->first();
            $squat = KpiResult::where('date', $tanggal)
                ->where('player_id', $pemain_id)
                ->where('kpi_parameter_id', $squatId)
                ->first();

            $data = [];
            if ($res != null) {
                $res = $res->toArray();
                $data = array_merge($res, $resultArray[0]);
            } else {
                $data = $resultArray[0];
            }

            if (isset($beepTest)) {
                $data['beep_test'] = (float) $beepTest->value;
            }
            if (isset($benchpress)) {
                $data['bench_press'] = (float) $benchpress->value;
            }
            if (isset($benchpress)) {
                $data['squat'] = (float) $squat->value;
            }
            $caArray = [];
            if (isset($ca1)) {
                array_push($caArray, (float) $ca1->value);
            }
            if (isset($ca2)) {
                array_push($caArray, (float) $ca2->value);
            }
            if (isset($ca3)) {
                array_push($caArray, (float) $ca3->value);
            }
            if (count($caArray) > 0) {
                $data['court_agility'] = (float) min($caArray);
            }


            return $this->sendResponse($data, 'Fitness Tools retrieved successfully');
            // return $result;
        }

        if ($request->query('groupDate') != null) {
            $fitnessTools = FitnessTool::groupBy('tanggal')
                ->get();

            return $this->sendResponse($fitnessTools->toArray(), 'Fitness Tools retrieved successfully');
        }

        $fitnessTools = $this->fitnessToolRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($fitnessTools->toArray(), 'Fitness Tools retrieved successfully');
    }

    public function tableReport()
    {
        $data = FitnessTool::join('tabel_kelompoks', 'fitness_tools.kelompok_id', '=', 'tabel_kelompoks.id')
            ->select(DB::raw('tanggal as date, gender, kelompok_id as group_id, count(*) as count, tabel_kelompoks.kelompok as group_name'))
            ->groupBy('tanggal', 'kelompok_id', 'gender')->get();
        return $this->sendResponse($data, 'Fitness Test retrived successfully');
    }

    public function tableDetailReport($date, $gender, $group_id)
    {
        $tableData = FitnessTool::join('master_pemains', 'fitness_tools.pemain_id', '=', 'master_pemains.id')
            ->select(DB::raw('fitness_tools.*, master_pemains.nama_lengkap as player_name'))
            ->where('tanggal', $date)
            ->where('gender', $gender)
            ->where('fitness_tools.kelompok_id', $group_id)
            ->where('court_agility', '!=', 0)
            ->where('beep_test', '!=', 0)
            ->where('bench_press', '!=', 0)
            ->where('squat', '!=', 0)
            ->with('player', 'group')
            ->get();

        $group = TabelKelompok::where('id', $group_id)->first();

        $data = [
            'table_data' => $tableData,
            'date' => $date,
            'gender' => $gender,
            'group_name' => $group->kelompok,
        ];
        return $this->sendResponse($data, 'Fitness Test retrived successfully');
    }

    /**
     * Store a newly created FitnessTool in storage.
     * POST /fitnessTools
     *
     * @param CreateFitnessToolAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateFitnessToolAPIRequest $request)
    {
        $input = $request->all();

        $fitnessTool = $this->fitnessToolRepository->create($input);

        return $this->sendResponse($fitnessTool->toArray(), 'Fitness Tool saved successfully');
    }

    /**
     * Display the specified FitnessTool.
     * GET|HEAD /fitnessTools/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var FitnessTool $fitnessTool */
        $fitnessTool = $this->fitnessToolRepository->find($id);

        if (empty($fitnessTool)) {
            return $this->sendError('Fitness Tool not found');
        }

        return $this->sendResponse($fitnessTool->toArray(), 'Fitness Tool retrieved successfully');
    }

    /**
     * Update the specified FitnessTool in storage.
     * PUT/PATCH /fitnessTools/{id}
     *
     * @param int $id
     * @param UpdateFitnessToolAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFitnessToolAPIRequest $request)
    {
        $input = $request->all();

        /** @var FitnessTool $fitnessTool */
        $fitnessTool = $this->fitnessToolRepository->find($id);

        if (empty($fitnessTool)) {
            return $this->sendError('Fitness Tool not found');
        }

        $fitnessTool = $this->fitnessToolRepository->update($input, $id);

        return $this->sendResponse($fitnessTool->toArray(), 'FitnessTool updated successfully');
    }

    /**
     * Remove the specified FitnessTool from storage.
     * DELETE /fitnessTools/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var FitnessTool $fitnessTool */
        $fitnessTool = $this->fitnessToolRepository->find($id);

        if (empty($fitnessTool)) {
            return $this->sendError('Fitness Tool not found');
        }

        $fitnessTool->delete();

        return $this->sendSuccess('Fitness Tool deleted successfully');
    }
}
