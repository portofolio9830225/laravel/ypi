<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateHeaderGapAPIRequest;
use App\Http\Requests\API\UpdateHeaderGapAPIRequest;
use App\Models\HeaderGap;
use App\Repositories\HeaderGapRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class HeaderGapController
 * @package App\Http\Controllers\API
 */

class HeaderGapAPIController extends AppBaseController
{
    /** @var  HeaderGapRepository */
    private $headerGapRepository;

    public function __construct(HeaderGapRepository $headerGapRepo)
    {
        $this->headerGapRepository = $headerGapRepo;
    }

    /**
     * Display a listing of the HeaderGap.
     * GET|HEAD /headerGaps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $headerGaps = $this->headerGapRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($headerGaps->toArray(), 'Header Gaps retrieved successfully');
    }

    /**
     * Store a newly created HeaderGap in storage.
     * POST /headerGaps
     *
     * @param CreateHeaderGapAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateHeaderGapAPIRequest $request)
    {
        $input = $request->all();

        $headerGap = $this->headerGapRepository->create($input);

        return $this->sendResponse($headerGap->toArray(), 'Header Gap saved successfully');
    }

    /**
     * Display the specified HeaderGap.
     * GET|HEAD /headerGaps/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var HeaderGap $headerGap */
        $headerGap = $this->headerGapRepository->find($id);

        if (empty($headerGap)) {
            return $this->sendError('Header Gap not found');
        }

        return $this->sendResponse($headerGap->toArray(), 'Header Gap retrieved successfully');
    }

    /**
     * Update the specified HeaderGap in storage.
     * PUT/PATCH /headerGaps/{id}
     *
     * @param int $id
     * @param UpdateHeaderGapAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHeaderGapAPIRequest $request)
    {
        $input = $request->all();

        /** @var HeaderGap $headerGap */
        $headerGap = $this->headerGapRepository->find($id);

        if (empty($headerGap)) {
            return $this->sendError('Header Gap not found');
        }

        $headerGap = $this->headerGapRepository->update($input, $id);

        return $this->sendResponse($headerGap->toArray(), 'HeaderGap updated successfully');
    }

    /**
     * Remove the specified HeaderGap from storage.
     * DELETE /headerGaps/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var HeaderGap $headerGap */
        $headerGap = $this->headerGapRepository->find($id);

        if (empty($headerGap)) {
            return $this->sendError('Header Gap not found');
        }

        $headerGap->delete();

        return $this->sendSuccess('Header Gap deleted successfully');
    }
}
