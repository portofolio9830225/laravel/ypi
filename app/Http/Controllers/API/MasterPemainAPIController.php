<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterPemainAPIRequest;
use App\Http\Requests\API\UpdateMasterPemainAPIRequest;
use App\Models\MasterPemain;
use App\Repositories\MasterPemainRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MasterPemainController
 * @package App\Http\Controllers\API
 */

class MasterPemainAPIController extends AppBaseController
{
    /** @var  MasterPemainRepository */
    private $masterPemainRepository;

    public function __construct(MasterPemainRepository $masterPemainRepo)
    {
        $this->masterPemainRepository = $masterPemainRepo;
    }

    /**
     * Display a listing of the MasterPemain.
     * GET|HEAD /masterPemains
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('kelompok_id') != null && $request->query('gender') != null) {
            $masterPemains = MasterPemain::where('kelompok_id', $request->query('kelompok_id'))
                ->where('sex', $request->query('gender'))
                ->get();

            return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
        }

        // if ($request->query('kelompok_id') != null) {
        //     $masterPemains = MasterPemain::where('kelompok_id', $request->query('kelompok_id'))
        //         ->get();

        //     return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
        // }

        if ($request->has(['group_id', 'coach_id'])) {
            $masterPemains = MasterPemain::where('kelompok_id', $request->query('group_id'))
                ->where('master_pelatih_id', $request->query('coach_id'))
                ->get();

            return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
        }

        if ($request->has('coach_id')) {
            $masterPemains = MasterPemain::where('master_pelatih_id', $request->query('coach_id'))
                ->get();

            return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
        }

        // $masterPemains = $this->masterPemainRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
        $masterPemains = MasterPemain::with('contact', 'group')->get();

        return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
    }

    public function byCoach($coach_id)
    {
        $masterPemains = MasterPemain::with('group:id,kelompok')->where('master_pelatih_id', $coach_id)->get();

        return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
    }

    public function notCoach($coach_id)
    {
        $masterPemains = MasterPemain::with('coach:id,nama_lengkap', 'group:id,kelompok')->where('master_pelatih_id', '<>', $coach_id)
            ->orWhere('master_pelatih_id', null)->get();

        return $this->sendResponse($masterPemains, 'Master Pemains retrieved successfully');
    }

    /**
     * Store a newly created MasterPemain in storage.
     * POST /masterPemains
     *
     * @param CreateMasterPemainAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $masterPemain = $this->masterPemainRepository->create($input);
        // $masterPemain = MasterPemain::create($input);

        return $this->sendResponse($masterPemain->toArray(), 'Master Pemain saved successfully');
    }

    /**
     * Display the specified MasterPemain.
     * GET|HEAD /masterPemains/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MasterPemain $masterPemain */
        $masterPemain = $this->masterPemainRepository->find($id);

        if (empty($masterPemain)) {
            return $this->sendError('Master Pemain not found');
        }

        return $this->sendResponse($masterPemain->toArray(), 'Master Pemain retrieved successfully');
    }

    /**
     * Update the specified MasterPemain in storage.
     * PUT/PATCH /masterPemains/{id}
     *
     * @param int $id
     * @param UpdateMasterPemainAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var MasterPemain $masterPemain */
        $masterPemain = $this->masterPemainRepository->find($id);

        if (empty($masterPemain)) {
            return $this->sendError('Master Pemain not found');
        }

        $masterPemain = $this->masterPemainRepository->update($input, $id);

        return $this->sendResponse($masterPemain->toArray(), 'MasterPemain updated successfully');
    }

    /**
     * Remove the specified MasterPemain from storage.
     * DELETE /masterPemains/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MasterPemain $masterPemain */
        $masterPemain = $this->masterPemainRepository->find($id);

        if (empty($masterPemain)) {
            return $this->sendError('Master Pemain not found');
        }

        $masterPemain->delete();

        return $this->sendSuccess('Master Pemain deleted successfully');
    }

    // public function pelatih()
    // {
    //     MasterPemain::find(request()->pemain_id)->update([
    //         'master_pelatih_id' => request()->pelatih_id
    //     ]);

    //     return $this->sendSuccess('Add Pelatih in Pemain successfully');
    // }

    public function pelatih(Request $request)
    {
        $input = $request->all();

        $player = MasterPemain::find($input['pemain_id']);
        $player->master_pelatih_id = $input['pelatih_id'];
        $player->save();

        return $this->sendSuccess('Add Pelatih in Pemain successfully');
    }

    public function removePelatih(Request $request)
    {
        $input = $request->all();
        $player = MasterPemain::find($input['pemain_id']);
        $player->master_pelatih_id = null;
        $player->save();
    }
}
