<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKpiIndicatorAPIRequest;
use App\Http\Requests\API\UpdateKpiIndicatorAPIRequest;
use App\Models\KpiIndicator;
use App\Repositories\KpiIndicatorRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class KpiIndicatorController
 * @package App\Http\Controllers\API
 */

class KpiIndicatorAPIController extends AppBaseController
{
    /** @var  KpiIndicatorRepository */
    private $kpiIndicatorRepository;

    public function __construct(KpiIndicatorRepository $kpiIndicatorRepo)
    {
        $this->kpiIndicatorRepository = $kpiIndicatorRepo;
    }

    /**
     * Display a listing of the KpiIndicator.
     * GET|HEAD /kpiIndicators
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('kpi_indicator_id') != null) {
            $kpiIndicators = KpiIndicator::where('kpi_indicator_id', $request->query('kpi_indicator_id'))->get();

            return $this->sendResponse($kpiIndicators->toArray(), 'Kpi Indicators retrieved successfully');
        }

        $kpiIndicators = $this->kpiIndicatorRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($kpiIndicators->toArray(), 'Kpi Indicators retrieved successfully');
    }

    /**
     * Store a newly created KpiIndicator in storage.
     * POST /kpiIndicators
     *
     * @param CreateKpiIndicatorAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateKpiIndicatorAPIRequest $request)
    {
        $input = $request->all();

        $kpiIndicator = $this->kpiIndicatorRepository->create($input);

        return $this->sendResponse($kpiIndicator->toArray(), 'Kpi Indicator saved successfully');
    }

    /**
     * Display the specified KpiIndicator.
     * GET|HEAD /kpiIndicators/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var KpiIndicator $kpiIndicator */
        $kpiIndicator = $this->kpiIndicatorRepository->find($id);

        if (empty($kpiIndicator)) {
            return $this->sendError('Kpi Indicator not found');
        }

        return $this->sendResponse($kpiIndicator->toArray(), 'Kpi Indicator retrieved successfully');
    }

    /**
     * Update the specified KpiIndicator in storage.
     * PUT/PATCH /kpiIndicators/{id}
     *
     * @param int $id
     * @param UpdateKpiIndicatorAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKpiIndicatorAPIRequest $request)
    {
        $input = $request->all();

        /** @var KpiIndicator $kpiIndicator */
        $kpiIndicator = $this->kpiIndicatorRepository->find($id);

        if (empty($kpiIndicator)) {
            return $this->sendError('Kpi Indicator not found');
        }

        $kpiIndicator = $this->kpiIndicatorRepository->update($input, $id);

        return $this->sendResponse($kpiIndicator->toArray(), 'KpiIndicator updated successfully');
    }

    /**
     * Remove the specified KpiIndicator from storage.
     * DELETE /kpiIndicators/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var KpiIndicator $kpiIndicator */
        $kpiIndicator = $this->kpiIndicatorRepository->find($id);

        if (empty($kpiIndicator)) {
            return $this->sendError('Kpi Indicator not found');
        }

        $kpiIndicator->delete();

        return $this->sendSuccess('Kpi Indicator deleted successfully');
    }
}
