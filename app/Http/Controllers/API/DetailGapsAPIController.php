<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetailGapsAPIRequest;
use App\Http\Requests\API\UpdateDetailGapsAPIRequest;
use App\Models\DetailGaps;
use App\Repositories\DetailGapsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DetailGapsController
 * @package App\Http\Controllers\API
 */

class DetailGapsAPIController extends AppBaseController
{
    /** @var  DetailGapsRepository */
    private $detailGapsRepository;

    public function __construct(DetailGapsRepository $detailGapsRepo)
    {
        $this->detailGapsRepository = $detailGapsRepo;
    }

    /**
     * Display a listing of the DetailGaps.
     * GET|HEAD /detailGaps
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $detailGaps = $this->detailGapsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailGaps->toArray(), 'Detail Gaps retrieved successfully');
    }

    /**
     * Store a newly created DetailGaps in storage.
     * POST /detailGaps
     *
     * @param CreateDetailGapsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailGapsAPIRequest $request)
    {
        $input = $request->all();

        $detailGaps = $this->detailGapsRepository->create($input);

        return $this->sendResponse($detailGaps->toArray(), 'Detail Gaps saved successfully');
    }

    /**
     * Display the specified DetailGaps.
     * GET|HEAD /detailGaps/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailGaps $detailGaps */
        $detailGaps = $this->detailGapsRepository->find($id);

        if (empty($detailGaps)) {
            return $this->sendError('Detail Gaps not found');
        }

        return $this->sendResponse($detailGaps->toArray(), 'Detail Gaps retrieved successfully');
    }

    /**
     * Update the specified DetailGaps in storage.
     * PUT/PATCH /detailGaps/{id}
     *
     * @param int $id
     * @param UpdateDetailGapsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailGapsAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetailGaps $detailGaps */
        $detailGaps = $this->detailGapsRepository->find($id);

        if (empty($detailGaps)) {
            return $this->sendError('Detail Gaps not found');
        }

        $detailGaps = $this->detailGapsRepository->update($input, $id);

        return $this->sendResponse($detailGaps->toArray(), 'DetailGaps updated successfully');
    }

    /**
     * Remove the specified DetailGaps from storage.
     * DELETE /detailGaps/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailGaps $detailGaps */
        $detailGaps = $this->detailGapsRepository->find($id);

        if (empty($detailGaps)) {
            return $this->sendError('Detail Gaps not found');
        }

        $detailGaps->delete();

        return $this->sendSuccess('Detail Gaps deleted successfully');
    }
}
