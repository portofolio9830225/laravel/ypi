<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateYpiGoalAPIRequest;
use App\Http\Requests\API\UpdateYpiGoalAPIRequest;
use App\Models\YpiGoal;
use App\Repositories\YpiGoalRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class YpiGoalController
 * @package App\Http\Controllers\API
 */

class YpiGoalAPIController extends AppBaseController
{
    /** @var  YpiGoalRepository */
    private $ypiGoalRepository;

    public function __construct(YpiGoalRepository $ypiGoalRepo)
    {
        $this->ypiGoalRepository = $ypiGoalRepo;
    }

    /**
     * Display a listing of the YpiGoal.
     * GET|HEAD /ypiGoals
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $ypiGoals = $this->ypiGoalRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ypiGoals->toArray(), 'Ypi Goals retrieved successfully');
    }

    /**
     * Store a newly created YpiGoal in storage.
     * POST /ypiGoals
     *
     * @param CreateYpiGoalAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateYpiGoalAPIRequest $request)
    {
        $input = $request->all();

        $ypiGoal = $this->ypiGoalRepository->create($input);

        return $this->sendResponse($ypiGoal->toArray(), 'Ypi Goal saved successfully');
    }

    /**
     * Display the specified YpiGoal.
     * GET|HEAD /ypiGoals/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var YpiGoal $ypiGoal */
        $ypiGoal = $this->ypiGoalRepository->find($id);

        if (empty($ypiGoal)) {
            return $this->sendError('Ypi Goal not found');
        }

        return $this->sendResponse($ypiGoal->toArray(), 'Ypi Goal retrieved successfully');
    }

    /**
     * Update the specified YpiGoal in storage.
     * PUT/PATCH /ypiGoals/{id}
     *
     * @param int $id
     * @param UpdateYpiGoalAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateYpiGoalAPIRequest $request)
    {
        $input = $request->all();

        /** @var YpiGoal $ypiGoal */
        $ypiGoal = $this->ypiGoalRepository->find($id);

        if (empty($ypiGoal)) {
            return $this->sendError('Ypi Goal not found');
        }

        $ypiGoal = $this->ypiGoalRepository->update($input, $id);

        return $this->sendResponse($ypiGoal->toArray(), 'YpiGoal updated successfully');
    }

    /**
     * Remove the specified YpiGoal from storage.
     * DELETE /ypiGoals/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var YpiGoal $ypiGoal */
        $ypiGoal = $this->ypiGoalRepository->find($id);

        if (empty($ypiGoal)) {
            return $this->sendError('Ypi Goal not found');
        }

        $ypiGoal->delete();

        return $this->sendSuccess('Ypi Goal deleted successfully');
    }
}
