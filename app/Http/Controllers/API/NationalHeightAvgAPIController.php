<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateNationalHeightAvgAPIRequest;
use App\Http\Requests\API\UpdateNationalHeightAvgAPIRequest;
use App\Models\NationalHeightAvg;
use App\Repositories\NationalHeightAvgRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class NationalHeightAvgController
 * @package App\Http\Controllers\API
 */

class NationalHeightAvgAPIController extends AppBaseController
{
    /** @var  NationalHeightAvgRepository */
    private $nationalHeightAvgRepository;

    public function __construct(NationalHeightAvgRepository $nationalHeightAvgRepo)
    {
        $this->nationalHeightAvgRepository = $nationalHeightAvgRepo;
    }

    /**
     * Display a listing of the NationalHeightAvg.
     * GET|HEAD /nationalHeightAvgs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('nation') != null && $request->query('gender') != null) {
            $nationalHeightAvgs = NationalHeightAvg::where('nation', $request->query('nation'))
                ->where('gender', $request->query('gender'))->get();
            return $this->sendResponse($nationalHeightAvgs->toArray(), 'National Height Avgs retrieved successfully');
        }

        if ($request->query('nations') != null) {
            $nationalHeightAvgs = NationalHeightAvg::groupBy('nation')->get();
            return $this->sendResponse($nationalHeightAvgs->toArray(), 'National Height Avgs retrieved successfully');
        }

        $nationalHeightAvgs = $this->nationalHeightAvgRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($nationalHeightAvgs->toArray(), 'National Height Avgs retrieved successfully');
    }

    public function groupByNation(Request $request)
    {
        $nationalHeightAvgs = NationalHeightAvg::groupBy('nation')->groupBy('gender')->get();
        return $this->sendResponse($nationalHeightAvgs->toArray(), 'National Height Avgs retrieved successfully');
    }

    /**
     * Store a newly created NationalHeightAvg in storage.
     * POST /nationalHeightAvgs
     *
     * @param CreateNationalHeightAvgAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateNationalHeightAvgAPIRequest $request)
    {
        $input = $request->all();

        $nationalHeightAvg = $this->nationalHeightAvgRepository->create($input);

        return $this->sendResponse($nationalHeightAvg->toArray(), 'National Height Avg saved successfully');
    }

    public function batchStore(Request $request)
    {
        $input = $request->all();

        foreach ($input as $key => $value) {
            NationalHeightAvg::updateOrCreate([
                'nation' => $value['nation'],
                'age' => $value['age'],
                'gender' => $value['gender'],
            ], ['height' => $value['height']]);
        }
        return $this->sendResponse($input, 'Annual Plan Setting saved successfully');
    }

    /**
     * Display the specified NationalHeightAvg.
     * GET|HEAD /nationalHeightAvgs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var NationalHeightAvg $nationalHeightAvg */
        $nationalHeightAvg = $this->nationalHeightAvgRepository->find($id);

        if (empty($nationalHeightAvg)) {
            return $this->sendError('National Height Avg not found');
        }

        return $this->sendResponse($nationalHeightAvg->toArray(), 'National Height Avg retrieved successfully');
    }

    /**
     * Update the specified NationalHeightAvg in storage.
     * PUT/PATCH /nationalHeightAvgs/{id}
     *
     * @param int $id
     * @param UpdateNationalHeightAvgAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNationalHeightAvgAPIRequest $request)
    {
        $input = $request->all();

        /** @var NationalHeightAvg $nationalHeightAvg */
        $nationalHeightAvg = $this->nationalHeightAvgRepository->find($id);

        if (empty($nationalHeightAvg)) {
            return $this->sendError('National Height Avg not found');
        }

        $nationalHeightAvg = $this->nationalHeightAvgRepository->update($input, $id);

        return $this->sendResponse($nationalHeightAvg->toArray(), 'NationalHeightAvg updated successfully');
    }

    /**
     * Remove the specified NationalHeightAvg from storage.
     * DELETE /nationalHeightAvgs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id, Request $request)
    {
        /** @var NationalHeightAvg $nationalHeightAvg */
        if ($request->query('nation') != null && $request->query('gender') != null) {
            $nationalHeightAvgs = NationalHeightAvg::where('nation', $request->query('nation'))
                ->where('gender', $request->query('gender'))->delete();
            return $this->sendSuccess('National Height Avg deleted successfully');
        }

        $nationalHeightAvg = $this->nationalHeightAvgRepository->find($id);

        if (empty($nationalHeightAvg)) {
            return $this->sendError('National Height Avg not found');
        }

        $nationalHeightAvg->delete();

        // return $this->sendSuccess($request->query('nation'));
        return $this->sendSuccess('National Height Avg deleted successfully');
    }
}
