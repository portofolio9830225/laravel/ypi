<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDetailKpiPhysicAPIRequest;
use App\Http\Requests\API\UpdateDetailKpiPhysicAPIRequest;
use App\Models\DetailKpiPhysic;
use App\Repositories\DetailKpiPhysicRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DetailKpiPhysicController
 * @package App\Http\Controllers\API
 */

class DetailKpiPhysicAPIController extends AppBaseController
{
    /** @var  DetailKpiPhysicRepository */
    private $detailKpiPhysicRepository;

    public function __construct(DetailKpiPhysicRepository $detailKpiPhysicRepo)
    {
        $this->detailKpiPhysicRepository = $detailKpiPhysicRepo;
    }

    /**
     * Display a listing of the DetailKpiPhysic.
     * GET|HEAD /detailKpiPhysics
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $detailKpiPhysics = $this->detailKpiPhysicRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($detailKpiPhysics->toArray(), 'Detail Kpi Physics retrieved successfully');
    }

    /**
     * Store a newly created DetailKpiPhysic in storage.
     * POST /detailKpiPhysics
     *
     * @param CreateDetailKpiPhysicAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDetailKpiPhysicAPIRequest $request)
    {
        $input = $request->all();

        $detailKpiPhysic = $this->detailKpiPhysicRepository->create($input);

        return $this->sendResponse($detailKpiPhysic->toArray(), 'Detail Kpi Physic saved successfully');
    }

    /**
     * Display the specified DetailKpiPhysic.
     * GET|HEAD /detailKpiPhysics/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DetailKpiPhysic $detailKpiPhysic */
        $detailKpiPhysic = $this->detailKpiPhysicRepository->find($id);

        if (empty($detailKpiPhysic)) {
            return $this->sendError('Detail Kpi Physic not found');
        }

        return $this->sendResponse($detailKpiPhysic->toArray(), 'Detail Kpi Physic retrieved successfully');
    }

    /**
     * Update the specified DetailKpiPhysic in storage.
     * PUT/PATCH /detailKpiPhysics/{id}
     *
     * @param int $id
     * @param UpdateDetailKpiPhysicAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDetailKpiPhysicAPIRequest $request)
    {
        $input = $request->all();

        /** @var DetailKpiPhysic $detailKpiPhysic */
        $detailKpiPhysic = $this->detailKpiPhysicRepository->find($id);

        if (empty($detailKpiPhysic)) {
            return $this->sendError('Detail Kpi Physic not found');
        }

        $detailKpiPhysic = $this->detailKpiPhysicRepository->update($input, $id);

        return $this->sendResponse($detailKpiPhysic->toArray(), 'DetailKpiPhysic updated successfully');
    }

    /**
     * Remove the specified DetailKpiPhysic from storage.
     * DELETE /detailKpiPhysics/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DetailKpiPhysic $detailKpiPhysic */
        $detailKpiPhysic = $this->detailKpiPhysicRepository->find($id);

        if (empty($detailKpiPhysic)) {
            return $this->sendError('Detail Kpi Physic not found');
        }

        $detailKpiPhysic->delete();

        return $this->sendSuccess('Detail Kpi Physic deleted successfully');
    }
}
