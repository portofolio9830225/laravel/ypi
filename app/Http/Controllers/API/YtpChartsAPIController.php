<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateYtpChartsAPIRequest;
use App\Http\Requests\API\UpdateYtpChartsAPIRequest;
use App\Models\YtpCharts;
use App\Repositories\YtpChartsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class YtpChartsController
 * @package App\Http\Controllers\API
 */

class YtpChartsAPIController extends AppBaseController
{
    /** @var  YtpChartsRepository */
    private $ytpChartsRepository;

    public function __construct(YtpChartsRepository $ytpChartsRepo)
    {
        $this->ytpChartsRepository = $ytpChartsRepo;
    }

    /**
     * Display a listing of the YtpCharts.
     * GET|HEAD /ytpCharts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $ytpCharts = $this->ytpChartsRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($ytpCharts->toArray(), 'Ytp Charts retrieved successfully');
    }

    /**
     * Store a newly created YtpCharts in storage.
     * POST /ytpCharts
     *
     * @param CreateYtpChartsAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateYtpChartsAPIRequest $request)
    {
        $input = $request->all();

        $ytpCharts = $this->ytpChartsRepository->create($input);

        return $this->sendResponse($ytpCharts->toArray(), 'Ytp Charts saved successfully');
    }

    /**
     * Display the specified YtpCharts.
     * GET|HEAD /ytpCharts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var YtpCharts $ytpCharts */
        $ytpCharts = $this->ytpChartsRepository->find($id);

        if (empty($ytpCharts)) {
            return $this->sendError('Ytp Charts not found');
        }

        return $this->sendResponse($ytpCharts->toArray(), 'Ytp Charts retrieved successfully');
    }

    /**
     * Update the specified YtpCharts in storage.
     * PUT/PATCH /ytpCharts/{id}
     *
     * @param int $id
     * @param UpdateYtpChartsAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateYtpChartsAPIRequest $request)
    {
        $input = $request->all();

        /** @var YtpCharts $ytpCharts */
        $ytpCharts = $this->ytpChartsRepository->find($id);

        if (empty($ytpCharts)) {
            return $this->sendError('Ytp Charts not found');
        }

        $ytpCharts = $this->ytpChartsRepository->update($input, $id);

        return $this->sendResponse($ytpCharts->toArray(), 'YtpCharts updated successfully');
    }

    /**
     * Remove the specified YtpCharts from storage.
     * DELETE /ytpCharts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var YtpCharts $ytpCharts */
        $ytpCharts = $this->ytpChartsRepository->find($id);

        if (empty($ytpCharts)) {
            return $this->sendError('Ytp Charts not found');
        }

        $ytpCharts->delete();

        return $this->sendSuccess('Ytp Charts deleted successfully');
    }
}
