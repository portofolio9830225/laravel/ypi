<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateTrainingAttendanceAPIRequest;
use App\Http\Requests\API\UpdateTrainingAttendanceAPIRequest;
use App\Models\TrainingAttendance;
use App\Repositories\TrainingAttendanceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\MasterPemain;
use App\Models\SettingAttendance;
use App\Models\SettingPresence;
use DatePeriod;
use DateTime;
use DateInterval;
use Response;

/**
 * Class TrainingAttendanceController
 * @package App\Http\Controllers\API
 */

class TrainingAttendanceAPIController extends AppBaseController
{
    /** @var  TrainingAttendanceRepository */
    private $trainingAttendanceRepository;

    public function __construct(TrainingAttendanceRepository $trainingAttendanceRepo)
    {
        $this->trainingAttendanceRepository = $trainingAttendanceRepo;
    }

    /**
     * Display a listing of the TrainingAttendance.
     * GET|HEAD /trainingAttendances
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('kelompok_id') != null && $request->query('master_pelatih_id') != null && $request->query('pemain_id') != null) {
            $trainingAttendances = TrainingAttendance::where('kelompok_id', $request->query('kelompok_id'))
                ->where('master_pelatih_id', $request->query('master_pelatih_id'))
                ->where('pemain_id', $request->query('pemain_id'))
                ->with('status')
                ->get();

            return $this->sendResponse($trainingAttendances->toArray(), 'Training Attendances retrieved successfully');
        }
        $trainingAttendances = $this->trainingAttendanceRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($trainingAttendances->toArray(), 'Training Attendances retrieved successfully');
    }

    public function getSessionsRange(Request $request)
    {
        if ($request->has(['start_date', 'end_date', 'group_id', 'coach_id'])) {
            $input = $request->all();

            $from = date($input['start_date']);
            $to = date($input['end_date']);

            $attendanceSettings = SettingAttendance::whereBetween('tanggal', [$from, $to])
                ->select('session')
                ->where('kelompok', $input['group_id'])
                ->where('kode_pelatih', $input['coach_id'])
                ->groupBy('session')
                ->get();

            $data = [];
            foreach ($attendanceSettings as $key => $value) {
                array_push($data, $value['session']);
            }

            return $this->sendResponse($data, 'Training Attendances retrieved successfully');
        }

        return $this->sendError('Training Attendance not found');
    }

    /**
     * Store a newly created TrainingAttendance in storage.
     * POST /trainingAttendances
     *
     * @param CreateTrainingAttendanceAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateTrainingAttendanceAPIRequest $request)
    {
        $input = $request->all();

        $trainingAttendance = $this->trainingAttendanceRepository->create($input);

        return $this->sendResponse($trainingAttendance->toArray(), 'Training Attendance saved successfully');
    }

    public function storeDaily(Request $request)
    {
        $input = $request->all();
        $trainingPresenceId = 0;

        foreach ($input as $key => $value) {
            if (str_starts_with($key, 'status_') && $value === true) {
                $trainingPresenceId = (int) substr($key, 7);
            }
        }

        TrainingAttendance::updateOrCreate([
            'master_pelatih_id' => $input['coach_id'],
            'pemain_id' => $input['player_id'],
            'tanggal' => $input['date'],
            'session' => $input['session'],
            'kelompok_id' => $input['group_id'],
        ], [
            'presence_id' => $trainingPresenceId,
            'note' => $input['note'],
        ]);
        return $this->sendResponse($input, 'Training Attendance saved successfully');
    }

    public function deleteDaily(Request $request)
    {
        $input = $request->all();

        TrainingAttendance::where([
            'master_pelatih_id' => $input['coach_id'],
            'pemain_id' => $input['player_id'],
            'tanggal' => $input['date'],
            'session' => $input['session'],
            'kelompok_id' => $input['group_id'],
        ])->delete();

        return $this->sendResponse([
            'player_id' => $input['player_id'],
            'player_name' => $input['player_name'],
            'presence_id' => $input['presence_id'],
        ], 'Training Attendance deleted successfully');
    }

    public function showDaily(Request $request)
    {
        $input = $request->all();
        $listPlayers = MasterPemain::where('kelompok_id', $input['group_id'])
            ->get();

        $attendanceStatuses = SettingPresence::where('kelompok_id', $input['group_id'])
            ->where('pelatih_id', $input['coach_id'])
            ->get();

        $presence = SettingPresence::where('kelompok_id', $input['group_id'])
            ->where('pelatih_id', $input['coach_id'])
            ->where('presence', true)
            ->first();

        $data = [];
        foreach ($listPlayers as $key => $value) {
            $existingData = TrainingAttendance::where('kelompok_id', $input['group_id'])
                ->where('master_pelatih_id', $input['coach_id'])
                ->where('pemain_id', $value->id)
                ->where('tanggal', $input['date'])
                ->where('session', $input['session'])->first();

            if (isset($existingData)) {
                array_push($data, [
                    'player_id' => $value->id,
                    'player_name' => $value->nama_lengkap,
                    'presence_id' => $presence->id,
                    'status_' . $existingData->presence_id => true,
                    'note' => $existingData->note,
                ]);
            } else {
                array_push($data, [
                    'player_id' => $value->id,
                    'player_name' => $value->nama_lengkap,
                    'presence_id' => $presence->id,
                ]);
            }
        }
        return $this->sendResponse($data, 'Training Attendance retrived successfully');
    }

    public function batchStore(Request $request)
    {
        if ($request->has(['sessions', 'end_date', 'group_id', 'coach_id', 'player_id', 'presence_id'])) {
            $input = $request->all();
            $endDate = new DateTime($input['end_date']);
            $endDate->add(new DateInterval('P1D'));

            $period = new DatePeriod(
                new DateTime($input['start_date']),
                new DateInterval('P1D'),
                $endDate,
            );

            $data = [];
            foreach ($period as $key => $value) {
                foreach ($input['sessions'] as $k => $v) {
                    $session = SettingAttendance::where('kelompok', $input['group_id'])
                        ->where('kode_pelatih', $input['coach_id'])
                        ->where('tanggal', $value->format('Y-m-d'))
                        ->where('session', $v)
                        ->first();

                    if (isset($session)) {
                        $attendance = TrainingAttendance::updateOrCreate([
                            'kelompok_id' => $input['group_id'],
                            'master_pelatih_id' => $input['coach_id'],
                            'pemain_id' => $input['player_id'],
                            'tanggal' => $value->format('Y-m-d'),
                            'session' => $v,
                        ], [
                            'presence_id' => $input['presence_id'],
                            'note' => $input['note']
                        ]);

                        array_push($data, $attendance);
                    }
                }
            }

            return $this->sendResponse($data, 'Training Attendances retrieved successfully');
        }

        return $this->sendError('Training Attendance not found');
    }
    /**
     * Display the specified TrainingAttendance.
     * GET|HEAD /trainingAttendances/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var TrainingAttendance $trainingAttendance */
        $trainingAttendance = $this->trainingAttendanceRepository->find($id);

        if (empty($trainingAttendance)) {
            return $this->sendError('Training Attendance not found');
        }

        return $this->sendResponse($trainingAttendance->toArray(), 'Training Attendance retrieved successfully');
    }

    /**
     * Update the specified TrainingAttendance in storage.
     * PUT/PATCH /trainingAttendances/{id}
     *
     * @param int $id
     * @param UpdateTrainingAttendanceAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTrainingAttendanceAPIRequest $request)
    {
        $input = $request->all();

        /** @var TrainingAttendance $trainingAttendance */
        $trainingAttendance = $this->trainingAttendanceRepository->find($id);

        if (empty($trainingAttendance)) {
            return $this->sendError('Training Attendance not found');
        }

        $trainingAttendance = $this->trainingAttendanceRepository->update($input, $id);

        return $this->sendResponse($trainingAttendance->toArray(), 'TrainingAttendance updated successfully');
    }

    /**
     * Remove the specified TrainingAttendance from storage.
     * DELETE /trainingAttendances/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var TrainingAttendance $trainingAttendance */
        $trainingAttendance = $this->trainingAttendanceRepository->find($id);

        if (empty($trainingAttendance)) {
            return $this->sendError('Training Attendance not found');
        }

        $trainingAttendance->delete();

        return $this->sendSuccess('Training Attendance deleted successfully');
    }
}
