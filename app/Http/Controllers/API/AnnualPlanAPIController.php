<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateAnnualPlanAPIRequest;
use App\Http\Requests\API\UpdateAnnualPlanAPIRequest;
use App\Models\AnnualPlan;
use App\Repositories\AnnualPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class AnnualPlanController
 * @package App\Http\Controllers\API
 */

class AnnualPlanAPIController extends AppBaseController
{
    /** @var  AnnualPlanRepository */
    private $annualPlanRepository;

    public function __construct(AnnualPlanRepository $annualPlanRepo)
    {
        $this->annualPlanRepository = $annualPlanRepo;
    }

    /**
     * Display a listing of the AnnualPlan.
     * GET|HEAD /annualPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('master_ypi_id') != null) {
            $annualPlanSettings = AnnualPlan::where('master_ypi_id', $request->query('master_ypi_id'))
                ->get();
            $result = $annualPlanSettings->toArray();
            return $this->sendResponse($result, 'Annual Plan Settings retrieved successfully');
        }

        $annualPlans = $this->annualPlanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($annualPlans->toArray(), 'Annual Plans retrieved successfully');
    }

    public function seasonPlanner(Request $request)
    {
        if ($request->query('start') != null && $request->query('end') != null) {
            $ypiPlanner = [];
            $startDate = date_create($request->query('start'));
            $endDate = date_create($request->query('end'));
            while ($startDate < $endDate) {
                $start = clone $startDate;
                $end = clone $startDate;
                $sat = clone $startDate;
                date_modify($sat, '+5 day');
                date_modify($end, '+6 day');
                date_modify($startDate, '+7 day');

                $ypiPlanner[] = [
                    'week' => count($ypiPlanner) + 1,
                    'year' => date_format($start, 'Y'),
                    'month' => date_format($start, 'F'),
                    'month_number' => (int) date_format($start, 'm'),
                    'month_number_str' => date_format($start, 'm'),
                    'monday' => date_format($start, 'd'),
                    'sunday' => date_format($end, 'd'),
                    'saturday' => date_format($sat, 'd'),
                ];
            }
            return $this->sendResponse($ypiPlanner, 'Annual Plans retrieved successfully');
        }
        return $this->sendResponse([], 'Annual Plans retrieved successfully');
    }

    /**
     * Store a newly created AnnualPlan in storage.
     * POST /annualPlans
     *
     * @param CreateAnnualPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateAnnualPlanAPIRequest $request)
    {
        $input = $request->all();

        // $annualPlan = $this->annualPlanRepository->create($input);
        $annualPlan = AnnualPlan::updateOrCreate(
            [
                'master_ypi_id' => $input['master_ypi_id'],
                'annual_plan_setting_id' => $input['annual_plan_setting_id'],
                'week' => $input['week'],
            ],
            $input
        );

        return $this->sendResponse($annualPlan->toArray(), 'Annual Plan saved successfully');
    }

    /**
     * Display the specified AnnualPlan.
     * GET|HEAD /annualPlans/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var AnnualPlan $annualPlan */
        $annualPlan = $this->annualPlanRepository->find($id);

        if (empty($annualPlan)) {
            return $this->sendError('Annual Plan not found');
        }

        return $this->sendResponse($annualPlan->toArray(), 'Annual Plan retrieved successfully');
    }

    /**
     * Update the specified AnnualPlan in storage.
     * PUT/PATCH /annualPlans/{id}
     *
     * @param int $id
     * @param UpdateAnnualPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateAnnualPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var AnnualPlan $annualPlan */
        $annualPlan = $this->annualPlanRepository->find($id);

        if (empty($annualPlan)) {
            return $this->sendError('Annual Plan not found');
        }

        $annualPlan = $this->annualPlanRepository->update($input, $id);

        return $this->sendResponse($annualPlan->toArray(), 'AnnualPlan updated successfully');
    }

    /**
     * Remove the specified AnnualPlan from storage.
     * DELETE /annualPlans/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var AnnualPlan $annualPlan */
        $annualPlan = $this->annualPlanRepository->find($id);

        if (empty($annualPlan)) {
            return $this->sendError('Annual Plan not found');
        }

        $annualPlan->delete();

        return $this->sendSuccess('Annual Plan deleted successfully');
    }

    public function conditionDestroy(Request $request)
    {
        if ($request->has(['master_ypi_id', 'annual_plan_setting_id', 'week'])) {
            $input = $request->all();

            AnnualPlan::where('master_ypi_id', $input['master_ypi_id'])
                ->where('annual_plan_setting_id', $input['annual_plan_setting_id'])
                ->where('week', $input['week'])
                ->delete();

            return $this->sendSuccess('Annual Plan deleted successfully');
        }

        return $this->sendError('Annual Plan not found');
    }
}
