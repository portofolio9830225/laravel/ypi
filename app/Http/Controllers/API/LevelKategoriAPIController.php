<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateLevelKategoriAPIRequest;
use App\Http\Requests\API\UpdateLevelKategoriAPIRequest;
use App\Models\LevelKategori;
use App\Repositories\LevelKategoriRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class LevelKategoriController
 * @package App\Http\Controllers\API
 */

class LevelKategoriAPIController extends AppBaseController
{
    /** @var  LevelKategoriRepository */
    private $levelKategoriRepository;

    public function __construct(LevelKategoriRepository $levelKategoriRepo)
    {
        $this->levelKategoriRepository = $levelKategoriRepo;
    }

    /**
     * Display a listing of the LevelKategori.
     * GET|HEAD /levelKategoris
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $levelKategoris = $this->levelKategoriRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($levelKategoris->toArray(), 'Level Kategoris retrieved successfully');
    }

    /**
     * Store a newly created LevelKategori in storage.
     * POST /levelKategoris
     *
     * @param CreateLevelKategoriAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateLevelKategoriAPIRequest $request)
    {
        $input = $request->all();

        $levelKategori = $this->levelKategoriRepository->create($input);

        return $this->sendResponse($levelKategori->toArray(), 'Level Kategori saved successfully');
    }

    /**
     * Display the specified LevelKategori.
     * GET|HEAD /levelKategoris/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var LevelKategori $levelKategori */
        $levelKategori = $this->levelKategoriRepository->find($id);

        if (empty($levelKategori)) {
            return $this->sendError('Level Kategori not found');
        }

        return $this->sendResponse($levelKategori->toArray(), 'Level Kategori retrieved successfully');
    }

    /**
     * Update the specified LevelKategori in storage.
     * PUT/PATCH /levelKategoris/{id}
     *
     * @param int $id
     * @param UpdateLevelKategoriAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateLevelKategoriAPIRequest $request)
    {
        $input = $request->all();

        /** @var LevelKategori $levelKategori */
        $levelKategori = $this->levelKategoriRepository->find($id);

        if (empty($levelKategori)) {
            return $this->sendError('Level Kategori not found');
        }

        $levelKategori = $this->levelKategoriRepository->update($input, $id);

        return $this->sendResponse($levelKategori->toArray(), 'LevelKategori updated successfully');
    }

    /**
     * Remove the specified LevelKategori from storage.
     * DELETE /levelKategoris/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var LevelKategori $levelKategori */
        $levelKategori = $this->levelKategoriRepository->find($id);

        if (empty($levelKategori)) {
            return $this->sendError('Level Kategori not found');
        }

        $levelKategori->delete();

        return $this->sendSuccess('Level Kategori deleted successfully');
    }
}
