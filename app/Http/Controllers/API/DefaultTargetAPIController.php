<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateDefaultTargetAPIRequest;
use App\Http\Requests\API\UpdateDefaultTargetAPIRequest;
use App\Models\DefaultTarget;
use App\Repositories\DefaultTargetRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class DefaultTargetController
 * @package App\Http\Controllers\API
 */

class DefaultTargetAPIController extends AppBaseController
{
    /** @var  DefaultTargetRepository */
    private $defaultTargetRepository;

    public function __construct(DefaultTargetRepository $defaultTargetRepo)
    {
        $this->defaultTargetRepository = $defaultTargetRepo;
    }

    /**
     * Display a listing of the DefaultTarget.
     * GET|HEAD /defaultTargets
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $defaultTargets = $this->defaultTargetRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($defaultTargets->toArray(), 'Default Targets retrieved successfully');
    }

    /**
     * Store a newly created DefaultTarget in storage.
     * POST /defaultTargets
     *
     * @param CreateDefaultTargetAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateDefaultTargetAPIRequest $request)
    {
        $input = $request->all();

        $defaultTarget = $this->defaultTargetRepository->create($input);

        return $this->sendResponse($defaultTarget->toArray(), 'Default Target saved successfully');
    }

    /**
     * Display the specified DefaultTarget.
     * GET|HEAD /defaultTargets/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var DefaultTarget $defaultTarget */
        $defaultTarget = $this->defaultTargetRepository->find($id);

        if (empty($defaultTarget)) {
            return $this->sendError('Default Target not found');
        }

        return $this->sendResponse($defaultTarget->toArray(), 'Default Target retrieved successfully');
    }

    /**
     * Update the specified DefaultTarget in storage.
     * PUT/PATCH /defaultTargets/{id}
     *
     * @param int $id
     * @param UpdateDefaultTargetAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDefaultTargetAPIRequest $request)
    {
        $input = $request->all();

        /** @var DefaultTarget $defaultTarget */
        $defaultTarget = $this->defaultTargetRepository->find($id);

        if (empty($defaultTarget)) {
            return $this->sendError('Default Target not found');
        }

        $defaultTarget = $this->defaultTargetRepository->update($input, $id);

        return $this->sendResponse($defaultTarget->toArray(), 'DefaultTarget updated successfully');
    }

    /**
     * Remove the specified DefaultTarget from storage.
     * DELETE /defaultTargets/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var DefaultTarget $defaultTarget */
        $defaultTarget = $this->defaultTargetRepository->find($id);

        if (empty($defaultTarget)) {
            return $this->sendError('Default Target not found');
        }

        $defaultTarget->delete();

        return $this->sendSuccess('Default Target deleted successfully');
    }
}
