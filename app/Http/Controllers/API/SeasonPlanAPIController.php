<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeasonPlanAPIRequest;
use App\Http\Requests\API\UpdateSeasonPlanAPIRequest;
use App\Models\SeasonPlan;
use App\Repositories\SeasonPlanRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\SeasonPlanDetail;
use Illuminate\Support\Facades\DB;
use Response;

/**
 * Class SeasonPlanController
 * @package App\Http\Controllers\API
 */

class SeasonPlanAPIController extends AppBaseController
{
    /** @var  SeasonPlanRepository */
    private $seasonPlanRepository;

    public function __construct(SeasonPlanRepository $seasonPlanRepo)
    {
        $this->seasonPlanRepository = $seasonPlanRepo;
    }

    /**
     * Display a listing of the SeasonPlan.
     * GET|HEAD /seasonPlans
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seasonPlans = $this->seasonPlanRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seasonPlans->toArray(), 'Season Plans retrieved successfully');
    }

    /**
     * Store a newly created SeasonPlan in storage.
     * POST /seasonPlans
     *
     * @param CreateSeasonPlanAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSeasonPlanAPIRequest $request)
    {
        $input = $request->all();

        $seasonPlan = $this->seasonPlanRepository->create($input);
        // SeasonPlanDetail::create([
        //     'season_plan_id' => $seasonPlan->id,
        //     'name' => '',
        //     'name' => '',
        //     'input_type' => 'text'
        // ]);
        return $this->sendResponse($seasonPlan->toArray(), 'Season Plan saved successfully');
    }

    /**
     * Display the specified SeasonPlan.
     * GET|HEAD /seasonPlans/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SeasonPlan $seasonPlan */
        $seasonPlan = $this->seasonPlanRepository->find($id);

        if (empty($seasonPlan)) {
            return $this->sendError('Season Plan not found');
        }

        return $this->sendResponse($seasonPlan->toArray(), 'Season Plan retrieved successfully');
    }

    /**
     * Update the specified SeasonPlan in storage.
     * PUT/PATCH /seasonPlans/{id}
     *
     * @param int $id
     * @param UpdateSeasonPlanAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeasonPlanAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeasonPlan $seasonPlan */
        $seasonPlan = $this->seasonPlanRepository->find($id);

        if (empty($seasonPlan)) {
            return $this->sendError('Season Plan not found');
        }

        $seasonPlan = $this->seasonPlanRepository->update($input, $id);

        return $this->sendResponse($seasonPlan->toArray(), 'SeasonPlan updated successfully');
    }

    /**
     * Remove the specified SeasonPlan from storage.
     * DELETE /seasonPlans/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SeasonPlan $seasonPlan */
        $seasonPlan = $this->seasonPlanRepository->find($id);

        if (empty($seasonPlan)) {
            return $this->sendError('Season Plan not found');
        }

        $seasonPlan->delete();

        return $this->sendSuccess('Season Plan deleted successfully');
    }
}
