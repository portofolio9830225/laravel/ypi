<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMedicalRecordAPIRequest;
use App\Http\Requests\API\UpdateMedicalRecordAPIRequest;
use App\Models\MedicalRecord;
use App\Repositories\MedicalRecordRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MedicalRecordController
 * @package App\Http\Controllers\API
 */

class MedicalRecordAPIController extends AppBaseController
{
    /** @var  MedicalRecordRepository */
    private $medicalRecordRepository;

    public function __construct(MedicalRecordRepository $medicalRecordRepo)
    {
        $this->medicalRecordRepository = $medicalRecordRepo;
    }

    /**
     * Display a listing of the MedicalRecord.
     * GET|HEAD /medicalRecords
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('master_ypi_id') != null && $request->query('pemain_id') != null) {
            $medicalRecords = MedicalRecord::where('master_ypi_id', $request->query('master_ypi_id'))
                ->where('pemain_id', $request->query('pemain_id'))->get();

            return $this->sendResponse($medicalRecords->toArray(), 'Medical Records retrieved successfully');
        }

        $medicalRecords = $this->medicalRecordRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($medicalRecords->toArray(), 'Medical Records retrieved successfully');
    }

    public function tableReport()
    {
        $medicalRecords = MedicalRecord::with('player', 'ypi')->groupBy('pemain_id', 'master_ypi_id')->get()->toArray();
        $data = [];
        foreach ($medicalRecords as $key => $value) {
            if (isset($value['player'], $value['ypi'])) {
                array_push($data, [
                    'player_name' => $value['player']['nama_lengkap'],
                    'ypi_desc' => $value['ypi']['keterangan'],
                    'player_id' => $value['pemain_id'],
                    'ypi_id' => $value['master_ypi_id'],
                ]);
            }
        }
        return $this->sendResponse($data, 'Medical Records retrieved successfully');
    }

    public function reportData(Request $request)
    {
        if ($request->has(['player_id', 'ypi_id'])) {
            $input = $request->all();
            $tableData = MedicalRecord::where('pemain_id', $input['player_id'])
                ->where('master_ypi_id', $input['ypi_id'])
                ->get()->toArray();

            $data = [
                'player_name' => $input['player_name'],
                'table_data' => $tableData
            ];
            return $this->sendResponse($data, 'Medical Records retrieved successfully');
        }

        return $this->sendError('Medical Record not found');
    }

    /**
     * Store a newly created MedicalRecord in storage.
     * POST /medicalRecords
     *
     * @param CreateMedicalRecordAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateMedicalRecordAPIRequest $request)
    {
        $input = $request->all();

        $medicalRecord = $this->medicalRecordRepository->create($input);

        return $this->sendResponse($medicalRecord->toArray(), 'Medical Record saved successfully');
    }

    /**
     * Display the specified MedicalRecord.
     * GET|HEAD /medicalRecords/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MedicalRecord $medicalRecord */
        $medicalRecord = $this->medicalRecordRepository->find($id);

        if (empty($medicalRecord)) {
            return $this->sendError('Medical Record not found');
        }

        return $this->sendResponse($medicalRecord->toArray(), 'Medical Record retrieved successfully');
    }

    /**
     * Update the specified MedicalRecord in storage.
     * PUT/PATCH /medicalRecords/{id}
     *
     * @param int $id
     * @param UpdateMedicalRecordAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMedicalRecordAPIRequest $request)
    {
        $input = $request->all();

        /** @var MedicalRecord $medicalRecord */
        $medicalRecord = $this->medicalRecordRepository->find($id);

        if (empty($medicalRecord)) {
            return $this->sendError('Medical Record not found');
        }

        $medicalRecord = $this->medicalRecordRepository->update($input, $id);

        return $this->sendResponse($medicalRecord->toArray(), 'MedicalRecord updated successfully');
    }

    /**
     * Remove the specified MedicalRecord from storage.
     * DELETE /medicalRecords/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MedicalRecord $medicalRecord */
        $medicalRecord = $this->medicalRecordRepository->find($id);

        if (empty($medicalRecord)) {
            return $this->sendError('Medical Record not found');
        }

        $medicalRecord->delete();

        return $this->sendSuccess('Medical Record deleted successfully');
    }
}
