<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateKpiValueAPIRequest;
use App\Http\Requests\API\UpdateKpiValueAPIRequest;
use App\Models\KpiValue;
use App\Repositories\KpiValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Response;

/**
 * Class KpiValueController
 * @package App\Http\Controllers\API
 */

class KpiValueAPIController extends AppBaseController
{
    /** @var  KpiValueRepository */
    private $kpiValueRepository;

    public function __construct(KpiValueRepository $kpiValueRepo)
    {
        $this->kpiValueRepository = $kpiValueRepo;
    }

    /**
     * Display a listing of the KpiValue.
     * GET|HEAD /kpiValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('process_data')) {
            $kpiValues = KpiValue::where('kpi_indicator_id', $request->query('kpi_indicator_id'))
                ->where('player_id', $request->query('player_id'))
                ->get()->toArray();

            $groupedValues = [];
            foreach ($kpiValues as $key => $value) {
                $date = substr($value['date'], 0, 10);
                $data = [
                    'pb_' . $value['kpi_subindicator_id'] => $value['pb'],
                    'actual_' . $value['kpi_subindicator_id'] => $value['actual'],
                    'target_' . $value['kpi_subindicator_id'] => $value['target'],
                    'date' => $date,
                    'group_id' => $value['group_id'],
                ];
                if (isset($groupedValues[$value['group_id']])) {
                    $groupedValues[$value['group_id']] = array_merge($groupedValues[$value['group_id']], $data);
                } else {
                    $groupedValues[$value['group_id']] = $data;
                }
            }

            $arrayValues = [];
            foreach ($groupedValues as $key => $value) {
                $arrayValues[] = $value;
            }
            return $this->sendResponse($arrayValues, 'Kpi Values retrieved successfully');
        }

        $kpiValues = $this->kpiValueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($kpiValues->toArray(), 'Kpi Values retrieved successfully');
    }

    /**
     * Store a newly created KpiValue in storage.
     * POST /kpiValues
     *
     * @param CreateKpiValueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateKpiValueAPIRequest $request)
    {
        $input = $request->all();

        DB::transaction(function () use ($input) {
            $filteredData = [];
            foreach ($input as $key => $value) {
                if ($key === 'date' || $key === 'date_moment' || $key === 'kpi_indicator_id' || $key === 'player_id' || $key === 'group_id') {
                    continue;
                }
                $index = (int) substr($key, strlen($key) - 1);
                $propertyName = str_replace('_' . $index, '', $key);
                $filteredData[$index][$propertyName] = $value;
            }

            foreach ($filteredData as $key => $value) {
                KpiValue::updateOrCreate([
                    'kpi_indicator_id' => $input['kpi_indicator_id'],
                    'kpi_subindicator_id' => $key,
                    'date' => $input['date'],
                    'player_id' => $input['player_id'],
                    'group_id' => $input['group_id'],
                ], $value);
            }
        });

        return $this->sendResponse($input, 'Kpi Value saved successfully');
    }

    /**
     * Display the specified KpiValue.
     * GET|HEAD /kpiValues/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var KpiValue $kpiValue */
        $kpiValue = $this->kpiValueRepository->find($id);

        if (empty($kpiValue)) {
            return $this->sendError('Kpi Value not found');
        }

        return $this->sendResponse($kpiValue->toArray(), 'Kpi Value retrieved successfully');
    }

    /**
     * Update the specified KpiValue in storage.
     * PUT/PATCH /kpiValues/{id}
     *
     * @param int $id
     * @param UpdateKpiValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateKpiValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var KpiValue $kpiValue */
        $kpiValue = $this->kpiValueRepository->find($id);

        if (empty($kpiValue)) {
            return $this->sendError('Kpi Value not found');
        }

        $kpiValue = $this->kpiValueRepository->update($input, $id);

        return $this->sendResponse($kpiValue->toArray(), 'KpiValue updated successfully');
    }

    /**
     * Remove the specified KpiValue from storage.
     * DELETE /kpiValues/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        KpiValue::where('group_id', $id)->delete();

        return $this->sendSuccess('Kpi Value deleted successfully');
    }
}
