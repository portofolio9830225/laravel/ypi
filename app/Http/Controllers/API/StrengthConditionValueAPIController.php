<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateStrengthConditionValueAPIRequest;
use App\Http\Requests\API\UpdateStrengthConditionValueAPIRequest;
use App\Models\StrengthConditionValue;
use App\Repositories\StrengthConditionValueRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class StrengthConditionValueController
 * @package App\Http\Controllers\API
 */

class StrengthConditionValueAPIController extends AppBaseController
{
    /** @var  StrengthConditionValueRepository */
    private $strengthConditionValueRepository;

    public function __construct(StrengthConditionValueRepository $strengthConditionValueRepo)
    {
        $this->strengthConditionValueRepository = $strengthConditionValueRepo;
    }

    /**
     * Display a listing of the StrengthConditionValue.
     * GET|HEAD /strengthConditionValues
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if ($request->query('strength_condition_id') != null && $request->query('strength_condition_player_id') != null) {
            $strengthConditionValues = StrengthConditionValue::where('strength_condition_id', $request->query('strength_condition_id'))
                ->where('strength_condition_player_id', $request->query('strength_condition_player_id'))
                ->get();

            // $res = $strengthConditionValues;
            // if ($res != null) $res->toArray();
            return $this->sendResponse($strengthConditionValues, 'Strength Condition Values retrieved successfully');
        }


        $strengthConditionValues = $this->strengthConditionValueRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($strengthConditionValues->toArray(), 'Strength Condition Values retrieved successfully');
    }

    /**
     * Store a newly created StrengthConditionValue in storage.
     * POST /strengthConditionValues
     *
     * @param CreateStrengthConditionValueAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateStrengthConditionValueAPIRequest $request)
    {
        $input = $request->all();

        $strengthConditionValue = $this->strengthConditionValueRepository->create($input);

        return $this->sendResponse($strengthConditionValue->toArray(), 'Strength Condition Value saved successfully');
    }

    /**
     * Display the specified StrengthConditionValue.
     * GET|HEAD /strengthConditionValues/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var StrengthConditionValue $strengthConditionValue */
        $strengthConditionValue = $this->strengthConditionValueRepository->find($id);

        if (empty($strengthConditionValue)) {
            return $this->sendError('Strength Condition Value not found');
        }

        return $this->sendResponse($strengthConditionValue->toArray(), 'Strength Condition Value retrieved successfully');
    }

    /**
     * Update the specified StrengthConditionValue in storage.
     * PUT/PATCH /strengthConditionValues/{id}
     *
     * @param int $id
     * @param UpdateStrengthConditionValueAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateStrengthConditionValueAPIRequest $request)
    {
        $input = $request->all();

        /** @var StrengthConditionValue $strengthConditionValue */
        $strengthConditionValue = $this->strengthConditionValueRepository->find($id);

        if (empty($strengthConditionValue)) {
            return $this->sendError('Strength Condition Value not found');
        }

        $strengthConditionValue = $this->strengthConditionValueRepository->update($input, $id);

        return $this->sendResponse($strengthConditionValue->toArray(), 'StrengthConditionValue updated successfully');
    }

    /**
     * Remove the specified StrengthConditionValue from storage.
     * DELETE /strengthConditionValues/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var StrengthConditionValue $strengthConditionValue */
        $strengthConditionValue = $this->strengthConditionValueRepository->find($id);

        if (empty($strengthConditionValue)) {
            return $this->sendError('Strength Condition Value not found');
        }

        $strengthConditionValue->delete();

        return $this->sendSuccess('Strength Condition Value deleted successfully');
    }
}
