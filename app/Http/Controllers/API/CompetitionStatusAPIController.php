<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCompetitionStatusAPIRequest;
use App\Http\Requests\API\UpdateCompetitionStatusAPIRequest;
use App\Models\CompetitionStatus;
use App\Repositories\CompetitionStatusRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class CompetitionStatusController
 * @package App\Http\Controllers\API
 */

class CompetitionStatusAPIController extends AppBaseController
{
    /** @var  CompetitionStatusRepository */
    private $competitionStatusRepository;

    public function __construct(CompetitionStatusRepository $competitionStatusRepo)
    {
        $this->competitionStatusRepository = $competitionStatusRepo;
    }

    /**
     * Display a listing of the CompetitionStatus.
     * GET|HEAD /competitionStatuses
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $competitionStatuses = $this->competitionStatusRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($competitionStatuses->toArray(), 'Competition Statuses retrieved successfully');
    }

    /**
     * Store a newly created CompetitionStatus in storage.
     * POST /competitionStatuses
     *
     * @param CreateCompetitionStatusAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $competitionStatus = $this->competitionStatusRepository->create($input);

        return $this->sendResponse($competitionStatus->toArray(), 'Competition Status saved successfully');
    }

    /**
     * Display the specified CompetitionStatus.
     * GET|HEAD /competitionStatuses/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CompetitionStatus $competitionStatus */
        $competitionStatus = $this->competitionStatusRepository->find($id);

        if (empty($competitionStatus)) {
            return $this->sendError('Competition Status not found');
        }

        return $this->sendResponse($competitionStatus->toArray(), 'Competition Status retrieved successfully');
    }

    /**
     * Update the specified CompetitionStatus in storage.
     * PUT/PATCH /competitionStatuses/{id}
     *
     * @param int $id
     * @param UpdateCompetitionStatusAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var CompetitionStatus $competitionStatus */
        $competitionStatus = $this->competitionStatusRepository->find($id);

        if (empty($competitionStatus)) {
            return $this->sendError('Competition Status not found');
        }

        $competitionStatus = $this->competitionStatusRepository->update($input, $id);

        return $this->sendResponse($competitionStatus->toArray(), 'CompetitionStatus updated successfully');
    }

    /**
     * Remove the specified CompetitionStatus from storage.
     * DELETE /competitionStatuses/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CompetitionStatus $competitionStatus */
        $competitionStatus = $this->competitionStatusRepository->find($id);

        if (empty($competitionStatus)) {
            return $this->sendError('Competition Status not found');
        }

        $competitionStatus->delete();

        return $this->sendSuccess('Competition Status deleted successfully');
    }
}
