<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateMasterContactAPIRequest;
use App\Http\Requests\API\UpdateMasterContactAPIRequest;
use App\Models\MasterContact;
use App\Repositories\MasterContactRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class MasterContactController
 * @package App\Http\Controllers\API
 */

class MasterContactAPIController extends AppBaseController
{
    /** @var  MasterContactRepository */
    private $masterContactRepository;

    public function __construct(MasterContactRepository $masterContactRepo)
    {
        $this->masterContactRepository = $masterContactRepo;
    }

    /**
     * Display a listing of the MasterContact.
     * GET|HEAD /masterContacts
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $masterContacts = $this->masterContactRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($masterContacts->toArray(), 'Master Contacts retrieved successfully');
    }

    /**
     * Store a newly created MasterContact in storage.
     * POST /masterContacts
     *
     * @param CreateMasterContactAPIRequest $request
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $masterContact = $this->masterContactRepository->create($input);

        return $this->sendResponse($masterContact->toArray(), 'Master Contact saved successfully');
    }

    /**
     * Display the specified MasterContact.
     * GET|HEAD /masterContacts/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var MasterContact $masterContact */
        $masterContact = $this->masterContactRepository->find($id);

        if (empty($masterContact)) {
            return $this->sendError('Master Contact not found');
        }

        return $this->sendResponse($masterContact->toArray(), 'Master Contact retrieved successfully');
    }

    /**
     * Update the specified MasterContact in storage.
     * PUT/PATCH /masterContacts/{id}
     *
     * @param int $id
     * @param UpdateMasterContactAPIRequest $request
     *
     * @return Response
     */
    public function update($id, Request $request)
    {
        $input = $request->all();

        /** @var MasterContact $masterContact */
        $masterContact = $this->masterContactRepository->find($id);

        if (empty($masterContact)) {
            return $this->sendError('Master Contact not found');
        }

        $masterContact = $this->masterContactRepository->update($input, $id);

        return $this->sendResponse($masterContact->toArray(), 'MasterContact updated successfully');
    }

    /**
     * Remove the specified MasterContact from storage.
     * DELETE /masterContacts/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var MasterContact $masterContact */
        $masterContact = $this->masterContactRepository->find($id);

        if (empty($masterContact)) {
            return $this->sendError('Master Contact not found');
        }

        $masterContact->delete();

        return $this->sendSuccess('Master Contact deleted successfully');
    }
}
