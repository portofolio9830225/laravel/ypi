<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSubExerciseAPIRequest;
use App\Http\Requests\API\UpdateSubExerciseAPIRequest;
use App\Models\SubExercise;
use App\Repositories\SubExerciseRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SubExerciseController
 * @package App\Http\Controllers\API
 */

class SubExerciseAPIController extends AppBaseController
{
    /** @var  SubExerciseRepository */
    private $subExerciseRepository;

    public function __construct(SubExerciseRepository $subExerciseRepo)
    {
        $this->subExerciseRepository = $subExerciseRepo;
    }

    /**
     * Display a listing of the SubExercise.
     * GET|HEAD /subExercises
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        if (
            $request->query('gym_type_id') != null
        ) {
            $subExercises = SubExercise::where('gym_type_id', $request->query('gym_type_id'))
                ->get();

            return $this->sendResponse($subExercises->toArray(), 'Sub Exercises retrieved successfully');
        }
        // $subExercises = $this->subExerciseRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );
        $subExercises = SubExercise::with('joinAction', 'muscleGroup')->get();

        return $this->sendResponse($subExercises->toArray(), 'Sub Exercises retrieved successfully');
    }

    /**
     * Store a newly created SubExercise in storage.
     * POST /subExercises
     *
     * @param CreateSubExerciseAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSubExerciseAPIRequest $request)
    {
        $input = $request->all();

        $subExercise = $this->subExerciseRepository->create($input);

        return $this->sendResponse($subExercise->toArray(), 'Sub Exercise saved successfully');
    }

    /**
     * Display the specified SubExercise.
     * GET|HEAD /subExercises/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SubExercise $subExercise */
        $subExercise = $this->subExerciseRepository->find($id);

        if (empty($subExercise)) {
            return $this->sendError('Sub Exercise not found');
        }

        return $this->sendResponse($subExercise->toArray(), 'Sub Exercise retrieved successfully');
    }

    /**
     * Update the specified SubExercise in storage.
     * PUT/PATCH /subExercises/{id}
     *
     * @param int $id
     * @param UpdateSubExerciseAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSubExerciseAPIRequest $request)
    {
        $input = $request->all();

        /** @var SubExercise $subExercise */
        $subExercise = $this->subExerciseRepository->find($id);

        if (empty($subExercise)) {
            return $this->sendError('Sub Exercise not found');
        }

        $subExercise = $this->subExerciseRepository->update($input, $id);

        return $this->sendResponse($subExercise->toArray(), 'SubExercise updated successfully');
    }

    /**
     * Remove the specified SubExercise from storage.
     * DELETE /subExercises/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SubExercise $subExercise */
        $subExercise = $this->subExerciseRepository->find($id);

        if (empty($subExercise)) {
            return $this->sendError('Sub Exercise not found');
        }

        $subExercise->delete();

        return $this->sendSuccess('Sub Exercise deleted successfully');
    }
}
