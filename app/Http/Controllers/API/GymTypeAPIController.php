<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateGymTypeAPIRequest;
use App\Http\Requests\API\UpdateGymTypeAPIRequest;
use App\Models\GymType;
use App\Repositories\GymTypeRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class GymTypeController
 * @package App\Http\Controllers\API
 */

class GymTypeAPIController extends AppBaseController
{
    /** @var  GymTypeRepository */
    private $gymTypeRepository;

    public function __construct(GymTypeRepository $gymTypeRepo)
    {
        $this->gymTypeRepository = $gymTypeRepo;
    }

    /**
     * Display a listing of the GymType.
     * GET|HEAD /gymTypes
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $gymTypes = $this->gymTypeRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($gymTypes->toArray(), 'Gym Types retrieved successfully');
    }

    /**
     * Store a newly created GymType in storage.
     * POST /gymTypes
     *
     * @param CreateGymTypeAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateGymTypeAPIRequest $request)
    {
        $input = $request->all();

        $gymType = $this->gymTypeRepository->create($input);

        return $this->sendResponse($gymType->toArray(), 'Gym Type saved successfully');
    }

    /**
     * Display the specified GymType.
     * GET|HEAD /gymTypes/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var GymType $gymType */
        $gymType = $this->gymTypeRepository->find($id);

        if (empty($gymType)) {
            return $this->sendError('Gym Type not found');
        }

        return $this->sendResponse($gymType->toArray(), 'Gym Type retrieved successfully');
    }

    /**
     * Update the specified GymType in storage.
     * PUT/PATCH /gymTypes/{id}
     *
     * @param int $id
     * @param UpdateGymTypeAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGymTypeAPIRequest $request)
    {
        $input = $request->all();

        /** @var GymType $gymType */
        $gymType = $this->gymTypeRepository->find($id);

        if (empty($gymType)) {
            return $this->sendError('Gym Type not found');
        }

        $gymType = $this->gymTypeRepository->update($input, $id);

        return $this->sendResponse($gymType->toArray(), 'GymType updated successfully');
    }

    /**
     * Remove the specified GymType from storage.
     * DELETE /gymTypes/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var GymType $gymType */
        $gymType = $this->gymTypeRepository->find($id);

        if (empty($gymType)) {
            return $this->sendError('Gym Type not found');
        }

        $gymType->delete();

        return $this->sendSuccess('Gym Type deleted successfully');
    }
}
