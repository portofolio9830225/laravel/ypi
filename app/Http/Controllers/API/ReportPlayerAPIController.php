<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Reports\PlayerReport;

/**
 * Class AnnualPlanController
 * @package App\Http\Controllers\API
 */

class ReportPlayerAPIController extends AppBaseController
{
    private $playerReport;

    public function __construct(PlayerReport $playerRpt)
    {
        $this->playerReport = $playerRpt;
    }

    public function global(Request $request)
    {
        $input = $request->all();

        $this->playerReport->global($input);

        $pathToFile = storage_path('/app/player.pdf');
        return response()->download($pathToFile);
        // return $this->sendSuccess('Ok');
    }

    public function specialist(Request $request)
    {
        $input = $request->all();

        $this->playerReport->specialist($input);

        $pathToFile = storage_path('/app/player.pdf');
        return response()->download($pathToFile);
    }

    public function age(Request $request)
    {
        $input = $request->all();
        $this->playerReport->age($input);
        $pathToFile = storage_path('/app/player.pdf');
        return response()->download($pathToFile);
    }
}
