<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateCompetitionReportAPIRequest;
use App\Http\Requests\API\UpdateCompetitionReportAPIRequest;
use App\Models\CompetitionReport;
use App\Repositories\CompetitionReportRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use App\Models\DetailCompetitionReport;
use Response;

/**
 * Class CompetitionReportController
 * @package App\Http\Controllers\API
 */

class CompetitionReportAPIController extends AppBaseController
{
    /** @var  CompetitionReportRepository */
    private $competitionReportRepository;

    public function __construct(CompetitionReportRepository $competitionReportRepo)
    {
        $this->competitionReportRepository = $competitionReportRepo;
    }

    /**
     * Display a listing of the CompetitionReport.
     * GET|HEAD /competitionReports
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        // $competitionReports = $this->competitionReportRepository->all(
        //     $request->except(['skip', 'limit']),
        //     $request->get('skip'),
        //     $request->get('limit')
        // );

        $competitionReports = CompetitionReport::with('player', 'competitionInfo', 'resultCategory')->get();

        return $this->sendResponse($competitionReports->toArray(), 'Competition Reports retrieved successfully');
    }

    /**
     * Store a newly created CompetitionReport in storage.
     * POST /competitionReports
     *
     * @param CreateCompetitionReportAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateCompetitionReportAPIRequest $request)
    {
        $input = $request->all();

        $competitionReport = $this->competitionReportRepository->create($input);

        return $this->sendResponse($competitionReport->toArray(), 'Competition Report saved successfully');
    }

    public function storeWithDetailData(Request $request)
    {
        $input = $request->all();
        if (isset($input['id'])) {
            $competitionReport = CompetitionReport::find($input['id']);
            $competitionReport->fill($input);
            $competitionReport->save();
        } else {
            $competitionReport = CompetitionReport::create($input);
        }

        foreach ($input['detailData'] as $key => $value) {
            if (isset($value['id'])) {
                $detailCompetitionReport = DetailCompetitionReport::find($value['id']);
                $detailCompetitionReport->fill($value);
                $detailCompetitionReport->save();
            } else {
                $value['competition_report_id'] = $competitionReport['id'];
                $detailCompetitionReport = DetailCompetitionReport::create($value);
            }
        }

        return $this->sendResponse($competitionReport->toArray(), 'Competition Report saved successfully');
    }

    /**
     * Display the specified CompetitionReport.
     * GET|HEAD /competitionReports/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var CompetitionReport $competitionReport */
        $competitionReport = $this->competitionReportRepository->find($id);

        if (empty($competitionReport)) {
            return $this->sendError('Competition Report not found');
        }

        return $this->sendResponse($competitionReport->toArray(), 'Competition Report retrieved successfully');
    }

    /**
     * Update the specified CompetitionReport in storage.
     * PUT/PATCH /competitionReports/{id}
     *
     * @param int $id
     * @param UpdateCompetitionReportAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCompetitionReportAPIRequest $request)
    {
        $input = $request->all();

        /** @var CompetitionReport $competitionReport */
        $competitionReport = $this->competitionReportRepository->find($id);

        if (empty($competitionReport)) {
            return $this->sendError('Competition Report not found');
        }

        $competitionReport = $this->competitionReportRepository->update($input, $id);

        return $this->sendResponse($competitionReport->toArray(), 'CompetitionReport updated successfully');
    }

    /**
     * Remove the specified CompetitionReport from storage.
     * DELETE /competitionReports/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var CompetitionReport $competitionReport */
        $competitionReport = $this->competitionReportRepository->find($id);

        if (empty($competitionReport)) {
            return $this->sendError('Competition Report not found');
        }

        $competitionReport->delete();
        DetailCompetitionReport::where('competition_report_id', $id)->delete();

        return $this->sendSuccess('Competition Report deleted successfully');
    }
}
