<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateSeasonPlannerDetailAPIRequest;
use App\Http\Requests\API\UpdateSeasonPlannerDetailAPIRequest;
use App\Models\SeasonPlannerDetail;
use App\Repositories\SeasonPlannerDetailRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class SeasonPlannerDetailController
 * @package App\Http\Controllers\API
 */

class SeasonPlannerDetailAPIController extends AppBaseController
{
    /** @var  SeasonPlannerDetailRepository */
    private $seasonPlannerDetailRepository;

    public function __construct(SeasonPlannerDetailRepository $seasonPlannerDetailRepo)
    {
        $this->seasonPlannerDetailRepository = $seasonPlannerDetailRepo;
    }

    /**
     * Display a listing of the SeasonPlannerDetail.
     * GET|HEAD /seasonPlannerDetails
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $seasonPlannerDetails = $this->seasonPlannerDetailRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($seasonPlannerDetails->toArray(), 'Season Planner Details retrieved successfully');
    }

    /**
     * Store a newly created SeasonPlannerDetail in storage.
     * POST /seasonPlannerDetails
     *
     * @param CreateSeasonPlannerDetailAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateSeasonPlannerDetailAPIRequest $request)
    {
        $input = $request->all();

        $seasonPlannerDetail = $this->seasonPlannerDetailRepository->create($input);

        return $this->sendResponse($seasonPlannerDetail->toArray(), 'Season Planner Detail saved successfully');
    }

    /**
     * Display the specified SeasonPlannerDetail.
     * GET|HEAD /seasonPlannerDetails/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var SeasonPlannerDetail $seasonPlannerDetail */
        $seasonPlannerDetail = $this->seasonPlannerDetailRepository->find($id);

        if (empty($seasonPlannerDetail)) {
            return $this->sendError('Season Planner Detail not found');
        }

        return $this->sendResponse($seasonPlannerDetail->toArray(), 'Season Planner Detail retrieved successfully');
    }

    /**
     * Update the specified SeasonPlannerDetail in storage.
     * PUT/PATCH /seasonPlannerDetails/{id}
     *
     * @param int $id
     * @param UpdateSeasonPlannerDetailAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateSeasonPlannerDetailAPIRequest $request)
    {
        $input = $request->all();

        /** @var SeasonPlannerDetail $seasonPlannerDetail */
        $seasonPlannerDetail = $this->seasonPlannerDetailRepository->find($id);

        if (empty($seasonPlannerDetail)) {
            return $this->sendError('Season Planner Detail not found');
        }

        $seasonPlannerDetail = $this->seasonPlannerDetailRepository->update($input, $id);

        return $this->sendResponse($seasonPlannerDetail->toArray(), 'SeasonPlannerDetail updated successfully');
    }

    /**
     * Remove the specified SeasonPlannerDetail from storage.
     * DELETE /seasonPlannerDetails/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var SeasonPlannerDetail $seasonPlannerDetail */
        $seasonPlannerDetail = $this->seasonPlannerDetailRepository->find($id);

        if (empty($seasonPlannerDetail)) {
            return $this->sendError('Season Planner Detail not found');
        }

        $seasonPlannerDetail->delete();

        return $this->sendSuccess('Season Planner Detail deleted successfully');
    }
}
