<?php

namespace App\Reports;

use TCPDF;

class MedicalRecordReport
{
    public function global()
    {
        // create new PDF document
        $pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('TCPDF Example 002');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 10, PDF_MARGIN_RIGHT);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set font
        $pdf->SetFont('dejavusans', '', 11, '', true);

        // add a page
        $pdf->AddPage();

        // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')

        $html = '<div style="text-align:center;font-size: 30px;font-weight:bold;">MEDICAL RECORD</div>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->ln(5);

        $html = '<p>Player Name : </p>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->ln(3);

        $html = '<table border="1" cellpadding="3">
        <tr>
            <th rowspan="2" width="30px">No</th>
            <th align="center" rowspan="2">Date</th>
            <th align="center" rowspan="2">Day</th>
            <th align="center" colspan="6">INJURY STATUS</th>
        </tr>
        <tr>
            <th align="center" style="text-transform:90deg;">Injured</th>
            <th align="center">Primary Injury Area</th>
            <th align="center">Details</th>
            <th align="center">Treatment Sought</th>
            <th align="center">Injured Day</th>
            <th align="center">Treadment Day</th>
        </tr>
        </table>';
        $pdf->writeHTML($html, true, false, true, false, '');

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output(storage_path('/app/medical_record.pdf'), 'F');
    }
}
