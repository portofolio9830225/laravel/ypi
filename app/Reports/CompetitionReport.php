<?php

namespace App\Reports;

use App\Models\CompetitionStatus;
use App\Models\LevelKategori;
use App\Models\MasterAchievement;
use App\Models\TabelKategori;
use TCPDF;

class CustomPDF extends TCPDF
{
    protected $clubImage;

    public function setClubImage($url)
    {
        $this->clubImage = str_replace(url('/'), public_path(), $url);
    }

    // Page header
    public function Header()
    {
        $this->SetFont('helvetica', 'B', 24);
        $html = <<<EOD
        <table border="0">
            <tr>
                <td width="70%">
                    <span>TOURNAMENT DATA</span>
                </td>
            </tr>
        </table>
        EOD;
        $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 'B', $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $pagination = 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages();
        $html = <<<EOD
        <table border="0">
            <tr>
                <td width="10%" align="left">
                    
                </td>
                <td width="70%" align="left">
                    <span>Bypro YPI Badminton Software (ypi.bypro.id)</span>
                </td>
                <td width="20%" align="right">
                    <span>{$pagination}</span>
                </td>
            </tr>
        </table>
        EOD;
        $this->Image($file = $this->clubImage, $x = null, $y = null, $w = 10, $h = 10);
        $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 'T', $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
    }

    public function createHtmlTable($header, $data)
    {
        $content = '';
        foreach ($data as $key => $value) {
            $row = '';
            foreach ($value as $k => $v) {
                $row .= '<td>' . $v . '</td>';
            }
            $content .= '<tr>' . $row . '</tr>';
        }
        $totalPlayer = count($data);

        $table = <<<EOD
        <table border="1" cellpadding="3">
            <tr style="background-color: #FD646E;font-weight: bold;color: #FFFFFF;">
                {$header}
            </tr>
            {$content}
            <tr>
                <td style="font-weight: bold;" colspan="5">{$totalPlayer} Player</td>
            </tr>
        </table>
        EOD;

        return $table;
    }
}

class CompetitionReport
{
    function global($input, $idTabelKategori = 0)
    {
        $club_image_url = $input['club_image'];

        // create new PDF document
        $pdf = new CustomPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->setClubImage($club_image_url);

        // set document information
        $pdf->SetCreator('ypi.bypro.id');
        $pdf->SetAuthor('ypi.bypro.id');
        $pdf->SetTitle('Data Competition Report');
        $pdf->SetSubject('Data Competition Report');
        $pdf->SetKeywords('bypro, ypi, badminton, player');

        // set default header data
        $pdf->SetHeaderData($input['club_image'], PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // set font
        $pdf->SetFont('dejavusans', '', 11, '', true);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // add a page
        $pdf->AddPage();

        if ($idTabelKategori != 0) {
            $data = MasterAchievement::where('kategori_id', $idTabelKategori)
                ->whereRaw("DATE_FORMAT(tanggal_mulai,'%Y') >= " . $input['start_year'])
                ->whereRaw("DATE_FORMAT(tanggal_akhir,'%Y') <= " . $input['end_year'])
                ->get();
        } else {
            $data = MasterAchievement::whereRaw("DATE_FORMAT(tanggal_mulai,'%Y') >= " . $input['start_year'])
                ->whereRaw("DATE_FORMAT(tanggal_akhir,'%Y') <= " . $input['end_year'])->get();
        }

        // create some HTML content
        $html = '<table border="1" cellpadding="3">
        <tr style="background-color: #FD646E;font-weight: bold;color: #FFFFFF;" align="center">
            <th>Tournament Name</th>
            <th>Start</th>
            <th>End</th>
            <th>Competition Detail</th>
            <th>Competition Level</th>
        </tr>';

        foreach ($data as $item) {

            $tabelKategori  = TabelKategori::find($item->kategori_id);
            $levelKategori  = LevelKategori::find($tabelKategori->level_kategori_id);

            $html .= '<tr>
            <td>' . $item->nama_event . '</td>
            <td align="center">' . date('d-m-Y', strtotime($item->tanggal_mulai)) . '</td>
            <td align="center">' . date('d-m-Y', strtotime($item->tanggal_akhir)) . '</td>
            <td align="center">' . $tabelKategori->nama_kategori . '</td>
            <td align="center">' . $levelKategori->level . '</td>
            </tr>';
        }

        $html .= '</table>';

        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');

        //Close and output PDF document
        $pdf->Output(storage_path('/app/competition.pdf'), 'F');
    }

    function groupByCategory($image, $data)
    {
        // create new PDF document
        $pdf = new CustomPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('ypi.bypro.id');
        $pdf->SetAuthor('ypi.bypro.id');
        $pdf->SetTitle('Data Competition Report');
        $pdf->SetSubject('Data Competition Report');
        $pdf->SetKeywords('bypro, ypi, badminton, player');

        // set default header data
        $pdf->SetHeaderData($image, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // set font
        $pdf->SetFont('dejavusans', '', 11, '', true);

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // add a page
        $pdf->AddPage();

        foreach ($data as $item) {
            // create some HTML content
            $html = '<table border="1" cellpadding="3">
            <tr>
                <th colspan="5" align="center">' . $item['nama_kategori'] . '</th>
            </tr>
            <tr style="background-color: #FD646E;font-weight: bold;color: #FFFFFF;" align="center">
                <th>Tournament Name</th>
                <th>Start</th>
                <th>End</th>
                <th>Competition Detail</th>
                <th>Competition Level</th>
            </tr>';

            foreach ($item['competition_detail'] as $val) {
                $tabelKategori  = TabelKategori::find($val->kategori_id);
                $levelKategori  = LevelKategori::find($tabelKategori->level_kategori_id);

                $html .= '<tr>
                <td>' . $val['nama_event'] . '</td>
                <td align="center">' . date('d-m-Y', strtotime($val['tanggal_mulai'])) . '</td>
                <td align="center">' . date('d-m-Y', strtotime($val['tanggal_akhir'])) . '</td>
                <td align="center">' . $tabelKategori->nama_kategori . '</td>
                <td align="center">' . $levelKategori->level . '</td>
                </tr>';
            }

            $html .= '<tr>
            <th colspan="5"><b>' . count($item['competition_detail']) . ' Competition</b></th>
            </tr>';

            $html .= '</table>';

            // output the HTML content
            $pdf->writeHTML($html, true, false, true, false, '');

            $pdf->Ln(5);
        }

        //Close and output PDF document
        $pdf->Output(storage_path('/app/competitionGroup.pdf'), 'F');
    }
}
