<?php

namespace App\Reports;

use App\Models\Club;
use App\Models\MasterPemain;
use App\Models\TabelKelompok;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use TCPDF;

class CustomPDF extends TCPDF
{
    protected $clubImage;

    public function setClubImage($url)
    {
        $this->clubImage = str_replace(url('/'), public_path(), $url);
    }

    // Page header
    public function Header()
    {
        $this->SetFont('helvetica', 'B', 24);
        $html = <<<EOD
        <table border="0">
            <tr>
                <td width="70%">
                    <span>Player Data</span>
                </td>
            </tr>
        </table>
        EOD;

        $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 'B', $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
    }

    // Page footer
    public function Footer()
    {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $pagination = 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages();
        $html = <<<EOD
        <table border="0">
            <tr>
                <td width="10%" align="left">
                    
                </td>
                <td width="70%" align="left">
                    <span>Bypro YPI Badminton Software (ypi.bypro.id)</span>
                </td>
                <td width="20%" align="right">
                    <span>{$pagination}</span>
                </td>
            </tr>
        </table>
        EOD;
        $this->Image($file = $this->clubImage, $x = null, $y = null, $w = 10, $h = 10);
        $this->writeHTMLCell($w = 0, $h = 0, $x = '', $y = '', $html, $border = 'T', $ln = 1, $fill = 0, $reseth = true, $align = 'top', $autopadding = true);
    }

    public function createHtmlTable($header, $data)
    {
        $content = '';
        foreach ($data as $key => $value) {
            $row = '';
            foreach ($value as $k => $v) {
                $row .= '<td>' . $v . '</td>';
            }
            $content .= '<tr>' . $row . '</tr>';
        }
        $totalPlayer = count($data);

        $table = <<<EOD
        <table border="1" cellpadding="3">
            <tr style="background-color: #FD646E;font-weight: bold;color: #FFFFFF;">
                {$header}
            </tr>
            {$content}
            <tr>
                <td style="font-weight: bold;" colspan="5">{$totalPlayer} Player</td>
            </tr>
        </table>
        EOD;

        return $table;
    }
}

class PlayerReport
{
    function global($input)
    {
        $specialists = $input['specialist'];
        $player_status = $input['player_status'];
        $club_image_url = $input['club_image'];

        // create new PDF document
        $pdf = new CustomPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->setClubImage($club_image_url);

        // set document information
        $pdf->SetCreator('ypi.bypro.id');
        $pdf->SetAuthor('ypi.bypro.id');
        $pdf->SetTitle('Data Player Report');
        $pdf->SetSubject('Data Player Report');
        $pdf->SetKeywords('bypro, ypi, badminton, player');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 11, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

        // data loading
        $data = [];
        $specialistColumns = [];
        if (in_array(1, $specialists)) {
            array_push($specialistColumns, 'tunggal_putra');
        }
        if (in_array(2, $specialists)) {
            array_push($specialistColumns, 'ganda_putra');
        }
        if (in_array(3, $specialists)) {
            array_push($specialistColumns, 'tunggal_putri');
        }
        if (in_array(4, $specialists)) {
            array_push($specialistColumns, 'ganda_putri');
        }
        if (in_array(5, $specialists)) {
            array_push($specialistColumns, 'ganda_campuran');
        }

        if ($player_status === 1) {
            $query = MasterPemain::select('nama_lengkap', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif', 'kelompok_id')->with('group');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get()->toArray();
        } else if ($player_status === 2) {
            $query = MasterPemain::select('nama_lengkap', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif')->with('group');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get();
            $data = $data->filter(function ($value, $key) {
                return $value->aktif === true;
            });
            $data = $data->toArray();
        } else if ($player_status === 3) {
            $query = MasterPemain::select('nama_lengkap', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif')->with('group');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get();
            $data = $data->filter(function ($value, $key) {
                return $value->aktif === false;
            });
            $data = $data->toArray();
        }

        foreach ($data as $key => $value) {
            unset($data[$key]['tunggal_putra']);
            unset($data[$key]['tunggal_putri']);
            unset($data[$key]['ganda_putra']);
            unset($data[$key]['ganda_putri']);
            unset($data[$key]['ganda_campuran']);
            unset($data[$key]['aktif']);
            unset($data[$key]['kelompok_id']);
            $data[$key]['tanggal_lahir'] = substr($data[$key]['tanggal_lahir'], 0, 10);
            $dob = new Carbon($value['tanggal_lahir']);
            $data[$key]['age'] = $dob->age;
            if (isset($data[$key]['group'])) {
                $data[$key]['group_name'] = $data[$key]['group']['kelompok'];
            } else {
                $data[$key]['group_name'] = '-';
            }
            unset($data[$key]['group']);
        }

        $headerEn = <<<EOD
            <th align="center">Full Name</th>
            <th align="center">Address</th>
            <th align="center">Mobile</th>
            <th align="center">Birth Date</th>
            <th align="center">Age</th>
            <th align="center">Group</th>
        EOD;

        $header = $headerEn;

        $content = '';
        foreach ($data as $key => $value) {
            $row = '';
            foreach ($value as $k => $v) {
                $row .= '<td>' . $v . '</td>';
            }
            $content .= '<tr>' . $row . '</tr>';
        }

        $table = <<<EOD
        <table border="1" cellpadding="3">
            <tr style="background-color: #FD646E;font-weight: bold;color: #FFFFFF;">
                {$header}
            </tr>
            {$content}
        </table>
        EOD;

        $pdf->writeHTML($table, true, false, false, false, '');

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output(storage_path('/app/player.pdf'), 'F');
    }

    function age($input)
    {
        $specialists = $input['specialist'];
        $player_status = $input['player_status'];
        $club_image_url = $input['club_image'];

        // create new PDF document
        $pdf = new CustomPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->setClubImage($club_image_url);

        // set document information
        $pdf->SetCreator('ypi.bypro.id');
        $pdf->SetAuthor('ypi.bypro.id');
        $pdf->SetTitle('Data Player Report');
        $pdf->SetSubject('Data Player Report');
        $pdf->SetKeywords('bypro, ypi, badminton, player');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 11, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

        // data loading
        $data = [];
        $specialistColumns = [];
        if (in_array(1, $specialists)) {
            array_push($specialistColumns, 'tunggal_putra');
        }
        if (in_array(2, $specialists)) {
            array_push($specialistColumns, 'ganda_putra');
        }
        if (in_array(3, $specialists)) {
            array_push($specialistColumns, 'tunggal_putri');
        }
        if (in_array(4, $specialists)) {
            array_push($specialistColumns, 'ganda_putri');
        }
        if (in_array(5, $specialists)) {
            array_push($specialistColumns, 'ganda_campuran');
        }

        if ($player_status === 1) {
            $query = MasterPemain::select('nama_lengkap', 'kelompok_id', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get()->toArray();
        } else if ($player_status === 2) {
            $query = MasterPemain::select('nama_lengkap', 'kelompok_id', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get();
            $data = $data->filter(function ($value, $key) {
                return $value->aktif === true;
            });
            $data = $data->toArray();
        } else if ($player_status === 3) {
            $query = MasterPemain::select('nama_lengkap', 'kelompok_id', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get();
            $data = $data->filter(function ($value, $key) {
                return $value->aktif === false;
            });
            $data = $data->toArray();
        }

        foreach ($data as $key => $value) {
            $data[$key]['tanggal_lahir'] = substr($data[$key]['tanggal_lahir'], 0, 10);
            $dob = new Carbon($value['tanggal_lahir']);
            $data[$key]['age'] = $dob->age;
        }

        $headerEn = <<<EOD
            <th align="center">Full Name</th>
            <th align="center">Address</th>
            <th align="center">Mobile</th>
            <th align="center">Birth Date</th>
            <th align="center">Age</th>
        EOD;

        $header = $headerEn;

        $groupClassifications = TabelKelompok::get()->toArray();

        foreach ($groupClassifications as $key => $value) {
            $filteredData = array_filter($data, function ($var) use ($value) {
                return ($var['kelompok_id'] == $value['id']);
            });
            foreach ($filteredData as $k => $v) {
                unset($filteredData[$k]['tunggal_putra']);
                unset($filteredData[$k]['tunggal_putri']);
                unset($filteredData[$k]['ganda_putra']);
                unset($filteredData[$k]['ganda_putri']);
                unset($filteredData[$k]['ganda_campuran']);
                unset($filteredData[$k]['aktif']);
                unset($filteredData[$k]['kelompok_id']);
            }

            $table = $pdf->createHtmlTable($header, $filteredData);

            $pdf->Cell(0, 0, $value['kelompok'], 1, 1, 'C', 0, '', 0);
            $pdf->writeHTML($table, true, false, false, false, '');
        }

        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output(storage_path('/app/player.pdf'), 'F');
    }

    function specialist($input)
    {
        $specialists = $input['specialist'];
        $player_status = $input['player_status'];
        $club_image_url = $input['club_image'];
        // create new PDF document
        $pdf = new CustomPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->setClubImage($club_image_url);

        // set document information
        $pdf->SetCreator('ypi.bypro.id');
        $pdf->SetAuthor('ypi.bypro.id');
        $pdf->SetTitle('Data Player Report');
        $pdf->SetSubject('Data Player Report');
        $pdf->SetKeywords('bypro, ypi, badminton, player');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' 001', PDF_HEADER_STRING, array(0, 64, 255), array(0, 64, 128));
        $pdf->setFooterData(array(0, 64, 0), array(0, 64, 128));

        // set header and footer fonts
        $pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $pdf->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $pdf->SetFont('dejavusans', '', 11, '', true);

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $pdf->AddPage();

        // data loading
        $data = [];
        $specialistColumns = [];
        if (in_array(1, $specialists)) {
            array_push($specialistColumns, 'tunggal_putra');
        }
        if (in_array(2, $specialists)) {
            array_push($specialistColumns, 'ganda_putra');
        }
        if (in_array(3, $specialists)) {
            array_push($specialistColumns, 'tunggal_putri');
        }
        if (in_array(4, $specialists)) {
            array_push($specialistColumns, 'ganda_putri');
        }
        if (in_array(5, $specialists)) {
            array_push($specialistColumns, 'ganda_campuran');
        }

        if ($player_status === 1) {
            $query = MasterPemain::select('nama_lengkap', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif', 'kelompok_id')->with('group');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get()->toArray();
        } else if ($player_status === 2) {
            $query = MasterPemain::select('nama_lengkap', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif', 'kelompok_id')->with('group');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get();
            $data = $data->filter(function ($value, $key) {
                return $value->aktif === true;
            });
            $data = $data->toArray();
        } else if ($player_status === 3) {
            $query = MasterPemain::select('nama_lengkap', 'alamat', 'hand_phone', 'tanggal_lahir', 'tunggal_putra', 'tunggal_putri', 'ganda_putra', 'ganda_putri', 'ganda_campuran', 'aktif', 'kelompok_id')->with('group');
            foreach ($specialistColumns as $key => $value) {
                $query->orWhere($specialistColumns[$key], '=', 1);
            }
            $data = $query->get();
            $data = $data->filter(function ($value, $key) {
                return $value->aktif === false;
            });
            $data = $data->toArray();
        }

        $headerEn = <<<EOD
            <th align="center">Full Name</th>
            <th align="center">Address</th>
            <th align="center">Mobile</th>
            <th align="center">Birth Date</th>
            <th align="center">Age</th>
            <th align="center">Group</th>
        EOD;

        $header = $headerEn;

        foreach ($data as $key => $value) {
            $dob = new Carbon($value['tanggal_lahir']);
            $data[$key]['age'] = $dob->age;
            unset($data[$key]['kelompok_id']);

            if (isset($data[$key]['group'])) {
                $data[$key]['group_name'] = $data[$key]['group']['kelompok'];
            } else {
                $data[$key]['group_name'] = '-';
            }
            unset($data[$key]['group']);
        }

        $manSingleData = array_filter($data, function ($var) {
            return ($var['tunggal_putra'] == true);
        });
        foreach ($manSingleData as $key => $value) {
            unset($manSingleData[$key]['tunggal_putra']);
            unset($manSingleData[$key]['tunggal_putri']);
            unset($manSingleData[$key]['ganda_putra']);
            unset($manSingleData[$key]['ganda_putri']);
            unset($manSingleData[$key]['ganda_campuran']);
            unset($manSingleData[$key]['aktif']);
            $manSingleData[$key]['tanggal_lahir'] = substr($manSingleData[$key]['tanggal_lahir'], 0, 10);
        }

        $manDoubleData = array_filter($data, function ($var) {
            return ($var['ganda_putra'] == true);
        });
        foreach ($manDoubleData as $key => $value) {
            unset($manDoubleData[$key]['tunggal_putra']);
            unset($manDoubleData[$key]['tunggal_putri']);
            unset($manDoubleData[$key]['ganda_putra']);
            unset($manDoubleData[$key]['ganda_putri']);
            unset($manDoubleData[$key]['ganda_campuran']);
            unset($manDoubleData[$key]['aktif']);
            $manDoubleData[$key]['tanggal_lahir'] = substr($manDoubleData[$key]['tanggal_lahir'], 0, 10);
        }

        $womanSingleData = array_filter($data, function ($var) {
            return ($var['tunggal_putri'] == true);
        });
        foreach ($womanSingleData as $key => $value) {
            unset($womanSingleData[$key]['tunggal_putra']);
            unset($womanSingleData[$key]['tunggal_putri']);
            unset($womanSingleData[$key]['ganda_putra']);
            unset($womanSingleData[$key]['ganda_putri']);
            unset($womanSingleData[$key]['ganda_campuran']);
            unset($womanSingleData[$key]['aktif']);
            $womanSingleData[$key]['tanggal_lahir'] = substr($womanSingleData[$key]['tanggal_lahir'], 0, 10);
        }

        $womanDoubleData = array_filter($data, function ($var) {
            return ($var['ganda_putri'] == true);
        });
        foreach ($womanDoubleData as $key => $value) {
            unset($womanDoubleData[$key]['tunggal_putra']);
            unset($womanDoubleData[$key]['tunggal_putri']);
            unset($womanDoubleData[$key]['ganda_putra']);
            unset($womanDoubleData[$key]['ganda_putri']);
            unset($womanDoubleData[$key]['ganda_campuran']);
            unset($womanDoubleData[$key]['aktif']);
            $womanDoubleData[$key]['tanggal_lahir'] = substr($womanDoubleData[$key]['tanggal_lahir'], 0, 10);
        }

        $mixDoubleData = array_filter($data, function ($var) {
            return ($var['ganda_campuran'] == true);
        });
        foreach ($mixDoubleData as $key => $value) {
            unset($mixDoubleData[$key]['tunggal_putra']);
            unset($mixDoubleData[$key]['tunggal_putri']);
            unset($mixDoubleData[$key]['ganda_putra']);
            unset($mixDoubleData[$key]['ganda_putri']);
            unset($mixDoubleData[$key]['ganda_campuran']);
            unset($mixDoubleData[$key]['aktif']);
            $mixDoubleData[$key]['tanggal_lahir'] = substr($mixDoubleData[$key]['tanggal_lahir'], 0, 10);
        }

        $manSingleTable = $pdf->createHtmlTable($header, $manSingleData);
        $manDoubleTable = $pdf->createHtmlTable($header, $manDoubleData);
        $womanSingleTable = $pdf->createHtmlTable($header, $womanSingleData);
        $womanDoubleTable = $pdf->createHtmlTable($header, $womanDoubleData);
        $mixDoubleTable = $pdf->createHtmlTable($header, $mixDoubleData);

        $pdf->Cell(0, 0, 'Man Single', 1, 1, 'C', 0, '', 0);
        $pdf->writeHTML($manSingleTable, true, false, false, false, '');

        $pdf->Cell(0, 0, 'Man Double', 1, 1, 'C', 0, '', 0);
        $pdf->writeHTML($manDoubleTable, true, false, false, false, '');

        $pdf->Cell(0, 0, 'Woman Single', 1, 1, 'C', 0, '', 0);
        $pdf->writeHTML($womanSingleTable, true, false, false, false, '');

        $pdf->Cell(0, 0, 'Woman Double', 1, 1, 'C', 0, '', 0);
        $pdf->writeHTML($womanDoubleTable, true, false, false, false, '');

        $pdf->Cell(0, 0, 'Mix Double', 1, 1, 'C', 0, '', 0);
        $pdf->writeHTML($mixDoubleTable, true, false, false, false, '');


        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $pdf->Output(storage_path('/app/player.pdf'), 'F');
    }
}
