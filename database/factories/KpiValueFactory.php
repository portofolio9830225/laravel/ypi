<?php

namespace Database\Factories;

use App\Models\KpiValue;
use Illuminate\Database\Eloquent\Factories\Factory;

class KpiValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KpiValue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'kpi_indicator_id' => $this->faker->randomDigitNotNull,
        'kpi_subindicator_id' => $this->faker->randomDigitNotNull,
        'date' => $this->faker->word,
        'pb' => $this->faker->randomDigitNotNull,
        'target' => $this->faker->randomDigitNotNull,
        'actual' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
