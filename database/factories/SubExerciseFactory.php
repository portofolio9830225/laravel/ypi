<?php

namespace Database\Factories;

use App\Models\SubExercise;
use Illuminate\Database\Eloquent\Factories\Factory;

class SubExerciseFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SubExercise::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'excercise_desc' => $this->faker->word,
        'gym_type_id' => $this->faker->randomDigitNotNull,
        'tut_per_rep' => $this->faker->randomDigitNotNull,
        'exercise_category_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
