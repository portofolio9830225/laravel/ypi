<?php

namespace Database\Factories;

use App\Models\SeasonPlannerDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeasonPlannerDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SeasonPlannerDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'season_planner_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'description' => $this->faker->word,
        'type' => $this->faker->word,
        'inChart' => $this->faker->word,
        'chartType' => $this->faker->word,
        'chartColor' => $this->faker->word,
        'chartNumber' => $this->faker->randomDigitNotNull,
        'chartName' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
