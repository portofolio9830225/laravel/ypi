<?php

namespace Database\Factories;

use App\Models\HeaderGap;
use Illuminate\Database\Eloquent\Factories\Factory;

class HeaderGapFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HeaderGap::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'event_name' => $this->faker->word,
        'present' => $this->faker->word,
        'future' => $this->faker->word,
        'player_gap' => $this->faker->word,
        'gap_value1' => $this->faker->randomDigitNotNull,
        'gap_value2' => $this->faker->randomDigitNotNull,
        'gap_value3' => $this->faker->randomDigitNotNull,
        'gap_value4' => $this->faker->randomDigitNotNull,
        'gap_value5' => $this->faker->randomDigitNotNull,
        'competition' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
