<?php

namespace Database\Factories;

use App\Models\AnnualPlanSetting;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnnualPlanSettingFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AnnualPlanSetting::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'kelompok' => $this->faker->randomDigitNotNull,
        'description' => $this->faker->word,
        'color' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
