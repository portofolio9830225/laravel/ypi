<?php

namespace Database\Factories;

use App\Models\FitnessTest;
use Illuminate\Database\Eloquent\Factories\Factory;

class FitnessTestFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FitnessTest::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tanggal_test' => $this->faker->word,
        'gender' => $this->faker->word,
        'kelompok_id' => $this->faker->randomDigitNotNull,
        'pemain_id' => $this->faker->randomDigitNotNull,
        'court_agility' => $this->faker->randomDigitNotNull,
        'vo2max' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
