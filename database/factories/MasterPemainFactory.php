<?php

namespace Database\Factories;

use App\Models\MasterPemain;
use Illuminate\Database\Eloquent\Factories\Factory;

class MasterPemainFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MasterPemain::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nomor_ic' => $this->faker->word,
        'nama_lengkap' => $this->faker->word,
        'alamat' => $this->faker->word,
        'negara' => $this->faker->word,
        'kodepos' => $this->faker->randomDigitNotNull,
        'tempat_lahir' => $this->faker->word,
        'tanggal_lahir' => $this->faker->word,
        'home_phone' => $this->faker->word,
        'hand_phone' => $this->faker->word,
        'business_phone' => $this->faker->word,
        'nama_sekolah' => $this->faker->word,
        'kelas' => $this->faker->word,
        'wali_kelas' => $this->faker->word,
        'no_ruang' => $this->faker->word,
        'keterangan' => $this->faker->text,
        'golongan_darah' => $this->faker->word,
        'email' => $this->faker->word,
        'contact_id' => $this->faker->randomDigitNotNull,
        'aktif' => $this->faker->word,
        'sex' => $this->faker->word,
        'kelompok_id' => $this->faker->randomDigitNotNull,
        'tunggal_putra' => $this->faker->word,
        'tunggal_putri' => $this->faker->word,
        'ganda_putra' => $this->faker->word,
        'ganda_putri' => $this->faker->word,
        'ganda_campuran' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
