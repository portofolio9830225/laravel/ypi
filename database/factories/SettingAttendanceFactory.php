<?php

namespace Database\Factories;

use App\Models\SettingAttendance;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingAttendanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SettingAttendance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'bulan' => $this->faker->randomDigitNotNull,
        'tahun' => $this->faker->randomDigitNotNull,
        'kelompok' => $this->faker->word,
        'kode_pelatih' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'session' => $this->faker->randomDigitNotNull,
        'am_pm' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
