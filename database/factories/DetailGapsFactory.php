<?php

namespace Database\Factories;

use App\Models\DetailGaps;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailGapsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailGaps::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'header_gap_id' => $this->faker->randomDigitNotNull,
        'category' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'value' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
