<?php

namespace Database\Factories;

use App\Models\AnnualPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

class AnnualPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AnnualPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'kelompok' => $this->faker->randomDigitNotNull,
        'no_urut' => $this->faker->randomDigitNotNull,
        'week' => $this->faker->randomDigitNotNull,
        'plan_description' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
