<?php

namespace Database\Factories;

use App\Models\TrainingAttendance;
use Illuminate\Database\Eloquent\Factories\Factory;

class TrainingAttendanceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TrainingAttendance::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'kelompok_id' => $this->faker->randomDigitNotNull,
        'master_pelatih_id' => $this->faker->randomDigitNotNull,
        'pemain_id' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'session' => $this->faker->randomDigitNotNull,
        'presence_id' => $this->faker->randomDigitNotNull,
        'note' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
