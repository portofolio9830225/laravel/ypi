<?php

namespace Database\Factories;

use App\Models\SettingPresence;
use Illuminate\Database\Eloquent\Factories\Factory;

class SettingPresenceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SettingPresence::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'kelompok_id' => $this->faker->randomDigitNotNull,
        'pelatih_id' => $this->faker->randomDigitNotNull,
        'keterangan' => $this->faker->word,
        'inisial' => $this->faker->word,
        'presence' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
