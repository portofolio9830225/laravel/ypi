<?php

namespace Database\Factories;

use App\Models\StrengthConditionPlayer;
use Illuminate\Database\Eloquent\Factories\Factory;

class StrengthConditionPlayerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StrengthConditionPlayer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'strength_conditioning_id' => $this->faker->randomDigitNotNull,
        'pemain_id' => $this->faker->randomDigitNotNull,
        'body_weight' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
