<?php

namespace Database\Factories;

use App\Models\DefaultTarget;
use Illuminate\Database\Eloquent\Factories\Factory;

class DefaultTargetFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DefaultTarget::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'mon1' => $this->faker->randomDigitNotNull,
        'mon2' => $this->faker->randomDigitNotNull,
        'mon3' => $this->faker->randomDigitNotNull,
        'mon4' => $this->faker->randomDigitNotNull,
        'tue1' => $this->faker->randomDigitNotNull,
        'tue2' => $this->faker->randomDigitNotNull,
        'tue3' => $this->faker->randomDigitNotNull,
        'tue4' => $this->faker->randomDigitNotNull,
        'wed1' => $this->faker->randomDigitNotNull,
        'wed2' => $this->faker->randomDigitNotNull,
        'wed3' => $this->faker->randomDigitNotNull,
        'wed4' => $this->faker->randomDigitNotNull,
        'thu1' => $this->faker->randomDigitNotNull,
        'thu2' => $this->faker->randomDigitNotNull,
        'thu3' => $this->faker->randomDigitNotNull,
        'thu4' => $this->faker->randomDigitNotNull,
        'fri1' => $this->faker->randomDigitNotNull,
        'fri2' => $this->faker->randomDigitNotNull,
        'fri3' => $this->faker->randomDigitNotNull,
        'fri4' => $this->faker->randomDigitNotNull,
        'sat1' => $this->faker->randomDigitNotNull,
        'sat2' => $this->faker->randomDigitNotNull,
        'sat3' => $this->faker->randomDigitNotNull,
        'sat4' => $this->faker->randomDigitNotNull,
        'sun1' => $this->faker->randomDigitNotNull,
        'sun2' => $this->faker->randomDigitNotNull,
        'sun3' => $this->faker->randomDigitNotNull,
        'sun4' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
