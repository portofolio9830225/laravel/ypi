<?php

namespace Database\Factories;

use App\Models\NationalHeightAvg;
use Illuminate\Database\Eloquent\Factories\Factory;

class NationalHeightAvgFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NationalHeightAvg::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nation_id' => $this->faker->randomDigitNotNull,
        'nation' => $this->faker->word,
        'age' => $this->faker->randomDigitNotNull,
        'height' => $this->faker->randomDigitNotNull,
        'gender' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
