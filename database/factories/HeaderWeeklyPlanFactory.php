<?php

namespace Database\Factories;

use App\Models\HeaderWeeklyPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

class HeaderWeeklyPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HeaderWeeklyPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'minggu' => $this->faker->randomDigitNotNull,
        'tanggal_awal' => $this->faker->word,
        'tanggal_akhir' => $this->faker->word,
        'actual_tr_vol' => $this->faker->randomDigitNotNull,
        'actual_tr_vol_prosen' => $this->faker->randomDigitNotNull,
        'actual_tr_intensity' => $this->faker->randomDigitNotNull,
        'actual_tr_load' => $this->faker->randomDigitNotNull,
        'target_tr_vol' => $this->faker->randomDigitNotNull,
        'target_tr_vol_prosen' => $this->faker->randomDigitNotNull,
        'target_tr_intensity' => $this->faker->randomDigitNotNull,
        'target_tr_load' => $this->faker->randomDigitNotNull,
        'weekly_training_goal' => $this->faker->word,
        'notes' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
