<?php

namespace Database\Factories;

use App\Models\StrengthConditioning;
use Illuminate\Database\Eloquent\Factories\Factory;

class StrengthConditioningFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StrengthConditioning::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date_test' => $this->faker->word,
        'gender' => $this->faker->word,
        'age_group_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
