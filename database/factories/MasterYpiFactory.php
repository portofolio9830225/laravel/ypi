<?php

namespace Database\Factories;

use App\Models\MasterYpi;
use Illuminate\Database\Eloquent\Factories\Factory;

class MasterYpiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MasterYpi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'keterangan' => $this->faker->word,
        'kode_pelatih' => $this->faker->randomDigitNotNull,
        'voltarget' => $this->faker->randomDigitNotNull,
        'tanggal_mulai' => $this->faker->date('Y-m-d H:i:s'),
        'tanggal_selesai' => $this->faker->date('Y-m-d H:i:s'),
        'default_intensity' => $this->faker->randomDigitNotNull
        ];
    }
}
