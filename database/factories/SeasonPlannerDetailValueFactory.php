<?php

namespace Database\Factories;

use App\Models\SeasonPlannerDetailValue;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeasonPlannerDetailValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SeasonPlannerDetailValue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'season_planner_id' => $this->faker->randomDigitNotNull,
        'season_planner_detail_id' => $this->faker->randomDigitNotNull,
        'week_index' => $this->faker->randomDigitNotNull,
        'value' => $this->faker->word,
        'text_color' => $this->faker->word,
        'description' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
