<?php

namespace Database\Factories;

use App\Models\DetailPbKpi;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailPbKpiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailPbKpi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'kpi_physic_id' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'pb' => $this->faker->randomDigitNotNull,
        'target' => $this->faker->randomDigitNotNull,
        'actual' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
