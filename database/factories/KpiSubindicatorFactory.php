<?php

namespace Database\Factories;

use App\Models\KpiSubindicator;
use Illuminate\Database\Eloquent\Factories\Factory;

class KpiSubindicatorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KpiSubindicator::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'kpi_indicator_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'indicator_type' => $this->faker->randomElement(]),
        'color' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
