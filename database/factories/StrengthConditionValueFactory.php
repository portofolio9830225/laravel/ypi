<?php

namespace Database\Factories;

use App\Models\StrengthConditionValue;
use Illuminate\Database\Eloquent\Factories\Factory;

class StrengthConditionValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StrengthConditionValue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'strength_condition_id' => $this->faker->randomDigitNotNull,
        'strength_condition_player_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'reps' => $this->faker->randomDigitNotNull,
        'weight' => $this->faker->randomDigitNotNull,
        'predict' => $this->faker->randomDigitNotNull,
        'min' => $this->faker->randomDigitNotNull,
        'max' => $this->faker->randomDigitNotNull,
        'strength' => $this->faker->randomDigitNotNull,
        'last' => $this->faker->randomDigitNotNull,
        'diff' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
