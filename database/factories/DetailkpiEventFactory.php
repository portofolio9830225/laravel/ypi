<?php

namespace Database\Factories;

use App\Models\DetailkpiEvent;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailkpiEventFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailkpiEvent::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'tanggal_kpi' => $this->faker->word,
        'keterangan_1' => $this->faker->word,
        'keterangan_2' => $this->faker->word,
        'keterangan_3' => $this->faker->word,
        'keterangan_4' => $this->faker->word,
        'event_name' => $this->faker->word,
        'target_1' => $this->faker->randomDigitNotNull,
        'target_2' => $this->faker->randomDigitNotNull,
        'target_3' => $this->faker->randomDigitNotNull,
        'target_4' => $this->faker->randomDigitNotNull,
        'actual_1' => $this->faker->randomDigitNotNull,
        'actual_2' => $this->faker->randomDigitNotNull,
        'actual_3' => $this->faker->randomDigitNotNull,
        'actual_4' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
