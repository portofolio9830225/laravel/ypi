<?php

namespace Database\Factories;

use App\Models\HeightPredicted;
use Illuminate\Database\Eloquent\Factories\Factory;

class HeightPredictedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HeightPredicted::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'age' => $this->faker->randomDigitNotNull,
        'date_predicted' => $this->faker->word,
        'height' => $this->faker->randomDigitNotNull,
        'weight' => $this->faker->randomDigitNotNull,
        'fathers_height' => $this->faker->randomDigitNotNull,
        'mothers_height' => $this->faker->randomDigitNotNull,
        'predicted_height' => $this->faker->randomDigitNotNull,
        'maturity_status' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
