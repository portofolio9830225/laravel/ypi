<?php

namespace Database\Factories;

use App\Models\CompetitionReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompetitionReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompetitionReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'master_achievement_id' => $this->faker->randomDigitNotNull,
        'venue' => $this->faker->word,
        'specialist' => $this->faker->word,
        'event_level' => $this->faker->word,
        'result_id' => $this->faker->randomDigitNotNull,
        'summary_notes' => $this->faker->text,
        'suggestion_notes' => $this->faker->text,
        'kelompok_id' => $this->faker->randomDigitNotNull,
        'partner_id' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
