<?php

namespace Database\Factories;

use App\Models\DetailCompetitionReport;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailCompetitionReportFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailCompetitionReport::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'competition_report_id' => $this->faker->randomDigitNotNull,
        'round' => $this->faker->word,
        'opponent' => $this->faker->word,
        'nationality' => $this->faker->word,
        'set1a' => $this->faker->randomDigitNotNull,
        'set1b' => $this->faker->randomDigitNotNull,
        'set2a' => $this->faker->randomDigitNotNull,
        'set2b' => $this->faker->randomDigitNotNull,
        'set3a' => $this->faker->randomDigitNotNull,
        'set3b' => $this->faker->randomDigitNotNull,
        'comment' => $this->faker->word,
        'result' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
