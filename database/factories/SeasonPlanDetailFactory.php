<?php

namespace Database\Factories;

use App\Models\SeasonPlanDetail;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeasonPlanDetailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SeasonPlanDetail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'season_plan_id' => $this->faker->randomDigitNotNull,
        'name' => $this->faker->word,
        'input_type' => $this->faker->randomElement(]),
        'desc_en' => $this->faker->text,
        'desc_id' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
