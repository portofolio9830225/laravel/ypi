<?php

namespace Database\Factories;

use App\Models\MasterPelatih;
use Illuminate\Database\Eloquent\Factories\Factory;

class MasterPelatihFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MasterPelatih::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nomor_ic' => $this->faker->word,
        'nama_lengkap' => $this->faker->word,
        'alamat' => $this->faker->word,
        'negara' => $this->faker->word,
        'kodepos' => $this->faker->randomDigitNotNull,
        'tempat_lahir' => $this->faker->word,
        'tanggal_lahir' => $this->faker->word,
        'home_phone' => $this->faker->word,
        'hand_phone' => $this->faker->word,
        'business_phone' => $this->faker->word,
        'email' => $this->faker->word,
        'golongan_darah' => $this->faker->word,
        'keterangan' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
