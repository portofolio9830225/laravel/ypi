<?php

namespace Database\Factories;

use App\Models\CompetitionStatus;
use Illuminate\Database\Eloquent\Factories\Factory;

class CompetitionStatusFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CompetitionStatus::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama_event' => $this->faker->word,
        'kategori_id' => $this->faker->randomDigitNotNull,
        'tanggal_mulai' => $this->faker->word,
        'tanggal_akhir' => $this->faker->word,
        'competition_status_id' => $this->faker->randomDigitNotNull,
        'color' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
