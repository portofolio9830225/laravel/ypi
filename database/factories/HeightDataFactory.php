<?php

namespace Database\Factories;

use App\Models\HeightData;
use Illuminate\Database\Eloquent\Factories\Factory;

class HeightDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = HeightData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'height_predicted_id' => $this->faker->randomDigitNotNull,
        'pemain_id' => $this->faker->randomDigitNotNull,
        'dob' => $this->faker->word,
        'age' => $this->faker->randomDigitNotNull,
        'bulan_awal' => $this->faker->randomDigitNotNull,
        'tahun_awal' => $this->faker->randomDigitNotNull,
        'bulan_akhir' => $this->faker->randomDigitNotNull,
        'tahun_akhir' => $this->faker->randomDigitNotNull,
        'bulan_1' => $this->faker->randomDigitNotNull,
        'bulan_2' => $this->faker->randomDigitNotNull,
        'bulan_3' => $this->faker->randomDigitNotNull,
        'bulan_4' => $this->faker->randomDigitNotNull,
        'bulan_5' => $this->faker->randomDigitNotNull,
        'bulan_6' => $this->faker->randomDigitNotNull,
        'bulan_7' => $this->faker->randomDigitNotNull,
        'bulan_8' => $this->faker->randomDigitNotNull,
        'bulan_9' => $this->faker->randomDigitNotNull,
        'bulan_10' => $this->faker->randomDigitNotNull,
        'bulan_11' => $this->faker->randomDigitNotNull,
        'bulan_12' => $this->faker->randomDigitNotNull,
        'growth_rate' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
