<?php

namespace Database\Factories;

use App\Models\SeasonPlanValue;
use Illuminate\Database\Eloquent\Factories\Factory;

class SeasonPlanValueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = SeasonPlanValue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'ypi_id' => $this->faker->randomDigitNotNull,
        'season_plan_id' => $this->faker->randomDigitNotNull,
        'season_plan_detail_id' => $this->faker->randomDigitNotNull,
        'bg_color' => $this->faker->word,
        'active' => $this->faker->word,
        'week_1' => $this->faker->word,
        'week_2' => $this->faker->word,
        'week_3' => $this->faker->word,
        'week_4' => $this->faker->word,
        'week_5' => $this->faker->word,
        'week_6' => $this->faker->word,
        'week_7' => $this->faker->word,
        'week_8' => $this->faker->word,
        'week_9' => $this->faker->word,
        'week_10' => $this->faker->word,
        'week_11' => $this->faker->word,
        'week_12' => $this->faker->word,
        'week_13' => $this->faker->word,
        'week_14' => $this->faker->word,
        'week_15' => $this->faker->word,
        'week_16' => $this->faker->word,
        'week_17' => $this->faker->word,
        'week_18' => $this->faker->word,
        'week_19' => $this->faker->word,
        'week_20' => $this->faker->word,
        'week_21' => $this->faker->word,
        'week_22' => $this->faker->word,
        'week_23' => $this->faker->word,
        'week_24' => $this->faker->word,
        'week_25' => $this->faker->word,
        'week_26' => $this->faker->word,
        'week_27' => $this->faker->word,
        'week_28' => $this->faker->word,
        'week_29' => $this->faker->word,
        'week_30' => $this->faker->word,
        'week_31' => $this->faker->word,
        'week_32' => $this->faker->word,
        'week_33' => $this->faker->word,
        'week_34' => $this->faker->word,
        'week_35' => $this->faker->word,
        'week_36' => $this->faker->word,
        'week_37' => $this->faker->word,
        'week_38' => $this->faker->word,
        'week_39' => $this->faker->word,
        'week_40' => $this->faker->word,
        'week_41' => $this->faker->word,
        'week_42' => $this->faker->word,
        'week_43' => $this->faker->word,
        'week_44' => $this->faker->word,
        'week_45' => $this->faker->word,
        'week_46' => $this->faker->word,
        'week_47' => $this->faker->word,
        'week_48' => $this->faker->word,
        'week_49' => $this->faker->word,
        'week_50' => $this->faker->word,
        'week_51' => $this->faker->word,
        'week_52' => $this->faker->word,
        'week_53' => $this->faker->word,
        'week_54' => $this->faker->word,
        'week_55' => $this->faker->word,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
