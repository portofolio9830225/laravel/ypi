<?php

namespace Database\Factories;

use App\Models\StrengthConditioningData;
use Illuminate\Database\Eloquent\Factories\Factory;

class StrengthConditioningDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = StrengthConditioningData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'gym_type' => $this->faker->randomDigitNotNull,
        'master_ypi_id' => $this->faker->randomDigitNotNull,
        'week' => $this->faker->randomDigitNotNull,
        'hari' => $this->faker->word,
        'tanggal' => $this->faker->word,
        'comments' => $this->faker->word,
        'rep_1' => $this->faker->randomDigitNotNull,
        'weight_1' => $this->faker->randomDigitNotNull,
        'rep_2' => $this->faker->randomDigitNotNull,
        'weight_2' => $this->faker->randomDigitNotNull,
        'rep_3' => $this->faker->randomDigitNotNull,
        'weight_3' => $this->faker->randomDigitNotNull,
        'rep_4' => $this->faker->randomDigitNotNull,
        'weight_4' => $this->faker->randomDigitNotNull,
        'rep_5' => $this->faker->randomDigitNotNull,
        'weight_5' => $this->faker->randomDigitNotNull,
        'rep_6' => $this->faker->randomDigitNotNull,
        'weight_6' => $this->faker->randomDigitNotNull,
        'tut_rep' => $this->faker->randomDigitNotNull,
        'tut_exercise' => $this->faker->randomDigitNotNull,
        'tonnage' => $this->faker->randomDigitNotNull,
        'average' => $this->faker->randomDigitNotNull,
        'physical_load' => $this->faker->randomDigitNotNull,
        'predicted_1rm' => $this->faker->randomDigitNotNull,
        'average_1rm_predict' => $this->faker->randomDigitNotNull,
        'actual_1rm' => $this->faker->randomDigitNotNull,
        'average_1rm' => $this->faker->randomDigitNotNull,
        'weekly_tut' => $this->faker->randomDigitNotNull,
        'weekly_tonnage' => $this->faker->randomDigitNotNull,
        'weekly_average' => $this->faker->randomDigitNotNull,
        'weekly_load' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
