<?php

namespace Database\Factories;

use App\Models\DetailWeeklyPlan;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailWeeklyPlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DetailWeeklyPlan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'master_ypi_id' => $this->faker->randomDigitNotNull,
        'minggu' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'session' => $this->faker->randomDigitNotNull,
        'jam' => $this->faker->word,
        'details' => $this->faker->word,
        'set' => $this->faker->randomDigitNotNull,
        'rep' => $this->faker->randomDigitNotNull,
        'pace' => $this->faker->word,
        'work_time' => $this->faker->randomDigitNotNull,
        'rest_interval' => $this->faker->randomDigitNotNull,
        'rest_period' => $this->faker->randomDigitNotNull,
        'emphesis_goal' => $this->faker->word,
        'duration' => $this->faker->randomDigitNotNull,
        'intensity' => $this->faker->randomDigitNotNull,
        'tr_load' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
