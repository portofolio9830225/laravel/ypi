<?php

namespace Database\Factories;

use App\Models\MedicalRecord;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicalRecordFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MedicalRecord::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'master_ypi_id' => $this->faker->randomDigitNotNull,
        'week' => $this->faker->randomDigitNotNull,
        'tanggal' => $this->faker->word,
        'hari' => $this->faker->word,
        'injured' => $this->faker->word,
        'primary_injured' => $this->faker->word,
        'detail_injured' => $this->faker->word,
        'treatment_injured' => $this->faker->word,
        'injured_days' => $this->faker->randomDigitNotNull,
        'treatment_injured_days' => $this->faker->randomDigitNotNull,
        'illied' => $this->faker->word,
        'primary_illied' => $this->faker->word,
        'symptomps' => $this->faker->word,
        'treatment_illied' => $this->faker->word,
        'illied_days' => $this->faker->randomDigitNotNull,
        'treatment_illied_days' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
