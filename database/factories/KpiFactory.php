<?php

namespace Database\Factories;

use App\Models\Kpi;
use Illuminate\Database\Eloquent\Factories\Factory;

class KpiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Kpi::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->word,
        'player_id' => $this->faker->randomDigitNotNull,
        'umur' => $this->faker->randomDigitNotNull,
        'height' => $this->faker->randomDigitNotNull,
        'weight' => $this->faker->randomDigitNotNull,
        'beep_test' => $this->faker->randomDigitNotNull,
        'vo2max' => $this->faker->randomDigitNotNull,
        'court_agility_1' => $this->faker->randomDigitNotNull,
        'court_agility_2' => $this->faker->randomDigitNotNull,
        'court_agility_3' => $this->faker->randomDigitNotNull,
        'squad' => $this->faker->randomDigitNotNull,
        'bench_press' => $this->faker->randomDigitNotNull,
        'vertical_jump' => $this->faker->randomDigitNotNull,
        'skipping' => $this->faker->randomDigitNotNull,
        'run' => $this->faker->randomDigitNotNull,
        'note' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
