<?php

namespace Database\Factories;

use App\Models\KpiResult;
use Illuminate\Database\Eloquent\Factories\Factory;

class KpiResultFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = KpiResult::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->word,
        'player_id' => $this->faker->randomDigitNotNull,
        'kpi_parameter_id' => $this->faker->randomDigitNotNull,
        'value' => $this->faker->text,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
