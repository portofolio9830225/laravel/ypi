<?php

namespace Database\Factories;

use App\Models\FitnessTool;
use Illuminate\Database\Eloquent\Factories\Factory;

class FitnessToolFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = FitnessTool::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pemain_id' => $this->faker->randomDigitNotNull,
        'kelompok_id' => $this->faker->randomDigitNotNull,
        'gender' => $this->faker->word,
        'tanggal' => $this->faker->word,
        'beep_test' => $this->faker->randomDigitNotNull,
        'court_agility' => $this->faker->randomDigitNotNull,
        'reach' => $this->faker->randomDigitNotNull,
        'cj_actual' => $this->faker->randomDigitNotNull,
        'cj_elevation' => $this->faker->randomDigitNotNull,
        'sq_actual' => $this->faker->randomDigitNotNull,
        'sq_elevation' => $this->faker->randomDigitNotNull,
        'rl_actual' => $this->faker->randomDigitNotNull,
        'rl_elevation' => $this->faker->randomDigitNotNull,
        'll_actual' => $this->faker->randomDigitNotNull,
        'll_elevation' => $this->faker->randomDigitNotNull,
        'ratio' => $this->faker->randomDigitNotNull,
        'difference' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
