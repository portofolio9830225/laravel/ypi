<?php

namespace Database\Factories;

use App\Models\MasterContact;
use Illuminate\Database\Eloquent\Factories\Factory;

class MasterContactFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MasterContact::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama_lengkap' => $this->faker->word,
        'alamat' => $this->faker->word,
        'negara' => $this->faker->word,
        'kodepos' => $this->faker->word,
        'telepon' => $this->faker->word,
        'email' => $this->faker->word,
        'keterangan' => $this->faker->word,
        'father_height' => $this->faker->randomDigitNotNull,
        'father_weight' => $this->faker->randomDigitNotNull,
        'mother_height' => $this->faker->randomDigitNotNull,
        'mother_weight' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
