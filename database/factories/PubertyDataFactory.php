<?php

namespace Database\Factories;

use App\Models\PubertyData;
use Illuminate\Database\Eloquent\Factories\Factory;

class PubertyDataFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PubertyData::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'height_predicted_id' => $this->faker->randomDigitNotNull,
        'pemain_id' => $this->faker->randomDigitNotNull,
        'dob' => $this->faker->word,
        'age' => $this->faker->randomDigitNotNull,
        'height' => $this->faker->randomDigitNotNull,
        'national_average' => $this->faker->randomDigitNotNull,
        'growth_rate' => $this->faker->randomDigitNotNull,
        'biological_category' => $this->faker->randomDigitNotNull,
        'nation_id' => $this->faker->randomDigitNotNull,
        'bulan_awal' => $this->faker->randomDigitNotNull,
        'tahun_awal' => $this->faker->randomDigitNotNull,
        'created_at' => $this->faker->date('Y-m-d H:i:s'),
        'updated_at' => $this->faker->date('Y-m-d H:i:s'),
        'deleted_at' => $this->faker->date('Y-m-d H:i:s')
        ];
    }
}
