<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterAchievementsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_achievements', function (Blueprint $table) {
            $table->id('id');
            $table->string('nama_event', 100);
            $table->integer('kategori_id');
            $table->date('tanggal_mulai');
            $table->date('tanggal_akhir');
            $table->integer('competition_status_id');
            $table->string('color', 15);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_achievements');
    }
}
