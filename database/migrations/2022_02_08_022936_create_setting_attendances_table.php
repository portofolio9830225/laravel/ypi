<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingAttendancesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_attendances', function (Blueprint $table) {
            $table->id('id');
            $table->integer('bulan');
            $table->integer('tahun');
            $table->string('kelompok', 50);
            $table->integer('kode_pelatih');
            $table->date('tanggal');
            $table->integer('session');
            $table->integer('am_pm');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting_attendances');
    }
}
