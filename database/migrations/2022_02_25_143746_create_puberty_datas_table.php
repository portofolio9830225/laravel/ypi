<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePubertyDatasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puberty_datas', function (Blueprint $table) {
            $table->id('id');
            $table->integer('height_predicted_id');
            $table->integer('pemain_id');
            $table->date('dob');
            $table->integer('age');
            $table->float('height');
            $table->float('national_average');
            $table->float('growth_rate');
            $table->integer('biological_category');
            $table->integer('nation_id');
            $table->integer('bulan_awal');
            $table->integer('tahun_awal');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('puberty_datas');
    }
}
