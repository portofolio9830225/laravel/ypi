<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kpi_parameters', function (Blueprint $table) {
            $table->boolean('shared')->comment('menambah tabel list kpi parameter sebagai template untuk tenant agar terstandarisasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kpi_parameters', function (Blueprint $table) {
            //
        });
    }
};
