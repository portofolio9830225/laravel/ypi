<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDefaultTargetsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_targets', function (Blueprint $table) {
            $table->id('id');
            $table->integer('master_ypi_id');
            $table->float('mon1')->default(0);
            $table->float('mon2')->default(0);
            $table->float('mon3')->default(0);
            $table->float('mon4')->default(0);
            $table->float('tue1')->default(0);
            $table->float('tue2')->default(0);
            $table->float('tue3')->default(0);
            $table->float('tue4')->default(0);
            $table->float('wed1')->default(0);
            $table->float('wed2')->default(0);
            $table->float('wed3')->default(0);
            $table->float('wed4')->default(0);
            $table->float('thu1')->default(0);
            $table->float('thu2')->default(0);
            $table->float('thu3')->default(0);
            $table->float('thu4')->default(0);
            $table->float('fri1')->default(0);
            $table->float('fri2')->default(0);
            $table->float('fri3')->default(0);
            $table->float('fri4')->default(0);
            $table->float('sat1')->default(0);
            $table->float('sat2')->default(0);
            $table->float('sat3')->default(0);
            $table->float('sat4')->default(0);
            $table->float('sun1')->default(0);
            $table->float('sun2')->default(0);
            $table->float('sun3')->default(0);
            $table->float('sun4')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('default_targets');
    }
}
