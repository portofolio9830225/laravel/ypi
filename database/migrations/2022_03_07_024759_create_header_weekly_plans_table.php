<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderWeeklyPlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_weekly_plans', function (Blueprint $table) {
            $table->id('id');
            $table->integer('master_ypi_id');
            $table->integer('minggu');
            $table->date('tanggal_awal');
            $table->date('tanggal_akhir');
            $table->integer('actual_tr_vol');
            $table->integer('actual_tr_vol_prosen');
            $table->integer('actual_tr_intensity');
            $table->integer('actual_tr_load');
            $table->integer('target_tr_vol');
            $table->integer('target_tr_vol_prosen');
            $table->integer('target_tr_intensity');
            $table->integer('target_tr_load');
            $table->string('weekly_training_goal', 100);
            $table->text('notes');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('header_weekly_plans');
    }
}
