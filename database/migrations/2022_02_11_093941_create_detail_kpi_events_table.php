<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailKpiEventsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_kpi_events', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->date('tanggal_kpi');
            $table->string('keterangan_1', 50);
            $table->string('keterangan_2', 50);
            $table->string('keterangan_3', 50);
            $table->string('keterangan_4', 50);
            $table->string('event_name', 75);
            $table->string('target_1', 50);
            $table->string('target_2', 50);
            $table->string('target_3', 50);
            $table->string('target_4', 50);
            $table->string('actual_1', 50);
            $table->string('actual_2', 50);
            $table->string('actual_3', 50);
            $table->string('actual_4', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_kpi_events');
    }
}
