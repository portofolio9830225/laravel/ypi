<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailKpiPhysicsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_kpi_physics', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->date('tanggal_kpi');
            $table->string('keterangan', 100);
            $table->date('tanggal_1');
            $table->float('pb_1');
            $table->float('target_1');
            $table->float('actual_1');
            $table->date('tanggal_2');
            $table->float('pb_2');
            $table->float('target_2');
            $table->float('actual_2');
            $table->date('tanggal_3');
            $table->float('pb_3');
            $table->float('target_3');
            $table->float('actual_3');
            $table->date('tanggal_4');
            $table->float('pb_4');
            $table->float('target_4');
            $table->float('actual_4');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_kpi_physics');
    }
}
