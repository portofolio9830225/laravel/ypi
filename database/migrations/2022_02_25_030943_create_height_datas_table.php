<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeightDatasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('height_datas', function (Blueprint $table) {
            $table->id('id');
            $table->integer('height_predicted_id');
            $table->integer('pemain_id');
            $table->date('dob');
            $table->integer('age');
            $table->integer('bulan_awal');
            $table->integer('tahun_awal');
            $table->integer('bulan_akhir');
            $table->integer('tahun_akhir');
            $table->float('bulan_1');
            $table->float('bulan_2');
            $table->float('bulan_3');
            $table->float('bulan_4');
            $table->float('bulan_5');
            $table->float('bulan_6');
            $table->float('bulan_7');
            $table->float('bulan_8');
            $table->float('bulan_9');
            $table->float('bulan_10');
            $table->float('bulan_11');
            $table->float('bulan_12');
            $table->float('growth_rate');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('height_datas');
    }
}
