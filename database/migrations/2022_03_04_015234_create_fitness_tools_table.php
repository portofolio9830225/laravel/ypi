<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFitnessToolsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitness_tools', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->integer('kelompok_id');
            $table->string('gender', 10);
            $table->date('tanggal');
            $table->float('beep_test');
            $table->float('court_agility');
            $table->float('reach');
            $table->float('cj_actual');
            $table->float('cj_elevation');
            $table->float('sq_actual');
            $table->float('sq_elevation');
            $table->float('rl_actual');
            $table->float('rl_elevation');
            $table->float('ll_actual');
            $table->float('ll_elevation');
            $table->float('ratio');
            $table->float('difference');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fitness_tools');
    }
}
