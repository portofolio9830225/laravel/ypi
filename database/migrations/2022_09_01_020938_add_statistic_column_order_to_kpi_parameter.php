<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('kpi_parameters', function (Blueprint $table) {
            $table->integer('column_order');
            $table->boolean('calculate_statistic');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('kpi_parameters', function (Blueprint $table) {
            $table->dropColumn('column_order');
            $table->dropColumn('calculate_statistic');
        });
    }
};
