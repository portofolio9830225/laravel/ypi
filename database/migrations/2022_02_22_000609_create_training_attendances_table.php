<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrainingAttendancesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('training_attendances', function (Blueprint $table) {
            $table->id('id');
            $table->integer('kelompok_id');
            $table->integer('master_pelatih_id');
            $table->integer('pemain_id');
            $table->date('tanggal');
            $table->integer('session');
            $table->integer('presence_id');
            $table->string('note', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('training_attendances');
    }
}
