<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRmCalculationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rm_calculations', function (Blueprint $table) {
            $table->id();
            $table->integer('master_ypi_id');
            $table->integer('pemain_id');
            $table->float('weight');
            $table->float('reps');
            $table->float('rm_calc');
            $table->integer('week');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rm_calculations');
    }
}
