<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKpiValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpi_values', function (Blueprint $table) {
            $table->id('id');
            $table->integer('kpi_indicator_id');
            $table->integer('kpi_subindicator_id');
            $table->integer('player_id');
            $table->integer('group_id');
            $table->date('date');
            $table->integer('pb');
            $table->integer('target');
            $table->integer('actual');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kpi_values');
    }
}
