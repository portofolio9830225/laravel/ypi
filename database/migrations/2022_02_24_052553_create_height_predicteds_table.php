<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeightPredictedsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('height_predicteds', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->integer('age');
            $table->date('date_predicted');
            $table->float('height');
            $table->float('weight');
            $table->float('fathers_height');
            $table->float('mothers_height');
            $table->float('predicted_height');
            $table->float('maturity_status');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('height_predicteds');
    }
}
