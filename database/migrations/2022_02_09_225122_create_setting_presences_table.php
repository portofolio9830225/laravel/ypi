<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingPresencesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_presences', function (Blueprint $table) {
            $table->id('id');
            $table->integer('kelompok_id');
            $table->integer('pelatih_id');
            $table->string('keterangan', 50);
            $table->string('inisial', 2);
            $table->boolean('presence');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('setting_presences');
    }
}
