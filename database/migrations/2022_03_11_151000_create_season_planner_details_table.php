<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonPlannerDetailsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_planner_details', function (Blueprint $table) {
            $table->id('id');
            $table->integer('master_ypi_id');
            $table->integer('season_planner_id');
            $table->string('name', 40);
            $table->text('description');
            $table->enum('type', ['text', 'number']);
            $table->boolean('inChart');
            $table->integer('ytp_charts_id');
            $table->enum('chartType', ['line', 'bar', 'scatter']);
            $table->string('chartColor', 15);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('season_planner_details');
    }
}
