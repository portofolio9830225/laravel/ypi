<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrengthConditionValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strength_condition_values', function (Blueprint $table) {
            $table->id('id');
            $table->integer('strength_condition_id');
            $table->integer('strength_condition_player_id');
            $table->string('name', 20);
            $table->float('reps');
            $table->float('weight');
            $table->float('predict');
            $table->float('min');
            $table->float('max');
            $table->float('strength');
            $table->float('last');
            $table->float('diff');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('strength_condition_values');
    }
}
