<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('club', function (Blueprint $table) {
            $table->string('id')->change();
            $table->longText('data');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->string('active_club_id')->change();
            $table->dropColumn('level');
            $table->enum('role', ['superadmin', 'club_admin', 'coach']);
        });

        Schema::table('tabel_kelompoks', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('master_achievements', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('level_kategoris', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('tabel_kategoris', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('competition_statuses', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('master_contacts', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('master_pelatihs', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('master_pemains', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('national_height_avgs', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('group_activities', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('annual_plan_settings', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('annual_plans', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('master_ypis', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('header_weekly_plans', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('result_categories', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('detail_competition_reports', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('competition_reports', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('medical_records', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('setting_attendances', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('setting_presences', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('training_attendances', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('kpi_results', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('gym_types', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('exercise_categories', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('sub_exercises', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('strength_conditionings', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('strength_conditioning_datas', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('fitness_tools', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('height_predicteds', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('kpi_parameters', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('constants', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('season_plans', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('puberty_datas', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('ypi_goals', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('ypi_events', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('strength_condition_players', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('strength_condition_values', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('season_plan_values', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('height_datas', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('detail_weekly_plans', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });

        Schema::table('default_targets', function (Blueprint $table) {
            $table->string('tenant_id');
            $table->unique(['tenant_id', 'id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('club', function (Blueprint $table) {
            $table->integer('id')->change();
            $table->dropColumn('data');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->integer('active_club_id')->change();
            $table->string('level');
            $table->dropColumn('role');
        });

        Schema::table('tabel_kelompoks', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('master_achievements', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('tabel_kategoris', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('competition_statuses', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('master_contacts', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('master_pelatihs', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('level_kategoris', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('master_pemains', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('national_height_avgs', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('group_activities', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('annual_plan_settings', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('annual_plans', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('master_ypis', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('header_weekly_plans', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('result_categories', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('detail_competition_reports', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('competition_reports', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('medical_records', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('setting_attendances', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('setting_presences', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('training_attendances', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('kpi_results', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('gym_types', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('exercise_categories', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('sub_exercises', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('strength_conditionings', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('strength_conditioning_datas', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('fitness_tools', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('height_predicteds', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('kpi_parameters', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('constants', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('season_plans', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('puberty_datas', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('ypi_goals', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('ypi_events', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('strength_condition_players', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('strength_condition_values', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('season_plan_values', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('height_datas', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('detail_weekly_plans', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });

        Schema::table('default_targets', function (Blueprint $table) {
            $table->dropUnique(['tenant_id', 'id']);
            $table->dropColumn('tenant_id');
        });
    }
};
