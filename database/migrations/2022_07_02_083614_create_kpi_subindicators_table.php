<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKpiSubindicatorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpi_subindicators', function (Blueprint $table) {
            $table->id('id');
            $table->integer('kpi_indicator_id');
            $table->string('name', 50);
            $table->enum('indicator_type', ['more_is_better','less_is_better']);
            $table->string('color', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kpi_subindicators');
    }
}
