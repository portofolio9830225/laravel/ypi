<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailWeeklyPlansTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_weekly_plans', function (Blueprint $table) {
            $table->id('id');
            $table->integer('master_ypi_id');
            $table->integer('minggu');
            $table->date('tanggal');
            $table->integer('session');
            $table->time('jam');
            $table->string('details', 50);
            $table->integer('set');
            $table->integer('rep');
            $table->string('pace', 10);
            $table->integer('work_time');
            $table->integer('rest_interval');
            $table->integer('rest_period');
            $table->string('emphesis_goal', 50);
            $table->integer('duration');
            $table->integer('intensity');
            $table->integer('tr_load');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_weekly_plans');
    }
}
