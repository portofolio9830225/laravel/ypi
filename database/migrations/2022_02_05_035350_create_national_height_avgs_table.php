<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNationalHeightAvgsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('national_height_avgs', function (Blueprint $table) {
            $table->id('id');
            $table->integer('nation_id');
            $table->string('nation', 50);
            $table->integer('age');
            $table->float('height');
            $table->string('gender', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('national_height_avgs');
    }
}
