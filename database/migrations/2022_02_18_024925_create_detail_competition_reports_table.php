<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailCompetitionReportsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_competition_reports', function (Blueprint $table) {
            $table->id('id');
            $table->integer('competition_report_id');
            $table->string('round', 10);
            $table->string('opponent', 50);
            $table->string('nationality', 50);
            $table->integer('set1a');
            $table->integer('set1b');
            $table->integer('set2a');
            $table->integer('set2b');
            $table->integer('set3a');
            $table->integer('set3b');
            $table->string('comment', 65);
            $table->string('result', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_competition_reports');
    }
}
