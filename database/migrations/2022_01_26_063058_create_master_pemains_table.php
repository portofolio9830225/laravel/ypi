<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterPemainsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_pemains', function (Blueprint $table) {
            $table->id('id');
            $table->string('nomor_ic', 50);
            $table->string('nama_lengkap', 100);
            $table->string('alamat', 100);
            $table->string('negara', 50);
            $table->integer('kodepos');
            $table->string('tempat_lahir', 50);
            $table->date('tanggal_lahir');
            $table->string('home_phone', 25);
            $table->string('hand_phone', 25);
            $table->string('business_phone', 25);
            $table->string('nama_sekolah', 50);
            $table->string('kelas', 10);
            $table->string('wali_kelas', 50);
            $table->string('no_ruang', 10);
            $table->text('keterangan');
            $table->string('golongan_darah', 10);
            $table->string('email');
            $table->integer('contact_id');
            $table->boolean('aktif');
            $table->string('sex', 15);
            $table->integer('kelompok_id');
            $table->boolean('tunggal_putra');
            $table->boolean('tunggal_putri');
            $table->boolean('ganda_putra');
            $table->boolean('ganda_putri');
            $table->boolean('ganda_campuran');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_pemains');
    }
}
