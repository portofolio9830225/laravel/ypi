<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHeaderGapsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('header_gaps', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->date('tanggal');
            $table->string('event_name', 20);
            $table->string('present', 50);
            $table->string('future', 50);
            $table->string('player_gap', 50);
            $table->float('gap_value1');
            $table->float('gap_value2');
            $table->float('gap_value3');
            $table->float('gap_value4');
            $table->float('gap_value5');
            $table->string('competition', 50);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('header_gaps');
    }
}
