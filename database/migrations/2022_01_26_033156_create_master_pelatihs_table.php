<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterPelatihsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_pelatihs', function (Blueprint $table) {
            $table->id('id');
            $table->string('nomor_ic', 50);
            $table->string('nama_lengkap', 100);
            $table->string('alamat', 100);
            $table->string('negara', 50);
            $table->integer('kodepos');
            $table->string('tempat_lahir', 50);
            $table->date('tanggal_lahir');
            $table->string('home_phone', 25);
            $table->string('hand_phone', 25);
            $table->string('business_phone', 25);
            $table->string('email');
            $table->string('golongan_darah', 10);
            $table->text('keterangan');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_pelatihs');
    }
}
