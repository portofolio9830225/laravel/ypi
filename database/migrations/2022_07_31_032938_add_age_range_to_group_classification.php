<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tabel_kelompoks', function (Blueprint $table) {
            $table->integer('age_min');
            $table->integer('age_max');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tabel_kelompoks', function (Blueprint $table) {
            $table->dropColumn('age_min');
            $table->dropColumn('age_max');
        });
    }
};
