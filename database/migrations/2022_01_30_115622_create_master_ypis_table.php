<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterYpisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_ypis', function (Blueprint $table) {
            $table->id();
            $table->string('keterangan', 100);
            $table->integer('kode_pelatih');
            $table->float('voltarget');
            $table->dateTime('tanggal_mulai');
            $table->dateTime('tanggal_selesai');
            $table->float('default_intensity');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_ypis');
    }
}
