<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFitnessTestsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fitness_tests', function (Blueprint $table) {
            $table->id('id');
            $table->date('tanggal_test');
            $table->string('gender', 10);
            $table->integer('kelompok_id');
            $table->integer('pemain_id');
            $table->float('court_agility');
            $table->float('vo2max');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fitness_tests');
    }
}
