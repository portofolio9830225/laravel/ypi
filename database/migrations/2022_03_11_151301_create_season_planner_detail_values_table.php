<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonPlannerDetailValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_planner_detail_values', function (Blueprint $table) {
            $table->id('id');
            $table->integer('master_ypi_id');
            $table->integer('season_planner_id');
            $table->integer('season_planner_detail_id');
            $table->integer('week_index');
            $table->string('value', 100);
            $table->string('text_color', 15);
            $table->string('description', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('season_planner_detail_values');
    }
}
