<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPbKpisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_pb_kpis', function (Blueprint $table) {
            $table->id('id');
            $table->integer('kpi_physic_id');
            $table->date('tanggal');
            $table->float('pb');
            $table->float('target');
            $table->float('actual');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detail_pb_kpis');
    }
}
