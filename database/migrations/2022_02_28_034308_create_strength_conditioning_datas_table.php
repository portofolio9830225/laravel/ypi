<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStrengthConditioningDatasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('strength_conditioning_datas', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->integer('gym_type');
            $table->integer('master_ypi_id');
            $table->integer('week');
            $table->string('hari', 10);
            $table->date('tanggal');
            $table->string('comments', 50);
            $table->float('rep_1');
            $table->float('weight_1');
            $table->float('rep_2');
            $table->float('weight_2');
            $table->float('rep_3');
            $table->float('weight_3');
            $table->float('rep_4');
            $table->float('weight_4');
            $table->float('rep_5');
            $table->float('weight_5');
            $table->float('rep_6');
            $table->float('weight_6');
            $table->float('tut_rep');
            $table->float('tut_exercise');
            $table->float('tonnage');
            $table->float('average');
            $table->float('physical_load');
            $table->float('predicted_1rm');
            $table->float('average_1rm_predict');
            $table->float('actual_1rm');
            $table->float('average_1rm');
            $table->float('weekly_tut');
            $table->float('weekly_tonnage');
            $table->float('weekly_average');
            $table->float('weekly_load');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('strength_conditioning_datas');
    }
}
