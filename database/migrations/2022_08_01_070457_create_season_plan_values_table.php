<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeasonPlanValuesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('season_plan_values', function (Blueprint $table) {
            $table->id('id');
            $table->integer('ypi_id');
            $table->integer('season_plan_id');
            $table->integer('season_plan_detail_id');
            $table->string('bg_color', 15);
            $table->boolean('active');
            $table->string('week_1', 200);
            $table->string('week_2', 200);
            $table->string('week_3', 200);
            $table->string('week_4', 200);
            $table->string('week_5', 200);
            $table->string('week_6', 200);
            $table->string('week_7', 200);
            $table->string('week_8', 200);
            $table->string('week_9', 200);
            $table->string('week_10', 200);
            $table->string('week_11', 200);
            $table->string('week_12', 200);
            $table->string('week_13', 200);
            $table->string('week_14', 200);
            $table->string('week_15', 200);
            $table->string('week_16', 200);
            $table->string('week_17', 200);
            $table->string('week_18', 200);
            $table->string('week_19', 200);
            $table->string('week_20', 200);
            $table->string('week_21', 200);
            $table->string('week_22', 200);
            $table->string('week_23', 200);
            $table->string('week_24', 200);
            $table->string('week_25', 200);
            $table->string('week_26', 200);
            $table->string('week_27', 200);
            $table->string('week_28', 200);
            $table->string('week_29', 200);
            $table->string('week_30', 200);
            $table->string('week_31', 200);
            $table->string('week_32', 200);
            $table->string('week_33', 200);
            $table->string('week_34', 200);
            $table->string('week_35', 200);
            $table->string('week_36', 200);
            $table->string('week_37', 200);
            $table->string('week_38', 200);
            $table->string('week_39', 200);
            $table->string('week_40', 200);
            $table->string('week_41', 200);
            $table->string('week_42', 200);
            $table->string('week_43', 200);
            $table->string('week_44', 200);
            $table->string('week_45', 200);
            $table->string('week_46', 200);
            $table->string('week_47', 200);
            $table->string('week_48', 200);
            $table->string('week_49', 200);
            $table->string('week_50', 200);
            $table->string('week_51', 200);
            $table->string('week_52', 200);
            $table->string('week_53', 200);
            $table->string('week_54', 200);
            $table->string('week_55', 200);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('season_plan_values');
    }
}
