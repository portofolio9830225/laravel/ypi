<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYpiColorsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ypi_colors', function (Blueprint $table) {
            $table->id('id');
            $table->integer('master_ypi_id');
            $table->string('detail', 20);
            $table->string('periodization', 20);
            $table->string('skills', 20);
            $table->string('physic', 20);
            $table->string('tactic', 20);
            $table->string('mental', 20);
            $table->string('progress', 20);
            $table->string('peak_index', 20);
            $table->string('timing', 20);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ypi_colors');
    }
}
