<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubExercisesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_exercises', function (Blueprint $table) {
            $table->id('id');
            $table->string('excercise_desc', 50);
            $table->integer('gym_type_id');
            $table->integer('tut_per_rep');
            $table->integer('exercise_category_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_exercises');
    }
}
