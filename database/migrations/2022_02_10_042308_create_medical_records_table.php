<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalRecordsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_records', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->integer('master_ypi_id');
            $table->integer('week');
            $table->date('tanggal');
            $table->string('hari', 10);
            $table->boolean('injured');
            $table->string('primary_injured', 50);
            $table->string('detail_injured', 50);
            $table->boolean('treatment_injured');
            $table->integer('injured_days');
            $table->integer('treatment_injured_days');
            $table->boolean('illied');
            $table->string('primary_illied', 50);
            $table->string('symptomps', 50);
            $table->boolean('treatment_illied');
            $table->integer('illied_days');
            $table->integer('treatment_illied_days');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('medical_records');
    }
}
