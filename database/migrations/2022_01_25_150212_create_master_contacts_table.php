<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMasterContactsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_contacts', function (Blueprint $table) {
            $table->id('id');
            $table->string('nama_lengkap', 50);
            $table->string('alamat', 100);
            $table->string('negara', 50);
            $table->string('kodepos', 10);
            $table->string('telepon', 25);
            $table->string('email');
            $table->string('keterangan', 250);
            $table->float('father_height');
            $table->float('father_weight');
            $table->float('mother_height');
            $table->float('mother_weight');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('master_contacts');
    }
}
