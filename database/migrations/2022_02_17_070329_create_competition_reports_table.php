<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionReportsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competition_reports', function (Blueprint $table) {
            $table->id('id');
            $table->integer('pemain_id');
            $table->integer('master_achievement_id');
            $table->string('venue', 50);
            $table->string('specialist', 50);
            $table->string('event_level', 50);
            $table->integer('result_id');
            $table->text('summary_notes');
            $table->text('suggestion_notes');
            $table->integer('kelompok_id');
            $table->integer('partner_id')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competition_reports');
    }
}
