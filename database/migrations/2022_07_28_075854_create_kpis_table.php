<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKpisTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpis', function (Blueprint $table) {
            $table->id('id');
            $table->date('date');
            $table->integer('player_id');
            $table->integer('umur');
            $table->float('height');
            $table->float('weight');
            $table->float('beep_test');
            $table->float('vo2max');
            $table->float('court_agility_1');
            $table->float('court_agility_2');
            $table->float('court_agility_3');
            $table->float('squad');
            $table->float('bench_press');
            $table->float('vertical_jump');
            $table->float('skipping');
            $table->float('run');
            $table->text('note');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('kpis');
    }
}
