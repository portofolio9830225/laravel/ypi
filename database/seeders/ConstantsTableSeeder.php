<?php

namespace Database\Seeders;

use App\Models\KpiParameter;
use App\Models\SeasonPlan;
use App\Models\SeasonPlanDetail;
use Illuminate\Database\Seeder;

class ConstantsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        $spd = SeasonPlanDetail::with('seasonPlan')->get();
        $defaultSp = [];
        foreach ($spd as $key => $value) {
            array_push($defaultSp, [
                'season_plan_id' => $value->season_plan_id,
                'season_plan_detail_id' => $value->id,
                'active' => true,
                'bg_color' => "#ffa940",
                'seasonPlan' => $value->seasonPlan->name,
                'seasonPlanDetail' => $value->name,
            ]);
        }

        $sp1 = SeasonPlan::where('name', 'DETAIL AND SPORTS & STUDY BALANCE')->first();
        $sp2 = SeasonPlan::where('name', 'PERIODIZATION')->first();
        $sp3 = SeasonPlan::where('name', 'TECHNICAL DEVELOPMENT')->first();
        $sp4 = SeasonPlan::where('name', 'TACTICAL AND STRATEGY')->first();
        $sp5 = SeasonPlan::where('name', 'PHYSICAL DEVELOPMENT')->first();
        $sp6 = SeasonPlan::where('name', 'MENTAL SKILLS')->first();
        $sp8 = SeasonPlan::where('name', '% WORKOUT & TIME')->first();

        $spd1 = $spd->where('name', 'Target')->first();
        $spd2 = $spd->where('name', 'Rate Important of Competition')->first();
        $spd3 = $spd->where('name', 'Date for Fitness Test')->first();
        $spd4 = SeasonPlanDetail::where('name', 'Academy Index (1-5)')->first();
        $spd5 = SeasonPlanDetail::where('name', 'Sport Index (1-5)')->first();
        $spd6 = $spd->where('name', 'Public Holiday')->first();
        $spd7 = SeasonPlanDetail::where('name', 'Training Phase')->first();
        $spd8 = SeasonPlanDetail::where('name', 'Sub Phase')->first();
        $spd35 = SeasonPlanDetail::where('name', '% Intensity')->first();
        $spd36 = SeasonPlanDetail::where('name', '% Volume')->first();
        $spd37 = SeasonPlanDetail::where('name', '% Spesific')->first();
        $spd38 = SeasonPlanDetail::where('name', 'No of hours per week')->first();
        $spd39 = SeasonPlanDetail::where('name', 'Grand total no of hours')->first();

        $kpiPar1 = KpiParameter::where('name', 'Age')->first();
        $kpiPar2 = KpiParameter::where('name', 'Height (cm)')->first();
        $kpiPar3 = KpiParameter::where('name', 'Weight (cm)')->first();
        $kpiPar4 = KpiParameter::where('name', 'Beep Test (level, shuttle)')->first();
        $kpiPar5 = KpiParameter::where('name', 'VO2Max (ml/kg/mn)')->first();
        $kpiPar6 = KpiParameter::where('name', 'Court Agility Clockwise (sec)')->first();
        $kpiPar7 = KpiParameter::where('name', 'Court Agility Anti Clockwise (sec)')->first();
        $kpiPar8 = KpiParameter::where('name', 'Court Agility Random (sec)')->first();
        $kpiPar9 = KpiParameter::where('name', 'Squat (kg)')->first();
        $kpiPar10 = KpiParameter::where('name', 'Bench Press (kg)')->first();

        \DB::table('constants')->insert(array(
            0 =>
            array(
                'key' => 'FIXED_SEASON_PLANS_ID',
                'value' => '0',
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'key' => 'FIXED_SEASON_PLAN_DETAILS_ID',
                'value' => "{$spd7->id},{$spd8->id},{$spd35->id},{$spd36->id},{$spd37->id}",
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'key' => 'DEFAULT_SEASON_PLANS',
                'value' => json_encode($defaultSp),
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'key' => 'SPD_ACADEMY_INDEX_ID',
                'value' => $spd4->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'key' => 'SPD_SPORT_INDEX_ID',
                'value' => $spd5->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'key' => 'SPD_INTENSITY_ID',
                'value' => $spd35->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'key' => 'SPD_VOLUME_ID',
                'value' => $spd36->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'key' => 'SPD_SPESIFIC_ID',
                'value' => $spd37->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'key' => 'SP_STUDY_BALANCE_ID',
                'value' => $sp1->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'key' => 'SPD_TARGET_ID',
                'value' => $spd1->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'key' => 'SPD_RATE_IMPORTANT_ID',
                'value' => $spd2->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'key' => 'SPD_DATE_FITNESS_ID',
                'value' => $spd3->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'key' => 'SPD_PUBLIC_HOLIDAY_ID',
                'value' => $spd6->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            13 =>
            array(
                'key' => 'SPD_TRAINING_PHASE_ID',
                'value' => $spd7->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            14 =>
            array(
                'key' => 'SPD_SUB_PHASE_ID',
                'value' => $spd8->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            15 =>
            array(
                'key' => 'SP_WORK_TIME_ID',
                'value' => $sp8->id,
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            16 =>
            array(
                'key' => 'YTP_DATE_COLOR',
                'value' => '#99ccff',
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            17 =>
            array(
                'key' => 'YTP_SEASON_PLAN_COLOR',
                'value' => '#ff6600',
                'created_at' => '2022-08-11 23:09:16',
                'updated_at' => '2022-08-11 23:09:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            18 =>
            array(
                'key' => 'SP_TECH_DEV_ID',
                'value' => $sp3->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            19 =>
            array(
                'key' => 'SP_TACTIC_ID',
                'value' => $sp4->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            20 =>
            array(
                'key' => 'SP_PHYSICAL_DEV_ID',
                'value' => $sp5->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            21 =>
            array(
                'key' => 'SP_MENTAL_ID',
                'value' => $sp6->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            22 =>
            array(
                'key' => 'SPD_INTENSITY_ID',
                'value' => $spd35->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            23 =>
            array(
                'key' => 'SPD_VOLUME_ID',
                'value' => $spd36->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            24 =>
            array(
                'key' => 'SPD_SPESIFIC_ID',
                'value' => $spd37->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            25 =>
            array(
                'key' => 'SPD_NO_HOUR_WEEK_ID',
                'value' => $spd38->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            26 =>
            array(
                'key' => 'SPD_TOTAL_HOUR_WEEK_ID',
                'value' => $spd39->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            27 =>
            array(
                'key' => 'PARAMETER_KPI_COMPARISON',
                'value' => sprintf('[{"id":1,"name":"Fastest Court Agility","type":"min","parameters_id":[%s,%s,%s]}]', $kpiPar6->id, $kpiPar7->id, $kpiPar8->id),
                'created_at' => '2022-09-26 13:31:33',
                'updated_at' => '2022-10-31 10:38:41',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            28 =>
            array(
                'key' => 'DATA_VO2MAX_CA_COMPARISON',
                'value' => '{"vo2max_parameter_id":' . $kpiPar5->id . ',"ca_min_custom_parameter_id":1}',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            29 =>
            array(
                'key' => 'DATA_BENCHPRESS_SQUAT_COMPARISON',
                'value' => sprintf('{"benchpress_parameter_id":%s,"squat_parameter_id":%s}', $kpiPar10->id, $kpiPar9->id),
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            30 =>
            array(
                'key' => 'SP_PERIODIZATION_ID',
                'value' => $sp2->id,
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            31 =>
            array(
                'key' => 'HELP_MATURATIONAL_STATUS',
                'value' => '<p><strong>Height Predicted Formula:</strong></p><table style="width: auto;"><tbody><tr><th colSpan="1" rowSpan="1" width="auto">Age</th><th colSpan="1" rowSpan="1" width="auto">Formula (Male)</th></tr><tr><td colSpan="1" rowSpan="1" width="auto">&lt;=10</td><td colSpan="1" rowSpan="1" width="auto">(-11.038 +0.97135 * (height / 2.54) + (-0.0039981 * weight) / 0.4536 + 0.45932 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">11</td><td colSpan="1" rowSpan="1" width="auto">(-10.4917 +0.81239 * (data.height / 2.54) + (-0.002905 * data.weight) / 0.4536 + 0.54781 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">12</td><td colSpan="1" rowSpan="1" width="auto">(-9.3522 +0.68325 * (data.height / 2.54) + (-0.0020076 * data.weight) / 0.4536 + 0.60927 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">13</td><td colSpan="1" rowSpan="1" width="auto">(-7.8632 +0.60818 * (data.height / 2.54) + (-0.0013895 * data.weight) / 0.4536 + 0.62407 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">14</td><td colSpan="1" rowSpan="1" width="auto">(-6.4299 +0.59151 * (data.height / 2.54) + (-0.0009776 * data.weight) / 0.4536 + 0.58762 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">15</td><td colSpan="1" rowSpan="1" width="auto">(-5.1282 +0.63757 * (data.height / 2.54) + (-0.0006988 * data.weight) / 0.4536 + 0.49536 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">16</td><td colSpan="1" rowSpan="1" width="auto">(-3.9292 +0.75069 * (data.height / 2.54) + (-0.0004795 * data.weight) / 0.4536 + 0.34271 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">&gt;=17</td><td colSpan="1" rowSpan="1" width="auto">(-3.283 +0.9352 * (data.height / 2.54) + (-0.000247 * data.weight) / 0.4536 + 0.1251 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr></tbody></table><p><br></p><table style="width: auto;"><tbody><tr><th colSpan="1" rowSpan="1" width="auto">Age</th><th colSpan="1" rowSpan="1" width="auto">Formula (Female)</th></tr><tr><td colSpan="1" rowSpan="1" width="auto">&lt;=10</td><td colSpan="1" rowSpan="1" width="auto">(0.33468 +0.82771 * (data.height / 2.54) + (-0.007397 * data.weight) / 0.4536 + 0.37312 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">11</td><td colSpan="1" rowSpan="1" width="auto">(3.50436 +0.67173 * (data.height / 2.54) + (-0.006136 * data.weight) / 0.4536 + 0.42042 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">12</td><td colSpan="1" rowSpan="1" width="auto">(4.84365 +0.64452 * (data.height / 2.54) + (-0.004894 * data.weight) / 0.4536 + 0.3949 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">13</td><td colSpan="1" rowSpan="1" width="auto">(3.21417 +0.7226 * (data.height / 2.54) + (-0.003661 * data.weight) / 0.4536 + 0.31163 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">14</td><td colSpan="1" rowSpan="1" width="auto">(0.32425 +0.85062 * (data.height / 2.54) + (-0.0025 * data.weight) / 0.4536 + 0.20235 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">15</td><td colSpan="1" rowSpan="1" width="auto">(-2.35055 +0.97319 * (data.height / 2.54) + (-0.001477 * data.weight) / 0.4536 + 0.0988 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">16</td><td colSpan="1" rowSpan="1" width="auto">(-3.17885 +1.03496 * (data.height / 2.54) + (-0.000655 * data.weight) / 0.4536 + 0.03272 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">&gt;=17</td><td colSpan="1" rowSpan="1" width="auto">(-0.65579 +0.98054 * (data.height / 2.54) + (-0.0001 * data.weight) / 0.4536 + 0.03584 *((2.316 +0.955 * (data.fathers_height / 2.54) + (2.803 + 0.953 * (data.mothers_height / 2.54))) / 2)) *2.54;</td></tr></tbody></table><p> &nbsp; <strong>Apa itu status kedewasaan / Maturational status?</strong></p><p>Perubahan fisik ini dikenal sebagai proses \'pematangan biologis\'. Mereka mewakili status kedewasaan seseorang pada saat pengukuran yang diukur dengan usia kerangka dan karakteristik seks sekunder seperti perkembangan otot, rambut kemaluan dan ketiak, pertumbuhan payudara, dll.23 Okt 2020</p><p><strong>Apa itu contoh pematangan?</strong></p><p>1) KEDEWASAA FISIK- Mengacu pada pertumbuhan dan perkembangan fisik yang dialami manusia sampai usia tua. Sebagai contoh; seorang anak terutama tergantung pada refleksnya pada tahap awal perkembangan. Pertumbuhan berat badan, tinggi badan, massa tubuh, perluasan jaringan otot terlihat pada proses pematangan.</p><p>Apa itu pematangan secara sederhana?</p><p>Pematangan adalah proses mencapai tahap perkembangan penuh atau lanjutan. Dengan kata lain, pematangan adalah proses pematangan (atau pematangan) atau mencapai kedewasaan. Kata tersebut sering digunakan untuk merujuk pada proses seseorang berkembang secara fisik atau mulai bertindak lebih dewasa (lebih dewasa).</p><p><br></p><p><br></p><p><br></p><p><br></p>',
                'created_at' => '2022-10-28 14:20:59',
                'updated_at' => '2022-11-11 10:25:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            32 =>
            array(
                'key' => 'HELP_SC_PROGRAM',
                'value' => '<table style="width: auto;"><tbody><tr><th colSpan="1" rowSpan="1" width="auto">Field</th><th colSpan="1" rowSpan="1" width="auto">Formula</th></tr><tr><td colSpan="1" rowSpan="1" width="auto">Tut/Exercise</td><td colSpan="1" rowSpan="1" width="auto">(rep1 + rep2 + rep3 + rep4 + rep5 + rep6) * Tut/Rep</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Tonnage Kg</td><td colSpan="1" rowSpan="1" width="auto">SUM(weight 1-6 * rep 1-6)</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Physical Load</td><td colSpan="1" rowSpan="1" width="auto">totalReps * tut/rep * tonnage * (average kg / actual_1rm)</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Average 1RM</td><td colSpan="1" rowSpan="1" width="auto">(average kg / actual_1rm) * 100</td></tr></tbody></table><p>Sub Strenght and Conditioning setting ini adalah tool yang menyediakan jenis2 latihan beban yang terkait dengan permainan badminton, sehingga nantinya pelatih didalam membuat program sesi latihan beban mudah tinggal menarik dari jenis latihan yang telah disediakan. S&C setting ini terdiri dari:</p><p>1. Jenis Latihan</p><p>2. Join Action / sendi yang aktif</p><p>3. TUT (Time Under Tention) mengacu pada jumlah otot ditahan dibawah ketegangan atau ketegangan selama satu set latihan. memperpanjang setiap fase gerakan untuk membuat set lebih lama, tujuannya adalah untuk memaksa otot anda bekerja lebih keras dan mengoptimalkan kekuatan otot, daya tahan otot dan pertumbuhan.</p><p>4. Muscle group adalah kelompok otot/ group otot dan ada 3 jenis otot dalam tubuh yaitu:</p><p> a. Otot Jantung adalah otot yang mengontrol jantung.</p><p> b. Otot Polos adalah otot yang mengontrol fungsi tidak sadar seperti penyempitan pembuluh darah.</p><p> c. Otot Rangka adalah otot yang ditargetkan melalui latihan untuk membantu tubuh bergerak.</p><p> Mereka membentuk sekitar 40% dari berat badan.</p><p>5. Para ahli biasanya meyebut ini sebagai Kelompok otot utama dalam tubuh seperti</p><p> a, Otot Dada / Chest</p><p> b. Otot bagian belakang/Back s</p><p> c. Otot Lengan/ Arms</p><p> d. Otot Perut / Abdominals</p><p> e. Otot Kaki / Legs</p><p> f. Otot bahu /Shoulders</p><p><span style="color: rgb(255, 0, 0);">6. alat Dumble atau Olympic bar</span></p><p><span style="color: rgb(255, 0, 0);">7. Kolom </span><span style="color: rgb(255, 0, 0);"><strong>comments</strong></span><span style="color: rgb(255, 0, 0);"> diganti </span><span style="color: rgb(255, 0, 0);"><strong>type of exercise</strong></span><span style="color: rgb(255, 0, 0);"> </span></p><p><span style="color: rgb(255, 0, 0);">8. Kolom Weight ditambah satuan: </span><span style="color: rgb(255, 0, 0);"><strong>weight (kg)</strong></span></p><p><span style="color: rgb(255, 0, 0);">9</span><span style="color: rgb(255, 0, 0);"><strong>. </strong></span><span style="color: rgb(255, 0, 0);">di edit data ditambahkan beberapa row untuk isian beberapa type of exercise</span></p><p><span style="color: rgb(255, 0, 0);">10. untuk tanggal/date tidak auto tanggal, </span><span style="color: rgb(255, 0, 0);"><strong>tergantung tanggal latihan</strong></span><span style="color: rgb(255, 0, 0);"> </span><span style="color: rgb(255, 0, 0);"><strong>bebannya</strong></span><span style="color: rgb(255, 0, 0);">, frekuensi latihannya bisa 1 kali atau 2 kali atau 3 kali. semuanya tergantung program pelatih</span></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p><p><br></p>',
                'created_at' => '2022-10-28 16:34:00',
                'updated_at' => '2022-11-12 04:52:24',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            34 =>
            array(
                'key' => 'KPI_BEEP_TEST_ID',
                'value' => $kpiPar4->id,
                'created_at' => '2022-10-29 20:30:28',
                'updated_at' => '2022-10-29 20:30:28',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            35 =>
            array(
                'key' => 'KPI_VO2MAX_ID',
                'value' => $kpiPar5->id,
                'created_at' => '2022-10-29 20:30:28',
                'updated_at' => '2022-10-29 20:30:28',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            36 =>
            array(
                'key' => 'HELP_FITNESS_TOOL',
                'value' => '<table style="width: auto;"><tbody><tr><th colSpan="1" rowSpan="1" width="auto">Field</th><th colSpan="1" rowSpan="1" width="auto">Formula</th></tr><tr><td colSpan="1" rowSpan="1" width="auto">Eccentric Utilisation</td><td colSpan="1" rowSpan="1" width="auto">Countermovement Jump Elevation / Squat Jump Elevation</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Leg Strength Difference </td><td colSpan="1" rowSpan="1" width="auto">((Right Leg Elevation - Left Leg Elevation) / Left Leg Elevation) * 100</td></tr></tbody></table><p><br></p><p><br></p><p><br></p>',
                'created_at' => '2022-10-29 20:34:28',
                'updated_at' => '2022-11-02 04:53:47',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            37 =>
            array(
                'key' => 'KPI_COURT_AGILITY_1_ID',
                'value' => $kpiPar6->id,
                'created_at' => '2022-10-30 10:15:16',
                'updated_at' => '2022-10-30 10:15:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            38 =>
            array(
                'key' => 'KPI_COURT_AGILITY_2_ID',
                'value' => $kpiPar7->id,
                'created_at' => '2022-10-30 10:15:16',
                'updated_at' => '2022-10-30 10:15:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            39 =>
            array(
                'key' => 'KPI_COURT_AGILITY_3_ID',
                'value' => $kpiPar8->id,
                'created_at' => '2022-10-30 10:15:16',
                'updated_at' => '2022-10-30 10:15:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            40 =>
            array(
                'key' => 'KPI_BENCH_PRESS_ID',
                'value' => $kpiPar10->id,
                'created_at' => '2022-10-30 10:15:16',
                'updated_at' => '2022-10-30 10:15:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            41 =>
            array(
                'key' => 'KPI_SQUAT_ID',
                'value' => $kpiPar9->id,
                'created_at' => '2022-10-30 10:15:16',
                'updated_at' => '2022-10-30 10:15:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            42 =>
            array(
                'key' => 'KPI_AGE_ID',
                'value' => $kpiPar1->id,
                'created_at' => '2022-11-04 15:45:13',
                'updated_at' => '2022-11-04 15:45:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            43 =>
            array(
                'key' => 'KPI_HEIGHT_ID',
                'value' => $kpiPar2->id,
                'created_at' => '2022-11-04 15:45:13',
                'updated_at' => '2022-11-04 15:45:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            44 =>
            array(
                'key' => 'KPI_WEIGHT_ID',
                'value' => $kpiPar3->id,
                'created_at' => '2022-11-04 15:45:13',
                'updated_at' => '2022-11-04 15:45:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
