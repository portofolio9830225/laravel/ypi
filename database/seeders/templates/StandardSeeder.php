<?php

namespace Database\Seeders\Templates;

use App\Models\TabelKelompok;
use Database\Seeders\CompetitionStatusesTableSeeder;
use Database\Seeders\ConstantsTableSeeder;
use Database\Seeders\ExerciseCategoriesTableSeeder;
use Database\Seeders\GroupActivitiesTableSeeder;
use Database\Seeders\GymTypesTableSeeder;
use Database\Seeders\KpiParametersTableSeeder;
use Database\Seeders\LevelKategorisTableSeeder;
use Database\Seeders\NationalHeightAvgsTableSeeder;
use Database\Seeders\ResultCategoriesTableSeeder;
use Database\Seeders\SeasonPlanDetailsTableSeeder;
use Database\Seeders\SeasonPlansTableSeeder;
use Database\Seeders\SubExercisesTableSeeder;
use Database\Seeders\TabelKategorisTableSeeder;
use Database\Seeders\TabelKelompoksTableSeeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;

class StandardSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run($tenantId)
    {
        $groupSeeder = new TabelKelompoksTableSeeder();
        $groupSeeder->run($tenantId);

        $levelSeeder = new LevelKategorisTableSeeder();
        $levelSeeder->run($tenantId);

        $kategoriSeeder = new TabelKategorisTableSeeder();
        $kategoriSeeder->run($tenantId);

        $compStatusSeeder = new CompetitionStatusesTableSeeder();
        $compStatusSeeder->run($tenantId);

        $heightSeeder = new NationalHeightAvgsTableSeeder();
        $heightSeeder->run($tenantId);

        $coachGroupSeeder = new GroupActivitiesTableSeeder();
        $coachGroupSeeder->run($tenantId);

        $spSeeder = new SeasonPlansTableSeeder();
        $spSeeder->run($tenantId);

        $spdSeeder = new SeasonPlanDetailsTableSeeder();
        $spdSeeder->run($tenantId);

        $resultSeeder = new ResultCategoriesTableSeeder();
        $resultSeeder->run($tenantId);

        $kpiParamSeeder = new KpiParametersTableSeeder();
        $kpiParamSeeder->run($tenantId);

        $gymSeeder = new GymTypesTableSeeder();
        $gymSeeder->run($tenantId);

        $exerciseSeeder = new ExerciseCategoriesTableSeeder();
        $exerciseSeeder->run($tenantId);

        $subExerciseSeeder = new SubExercisesTableSeeder();
        $subExerciseSeeder->run($tenantId);

        $constantSeeder = new ConstantsTableSeeder();
        $constantSeeder->run($tenantId);
    }
}
