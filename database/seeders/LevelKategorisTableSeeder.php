<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class LevelKategorisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('level_kategoris')->insert(array(
            0 =>
            array(
                'level' => 1,
                'created_at' => '2022-01-30 23:23:22',
                'updated_at' => '2022-01-30 23:23:22',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'level' => 2,
                'created_at' => '2022-01-30 23:23:22',
                'updated_at' => '2022-01-30 23:23:22',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'level' => 3,
                'created_at' => '2022-01-30 23:23:22',
                'updated_at' => '2022-01-30 23:23:22',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'level' => 4,
                'created_at' => '2022-02-05 10:36:42',
                'updated_at' => '2022-02-05 10:36:42',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'level' => 5,
                'created_at' => '2022-02-05 10:36:47',
                'updated_at' => '2022-02-05 10:36:47',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'level' => 6,
                'created_at' => '2022-02-05 10:36:52',
                'updated_at' => '2022-02-05 10:36:52',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'level' => 7,
                'created_at' => '2022-02-05 10:36:57',
                'updated_at' => '2022-02-05 10:36:57',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'level' => 8,
                'created_at' => '2022-02-05 10:37:01',
                'updated_at' => '2022-02-05 10:37:01',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'level' => 9,
                'created_at' => '2022-02-05 10:37:06',
                'updated_at' => '2022-02-05 10:37:06',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'level' => 10,
                'created_at' => '2022-02-05 10:37:11',
                'updated_at' => '2022-02-05 10:37:11',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'level' => 11,
                'created_at' => '2022-07-30 09:24:14',
                'updated_at' => '2022-07-30 09:24:14',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
