<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class KpiParametersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('kpi_parameters')->insert(array(
            0 =>
            array(
                'name' => 'Age',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:08:28',
                'updated_at' => '2022-11-06 07:40:21',
                'deleted_at' => NULL,
                'column_order' => 1,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'name' => 'Height (cm)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:08:44',
                'updated_at' => '2022-09-02 13:08:44',
                'deleted_at' => NULL,
                'column_order' => 2,
                'calculate_statistic' => 0,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'name' => 'Weight (cm)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:08:50',
                'updated_at' => '2022-09-02 13:08:50',
                'deleted_at' => NULL,
                'column_order' => 3,
                'calculate_statistic' => 0,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'name' => 'Beep Test (level, shuttle)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:09:55',
                'updated_at' => '2022-09-02 13:10:01',
                'deleted_at' => NULL,
                'column_order' => 4,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'name' => 'VO2Max (ml/kg/mn)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:10:16',
                'updated_at' => '2022-09-02 13:10:16',
                'deleted_at' => NULL,
                'column_order' => 5,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'name' => 'Court Agility Clockwise (sec)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:10:37',
                'updated_at' => '2022-11-11 15:54:34',
                'deleted_at' => NULL,
                'column_order' => 6,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'name' => 'Court Agility Anti Clockwise (sec)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:11:43',
                'updated_at' => '2022-11-11 15:54:25',
                'deleted_at' => NULL,
                'column_order' => 7,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'name' => 'Court Agility Random (sec)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:11:55',
                'updated_at' => '2022-11-11 15:54:15',
                'deleted_at' => NULL,
                'column_order' => 8,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'name' => 'Squat (kg)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:12:21',
                'updated_at' => '2022-10-30 10:20:15',
                'deleted_at' => NULL,
                'column_order' => 9,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'name' => 'Bench Press (kg)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:12:37',
                'updated_at' => '2022-09-02 13:12:37',
                'deleted_at' => NULL,
                'column_order' => 10,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'name' => 'Vertical Jump (cm)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:13:12',
                'updated_at' => '2022-09-02 13:13:12',
                'deleted_at' => NULL,
                'column_order' => 11,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'name' => 'Skipping Rope (per minute)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:13:39',
                'updated_at' => '2022-09-02 13:13:39',
                'deleted_at' => NULL,
                'column_order' => 12,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'name' => 'Run 20 meter (Second)',
                'input_type' => 'number',
                'created_at' => '2022-09-02 13:13:52',
                'updated_at' => '2022-09-02 13:13:52',
                'deleted_at' => NULL,
                'column_order' => 13,
                'calculate_statistic' => 1,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
