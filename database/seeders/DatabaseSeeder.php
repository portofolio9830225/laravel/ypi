<?php

namespace Database\Seeders;

use App\Models\Club;
use App\Models\RoleClubMember;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $club = Club::create([
            'nama' => 'Bypro - YPI Online',
            'alamat' => 'Semarang - Indonesia',
        ]);

        User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('password'),
            'role' => 'superadmin',
            'active_club_id' => $club->id
        ]);
    }
}
