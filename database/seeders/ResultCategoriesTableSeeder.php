<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ResultCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('result_categories')->insert(array(
            0 =>
            array(
                'result' => 'Qualifying round',
                'created_at' => '2022-10-02 09:37:47',
                'updated_at' => '2022-10-02 09:37:47',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'result' => 'Top 32',
                'created_at' => '2022-10-02 09:37:54',
                'updated_at' => '2022-10-02 09:37:54',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'result' => 'Top 16',
                'created_at' => '2022-10-02 09:38:04',
                'updated_at' => '2022-10-02 09:38:04',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'result' => 'Quarter final',
                'created_at' => '2022-10-02 09:38:19',
                'updated_at' => '2022-10-02 09:38:19',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'result' => 'Semi-Final',
                'created_at' => '2022-10-02 09:38:37',
                'updated_at' => '2022-10-02 09:38:37',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'result' => 'Runner Up',
                'created_at' => '2022-10-02 09:38:46',
                'updated_at' => '2022-10-02 09:38:46',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'result' => 'Champion',
                'created_at' => '2022-10-02 09:38:54',
                'updated_at' => '2022-11-20 09:05:05',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
