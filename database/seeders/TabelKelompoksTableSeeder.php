<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TabelKelompoksTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('tabel_kelompoks')->insert(array(
            1 =>
            array(
                'kelompok' => 'U-9 putra',
                'created_at' => '2021-01-27 03:29:17',
                'updated_at' => '2022-08-22 05:34:02',
                'deleted_at' => NULL,
                'age_min' => 5,
                'age_max' => 8,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'kelompok' => 'U-11 putra',
                'created_at' => '1999-12-28 08:35:03',
                'updated_at' => '2022-08-22 05:34:56',
                'deleted_at' => NULL,
                'age_min' => 9,
                'age_max' => 10,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'kelompok' => 'U-13 Putra',
                'created_at' => '2022-02-05 10:29:56',
                'updated_at' => '2022-08-22 05:35:55',
                'deleted_at' => NULL,
                'age_min' => 11,
                'age_max' => 12,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'kelompok' => 'U-15 Putra',
                'created_at' => '2022-02-05 10:30:06',
                'updated_at' => '2022-08-22 05:36:39',
                'deleted_at' => NULL,
                'age_min' => 13,
                'age_max' => 14,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'kelompok' => 'U-17 Putra',
                'created_at' => '2022-02-05 10:30:17',
                'updated_at' => '2022-08-22 05:37:23',
                'deleted_at' => NULL,
                'age_min' => 15,
                'age_max' => 16,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'kelompok' => 'U-19 Putra',
                'created_at' => '2022-02-05 10:31:07',
                'updated_at' => '2022-08-22 05:38:09',
                'deleted_at' => NULL,
                'age_min' => 17,
                'age_max' => 18,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'kelompok' => 'DEWASA Putra',
                'created_at' => '2022-02-05 10:31:20',
                'updated_at' => '2022-08-22 05:38:48',
                'deleted_at' => NULL,
                'age_min' => 19,
                'age_max' => 40,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'kelompok' => 'MASTER',
                'created_at' => '2022-02-05 10:31:28',
                'updated_at' => '2022-09-29 08:56:48',
                'deleted_at' => NULL,
                'age_min' => 40,
                'age_max' => 75,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'kelompok' => 'U-9 Putri',
                'created_at' => '2022-08-22 05:34:31',
                'updated_at' => '2022-08-22 05:34:31',
                'deleted_at' => NULL,
                'age_min' => 5,
                'age_max' => 8,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'kelompok' => 'U-11 Putri',
                'created_at' => '2022-08-22 05:35:20',
                'updated_at' => '2022-08-22 05:35:20',
                'deleted_at' => NULL,
                'age_min' => 9,
                'age_max' => 10,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'kelompok' => 'U-13 Putri',
                'created_at' => '2022-08-22 05:36:17',
                'updated_at' => '2022-08-22 05:36:17',
                'deleted_at' => NULL,
                'age_min' => 11,
                'age_max' => 12,
                'tenant_id' => $tenantId,
            ),
            13 =>
            array(
                'kelompok' => 'U-15 Putri',
                'created_at' => '2022-08-22 05:37:00',
                'updated_at' => '2022-08-22 05:37:00',
                'deleted_at' => NULL,
                'age_min' => 13,
                'age_max' => 14,
                'tenant_id' => $tenantId,
            ),
            14 =>
            array(
                'kelompok' => 'U-17 Putri',
                'created_at' => '2022-08-22 05:37:44',
                'updated_at' => '2022-08-22 05:37:44',
                'deleted_at' => NULL,
                'age_min' => 15,
                'age_max' => 16,
                'tenant_id' => $tenantId,
            ),
            15 =>
            array(
                'kelompok' => 'U-19 Putri',
                'created_at' => '2022-08-22 05:38:25',
                'updated_at' => '2022-08-22 05:38:25',
                'deleted_at' => NULL,
                'age_min' => 17,
                'age_max' => 18,
                'tenant_id' => $tenantId,
            ),
            16 =>
            array(
                'kelompok' => 'Dewasa Putri',
                'created_at' => '2022-08-22 05:39:02',
                'updated_at' => '2022-08-22 05:39:02',
                'deleted_at' => NULL,
                'age_min' => 19,
                'age_max' => 40,
                'tenant_id' => $tenantId,
            ),
            17 =>
            array(
                'kelompok' => 'Master Putra',
                'created_at' => '2022-08-22 05:39:19',
                'updated_at' => '2022-08-22 05:39:19',
                'deleted_at' => NULL,
                'age_min' => 41,
                'age_max' => 70,
                'tenant_id' => $tenantId,
            ),
            18 =>
            array(
                'kelompok' => 'Master Putri',
                'created_at' => '2022-08-22 05:39:32',
                'updated_at' => '2022-08-22 05:39:32',
                'deleted_at' => NULL,
                'age_min' => 41,
                'age_max' => 70,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
