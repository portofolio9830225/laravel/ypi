<?php

namespace Database\Seeders;

use App\Models\LevelKategori;
use Illuminate\Database\Seeder;

class TabelKategorisTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        $level1 = LevelKategori::where('level', 1)->first();
        $level2 = LevelKategori::where('level', 2)->first();
        $level3 = LevelKategori::where('level', 3)->first();
        $level4 = LevelKategori::where('level', 4)->first();
        $level5 = LevelKategori::where('level', 5)->first();
        $level6 = LevelKategori::where('level', 6)->first();
        $level7 = LevelKategori::where('level', 7)->first();
        $level8 = LevelKategori::where('level', 8)->first();

        \DB::table('tabel_kategoris')->insert(array(
            2 =>
            array(
                'nama_kategori' => 'Olympic Games',
                'level_kategori_id' => $level1->id,
                'created_at' => '2022-02-05 10:17:37',
                'updated_at' => '2022-02-05 10:17:37',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'nama_kategori' => 'Thomas Cup',
                'level_kategori_id' => $level1->id,
                'created_at' => '2022-02-05 10:17:44',
                'updated_at' => '2022-02-05 10:17:44',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'nama_kategori' => 'Uber Cup',
                'level_kategori_id' => $level1->id,
                'created_at' => '2022-02-05 10:17:52',
                'updated_at' => '2022-02-05 10:17:52',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'nama_kategori' => 'World Championship',
                'level_kategori_id' => $level1->id,
                'created_at' => '2022-02-05 10:18:26',
                'updated_at' => '2022-02-05 10:18:26',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'nama_kategori' => 'World Junior Championship',
                'level_kategori_id' => $level1->id,
                'created_at' => '2022-02-05 10:18:40',
                'updated_at' => '2022-02-05 10:18:40',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'nama_kategori' => 'Super Series 1000',
                'level_kategori_id' => $level2->id,
                'created_at' => '2022-02-05 10:19:50',
                'updated_at' => '2022-02-05 10:19:50',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'nama_kategori' => 'Super Series 750',
                'level_kategori_id' => $level2->id,
                'created_at' => '2022-02-05 10:20:16',
                'updated_at' => '2022-02-05 10:20:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'nama_kategori' => 'Super Series 500',
                'level_kategori_id' => $level3->id,
                'created_at' => '2022-02-05 10:20:34',
                'updated_at' => '2022-02-05 10:20:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'nama_kategori' => 'Super Series 300',
                'level_kategori_id' => $level3->id,
                'created_at' => '2022-02-05 10:20:58',
                'updated_at' => '2022-02-05 10:20:58',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'nama_kategori' => 'Super Series 100',
                'level_kategori_id' => $level4->id,
                'created_at' => '2022-02-05 10:21:36',
                'updated_at' => '2022-02-05 10:41:33',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'nama_kategori' => 'International Challange',
                'level_kategori_id' => $level4->id,
                'created_at' => '2022-02-05 10:41:19',
                'updated_at' => '2022-02-05 10:41:19',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            13 =>
            array(
                'nama_kategori' => 'International Series',
                'level_kategori_id' => $level4->id,
                'created_at' => '2022-02-05 10:41:52',
                'updated_at' => '2022-02-05 10:41:52',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            14 =>
            array(
                'nama_kategori' => 'Future series',
                'level_kategori_id' => $level4->id,
                'created_at' => '2022-02-05 10:42:16',
                'updated_at' => '2022-02-05 10:42:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            15 =>
            array(
                'nama_kategori' => 'Youth International Age Grou[',
                'level_kategori_id' => $level5->id,
                'created_at' => '2022-02-05 10:43:21',
                'updated_at' => '2022-02-05 10:43:21',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            16 =>
            array(
                'nama_kategori' => 'National Open',
                'level_kategori_id' => $level5->id,
                'created_at' => '2022-02-05 10:43:34',
                'updated_at' => '2022-02-05 10:43:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            17 =>
            array(
                'nama_kategori' => 'National Circuit',
                'level_kategori_id' => $level6->id,
                'created_at' => '2022-02-05 10:44:37',
                'updated_at' => '2022-02-05 10:44:37',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            18 =>
            array(
                'nama_kategori' => 'Interschool Competition',
                'level_kategori_id' => $level7->id,
                'created_at' => '2022-02-05 10:45:31',
                'updated_at' => '2022-02-05 10:45:31',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            19 =>
            array(
                'nama_kategori' => 'National Private Championship',
                'level_kategori_id' => $level7->id,
                'created_at' => '2022-02-05 10:47:01',
                'updated_at' => '2022-02-05 10:47:01',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            20 =>
            array(
                'nama_kategori' => 'Provinsi',
                'level_kategori_id' => $level7->id,
                'created_at' => '2022-02-06 05:22:45',
                'updated_at' => '2022-08-31 10:02:48',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            21 =>
            array(
                'nama_kategori' => 'Kabupten / kotamadya',
                'level_kategori_id' => $level8->id,
                'created_at' => '2022-02-06 05:23:28',
                'updated_at' => '2022-08-31 10:03:27',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
