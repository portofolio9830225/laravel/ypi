<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class CompetitionStatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('competition_statuses')->insert(array(
            0 =>
            array(
                'status' => 'PRIMARY TOURNAMENT',
                'created_at' => '2021-07-08 02:58:26',
                'updated_at' => '2022-02-05 10:52:25',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'status' => 'SECONDARY TOURNAMENT',
                'created_at' => '1983-10-20 16:41:09',
                'updated_at' => '2022-02-05 10:52:39',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'status' => 'GAIN EXPERIENCES',
                'created_at' => '2017-09-11 14:30:57',
                'updated_at' => '2022-02-05 10:56:51',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'status' => 'GET FOR ATHLETE LEVEL',
                'created_at' => '2022-02-05 10:57:00',
                'updated_at' => '2022-03-20 10:13:48',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'status' => 'EVALUASI TEHNIK DASAR',
                'created_at' => '2022-06-27 13:42:47',
                'updated_at' => '2022-06-27 13:42:47',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
