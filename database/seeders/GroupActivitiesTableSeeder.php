<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GroupActivitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('group_activities')->insert(array(
            0 =>
            array(
                'group' => 'KEJUARAAN INTERNASIONAL',
                'created_at' => '2022-02-12 18:40:42',
                'updated_at' => '2022-02-13 09:58:49',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'group' => 'KEJUARAN NASIONAL',
                'created_at' => '2022-02-12 18:40:58',
                'updated_at' => '2022-02-12 18:44:18',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'group' => 'KEJUARAAN TINGKAT PROVINSI',
                'created_at' => '2022-02-12 18:41:24',
                'updated_at' => '2022-02-12 18:44:36',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'group' => 'KEJUARAAN TINGKAT KAB/KOTA',
                'created_at' => '2022-02-12 18:44:56',
                'updated_at' => '2022-02-12 18:44:56',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'group' => 'TRAINING PROGRAM',
                'created_at' => '2022-02-12 18:45:05',
                'updated_at' => '2022-02-12 18:45:05',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'group' => 'AKTIFITAS SEKOLAH',
                'created_at' => '2022-02-12 18:45:21',
                'updated_at' => '2022-02-12 18:45:21',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'group' => 'HARI LIBUR',
                'created_at' => '2022-02-12 18:45:40',
                'updated_at' => '2022-11-19 16:28:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
