<?php

namespace Database\Seeders;

use App\Models\Constant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ConstantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Constant::firstOrCreate(['key' => 'FIXED_SEASON_PLANS_ID'], ['key' => 'FIXED_SEASON_PLANS_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'FIXED_SEASON_PLAN_DETAILS_ID'], ['key' => 'FIXED_SEASON_PLAN_DETAILS_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'DEFAULT_SEASON_PLANS'], ['key' => 'DEFAULT_SEASON_PLANS', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_ACADEMY_INDEX_ID'], ['key' => 'SPD_ACADEMY_INDEX_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_SPORT_INDEX_ID'], ['key' => 'SPD_SPORT_INDEX_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_INTENSITY_ID'], ['key' => 'SPD_INTENSITY_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_VOLUME_ID'], ['key' => 'SPD_VOLUME_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_SPESIFIC_ID'], ['key' => 'SPD_SPESIFIC_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_STUDY_BALANCE_ID'], ['key' => 'SP_STUDY_BALANCE_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_TARGET_ID'], ['key' => 'SPD_TARGET_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_RATE_IMPORTANT_ID'], ['key' => 'SPD_RATE_IMPORTANT_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_DATE_FITNESS_ID'], ['key' => 'SPD_DATE_FITNESS_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_PUBLIC_HOLIDAY_ID'], ['key' => 'SPD_PUBLIC_HOLIDAY_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_TRAINING_PHASE_ID'], ['key' => 'SPD_TRAINING_PHASE_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_SUB_PHASE_ID'], ['key' => 'SPD_SUB_PHASE_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_WORK_TIME_ID'], ['key' => 'SP_WORK_TIME_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'YTP_DATE_COLOR'], ['key' => 'YTP_DATE_COLOR', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_TECH_DEV_ID'], ['key' => 'SP_TECH_DEV_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_PHYSICAL_DEV_ID'], ['key' => 'SP_PHYSICAL_DEV_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_TACTIC_ID'], ['key' => 'SP_TACTIC_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_MENTAL_ID'], ['key' => 'SP_MENTAL_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SP_PERIODIZATION_ID'], ['key' => 'SP_PERIODIZATION_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_NO_HOUR_WEEK_ID'], ['key' => 'SPD_NO_HOUR_WEEK_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'SPD_TOTAL_HOUR_WEEK_ID'], ['key' => 'SPD_TOTAL_HOUR_WEEK_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'PARAMETER_KPI_COMPARISON'], ['key' => 'PARAMETER_KPI_COMPARISON', 'value' => '']);
        Constant::firstOrCreate(['key' => 'DATA_VO2MAX_CA_COMPARISON'], ['key' => 'DATA_VO2MAX_CA_COMPARISON', 'value' => '{"vo2max_parameter_id":5,"ca_min_custom_parameter_id":2}']);
        Constant::firstOrCreate(['key' => 'DATA_BENCHPRESS_SQUAD_COMPARISON'], ['key' => 'DATA_BENCHPRESS_SQUAD_COMPARISON', 'value' => '{"benchpress_parameter_id":10,"squat_parameter_id":9}']);
        Constant::firstOrCreate(['key' => 'KPI_BEEP_TEST_ID'], ['key' => 'KPI_BEEP_TEST_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_VO2MAX_ID'], ['key' => 'KPI_VO2MAX_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_COURT_AGILITY_1_ID'], ['key' => 'KPI_COURT_AGILITY_1_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_COURT_AGILITY_2_ID'], ['key' => 'KPI_COURT_AGILITY_2_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_COURT_AGILITY_3_ID'], ['key' => 'KPI_COURT_AGILITY_3_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_BENCH_PRESS_ID'], ['key' => 'KPI_BENCH_PRESS_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_SQUAT_ID'], ['key' => 'KPI_SQUAT_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_AGE_ID'], ['key' => 'KPI_AGE_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_HEIGHT_ID'], ['key' => 'KPI_HEIGHT_ID', 'value' => '']);
        Constant::firstOrCreate(['key' => 'KPI_WEIGHT_ID'], ['key' => 'KPI_WEIGHT_ID', 'value' => '']);
    }
}
