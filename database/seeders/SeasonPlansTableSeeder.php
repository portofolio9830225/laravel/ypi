<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SeasonPlansTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('season_plans')->insert(array(
            0 =>
            array(
                'name' => 'DETAIL AND SPORTS & STUDY BALANCE',
                'description' => 'DETAIL AND SPORTS & STUDY BALANCE',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'name' => 'PERIODIZATION',
                'description' => 'PERIODIZATION',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'name' => 'TECHNICAL DEVELOPMENT',
                'description' => 'TECHNICAL DEVELOPMENT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'name' => 'TACTICAL AND STRATEGY',
                'description' => 'TACTICAL AND STRATEGY',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'name' => 'PHYSICAL DEVELOPMENT',
                'description' => 'PHYSICAL DEVELOPMENT',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'name' => 'MENTAL SKILLS',
                'description' => 'MENTAL SKILLS',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'name' => 'PROGRESS',
                'description' => 'PROGRESS',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'name' => '% WORKOUT & TIME',
                'description' => '% WORKOUT & TIME',
                'created_at' => NULL,
                'updated_at' => NULL,
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
