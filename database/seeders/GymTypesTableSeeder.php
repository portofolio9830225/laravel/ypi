<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GymTypesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('gym_types')->insert(array(
            0 =>
            array(
                'gym_description' => 'Flexion and extension',
                'created_at' => '2022-03-05 16:47:45',
                'updated_at' => '2022-06-09 21:06:50',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'gym_description' => 'Pronation and supination ',
                'created_at' => '2022-03-05 16:48:06',
                'updated_at' => '2022-03-05 16:48:06',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'gym_description' => 'Abduction and adduction ',
                'created_at' => '2022-03-05 16:48:20',
                'updated_at' => '2022-03-05 16:48:20',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'gym_description' => 'Circumductions ',
                'created_at' => '2022-03-05 16:48:34',
                'updated_at' => '2022-03-05 16:48:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'gym_description' => 'Inversion and eversions ',
                'created_at' => '2022-03-05 16:49:15',
                'updated_at' => '2022-03-05 16:49:15',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'gym_description' => 'Internal and external Rotations  ',
                'created_at' => '2022-03-05 16:49:39',
                'updated_at' => '2022-03-05 16:49:39',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'gym_description' => 'Elevation and depression',
                'created_at' => '2022-03-05 16:49:51',
                'updated_at' => '2022-03-05 16:49:51',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'gym_description' => 'Upper body',
                'created_at' => '2022-10-25 09:05:43',
                'updated_at' => '2022-11-11 10:04:50',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'gym_description' => 'Lower body',
                'created_at' => '2022-11-11 10:04:58',
                'updated_at' => '2022-11-11 10:04:58',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
