<?php

namespace Database\Seeders;

use App\Models\SeasonPlan;
use Illuminate\Database\Seeder;

class SeasonPlanDetailsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        $sportStudy = SeasonPlan::where('name', 'DETAIL AND SPORTS & STUDY BALANCE')->first();
        $periodization = SeasonPlan::where('name', 'PERIODIZATION')->first();
        $techDev = SeasonPlan::where('name', 'TECHNICAL DEVELOPMENT')->first();
        $tactical = SeasonPlan::where('name', 'TACTICAL AND STRATEGY')->first();
        $physical = SeasonPlan::where('name', 'PHYSICAL DEVELOPMENT')->first();
        $mental = SeasonPlan::where('name', 'MENTAL SKILLS')->first();
        $progress = SeasonPlan::where('name', 'PROGRESS')->first();
        $workout = SeasonPlan::where('name', '% WORKOUT & TIME')->first();

        \DB::table('season_plan_details')->insert(array(
            0 =>
            array(
                'season_plan_id' => $sportStudy->id,
                'name' => 'Target',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:01:38',
                'updated_at' => '2022-08-12 23:01:38',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'season_plan_id' => $sportStudy->id,
                'name' => 'Rate Important of Competition',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:01:50',
                'updated_at' => '2022-08-12 23:01:50',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'season_plan_id' => $sportStudy->id,
                'name' => 'Date for Fitness Test',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:01:59',
                'updated_at' => '2022-08-12 23:01:59',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'season_plan_id' => $sportStudy->id,
                'name' => 'Academy Index (1-5)',
                'input_type' => 'number',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:02:14',
                'updated_at' => '2022-08-12 23:02:14',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'season_plan_id' => $sportStudy->id,
                'name' => 'Sport Index (1-5)',
                'input_type' => 'number',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:02:27',
                'updated_at' => '2022-08-12 23:02:27',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'season_plan_id' => $sportStudy->id,
                'name' => 'Public Holiday',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:02:40',
                'updated_at' => '2022-08-12 23:02:40',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'season_plan_id' => $periodization->id,
                'name' => 'Training Phase',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p><strong>Rencana tahunan memiliki tiga fase pelatihan: Persiapan, Kompetitif dan transisi.</strong></p><p><strong>Tujuan dan karakteristik </strong>dari operan-operan ini tetap sama apakah Anda melakukannya sekali atau mengulanginya beberapa kali, seperti dalam dua siklus atau tiga siklus. Penting untuk keberhasilan atlet bahwa Anda mengikuti durasi, urutan, karakteristik, dan penekanan yang disarankan pada setiap fase pelatihan yang disarankan. ini memastikan bahwa bentuk atletik tertinggi tercapai untuk kompetisi yang direncanakan.</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <em> &nbsp; Tudor O.Bompa, PhD</em></p>',
                'created_at' => '2022-08-12 23:03:51',
                'updated_at' => '2022-11-10 14:23:54',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'season_plan_id' => $periodization->id,
                'name' => 'Sub Phase',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p><strong>MUSIM: TIGA FASE</strong></p><p>Musim pelatihan biasanya dibagi menjadi tiga fase:</p><p><br></p><p><strong>PRE SEASON</strong>:</p><p>Tujuan utama pramusim adalah mempersiapkan atlet untuk kompetisi mendatang. Persiapan ini meliputi latihan fisik, mental dan pengembangan keahlian. Fase ini terkadang dibagi menjadi dua subfase, fase umum dan fase khusus.</p><p><br></p><p> <strong>SUBPHASE PRE SEASON UMUM</strong>:</p><p>menyediakan dasar untuk inseason. subfase ini menampilkan volume pelatihan yang tinggi, yang menurunkan intensitas.</p><p> </p><p><strong>SUB PHASE PRE SEASON KHUSUS:</strong></p><p>Sub fase ini bertindak sebagai transisi ke kompetisi, dan tujuan utamanya adalah untuk memadukan berbagai jenis pelatihan volume latihan fisik biasanya tetap tinggi, tetapi latihan menjadi lebih spesifik. sebagai pendekatan in season, pekerjaan teknis dan tacktical menjadi lebih seperti yang dibutuhkan dalam kompetisi, dan intensitas lebih dekat ke tingkat persaingan yang akan dihadapi</p><p><br></p><p><strong>THE INSEASON: </strong></p><p>adalah bagian kompetitif musim ini. Tujuan utama dari fase ini adalah untuk menstabilkan kinerja atlet. dalam beberapa kasus, inseason dibagi lagi menjadi subfase pra kompetitif dan kompetitif utama</p><p><br></p><p><strong>SUB PASHE PRECOMPETITIVE:</strong></p><p>Kompetisi kurang penting dibandingkan dengan subfase kompetitif utama. atlet menggunakan kompetisi untuk menyempurnakan pelatihan dan untuk mengetahui seberapa siap mereka sebenarnya. Subfase ini memberi atlet waktu untuk mengambil semua keterampilan dan kemampuan yang dikembangkan di pra musim dan menggabungkan mereka ke dalam kinerja kompetitif. Kinerja mungkin agak tidak konsisten selama periode ini, dan adaptasi dengan situasi kompetitif terus terjadi.</p><p><br></p><p><strong>SUBFASE KOMPETITIF UTAMA:</strong></p><p>Dalam subfase ini, fokusnya adalah pada hasil - tujuannya adalah kinerja maksimal. Intensitas latihan tetap tinggi, dan tekanan persaingan juga tinggi; pengurangan volume latihan untuk mengakomodasi tingkat intensitas ini. Bagian dari musim pelatihan ini juga menampilkan periode istirahat aktif yang singkat. Pada periode ini, fokus utama adalah pada pulih dari stres persaingan sambil tetap aktif. Istirahat seperti itu meningkatkan kemampuan atlet untuk tampil di puncak mereka.</p><p><br></p><p><strong>THE POSTSEASON:</strong></p><p>juga disebut sebagai off season. tujuan dari fase ini adalah untuk menjaga kebugaran dan untuk merehabilitasi cedera yang tersisa. Fokusnya adalah pada aktivitas fisik umum - atlet dapat berpartisipasi dalam kegiatan yang mirip dengan olahraga mereka, tetapi mereka harus menghindari spesialisasi. Intensitas pelatihan rendah; volume mungkin juga rendah - bagaimanapun juga, atlet masih dalam masa pemulihan dari inseason.</p>',
                'created_at' => '2022-08-12 23:03:56',
                'updated_at' => '2022-11-10 14:24:17',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'season_plan_id' => $periodization->id,
                'name' => 'Macro Cycle',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Macrocycles digunakan untuk mengatur pelatihan atlet menuju tujuan pelatihan tertentu. Mereka juga memandu pemilihan volume dan intensitas latihan.</p><p>Durasi siklus makro tergantung pada periode pelatihan yang menjadi bagiannya dan tujuan pelatihan / kinerja yang telah ditetapkan. misalnya, selama periode persiapan, siklus makro dapat berlangsung dari empat hingga enam minggu karena waktu yang diperlukan untuk mencapai tingkat kebugaran dasar yang terkait dengan periode ini.</p><p><br></p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>KARAKTERISTIK MAKROSIKLUS</strong></p><p><strong>JENIS MAKROSIKLUS TUJUAN MAKROSIKLAT PELATIHAN BEBAN FASE ASOSIASI</strong></p><p>Pengkondisian Umum Kualitas Fisik Dasar (dasar) Volume sedang, intensitas rendah Persiapan Umum</p><p><br></p><p>Pengkondisian Dasar Kualitas fisik dasar, volume dasar Sedang hingga tinggi, Persiapan Umum</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; keterampilan psikologis, keterampilan motorik dasar intensitas rendah hingga sedang</p><p><br></p><p>Pengkondisian khusus olahraga Kualitas fisik fungsional (spesifik), Volume sedang, sedang hingga tinggi Persiapan Spesifik</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; keterampilan psikologis tertentu, dasar dan intensitas</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; keterampilan motorik yang lebih kompleks</p><p><br></p><p>Evaluasi awal Taktik individu dan kelompok, prakompetisi Beban optimal (dalam hal Prakompetitif dan utama</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; dan strategi persaingan, fokus dan persaingan) kompetitif</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; aktivasi otomatis</p><p><br></p><p>Kompetisi Mirip dengan siklus makro pra-kompetisi Beban optimal (dalam hal Kompetitif Utama</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;tujuan pelatihan)</p><p><br></p><p>Transisi Keterampilan dan taktik motorik dasar, umum Volume sedang, intensitas rendah Kompetisi utama</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;aktivitas, kualitas fisik dasar</p>',
                'created_at' => '2022-08-12 23:04:14',
                'updated_at' => '2022-11-10 14:24:41',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'season_plan_id' => $periodization->id,
                'name' => 'Messo Cycle',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Macrocycles digunakan untuk mengatur pelatihan atlet menuju tujuan pelatihan tertentu. Mereka juga memandu pemilihan volume dan intensitas latihan.</p><p>Durasi siklus makro tergantung pada periode pelatihan yang menjadi bagiannya dan tujuan pelatihan / kinerja yang telah ditetapkan. misalnya, selama periode persiapan, siklus makro dapat berlangsung dari empat hingga enam minggu karena waktu yang diperlukan untuk mencapai tingkat kebugaran dasar yang terkait dengan periode ini.</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> &nbsp; &nbsp;KARAKTERISTIK MAKROSIKLUS</strong></p><table style="width: auto;"><tbody><tr><th colSpan="1" rowSpan="1" width="184">JENIS SIKLUS MAKRO</th><th colSpan="1" rowSpan="1" width="308">TUJUAN SIKLAS MAKRO </th><th colSpan="1" rowSpan="1" width="254.17"> BEBAN &nbsp;LATIHAN</th><th colSpan="1" rowSpan="1" width="221">FASE ASOSIASI</th></tr><tr><td colSpan="1" rowSpan="1" width="auto">Pengkondisian Umum</td><td colSpan="1" rowSpan="1" width="auto">Kualitas Fisik Dasar (dasar)</td><td colSpan="1" rowSpan="1" width="auto">Volume sedang, intensitas rendah </td><td colSpan="1" rowSpan="1" width="auto">Persiapan Umum</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Pengkondisian Dasa</td><td colSpan="1" rowSpan="1" width="auto">Kualitas fisik dasar,keterampilan psikologis, keterampilan motorik dasar </td><td colSpan="1" rowSpan="1" width="auto">volume dasar Sedang hingga tinggi, intensitas rendah hingga sedang</td><td colSpan="1" rowSpan="1" width="auto">Persiapan Umum</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Pengkondisian khusus olahraga</td><td colSpan="1" rowSpan="1" width="auto"> Kualitas fisik fungsional (spesifik), &nbsp;keterampilan psikologis tertentu, dasar dan intensitas keterampilan motorik yang lebih kompleks<br></td><td colSpan="1" rowSpan="1" width="auto">Volume sedang, sedang hingga tinggi </td><td colSpan="1" rowSpan="1" width="auto">Persiapan Spesifi</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Evaluasi awal</td><td colSpan="1" rowSpan="1" width="auto">Taktik individu dan kelompok, prakompetisi dan strategi persaingan, fokus dan persaingan) kompetitif aktivasi otomatis<br></td><td colSpan="1" rowSpan="1" width="auto">Beban optimal (dalam hal kompetisi) </td><td colSpan="1" rowSpan="1" width="auto">pra-kompetisi dan kompetisi utama</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Kompetisi </td><td colSpan="1" rowSpan="1" width="auto">Mirip dengan siklus makro pra-kompetisi </td><td colSpan="1" rowSpan="1" width="auto">Beban optimal (dalam hal tujuan pelatihan)</td><td colSpan="1" rowSpan="1" width="auto">Kompetitif Utama</td></tr><tr><td colSpan="1" rowSpan="1" width="auto">Transisi </td><td colSpan="1" rowSpan="1" width="auto">Keterampilan dan taktik motorik dasar, aktivitas umum, kualitas fisik dasar</td><td colSpan="1" rowSpan="1" width="auto">Volume sedang, intensitas rendah </td><td colSpan="1" rowSpan="1" width="auto">Kompetisi utama</td></tr></tbody></table><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p><p><br></p>',
                'created_at' => '2022-08-12 23:04:23',
                'updated_at' => '2022-08-12 23:04:23',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'season_plan_id' => $periodization->id,
                'name' => 'Micro Cycle',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Microcycles adalah periode pelatihan yang dirancang untuk memaksimalkan manfaat dari setiap latihan, dan biasanya berlangsung selama satu minggu.</p><p>Desain setiap siklus mikro didasarkan pada serangkaian faktor yang bertujuan untuk mendapatkan hasil maksimal dari setiap atlet. Dengan memantau secara cermat tanda-tanda kelelahan atlet, Anda dapat menyusun serangkaian latihan / sesi latihan yang bervariasi baik beban latihan (volume dan intensitas) dan latihan / latihan yang dipilih.</p>',
                'created_at' => '2022-08-12 23:04:31',
                'updated_at' => '2022-08-12 23:04:31',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'season_plan_id' => $techDev->id,
                'name' => 'Front Area',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p><strong>Pukulan Forehand : </strong>Angkat Lurus dan silang, netting spin, netting silang, dorong lurus & silang, tap lurus dan silang</p><p><strong>Pukulan Backhand</strong> adalah Angkat Lurus dan silang, netting spin, netting silang, dorong lurus & silang, tap lurus dan silang</p><p><strong>Basic Skill Drill</strong> adalah automatisasi semua pukulan dasar yang telah dipelajari, pelatihannya mulai dari jumlah pukulan yang mudah (1 s.d 2 titik) terus meningkat ke sedang (3-4 titik) dan tingkat susah (6 titik)</p><p><strong>Basic skill </strong>adalah pukulan dasar yang benar, pelatihannya mulai dari satu-satu pukulan terus meningkat satu-dua pukulan dan seterusnya </p>',
                'created_at' => '2022-08-12 23:07:02',
                'updated_at' => '2022-09-07 06:00:50',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'season_plan_id' => $techDev->id,
                'name' => 'Middle Area',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p><strong>PUKULAN SAMPING KANAN / FOREHAND LATERAL</strong></p><ol><li><strong>Sisi bawah kanan /forehand lateral</strong>: pengembalian defen stop dan angkat kebelakang, kembalian dengan mengontrol pukulan pendek lurus & silang</li><li><strong>Sisi samping datar kanan / forehand lateral:</strong> kembalian dengan mengontrol pukulan pendek lurus & silang, kembalian pukulan dengan ayunan lurus & silang , kembalian dengan pukulan blok jumping smash lurus atau silang, kembalian dengan pukulan flick lan lob lurus atau silang, pukulan drive lurus atau silang</li></ol><p><strong>PUKULAN SAMPING KIRI / BACKHAND LATERAL </strong>:</p><ol><li><strong>Sisi bawah kiri /backhand lateral</strong>: pengembalian defen stop dan angkat kebelakang, kembalian dengan mengontrol pukulan pendek lurus & silang</li><li><strong>Sisi samping datar kiri / Backhand lateral:</strong> kembalian dengan mengontrol pukulan pendek lurus & silang, kembalian pukulan dengan ayunan lurus & silang , kembalian dengan pukulan blok jumping smash lurus atau silang, kembalian dengan pukulan flick lan lob lurus atau silang, pukulan drive lurus atau silang</li><li><strong>Sisi samping atas/ Overhead lateral:</strong> kembalian dengan mengontrol pukulan pendek lurus & silang, kembalian pukulan dengan ayunan lurus & silang , kembalian dengan pukulan blok jumping smash lurus atau silang, kembalian dengan pukulan flick lan lob lurus atau silang</li></ol>',
                'created_at' => '2022-08-12 23:07:11',
                'updated_at' => '2022-08-19 15:25:35',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            13 =>
            array(
                'season_plan_id' => $techDev->id,
                'name' => 'Baseline Area',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p><strong>Pukulan Forehand </strong>: </p><ol><li>Pukulan Lob tinggi kebelakang &nbsp;lurus atau silang atau lob serang </li><li>Pukulan full dropshot atau setengah aarah lurus atau silang</li><li>Pukulan smash penuh dengan ayunan penuh atau pukulan flick dengan arah lurus atau silang</li><li>Pukulan transisi arah lurus datar atau silang datar</li></ol><p><br></p><p><strong>Pukulan backhand</strong>:</p><ol><li>Pukulan Lob tinggi kebelakang &nbsp;lurus atau silang atau drive lurus atau silang</li><li>Pukulan full dropshot atau setengah arah lurus atau silang</li><li>Pukulan smash penuh dengan ayunan penuh atau pukulan flick dengan arah lurus atau silang</li><li>Pukulan transisi arah lurus datar atau silang datar</li></ol><p><br></p>',
                'created_at' => '2022-08-12 23:07:19',
                'updated_at' => '2022-08-19 15:26:01',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            14 =>
            array(
                'season_plan_id' => $techDev->id,
                'name' => 'Badminton Step',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<ol><li>Gerakan depan dan belakang setengah lapangan</li><li> Kanan dan kiri ke depan (area depan)</li><li> Sisi kanan dan kiri (bagian samping/tengah)</li><li> Sisi belakang kanan dan kiri (area baseline)</li><li> Langkah enam sudut (enam titik)</li><li> Badminton Bayangan / shadow badminton</li></ol>',
                'created_at' => '2022-08-12 23:07:27',
                'updated_at' => '2022-08-19 15:27:23',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            15 =>
            array(
                'season_plan_id' => $tactical->id,
                'name' => 'Offensive',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Reli cepat, mengatur permainan dimulai dari area depan dan ditindak lanjuti.</p><p>Tujuannya adalah mengontrol permainan dan tidak pernah memberikan kesempatan dan perubahan supaya permainan lawan tidak berkembang</p><p><br></p><p>Persiapan adalah kebugaran yang baik, kecepatan tinggi, memiliki keterampilan menyerang yang baik dan selalu mengambil inisiatif dalam permainan</p>',
                'created_at' => '2022-08-12 23:08:06',
                'updated_at' => '2022-08-12 23:08:06',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            16 =>
            array(
                'season_plan_id' => $tactical->id,
                'name' => 'Devensive',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p>sulit untuk dikalahkan/dimatikan, pengembalian permainan dengan pukulan - pukulan yang jauh dari lawan, permainan kesudut lawan dan dalam agar susah diserang.</p><p><strong>Tujuannya</strong> adalah menciptakan reli panjang, reposisi kembali dari tekanan lawan dan mengadu kebugaran</p><p><strong>Persiapan: </strong>waspada, sabar dan tetap fokus</p>',
                'created_at' => '2022-08-12 23:08:14',
                'updated_at' => '2022-08-19 15:29:22',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            17 =>
            array(
                'season_plan_id' => $tactical->id,
                'name' => 'Counter Attack',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>pada permainan reli cepat atau lambat, dimulai dari bertahan dan secara mendadak diikuti dengan serangan yang mematikan</p><p><strong>Tujuannya </strong>adalah membuat lawan tidak waspada dan reaksi cepat untuk membalikkan serangan</p><p><strong>Persiapan</strong> adalah Waspada, antisipasi yang baik dan sabar</p>',
                'created_at' => '2022-08-12 23:08:24',
                'updated_at' => '2022-08-19 15:29:37',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            18 =>
            array(
                'season_plan_id' => $tactical->id,
                'name' => 'Deception',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>adalah pukulan tipuan dengan tehnik pukulan ganda / kedutan dan pukulan cepat / flik</p><p><strong>Tujuannya</strong> agar lawan keluar dari posisinya / merusak posisi lawan dan salah antisipasi/arah</p><p><strong>Persiapan: </strong>Gerakan cepat, pukulan ganda pendek dengan pergelangan tangan yang kuat</p>',
                'created_at' => '2022-08-12 23:08:32',
                'updated_at' => '2022-08-19 15:29:51',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            19 =>
            array(
                'season_plan_id' => $physical->id,
                'name' => 'Aerobic Capacity',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p>Tandai periode di mana Anda berniat untuk fokus pada peningkatan aerobik (misalnya lari jauh tapi intensitas rendah), fokus pada kapasitas aerobik selama pada tahap awal fase persiapan , setelah dasar aerobik Anda kuat dan mapan, Anda dapat mulai fokus untuk meningkatkan kapasitas anaerobik Anda. detail lebih lanjut seperti detak jantung dan durasi latihan harus dicatat dalam log book latihan fisik Anda.</p><p>Latihan aerobik mengacu pada latihan progresif dan teratur yang dilakukan dalam jangka waktu yang lama dan melibatkan sebagian besar otot rangka. </p><p>Latihan aerobik menyebabkan adaptasi dalam sistem transportasi oksigen (jantung dan sistem peredaran darah) dan sistem pemanfaatan oksigen ddalam otot.</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong> ADA DUA JENIS UTAMA PELATIHAN AEROBIK:</strong></p><ol><li><strong>LATIHAN AEROBIK BERKELANJUTAN : </strong>mengacu pada latihan yang dilakukan setidaknya selama 20 menit pada intensitas yang menekankan kedua sistem transportasi oksigen dan sistem pemanfaatan oksigen dalam otot</li><li><strong>PELATIHAN INTERVAL AEROBIK</strong> : mengacu pada serangkaian tekanan yang berulang dari latihan yang berintensitas tinggi yang disisipi oleh periode jeda. &nbsp;jeda ini dapat terdiri dari: &nbsp;istirahat atau olahraga ringan. Dalam latihan interval aerobik, rasio kerja/jeda biasanya 1:1, seluruh periode kerja/jeda antara 30 detik dan 3 menit</li></ol><p>secara umum diterima bahwa kombinasi interval dan pelatihan terus menerus adalah persiapan yang paling efektif untuk atlet.</p><p>Latihan interval menyebabkan adaptasi yang meningkatkan kemampuan otot untuk mengekstrak dan menggunakan oksigen yang tersedia oleh sistem kardiovaskular. meningkatkan kemampuan untuk menggunakan oksigen pada gilirannya tergantung pada perubahan struktural dan metabolisme di dalam otot.</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>MANFAAT PELATIHAN AEROBIK</strong></p><p>Manfaat utama dari latihan aerobik adalah sebagai berikut:</p><ul><li>meningkatkan kapasitas untuk melakukan latihan pada intensitas yang relatif tinggi untuk waktu yang lama ( lebih dari 30 menit ) </li><li>Meningkatkan kemampuan untuk melawan kelelahan.</li><li>Meningkatkan kemampuan untuk melakukan latihan dengan kecepatan tinggi untuk waktu yang singkat (5 hingga 15 menit)</li><li>Meningkatkan kemampuan untuk pulih dengan cepat dari tingkat kerja yang tinggi</li><li>Meningkatkan kemampuan untuk mengeluarkan energi dalam jumlah besar</li></ul><p>Pelatihan aerobik dikaitkan dengan peningkatan kemampuan untuk melakukan pekerjaan yang lama pada intensitas sub maksimal. </p><p>Peningkatan tersebut biasanya dikaitkan dengan peningkatan VO2 max.</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> VARIABEL PROGRAM PELATIHAN</strong></p><p>Berikut adalah variabel kunci yang perlu dipertimbangkan ketika merancang program pelatihan aerobik:</p><p><strong>&gt; INTENSITAS : </strong>upaya fisik yang dilakukan atlet dalam sesi latihan individu</p><p><strong>&gt; DURATION: </strong>jumlah waktu sebenarnya atlet berolahraga dalam satu sesi latihan</p><p><strong>&gt; FREKUENSI:</strong> jumlah sesi pelatihan per minggu</p><p><strong>&gt; TOTAL VOLUME:</strong> hasil kali intensitas, durasi, dan frekuensi</p><p><strong>&gt; PANJANG PROGRAM:</strong> lamanya waktu program pelatihan atau bagian dari program pelatihan berlangsung</p><p><strong>&gt; MODE LATIHAN: </strong>jenis latihan yang dipilih untuk program latihan</p><p><strong>&gt; JENIS LATIHAN:</strong> latihan aerobik berkelanjutan atau latihan aerobik</p>',
                'created_at' => '2022-08-12 23:10:08',
                'updated_at' => '2022-10-25 08:56:21',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            20 =>
            array(
                'season_plan_id' => $physical->id,
                'name' => 'An-Aerobic Capacity',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p>Ketika dasar aerobik Anda sudah kuat dan mapan, Anda dapat mulai meningkatkan kapasitas anaerobik Anda.</p><p>Hindari melakukan latihan interval dua hari berturut-turut. rincian lebih lanjut (seperti detak jantung dan durasi latihan (untuk latihan aerobik) serta tidak ada interval dan tidak ada " sprint " harus dicatat dalam log book latihan fisik Anda.</p><p>Mengidentifikasi sejumlah manfaat anaerobik dari latihan aerobik. Program yang dirancang untuk mengenakan kebutuhan khusus pada komponen tertentu dari sistem anaerobik dapat lebih meningkatkan kinerja anaerobik.</p><p>Sebagai contoh:</p><ul><li>Meningkatkan pergantian ATP meningkatkan daya alaktik anaerobik. Pelatihan intensitas tinggi dapat meningkatkan aktivitas enzim sistem alaktik anaerobik.</li><li>Atlet yang berlatih dan berkompetisi dalam aktivitas intensitas tinggi seringkali dapat mentolerir konsentrasi asam laktat yang lebih tinggi.</li><li>Mampu menyangga asam laktat membantu memperpanjang kinerja intensitas tinggi. Pelatihan intensitas tinggi meningkatkan kemampuan ini untuk menyangga asam laktat.</li><li>Ada hubungan positif antara persentase putaran cepat dan daya dan kapasitas alaktik anaerobik.</li></ul><p><br></p><p>penting untuk dicatat bahwa sejumlah faktor mempengaruhi kinerja anaerobik. misalnya, massa otot yang lebih besar memungkinkan atlet untuk menghasilkan lebih banyak kekuatan dan kapasitas absolut baik dalam sistem alaktik maupun laktat.</p><p><br></p><p><strong>ANAEROBIK ALAKTIK:</strong></p><p><br></p><p><strong>Ada tiga jenis (metode) pelatihan anaerobik alaktik:</strong></p><p><br></p><ol><li>Latihan untuk mengembangkan daya alaktik anaerobik (daya puncak): biasanya latihan ketahanan atau beban untuk kekuatan.</li><li>Pelatihan untuk mempertahankan daya alaktik anaerobik: adalah bentuk pelatihan yang paling umum ketika tujuannya adalah memberikan energi alaktik anaerobik dalam jumlah besar.</li><li>Pelatihan untuk mengembangkan kapasitas anaerobik alaktik.</li></ol><p><br></p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> METODE PELATIHAN UNTUK SISTEM ALAKTIK ANAEROBIK PELATIHAN PEAK POWER VARIABEL PEMELIHARAAN PEAK POWER KAPASITAS </strong></p><p>Tahap Latihan (dtk) 1 - 5 5 - 15 15 - 30</p><p>Fase Jeda (dtk) 2 - 10 25 - 90 90 - 180</p><p>Rasio W:P 1: 2 90 - 100 85 - 90</p><p>Intensitas (% usaha maksimal) 95 - 100 90 - 100 85 - 90</p><p>Pengulangan (No) 1 - 7 5 - 6 2 - 4</p><p>Volume Set Maksimum (Detik) 60 60 60</p><p>Set (Tidak) 5 - 10 5 - 6 2 - 4</p><p>Istirahat antar set (min) 5 - 10 5 - 10 10 - 15</p><p>Rentang Kemajuan untuk latihan Vol (Min) 0 - 1,5 1 - 8 1 - 8</p><p>Perkiraan Waktu Latihan (Min) 25 5 - 60 35 - 45</p><p><br></p><p><strong>LAKTIK ANAEROBIK</strong></p><p><br></p><p>Upaya maksimal yang berlangsung sekitar 30 detik terutama tergantung pada sistem laktat anaerobik - 70% versus 15% untuk sistem alaktik anaerob dan sistem aerobik. pelatihan yang dilakukan dalam 5 sampai 10 detik terakhir dari upaya tersebut mencerminkan kekuatan laktat anaerobik. program pelatihan yang membutuhkan pekerjaan berintensitas tinggi selama 20 hingga 30 detik menekankan komponen ini dan menyebabkan perubahan fisik yang sesuai.</p><p><br></p><p><strong>Ada tiga metode pelatihan laktat anaerobik:</strong></p><ol><li>Pelatihan untuk mengembangkan daya anaerobik laktat (peak power). Pelatihan untuk daya laktat anaerobik mirip dengan pelatihan untuk kapasitas alaktik anaerobik.</li><li>Pelatihan untuk mempertahankan daya anaerobik laktat. Pelatihan semacam ini adalah bentuk pelatihan laktat yang paling umum.</li><li>Pelatihan untuk mengembangkan kapasitas anaerobik</li></ol><p> </p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong> METODE PELATIHAN UNTUK SISTEM LAKTIK ANAEROBIK PELATIHAN PEAK POWER VARIABEL PEMELIHARAAN PEAK POWER KAPASITAS </strong></p><p>Fase Latihan (dtk) 20-30 30-60 60-90</p><p>Fase Jeda (dtk) 120-150 150-360 240-270</p><p>Rasio W:P 1:6 atau 1:5 1:5 atau 1:6 80-85</p><p>Intensitas (% usaha maksimal) 85-90 85-90 80-85</p><p>Pengulangan (Tidak) 6-9 3-6 2-3</p><p>Volume Set Maksimum (Detik) 3 3 3</p><p>Set (Tidak) 4-6 4-6 2-3</p><p>Istirahat antar set (min) 10-15 10-15 15-20</p><p>Rentang Kemajuan untuk latihan Vol (Min) 3-12 3-12 4-9</p><p>Perkiraan Waktu Latihan (Min) 55-75 60-75 24-40</p><p><br></p>',
                'created_at' => '2022-08-12 23:10:34',
                'updated_at' => '2022-08-19 15:30:51',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            21 =>
            array(
                'season_plan_id' => $physical->id,
                'name' => 'Strength',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Tandai periode di mana Anda ingin fokus pada pengembangan kekuatan maksimal, kekuatan otot atau kecepatan otot, detail lebih cepat seperti latihan yang sebenarnya, intensitas latihan atau volume latihan (jumlah set dan pengulangan) harus dimasukkan dalam buku catatan latihan fisik Anda.</p><p><br></p><p><strong>1. ADAPTASI ANATOMI / ANATOMICAL ADAPTATION:</strong></p><p>setelah masa transisi, secara ilmiah atlet akan memulai program latihan kekuatan untuk menyesuaikan anatomi dengan program baru (mereka yang tidak melakukan latihan kekuatan yang kuat)</p><p><br></p><p><strong>2. KEKUATAN UMUM/HIPERTROFI / HYPERTROPHY:</strong></p><ul><li>untuk pemula (stabilitas inti, serat otot terutama ditargetkan/serat putaran lambat atau cepat, ukuran otot akan meningkat/hipertrofi)</li><li>Volume (set 3 - 6 dan ulangi 8 - 12); intensitas 65 - 80% 1RM; waktu istirahat 30 - 60 detik; frekuensi 3 - 4 kali/minggu.</li><li>Dalam hal fase pelatihan (daya tahan, hipertrofi, kekuatan)</li></ul><p><br></p><p><strong>3. KEKUATAN MAKSIMUM/ MAX STRENGTH:</strong></p><p>karena sebagian besar olahraga membutuhkan kekuatan dan daya tahan otot dan dipengaruhi oleh kekuatan maksimal yang terakhir perlu dikembangkan</p><ul><li>volume (set 3 - 4 dan repetisi 1 - 6); intensitas 85 - 100% 1RM; waktu istirahat 2 - 4 menit; frekuensi 2 kali/minggu</li></ul><p><br></p><p><strong>4. KEKUATAN KECEPATAN / SPEED STRENGTH: </strong>mengacu pada tingkat kontraksi maksimum, atau kemampuan untuk menggerakkan tubuh secepat mungkin. faktor kinerja fisik ini sangat penting untuk kinerja puncak dalam banyak olahraga. Kecepatan tergantung pada persentase kedutan cepat di tubuh atlet dan pada kemampuan sistem saraf untuk menggunakan serat ini dengan benar.</p><p>Dengan melakukan latihan yang tepat, atlet dapat meningkatkan kapasitasnya untuk merekrut jenis serat otot yang diinginkan.</p><p>Kecepatan-kekuatan penting dalam olahraga yang melibatkan lari cepat, lompat, lempar, dan pukulan dengan sebuah alat.</p><p><br></p><ul><li>volume (set 3 - 4 dan ulangi 4- 6); intensitas 85 - 100% dari kekuatan maksimal; waktu istirahat 2 - 4 menit; frekuensi 2 kali/minggu</li></ul><p><br></p><p><strong>5. KETAHANAN KEKUATAN / STRENGTH ENDURANCE :</strong> Yang sering disebut sebagai muscle endurance – adalah kemampuan untuk mempertahankan atau mengulang suatu otot terhadap yang relatif tinggi kebutuhannya.</p><p><br></p><ul><li>volume (15 atau lebih pengulangan); intensitas 40 - 70% dari kekuatan maksimal; waktu istirahat 30 - 45 detik; frekuensi 1 - 2 kali/minggu.</li></ul><p><br></p><p><strong>6. DEVELOP POWER / PENGEMBANGAN DAYA:</strong></p><ul><li>Setelah dasar kekuatan dikembangkan, kekuatan maksimal secara bertahap diubah menjadi kekuatan dan daya tahan otot melalui penerapan yang sesuai dan metode pelatihan yang memadai berdasarkan kebutuhan olahraga tertentu.</li><li>Karakteristik utama adalah daya maksimal, bobot berat, eksplosif, dan plyometric.</li><li>Serat otot terutama ditargetkan (mengembangkan serabut otot putih (fast twist) mereka menjadi lebih kuat</li><li>Tingkat serat otot akan berubah (adaptasi saraf).</li><li>Repetisi 1 - 3</li></ul><p><br></p><p><strong>7. KEKUATAN / POWER:</strong></p><p> &nbsp; &nbsp; * Daya ledak / plyometric</p><p> &nbsp; &nbsp; * Kekuatan + kecepatan</p><p><br></p>',
                'created_at' => '2022-08-12 23:11:01',
                'updated_at' => '2022-09-29 09:21:59',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            22 =>
            array(
                'season_plan_id' => $physical->id,
                'name' => 'Flexibility',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p><strong>FLEKSIBILITAS -</strong> rentang gerak bagian tubuh.</p><p><br></p><p>Tandai periode di mana Anda ingin meningkatkan fleksibilitas Anda</p><p><br></p><p><strong>FLEKSIBILITAS</strong> adalah komponen kunci dari kinerja dalam olahraga, pencegahan cedera, dan rehabilitasi cedera</p><p><br></p><p><strong>Ada tiga jenis fleksibilitas:</strong></p><p><br></p><ol><li><strong>Fleksibilitas aktif statis</strong>, rentang gerak terjadi sebagai akibat dari gerakan yang lambat dan terkontrol. "Statis" mengacu pada gerakan menjadi lambat; "aktif" mengacu pada kekuatan otot internal yang mengendalikan gerakan.</li><li><strong>Fleksibilitas aktif dinamis</strong>, jangkauan gerak dicapai dengan sangat cepat. "Dinamis" mengacu pada gerakan yang cepat.</li><li><strong>Fleksibilitas Pasif ( PNF )</strong>, gaya eksternal dalam meningkatkan rentang gerak normal di luar ekstrem aktif.</li></ol><p><br></p><p><strong>BATAS JANGKAUAN GERAK</strong></p><p>Baik tulang dan jaringan lunak membatasi rentang gerak atlet pada sendi tertentu. Berikut adalah beberapa contoh jaringan yang membatasi:</p><p>* Tulang * Tulang Rawan</p><p>* Ligamen * Camsule sendi</p><p>* Tendon * Otot</p><p><br></p>',
                'created_at' => '2022-08-12 23:11:10',
                'updated_at' => '2022-08-19 15:31:22',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            23 =>
            array(
                'season_plan_id' => $physical->id,
                'name' => 'Speed',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Speed</p><p>Kelincahanan / Agility / change direction</p><p>Kecepatan reaksi / Quickness / reaction time</p><p>Overspeed</p><p><br></p>',
                'created_at' => '2022-08-12 23:11:19',
                'updated_at' => '2022-08-19 15:31:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            24 =>
            array(
                'season_plan_id' => $physical->id,
                'name' => 'Nutrition',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Untuk setiap fase pelatihan,</p><p>Tentukan perkiraan total kalori yang ingin dikonsumsi atlet per hari, misalnya: untuk satu minggu sebelum kompetisi utama, Anda dapat menunjukkan beban "carbo" dalam minggu itu. Anda mungkin juga ingin menunjukkan kelompok makanan yang ingin Anda konsumsi lebih banyak dan juga suplemen apa pun yang mungkin Anda gunakan. Ini harus dikaitkan dengan intensitas latihan, volume latihan, dan target berat badan Anda.</p>',
                'created_at' => '2022-08-12 23:11:34',
                'updated_at' => '2022-10-10 16:09:12',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            25 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Team Cohesion',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<ol><li>Secara umum, lingkungan tempat atlet beroperasi hampir semua mempengaruhi yang mereka lakukan: motivasi untuk berlatih, Keterbukaan, komitmen untuk meningkat kinerja, tingkat kecemasan, perasaan penerimaan pribadi, kepuasan keseluruhan dan kemampuan untuk melakukan potensi (Orlick, 1986)</li><li>Lingkungan yang positif ditandai dengan hal-hal berikut: Kepuasan pribadi ; * Identitas tim yang kuat; * Kekompakan Tim; * Saling penerimaan; * Komunikasi yang efektif.</li><li>Setidaknya ada lima cara untuk membantu dalam pengembangan* lingkungan yang positif: * Rencanakan keterlibatan jangka panjang dalam olahraga; * Tawarkan banyak insentif; * Menentukan tujuan; * Kembangkan harmoni tim; Berkomunikasi secara efektif</li></ol>',
                'created_at' => '2022-08-12 23:11:59',
                'updated_at' => '2022-08-19 15:33:02',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            26 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Goal Setting',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p>Goal setting adalah sebuah proses yang bertujuan untuk mengidentifikasi prioritas turnamen yang kemudian dikembangkan pelatih menjadi strategi untuk mencapai tujuan prestasi atlet secara profesional. </p><p>Goal setting merupakan sesuatu yang terus menerus disarankan demi</p><p>mencapai kesuksesan karena memang efektif dalam proses mencapai tujuan yang atlet ingin raih ataupun dalam kehidupan.</p><p>Ada pun cara menetapkan tujuan dalam olahraga yaitu:</p><p><strong>1. SMART</strong> (Specific, Measurable, Attainable, Relevant and Time-bound) - Goal Setting</p><p><strong>2. Outcome, Performance, Process-Goal Setting</strong></p><p><strong>Spesifik</strong></p><p>Penetapan tujuan untuk melakukan yang terbaik yakni melakukan satu kinerja yang spesifik,</p><p>misal dalam hal stamina teknik, atau taktik, sarana dalam menuju kehidupan yang lebih baik.</p><p>Memang ada keadaan di mana goal setting hanya bisa bersifat subjektif, namun sebisa</p><p>mungkin ini dijadikan pengecualian dan lebih sering menetapkan tujuan yang spesifik atau</p><p>sejelas mungkin.</p><p><strong>Measurable (Terukur)</strong></p><p>Yang terpenting suatu tujuan haruslah bisa terukur. Penetapan tujuan haruslah terukur dalam</p><p>memberikan tolok ukur pada sebuah tujuan yang dianggap dapat dicapai mengacu pada</p><p>kemampuan atau kemahiran atlet. Contoh tolok ukur yang bisa dilakukan adalah apabila</p><p>berhasil melakukan smash yang sesuai harapan sebanyak 10 kali berturut-turut pada saat</p><p>latihan.</p><p><strong>Attainable (Mampu dicapai)</strong></p><p>Sebuah tujuan haruslah realistis dan memang mampu dicapai. Tujuan yang realistis adalah</p><p>sebuah tujuan yang memang dalam jangka waktu tertentu dapat diraih dengan kinerja yang</p><p>keras dan maksimal. Contoh tujuan realistis adalah ‘lolos seleksi pelatnas’ saat atlet tersebut</p><p>memang sudah banyak mengasah diri di berbagai pertandingan dan berlatih secara keras.</p><p>Contoh tujuan yang tidak realistis adalah apabila berharap satu tujuan dicapai dalam waktu</p><p>yang singkat dan sangat ekstrem. Misalnya, memberikan sebuah target ‘untuk memiliki</p><p>kemampuan backhand smash seperti Taufik Hidayat dalam jangka waktu 5 hari padahal atlet</p><p>baru belajar backhand smash selama seminggu.</p><p><strong>Relevant (Relevan)</strong></p><p>Pengertian relevan adalah mempunyai kaitan dan hubungan yang erat dengan pokok masalah.</p><p>Bermakna sebagai referensi yang berhubungan dengan hal-hal yang akan dilakukan.</p><p>diperlukan data-data yang menunjang yang terkait dengan permasalahan yang akan dihadapi,</p><p>data-data yang berguna langsung dengan sebuah tujuan. Penting juga untuk diperhatikan</p><p>bahwa realistis bukanlah pesimistis.</p><p><strong>Time Bound (Batas Waktu)</strong></p><p>Batas waktu adalah tujuan atau tugas yang diukur atau dibatasi oleh waktu. Ini berarti bahwa</p><p>pelatih hanya memiliki waktu tertentu untuk mencapainya. Secara sederhana, time bound</p><p>bahwa atlet mempunyai waktu kapan tujuan itu ingin dicapai. Tujuan tersebut dapat berupa</p><p>skill ataupun prestasi yang ingin diraih dalam jangka waktu tertentu. Misalnya menguasai</p><p>sebuah kemampuan dalam waktu 2 minggu atau setelah 3 pertandingan. Perlu diperhatikan</p><p>lagi bahwa penetapan waktu untuk mencapai suatu tujuan tertentu juga harus realistis.</p>',
                'created_at' => '2022-08-12 23:12:13',
                'updated_at' => '2022-08-19 15:34:24',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            27 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Imagery',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p><span style="color: rgb(32, 33, 36);"><strong>Imagery adalah t</strong></span><span style="color: rgb(32, 33, 36);">eknik yang biasa digunakan oleh psikolog olahraga untuk membantu seseorang memvisualisasikan atau melatih mental berkaitan dengan kegiatan yang akan dilakukan (Setyawati, 2014).</span></p><p>Apa yang dimaksud dengan imagery dan berikan contohnya?</p><p><strong>Imagery</strong> merupakan pengalaman sensoris yang terjadi dalam dalam pikiran tanpa adanya alat bantu dari lingkungan. Melalui <strong>imagery</strong> , atlet bulutangkis dapat memukul kok dengan keras sambil berbaring di sofa tanpa harus memegang raket dan bergerak.</p><p><strong>Imagery</strong> melibatkan keterampilan dan gerakan khusus imagery yang terkait dengan latihan, seperti bentuk dan posisi tubuh, arah gerakan, langkah atau keterampilan tertentu, dll.</p>',
                'created_at' => '2022-08-12 23:12:25',
                'updated_at' => '2022-08-19 15:34:59',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            28 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Self Talk',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p><span style="color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 15px;">Self talk merupakan dialog internal dengan diri kita sendiri. Hal tersebut biasanya dipengaruhi oleh pikiran alam bawah sadar, keyakinan, pertanyaan, ungkapan pikiran, dan ide diri sendiri. Jenis self talk sendiri ada dua, yaitu self talk positif dan negatif. Oleh sebab itu, terkadang self talk bisa melapangkan hati atau juga bisa menyusahkan.</span></p><p><span style="color: rgb(102, 102, 102); background-color: rgb(255, 255, 255); font-size: 15px;">Berbagai macam self talk yang terjadi akan bergantung pada kepribadian masing-masing orang. Untuk seseorang yang memiliki sikap optimis, maka biasanya self talk yang mereka lakukan akan lebih penuh harapan dan juga positif. Begitu pula sebaliknya, bisa self talk yang dilakukan cenderung pesimis, maka semuanya akan menyulitkan diri sendiri.</span></p><h2 style="text-align: left;">Benarkah Self-Talk Positif Bermanfaat?</h2><p style="text-align: left;">Masing-masing individu akan selalu mempunyai suara batin di kepala mereka sendiri, yang mana hal itu akan bekerja seperti narasi. Sama halnya seperti narasi yang berlangsung di dalam drama ataupun film. Kisah hidup yang mereka alami akan mempunyai suara batin yang berasal dari dalam diri sendiri.</p><p style="text-align: left;">Suara batin tersebut terus memberitahu bahwa kita harus selalu menimbang pro dan kontra, menyampaikan segala sesuatu dengan benar ketika sedang berbicara, dan sering mendorong diri sendiri untuk bertindak. Oleh sebab itu, berbicara dengan diri sendiri mengenai hal-hal yang positif akan sangat bermanfaat untuk kesehatan mental dan fisik. Perlu diingat bahwa kita tidak bisa menjalani kehidupan yang positif dengan pikiran yang masih negatif.</p><h2>Manfaat Positive Self-Talk</h2><p>Ada banyak manfaat yang dapat kita peroleh dengan melakukan self talk yang positif. Berikut ini adalah beberapa diantaranya:</p><h3>1. Mempunyai perspektif baru di masa sulit</h3><p>Ketika sedang di dalam masa sulit, seperti halnya saat masa pandemi, dimana banyak orang yang kehilangan pekerjaan, sedang merasa patah hati, dan lain sebagainya. Tentu hal tersebut akan menambah kesusahan apabila kamu selalu berpikir negatif. Oleh karena itu, Kevin Gilliland, PsyD, seorang psikolog klinis yang memiliki lisensi dan juga seorang direktur eksekutif Innovation 360, yaitu sebuah layanan konseling rawat jalan, menyarankan untuk selalu melakukan self talk yang positif. Dengan melakukan hal tersebut secara rutin, maka akan membantu kita untuk mempunyai perspektif baru agar masa-masa sulit menjadi lebih ringan.</p><h3 style="text-align: left;">2. Memiliki hubungan yang lebih baik</h3><p style="text-align: left;">Dengan melakukan positive self talk tak hanya akan berpengaruh pada kesehatan mental, emosional, dan juga fisik saja. Akan tetapi juga akan berpengaruh pada hubungan antar manusia. Ketika dapat suatu hal yang baik di dalam diri kita sendiri, maka tentunya kita juga akan lebih mudah melihat hal-hal baik yang ada di dalam diri orang lain.</p><h3 style="text-align: left;">3. Meningkatkan kepercayaan diri</h3><p style="text-align: left;">Menurut Dr. Ho, positive self talk juga dapat membantu seseorang untuk membangun kepercayaan diri dan juga kontrol yang lebih baik atas apa yang terjadi di dalam hidup mereka. Pada intinya, apabila kamu memiliki percaya diri yang tinggi, maka tujuan akan lebih mudah tercapai, karena lebih realistis untuk menggapainya.</p><h3 style="text-align: left;">4. Menurunkan rasa kesepian</h3><p style="text-align: left;">Ketika pikiran negatif menyerang, seringkali rasa kesepian ikut hadir menyertainya. Biasanya hal itu akan membuat kita ingin mundur dari keramaian dan bersembunyi di dalam zona nyaman. Tapi, apakah kamu tahu bahwa positive self talk justru memberikan efek sebaliknya? Dimana kamu akan merasa lebih bisa menyeimbangkan semuanya. Selain itu, kamu juga akan lebih mudah untuk berhubungan dengan orang-orang tercinta dan orang-orang yang selalu ada untukmu.</p><p><br></p>',
                'created_at' => '2022-08-12 23:12:33',
                'updated_at' => '2022-08-19 15:35:25',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            29 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Concentration',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Mampu mengendalikan emosinya memungkinkan atlet untuk berkonsentrasi lebih baik.</p><p>Dan mampu berkonsentrasi adalah potensi dasar atletik. Atlet mengembangkan perhatian dan mengontrolnya dengan belajar memilih dan berkonsentrasi pada isyarat dan faktor yang relevan dengan tugas.</p><p><strong>Bagian ini mencakup tiga keterampilan dalam menjaga perhatian</strong>:</p><li><strong>KONSENTRASI:</strong> adalah keadaan pikiran yang alami dan santai yang memungkinkan atlet menerima dan menafsirkan informasi yang relevan. itu tidak mengacu pada berusaha keras untuk memperhatikan.</li><ul><li><strong>VISUALISASI:</strong> mengacu pada proses melihat diri Anda pada layar di mata pikiran Anda, secara sadar membangkitkan dan membimbing pikiran di mana Anda muncul ke arah akhir tertentu. pemikiran-pemikiran ini dapat mempengaruhi kinerja.</li></ul><ol><li><strong>VISUALISASI : </strong>dapat berbentuk pandangan internal atau eksternal. tampilan kamera yang menunjukkan shuttlers jumping smash adalah tampilan luar.</li></ol><p><strong>ADA DUA JENIS VISUALISASI YANG BERBEDA:</strong></p><li><strong>PEMECAHAN MASALAH -</strong> menggunakan visualisasi untuk membantu relaksasi dan konsentrasi atau untuk memeriksa kemungkinan pilihan dalam situasi tertentu.</li><li><strong>MENTAL REHEARSAL</strong> - menggunakan visualisasi untuk membantu kinerja gerakan atau keterampilan tertentu (Syser & Connolly, 1984)</li><ol></ol><p><br></p>',
                'created_at' => '2022-08-12 23:12:41',
                'updated_at' => '2022-08-19 15:35:46',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            30 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Strategies for Competition',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<li></li><p><strong>Strategi pra-kompetisi </strong></p><p>adalah rencana yang digunakan atlet untuk memastikan rasa percaya diri dan kontrol serta fokus yang tepat sebelum kompetisi. Rencana ini harus mencakup semua perilaku yang dilakukan atlet sebelum bertanding.</p><p>Strategi pra-kompetisi mencakup kegiatan sepanjang hari kompetisi. dengan kata lain, strategi mencakup pada saat kompetisi dan lokasi kompetisi.</p><p><strong>WAKTU KOMPETISI</strong></p><p>Perhatian utama pada hari "H" kompetisi adalah untuk menyelesaikan kegiatan penting dan meminimalkan gangguan dengan merencanakan, mengendalikan aktivasi, dan memfokuskan pikiran.</p><p>Perencanaan dalam konteks pengembangan strategi pra-kompetisi dirancang untuk membantu atlet menghadapi hal-hal yang dapat mengurangi kinerja. spesifikasi rencana seorang atlet tergantung pada apakah atlet tersebut harus melakukan perjalanan ke tempat pertandingan.</p><p>berikut adalah daftar rencana kegiatan yang harus dicakup</p><ol><li><strong>WAKING UP</strong> : perasaan orang saat pertama kali bangun sering kali mempengaruhi sikap dan perilaku sepanjang hari.</li><li><strong>MAKAN : </strong>yang terpenting adalah apa dan kapan harus makan, mengingat kapan kompetisi akan berlangsung</li><li><strong>MELAKUKAN AKTIVITAS RINGAN:</strong> jika perlu atau memungkinkan</li><li><strong>MEMERIKSA PERALATAN: </strong>ulangi prosedur yang diikuti pada malam sebelum atau sebelum bepergian ke tempat kompetisi</li><li><strong>VISUALISASI WAKTU KHUSUS</strong>: gunakan latihan mental atau pemecahan masalah</li><li><strong>MENGIZINKAN RUANG PRIBADI / MENYENDIRI:</strong> terlalu banyak berinteraksi dengan rekan tim atau kompetisi dapat menyebabkan stres</li><li><strong>MELAKUKAN KEGIATAN DISOSIASI:</strong> luangkan waktu sejenak untuk meninggalkan situasi kompetisi</li><li><strong>REFOCUSSING :</strong> mengetahui cara mengatasi kehilangan konsentrasi.</li><li></li></ol><p><strong>LANGKAH UNTUK MENGEMBANGKAN STRATEGI KOMPETISI UNTUK KONTROL YANG LEBIH BESAR"</strong></p><ul><li><strong>IDENTIFIKASI PEMICU</strong> - peristiwa yang menyebabkan kemarahan</li><li><strong>REDIFINE PEMICU / MENDIVINISIKAN KEMBALI: </strong>dengan cara yang positif</li><li><strong>MONITORING DIRI-TALK</strong> saat kemarahan meningkat</li><li><strong>LATIHAN SELF-TALK</strong> untuk memicu perilaku positif</li><li><strong>LATIHAN MENGENDALIKAN TINDAKAN,</strong> bukan membela ego</li><li><strong>MENGGABUNGKAN TEKNIK SELF-TALK, VISUALISASI, DAN RELAKSASI </strong>untuk membentuk strategi kompetisi</li><li><strong>LATIH SECARA TERATUR MEMFOKUSKAN RESPONS</strong> terhadap situasi yang menyebabkan Anda kehilangan kesabaran.</li></ul><p><br></p>',
                'created_at' => '2022-08-12 23:12:50',
                'updated_at' => '2022-08-19 15:36:05',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            31 =>
            array(
                'season_plan_id' => $mental->id,
                'name' => 'Application',
                'input_type' => 'text',
                'desc_en' => '',
                'desc_id' => '<p>Menerapkan prinsip-prinsip pelatihan mental untuk atlet Anda dan diri Anda sendiri disebut penglihatan ganda (Smith, 1983). Langkah yang Anda ambil untuk memperoleh penglihatan ganda sejajar dengan langkah-langkah yang diambil atlet untuk mempelajari keterampilan pelatihan mental:</p><ul><li>Mengembangkan keterampilan pelatihan mental. berlatih kontrol emosi dan perhatian dengan atlet Anda</li></ul><ol><li>Mengamati IPS. Gunakan bentuk yang sama yang dilakukan atlet untuk menilai kinerja Anda dan menemukan IPS Anda</li><li>Strategi perencanaan. Pikirkan bagaimana menggunakan keterampilan mental untuk mencapai dan mempertahankan IPS Anda</li><li>Mengembangkan prosedur koping. Kembangkan prosedur khusus untuk menangani masalah pembinaan yang merepotkan. prosedur-prosedur ini dapat melengkapi keterampilan pelatihan mental Anda dan membantu Anda mempertahankan IPS Anda.</li></ol><p><strong>Berikut beberapa prosedur koping yang direkomendasikan:</strong></p><ul><li><strong>HINDARI,</strong> jika Anda tahu bahwa interaksi atau situasi tertentu akan menimbulkan stresl dan Anda dapat menghindarinya tanpa mengganggu pelatihan Anda, sebaiknya Anda melakukannya.</li><li><strong>KOMPENSASI,</strong> Mengganti respon yang tidak tepat untuk situasi tertentu dengan perilaku yang lebih tepat</li><li><strong>DELEGASI TANGGUNG JAWAB PELATIHAN,</strong> Membagi tanggung jawab membantu memastikan bahwa semuanya selesai-tanpa membuat Anda lebih stres</li><li><strong>ASISTEN DARI ATLET,</strong> jika Anda cenderung kasar secara verbal dengan atlet, ofisial, atau penonton, beri izin kepada atlet Anda untuk tetap berada di bawah Anda bahwa Anda sedang terbawa suasana.</li><li><strong>EVALUASI PEER (TEMAN SEBAYA).</strong> Mintalah pelatih lain menilai perilaku Anda dan memfasilitasi diskusi tentang kemungkinan prosedur koping.</li><li><strong>EVALUASI DIRI.</strong> Rekam video diri Anda selama latihan dan kompetisi - ini akan memberi Anda evaluasi yang lebih objektif atas perilaku Anda.</li></ul><p>Mengintegrasikan prosedur dan strategi koping (menyalin):</p><p>Jadikan prosedur koping sebagai bagian dari pra-kompetisi dan kompetisi perilaku Anda.</p><p>&gt; Menerapkan strategi dalam praktik</p><p>&gt; Memvisualisasikan tanggapan yang tepat untuk situasi kritis</p><p>&gt; Menerapkan strategi dalam persaingan</p><p>&gt; Mengevaluasi efektivitas strategi Anda</p><p>&gt; tanya jawab dengan atlet dan rekan untuk membantu menyempurnakan &nbsp;</p><p> &nbsp; strategi Anda</p><p><br></p>',
                'created_at' => '2022-08-12 23:13:00',
                'updated_at' => '2022-08-19 15:36:19',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            32 =>
            array(
                'season_plan_id' => $progress->id,
                'name' => 'Monitoring/Evaluation',
                'input_type' => 'text',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p><strong>Menilai dan memantau kapasitas fisik seorang atlet dapat membantu Anda dalam banyak hal:</strong></p><ul><li>Memungkinkan Anda merancang program pelatihan yang lebih tepat</li><li>Membantu Anda memantau kemajuan dalam pelatihan</li><li> Memotivasi atlet dan memberi mereka insentif untuk berlatih</li><li>Membantu atlet memahami tujuan dan alasan pelatihan</li><li>Memberi Anda lebih banyak data tentang karakteristik fisik atlet - dan berkontribusi pada pengembangan standar khusus olahraga.</li><li>Dapat membantu Anda membimbing atlet ke dalam olahraga yang lebih sesuai dengan atribut fisik mereka.</li><li>Dapat membantu Anda memprediksi kinerja</li></ul><p><br></p><p><strong>Menilai kapasitas fisik seorang atlet terdiri dari melakukan hal berikut:</strong></p><ul><li>Menggunakan tes. tes adalah teknik atau prosedur khusus yang digunakan untuk mengumpulkan informasi terkait kinerja</li><li>Melakukan pengukuran, yaitu mengumpulkan tanggal kuantitatif tertentu</li><li>Melakukan evaluasi. Evaluasi adalah interpretasi data yang dikumpulkan melalui pengujian dan pengukuran. Evaluasi suara juga melibatkan perbandingan dengan beberapa standar atau norma.</li></ul><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>PEMANTAUAN</strong></p><p><br></p><p>Pemantauan kinerja terdiri dari memiliki pendekatan yang direncanakan untuk pengujian dan menggunakan hasil pengujian.agar efektif, </p><p>Pemantauan harus dilakukan pada waktu yang tepat selama tahun pelatihan. selain itu, Anda harus menggunakan tes yang sesuai untuk periode pelatihan tempat Anda mengelolanya.</p><p>Misalnya, Anda mungkin menggunakan banyak tes laboratorium sebelum setiap fase pelatihan utama, tetapi sejumlah kecil tes lapangan</p><p>pada akhir siklus pelatihan tertentu. tes kinerja mingguan atau bahkan harian, </p><p>Misalnya, waktu sprint atau waktu yang dibutuhkan untuk menyelesaikan rangkaian latihan, mungkin juga informatif.</p>',
                'created_at' => '2022-08-12 23:13:18',
                'updated_at' => '2022-08-19 15:32:32',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            33 =>
            array(
                'season_plan_id' => $workout->id,
                'name' => '% Intensity',
                'input_type' => 'number',
                'desc_en' => '<p><br></p>',
                'desc_id' => '<p>November 23rd, 2018</p><p>Memahami Frekuensi Intensitas & Volume Latihan. Pada kali ini saya akan menjelaskan mengenai frekuensi,intensitas, dan volume. Jika anda ingin membuat atau menjalankan program latihan anda, pertama-tama anda harus mengetahui 3 hal tersebut. 3 hal ini memiliki peran penting dalam semua rencana latihan anda, Jika anda sudah memahami 3 hal tersebut anda dapat mengikuti atau membuat program latihan anda dengan efisien.</p><p><br></p><p><span style="color: rgb(68, 68, 68);">"intensitas". Intensitas dalam program pelatihan terkait dengan jumlah beban yang anda gunakan. Jika anda berlatih dengan beban yang sangat berat maka itu adalah pelatihan dengan intensitas tinggi. Jika anda berlatih dengan beban yang ringan maka itu adalah intensitas rendah. Powerlifting biasanya program dengan intensitas tinggi karena setiap repetisi dilakukan dengan beban berat yang membutuhkan banyak upaya untuk mengangkatnya. Banyak program powerlifting dengan volume rendah dan umumnya frekuensinya juga rendah. Anda perlu memahami intensitas karena volume dan frekuensi anda akan menentukan jenis intensitas apa yang harus anda terapkan. Misalnya, jika saya mengatakan bahwa anda harus melakukan 5 set 3-5 repetisi pada suatu latihan dan anda menggunakan pendekatan intensitas rendah, maka anda tidak akan mendapatkan apa pun. Jika anda menggunakan intensitas rendah dengan 3-5 repetisi maka saya ragu anda akan mendapat hasil maksimal. Hal yang sama dapat dikatakan untuk latihan untuk 3 set 12-15 repetisi. Jika anda memilih berat dengan intensitas tinggi maka kemungkinan anda tidak akan mencapai rentang 12-15 reps.</span></p><p><span style="color: rgb(51, 51, 51);">Writer : </span><a href="https://www.sfidn.com/forum/member/2026-cakra-adi" target="_blank">Cakra Adi ( SFIDN Personal Trainer </a></p><p><br></p><p>I<strong>NTENSITAS,</strong></p><p>Komponen kualitatif dari latihan yang dilakukan seorang atlet dalam waktu tertentu, juga merupakan komponen penting dari pelatihan.</p><p>Semakin banyak latihan yang dilakukan atlet per unit waktu, semakin tinggi intensitasnya.</p><p><br></p><p><strong>Intensitas adalah</strong> fungsi dari kekuatan impuls saraf yang digunakan atlet dalam latihan. Kekuatan suatu stimulus tergantung pada beban, kecepatan kinerja, dan variasi interval atau istirahat antara pengulangan.</p><p>Yang terakhir namun tidak kalah pentingnya, elemen penting dari intensitas adalah ketegangan psikologis dari suatu latihan, kerja otot dan keterlibatan klub melalui konsentrasi maksimum yang menentukan intensitas selama pelatihan atau kompetisi.</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p><p><strong>SKALA INTENSITAS UNTUK LATIHAN KECEPATAN DAN KEKUATAN </strong></p><p><strong>NO</strong></p><p><strong>INTENSITAS KINERJA MAKSIMUM INTENSITAS</strong></p><p><strong> </strong>1 30 - 50 Rendah</p><p>2 50 - 70 Menengah</p><p>3 70 - 80 Sedang</p><p> &nbsp;4 80 - 90 Sub-Maksimum</p><p>5 90 - 100 Maksimum</p><p> &nbsp; 6 100 - 105 Super Maksimum</p><p><br></p><p><strong>EMPAT ZONA INTENSITAS BERDASARKAN REAKSI DENYUT NADI TERHADAP BEBAN LATIHAN</strong></p><p><strong>INTENSITAS PERSENTASE </strong></p><p><strong>ZONA JENIS INTENSITAS DENYUT JANTUNG / MIN</strong></p><p>1 Rendah 120 - 150 </p><p>2 Sedang 150 - 170 &nbsp;</p><p>3 Tinggi 170 - 185 &nbsp;</p><p>4 Maksimum &gt; 185 &nbsp; &nbsp; </p><p><br></p><p>HR super maksimum = HR istirahat + 60 (HR max - HR &nbsp; &nbsp;</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong> &nbsp;LIMA ZONA INTENSITAS</strong></p><p><br></p><p> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</p><p>NO ZONA DURASI KERJA LEVEL INTENSITAS ENERGI SISTEM PENGHASIL</p><p> &nbsp;1 1 - 15 detik Hingga batas maksimum ATP-CP</p><p> &nbsp;2 15 - 60 detik Maksimum ATP-CP dan LA</p><p> &nbsp;3 1 - 6 menit LA sub-maksimum dan aerobik</p><p> &nbsp;4 6 - 30 menit Aerobik Sedang</p><p> &nbsp;5 Lebih dari 30 menit Aerobik Rendah</p><p><br></p><p><strong> ERGOGENESIS %</strong></p><p>ANAEROBIK AEROBIK</p><p>100 - 95 0 - 5</p><p>90 - 80 10 - 20</p><p>70 - (40 - 30) 30 - (60 - 70)</p><p>(40 - 30) - 10 (60 - 70) - 90</p><p>5 95</p><p><br></p><p><span style="color: rgb(68, 68, 68);"><strong>KESIMPULAN </strong></span></p><p><span style="color: rgb(68, 68, 68);">Dengan anda </span><a href="https://www.sfidn.com/frekuensi-intensitas--volume-latihan" target="_blank">memahami volume, intensitas, dan frekuensi</a><span style="color: rgb(68, 68, 68);">. Anda dapat memilih intensitas yang tepat pada tiap set latihan anda. Anda dapat membuat progres besar tanpa memahami ketiga hal ini, akan tetapi anda akan membuat progres yang lebih baik dan memiliki kemampuan untuk membuat rencana latihan yang lebih baik untuk anda sendiri di masa depan.</span></p>',
                'created_at' => '2022-08-12 23:13:56',
                'updated_at' => '2022-08-19 15:37:05',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            34 =>
            array(
                'season_plan_id' => $workout->id,
                'name' => '% Volume',
                'input_type' => 'number',
                'desc_en' => '',
                'desc_id' => '<p>November 23rd, 2018</p><p><strong>Memahami Frekuensi Intensitas & Volume Latihan</strong>.</p><p>Pada kali ini saya akan menjelaskan mengenai frekuensi,intensitas, dan volume. Jika anda ingin membuat atau menjalankan program latihan anda, pertama-tama anda harus mengetahui 3 hal tersebut. 3 hal ini memiliki peran penting dalam semua rencana latihan anda, Jika anda sudah memahami 3 hal tersebut anda dapat mengikuti atau membuat program latihan anda dengan efisien.</p><p><br></p><p><span style="color: rgb(68, 68, 68);"><strong>Membahas "volume"</strong></span><span style="color: rgb(68, 68, 68);"> dalam rencana pelatihan maka kita berbicara tentang beban kerja yang akan kita lalui di setiap otot. Sebuah rencana dengan volume tinggi anda dapat melakukannya dengan 5-10 set per exercise, 15+ repetisi per set, dan 10 exercise per hari. Rencana dengan volume yang rendah mungkin membuat anda menargetkan 3-5 set per exercise, 4-6 repetisi per set, dan 3-5 exercise per hari. Program powerlifting biasanya adalah program dengan volume rendah sementara program endurance akan cenderung menjadi latihan dengan volume tinggi. Saya sering menemukan bahwa rencana berfrekuensi tinggi berjalan dengan baik dengan rencana volume tinggi dan rencana frekuensi rendah berjalan dengan baik dengan rencana volume rendah. Terlepas dari gaya anda di sini, jika frekuensi memengaruhi volume, maka volume juga dipengaruhi oleh Intensitas, yang merupakan topik yang akan dibahas berikutnya.</span></p><p><span style="color: rgb(68, 68, 68);">oleh </span></p><p><span style="color: rgb(51, 51, 51);">Writer : </span><a href="https://www.sfidn.com/forum/member/2026-cakra-adi" target="_blank">Cakra Adi ( SFIDN Personal Trainer </a></p><p><br></p><p><strong>Volume </strong></p><p>adalah ukuran jumlah total aktivitas atau latihan yang Anda lakukan. Jika Anda seorang atlet aerobik, Anda mengukur pelatihan dalam unit seperti jarak dan waktu.</p><p><br></p><p><strong>Volume latihan </strong></p><p><br></p><p>Volume latihan resistensi biasanya melibatkan variabel seperti:</p><p><br></p><ul><li><strong>Repetisi </strong>- Pengulangan atau pengulangan melakukan latihan satu kali.</li><li><strong>Set </strong>- Pengelompokan repetisi.</li><li><strong>Berat</strong> - Berapa banyak berat atau beban yang Anda angkat.</li><li><strong>Waktu</strong> - Berapa lama Anda menghabiskan pelatihan atau waktu di bawah tekanan per set.</li><li><strong>Jarak yang ditempuh</strong> - Seberapa jauh Anda memindahkan diri sendiri atau beban yang Anda lakukan bekerja melawan.</li><li><strong>Upaya </strong>- Seberapa keras Anda latihan, lebih banyak tentang ini dalam satu menit.</li></ul><p><br></p><p>Seringkali kita menghitung volume untuk satu kelompok otot seperti bisep atau untuk pola gerakan seperti jongkok.</p><p><br></p><p><strong>Apa itu volume dalam pelatihan?</strong></p><p>Secara umum, volume latihan dianggap sebagai produk dari intensitas—<strong>jumlah beban spesifik yang digunakan per set, jumlah repetisi yang dilakukan, dan jumlah total set dalam satu latihan. </strong>Volume juga dapat menjelaskan waktu di bawah ketegangan atau tempo (kecepatan) latihan.</p><p><br></p><p><span style="color: rgb(68, 68, 68);"><strong>KESIMPULAN </strong></span></p><p><span style="color: rgb(68, 68, 68);"><strong> </strong></span><span style="color: rgb(68, 68, 68);">Dengan anda </span><a href="https://www.sfidn.com/frekuensi-intensitas--volume-latihan" target="_blank">memahami volume, intensitas, dan frekuensi</a><span style="color: rgb(68, 68, 68);">. Anda dapat memilih intensitas yang tepat pada tiap set latihan anda. Anda dapat membuat progres besar tanpa memahami ketiga hal ini, akan tetapi anda akan membuat progres yang lebih baik dan memiliki kemampuan untuk membuat rencana latihan yang lebih baik untuk anda sendiri di masa depan.</span></p><p><br></p>',
                'created_at' => '2022-08-12 23:14:04',
                'updated_at' => '2022-08-19 15:37:17',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            35 =>
            array(
                'season_plan_id' => $workout->id,
                'name' => '% Spesific',
                'input_type' => 'number',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:14:16',
                'updated_at' => '2022-08-12 23:14:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            36 =>
            array(
                'season_plan_id' => $workout->id,
                'name' => 'No of hours per week',
                'input_type' => 'number',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:14:21',
                'updated_at' => '2022-08-15 14:23:21',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            37 =>
            array(
                'season_plan_id' => $workout->id,
                'name' => 'Grand total no of hours',
                'input_type' => 'number',
                'desc_en' => '',
                'desc_id' => '',
                'created_at' => '2022-08-12 23:14:32',
                'updated_at' => '2022-08-15 14:23:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
