<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ExerciseCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        \DB::table('exercise_categories')->insert(array(
            0 =>
            array(
                'category' => 'Quadriceps ',
                'created_at' => '2022-03-05 16:53:16',
                'updated_at' => '2022-03-05 16:53:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'category' => 'Hamstrings ',
                'created_at' => '2022-03-05 16:53:53',
                'updated_at' => '2022-03-05 16:53:53',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'category' => 'Abdominals ',
                'created_at' => '2022-03-05 16:54:19',
                'updated_at' => '2022-03-05 16:54:19',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'category' => 'Spinal Erectors ',
                'created_at' => '2022-03-05 16:54:57',
                'updated_at' => '2022-03-05 16:54:57',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'category' => 'Biceps',
                'created_at' => '2022-03-05 16:55:25',
                'updated_at' => '2022-10-25 08:44:16',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'category' => 'Deltoids ',
                'created_at' => '2022-03-05 16:55:38',
                'updated_at' => '2022-03-05 16:55:38',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'category' => 'Rotator Cuffs',
                'created_at' => '2022-03-05 16:55:46',
                'updated_at' => '2022-03-05 16:55:46',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'category' => 'Leg extension ',
                'created_at' => '2022-03-05 16:56:25',
                'updated_at' => '2022-03-05 16:56:25',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'category' => ' Leg flexion ',
                'created_at' => '2022-03-05 16:56:49',
                'updated_at' => '2022-03-05 16:56:49',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'category' => 'Trunk flexion ',
                'created_at' => '2022-03-05 16:57:08',
                'updated_at' => '2022-03-05 16:57:08',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'category' => 'Back extension ',
                'created_at' => '2022-03-05 16:57:32',
                'updated_at' => '2022-03-05 16:57:32',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'category' => 'Elbow flexion / extension',
                'created_at' => '2022-03-05 16:58:09',
                'updated_at' => '2022-03-05 16:58:09',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'category' => ' Shoulder flex/ext',
                'created_at' => '2022-03-05 16:58:43',
                'updated_at' => '2022-03-05 16:58:43',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            13 =>
            array(
                'category' => 'Internal and external rot',
                'created_at' => '2022-03-05 16:58:58',
                'updated_at' => '2022-03-05 16:58:58',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            14 =>
            array(
                'category' => 'Tricept',
                'created_at' => '2022-10-25 08:44:27',
                'updated_at' => '2022-10-25 08:44:27',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
