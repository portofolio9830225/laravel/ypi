<?php

namespace Database\Seeders;

use App\Models\ExerciseCategory;
use App\Models\GymType;
use Illuminate\Database\Seeder;

class SubExercisesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run($tenantId)
    {
        $gym1 = GymType::where('gym_description', 'Flexion and extension')->first();
        $gym2 = GymType::where('gym_description', 'Pronation and supination ')->first();

        $exer1 = ExerciseCategory::where('category', 'Trunk flexion ')->first();
        $exer2 = ExerciseCategory::where('category', 'Elbow flexion / extension')->first();
        $exer3 = ExerciseCategory::where('category', 'Biceps')->first();

        \DB::table('sub_exercises')->insert(array(
            0 =>
            array(
                'excercise_desc' => 'BENCH ROWS',
                'gym_type_id' => $gym1->id,
                'tut_per_rep' => 75,
                'exercise_category_id' => $exer1->id,
                'created_at' => '2022-02-05 11:06:08',
                'updated_at' => '2022-09-29 09:53:41',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            1 =>
            array(
                'excercise_desc' => 'BICEPT CURL',
                'gym_type_id' => $gym1->id,
                'tut_per_rep' => 100,
                'exercise_category_id' => $exer2->id,
                'created_at' => '2022-02-05 11:06:50',
                'updated_at' => '2022-07-26 07:14:20',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            2 =>
            array(
                'excercise_desc' => 'DUMBLE PULLOVERS',
                'gym_type_id' => $gym2->id,
                'tut_per_rep' => 20,
                'exercise_category_id' => $exer3->id,
                'created_at' => '2022-02-05 11:08:30',
                'updated_at' => '2022-10-25 08:37:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            3 =>
            array(
                'excercise_desc' => 'DUMBLE ROWS',
                'gym_type_id' => $gym1->id,
                'tut_per_rep' => 20,
                'exercise_category_id' => $exer3->id,
                'created_at' => '2022-02-05 11:08:53',
                'updated_at' => '2022-10-25 08:39:28',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            4 =>
            array(
                'excercise_desc' => 'CHIN UP',
                'gym_type_id' => $gym1->id,
                'tut_per_rep' => 6,
                'exercise_category_id' => $exer3->id,
                'created_at' => '2022-02-05 11:09:43',
                'updated_at' => '2022-10-25 08:41:47',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            5 =>
            array(
                'excercise_desc' => 'DUMBLE STIFF LEG',
                'gym_type_id' => 2,
                'tut_per_rep' => 10,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 11:11:52',
                'updated_at' => '2022-02-05 11:11:52',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            6 =>
            array(
                'excercise_desc' => 'DUMBLE UPRIGHT',
                'gym_type_id' => 2,
                'tut_per_rep' => 20,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 11:12:46',
                'updated_at' => '2022-02-05 11:12:46',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            7 =>
            array(
                'excercise_desc' => 'DEADLIFT',
                'gym_type_id' => 2,
                'tut_per_rep' => 15,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 11:13:05',
                'updated_at' => '2022-02-05 11:13:05',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            8 =>
            array(
                'excercise_desc' => 'GOOD MORNING',
                'gym_type_id' => 2,
                'tut_per_rep' => 10,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 11:13:56',
                'updated_at' => '2022-02-05 11:13:56',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            9 =>
            array(
                'excercise_desc' => 'HIGH PULL',
                'gym_type_id' => 2,
                'tut_per_rep' => 15,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 11:14:36',
                'updated_at' => '2022-02-05 12:52:19',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            10 =>
            array(
                'excercise_desc' => 'ICELANDIC CURL',
                'gym_type_id' => 2,
                'tut_per_rep' => 20,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 12:53:04',
                'updated_at' => '2022-02-05 12:54:24',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            11 =>
            array(
                'excercise_desc' => 'PUL DOWN/UP',
                'gym_type_id' => $gym1->id,
                'tut_per_rep' => 10,
                'exercise_category_id' => $exer2->id,
                'created_at' => '2022-02-05 12:55:13',
                'updated_at' => '2022-07-26 07:16:28',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            12 =>
            array(
                'excercise_desc' => 'SEATED ROW',
                'gym_type_id' => 2,
                'tut_per_rep' => 10,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 12:55:58',
                'updated_at' => '2022-02-05 12:55:58',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            13 =>
            array(
                'excercise_desc' => 'UPRIGHT ROW',
                'gym_type_id' => 2,
                'tut_per_rep' => 10,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 12:56:36',
                'updated_at' => '2022-02-05 12:56:36',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            14 =>
            array(
                'excercise_desc' => '1-ARM THROWS (MEDICINE  BALL)',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 12:57:52',
                'updated_at' => '2022-02-05 12:58:46',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            15 =>
            array(
                'excercise_desc' => '1-ARM THROWS (SMALL BALL)',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 12:59:25',
                'updated_at' => '2022-02-05 12:59:25',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            16 =>
            array(
                'excercise_desc' => 'ALTERNATING LEG JUMP SQUAD',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 15:34:58',
                'updated_at' => '2022-02-05 15:34:58',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            17 =>
            array(
                'excercise_desc' => 'BENCHS BEHID NECK',
                'gym_type_id' => 1,
                'tut_per_rep' => 30,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 15:35:46',
                'updated_at' => '2022-02-05 15:36:56',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            18 =>
            array(
                'excercise_desc' => 'BENCH PRESS',
                'gym_type_id' => 1,
                'tut_per_rep' => 30,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 15:36:40',
                'updated_at' => '2022-02-05 15:36:40',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            19 =>
            array(
                'excercise_desc' => 'BODY WEIGHT LUNGE',
                'gym_type_id' => 1,
                'tut_per_rep' => 10,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:10:42',
                'updated_at' => '2022-02-05 16:10:42',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            20 =>
            array(
                'excercise_desc' => 'CLAP PUSH UPS',
                'gym_type_id' => 1,
                'tut_per_rep' => 30,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:11:21',
                'updated_at' => '2022-02-05 16:11:21',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            21 =>
            array(
                'excercise_desc' => 'DB BENCH PRESS (ALL ANGLES)',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:13:06',
                'updated_at' => '2022-02-05 16:13:06',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            22 =>
            array(
                'excercise_desc' => 'DB LUNGE',
                'gym_type_id' => 1,
                'tut_per_rep' => 10,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:13:54',
                'updated_at' => '2022-02-05 16:13:54',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            23 =>
            array(
                'excercise_desc' => 'DB NARROW/CLOSE GRIP',
                'gym_type_id' => 1,
                'tut_per_rep' => 30,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:14:36',
                'updated_at' => '2022-02-05 16:14:36',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            24 =>
            array(
                'excercise_desc' => 'DB PRESS',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:14:59',
                'updated_at' => '2022-02-05 16:14:59',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            25 =>
            array(
                'excercise_desc' => 'DB TRICEPT EXTENTION',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:34:00',
                'updated_at' => '2022-02-05 16:34:00',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            26 =>
            array(
                'excercise_desc' => 'FRONT SQUAT',
                'gym_type_id' => 1,
                'tut_per_rep' => 15,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:34:40',
                'updated_at' => '2022-02-05 16:34:40',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            27 =>
            array(
                'excercise_desc' => 'INCLINE / DECLINE PRESS',
                'gym_type_id' => 1,
                'tut_per_rep' => 30,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:35:19',
                'updated_at' => '2022-02-05 16:35:19',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            28 =>
            array(
                'excercise_desc' => 'JUMP AND LANDING',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:35:46',
                'updated_at' => '2022-02-05 16:35:46',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            29 =>
            array(
                'excercise_desc' => 'JUMP SQUAT',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:36:13',
                'updated_at' => '2022-02-05 16:36:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            30 =>
            array(
                'excercise_desc' => 'LEG PRESS',
                'gym_type_id' => 1,
                'tut_per_rep' => 10,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:36:34',
                'updated_at' => '2022-02-05 16:36:34',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            31 =>
            array(
                'excercise_desc' => 'LEG PUSH',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:36:59',
                'updated_at' => '2022-02-05 16:36:59',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            32 =>
            array(
                'excercise_desc' => 'MEDICINE BALL THROWS',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:37:54',
                'updated_at' => '2022-02-05 16:37:54',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            33 =>
            array(
                'excercise_desc' => 'MILITARY PRESS',
                'gym_type_id' => 1,
                'tut_per_rep' => 30,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:38:18',
                'updated_at' => '2022-02-05 16:38:18',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            34 =>
            array(
                'excercise_desc' => 'PUSH UP (BODY WEIGHT)',
                'gym_type_id' => 1,
                'tut_per_rep' => 20,
                'exercise_category_id' => 1,
                'created_at' => '2022-02-05 16:38:54',
                'updated_at' => '2022-02-05 16:38:54',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            35 =>
            array(
                'excercise_desc' => 'SPLIT SQUAT',
                'gym_type_id' => 1,
                'tut_per_rep' => 15,
                'exercise_category_id' => 2,
                'created_at' => '2022-02-05 16:46:35',
                'updated_at' => '2022-02-05 16:46:35',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            36 =>
            array(
                'excercise_desc' => 'PLANK',
                'gym_type_id' => 3,
                'tut_per_rep' => 35,
                'exercise_category_id' => 3,
                'created_at' => '2022-02-05 16:47:23',
                'updated_at' => '2022-02-05 16:47:23',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            37 =>
            array(
                'excercise_desc' => 'CORE OLYMPIC LIFT',
                'gym_type_id' => 3,
                'tut_per_rep' => 20,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:48:27',
                'updated_at' => '2022-02-05 16:50:26',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            38 =>
            array(
                'excercise_desc' => 'CHRUNCHES',
                'gym_type_id' => 3,
                'tut_per_rep' => 30,
                'exercise_category_id' => 3,
                'created_at' => '2022-02-05 16:49:13',
                'updated_at' => '2022-02-05 16:49:13',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            39 =>
            array(
                'excercise_desc' => 'DB PUSH PRESS',
                'gym_type_id' => 3,
                'tut_per_rep' => 20,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:51:04',
                'updated_at' => '2022-02-05 16:51:04',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            40 =>
            array(
                'excercise_desc' => 'KNEE-UP',
                'gym_type_id' => 3,
                'tut_per_rep' => 20,
                'exercise_category_id' => 3,
                'created_at' => '2022-02-05 16:51:54',
                'updated_at' => '2022-02-05 16:51:54',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            41 =>
            array(
                'excercise_desc' => 'SIT UP',
                'gym_type_id' => 3,
                'tut_per_rep' => 60,
                'exercise_category_id' => 3,
                'created_at' => '2022-02-05 16:52:46',
                'updated_at' => '2022-02-05 16:57:52',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            42 =>
            array(
                'excercise_desc' => 'POWER CLEAN / UP',
                'gym_type_id' => 3,
                'tut_per_rep' => 20,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:56:07',
                'updated_at' => '2022-02-05 16:56:07',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            43 =>
            array(
                'excercise_desc' => 'PUSH PRESS',
                'gym_type_id' => 3,
                'tut_per_rep' => 10,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:56:37',
                'updated_at' => '2022-02-05 16:56:37',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            44 =>
            array(
                'excercise_desc' => 'SIDE HOVER',
                'gym_type_id' => 3,
                'tut_per_rep' => 35,
                'exercise_category_id' => 3,
                'created_at' => '2022-02-05 16:57:23',
                'updated_at' => '2022-02-05 16:57:23',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            45 =>
            array(
                'excercise_desc' => 'SHUTTLE RUN (PULL)',
                'gym_type_id' => 3,
                'tut_per_rep' => 40,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:58:47',
                'updated_at' => '2022-02-05 16:58:47',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            46 =>
            array(
                'excercise_desc' => 'SPLIT LEG PUSH PRESS',
                'gym_type_id' => 3,
                'tut_per_rep' => 20,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:59:21',
                'updated_at' => '2022-02-05 16:59:21',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            47 =>
            array(
                'excercise_desc' => 'SPRINTING TECHNIQUE',
                'gym_type_id' => 3,
                'tut_per_rep' => 10,
                'exercise_category_id' => 4,
                'created_at' => '2022-02-05 16:59:50',
                'updated_at' => '2022-02-05 16:59:50',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            48 =>
            array(
                'excercise_desc' => 'WOOD CHOPPER',
                'gym_type_id' => 3,
                'tut_per_rep' => 15,
                'exercise_category_id' => 3,
                'created_at' => '2022-02-05 17:00:25',
                'updated_at' => '2022-02-05 17:00:25',
                'deleted_at' => NULL,
                'tenant_id' => $tenantId,
            ),
            49 =>
            array(
                'excercise_desc' => 'vertical jump',
                'gym_type_id' => 8,
                'tut_per_rep' => 58,
                'exercise_category_id' => 5,
                'created_at' => '2022-09-21 11:26:02',
                'updated_at' => '2022-09-21 11:26:22',
                'deleted_at' => '2022-09-21 11:26:22',
                'tenant_id' => $tenantId,
            ),
        ));
    }
}
