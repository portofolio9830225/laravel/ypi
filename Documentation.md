### Table Names

#### Input

##### Competition Info

-   tabel_kategoris

##### Competition Level

-   level_kategoris

##### Group Classification

-   tabel_kelompoks

##### Phonebook

-   master_contacts

##### Coach Data

-   master_pelatihs

##### Player Data

-   master_pemains

##### Competition Status

-   competition_statuses

##### Competition Detail

-   master_achievements

##### Master YPI

-   master_ypis
-   default_target-
-   ypi_goals-
-   ypi_events-

---

#### Setting

##### S&C JOINT ACTIONS

-   gym_types

##### S&C MUSCLE GROUPS

-   exercise_categories

##### Sub Strength & Conditioning Setting

-   sub_exercises

##### National Height Average Settings

-   national_height_avgs

##### Group Activities

-   group_activities

##### Coach Agendas

-   annual_plan_settings

##### Attendance Settings

-   setting_attendances
-   setting_presences -

---

#### Master Plan

##### YTP

-   season_planners-
-   season_planner_details-
-   season_planner_detail_values-

##### Weekly Training Programme

-   header_weekly_plans
-   detail_weekly_plans-

##### Coach Agenda

-   annual_plans

---

#### Individual

##### Analysis Gaps

-   header_gaps
-   detail_gaps

##### Key Performance Indicators

-   detail_kpi_events
-   detail_kpi_physics
-   detail_pb_kpis

##### Medical Records

-   medical_records

##### Result Categories

-   result_categories

##### Competition Reports

-   competition_reports
-   detail_competition_reports

##### Attendances

-   training_attendances

---

#### Data Management

##### Strength & Conditionings

-   strength_conditionings
-   strength_condition_players
-   strength_condition_values

##### Strength Conditioning Datas

-   strength_conditioning_datas

##### Maturational Statuses

-   height_predicteds
-   height_datas
-   puberty_datas

##### Fitness Monitor Tools

-   fitness_tools

##### Fitness Tests

-   fitness_tests

##### Kpi Tests

-   kpi_parameters
-   kpi_results

---

php artisan infyom:api HeaderKpi --fromTable --tableName=header_kpi
php artisan infyom:api Kpi

###

constants values
'1', 'FIXED_SEASON_PLANS_ID', DETAIL AND SPORTS & STUDY BALANCE
'2', 'FIXED_SEASON_PLAN_DETAILS_ID', Training Phase, Sub Phase, % Intensity, % Volume, % Spesific
'3', 'DEFAULT_SEASON_PLANS',
'4', 'SPD_ACADEMY_INDEX_ID',
'5', 'SPD_SPORT_INDEX_ID',
'6', 'SPD_INTENSITY_ID',
'7', 'SPD_VOLUME_ID',
'8', 'SPD_SPESIFIC_ID',
'9', 'SP_STUDY_BALANCE_ID',
'10', 'SPD_TARGET_ID',
'11', 'SPD_RATE_IMPORTANT_ID',
'12', 'SPD_DATE_FITNESS_ID',
'13', 'SPD_PUBLIC_HOLIDAY_ID',
'14', 'SPD_TRAINING_PHASE_ID',
'15', 'SPD_SUB_PHASE_ID',
'16', 'SP_WORK_TIME_ID',
'17', 'YTP_DATE_COLOR',
'18', 'YTP_SEASON_PLAN_COLOR',

[{"season_plan_id":1,"season_plan_detail_id":2,"active":true,"bg_color":"#ffa940"},{"season_plan_id":1,"season_plan_detail_id":3,"active":true,"bg_color":"#ffa940"},{"season_plan_id":1,"season_plan_detail_id":13,"active":true,"bg_color":"#ffa940"},{"season_plan_id":1,"season_plan_detail_id":14,"active":true,"bg_color":"#ffa940"},{"season_plan_id":1,"season_plan_detail_id":15,"active":true,"bg_color":"#ffa940"},{"season_plan_id":1,"season_plan_detail_id":16,"active":true,"bg_color":"#ffa940"},{"season_plan_id":2,"season_plan_detail_id":4,"active":true,"bg_color":"#ffa940"},{"season_plan_id":2,"season_plan_detail_id":10,"active":true,"bg_color":"#ffa940"},{"season_plan_id":2,"season_plan_detail_id":17,"active":true,"bg_color":"#ffa940"},{"season_plan_id":2,"season_plan_detail_id":18,"active":true,"bg_color":"#ffa940"},{"season_plan_id":2,"season_plan_detail_id":19,"active":true,"bg_color":"#ffa940"},{"season_plan_id":3,"season_plan_detail_id":11,"active":true,"bg_color":"#ffa940"},{"season_plan_id":3,"season_plan_detail_id":24,"active":true,"bg_color":"#ffa940"},{"season_plan_id":3,"season_plan_detail_id":25,"active":true,"bg_color":"#ffa940"},{"season_plan_id":3,"season_plan_detail_id":26,"active":true,"bg_color":"#ffa940"},{"season_plan_id":3,"season_plan_detail_id":27,"active":true,"bg_color":"#ffa940"},{"season_plan_id":13,"season_plan_detail_id":9,"active":true,"bg_color":"#ffa940"},{"season_plan_id":13,"season_plan_detail_id":28,"active":true,"bg_color":"#ffa940"},{"season_plan_id":13,"season_plan_detail_id":29,"active":true,"bg_color":"#ffa940"},{"season_plan_id":13,"season_plan_detail_id":30,"active":true,"bg_color":"#ffa940"},{"season_plan_id":14,"season_plan_detail_id":12,"active":true,"bg_color":"#ffa940"},{"season_plan_id":14,"season_plan_detail_id":20,"active":true,"bg_color":"#ffa940"},{"season_plan_id":14,"season_plan_detail_id":21,"active":true,"bg_color":"#ffa940"},{"season_plan_id":14,"season_plan_detail_id":22,"active":true,"bg_color":"#ffa940"},{"season_plan_id":14,"season_plan_detail_id":23,"active":true,"bg_color":"#ffa940"},{"season_plan_id":14,"season_plan_detail_id":31,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":32,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":33,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":34,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":35,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":36,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":37,"active":true,"bg_color":"#ffa940"},{"season_plan_id":15,"season_plan_detail_id":38,"active":true,"bg_color":"#ffa940"},{"season_plan_id":16,"season_plan_detail_id":39,"active":true,"bg_color":"#ffa940"},{"season_plan_id":17,"season_plan_detail_id":40,"active":true,"bg_color":"#ffa940"},{"season_plan_id":17,"season_plan_detail_id":41,"active":true,"bg_color":"#ffa940"},{"season_plan_id":17,"season_plan_detail_id":42,"active":true,"bg_color":"#ffa940"},{"season_plan_id":17,"season_plan_detail_id":43,"active":true,"bg_color":"#ffa940"},{"season_plan_id":17,"season_plan_detail_id":44,"active":true,"bg_color":"#ffa940"}]
